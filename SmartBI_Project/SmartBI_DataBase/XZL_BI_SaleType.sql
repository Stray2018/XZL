﻿CREATE TABLE [dbo].[XZL_BI_SaleType]
(
	[GroupCode] NVARCHAR(50) NOT NULL, 
    [GroupName] NVARCHAR(100) NOT NULL, 
    [SaleType] INT NOT NULL, 
    [SaleTypeName] NVARCHAR(50) NULL 
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'品类编码',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SaleType',
    @level2type = N'COLUMN',
    @level2name = N'GroupCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'品类名称',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SaleType',
    @level2type = N'COLUMN',
    @level2name = N'GroupName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'分析类别',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SaleType',
    @level2type = N'COLUMN',
    @level2name = N'SaleType'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'高利润销售类别',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SaleType',
    @level2type = NULL,
    @level2name = NULL
﻿CREATE TABLE [dbo].[XZL_BI_SalesOfCategories]
(
	[GroupCode] NVARCHAR(50) NOT NULL, 
    [GroupName] NVARCHAR(50) NULL, 
    [SalesTypeCode] NVARCHAR(50) NULL, 
    [SalesTypeName] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_XZL_BI_SalesOfCategories] PRIMARY KEY ([GroupCode]) 
)

GO
exec sp_addextendedproperty 'MS_Description', '商品销售分类设置', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_SalesOfCategories'
go
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'品类编码',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesOfCategories',
    @level2type = N'COLUMN',
    @level2name = N'GroupCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'品类名称',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesOfCategories',
    @level2type = N'COLUMN',
    @level2name = N'GroupName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'销售分类编码',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesOfCategories',
    @level2type = N'COLUMN',
    @level2name = N'SalesTypeCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'销售分类名称',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesOfCategories',
    @level2type = N'COLUMN',
    @level2name = N'SalesTypeName'
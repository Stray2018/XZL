﻿CREATE TABLE [dbo].[XZL_BI_VIPPointPeroid]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PerName] NVARCHAR(50) NOT NULL, 
    [STPoint] INT NOT NULL, 
    [EnPoint] INT NOT NULL
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'会员积分段设置',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPPointPeroid',
    @level2type = NULL,
    @level2name = NULL
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'积分段说明',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPPointPeroid',
    @level2type = N'COLUMN',
    @level2name = N'PerName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'开始积分',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPPointPeroid',
    @level2type = N'COLUMN',
    @level2name = N'STPoint'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'结束积分',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPPointPeroid',
    @level2type = N'COLUMN',
    @level2name = N'EnPoint'
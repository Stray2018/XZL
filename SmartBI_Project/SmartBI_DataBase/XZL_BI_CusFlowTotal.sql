﻿CREATE TABLE [dbo].[XZL_BI_CusFlowTotal] (
    [EnterDate]         DATE           NOT NULL,
    [shpCode]           NVARCHAR (20)  NOT NULL,
    [shpName]           NVARCHAR (100) NULL,
    [empCode]           NVARCHAR (20)  NOT NULL,
    [empName]           NVARCHAR (100) NULL,
    [Qty1_EnterBatch]   INT            CONSTRAINT [DF__XZL_BI_Cu__Enter__45B6A830] DEFAULT ((0)) NULL,
    [Qty1_EnterTotal]   INT            CONSTRAINT [DF__XZL_BI_Cu__Enter__46AACC69] DEFAULT ((0)) NULL,
    [Qty2_TrialBatch]   INT            CONSTRAINT [DF__XZL_BI_Cu__Trial__479EF0A2] DEFAULT ((0)) NULL,
    [Qty2_TrialTotal]   INT            CONSTRAINT [DF__XZL_BI_Cu__Trial__489314DB] DEFAULT ((0)) NULL,
    [Qty3_BuyBatch]     INT            CONSTRAINT [DF__XZL_BI_Cu__BuyCu__49873914] DEFAULT ((0)) NULL,
    [Qty3_BuyTotal]     INT            CONSTRAINT [DF__XZL_BI_Cu__BuyCu__4A7B5D4D] DEFAULT ((0)) NULL,
    [Qty4_ChannelBatch] INT            CONSTRAINT [DF__XZL_BI_Cu__Chann__4B6F8186] DEFAULT ((0)) NULL,
    [Qty4_ChannelTotal] INT            CONSTRAINT [DF__XZL_BI_Cu__Chann__4C63A5BF] DEFAULT ((0)) NULL,
    [Qty5_ChanBuyBatch] INT            CONSTRAINT [DF__XZL_BI_Cu__ChanB__4D57C9F8] DEFAULT ((0)) NULL,
    [Qty5_ChanBuyTotal] INT            CONSTRAINT [DF__XZL_BI_Cu__ChanB__4E4BEE31] DEFAULT ((0)) NULL,
    [workGroup]         NVARCHAR (20)  NOT NULL,
    CONSTRAINT [XZL_BI_CusFlowTotal_pk] PRIMARY KEY NONCLUSTERED ([EnterDate], [shpCode], [empCode], [workGroup])
);
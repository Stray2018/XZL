﻿CREATE TABLE [dbo].[XZL_BI_VIPActivePeroid]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PerName] NVARCHAR(50) NOT NULL, 
    [StNum] INT NOT NULL, 
    [EnNum] INT NOT NULL
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'会员活跃度设置',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPActivePeroid',
    @level2type = NULL,
    @level2name = NULL
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'活跃度名称',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPActivePeroid',
    @level2type = N'COLUMN',
    @level2name = N'PerName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'结束天数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPActivePeroid',
    @level2type = N'COLUMN',
    @level2name = N'EnNum'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'开始天数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_VIPActivePeroid',
    @level2type = N'COLUMN',
    @level2name = N'StNum'
﻿CREATE TABLE [dbo].[XZL_BI_SalesALYRow]
(
	[shpCode] NVARCHAR(50) NOT NULL , 
    [StDate] DATE NOT NULL, 
    [EnDate] DATE NOT NULL, 
    [RowType] NVARCHAR(10) NOT NULL,
    [Qty1_EnterBatch] INT NULL, 
    [Qty1_EnterTotal] INT NULL, 
    [Qty2_TrialBatch] INT NULL, 
    [Qty2_TrialTotal] INT NULL, 
    [Qty3_BuyBatch] INT NULL, 
    [Qty3_BuyTotal] INT NULL, 
    [PerForBatch] NUMERIC(18, 4) NULL, 
    [PerForTotal] NUMERIC(18, 4) NULL, 
    [QtyForBatch] FLOAT NULL, 
    [QtyForTotal] FLOAT NULL, 
    [QtyForBuy] FLOAT NULL, 
    [PriceForBatch] FLOAT NULL, 
    [PriceForTotal] FLOAT NULL, 
    [YJTotal] NUMERIC(18, 4) NULL, 
    [GoldTotal] NUMERIC(18, 4) NULL, 
    [GoldTotalNot] NUMERIC(18,4) NULL,
    [NotGoldTotal] NUMERIC(18, 4) NULL, 
    CONSTRAINT [PK_XZL_BI_SalesALYRow] PRIMARY KEY ([shpCode], [StDate], [EnDate], [RowType])
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'店铺编码',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'shpCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'开始日期',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'StDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'结束日期',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'EnDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'进店台数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'Qty1_EnterBatch'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'进店人数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'Qty1_EnterTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'试戴台数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'Qty2_TrialBatch'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'试戴人数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'Qty2_TrialTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'购买台数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'Qty3_BuyBatch'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'购买人数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'Qty3_BuyTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'批数成交率',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'PerForBatch'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'人数成交率',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = 'PerForTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'批均件数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'QtyForBatch'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'人均件数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'QtyForTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'购买件数',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'QtyForBuy'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'批均单价',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'PriceForBatch'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'人均单价',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'PriceForTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'总业绩',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = 'YJTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'素金总业绩',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'GoldTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'非素总业绩',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'NotGoldTotal'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'行数据类型(1:计划,2:实际,3:同比数据,4:同比率,5:环比数据,6:环比率)',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'RowType'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'素金非价值化业绩',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesALYRow',
    @level2type = N'COLUMN',
    @level2name = N'GoldTotalNot'
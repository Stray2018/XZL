﻿CREATE TABLE [dbo].[XZL_BI_PromSale]
(
	[shpCode] NVARCHAR(50) NOT NULL, 
    [shpName] NVARCHAR(150) NOT NULL, 
    [StDate] DATE NOT NULL, 
    [EnDate] DATE NOT NULL, 
    [Creator] NVARCHAR(20) NOT NULL, 
    [Remarks] NVARCHAR(200) NOT NULL 
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'店铺促销设置',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = NULL,
    @level2name = NULL
GO

CREATE UNIQUE INDEX [IX_XZL_BI_PromSale_PK] ON [dbo].[XZL_BI_PromSale] ([shpCode], [StDate], [EnDate]) INCLUDE(Remarks)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'店铺编码',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = N'COLUMN',
    @level2name = N'shpCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'店铺名称',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = N'COLUMN',
    @level2name = N'shpName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'开始日期',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = N'COLUMN',
    @level2name = N'StDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'结束日期',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = N'COLUMN',
    @level2name = N'EnDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'创建人姓名',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = N'COLUMN',
    @level2name = N'Creator'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'活动主题',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_PromSale',
    @level2type = N'COLUMN',
    @level2name = N'Remarks'
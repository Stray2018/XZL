﻿create table XZL_BI_StorageAges
(
	AgeCode nvarchar(10) not null,
	AgeDescr NVARCHAR(100) not null,
	Compare NVARCHAR(5) not null,
	DateType nVarchar(10) not null,
	StValue int not null,
	EnValue int not null,
	LinkOITMFld Nvarchar(50) not null,
	LinkOITMValue NVARCHAR(100) not null,
	CalcOITMFld nvarchar(50) not null, 
    CONSTRAINT [PK_XZL_BI_StorageAges] PRIMARY KEY ([AgeCode]) 
)
go

exec sp_addextendedproperty 'MS_Description', '库齡段', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges'
go

exec sp_addextendedproperty 'MS_Description', '库齡段编码', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'AgeCode'
go

exec sp_addextendedproperty 'MS_Description', '库龄段描述', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'AgeDescr'
go

exec sp_addextendedproperty 'MS_Description', '比较符', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'Compare'
go

exec sp_addextendedproperty 'MS_Description', '日期类别', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'DateType'
go

exec sp_addextendedproperty 'MS_Description', '开始时段', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'StValue'
go

exec sp_addextendedproperty 'MS_Description', '结束时段', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'EnValue'
go

exec sp_addextendedproperty 'MS_Description', '关联物料档案字段，数据统计来源', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'LinkOITMFld'
go

exec sp_addextendedproperty 'MS_Description', '关联物料档案自段对应的值', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'LinkOITMValue'
go

exec sp_addextendedproperty 'MS_Description', '计算库龄的字段', 'SCHEMA', 'dbo', 'TABLE', 'XZL_BI_StorageAges', 'COLUMN', 'CalcOITMFld'
go
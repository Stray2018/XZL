﻿CREATE TABLE [dbo].[XZL_BI_SalesAnalysis]
(
	[shpCode] NVARCHAR(50) NOT NULL , 
    [shpName] NVARCHAR(100) NULL, 
    [StDate] DATE NULL, 
    [EnDate] DATE NULL, 
    [Creator] NVARCHAR(50) NULL, 
    [causeALY] NVARCHAR(MAX) NULL, 
    [SugForimprove] NVARCHAR(MAX) NULL, 
    [WorkSummary] NVARCHAR(max) NULL,
    [CreateDate] DATETIME NULL, 
    CONSTRAINT [AK_XZL_BI_SalesAnalysis_Column] UNIQUE ([shpCode], [StDate], [EnDate])
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'店铺编码',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'shpCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'店铺名称',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'shpName'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'开始日期',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'StDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'结束日期',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'EnDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'创建人',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'Creator'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'原因分析',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'causeALY'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'改进意见',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'SugForimprove'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'创建人',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'CreateDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'日常工作总结',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'XZL_BI_SalesAnalysis',
    @level2type = N'COLUMN',
    @level2name = N'WorkSummary'
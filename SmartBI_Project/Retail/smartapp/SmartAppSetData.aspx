﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SmartAppSetData.aspx.cs" Inherits="WebApp1.App_Code.SmartAppSetData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no,minimum-scalable=1.0,maximum-scale=1.0" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <asp:Label Font-Size="20" ID="Label1" runat="server" Text="每日人数汇报"></asp:Label>
        </div>
        <div  align="left" style="width:100%;">
            <asp:Label ID="Label2" runat="server" Text="姓名:"></asp:Label>
            <asp:TextBox Font-Bold="True" ID="empNameSet" runat="server" Enabled="False" Font-Names="宋体" Font-Size="12pt" ForeColor="Black" Width="60px" ></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="店铺:"></asp:Label><asp:TextBox Font-Bold="True" ID="shpNameSet" runat="server" Enabled="False" Font-Names="宋体" Font-Size="12pt" ForeColor="Black" Width="161px"></asp:TextBox>
            </div><br/>
        <div  align="left" style="width: 100%">
            <asp:Label ID="Label4" runat="server" Text="日期:"></asp:Label><asp:TextBox  ID="DocDate" TextMode="Date" runat="server" AutoPostBack="true" ></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server">班组:</asp:Label>
            <asp:RadioButton  ID="RadioButton1" Text="A班" runat="server" GroupName="AB" />&nbsp;&nbsp;&nbsp;<asp:RadioButton  ID="RadioButton2" Text="B班" runat="server" GroupName="AB" />
        </div>
        <div style="width: 100%" >
        <asp:Repeater ID="oData2" runat="server">
            <HeaderTemplate>
                <table>
                <tr style="border: black; border-width: thin;">
                    <th style="width: 10%">序号</th>
                    <th style="width: 40%">项目</th>
                    <th style="width: 25%">台数</th>
                    <th style="width: 25%">人数</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="text-align: center" ><asp:Label runat="server" ID="xh" Text='<%# Eval("xh") %>'></asp:Label></td>
                    <td style="text-align: center"><asp:Label runat="server" ID="Project" Text='<%# Eval("Project") %>'></asp:Label></td>
                    <td style="text-align: center" ><asp:TextBox Width="80%" runat="server" ID="Batch" Text='<%# Eval("BatchQty") %>'></asp:TextBox></td>
                    <td style="text-align: center"><asp:TextBox Width="80%" runat="server" ID="Total" Text='<%# Eval("TotalQty") %>'></asp:TextBox></td>
                    <td><asp:Label Visible="False" runat="server" ID="ProjectName"  Text='<%# Eval("ProjectName") %>'></asp:Label></td>
                    <td><asp:Label Visible="False" runat="server" ID="QtyName"  Text='<%# Eval("QtyName") %>'></asp:Label></td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr>
                    <td style="text-align: center">
                        <asp:Label runat="server" ID="xh" Text='<%# Eval("xh") %>'></asp:Label></td>
                    <td style="text-align: center">
                        <asp:Label runat="server" ID="Project" Text='<%# Eval("Project") %>'></asp:Label></td>
                    <td style="text-align: center">
                        <asp:TextBox Width="80%" runat="server" ID="Batch" Text='<%# Eval("BatchQty") %>'></asp:TextBox></td>
                    <td style="text-align: center">
                        <asp:TextBox Width="80%" runat="server" ID="Total" Text='<%# Eval("TotalQty") %>'></asp:TextBox></td>
                    <td>
                        <asp:Label Visible="False" runat="server" ID="ProjectName" Text='<%# Eval("ProjectName") %>'></asp:Label></td>
                    <td>
                        <asp:Label Visible="False" runat="server" ID="QtyName" Text='<%# Eval("QtyName") %>'></asp:Label></td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                <tr>
                    <asp:Literal ID="totalData" runat="server"></asp:Literal>
                </tr>
            </table>
            </FooterTemplate>
           
        </asp:Repeater>
        </div>
        <br/><br/>
        <div style="width: 100%" >
            <asp:Button ID="Button2"  runat="server" Font-Size="15" Text="查询" OnClick="Button2_Click" style="margin-left: 5%"  />
            <asp:Button ID="Button1"  runat="server" Font-Size="15" Text="保存" OnClick="Button1_Click" style="margin-left: 35%" />
        </div>
        
    </form>
</body>
</html>

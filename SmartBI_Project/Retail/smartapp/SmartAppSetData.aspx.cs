﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Retail;

namespace WebApp1.App_Code
{
    public partial class SmartAppSetData : System.Web.UI.Page
    {
        //public string mobile = ,empCode = "", empName = "", shpCode ="", shpName = "",workGroup="";
        public string workGroup = "";
        
        public static DataTable oDataTable = new DataTable("MyData");

       
        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                workGroup = RadioButton1.Checked ? "A班" : RadioButton2.Checked ? "B班" : string.Empty;
                if (workGroup.Equals(String.Empty))
                {
                    ClientScript.RegisterStartupScript(GetType(), "message",
                        "<script>alert('班组未选择!');</script>");
                    return;
                }
                bindData();
            }
            catch (Exception exception)
            {
                ClientScript.RegisterStartupScript(GetType(), "message",
                    "<script>alert('"+exception.ToString()+"');</script>");
                WriteLog(exception.ToString());
            }
            
        }

        public static string ColumnList = "";
        public static string sqlcmd = "";
        protected void Button1_Click(object sender, EventArgs e)
        {
            sqlcmd = "";
            try
            {
                if (Request.Cookies["SmartAppMobile"] ==null)
                {
                    WriteLog("登陆超时!"+ Session["Mobile"].ToString());
                    ClientScript.RegisterStartupScript(GetType(), "message",
                        "<script>alert('登陆超时，请重新登陆!');</script>");
                    Response.Redirect("SmartAppLogin.aspx",false);
                    Context.ApplicationInstance.CompleteRequest();
                    WriteLog("登陆超时!" + Session["Mobile"].ToString());
                    return;
                }
                workGroup = RadioButton1.Checked ? "A班" : RadioButton2.Checked ? "B班" : "";
            if (workGroup.Equals(""))
            {
                ClientScript.RegisterStartupScript(GetType(), "message",
                    "<script>alert('班组未选择!');</script>");
                return;
            }
            if (ColumnList.Equals(""))
            {
                WriteLog("字段列为空!" + Session["Mobile"].ToString());
                ClientScript.RegisterStartupScript(GetType(), "message",
                    "<script>alert('数据列表为空,请重新登陆!');</script>");
               
                Response.Redirect("SmartAppLogin.aspx",false);
                Context.ApplicationInstance.CompleteRequest();
                WriteLog("字段列为空!" + Session["Mobile"].ToString());
                    return;
            }
                string ValueResult = ColumnList.Replace("EnterDate", "'"+DocDate.Text+"'").Replace("shpCode","'"+Session["shpCode"].ToString()+"'").Replace("workGroup", "'"+workGroup+"'")
                .Replace("shpName", "'"+Session["shpName"].ToString() + "'").Replace("empCode", "'" +Session["empCode"].ToString() + "'").Replace("empName", "'" +Session["empName"].ToString() + "'");
            foreach (RepeaterItem dlItem in oData2.Items)//遍历每一行数据
            {
                Label xhLabel = dlItem.FindControl("xh") as Label;
                Label project = dlItem.FindControl("Project") as Label;
                Label ProjectName = dlItem.FindControl("ProjectName") as Label;
                Label QtyName = dlItem.FindControl("QtyName") as Label;
                TextBox BatcyQty = dlItem.FindControl("Batch") as TextBox;
                TextBox TotalQty = dlItem.FindControl("Total") as TextBox;
                string ColumName = QtyName.Text + "_" + ProjectName.Text;
                ValueResult = ValueResult.Replace( ColumName+ "Batch", BatcyQty.Text == "" ? "0" : BatcyQty.Text);
                ValueResult = ValueResult.Replace(ColumName + "Total", TotalQty.Text == "" ? "0" : TotalQty.Text);
            }

             sqlcmd =
                "delete  FROM XZL_BI_CusFlowTotal WHERE EnterDate='"+DocDate.Text+"' AND shpCode='"+Session["shpCode"].ToString()+"' AND empCode='"+Session["empCode"].ToString()+"' and workGroup='"+workGroup+"';" +
                "insert XZL_BI_CusFlowTotal( " + ColumnList+")values("+ValueResult+") " ;

                SqlConnection oSqlConnection =
                    new SqlConnection(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
                oSqlConnection.Open();
                
                SqlCommand oSqlCommand = oSqlConnection.CreateCommand();
                oSqlCommand.CommandText = sqlcmd;
                oSqlCommand.ExecuteNonQuery();
                oSqlConnection.Close();
                ClientScript.RegisterStartupScript(GetType(), "message",
                "<script>alert('数据保存成功!');</script>");
               

            }
            catch (Exception exception)
            {
                //Page.RegisterStartupScript("", "<script language='javascript'>window.alert('"+exception.ToString()+"');</script>");
               ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('" + exception.ToString().Replace("'","\'").Replace("\r\n","\\r\\n")+"');</script>");
               WriteLog(Session["Mobile"].ToString()+exception.ToString()+Environment.NewLine+sqlcmd);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if(Request.Cookies["SmartAppMobile"] == null)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "message",
                            "<script>alert('请重新登陆!');</script>");
                        Response.Redirect("SmartAppLogin.aspx",false);
                        Context.ApplicationInstance.CompleteRequest();
                        return;
                    }
                    DocDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    bindData();
                }
            }
            catch (Exception exception)
            {
                ClientScript.RegisterStartupScript(GetType(), "message",
                    "<script>alert('" + exception.Message + "');</script>");
                WriteLog(exception.ToString());
            }
        }

        protected void bindData()
        {
            SqlConnection oSqlConnection =
                new SqlConnection(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
            oSqlConnection.Open();
            if (Session["shpName"]==null)
            {
                Response.Redirect("SmartAppLogin.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
                return;
            }
            shpNameSet.Text = Session["shpName"].ToString();
            empNameSet.Text = Session["empName"].ToString();            
            workGroup = RadioButton1.Checked ? "A班" : RadioButton2.Checked ? "B班" : "";
            
            oDataTable.Clear();
            string sqlCmd =
                "SELECT ROW_NUMBER()OVER(ORDER BY LEFT(b.name,CHARINDEX('_',b.name)-1)) xh,REPLACE(REPLACE(CONVERT(VARCHAR(max), c.value),'人数',''),'台数','') AS Project, " +
                "REPLACE(REPLACE(RIGHT(b.Name, LEN(b.Name) - CHARINDEX('_', b.Name)), 'Batch', ''), 'Total', '') ProjectName ,LEFT(b.name, CHARINDEX('_', b.name) - 1) QtyName, " +
                "CONVERT(INT, null) AS BatchQty, CONVERT(INT, null) AS TotalQty " +
                "FROM sys.tables a LEFT JOIN sys.columns b ON a.object_id = b.object_id LEFT JOIN sys.extended_properties c ON a.object_id = c.major_id " +
                "WHERE a.name = 'XZL_BI_CusFlowTotal' AND c.minor_id <> 0 AND b.column_id = c.minor_id AND b.name LIKE 'Qty%' " +
                "AND a.schema_id = (SELECT schema_id FROM   sys.schemas WHERE  name = 'dbo' ) " +
                "GROUP BY REPLACE(REPLACE(CONVERT(VARCHAR(max), c.value), '人数', ''), '台数', ''),REPLACE(REPLACE(RIGHT(b.Name, LEN(b.Name) - CHARINDEX('_', b.Name)), 'Batch', ''), 'Total', ''),LEFT(b.name, CHARINDEX('_', b.name) - 1) " +
                "ORDER BY LEFT(b.name, CHARINDEX('_', b.name) - 1)";
            oDataTable = new DataTable("MyData");

            
            SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(sqlCmd, oSqlConnection);
            oSqlDataAdapter.Fill(oDataTable);

            string sqlCmd1 = "select * from XZL_BI_CusFlowTotal where shpCode ='" +Session["shpCode"].ToString() +
                             "' and empCode = '" +Session["empCode"].ToString() + "' and EnterDate='" + DocDate.Text + "' and workGroup = '"+workGroup+"'";

            DataTable ODtData = new DataTable("MyData");
            oSqlDataAdapter = new SqlDataAdapter(sqlCmd1, oSqlConnection);
            oSqlDataAdapter.Fill(ODtData);
            oSqlConnection.Close();
            oSqlConnection.Dispose();
            ColumnList = "";
            foreach (DataColumn oColumn in ODtData.Columns)
            {
                ColumnList += ',' + oColumn.ColumnName;
            }

            ColumnList = ColumnList.Substring(1);
            if (oDataTable.Rows.Count > 0)
            {
                if (ODtData.Rows.Count > 0)
                {
                    foreach (DataRow oDataRow in oDataTable.Rows)
                    {
                        string BatchQty = oDataRow["QtyName"].ToString() + "_" + oDataRow["ProjectName"] +
                                          "Batch";
                        string TotalQty = oDataRow["QtyName"].ToString() + "_" + oDataRow["ProjectName"] +
                                          "Total";
                        oDataRow["BatchQty"] = ODtData.Rows[0][BatchQty];
                        oDataRow["TotalQty"] = ODtData.Rows[0][TotalQty];
                    }
                }

                oData2.DataSource = oDataTable;
                oData2.DataBind();
            }
            

        }

        protected void WriteLog(string logtxt)
        {
            StreamWriter oWriter = new StreamWriter(Request.PhysicalApplicationPath + "\\errLog.txt", true);
            oWriter.Write("日期："+DateTime.Now.ToString()+"\t"+logtxt.Replace("\r\n","")+Environment.NewLine);
            oWriter.Flush();
            oWriter.Close();
            oWriter.Dispose();
        }
       
    }
    }
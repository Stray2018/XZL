﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Retail;

namespace WebApp1.App_Code
{
    public partial class SmartAppLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void loginERP(string mobile)
        {
            SqlConnection oSqlConnection =
                new SqlConnection(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
            oSqlConnection.Open();
            string sqlLogo = "SELECT T1.empCode,T1.empName,T3.DocEntry as shpCode,T3.shpName,T1.Mobile FROM OHEM T1 INNER JOIN CMP1 T2 ON T1.DepEntry=T2.DocEntry INNER JOIN OSHP T3 ON T2.DocEntry=T3.DepEntry WHERE T1.Mobile='"+TextBox1.Text+"' AND T1.Dimission=0";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlLogo, oSqlConnection);
            DataTable oDs = new DataTable();
            dataAdapter.Fill(oDs);
            if (oDs.Rows.Count.Equals(1))
            {
                Session.Clear();
                Session["Mobile"]= oDs.Rows[0]["Mobile"].ToString();
                Session["shpName"] = oDs.Rows[0]["shpName"].ToString();
                Session["empName"] = oDs.Rows[0]["empName"].ToString();
                Session["empCode"] = oDs.Rows[0]["empCode"].ToString();
                Session["shpCode"] = oDs.Rows[0]["shpCode"].ToString();
                oSqlConnection.Close();
                oSqlConnection.Dispose();
                HttpCookie oHttpCookie = new HttpCookie("SmartAppMobile",mobile);
                oHttpCookie.Expires = DateTime.Now.AddMinutes(20);
                Response.Cookies.Add(oHttpCookie);
                
               Response.Redirect("SmartBI.html",false);
               
            }
            else
            {
                oSqlConnection.Close();
                oSqlConnection.Dispose();
                ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('登陆失败,请检查手机号是否正确!');</script>");
            }
                
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //if (TextBox1.Text.Length.Equals(11))
            //{
                loginERP(TextBox1.Text);
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('手机号输入不正确,请重新输入!');</script>");
            //}
        }
    }
}
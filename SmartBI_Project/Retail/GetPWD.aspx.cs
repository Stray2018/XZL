﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;
using Retail;

namespace WebApp1
{
    public partial class GetPWD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Visible = false;   
        }

        protected void oButton1_Click(object sender, EventArgs e)
        {
            Label1.Visible = false;
            //string MyDb = System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"];
            //SqlConnection oConnection = new SqlConnection(MyDb);
            //oConnection.Open();
            if (Global.sqlConn.State == ConnectionState.Open)
            {
                Global.sqlConn.Close();
            }
            Global.sqlConn.Open();
            string sqlLogo = "select * from OUSR where UserCode='"+oUsr.Text+"' and Password='"+oPwd.Text+"'";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlLogo, Global.sqlConn);
            DataTable oDs = new DataTable();
            dataAdapter.Fill(oDs);
            if(oDs.Rows.Count>0)
            {
                string sqlCmd = "";
                if (Global.sqlConn.ConnectionString.ToUpper().Contains("JXF_FORWIN"))
                {
                    sqlCmd = "select SpecialValueC from OSYS where Code='SA_A47'";
                    SqlDataReader oDataReader;
                    SqlCommand oSqlCommand = new SqlCommand(sqlCmd, Global.sqlConn);
                    oDataReader = oSqlCommand.ExecuteReader();
                    oDataReader.Read();
                    Label1.Text = oDataReader[0].ToString();
                    Label1.Visible = true;
                    oDataReader.Close();
                    Global.sqlConn.Close();
                    Label1.Visible = true;
                    return;
                }
                    
                sqlCmd = "select Duty from OUSR T1 inner join OHEM T2 on T1.UserCode=T2.UserID and t1.UserCode='" + oUsr.Text + "'";
                dataAdapter.SelectCommand.CommandText = sqlCmd;
                oDs= new DataTable();
                dataAdapter.Fill(oDs);
                if (!(oDs.Rows.Count > 0)) { return; }
                string duty = oDs.Rows[0][0].ToString();
                sqlCmd = "";
                if (duty.Equals("总经理") || duty.Equals("副总"))
                {
                    sqlCmd = "select SpecialValueC from OSYS where Code='SO_A152';";
                }
                else if (duty.Equals("店长"))
                {
                    sqlCmd = "select SpecialValueC from OSYS where Code='SO_A15';";
                }
                else if (duty.Equals("总监") || duty.Equals("营运经理"))
                {
                    sqlCmd = "select SpecialValueC from OSYS where Code='SO_A151';";

                }
                if (!sqlCmd.Equals(""))
                {
                    oDs = new DataTable();
                    dataAdapter.SelectCommand.CommandText = sqlCmd;
                    dataAdapter.Fill(oDs);
                    Label1.Text = oDs.Rows[0][0].ToString();
                    Label1.Visible = true;
                }
                
            }

            else
            {
                ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('登陆失败,请检查用户名和密码!');</script>");
            }
        }
    }
}
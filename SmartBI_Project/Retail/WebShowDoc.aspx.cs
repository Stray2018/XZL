﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;

namespace Retail
{
    public partial class WebShowDoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            CrystalReportViewer1.DisplayToolbar = false;
            CrystalReportViewer1.DisplayStatusbar = false;
            CrystalReportViewer1.Visible = false;
            CrystalReportViewer1.BestFitPage = true;
            CrystalReportViewer1.BorderStyle = BorderStyle.None;
            CrystalReportViewer1.DocumentView = DocumentViewType.WebLayout;
            CrystalReportViewer1.ShowAllPageIds = true;
            CrystalReportViewer1.DisplayGroupTree = false;
            CrystalReportViewer1.HasToggleGroupTreeButton = false;
            GroupTreeStyle oTreeStyle = new GroupTreeStyle();
           oTreeStyle.Width = 0;
           CrystalReportViewer1.GroupTreeStyle = oTreeStyle;
          
            string DocEntry = Request.QueryString["DocEntry"];
            
            string TempPath = WebConfigurationManager.AppSettings["QATempPath"];
            string templateSql = WebConfigurationManager.AppSettings["QATempSql"]+DocEntry;
            string TemplateName = "";
            SqlConnection oSqlConnection = new SqlConnection(WebConfigurationManager.AppSettings["sqlPosConnectionString"]);
            SqlDataAdapter oDataAdapter = new SqlDataAdapter(templateSql,oSqlConnection);
            DataTable oDataTable = new DataTable();
            oDataAdapter.Fill(oDataTable);
            if (oDataTable.Rows.Count>0)
            {
                TemplateName = oDataTable.Rows[0][0].ToString();
            }
            oDataAdapter.Dispose();
            oSqlConnection.Close();
            CrystalReportSource tempSource = new CrystalReportSource();
            string reportfile = TempPath + "\\" + TemplateName + ".rpt";
            tempSource.Report.FileName = reportfile;
            tempSource.ReportDocument.Load(reportfile,OpenReportMethod.OpenReportByTempCopy);
            
            string sqlConnstr = System.Web.Configuration.WebConfigurationManager.AppSettings["sqlPosConnectionString"];
            SqlConnectionStringBuilder oStringBuilder = new SqlConnectionStringBuilder(sqlConnstr);

            foreach (IConnectionInfo connectionInfo in tempSource.ReportDocument.DataSourceConnections)
            {
                connectionInfo.SetConnection(oStringBuilder.DataSource, oStringBuilder.InitialCatalog, oStringBuilder.UserID, oStringBuilder.Password);
            }
            tempSource.ReportDocument.SetDatabaseLogon(oStringBuilder.UserID, oStringBuilder.Password, oStringBuilder.DataSource, oStringBuilder.InitialCatalog, true);
            tempSource.ReportDocument.SetParameterValue("DocEntry",DocEntry);
            tempSource.DataBind();
            CrystalReportViewer1.ReportSource = tempSource;
            CrystalReportViewer1.Visible = true;

        }
    }
}
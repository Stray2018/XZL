﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp1
{
    public partial class PDFPrievw : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string QueStr ="~/bin/ExData/"+Request.QueryString["PDFfile"]+".pdf";
            string filepath = Server.MapPath(QueStr);
            Priview(this, filepath);
        }
        public static void Priview(System.Web.UI.Page p, string inFilePath)
        {
            p.Response.ContentType = "Application/pdf";
            string fileName = inFilePath.Substring(inFilePath.LastIndexOf('\\') + 1);
            p.Response.AddHeader("content-disposition", "filename=" + fileName);
            p.Response.WriteFile(inFilePath);
            p.Response.End();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Retail
{
    public class Global : System.Web.HttpApplication
    {
        public static SqlConnection sqlConn = new SqlConnection();

        protected void Application_Start(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder oBuilder =
                new SqlConnectionStringBuilder(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
            oBuilder.Pooling = true;
            oBuilder.MinPoolSize = 2;
            oBuilder.MaxPoolSize = 20;
            if (sqlConn.State == ConnectionState.Open)
            {
              sqlConn.Close();  
            }
            sqlConn.ConnectionString = oBuilder.ConnectionString;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder oBuilder =
                new SqlConnectionStringBuilder(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
            oBuilder.Pooling = true;
            oBuilder.MinPoolSize = 2;
            oBuilder.MaxPoolSize = 20;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = oBuilder.ConnectionString;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder oBuilder =
                new SqlConnectionStringBuilder(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
            oBuilder.Pooling = true;
            oBuilder.MinPoolSize = 2;
            oBuilder.MaxPoolSize = 20;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = oBuilder.ConnectionString;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder oBuilder =
                new SqlConnectionStringBuilder(System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"]);
            oBuilder.Pooling = true;
            oBuilder.MinPoolSize = 2;
            oBuilder.MaxPoolSize = 20;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = oBuilder.ConnectionString;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
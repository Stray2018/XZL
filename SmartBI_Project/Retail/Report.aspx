﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="WebApp1.Report" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script type="text/javascript">
        function Print() {
            var dvReport = document.getElementById("dvReport");
            var frame1 = dvReport.getElementsByTagName("iframe")[0];
            if (navigator.appName.indexOf("Internet Explorer") != -1) {
                frame1.name = frame1.id;
                window.frames[frame1.id].focus();
                window.frames[frame1.id].print();
            }
            else {
                var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
                frameDoc.print();
            }
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
       
    <div style="margin-left: 80px">
    
        <asp:Label ID="Label1" runat="server" Text="班次:"></asp:Label>
        <asp:DropDownList ID="DropDownList2" Width="84px" runat="server">
            <asp:ListItem>A班</asp:ListItem>
        </asp:DropDownList>
        &nbsp;  
        &nbsp;  
        <asp:Label ID="Label2" runat="server" Text="日期:"></asp:Label>
        &nbsp;
        <asp:TextBox ID="TextBox2" runat="server" TextMode="Date"></asp:TextBox>
        &nbsp;
        <asp:Label ID="Label3" runat="server" Text="店铺:" Width="40px"></asp:Label>
        &nbsp; 
        <asp:DropDownList ID="DropDownList1" runat="server" Width="141px">
            <asp:ListItem>空</asp:ListItem>
        </asp:DropDownList>
        &nbsp;&nbsp;    
        <asp:Button ID="Button1" runat="server" Text="查询" Width="50px" OnClick="Button1_Click" Font-Size="12pt" />
        &nbsp;&nbsp;
        <%--<asp:Button ID="Button2" runat="server" Text="打印" OnClick="Button2_Click" />
        &nbsp;--%>
        <%--<asp:Button ID="btnPrint" runat="server" Text="打印" OnClientClick="Print()"></asp:Button>--%>
        <%--&nbsp;&nbsp;--%>
        <%--<asp:Label ID=--%>
        <%--<asp:Button ID="Button2" runat="server" OnClick="Button2_Click1" Text="打印" />--%>
        <br/>
    
    </div>
        <br/>
        <div>
           
            <div id="dvReport">
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True" GroupTreeImagesFolderUrl="" Height="50px" ReportSourceID="CrystalReportSource1" ToolbarImagesFolderUrl="" ToolPanelView="None" ToolPanelWidth="200px" Width="350px" HasSearchButton="False" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" HasZoomFactorList="False" DisplayStatusbar="False" EnableDatabaseLogonPrompt="False" EnableDrillDown="False" EnableParameterPrompt="False" ReuseParameterValuesOnRefresh="True" />
            <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                <%--<Report FileName=Server.MapPath("/bin/Report.rpt") />--%> 
            </CR:CrystalReportSource>
            </div>  
        </div>
    </form>
</body>
</html>

﻿CREATE TABLE [dbo].[IDX_PO_HEADER](
	[Owner] [NVARCHAR](10) NOT NULL,
	[CUSTOMERID] [NVARCHAR](15) NOT NULL,
	[POTYPE] [NVARCHAR](2) NULL,
	[PONO] [NVARCHAR](20) NOT NULL,
	[CREATIONTIME] [datetime] NULL,
	[EXPECTARRIVETIME] [datetime] NULL,
	[SUPPLIERID] [NVARCHAR](35) NULL,
	[SUPPLIERNAME] [NVARCHAR](100) NULL,
	[ASNREFERENCE2] [NVARCHAR](100) NULL,
	[ASNREFERENCE3] [NVARCHAR](100) NULL,
	[ASNREFERENCE4] [NVARCHAR](100) NULL,
	[ASNREFERENCE5] [NVARCHAR](100) NULL,
	[H_EDI_01] [NVARCHAR](100) NULL,
	[H_EDI_02] [NVARCHAR](100) NULL,
	[H_EDI_03] [NVARCHAR](100) NULL,
	[H_EDI_04] [NVARCHAR](100) NULL,
	[H_EDI_05] [NVARCHAR](100) NULL,
	[USERDEFINE1] [NVARCHAR](100) NULL,
	[USERDEFINE2] [NVARCHAR](100) NULL,
	[USERDEFINE3] [NVARCHAR](100) NULL,
	[USERDEFINE4] [NVARCHAR](100) NULL,
	[USERDEFINE5] [NVARCHAR](100) NULL,
	[CREATETIME] [datetime] NULL,
	[INTERFACEFLAG] [char](1) NULL,
	[INTERFACEREASON] [NVARCHAR](200) NULL,
	[Warehouse] [char](10) NULL,
	[Notes] [NVARCHAR](2000) NULL,
 CONSTRAINT [IX_HEADER_P] PRIMARY KEY CLUSTERED 
(
	[PONO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

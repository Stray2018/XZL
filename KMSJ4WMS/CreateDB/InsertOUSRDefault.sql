﻿IF NOT EXISTS(SELECT * FROM ousr WHERE usercode = 'admin')
INSERT dbo.OUSR
(
    UserCode,
    UserName,
    oPWD,
    Supper,
    IsPur,
    IsRet,
    IsSal,
    IsRet4Sal
)
VALUES
(   'admin',   -- UserCode - NVARCHAR(20)
    'admin',   -- UserName - NVARCHAR(20)
    '123456',   -- oPWD - NVARCHAR(100)
    1, -- Supper - bit
    1, -- IsPur - bit
    1, -- IsRet - bit
    1, -- IsSal - bit
    1  -- IsRet4Sal - bit
    )
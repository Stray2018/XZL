﻿namespace KMSJ4WMS
{
    partial class Logon
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.oUser = new System.Windows.Forms.TextBox();
            this.oPWD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IsOK = new System.Windows.Forms.Button();
            this.IsClose = new System.Windows.Forms.Button();
            this.IsSet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(295, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "用户";
            // 
            // oUser
            // 
            this.oUser.Location = new System.Drawing.Point(339, 104);
            this.oUser.Name = "oUser";
            this.oUser.Size = new System.Drawing.Size(100, 21);
            this.oUser.TabIndex = 1;
            // 
            // oPWD
            // 
            this.oPWD.Location = new System.Drawing.Point(339, 148);
            this.oPWD.Name = "oPWD";
            this.oPWD.PasswordChar = '*';
            this.oPWD.Size = new System.Drawing.Size(100, 21);
            this.oPWD.TabIndex = 3;
            this.oPWD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.oPWD_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "密码";
            // 
            // IsOK
            // 
            this.IsOK.Location = new System.Drawing.Point(255, 225);
            this.IsOK.Name = "IsOK";
            this.IsOK.Size = new System.Drawing.Size(75, 23);
            this.IsOK.TabIndex = 4;
            this.IsOK.Text = "登陆";
            this.IsOK.UseVisualStyleBackColor = true;
            this.IsOK.Click += new System.EventHandler(this.IsOK_Click);
            // 
            // IsClose
            // 
            this.IsClose.Location = new System.Drawing.Point(364, 225);
            this.IsClose.Name = "IsClose";
            this.IsClose.Size = new System.Drawing.Size(75, 23);
            this.IsClose.TabIndex = 5;
            this.IsClose.Text = "关闭";
            this.IsClose.UseVisualStyleBackColor = true;
            this.IsClose.Click += new System.EventHandler(this.IsClose_Click);
            // 
            // IsSet
            // 
            this.IsSet.Location = new System.Drawing.Point(478, 225);
            this.IsSet.Name = "IsSet";
            this.IsSet.Size = new System.Drawing.Size(75, 23);
            this.IsSet.TabIndex = 6;
            this.IsSet.Text = "配置";
            this.IsSet.UseVisualStyleBackColor = true;
            this.IsSet.Click += new System.EventHandler(this.IsSet_Click);
            // 
            // Logon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 428);
            this.Controls.Add(this.IsSet);
            this.Controls.Add(this.IsClose);
            this.Controls.Add(this.IsOK);
            this.Controls.Add(this.oPWD);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.oUser);
            this.Controls.Add(this.label1);
            this.Name = "Logon";
            this.Text = "登陆";
            this.Load += new System.EventHandler(this.Logon_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox oUser;
        private System.Windows.Forms.TextBox oPWD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button IsOK;
        private System.Windows.Forms.Button IsClose;
        private System.Windows.Forms.Button IsSet;
    }
}


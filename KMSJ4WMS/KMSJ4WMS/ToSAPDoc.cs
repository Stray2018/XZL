﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SAPbobsCOM;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Collections;

namespace KMSJ4WMS
{
    public partial class ToSAPDoc : Form
    
    {
        public static SAPbobsCOM.Company oCompany = new CompanyClass();
        public ToSAPDoc()
        {
            InitializeComponent();
        }

        private void ToCenPur_Click(object sender, EventArgs e)
        {
            Common.Common.CenterPur(false);
        }

        private void ToCenRePur_Click(object sender, EventArgs e)
        {
            Common.Common.CenterRePur(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Common.Common.WMSToTOPDN(false);
        }

        private void ToSAPSalePur_Click(object sender, EventArgs e)
        {
            Common.Common.CenterToShp(false);

        }

        private void TOSAPRePurReSale_Click(object sender, EventArgs e)
        {
            Common.Common.ShpToCenter(false);
        }

       static string serviceFilePath = $"{Application.StartupPath}\\KMSJ4WMS.exe";
       static string serviceName = "WMS4SAP";

        private void InstallService_Click(object sender, EventArgs e)
        {
            if (this.IsServiceExisted(serviceName)) this.UninstallService1(serviceName);
            this.InstallService1(System.Windows.Forms.Application.ExecutablePath);
        }

        private void StartService_Click(object sender, EventArgs e)
        {
 
                if (this.IsServiceExisted(serviceName)) this.ServiceStart(serviceName);
   
        }

        private void StopService_Click(object sender, EventArgs e)
        {
            
                if (this.IsServiceExisted(serviceName)) this.ServiceStop(serviceName);

        }

        private void UninstallService_Click(object sender, EventArgs e)
        {
           
                if (this.IsServiceExisted(serviceName))
                {
                    this.ServiceStop(serviceName);
                    this.UninstallService1(System.Windows.Forms.Application.ExecutablePath);
                }
           
        }
        private bool IsServiceExisted(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            string serEmpty = String.Empty;
            foreach (ServiceController sc in services)
            {
               
                if (sc.ServiceName.ToLower() == serviceName.ToLower())
                {
                    return true;
                }
            }
            return false;
        }

        //安装服务
        private void InstallService1(string serviceFilePath)
        {
            try
            {
                Common.ServiceHelper.Install(
                    "WMS4SAP",
                    "鑫源堂WMS和SAP接口",
                    "\"" + serviceFilePath + "\"" + " /Service",
                    "自动同步WMS数据",
                    Common.ServiceStartType.Auto,
                    Common.ServiceAccount.LocalSystem,
                    null
                );
                MessageBox.Show("服务安装完成!");
            }
            catch (Exception e)
            {
                MessageBox.Show("请用管理员身份运行程序后再次安装!" + Environment.NewLine + e.Message);
            }
            //using (AssemblyInstaller installer = new AssemblyInstaller())
            //{
            //    installer.UseNewContext = true;
            //    installer.Path = serviceFilePath;
            //    string[] myString = new string[1];
            //    myString[0] = "/Service";
            //    installer.CommandLine = myString;

            //    IDictionary savedState = new Hashtable();
            //    installer.Install(savedState);
            //    installer.Commit(savedState);
            //}
        }

        //卸载服务
        private void UninstallService1(string serviceFilePath)
        {
            using (AssemblyInstaller installer = new AssemblyInstaller())
            {
                installer.UseNewContext = true;
                installer.Path = serviceFilePath;
                installer.Uninstall(null);
            }
        }
        //启动服务
        private void ServiceStart(string serviceName)
        {
            using (ServiceController control = new ServiceController(serviceName))
            {
                if (control.Status == ServiceControllerStatus.Stopped)
                {
                    control.Start();
                }
            }
        }

        //停止服务
        private void ServiceStop(string serviceName)
        {
            using (ServiceController control = new ServiceController(serviceName))
            {
                if (control.Status == ServiceControllerStatus.Running)
                {
                    control.Stop();
                }
            }
        }

        private void ToSAPDoc_Load(object sender, EventArgs e)
        {
            if (this.IsServiceExisted(serviceName)) this.ServiceStop(serviceName);
            string sqlCmd = " SELECT collation_name FROM sys.databases WHERE name='" +
                            Program.wmsConn.InitialCatalog + "'";
            string wmsColl = Common.Common.GetDataTable(Program.wmsConn.ConnectionString, sqlCmd).Rows[0][0].ToString();
            sqlCmd= " SELECT collation_name FROM sys.databases WHERE name='" +
                    Program.SAPConn.InitialCatalog + "'";
            string sapColl = Common.Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd).Rows[0][0].ToString();
            if (!wmsColl.Equals(sapColl))
            {
                MessageBox.Show("WMS数据库和SAP数据排序规则不一致,请重建WMS数据库并且排序规则和SAP数据一致!");
                return;
            }
        }

        private void ToSAPDoc_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.IsServiceExisted(serviceName)) this.ServiceStart(serviceName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string errMsg = String.Empty;
            Common.Common.BaseDocumnetOITM(false);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string errMsg = String.Empty;
            Common.Common.BaseDocumnetOCRD( false);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string errMsg = String.Empty;
            Common.Common.BaseDocumnetOBPL(false);
        }

        private void CenScrap_Click(object sender, EventArgs e)
        {
            string errMsg = String.Empty;
            Common.Common.CenScrap(false);
        }

        private void CenOIGN_Click(object sender, EventArgs e)
        {
            string errMsg = String.Empty;
            Common.Common.CenAddStock(false);
        }
    }
}

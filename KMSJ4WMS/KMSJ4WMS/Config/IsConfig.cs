﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMSJ4WMS.Config
{
    public partial class IsConfig : Form
    {
        public IsConfig()
        {
            InitializeComponent();
        }
        private void saveConfigIni()
        {
            var oFileInfo = new FileInfo(Application.StartupPath + "/WMSInterFace.ini");
            if (oFileInfo.Exists)
            {
                oFileInfo.Delete();
            }
            oFileInfo = null;
            JObject ToSAPCon = new JObject();
            ToSAPCon.Add(
                new JProperty("WMS",
                    new JObject(
                        new JProperty("WMSDBserver", WMSServer.Text.Trim()),
                        new JProperty("WMSDBuser", WMSDbUser.Text.Trim()),
                        new JProperty("WMSDBpwd", Common.Common.Encrypt(WMSDbPwd.Text.Trim())),
                        new JProperty("WMSDB", WMSDB.Text.Trim())
                    )
                )
            );
            ToSAPCon.Add(
                new JProperty("SAP",
                    new JObject(
                        new JProperty("SAPDBTYPE", SAPDbType.Text.Trim()),
                        new JProperty("SAPDBserver", SAPserver.Text.Trim()),
                        new JProperty("SAPDBuser", SAPDbUser.Text.Trim()),
                        new JProperty("SAPDBpwd", Common.Common.Encrypt(SAPDbPwd.Text.Trim())),
                        new JProperty("SAPLicensesever", SAPLic.Text.Trim()),
                        new JProperty("SAPDB", SAPDb.Text.Trim()),
                        new JProperty("SAPUSER", SAPUser.Text.Trim()),
                        new JProperty("SAPPWD", Common.Common.Encrypt(SAPpwd.Text.Trim())),
                        new JProperty("SAPSLD", SAPSLD.Text.Trim())
                    ))
            );

            var oFileWriter = new StreamWriter(Application.StartupPath + "/WMSInterFace.ini");
            oFileWriter.Write(ToSAPCon.ToString());
            oFileWriter.Flush();
            oFileWriter.Close();
            oFileWriter.Dispose();
        }

        private void IsClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void IsOK_Click(object sender, EventArgs e)
        {
            saveConfigIni();
           
            this.Close();
        }

        private void IsConfig_Load(object sender, EventArgs e)
        {
            Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
            this.Top = Top;
            this.Left = Left;
            var oFileInfo = new FileInfo(Application.StartupPath + "/WMSInterFace.ini");
            if (oFileInfo.Exists)
            {
                oFileInfo = null;
                var oFileReader = new StreamReader(Application.StartupPath + "/WMSInterFace.ini");
                var ToSAPstr = oFileReader.ReadToEnd();
                oFileReader.Close();
                oFileReader.Dispose();
                var ToSAPCon = JObject.Parse(ToSAPstr);

                WMSServer.Text = ((JObject)ToSAPCon["WMS"])["WMSDBserver"].ToString();
                WMSDbUser.Text = ((JObject)ToSAPCon["WMS"])["WMSDBuser"].ToString();
                WMSDbPwd.Text = Common.Common.Decrypt(((JObject)ToSAPCon["WMS"])["WMSDBpwd"].ToString());
                WMSDB.Text = ((JObject)ToSAPCon["WMS"])["WMSDB"].ToString();

                SAPLic.Text = ((JObject)ToSAPCon["SAP"])["SAPLicensesever"].ToString();
                SAPSLD.Text = ((JObject)ToSAPCon["SAP"])["SAPSLD"].ToString();
                SAPserver.Text = ((JObject)ToSAPCon["SAP"])["SAPDBserver"].ToString();
                SAPDbType.Text = ((JObject)ToSAPCon["SAP"])["SAPDBTYPE"].ToString();
                SAPDb.Text = ((JObject)ToSAPCon["SAP"])["SAPDB"].ToString();
                SAPDbUser.Text = ((JObject)ToSAPCon["SAP"])["SAPDBuser"].ToString();
                SAPDbPwd.Text = Common.Common.Decrypt(((JObject)ToSAPCon["SAP"])["SAPDBpwd"].ToString());

                SAPUser.Text = ((JObject)ToSAPCon["SAP"])["SAPUSER"].ToString();
                SAPpwd.Text = Common.Common.Decrypt(((JObject)ToSAPCon["SAP"])["SAPPWD"].ToString());
            }
        }

        private void SAPserver_Leave(object sender, EventArgs e)
        {
            WMSServer.Text = SAPserver.Text;
        }

        private void WMSDbPwd_Leave(object sender, EventArgs e)
        {
            WMSDbPwd.Text = SAPDbPwd.Text;
        }

        private void SAPDbUser_Leave(object sender, EventArgs e)
        {
            WMSDbUser.Text = SAPDbUser.Text;
        }

        private void SAPDbPwd_Leave(object sender, EventArgs e)
        {
            WMSDbPwd.Text = SAPDbPwd.Text;
        }
    }
}

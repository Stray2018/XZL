﻿namespace KMSJ4WMS.Config
{
    partial class IsConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.WMSDB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.WMSDbPwd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.WMSDbUser = new System.Windows.Forms.TextBox();
            this.WMSServer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SAPDbType = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.SAPSLD = new System.Windows.Forms.TextBox();
            this.SAPDb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SAPDbUser = new System.Windows.Forms.TextBox();
            this.SAPpwd = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SAPUser = new System.Windows.Forms.TextBox();
            this.SAPDbPwd = new System.Windows.Forms.TextBox();
            this.SAPLic = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SAPserver = new System.Windows.Forms.TextBox();
            this.IsOK = new System.Windows.Forms.Button();
            this.IsClose = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.WMSDB);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.WMSDbPwd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.WMSDbUser);
            this.groupBox1.Controls.Add(this.WMSServer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(31, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 88);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "WMS数据库配置";
            // 
            // WMSDB
            // 
            this.WMSDB.Location = new System.Drawing.Point(610, 32);
            this.WMSDB.Name = "WMSDB";
            this.WMSDB.Size = new System.Drawing.Size(68, 21);
            this.WMSDB.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(551, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "数据库";
            // 
            // WMSDbPwd
            // 
            this.WMSDbPwd.Enabled = false;
            this.WMSDbPwd.Location = new System.Drawing.Point(446, 32);
            this.WMSDbPwd.Name = "WMSDbPwd";
            this.WMSDbPwd.Size = new System.Drawing.Size(93, 21);
            this.WMSDbPwd.TabIndex = 7;
            this.WMSDbPwd.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "密码";
            // 
            // WMSDbUser
            // 
            this.WMSDbUser.Enabled = false;
            this.WMSDbUser.Location = new System.Drawing.Point(290, 32);
            this.WMSDbUser.Name = "WMSDbUser";
            this.WMSDbUser.Size = new System.Drawing.Size(83, 21);
            this.WMSDbUser.TabIndex = 5;
            // 
            // WMSServer
            // 
            this.WMSServer.Enabled = false;
            this.WMSServer.Location = new System.Drawing.Point(72, 32);
            this.WMSServer.Name = "WMSServer";
            this.WMSServer.Size = new System.Drawing.Size(135, 21);
            this.WMSServer.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "用户";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "数据地址";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SAPDbType);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.SAPSLD);
            this.groupBox2.Controls.Add(this.SAPDb);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.SAPDbUser);
            this.groupBox2.Controls.Add(this.SAPpwd);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.SAPUser);
            this.groupBox2.Controls.Add(this.SAPDbPwd);
            this.groupBox2.Controls.Add(this.SAPLic);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.SAPserver);
            this.groupBox2.Location = new System.Drawing.Point(31, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(700, 172);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SAP Business One 配置";
            // 
            // SAPDbType
            // 
            this.SAPDbType.FormattingEnabled = true;
            this.SAPDbType.Items.AddRange(new object[] {
            "SQL2008",
            "SQL2012",
            "SQL2014",
            "SQL2016",
            "SQL2017",
            "HANADB"});
            this.SAPDbType.Location = new System.Drawing.Point(298, 123);
            this.SAPDbType.Name = "SAPDbType";
            this.SAPDbType.Size = new System.Drawing.Size(85, 20);
            this.SAPDbType.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(227, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 14;
            this.label13.Text = "数据库类别";
            // 
            // SAPSLD
            // 
            this.SAPSLD.Location = new System.Drawing.Point(82, 118);
            this.SAPSLD.Name = "SAPSLD";
            this.SAPSLD.Size = new System.Drawing.Size(135, 21);
            this.SAPSLD.TabIndex = 13;
            // 
            // SAPDb
            // 
            this.SAPDb.Location = new System.Drawing.Point(610, 31);
            this.SAPDb.Name = "SAPDb";
            this.SAPDb.Size = new System.Drawing.Size(68, 21);
            this.SAPDb.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "SLD服务器";
            // 
            // SAPDbUser
            // 
            this.SAPDbUser.Location = new System.Drawing.Point(290, 31);
            this.SAPDbUser.Name = "SAPDbUser";
            this.SAPDbUser.Size = new System.Drawing.Size(83, 21);
            this.SAPDbUser.TabIndex = 13;
            this.SAPDbUser.Leave += new System.EventHandler(this.SAPDbUser_Leave);
            // 
            // SAPpwd
            // 
            this.SAPpwd.Location = new System.Drawing.Point(464, 75);
            this.SAPpwd.Name = "SAPpwd";
            this.SAPpwd.Size = new System.Drawing.Size(75, 21);
            this.SAPpwd.TabIndex = 15;
            this.SAPpwd.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(551, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "数据库";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(387, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "SAP用户密码";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 10;
            this.label8.Text = "数据地址";
            // 
            // SAPUser
            // 
            this.SAPUser.Location = new System.Drawing.Point(290, 75);
            this.SAPUser.Name = "SAPUser";
            this.SAPUser.Size = new System.Drawing.Size(83, 21);
            this.SAPUser.TabIndex = 13;
            // 
            // SAPDbPwd
            // 
            this.SAPDbPwd.Location = new System.Drawing.Point(464, 31);
            this.SAPDbPwd.Name = "SAPDbPwd";
            this.SAPDbPwd.Size = new System.Drawing.Size(75, 21);
            this.SAPDbPwd.TabIndex = 15;
            this.SAPDbPwd.UseSystemPasswordChar = true;
            this.SAPDbPwd.Leave += new System.EventHandler(this.SAPDbPwd_Leave);
            // 
            // SAPLic
            // 
            this.SAPLic.Location = new System.Drawing.Point(82, 75);
            this.SAPLic.Name = "SAPLic";
            this.SAPLic.Size = new System.Drawing.Size(135, 21);
            this.SAPLic.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(231, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "SAP用户";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(231, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "用户";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 10;
            this.label12.Text = "许可服务器";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(387, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "密码";
            // 
            // SAPserver
            // 
            this.SAPserver.Location = new System.Drawing.Point(83, 31);
            this.SAPserver.Name = "SAPserver";
            this.SAPserver.Size = new System.Drawing.Size(135, 21);
            this.SAPserver.TabIndex = 11;
            this.SAPserver.Leave += new System.EventHandler(this.SAPserver_Leave);
            // 
            // IsOK
            // 
            this.IsOK.Location = new System.Drawing.Point(61, 331);
            this.IsOK.Name = "IsOK";
            this.IsOK.Size = new System.Drawing.Size(75, 23);
            this.IsOK.TabIndex = 2;
            this.IsOK.Text = "保存";
            this.IsOK.UseVisualStyleBackColor = true;
            this.IsOK.Click += new System.EventHandler(this.IsOK_Click);
            // 
            // IsClose
            // 
            this.IsClose.Location = new System.Drawing.Point(264, 331);
            this.IsClose.Name = "IsClose";
            this.IsClose.Size = new System.Drawing.Size(75, 23);
            this.IsClose.TabIndex = 3;
            this.IsClose.Text = "关闭";
            this.IsClose.UseVisualStyleBackColor = true;
            this.IsClose.Click += new System.EventHandler(this.IsClose_Click);
            // 
            // IsConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 384);
            this.Controls.Add(this.IsClose);
            this.Controls.Add(this.IsOK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "IsConfig";
            this.Text = "数据库配置";
            this.Load += new System.EventHandler(this.IsConfig_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox WMSDB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox WMSDbPwd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox WMSDbUser;
        private System.Windows.Forms.TextBox WMSServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SAPSLD;
        private System.Windows.Forms.TextBox SAPDb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox SAPDbUser;
        private System.Windows.Forms.TextBox SAPpwd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox SAPUser;
        private System.Windows.Forms.TextBox SAPDbPwd;
        private System.Windows.Forms.TextBox SAPLic;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox SAPserver;
        private System.Windows.Forms.Button IsOK;
        private System.Windows.Forms.Button IsClose;
        private System.Windows.Forms.ComboBox SAPDbType;
        private System.Windows.Forms.Label label13;
    }
}
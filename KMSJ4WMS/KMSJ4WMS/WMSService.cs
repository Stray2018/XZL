﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace KMSJ4WMS
{
    partial class WMSService : ServiceBase
    {
        public WMSService()
        {
            InitializeComponent();
        }

        public static Timer CenterPur = new Timer();
        public static Timer CenterRePur = new Timer();
        public static Timer WMSToTOPDN = new Timer();
        public static Timer CenterToShp = new Timer();
        public static Timer shpToCenter = new Timer();
        public static Timer BaseDocumnetOITM = new Timer();
        public static Timer BaseDocumnetOCRD = new Timer();
        public static Timer BaseDocumnetOBPL = new Timer();
        public static Timer CenScrap = new Timer();
        public static Timer CenOIGN = new Timer();
        protected override void OnStart(string[] args)
        {
            Common.Common.GetwmsConnStr();
            CenterPur = new Timer(300000);
           CenterPur.Enabled = true;
           CenterPur.Elapsed += CenterPur_Elapsed;

             CenterRePur = new Timer(300000);
            CenterRePur.Enabled = true;
            CenterRePur.Elapsed += CenterRePur_Elapsed;

            WMSToTOPDN = new Timer(60000);
            WMSToTOPDN.Enabled = true;
            WMSToTOPDN.Elapsed += WMSToTOPDN_Elapsed;

            CenterToShp = new Timer(300000);
            CenterToShp.Enabled = true;
            CenterToShp.Elapsed += CenterToShp_Elapsed;

            shpToCenter = new Timer(300000);
            shpToCenter.Enabled = true;
            shpToCenter.Elapsed += ShpToCenter_Elapsed;
            BaseDocumnetOITM = new Timer(300000);
            BaseDocumnetOITM.Enabled = true;
            BaseDocumnetOITM.Elapsed += BaseDocumnetOITM_Elapsed;

            BaseDocumnetOCRD = new Timer(300000);
            BaseDocumnetOCRD.Enabled = true;
            BaseDocumnetOCRD.Elapsed += BaseDocumnetOCRD_Elapsed;

            BaseDocumnetOBPL = new Timer(300000);
            BaseDocumnetOBPL.Enabled = true;
            BaseDocumnetOBPL.Elapsed += BaseDocumnetOBPL_Elapsed;

            CenScrap = new Timer(30000);
            CenScrap.Enabled = true;
            CenScrap.Elapsed += CenScrap_Elapsed;

            CenOIGN = new Timer(30000);
            CenOIGN.Enabled = false;
            CenOIGN.Elapsed += CenOIGN_Elapsed;
        }

        private void CenOIGN_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.CenAddStock));
            t.Start(true);
        }

        private void CenScrap_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.CenScrap));
            t.Start(true);
        }

        private void BaseDocumnetOBPL_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.BaseDocumnetOBPL));
            t.Start(true);
        }

        private void BaseDocumnetOCRD_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.BaseDocumnetOCRD));
            t.Start(true);
        }

        private void BaseDocumnetOITM_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.BaseDocumnetOITM));
            t.Start(true);
        }

        private void ShpToCenter_Elapsed(object sender, ElapsedEventArgs e)
        { 
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.ShpToCenter));
           t.Start(true);
           
        }

        private void CenterToShp_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.CenterToShp));
            t.Start(true);
        }

        private void WMSToTOPDN_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.WMSToTOPDN));
            t.Start(true);
        }

        private void CenterRePur_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.CenterRePur));
            t.Start(true);
        }

        private void CenterPur_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Common.Common.CenterPur));
            t.Start(true);
        }

        protected override void OnStop()
        {
            CenterPur.Enabled = false;
            CenterRePur.Enabled = false;
            WMSToTOPDN.Enabled = false;
            CenterToShp.Enabled = false;
            shpToCenter.Enabled = false;
            BaseDocumnetOBPL.Enabled = false;
            BaseDocumnetOCRD.Enabled = false;
            BaseDocumnetOITM.Enabled = false;
            CenScrap.Enabled = false;
            CenOIGN.Enabled = false;

        }
    }
}

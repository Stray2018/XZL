﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows.Forms;
using KMSJ4WMS.Common;

namespace KMSJ4WMS
{
    static class Program
    {

        public static int ScreenWidth = Convert.ToInt32(Screen.PrimaryScreen.Bounds.Width* 0.8);
        public static int ScreenHeig = Convert.ToInt32(Screen.PrimaryScreen.Bounds.Height * 0.8);
        public static SqlConnectionStringBuilder wmsConn = new SqlConnectionStringBuilder();
        public static SqlConnectionStringBuilder SAPConn = new SqlConnectionStringBuilder();
        public static Common.Ouser oUser = new Ouser();
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] server)
        {
            if (server.Length > 0)
            {
                ServiceBase[] ServicesToRun = new ServiceBase[]{new WMSService() };
                ServiceBase.Run(ServicesToRun);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Logon());
            }
        }
    }
}

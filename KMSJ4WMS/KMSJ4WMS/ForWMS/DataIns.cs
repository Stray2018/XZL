﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMSJ4WMS.ForWMS
{
    public partial class DataIns : Form
    {
        public string DataType = String.Empty;
        public string SAPDoc = string.Empty;
        public string WMSAPI = String.Empty;
        public DataIns()
        {
            InitializeComponent();
        }

        private void IsClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataIns_Load(object sender, EventArgs e)
        {
            dataGridView1.ColumnHeadersVisible = true;
            dataGridView1.ColumnHeadersHeight = 40;
            //dataGridView1.Anchor = AnchorStyles.Right;
            
            GetData();
            if (DataType.Equals("CenOIGE"))
            {
                //this.dataGridView1.Columns.Add("BF", "报废报损");
                this.dataGridView1.Columns["BF"].HeaderText = "报废报损";
                this.dataGridView1.Columns["BF"].Width = 80;
            }
            // this.WindowState = FormWindowState.Maximized
           this.Width =Convert.ToInt32(Program.ScreenWidth * 0.9);
           this.Width = Convert.ToInt32(Program.ScreenHeig * 0.9);
           dataGridView1.AutoResizeColumns();
        }

        private void GetData()
        {
            string sqlCmd = String.Empty;
            try
            {
                if (DataType.Equals("IsPur"))  //采购订单接口
            {
                SAPDoc = "OPOR";
                WMSAPI = "IDX_PO_CG";
                sqlCmd =
                    "SELECT 'N' oCh, 'SJJKCG'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as SupplCode,T1.DocDate,T1.DocDueDate ,T1.CardCode,T1.CardName ,t1.Comments Remark,T1.BPLName,T1.DocEntry FROM dbo.OPOR T1 INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' " +
                    "AND Confirmed='Y' and (Isnull(T1.U_WMSDocNum,'')='' or NOT EXISTS(SELECT U0.SAPDocEntry FROM ImportWMSOK U0 WHERE U0.SAPDoc = '"+SAPDoc+"' AND   WMSAPI = '"+WMSAPI+"' AND T1.DocEntry = U0.SAPDocEntry))";
            }
            if (DataType.Equals("IsRet"))  //采购退货接口
            {
                SAPDoc = "OPRR";
                WMSAPI = "IDX_SO_CT";
                sqlCmd =
                    "SELECT 'N' oCh, 'SJJKCT'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as SupplCode,T1.DocDate,T1.DocDueDate ,T1.CardCode,T1.CardName ,t1.Comments Remark,T1.BPLName,T1.DocEntry FROM dbo.OPRR T1 INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' " +
                    "AND  Confirmed='Y' and (Isnull(T1.U_WMSDocNum,'')='' or NOT EXISTS(SELECT U0.SAPDocEntry FROM ImportWMSOK U0 WHERE U0.SAPDoc = '" + SAPDoc + "' AND   WMSAPI = '" + WMSAPI + "' AND T1.DocEntry = U0.SAPDocEntry))";
            }
            if (DataType.Equals("IsSal"))  //门店发货接口
            {
                SAPDoc = "DeliveBPL";
                WMSAPI = "IDX_SO_XS";
                sqlCmd =
                    "SELECT 'N' oCh,'SJJKXS'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as SupplCode,T1.U_DocDate DocDate,T1.U_DocDueDate DocDueDate ,T1.U_CardCode CardCode,T1.U_CardName CardName,t1.Remark,T1.U_BPLName BPLName,T1.DocEntry FROM [@DeliveBPL] T1 " +
                    "where status = 'P' and  (Isnull(T1.U_WMSDocNum,'')='' or NOT EXISTS(SELECT U0.SAPDocEntry FROM ImportWMSOK U0 WHERE U0.SAPDoc = '" + SAPDoc + "' AND   WMSAPI = '" + WMSAPI + "' AND T1.DocEntry = U0.SAPDocEntry))";
            }
            if (DataType.Equals("IsRet4Sal"))  //门店退货接口
            {
                SAPDoc = "ODRF";
                WMSAPI = "IDX_SO_XT";
                sqlCmd =
               "SELECT 'N' oCh, 'SJJKXT'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) SupplCode,T1.DocDate,T1.DocDueDate ,T1.CardCode,T1.CardName ,t1.Comments Remark,T1.BPLName,T1.DocEntry FROM dbo.ODRF T1 where T1.DocStatus='O' and U_IsOK='是' and T1.objType='21' " +
               "AND  (Isnull(T1.U_WMSDocNum,'')='' or  NOT  EXISTS(SELECT U0.SAPDocEntry FROM ImportWMSOK U0 WHERE U0.SAPDoc = '" + SAPDoc + "' AND   WMSAPI = '" + WMSAPI + "' AND T1.DocEntry = U0.SAPDocEntry))";
            }
            if (DataType.Equals("CenOIGE"))  //报废报损
            {
                SAPDoc = "ODRF";
                WMSAPI = "IDX_SO_BFBS";
                sqlCmd =
                    "SELECT 'N' oCh, case when T1.U_WMSType = '报废' then  'SJJKBF' else 'SJJKBS' end +right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) SupplCode,T1.DocDate,T1.DocDueDate ,T1.CardCode,T1.CardName ,t1.Comments Remark,T1.BPLName,T1.DocEntry,T1.U_WMSType as BF FROM dbo.ODRF T1 inner join OBPL T2 on t1.BPLId=t2.BPLId and t2.MainBPL='Y' where isnull(T1.U_WMSType,'')<>'' and T1.DocStatus='O'  and T1.objType='60' " +
                    "AND  (Isnull(T1.U_WMSDocNum,'')='' or  NOT EXISTS(SELECT U0.SAPDocEntry FROM ImportWMSOK U0 WHERE U0.SAPDoc = '" + SAPDoc + "' AND   WMSAPI = '" + WMSAPI + "' AND T1.DocEntry = U0.SAPDocEntry))";
            }

            if (DataType.Equals("CenOIGN"))  //报溢
            {
                SAPDoc = "ODRF";
                WMSAPI = "IDX_PO_BY";
                sqlCmd =
                    "SELECT 'N' oCh, 'SJJKBY'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) SupplCode,T1.DocDate,T1.DocDueDate ,T1.CardCode,T1.CardName ,t1.Comments Remark,T1.BPLName,T1.DocEntry FROM dbo.ODRF T1 inner join OBPL T2 on t1.BPLId=t2.BPLId and t2.MainBPL='Y' where T1.DocStatus='O'  and T1.objType='59' " +
                    "AND  (Isnull(T1.U_WMSDocNum,'')='' or  NOT EXISTS(SELECT U0.SAPDocEntry FROM ImportWMSOK U0 WHERE U0.SAPDoc = '" + SAPDoc + "' AND   WMSAPI = '" + WMSAPI + "' AND T1.DocEntry = U0.SAPDocEntry))";
            }
                DataTable oDataTable = Common.Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);

            dataGridView1.DataSource = oDataTable;
            dataGridView1.Columns["DocEntry"].Visible = false;
            dataGridView1.AutoSize = true;
            dataGridView1.AutoResizeColumns();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        private void IsOK_Click(object sender, EventArgs e)
        {
            string sqlCmd = String.Empty,UpDocNumsqlCmd = String.Empty,DeletExistsData = String.Empty;
            string oCHList = String.Empty,okValue = String.Empty;
            DataTable oDataTable = dataGridView1.DataSource as DataTable;
            int ChlRow = dataGridView1.SelectedRows.Count;
            if (ChlRow<1)
            {
                return;
            }
            foreach (DataRow oRow in oDataTable.Rows)
            {
                if (oRow["oCh"].ToString().Trim().Equals("Y"))
                {
                    oCHList += "," + oRow["DocEntry"].ToString().Trim();
                    
                    okValue += ",('"+SAPDoc+"','"+WMSAPI+"'," + oRow["DocEntry"].ToString().Trim()+")";
                }
            }

            oCHList =oCHList.Length>1? oCHList.Substring(1):"";
            okValue = okValue.Length > 1 ? okValue.Substring(1) : "";
            DataSet oDataSet = new DataSet();
            SqlConnection oSqlConn = new SqlConnection(Program.SAPConn.ConnectionString);
            SqlDataAdapter oSqlData = new SqlDataAdapter();
            oSqlConn.Open();
            if (DataType.Equals("IsPur"))  //采购订单接口
            {
                sqlCmd =
                    "SELECT '' Owner,'' as CustomerID,'CG' as POType,'SJJKCG'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as PONO," +
                    "convert(datetime,T1.DocDate,121) as CreationTime,convert(datetime,T1.DocDueDate,121) as ExpectArriveTime,T1.CardCode as SupplierID,T1.CardName as SupplierNAME,'' as ASNReference2," +
                    "T1.DocEntry AS ASNReference3,'' as ASNReference4,'' as ASNReference5,'' as H_EDI_01," +
                    "'' as H_EDI_02,'' as H_EDI_03,'' as H_EDI_04,'' as H_EDI_05,(SELECT TOP 1 WhsCode FROM dbo.POR1 WHERE DocEntry = t1.DocEntry) as Warehouse," +
                    "convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason ,'' Notes,'' USERDEFINE1, '' USERDEFINE2,'' USERDEFINE3,'' USERDEFINE4,'' USERDEFINE5 FROM dbo.OPOR T1 INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' " +
                    "AND T1.DocEntry in("+oCHList+") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_PO_Header");

                sqlCmd =
                    "SELECT 'SJJKCG'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as PONO,T2.LineNum as POLineNo,T2.ItemCode as SKU," +
                    "T2.InvQty as ExpectedQty,'' as Lotatt01,'' as Lotatt02,'' as Lotatt03,'' as Lotatt04,'' as Lotatt05,'' as Lotatt06,T0.U_AppNo as Lotatt07," +
                    "T0.U_Factory as Lotatt10,'' as Lotatt12,0 as Price,'' as D_EDI_01,'' as D_EDI_02,'' as D_EDI_03,'' as D_EDI_04,'' as D_EDI_05,'' as UserDefine1,'' as UserDefine2," +
                    "'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,convert(datetime,GetDate(),121) as CreateTime,NULL as InterfaceFlag,'' as InterfaceReason,null Amount " +
                    "FROM dbo.OPOR T1 INNER JOIN dbo.POR1 T2 ON T2.DocEntry = T1.DocEntry INNER JOIN dbo.OITM T0 ON T2.ItemCode = T0.ItemCode " +
                    "INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' " +
                    "AND T1.DocEntry in(" + oCHList + ") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_PO_Details");
                UpDocNumsqlCmd = "delete ImportWMSOK where SAPDoc= '" + SAPDoc + "' and  WMSAPI='" + WMSAPI + "' and  SAPDocEntry in(" + oCHList + ");Update OPOR set U_WMSDocNum = 'SJJKCG'+right('0000000000'+convert(NVARCHAR(100),DocEntry),10) where docentry in(" + oCHList + ");INSERT dbo.ImportWMSOK(SAPDoc, WMSAPI, SAPDocEntry)VALUES"+okValue;
                DeletExistsData =
                    " Delete IDX_PO_Details where PONO in (select PONO from IDX_PO_Header where POType='CG' and ASNReference3 in(" + oCHList+ ") );delete IDX_PO_Header where POType='CG' and ASNReference3 in(" + oCHList + ")";
            }
            if (DataType.Equals("IsRet"))  //采购退货接口
            {
                sqlCmd =
                    "SELECT '' AS Owner,'' as CustomerID,'CT' as SOType,'SJJKCT'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10)  as SONO,'3' as Priority," +
                    "'1' as DeliveryFlag,convert(datetime,T1.DocDate,121) as CreationTime,convert(datetime,t1.DocDate ,121) as ExpectShipmentTime,T1.CardCode as ConsigneeID,T1.CardName as ConsigneeName,T1.Address2 as ConsigneeAddress," +
                    "'' as ConsigneeTel,'' as ConsigneeZip,'' as ConsigneeContact,'' as SOReference2,T1.DocEntry as SOReference3,'' as SOReference4,'' as SOReference5,'' as UserDefine1," +
                    "'' as UserDefine2,'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,'' as Notes," +
                    "(SELECT TOP 1 WhsCode FROM dbo.PRR1 WHERE DocEntry = t1.DocEntry) as Warehouse,convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason " +
                    "FROM dbo.OPRR T1 INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' " +
                    "AND T1.DocEntry in(" + oCHList + ") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_SO_Header");

                sqlCmd =
                    "SELECT 'SJJKCT'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10)  AS  SONO,T2.LineNum as SOLineNo,T2.ItemCode as SKU,sum(t02.AllocQty) as ExpectedQty," +
                    "CONVERT(datetime, T03.MnfDate, 121) as Lotatt01,CONVERT(datetime, T03.ExpDate, 121) as Lotatt02,T03.DistNumber as Lotatt04,'' as Lotatt05,'X001' as Lotatt06," +
                    "T0.U_AppNo as Lotatt07,'' as Lotatt12,0 as Price,0 as Amount,'' as D_EDI_01,'' as D_EDI_02,'' as D_EDI_03,'' as D_EDI_04,'' as D_EDI_05,'' as UserDefine1," +
                    "'' as UserDefine2,'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason " +
                    "FROM dbo.OPRR T1 INNER JOIN dbo.PRR1 T2 ON T2.DocEntry = T1.DocEntry and T1.DocEntry in(" + oCHList + ")  INNER JOIN dbo.OITM T0 ON T2.ItemCode = T0.ItemCode " +
                    "INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' " +
                    "LEFT JOIN dbo.OITL T01 ON T01.DocType = T2.ObjType AND T01.DocEntry = T2.DocEntry AND T01.DocLine = T2.LineNum " +
                    "LEFT JOIN dbo.ITL1 T02 ON T01.LogEntry = T02.LogEntry AND T01.ManagedBy = '10000044' LEFT JOIN dbo.OBTN T03 ON T02.MdAbsEntry = T03.AbsEntry" +
                    " GROUP BY 'SJJKCT' + RIGHT('0000000000' + CONVERT(NVARCHAR(100), T1.DocEntry), 10) ,"+
                    " CONVERT(DATETIME, T03.MnfDate, 121) , CONVERT(DATETIME, T03.ExpDate, 121) , T2.LineNum , T2.ItemCode , T03.DistNumber , T0.U_AppNo"+
                   " HAVING SUM(T02.AllocQty) <> 0"+
                    " ORDER BY SONO,T2.LineNum";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_SO_Details");
                UpDocNumsqlCmd = "delete ImportWMSOK where SAPDoc= '" + SAPDoc + "' and  WMSAPI='" + WMSAPI + "' and  SAPDocEntry in(" + oCHList + ");Update OPRR set U_WMSDocNum = 'SJJKCT'+right('0000000000'+convert(NVARCHAR(100),DocEntry),10) where docentry in(" + oCHList + ");INSERT dbo.ImportWMSOK(SAPDoc, WMSAPI, SAPDocEntry)VALUES" + okValue;
                DeletExistsData =
                    "Delete IDX_SO_Details where SONO in (select SONO from IDX_SO_Header where SOType='CG' and SOReference3 in(" + oCHList + ") );delete IDX_SO_Header where SOType='CG' and SOReference3 in(" + oCHList + ")";
            }
            if (DataType.Equals("IsSal"))  //门店发货接口
            {
                sqlCmd =
                     "SELECT '' AS Owner,'' as CustomerID,'XS' as SOType,'SJJKXS'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10)  as SONO,'3' as Priority," +
                     "'1' as DeliveryFlag,convert(datetime,T1.U_DocDate,121) as CreationTime,convert(datetime,t1.U_DocDueDate,121) as ExpectShipmentTime,T1.U_CardCode as ConsigneeID,T1.U_CardName as ConsigneeName,T1.U_Address2 as ConsigneeAddress," +
                     "'' as ConsigneeTel,'' as ConsigneeZip,'' as ConsigneeContact,'' as SOReference2,T1.DocEntry as SOReference3,'' as SOReference4,'' as SOReference5,'' as UserDefine1," +
                     "'' as UserDefine2,'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,'' as Notes," +
                     "convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason " +
                     "FROM [@DeliveBPL] T1 where T1.DocEntry in(" + oCHList + ") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_SO_Header");

                sqlCmd =
                    "SELECT 'SJJKXS'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10)  AS  SONO,T2.LineId as SOLineNo,T2.U_ItemCode as SKU,T2.U_Quantity as ExpectedQty," +
                    "CONVERT(datetime, T2.U_MnfDate, 121) as Lotatt01,CONVERT(datetime, T2.U_ExpDate, 121) as Lotatt02,T2.U_DistNumber as Lotatt04,'' as Lotatt05,'X001' as Lotatt06," +
                    "T0.U_AppNo as Lotatt07,'' as Lotatt12,0 as Price,0 as Amount,'' as D_EDI_01,'' as D_EDI_02,0 as D_EDI_03,0 as D_EDI_04,'' as D_EDI_05,'' as UserDefine1," +
                    "'' as UserDefine2,'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason " +
                    "FROM [@DeliveBPL] T1 INNER JOIN [@LIVEBPL1] T2 ON T2.DocEntry = T1.DocEntry and T1.DocEntry in(" + oCHList + ")  INNER JOIN dbo.OITM T0 ON T2.U_ItemCode = T0.ItemCode ";
                    
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_SO_Details");
                UpDocNumsqlCmd = "delete ImportWMSOK where SAPDoc= '" + SAPDoc + "' and  WMSAPI='" + WMSAPI + "' and  SAPDocEntry in(" + oCHList + ");Update [@DeliveBPL] set U_WMSDocNum = 'SJJKXS'+right('0000000000'+convert(NVARCHAR(100),DocEntry),10) where docentry in(" + oCHList + ");INSERT dbo.ImportWMSOK(SAPDoc, WMSAPI, SAPDocEntry)VALUES" + okValue;
                DeletExistsData =
                    "Delete IDX_SO_Details where SONO in (select SONO from IDX_SO_Header where SOType='CG' and SOReference3 in(" + oCHList + ") );delete IDX_SO_Header where SOType='CG' and SOReference3 in(" + oCHList + ")";
            }
            if (DataType.Equals("IsRet4Sal"))  //门店退货接口
            {
                sqlCmd =
                    "SELECT '' Owner,'' as CustomerID,'XT' as POType,'SJJKXT'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as PONO," +
                    "convert(datetime,T1.DocDate,121) as CreationTime,convert(datetime,T1.DocDueDate,121) as ExpectArriveTime,T1.CardCode as SupplierID,T1.CardName as SupplierNAME,'' as ASNReference2," +
                    "T1.DocEntry as ASNReference3,'' as ASNReference4,'' as ASNReference5,'' as H_EDI_01," +
                    "'' as H_EDI_02,'' as H_EDI_03,'' as H_EDI_04,'' as H_EDI_05,(SELECT TOP 1 WhsCode FROM DRF1 WHERE DocEntry = t1.DocEntry) as Warehouse," +
                    "convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason " +
                    "FROM ODRF T1 where T1.objType='21' and T1.U_IsOK='是' and T1.DocEntry in(" + oCHList + ") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_PO_Header");

                sqlCmd =
                    "SELECT 'SJJKXT'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as PONO,T2.LineNum as POLineNo,T2.ItemCode as SKU," +
                    "T4.Quantity as ExpectedQty,convert(datetime,T5.MnfDate,121) as Lotatt01,convert(datetime,T5.ExpDate,121) as Lotatt02,convert(datetime,T5.InDate,121) as Lotatt03,t5.DistNumber as Lotatt04,'' as Lotatt05,'X001' as Lotatt06,T0.U_AppNo as Lotatt07," +
                    "T0.U_Factory as Lotatt10,'' as Lotatt12,CASE WHEN T0.EvalSystem='B' THEN T5.CostTotal/ CASE WHEN T5.Quantity=0 THEN 1 ELSE t5.Quantity end ELSE T02.StockValue / CASE WHEN T02.OnHand = 0 THEN 1 ELSE t02.OnHand END  END * 1 + (ISNULL(S02.Rate, 0) / 100) AS Price,  --计算门店采购含税价" +Environment.NewLine+
                    "'' as D_EDI_01,'' as D_EDI_02,'' as D_EDI_03,'' as D_EDI_04,'' as D_EDI_05,'' as UserDefine1,'' as UserDefine2," +
                    "'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,convert(datetime,GetDate(),121) as CreateTime,NULL as InterfaceFlag,'' as InterfaceReason " +
                    "FROM dbo.ODRF T1 INNER JOIN dbo.DRF1 T2 ON T2.DocEntry = T1.DocEntry and T1.objType='21' and T1.U_IsOK='是' and T1.DocEntry in(" + oCHList + ") " +
                    "INNER JOIN OITW T02 ON T2.ItemCode=T02.ItemCode AND T2.WhsCode=T02.WhsCode " +
                    "INNER JOIN dbo.OITM T0 ON T2.ItemCode = T0.ItemCode " +
                    "INNER JOIN dbo.DRF16 T4 ON T2.DocEntry = T4.AbsEntry AND T2.ItemCode = T4.ItemCode AND T2.LineNum = T4.LineNum " +
                    "LEFT JOIN dbo.OBTN T5 ON T4.ObjAbs = T5.AbsEntry AND T4.ObjId = '10000044' " +
                    "LEFT JOIN OVTG S01 ON T0.VatGroupPu=S01.Code AND S01.Inactive='N'  --配送中心采购税率 " +Environment.NewLine+
                    "LEFT JOIN OVTG S02 ON S02.Rate=CASE WHEN S01.Rate<>0 THEN 3 ELSE 0 END AND S02.Category='I' AND S02.Inactive='N' --门店采购税率：配送中心采购税率不为0时，门店税率为3%，否则门店税率为0";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_PO_Details");
                UpDocNumsqlCmd = "delete ImportWMSOK where SAPDoc= '" + SAPDoc + "' and  WMSAPI='" + WMSAPI + "' and  SAPDocEntry in(" + oCHList + ");Update [ODRF] set U_WMSDocNum = 'SJJKXT'+right('0000000000'+convert(NVARCHAR(100),DocEntry),10) where docentry in(" + oCHList + ");INSERT dbo.ImportWMSOK(SAPDoc, WMSAPI, SAPDocEntry)VALUES" + okValue;
                DeletExistsData =
                    "Delete IDX_PO_Details where PONO in (select PONO from IDX_PO_Header where POType='XT' and ASNReference3 in(" + oCHList + ") );delete IDX_PO_Header where POType='XT' and ASNReference3 in(" + oCHList + ")";
            }
            if (DataType.Equals("CenOIGE"))  //配送中心报废
            {
                sqlCmd =
                    "SELECT '' Owner,'' as CustomerID,case when T1.WMSType='报废' then 'BF' else 'BS' end as SOType,case when T1.U_WMSType='报废' then  'SJJKBF' else 'SJJKBS' end +right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as SONO,'3' as Priority," +
                    "'1' as DeliveryFlag,convert(datetime,T1.DocDate,121) as CreationTime,convert(datetime,t1.DocDate+2,121) as ExpectShipmentTime,'' as ConsigneeID,'' as ConsigneeName,'' as ConsigneeAddress," +
                    "'' as ConsigneeTel,'' as ConsigneeZip,'' as ConsigneeContact,'' as SOReference2,T1.DocEntry as SOReference3,'' as SOReference4,'' as SOReference5,'' as UserDefine1," +
                    "'' as UserDefine2,'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,'' as Notes," +
                    "convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason " +
                    "FROM ODRF T1 where T1.objType='60'  and T1.DocEntry in(" + oCHList + ") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_SO_Header");

                sqlCmd =
                    "SELECT case when T1.U_WMSType='报废' then  'SJJKBF' else 'SJJKBS' end +right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as SONO,T2.LineNum as SOLineNo,T2.ItemCode as SKU," +
                    "T4.Quantity as ExpectedQty,convert(datetime,T5.MnfDate,121) as Lotatt01,convert(datetime,T5.ExpDate,121) as Lotatt02,t5.DistNumber as Lotatt04,'' as Lotatt05,'P001' as Lotatt06,T0.U_AppNo as Lotatt07," +
                    "'' as Lotatt12,CASE WHEN T0.EvalSystem='B' THEN T5.CostTotal/ CASE WHEN T5.Quantity=0 THEN 1 ELSE t5.Quantity end ELSE T02.StockValue / CASE WHEN T02.OnHand = 0 THEN 1 ELSE t02.OnHand END  END * 1 + (ISNULL(S02.Rate, 0) / 100) AS Price,  --计算门店采购含税价" + Environment.NewLine +
                    "'' as D_EDI_01,'' as D_EDI_02,'' as D_EDI_03,'' as D_EDI_04,'' as D_EDI_05,T2.whsCode as UserDefine1,'' as UserDefine2," +
                    "'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,convert(datetime,GetDate(),121) as CreateTime,NULL as InterfaceFlag,'' as InterfaceReason " +
                    "FROM dbo.ODRF T1 INNER JOIN dbo.DRF1 T2 ON T2.DocEntry = T1.DocEntry and T1.objType='60' and T1.DocEntry in(" + oCHList + ") " +
                    "INNER JOIN OITW T02 ON T2.ItemCode=T02.ItemCode AND T2.WhsCode=T02.WhsCode " +
                    "INNER JOIN dbo.OITM T0 ON T2.ItemCode = T0.ItemCode " +
                    "INNER JOIN dbo.DRF16 T4 ON T2.DocEntry = T4.AbsEntry AND T2.ItemCode = T4.ItemCode AND T2.LineNum = T4.LineNum " +
                    "LEFT JOIN dbo.OBTN T5 ON T4.ObjAbs = T5.AbsEntry AND T4.ObjId = '10000044' " +
                    "LEFT JOIN OVTG S01 ON T0.VatGroupPu=S01.Code AND S01.Inactive='N'  --配送中心采购税率 " + Environment.NewLine +
                    "LEFT JOIN OVTG S02 ON S02.Rate=CASE WHEN S01.Rate<>0 THEN 3 ELSE 0 END AND S02.Category='I' AND S02.Inactive='N' --门店采购税率：配送中心采购税率不为0时，门店税率为3%，否则门店税率为0";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_SO_Details");
                UpDocNumsqlCmd = "delete ImportWMSOK where SAPDoc= '" + SAPDoc + "' and  WMSAPI='" + WMSAPI + "' and  SAPDocEntry in(" + oCHList + ");Update [ODRF] set U_WMSDocNum = 'SJJKBF'+right('0000000000'+convert(NVARCHAR(100),DocEntry),10) where docentry in(" + oCHList + ");INSERT dbo.ImportWMSOK(SAPDoc, WMSAPI, SAPDocEntry)VALUES" + okValue;
                DeletExistsData =
                    "Delete IDX_SO_Details where SONO in (select SONO from IDX_SO_Header where SOType='BF' and SOReference3 in(" + oCHList + ") );delete IDX_SO_Header where SOType='BF' and SOReference3 in(" + oCHList + ")";
            }
            if (DataType.Equals("CenOIGN"))  //配送中心报溢
            {
                sqlCmd =
                    "SELECT '' Owner,'' as CustomerID,'BY' as POType,'SJJKBY'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as PONO," +
                    "convert(datetime,T1.DocDate,121) as CreationTime,convert(datetime,T1.DocDueDate,121) as ExpectArriveTime,'' as SupplierID,'' as SupplierNAME,'' as ASNReference2," +
                    "T1.DocEntry AS ASNReference3,'' as ASNReference4,'' as ASNReference5,'' as H_EDI_01," +
                    "'' as H_EDI_02,'' as H_EDI_03,'' as H_EDI_04,'' as H_EDI_05,(SELECT TOP 1 WhsCode FROM dbo.DRF1 WHERE DocEntry = t1.DocEntry) as Warehouse," +
                    "convert(datetime,GetDate(),121) as CreateTime,null as InterfaceFlag,'' as InterfaceReason ,'' Notes,'' USERDEFINE1, '' USERDEFINE2,'' USERDEFINE3,'' USERDEFINE4,'' USERDEFINE5 FROM dbo.ODRF T1 INNER JOIN dbo.OBPL T3 ON T1.BPLId = T3.BPLId AND T3.MainBPL = 'Y' and T1.objType='59' " +
                    "AND T1.DocEntry in(" + oCHList + ") ";
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_PO_Header");

                sqlCmd =
                    "SELECT 'SJJKBY'+right('0000000000'+convert(NVARCHAR(100),T1.DocEntry),10) as PONO, T2.LineNum as POLineNo,T2.ItemCode as SKU," +
                    "T4.Quantity as ExpectedQty,convert(datetime,T5.MnfDate,121) as Lotatt01,convert(datetime,T5.ExpDate,121) as Lotatt02,t5.DistNumber as Lotatt04,'' as Lotatt05,'X001' as Lotatt06,T0.U_AppNo as Lotatt07," +
                    " T0.U_Factory as Lotatt10, '' as Lotatt12,T2.Price AS Price,  --计算门店采购含税价" + Environment.NewLine +
                    "'' as D_EDI_01,'' as D_EDI_02,'' as D_EDI_03,'' as D_EDI_04,'' as D_EDI_05,T2.whsCode as UserDefine1,'' as UserDefine2," +
                    "'' as UserDefine3,'' as UserDefine4,'' as UserDefine5,convert(datetime,GetDate(),121) as CreateTime,NULL as InterfaceFlag,'' as InterfaceReason " +
                    "FROM dbo.ODRF T1 INNER JOIN dbo.DRF1 T2 ON T2.DocEntry = T1.DocEntry and T1.objType='59' and T1.DocEntry in(" +
                    oCHList + ") " +
                    "INNER JOIN OITW T02 ON T2.ItemCode=T02.ItemCode AND T2.WhsCode=T02.WhsCode " +
                    "INNER JOIN dbo.OITM T0 ON T2.ItemCode = T0.ItemCode " +
                    "INNER JOIN dbo.DRF16 T4 ON T2.DocEntry = T4.AbsEntry AND T2.ItemCode = T4.ItemCode AND T2.LineNum = T4.LineNum " +
                    "LEFT JOIN dbo.ODBN T5 ON T4.ObjAbs = T5.AbsEntry AND T4.ObjId = '10000068' ";
                    
                oSqlData = new SqlDataAdapter(sqlCmd, oSqlConn);
                oSqlData.Fill(oDataSet, "IDX_PO_Details");
                UpDocNumsqlCmd = "delete ImportWMSOK where SAPDoc= '" + SAPDoc + "' and  WMSAPI='" + WMSAPI + "' and  SAPDocEntry in(" + oCHList + ");Update ODRF set U_WMSDocNum = 'SJJKBY'+right('0000000000'+convert(NVARCHAR(100),DocEntry),10) where docentry in(" + oCHList + ");INSERT dbo.ImportWMSOK(SAPDoc, WMSAPI, SAPDocEntry)VALUES" + okValue;
                DeletExistsData =
                    " Delete IDX_PO_Details where PONO in (select PONO from IDX_PO_Header where POType='BY' and ASNReference3 in(" + oCHList + ") );delete IDX_PO_Header where POType='BY' and ASNReference3 in(" + oCHList + ")";
            }
            oSqlConn.Close();
            oSqlConn.Dispose();
            oSqlData.Dispose();
           
            {
                
            }
            try
            {
                string ErrMsg = string.Empty;
               bool WriteError= Common.Common.WriteToWMS(oDataSet, UpDocNumsqlCmd,DeletExistsData,out ErrMsg);
               if (WriteError.Equals(false))
               {
                   MessageBox.Show("传入数据到WMS失败，错误：" + Environment.NewLine + ErrMsg);
               }

                GetData();
            }
            catch (Exception exception)
            {
                MessageBox.Show("传入数据到WMS失败，错误：" + Environment.NewLine + exception.Message);
            }
        }


        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dataGridView1.Rows[e.RowIndex].Cells["oCh"].Value.ToString().Equals("Y"))
                {
                    dataGridView1.Rows[e.RowIndex].Cells["oCh"].Value = "N";
                    dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                    dataGridView1.Rows[e.RowIndex].Selected = false;

                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells["oCh"].Value = "Y";
                    dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.DodgerBlue;
                    dataGridView1.Rows[e.RowIndex].Selected = true;
                }
            }
            catch (Exception exception)
            {
                
            }
            
            
        }

        private void dataGridView1_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count > 1)
            {
                foreach (DataGridViewRow oSelectedRow in dataGridView1.SelectedRows)
                {
                    if (oSelectedRow.Cells["oCh"].Value.ToString().Equals("Y"))
                    {
                        oSelectedRow.Cells["oCh"].Value = "N";
                        oSelectedRow.DefaultCellStyle.BackColor = Color.White;
                    }
                    else
                    {
                        oSelectedRow.Cells["oCh"].Value = "Y";
                        oSelectedRow.DefaultCellStyle.BackColor = Color.DodgerBlue;
                    }
                }
            }

            }
            catch (Exception exception)
            {
               
            }
        }

        private void DataIns_ResizeEnd(object sender, EventArgs e)
        {
            dataGridView1.AutoResizeColumns();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMSJ4WMS.ForWMS
{
    public partial class MainForm : Form
    {
        private int childFormNumber = 0;
        

        public MainForm()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "窗口 " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "文本文件(*.txt)|*.txt|所有文件(*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "文本文件(*.txt)|*.txt|所有文件(*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
            this.Close();
            GC.Collect();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }



        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
            GC.Collect();
        }

        private void ManUser_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("ManUser", this))
            {
                ForWMS.ManUser oManUser = new ManUser();
                oManUser.MdiParent = this;
                oManUser.Show();
                
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Width = Program.ScreenWidth;
            this.Height = Program.ScreenHeig;
            this.Left = Convert.ToInt32(Screen.PrimaryScreen.Bounds.Width * 0.1);
            if (Program.oUser.Supper)
            {
                this.ManUser.Enabled = true;
                this.ManUser.Visible = true;
                this.toSAPDocToolStripMenuItem.Visible = true;
            }
            else
            {
                this.ManUser.Enabled = false;
                this.ManUser.Visible = false;
                this.toSAPDocToolStripMenuItem.Visible = false;
            }

            if (Program.oUser.IsPur || Program.oUser.Supper)
            {
                this.IsPur.Enabled = true;
                this.IsPur.Visible = true;
            }
            else
            {
                this.IsPur.Enabled = false;
                this.IsPur.Visible = false;
            }
            if (Program.oUser.IsRet || Program.oUser.Supper)
            {
                this.IsRet.Enabled = true;
                this.IsRet.Visible = true;
            }
            else
            {
                this.IsRet.Enabled = false;
                this.IsRet.Visible = false;
            }
            if (Program.oUser.IsSAL || Program.oUser.Supper)
            {
                this.IsSal.Enabled = true;
                this.IsSal.Visible = true;
            }
            else
            {
                this.IsSal.Enabled = false;
                this.IsSal.Visible = false;
            }
            if (Program.oUser.IsRet4SAL || Program.oUser.Supper)
            {
                this.IsRet4Sal.Enabled = true;
                this.IsRet4Sal.Visible = true;
            }
            else
            {
                this.IsRet4Sal.Enabled = false;
                this.IsRet4Sal.Visible = false;
            }
            if (Program.oUser.IsCenOIGE || Program.oUser.Supper)
            {
                this.CenOIGE.Enabled = true;
                this.CenOIGE.Visible = true;
            }
            else
            {
                this.CenOIGE.Enabled = false;
                this.CenOIGE.Visible = false;
            }
            //if (Program.oUser.IsCenOIGN || Program.oUser.Supper)
            //{
            //    this.CenOIGN.Enabled = true;
            //    this.CenOIGN.Visible = true;
            //}
            //else
            //{
                this.CenOIGN.Enabled = false;
                this.CenOIGN.Visible = false;
            //}
        }

        private void IsPur_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("IsPur",this))
            {
                ForWMS.DataIns oDataIns = new DataIns();
                oDataIns.Text = "采购订单接口";
                oDataIns.Name = "IsPur";
               oDataIns.MdiParent = this;
                oDataIns.DataType = "IsPur";
                oDataIns.AutoSize = true;
                oDataIns.Show();
            }
           
        }

        private void IsRet_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("IsRet", this))
            {
                ForWMS.DataIns oDataIns = new DataIns();
                oDataIns.Text = "采购退货接口";
                oDataIns.Name = "IsRet";
               oDataIns.MdiParent = this;
                oDataIns.DataType = "IsRet";
                oDataIns.AutoSize = true;
                oDataIns.Show();
            }
        }

        private void IsSal_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("IsSal", this))
            {
                ForWMS.DataIns oDataIns = new DataIns();
                oDataIns.Text = "门店发货接口";
                oDataIns.Name = "IsSal";
                oDataIns.MdiParent = this;
                oDataIns.DataType = "IsSal";
                oDataIns.Show();
            }
        }

        private void IsRet4Sal_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("IsRet4Sal", this))
            {
                ForWMS.DataIns oDataIns = new DataIns();
                oDataIns.Text = "门店退货接口";
                oDataIns.Name = "IsRet4Sal";
                oDataIns.MdiParent = this;
                oDataIns.DataType = "IsRet4Sal";
                oDataIns.AutoSize = true;
                oDataIns.Show();
            }
        }

        private void toSAPDocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("ToSAPDoc", this))
            {
                ToSAPDoc oToSapDoc = new ToSAPDoc();
                oToSapDoc.Text = "服务安装及测试";
                oToSapDoc.Name = "ToSAPDoc";
                oToSapDoc.MdiParent = MainForm.ActiveForm;
                oToSapDoc.AutoSize = true;
                oToSapDoc.Show();
            }
        }



        private void CenOIGE_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("CenOIGE", this))
            {
                ForWMS.DataIns oDataIns = new DataIns();
                oDataIns.Text = "配送中心报废/报损接口";
                oDataIns.Name = "CenOIGE";
                oDataIns.MdiParent = this;
                oDataIns.DataType = "CenOIGE";
                oDataIns.AutoSize = true;
                oDataIns.Show();
            }
        }

        private void CenOIGN_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("CenOIGN", this))
            {
                ForWMS.DataIns oDataIns = new DataIns();
                oDataIns.Text = "配送中心报溢接口";
                oDataIns.Name = "CenOIGN";
                oDataIns.MdiParent = this;
                oDataIns.DataType = "CenOIGN";
                oDataIns.AutoSize = true;
                oDataIns.Show();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMSJ4WMS.ForWMS
{
    public partial class ManUser : Form
    {
        public static SqlDataAdapter oDataAdapter = new SqlDataAdapter();
        public ManUser()
        {
            InitializeComponent();
        }

        private void IsClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void IsOK_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommandBuilder cb = new SqlCommandBuilder(oDataAdapter);
                oDataAdapter.UpdateCommand = cb.GetUpdateCommand();
                DataTable oDataTable = dataGridView2.DataSource as DataTable;
                oDataAdapter.Update(oDataTable);
                oDataTable.AcceptChanges();
                oDataAdapter.Dispose();
                this.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private void ManUser_Load(object sender, EventArgs e)
        {
            
            //Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            //Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
            //this.Top = Top;
            //this.Left = Left;
            string sqlCmd = "select * from OUSR";
            oDataAdapter = new SqlDataAdapter(sqlCmd,Program.wmsConn.ConnectionString);
            DataTable oDS = new DataTable();
            oDataAdapter.Fill(oDS);
            oDS.Columns["Supper"].DefaultValue = false;
            oDS.Columns["Supper"].ReadOnly = true;
            oDS.Columns["IsPur"].DefaultValue = false;
            oDS.Columns["IsRet"].DefaultValue = false;
            oDS.Columns["IsSal"].DefaultValue = false;
            oDS.Columns["IsRet4Sal"].DefaultValue = false;
            oDS.Columns["IsCenOIGE"].DefaultValue = false;
            oDS.Columns["IsCenOIGN"].DefaultValue = false;
            dataGridView2.DataSource = oDS;
            dataGridView2.AutoGenerateColumns = true;
            dataGridView2.Columns["userID"].Visible = false;
            dataGridView2.Columns["UserCode"].HeaderText = "用户编码";
            dataGridView2.Columns["UserName"].HeaderText = "用户名称";
            dataGridView2.Columns["oPWD"].HeaderText = "密码";

            DataGridViewColumn oViewColumn = dataGridView2.Columns["Supper"];
            oViewColumn.HeaderText = "超级用户";

            dataGridView2.Columns["IsPur"].HeaderText = "采购订单接口";
            dataGridView2.Columns["IsRet"].HeaderText = "采购退货接口";
            dataGridView2.Columns["IsSal"].HeaderText = "门店发货接口";
            dataGridView2.Columns["IsRet4Sal"].HeaderText = "门店退货接口";//
            dataGridView2.Columns["IsCenOIGE"].HeaderText = "报废/报损接口";
            dataGridView2.Columns["IsCenOIGN"].HeaderText = "报溢接口";
            dataGridView2.Columns["IsCenOIGN"].Visible = false;

        }

        private int CurrRow = 0;

        private void toolDelRow_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Close();
            dataGridView2.Rows.RemoveAt(CurrRow);
        }

        private void dataGridView2_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            CurrRow = e.RowIndex;
            if ((e.Button & MouseButtons.Right) != 0)
            {
                contextMenuStrip1.Show(MousePosition);
            }
        }
    }
}

﻿namespace KMSJ4WMS.ForWMS
{
    partial class ManUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IsOK = new System.Windows.Forms.Button();
            this.IsClose = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolDelRow = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IsOK
            // 
            this.IsOK.Location = new System.Drawing.Point(240, 419);
            this.IsOK.Name = "IsOK";
            this.IsOK.Size = new System.Drawing.Size(75, 23);
            this.IsOK.TabIndex = 1;
            this.IsOK.Text = "保存";
            this.IsOK.UseVisualStyleBackColor = true;
            this.IsOK.Click += new System.EventHandler(this.IsOK_Click);
            // 
            // IsClose
            // 
            this.IsClose.Location = new System.Drawing.Point(407, 419);
            this.IsClose.Name = "IsClose";
            this.IsClose.Size = new System.Drawing.Size(75, 23);
            this.IsClose.TabIndex = 2;
            this.IsClose.Text = "关闭";
            this.IsClose.UseVisualStyleBackColor = true;
            this.IsClose.Click += new System.EventHandler(this.IsClose_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(12, 13);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(939, 368);
            this.dataGridView2.TabIndex = 3;
            this.dataGridView2.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView2_CellMouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolDelRow});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 26);
            // 
            // toolDelRow
            // 
            this.toolDelRow.Name = "toolDelRow";
            this.toolDelRow.Size = new System.Drawing.Size(112, 22);
            this.toolDelRow.Text = "删除行";
            this.toolDelRow.Click += new System.EventHandler(this.toolDelRow_Click);
            // 
            // ManUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 526);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.IsClose);
            this.Controls.Add(this.IsOK);
            this.Name = "ManUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManUser";
            this.Load += new System.EventHandler(this.ManUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button IsOK;
        private System.Windows.Forms.Button IsClose;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolDelRow;
    }
}
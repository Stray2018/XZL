﻿namespace KMSJ4WMS.ForWMS
{
    partial class DataIns
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.oCh = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SupplCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocDueDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CardCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CardName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BPLName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsOK = new System.Windows.Forms.Button();
            this.IsClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.oCh,
            this.SupplCode,
            this.DocDate,
            this.DocDueDate,
            this.CardCode,
            this.CardName,
            this.Remark,
            this.BPLName});
            this.dataGridView1.Location = new System.Drawing.Point(1, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(742, 346);
            this.dataGridView1.StandardTab = true;
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            this.dataGridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseUp);
            // 
            // oCh
            // 
            this.oCh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.oCh.DataPropertyName = "oCh";
            this.oCh.FalseValue = "N";
            this.oCh.HeaderText = "选择";
            this.oCh.IndeterminateValue = "N";
            this.oCh.MinimumWidth = 60;
            this.oCh.Name = "oCh";
            this.oCh.TrueValue = "Y";
            // 
            // SupplCode
            // 
            this.SupplCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplCode.DataPropertyName = "SupplCode";
            this.SupplCode.HeaderText = "单据号";
            this.SupplCode.MinimumWidth = 100;
            this.SupplCode.Name = "SupplCode";
            this.SupplCode.ReadOnly = true;
            this.SupplCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SupplCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DocDate
            // 
            this.DocDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DocDate.DataPropertyName = "DocDate";
            this.DocDate.HeaderText = "单据日期";
            this.DocDate.MinimumWidth = 80;
            this.DocDate.Name = "DocDate";
            this.DocDate.ReadOnly = true;
            this.DocDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DocDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DocDueDate
            // 
            this.DocDueDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DocDueDate.DataPropertyName = "DocDueDate";
            this.DocDueDate.HeaderText = "到期日期";
            this.DocDueDate.MinimumWidth = 80;
            this.DocDueDate.Name = "DocDueDate";
            this.DocDueDate.ReadOnly = true;
            this.DocDueDate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DocDueDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CardCode
            // 
            this.CardCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CardCode.DataPropertyName = "CardCode";
            this.CardCode.HeaderText = "单位编码";
            this.CardCode.MinimumWidth = 60;
            this.CardCode.Name = "CardCode";
            this.CardCode.ReadOnly = true;
            this.CardCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CardCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CardName
            // 
            this.CardName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CardName.DataPropertyName = "CardName";
            this.CardName.HeaderText = "单位名称";
            this.CardName.MinimumWidth = 120;
            this.CardName.Name = "CardName";
            this.CardName.ReadOnly = true;
            this.CardName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CardName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Remark
            // 
            this.Remark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remark.DataPropertyName = "Remark";
            this.Remark.HeaderText = "备注";
            this.Remark.MinimumWidth = 100;
            this.Remark.Name = "Remark";
            this.Remark.ReadOnly = true;
            this.Remark.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Remark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BPLName
            // 
            this.BPLName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BPLName.DataPropertyName = "BPLName";
            this.BPLName.HeaderText = "分支名称";
            this.BPLName.MinimumWidth = 120;
            this.BPLName.Name = "BPLName";
            this.BPLName.ReadOnly = true;
            this.BPLName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BPLName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IsOK
            // 
            this.IsOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.IsOK.Location = new System.Drawing.Point(194, 367);
            this.IsOK.Name = "IsOK";
            this.IsOK.Size = new System.Drawing.Size(75, 23);
            this.IsOK.TabIndex = 1;
            this.IsOK.Text = "传入WMS";
            this.IsOK.UseVisualStyleBackColor = true;
            this.IsOK.Click += new System.EventHandler(this.IsOK_Click);
            // 
            // IsClose
            // 
            this.IsClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.IsClose.Location = new System.Drawing.Point(368, 366);
            this.IsClose.Name = "IsClose";
            this.IsClose.Size = new System.Drawing.Size(75, 23);
            this.IsClose.TabIndex = 2;
            this.IsClose.Text = "关闭";
            this.IsClose.UseVisualStyleBackColor = true;
            this.IsClose.Click += new System.EventHandler(this.IsClose_Click);
            // 
            // DataIns
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(744, 414);
            this.Controls.Add(this.IsClose);
            this.Controls.Add(this.IsOK);
            this.Controls.Add(this.dataGridView1);
            this.Name = "DataIns";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DataIns";
            this.Load += new System.EventHandler(this.DataIns_Load);
            this.ResizeEnd += new System.EventHandler(this.DataIns_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button IsOK;
        private System.Windows.Forms.Button IsClose;
        private System.Windows.Forms.DataGridViewCheckBoxColumn oCh;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocDueDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CardCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CardName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.DataGridViewTextBoxColumn BPLName;
    }
}
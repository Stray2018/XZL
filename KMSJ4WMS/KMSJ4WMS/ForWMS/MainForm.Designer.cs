﻿namespace KMSJ4WMS.ForWMS
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ManUser = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toSAPDocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IsPur = new System.Windows.Forms.ToolStripMenuItem();
            this.IsRet = new System.Windows.Forms.ToolStripMenuItem();
            this.IsSal = new System.Windows.Forms.ToolStripMenuItem();
            this.IsRet4Sal = new System.Windows.Forms.ToolStripMenuItem();
            this.CenOIGE = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.CenOIGN = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.IsPur,
            this.IsRet,
            this.IsSal,
            this.IsRet4Sal,
            this.CenOIGE,
            this.CenOIGN});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1020, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.ManUser,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem,
            this.toSAPDocToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(58, 21);
            this.fileMenu.Text = "文件(&F)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(133, 6);
            // 
            // ManUser
            // 
            this.ManUser.Name = "ManUser";
            this.ManUser.Size = new System.Drawing.Size(136, 22);
            this.ManUser.Text = "用户管理";
            this.ManUser.Click += new System.EventHandler(this.ManUser_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(133, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.exitToolStripMenuItem.Text = "退出(&X)";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // toSAPDocToolStripMenuItem
            // 
            this.toSAPDocToolStripMenuItem.Name = "toSAPDocToolStripMenuItem";
            this.toSAPDocToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.toSAPDocToolStripMenuItem.Text = "ToSAPDoc";
            this.toSAPDocToolStripMenuItem.Click += new System.EventHandler(this.toSAPDocToolStripMenuItem_Click);
            // 
            // IsPur
            // 
            this.IsPur.Name = "IsPur";
            this.IsPur.Size = new System.Drawing.Size(92, 21);
            this.IsPur.Text = "采购订单接口";
            this.IsPur.Click += new System.EventHandler(this.IsPur_Click);
            // 
            // IsRet
            // 
            this.IsRet.Name = "IsRet";
            this.IsRet.Size = new System.Drawing.Size(92, 21);
            this.IsRet.Text = "采购退货接口";
            this.IsRet.Click += new System.EventHandler(this.IsRet_Click);
            // 
            // IsSal
            // 
            this.IsSal.Name = "IsSal";
            this.IsSal.Size = new System.Drawing.Size(92, 21);
            this.IsSal.Text = "门店发货接口";
            this.IsSal.Click += new System.EventHandler(this.IsSal_Click);
            // 
            // IsRet4Sal
            // 
            this.IsRet4Sal.Name = "IsRet4Sal";
            this.IsRet4Sal.Size = new System.Drawing.Size(92, 21);
            this.IsRet4Sal.Text = "门店退货接口";
            this.IsRet4Sal.Click += new System.EventHandler(this.IsRet4Sal_Click);
            // 
            // CenOIGE
            // 
            this.CenOIGE.Name = "CenOIGE";
            this.CenOIGE.Size = new System.Drawing.Size(92, 21);
            this.CenOIGE.Text = "配送中心报废";
            this.CenOIGE.Click += new System.EventHandler(this.CenOIGE_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 590);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1020, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.toolStripStatusLabel.Text = "状态";
            // 
            // CenOIGN
            // 
            this.CenOIGN.Name = "CenOIGN";
            this.CenOIGN.Size = new System.Drawing.Size(92, 21);
            this.CenOIGN.Text = "配送中心报溢";
            this.CenOIGN.Click += new System.EventHandler(this.CenOIGN_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 612);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WMS接口程序";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem ManUser;
        private System.Windows.Forms.ToolStripMenuItem IsPur;
        private System.Windows.Forms.ToolStripMenuItem IsRet;
        private System.Windows.Forms.ToolStripMenuItem IsSal;
        private System.Windows.Forms.ToolStripMenuItem IsRet4Sal;
        private System.Windows.Forms.ToolStripMenuItem toSAPDocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CenOIGE;
        private System.Windows.Forms.ToolStripMenuItem CenOIGN;
    }
}




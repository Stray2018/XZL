﻿namespace KMSJ4WMS
{
    partial class ToSAPDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToCenPur = new System.Windows.Forms.Button();
            this.ToCenRePur = new System.Windows.Forms.Button();
            this.TOSAPRePurReSale = new System.Windows.Forms.Button();
            this.ToSAPSalePur = new System.Windows.Forms.Button();
            this.InstallService = new System.Windows.Forms.Button();
            this.StartService = new System.Windows.Forms.Button();
            this.StopService = new System.Windows.Forms.Button();
            this.UninstallService = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.CenScrap = new System.Windows.Forms.Button();
            this.CenOIGN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ToCenPur
            // 
            this.ToCenPur.Location = new System.Drawing.Point(52, 26);
            this.ToCenPur.Name = "ToCenPur";
            this.ToCenPur.Size = new System.Drawing.Size(231, 37);
            this.ToCenPur.TabIndex = 0;
            this.ToCenPur.Text = "配送中心_采购收货";
            this.ToCenPur.UseVisualStyleBackColor = true;
            this.ToCenPur.Click += new System.EventHandler(this.ToCenPur_Click);
            // 
            // ToCenRePur
            // 
            this.ToCenRePur.Location = new System.Drawing.Point(334, 26);
            this.ToCenRePur.Name = "ToCenRePur";
            this.ToCenRePur.Size = new System.Drawing.Size(231, 37);
            this.ToCenRePur.TabIndex = 1;
            this.ToCenRePur.Text = "配送中心_采购退货";
            this.ToCenRePur.UseVisualStyleBackColor = true;
            this.ToCenRePur.Click += new System.EventHandler(this.ToCenRePur_Click);
            // 
            // TOSAPRePurReSale
            // 
            this.TOSAPRePurReSale.Location = new System.Drawing.Point(334, 181);
            this.TOSAPRePurReSale.Name = "TOSAPRePurReSale";
            this.TOSAPRePurReSale.Size = new System.Drawing.Size(231, 58);
            this.TOSAPRePurReSale.TabIndex = 3;
            this.TOSAPRePurReSale.Text = "门店退货_门店采购退货_配送中心销售退货";
            this.TOSAPRePurReSale.UseVisualStyleBackColor = true;
            this.TOSAPRePurReSale.Click += new System.EventHandler(this.TOSAPRePurReSale_Click);
            // 
            // ToSAPSalePur
            // 
            this.ToSAPSalePur.Location = new System.Drawing.Point(52, 181);
            this.ToSAPSalePur.Name = "ToSAPSalePur";
            this.ToSAPSalePur.Size = new System.Drawing.Size(231, 58);
            this.ToSAPSalePur.TabIndex = 2;
            this.ToSAPSalePur.Text = "门店收货_配送中心销售发货_门店采购收货";
            this.ToSAPSalePur.UseVisualStyleBackColor = true;
            this.ToSAPSalePur.Click += new System.EventHandler(this.ToSAPSalePur_Click);
            // 
            // InstallService
            // 
            this.InstallService.Location = new System.Drawing.Point(31, 316);
            this.InstallService.Name = "InstallService";
            this.InstallService.Size = new System.Drawing.Size(95, 27);
            this.InstallService.TabIndex = 4;
            this.InstallService.Text = "安装服务";
            this.InstallService.UseVisualStyleBackColor = true;
            this.InstallService.Click += new System.EventHandler(this.InstallService_Click);
            // 
            // StartService
            // 
            this.StartService.Location = new System.Drawing.Point(209, 316);
            this.StartService.Name = "StartService";
            this.StartService.Size = new System.Drawing.Size(95, 27);
            this.StartService.TabIndex = 5;
            this.StartService.Text = "启动服务";
            this.StartService.UseVisualStyleBackColor = true;
            this.StartService.Click += new System.EventHandler(this.StartService_Click);
            // 
            // StopService
            // 
            this.StopService.Location = new System.Drawing.Point(366, 316);
            this.StopService.Name = "StopService";
            this.StopService.Size = new System.Drawing.Size(95, 27);
            this.StopService.TabIndex = 6;
            this.StopService.Text = "停止服务";
            this.StopService.UseVisualStyleBackColor = true;
            this.StopService.Click += new System.EventHandler(this.StopService_Click);
            // 
            // UninstallService
            // 
            this.UninstallService.Location = new System.Drawing.Point(526, 316);
            this.UninstallService.Name = "UninstallService";
            this.UninstallService.Size = new System.Drawing.Size(95, 27);
            this.UninstallService.TabIndex = 7;
            this.UninstallService.Text = "卸载服务";
            this.UninstallService.UseVisualStyleBackColor = true;
            this.UninstallService.Click += new System.EventHandler(this.UninstallService_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 89);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(231, 58);
            this.button1.TabIndex = 8;
            this.button1.Text = "配送中心发货_生成_开发-采购收货";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(690, 40);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "商品档案";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(690, 78);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "供应商档案";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(690, 123);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "分支-客户档案";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // CenScrap
            // 
            this.CenScrap.Location = new System.Drawing.Point(334, 100);
            this.CenScrap.Name = "CenScrap";
            this.CenScrap.Size = new System.Drawing.Size(231, 37);
            this.CenScrap.TabIndex = 12;
            this.CenScrap.Text = "配送中心_报废";
            this.CenScrap.UseVisualStyleBackColor = true;
            this.CenScrap.Click += new System.EventHandler(this.CenScrap_Click);
            // 
            // CenOIGN
            // 
            this.CenOIGN.Location = new System.Drawing.Point(584, 192);
            this.CenOIGN.Name = "CenOIGN";
            this.CenOIGN.Size = new System.Drawing.Size(145, 37);
            this.CenOIGN.TabIndex = 13;
            this.CenOIGN.Text = "配送中心_报溢";
            this.CenOIGN.UseVisualStyleBackColor = true;
            this.CenOIGN.Visible = false;
            this.CenOIGN.Click += new System.EventHandler(this.CenOIGN_Click);
            // 
            // ToSAPDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 459);
            this.Controls.Add(this.CenOIGN);
            this.Controls.Add(this.CenScrap);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.UninstallService);
            this.Controls.Add(this.StopService);
            this.Controls.Add(this.StartService);
            this.Controls.Add(this.InstallService);
            this.Controls.Add(this.TOSAPRePurReSale);
            this.Controls.Add(this.ToSAPSalePur);
            this.Controls.Add(this.ToCenRePur);
            this.Controls.Add(this.ToCenPur);
            this.Name = "ToSAPDoc";
            this.Text = "ToSAPDoc";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ToSAPDoc_FormClosed);
            this.Load += new System.EventHandler(this.ToSAPDoc_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ToCenPur;
        private System.Windows.Forms.Button ToCenRePur;
        private System.Windows.Forms.Button TOSAPRePurReSale;
        private System.Windows.Forms.Button ToSAPSalePur;
        private System.Windows.Forms.Button InstallService;
        private System.Windows.Forms.Button StartService;
        private System.Windows.Forms.Button StopService;
        private System.Windows.Forms.Button UninstallService;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button CenScrap;
        private System.Windows.Forms.Button CenOIGN;
    }
}
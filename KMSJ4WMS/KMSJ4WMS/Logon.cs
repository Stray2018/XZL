﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KMSJ4WMS.Common;
using KMSJ4WMS.Config;
using KMSJ4WMS.ForWMS;
using Newtonsoft.Json.Linq;

namespace KMSJ4WMS
{
    public partial class Logon : Form
    {
        public Logon()
        {
            InitializeComponent();
        }

        private void IsSet_Click(object sender, EventArgs e)
        {
            IsConfig oIsConfig = new IsConfig();
            oIsConfig.Show(this);
        }

        private void IsClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Logon_Load(object sender, EventArgs e)
        {
            Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
            this.Top = Top;
            this.Left = Left;
        }

        private void IsOK_Click(object sender, EventArgs e)
        {
            LogonToSystem();
        }

        private void LogonToSystem()
        {
            
            Common.Common.GetwmsConnStr();
            string sqlCmd = "select * from OUSR where UserCode = '"+this.oUser.Text.Trim()+"' and oPWD = '"+this.oPWD.Text.Trim()+"'";
            DataTable oDs = Common.Common.GetDataTable(Program.wmsConn.ConnectionString, sqlCmd);
            if (oDs.Rows.Count > 0)
            {
                Program.oUser.UserCode = this.oUser.Text.Trim();
                Program.oUser.Supper = Convert.ToBoolean(oDs.Rows[0]["Supper"].ToString());
                Program.oUser.IsPur = Convert.ToBoolean(oDs.Rows[0]["IsPur"].ToString());
                Program.oUser.IsRet = Convert.ToBoolean(oDs.Rows[0]["IsRet"].ToString());
                Program.oUser.IsSAL = Convert.ToBoolean(oDs.Rows[0]["IsSal"].ToString());
                Program.oUser.IsCenOIGE = Convert.ToBoolean(oDs.Rows[0]["IsCenOIGE"].ToString());
                Program.oUser.IsRet4SAL = Convert.ToBoolean(oDs.Rows[0]["IsRet4Sal"].ToString());
                Program.oUser.IsCenOIGN = Convert.ToBoolean(oDs.Rows[0]["IsCenOIGN"].ToString());

                this.Hide();
                ForWMS.MainForm oMainForm = new MainForm();
                oMainForm.Show(this);
            }
            else
            {
                MessageBox.Show("登陆失败，请检查用户名和密码！");
            }
        }

        private void oPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                LogonToSystem();
            }
        }
    }
}

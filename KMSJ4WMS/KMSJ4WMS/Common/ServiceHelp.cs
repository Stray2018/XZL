﻿using System;
using System.ComponentModel;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace KMSJ4WMS.Common
{


    /// <summary>
    /// 服务不存在异常
    /// </summary>
    public class ServiceNotExistException : ApplicationException
    {
        public ServiceNotExistException()
            : base("服务不存在！")
        {
        }

        public ServiceNotExistException(string message)
            : base(message)
        {
        }
    }





    /// <summary>
    /// 服务启动类型
    /// </summary>
    public enum ServiceStartType
    {
        Boot,
        System,
        Auto,
        Manual,
        Disabled
    }

    /// <summary>
    /// 服务运行帐户
    /// </summary>
    public enum ServiceAccount
    {
        LocalSystem,
        LocalService,
        NetworkService
    }



    /// <summary>
    /// Windows 服务辅助类
    /// </summary>
    public static class ServiceHelper
    {


        /// <summary>
        /// Win32 API相关
        /// </summary>
        private static class Win32Class
        {


            /// <summary>
            /// 打开服务管理器时请求的权限：全部
            /// </summary>
            public const int SC_MANAGER_ALL_ACCESS = 0xF003F;

            /// <summary>
            /// 服务类型：自有进程类服务
            /// </summary>
            public const int SERVICE_WIN32_OWN_PROCESS = 0x10;

            /// <summary>
            /// 打开服务时请求的权限：全部
            /// </summary>
            public const int SERVICE_ALL_ACCESS = 0xF01FF;

            /// <summary>
            /// 打开服务时请求的权限：停止
            /// </summary>
            public const int SERVICE_STOP = 0x20;

            /// <summary>
            /// 服务操作标记：停止
            /// </summary>
            public const int SERVICE_CONTROL_STOP = 0x1;

            /// <summary>
            /// 服务出错行为标记
            /// </summary>
            public const int SERVICE_ERROR_NORMAL = 0x1;





            /// <summary>
            /// 服务状态结构体
            /// </summary>
            [StructLayout(LayoutKind.Sequential)]
            public struct SERVICE_STATUS
            {
                public readonly int serviceType;
                public readonly ServiceState currentState;
                public readonly int controlsAccepted;
                public readonly int win32ExitCode;
                public readonly int serviceSpecificExitCode;
                public readonly int checkPoint;
                public readonly int waitHint;
            }

            /// <summary>
            /// 服务描述结构体
            /// </summary>
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
            public struct SERVICE_DESCRIPTION
            {
                public IntPtr description;
            }

            /// <summary>
            /// 服务状态结构体。遍历API会用到
            /// </summary>
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
            public class ENUM_SERVICE_STATUS
            {
                public int checkPoint;
                public int controlsAccepted;
                public int currentState;
                public string displayName;
                public string serviceName;
                public int serviceSpecificExitCode;
                public int serviceType;
                public int waitHint;
                public int win32ExitCode;
            }





            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern bool ChangeServiceConfig2(IntPtr serviceHandle, uint infoLevel,
            ref SERVICE_DESCRIPTION serviceDesc);

            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern IntPtr OpenSCManager(string machineName, string databaseName, int dwDesiredAccess);

            [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            public static extern IntPtr OpenService(IntPtr hSCManager, string lpServiceName, int dwDesiredAccess);

            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern IntPtr CreateService(IntPtr hSCManager, string lpServiceName, string lpDisplayName,
                int dwDesiredAccess, int dwServiceType, ServiceStartType dwStartType, int dwErrorControl,
                string lpBinaryPathName, string lpLoadOrderGroup, IntPtr lpdwTagId, string lpDependencies,
                string lpServiceStartName, string lpPassword);

            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern bool CloseServiceHandle(IntPtr handle);

            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern bool QueryServiceStatus(IntPtr hService, ref SERVICE_STATUS lpServiceStatus);

            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern bool DeleteService(IntPtr serviceHandle);

            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern bool ControlService(IntPtr hService, int dwControl,
            ref SERVICE_STATUS lpServiceStatus);

            [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
            public static extern bool EnumDependentServices(IntPtr serviceHandle, EnumServiceState serviceState,
                IntPtr bufferOfENUM_SERVICE_STATUS, int bufSize, ref int bytesNeeded, ref int numEnumerated);
        }



        /// <summary>
        /// 服务状态枚举。用于遍历从属服务API
        /// </summary>
        private enum EnumServiceState
        {
            Active = 1
        }

        /// <summary>
        /// 服务状态
        /// </summary>
        private enum ServiceState
        {
            Stopped = 1,
            StopPending = 3
        }



        /// <summary>
        /// 安装服务
        /// </summary>
        /// <param name="serviceName">服务名</param>
        /// <param name="displayName">友好名称</param>
        /// <param name="binaryFilePath">映像文件路径，可带参数</param>
        /// <param name="description">服务描述</param>
        /// <param name="startType">启动类型</param>
        /// <param name="account">启动账户</param>
        /// <param name="dependencies">依赖服务</param>
        public static void Install(string serviceName, string displayName, string binaryFilePath, string description,
            ServiceStartType startType, ServiceAccount account = ServiceAccount.LocalSystem,
            string[] dependencies = null)
        {
            var scm = OpenSCManager();

            var service = IntPtr.Zero;
            try
            {
                service = Win32Class.CreateService(scm, serviceName, displayName, Win32Class.SERVICE_ALL_ACCESS,
                    Win32Class.SERVICE_WIN32_OWN_PROCESS, startType, Win32Class.SERVICE_ERROR_NORMAL, binaryFilePath,
                    null, IntPtr.Zero, ProcessDependencies(dependencies), GetServiceAccountName(account), null);

                if (service == IntPtr.Zero)
                {
                    if (Marshal.GetLastWin32Error() == 0x431)
                    {
                        throw new ApplicationException("服务已存在！");
                    }
                    throw new ApplicationException("服务安装失败！");
                }


                var sd = new Win32Class.SERVICE_DESCRIPTION();
                try
                {
                    sd.description = Marshal.StringToHGlobalUni(description);
                    Win32Class.ChangeServiceConfig2(service, 1, ref sd);
                }
                finally
                {
                    Marshal.FreeHGlobal(sd.description);
                }
            }
            finally
            {
                if (service != IntPtr.Zero)
                {
                    Win32Class.CloseServiceHandle(service);
                }
                Win32Class.CloseServiceHandle(scm);
            }
        }

        /// <summary>
        /// 卸载服务
        /// </summary>
        /// <param name="serviceName">服务名</param>
        public static void Uninstall(string serviceName)
        {
            var scmHandle = IntPtr.Zero;
            var service = IntPtr.Zero;

            try
            {
                service = OpenService(serviceName, out scmHandle);

                StopService(service);

                if (!Win32Class.DeleteService(service) && Marshal.GetLastWin32Error() != 0x430
                )
                {
                    throw new ApplicationException("删除服务失败！");
                }
            }
            catch (ServiceNotExistException)
            {
            }
            finally
            {
                if (service != IntPtr.Zero)
                {
                    Win32Class.CloseServiceHandle(service);
                    Win32Class.CloseServiceHandle(scmHandle);
                }
            }
        }





        /// <summary>
        /// 转换帐户枚举为有效参数
        /// </summary>
        private static string GetServiceAccountName(ServiceAccount account)
        {
            if (account == ServiceAccount.LocalService)
            {
                return @"NT AUTHORITY\LocalService";
            }
            if (account == ServiceAccount.NetworkService)
            {
                return @"NT AUTHORITY\NetworkService";
            }
            return null;
        }

        /// <summary>
        /// 处理依赖服务参数
        /// </summary>
        private static string ProcessDependencies(string[] dependencies)
        {
            if (dependencies == null || dependencies.Length == 0)
            {
                return null;
            }
            var sb = new StringBuilder();
            foreach (var s in dependencies)
            {
                sb.Append(s).Append('\0');
            }
            sb.Append('\0');

            return sb.ToString();
        }





        /// <summary>
        /// 打开服务管理器
        /// </summary>
        private static IntPtr OpenSCManager()
        {
            var scm = Win32Class.OpenSCManager(null, null, Win32Class.SC_MANAGER_ALL_ACCESS);

            if (scm == IntPtr.Zero)
            {
                throw new ApplicationException("打开服务管理器失败！");
            }
            return scm;
        }

        /// <summary>
        /// 打开服务
        /// </summary>
        /// <param name="serviceName">服务名称</param>
        /// <param name="scmHandle">服务管理器句柄。供调用者释放</param>
        private static IntPtr OpenService(string serviceName, out IntPtr scmHandle)
        {
            scmHandle = OpenSCManager();

            var service = Win32Class.OpenService(scmHandle, serviceName, Win32Class.SERVICE_ALL_ACCESS);

            if (service == IntPtr.Zero)
            {
                var errCode = Marshal.GetLastWin32Error();

                Win32Class.CloseServiceHandle(scmHandle);

                if (errCode == 0x424)
                {
                    throw new ServiceNotExistException();
                }
                throw new Win32Exception();
            }

            return service;
        }

        /// <summary>
        /// 停止服务
        /// </summary>
        private static void StopService(IntPtr service)
        {
            var currState = GetServiceStatus(service);

            if (currState == ServiceState.Stopped)
            {
                return;
            }
            if (currState != ServiceState.StopPending)
            {
                var childSvs = EnumDependentServices(service, EnumServiceState.Active);
                if (childSvs.Length != 0)
                {
                    var scm = OpenSCManager();
                    try
                    {
                        foreach (var childSv in childSvs)
                        {
                            StopService(Win32Class.OpenService(scm, childSv, Win32Class.SERVICE_STOP));
                        }
                    }
                    finally
                    {
                        Win32Class.CloseServiceHandle(scm);
                    }
                }

                var status = new Win32Class.SERVICE_STATUS();
                Win32Class.ControlService(service, Win32Class.SERVICE_CONTROL_STOP, ref status);
            }

            if (!WaitForStatus(service, ServiceState.Stopped, new TimeSpan(0, 0, 30)))
            {
                throw new ApplicationException("停止服务失败！");
            }
        }

        /// <summary>
        /// 遍历从属服务
        /// </summary>
        /// <param name="serviceHandle"></param>
        /// <param name="state">选择性遍历（活动、非活动、全部）</param>
        private static string[] EnumDependentServices(IntPtr serviceHandle, EnumServiceState state)
        {
            var bytesNeeded = 0;
            var numEnumerated = 0;


            if (Win32Class.EnumDependentServices(serviceHandle, state, IntPtr.Zero, 0, ref bytesNeeded,
                ref numEnumerated))
            {
                return new string[0];
            }
            if (Marshal.GetLastWin32Error() != 0xEA)
            {
                throw new Win32Exception();
            }
            var structsStart = Marshal.AllocHGlobal(new IntPtr(bytesNeeded));
            try
            {
                if (!Win32Class.EnumDependentServices(serviceHandle, state, structsStart, bytesNeeded, ref bytesNeeded,
                    ref numEnumerated))
                {
                    throw new Win32Exception();
                }
                var dependentServices = new string[numEnumerated];
                var sizeOfStruct = Marshal.SizeOf(typeof(Win32Class.ENUM_SERVICE_STATUS));
                var structsStartAsInt64 = structsStart.ToInt64();
                for (var i = 0; i < numEnumerated; i++)
                {
                    var structure = new Win32Class.ENUM_SERVICE_STATUS();
                    var ptr = new IntPtr(structsStartAsInt64 + i * sizeOfStruct);
                    Marshal.PtrToStructure(ptr, structure);
                    dependentServices[i] = structure.serviceName;
                }

                return dependentServices;
            }
            finally
            {
                Marshal.FreeHGlobal(structsStart);
            }
        }

        /// <summary>
        /// 获取服务状态
        /// </summary>
        private static ServiceState GetServiceStatus(IntPtr service)
        {
            var status = new Win32Class.SERVICE_STATUS();

            if (!Win32Class.QueryServiceStatus(service, ref status))
            {
                throw new ApplicationException("获取服务状态出错！");
            }
            return status.currentState;
        }

        /// <summary>
        /// 等候服务至目标状态
        /// </summary>
        private static bool WaitForStatus(IntPtr serviceHandle, ServiceState desiredStatus, TimeSpan timeout)
        {
            var startTime = DateTime.Now;

            while (GetServiceStatus(serviceHandle) != desiredStatus)
            {
                if (DateTime.Now - startTime > timeout)
                {
                    return false;
                }
                Thread.Sleep(200);
            }

            return true;
        }
    }
}

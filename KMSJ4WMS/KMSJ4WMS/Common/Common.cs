﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SAPbobsCOM;

namespace KMSJ4WMS.Common
{
    class Common
    {
        public static string Encrypt(string str)
        {
            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            byte[] key = Encoding.UTF8.GetBytes("Stray520");

            byte[] data = Encoding.UTF8.GetBytes(str);

            MemoryStream MStream = new MemoryStream();


            CryptoStream CStream = new CryptoStream(MStream, descsp.CreateEncryptor(key, key), CryptoStreamMode.Write);

            CStream.Write(data, 0, data.Length);

            CStream.FlushFinalBlock();

            return Convert.ToBase64String(MStream.ToArray());
        }  //加密字符串

        public static string Decrypt(string str)
        {
            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            byte[] key = Encoding.UTF8.GetBytes("Stray520");

            byte[] data = Convert.FromBase64String(str);

            MemoryStream MStream = new MemoryStream();


            CryptoStream CStream = new CryptoStream(MStream, descsp.CreateDecryptor(key, key), CryptoStreamMode.Write);

            CStream.Write(data, 0, data.Length);

            CStream.FlushFinalBlock();

            return Encoding.UTF8.GetString(MStream.ToArray());
        }  //解密字符串
        public static DataTable GetDataTable(string Connstr, string sqlcmd)
        {
            using (SqlConnection oConnection = new SqlConnection(Connstr))
            {
                oConnection.Open();
                DataTable oDataTable = new DataTable("MYdata");
                SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(sqlcmd, oConnection);
                oSqlDataAdapter.SelectCommand.CommandTimeout = 30000;
                oSqlDataAdapter.Fill(oDataTable);
                oSqlDataAdapter.Dispose();
                return oDataTable;
            }
        }  //获取数据DataTable
        public static DataSet GetDataDataSet(string Connstr, string sqlcmd)
        {
            using (SqlConnection oConnection = new SqlConnection(Connstr))
            {
                oConnection.Open();
                DataSet oDataTable = new DataSet();
                SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(sqlcmd, oConnection);
                oSqlDataAdapter.SelectCommand.CommandTimeout = 30000;
                oSqlDataAdapter.Fill(oDataTable);
                oSqlDataAdapter.Dispose();
                return oDataTable;
            }
        }  //获取数据DataSet
        public static bool ExecSqlcmd(string oSqlConnection, string Sqlcmd)
        {
            int Resutl = 0;
            try
            {
             Common.GetwmsConnStr();
            using (SqlConnection oConnection = new SqlConnection(oSqlConnection))
            {
                oConnection.Open();
                SqlCommand oSqlCommand = oConnection.CreateCommand();
                oSqlCommand.CommandText = Sqlcmd;
                Resutl= oSqlCommand.ExecuteNonQuery();
                oSqlCommand.Dispose();
            }

            if (!(Resutl>0))
            {
                return false;
            }
           
            return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }  //执行SQL命令
        public static void GetwmsConnStr()
        {
            try
            {


                FileInfo oFileInfo = new FileInfo(Application.StartupPath + "/WMSInterFace.ini");
                if (oFileInfo.Exists)
                {
                    oFileInfo = null;
                    StreamReader oStReader = new StreamReader(Application.StartupPath + "/WMSInterFace.ini");
                    string connStr = oStReader.ReadToEnd();
                    oStReader.Close();
                    oStReader.Dispose();
                    JObject ToSAPJson = JObject.Parse(connStr);
                    Program.wmsConn.DataSource = ((JObject) ToSAPJson["WMS"])["WMSDBserver"].ToString().Trim();
                    Program.wmsConn.UserID = ((JObject) ToSAPJson["WMS"])["WMSDBuser"].ToString().Trim();
                    Program.wmsConn.Password =
                        Common.Decrypt(((JObject) ToSAPJson["WMS"])["WMSDBpwd"].ToString().Trim());
                    Program.wmsConn.InitialCatalog = ((JObject) ToSAPJson["WMS"])["WMSDB"].ToString().Trim();
                    Program.wmsConn.ConnectTimeout = 3000;

                    Program.SAPConn.InitialCatalog = ((JObject) ToSAPJson["SAP"])["SAPDB"].ToString();
                    Program.SAPConn.DataSource = ((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString();

                    Program.SAPConn.UserID = ((JObject) ToSAPJson["SAP"])["SAPDBuser"].ToString();
                    Program.SAPConn.Password = Common.Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString());
                }
            }
            catch (Exception e)
            {

            }
        }  //获取WMS和SAP的SQL连接
        
        public static bool IsExistWindow(string formname, Form oForms)
        {
            foreach (Form mdiChild in oForms.MdiChildren)
            {
                if (mdiChild.Name.Equals(formname))
                {
                    mdiChild.Focus();
                    return true;
                }
            }
            return false;
        }  //判断窗口是否打开

        public static bool GetSAPConn(out string ConnErr, ref SAPbobsCOM.Company oCompany)
        {
            ConnErr = "";
            if (oCompany.Connected)
            {
                return true;
            }
            StreamReader oStReader = new StreamReader(Application.StartupPath + "/WMSInterFace.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            
            oCompany.Server= ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString();
            oCompany.CompanyDB = ((JObject)ToSAPJson["SAP"])["SAPDB"].ToString();
            oCompany.LicenseServer = ((JObject)ToSAPJson["SAP"])["SAPLicensesever"].ToString();
            oCompany.SLDServer = ((JObject)ToSAPJson["SAP"])["SAPSLD"].ToString();
            oCompany.language = BoSuppLangs.ln_Chinese;
            
            string DBtype= ((JObject)ToSAPJson["SAP"])["SAPDBTYPE"].ToString();
            switch (DBtype)   
            {
                case "SQL2008":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "SQL2012":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "SQL2014":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "SQL2016":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "SQL2017":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2017;
                    break;
                case "HANADB":
                    oCompany.DbServerType = BoDataServerTypes.dst_HANADB;
                    break;
            }

            oCompany.DbUserName = ((JObject)ToSAPJson["SAP"])["SAPDBuser"].ToString();
            oCompany.DbPassword = Common.Decrypt(((JObject)ToSAPJson["SAP"])["SAPDBpwd"].ToString());

            oCompany.UserName = ((JObject)ToSAPJson["SAP"])["SAPUSER"].ToString();
            oCompany.Password = Common.Decrypt(((JObject)ToSAPJson["SAP"])["SAPPWD"].ToString());
            if (oCompany.Connect()==0)
            {
                return true;
            }
            else
            {
                connStr = oCompany.GetLastErrorDescription();
                return false;
            }
            
        }  //获取SAP DI 连接

        public static void CreateCenterDoc(out String Error, ref SAPbobsCOM.Company oCompanyAdd, bool IsCenter,DataSet oDataSet, string wmsTable,string wmsDocNumField,string WmsDocTypeField, string WmsDocType,BoObjectTypes objectTypes,int baseType,string SAPDoc,string WMSAPI,bool IsService)
        {
            Error = string.Empty;
            SAPbobsCOM.Documents oDoc;
            string DocEntry=String.Empty;
           
            try
            {
                foreach (DataRow headRow in oDataSet.Tables["TableHead"].Rows)
                {
                    GetSAPConn(out Error, ref oCompanyAdd);
                     oDoc = oCompanyAdd.GetBusinessObject(objectTypes) as SAPbobsCOM.Documents;
                string DocNum = headRow["DocNum"].ToString().Trim();
                DocEntry = headRow["DocEntry"].ToString().Trim();
                    string CardCode = headRow["CardCode"].ToString().Trim();
                string DocDate = headRow["DocDate"].ToString().Trim();
                int BPLID = Int32.Parse(headRow["BPLId"].ToString().Trim());
                oDoc.CardCode = CardCode;
                oDoc.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd",System.Globalization.CultureInfo.CurrentCulture);
                oDoc.BPL_IDAssignedToInvoice = BPLID;
                DataRow[] detailRows = oDataSet.Tables["TableRow"].Select("DocNum = '" + DocNum + "'");
                foreach (DataRow oDetailRow in detailRows)
                {
                    oDoc.Lines.BaseEntry = int.Parse(oDetailRow["DocEntry"].ToString());
                    oDoc.Lines.BaseLine = int.Parse(oDetailRow["LineNum"].ToString());
                    oDoc.Lines.BaseType = baseType;  //采购订单 
                    oDoc.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                    DataRow[] detailbatchnumber = oDataSet.Tables["TableDetail"].Select("DocNum = '" + DocNum + "' and LineNum = '"+ oDoc.Lines.BaseLine.ToString() + "'");
                    foreach (DataRow lineRow in detailbatchnumber)
                    {

                        oDoc.Lines.BatchNumbers.Quantity = double.Parse(lineRow["Quantity"].ToString());
                        oDoc.Lines.BatchNumbers.BatchNumber = lineRow["DistNumber"].ToString();
                        oDoc.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(lineRow["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                        oDoc.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(lineRow["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                        oDoc.Lines.BatchNumbers.Add();
                       
                    }
                    oDoc.Lines.Add();
                    }
                oCompanyAdd.StartTransaction();
                oDoc.Add();
                
                int errCode = 0;
                string errMsg = String.Empty;
                oCompanyAdd.GetLastError(out errCode, out errMsg);
                if (!errCode.Equals(0))
                {
                    if (IsService)
                    {
                       string sqlcmd =
                            "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','" + SAPDoc + "',''," + oDoc.GetAsXML() + "," + errCode.ToString() + "," + errMsg + ",getdate())";
                        Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                        continue;
                        }
                    else
                    {
                        MessageBox.Show(errMsg);
                        continue;
                    }
                }

                    SAPbobsCOM.Recordset oRec = oCompanyAdd.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                oRec.Command.Name = "SBO_SP_TransactionNotification";
                oRec.Command.Parameters.Item(1).Value = oCompanyAdd.GetNewObjectType();
               
                oRec.Command.Parameters.Item(2).Value = "A";
                oRec.Command.Parameters.Item(3).Value = 1;
                oRec.Command.Parameters.Item(4).Value = "DocEntry";
                oRec.Command.Parameters.Item(5).Value = oCompanyAdd.GetNewObjectKey();
                oRec.Command.Execute();
                    string oReCErrorMsg = oRec.Fields.Item(1).Value.ToString().Trim();
                    string oReCErrorCode = oRec.Fields.Item(0).Value.ToString().Trim();
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);
                if (errCode.Equals(0) && oReCErrorCode.Equals("0"))
                {
                    string sqlcmd = string.Empty;
                    switch (WmsDocType)
                    {
                            case "CG":
                            case "XT":
                                sqlcmd =
                                    "UPDATE IDX_PO_TRANSACTION SET INTERFACEFLAG = 'Y' WHERE PONO = '"+DocNum+"'";
                                break;
                            case "CT":
                            case "XS":
                                sqlcmd =
                                    "UPDATE IDX_SO_TRANSACTION SET INTERFACEFLAG = 'Y' WHERE SONO = '" + DocNum + "'";
                                break;
                        }
                    bool ReSult2=Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                    if (ReSult2)
                    {
                       
                            oCompanyAdd.EndTransaction(BoWfTransOpt.wf_Commit);
                            
                        }
                    else
                    {
                        oCompanyAdd.EndTransaction(BoWfTransOpt.wf_RollBack);
                     }

                    }
                else
                {
                    string sqlcmd = "update " + wmsTable + " set InterfaceFlag = 'N',InterfaceReason = '" + errMsg + "' where  " + wmsDocNumField + " = '"+DocNum+"' and "+ WmsDocTypeField + "= '" + WmsDocType + "'";
                    Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                    if (IsService)
                    {
                         sqlcmd =
                            "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','"+ SAPDoc + "','',"+oDoc.GetAsXML()+","+ errCode.ToString() + "," + errMsg + ",getdate())";
                        Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    }
                    else
                    {
                        MessageBox.Show(errMsg);
                    }
                }

                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDoc);
                
            }
            }
            catch (Exception e)
            {
                if (IsService)
                {
                   string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','" + SAPDoc + "',''," +e.ToString() + "," + e.HResult.ToString() + "," + e.Message + ",getdate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
            }
           

        }  //生成配送中心单据

        public static void CreateOIGEDoc(out String Error, ref SAPbobsCOM.Company oCompanyAdd,DataSet oDataSet, bool IsService)
        {
            Error = string.Empty;
            SAPbobsCOM.Documents oDoc;
            string DocEntry = String.Empty;
            string DocNum = string.Empty;

            try
            {
                foreach (DataRow headRow in oDataSet.Tables["TableHead"].Rows)
                {
                    GetSAPConn(out Error, ref oCompanyAdd);
                    oDoc = oCompanyAdd.GetBusinessObject(BoObjectTypes.oInventoryGenExit) as SAPbobsCOM.Documents;
                    DocNum = headRow["DocNum"].ToString().Trim();
                    DocEntry = headRow["DocEntry"].ToString().Trim();
                   
                    string DocDate = headRow["DocDate"].ToString().Trim();
                    int BPLID = Int32.Parse(headRow["BPLId"].ToString().Trim());
                    oDoc.UserFields.Fields.Item("U_IsOK").Value = "是";
                    oDoc.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    oDoc.BPL_IDAssignedToInvoice = BPLID;
                    oDoc.UserFields.Fields.Item("U_WMSDocNum").Value = DocNum;
                    oDoc.Comments = "基于发货单草稿：" + DocNum;
                    DataRow[] detailRows = oDataSet.Tables["TableRow"].Select("DocNum = '" + DocNum + "'");
                    foreach (DataRow oDetailRow in detailRows)
                    {
                        oDoc.Lines.ItemCode = oDetailRow["ItemCode"].ToString();
                        oDoc.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                        oDoc.Lines.WarehouseCode= oDetailRow["whsCode"].ToString();
                        oDoc.Lines.LineTotal =double.Parse(oDetailRow["LineTotal"].ToString());
                        DataRow[] detailbatchnumber = oDataSet.Tables["TableDetail"].Select("DocNum = '" + DocNum + "' and LineNum = '" + oDetailRow["LineNum"].ToString() + "'");
                        foreach (DataRow lineRow in detailbatchnumber)
                        {

                            oDoc.Lines.BatchNumbers.Quantity = double.Parse(lineRow["Quantity"].ToString());
                            oDoc.Lines.BatchNumbers.BatchNumber = lineRow["DistNumber"].ToString();
                            //oDoc.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(lineRow["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            //oDoc.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(lineRow["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            oDoc.Lines.BatchNumbers.Add();

                        }
                        oDoc.Lines.Add();
                    }
                    oCompanyAdd.StartTransaction();
                    oDoc.Add();

                    int errCode = 0;
                    string errMsg = String.Empty;
                    oCompanyAdd.GetLastError(out errCode, out errMsg);
                    if (!errCode.Equals(0))
                    {
                        if (IsService)
                        {
                            string sqlcmd =
                                 "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','配送中心报废','"+ DocNum + "'," + oDoc.GetAsXML() + "," + errCode.ToString() + "," + errMsg + ",getdate())";
                            Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                            continue;
                        }
                        else
                        {
                            MessageBox.Show(errMsg);
                            continue;
                        }
                    }

                    SAPbobsCOM.Recordset oRec = oCompanyAdd.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    oRec.Command.Name = "SBO_SP_TransactionNotification";
                    oRec.Command.Parameters.Item(1).Value = oCompanyAdd.GetNewObjectType();

                    oRec.Command.Parameters.Item(2).Value = "A";
                    oRec.Command.Parameters.Item(3).Value = 1;
                    oRec.Command.Parameters.Item(4).Value = "DocEntry";
                    oRec.Command.Parameters.Item(5).Value = oCompanyAdd.GetNewObjectKey();
                    oRec.Command.Execute();
                    string oReCErrorMsg = oRec.Fields.Item(1).Value.ToString().Trim();
                    string oReCErrorCode = oRec.Fields.Item(0).Value.ToString().Trim();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);
                    if (errCode.Equals(0) && oReCErrorCode.Equals("0"))
                    {
                        string sqlcmd =
                            "UPDATE IDX_SO_TRANSACTION SET INTERFACEFLAG = 'Y' WHERE SONO = '" + DocNum + "'";
                        bool ReSult2 = Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                        if (ReSult2)
                        {

                            oCompanyAdd.EndTransaction(BoWfTransOpt.wf_Commit);

                        }
                        else
                        {
                            oCompanyAdd.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }

                    }
                    else
                    {
                        string sqlcmd = "update IDX_SO_TRANSACTION set InterfaceFlag = 'N',InterfaceReason = '" + errMsg + "' where  SONO = '" + DocNum + "' and Sotype = 'BF' ";
                        Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                        if (IsService)
                        {
                            sqlcmd =
                               "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','配送中心报废','"+DocNum+"'," + oDoc.GetAsXML() + "," + errCode.ToString() + "," + errMsg + ",getdate())";
                            Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                        }
                        else
                        {
                            MessageBox.Show(errMsg);
                        }
                    }

                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDoc);

                }
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                         "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','配送中心报废','"+DocNum+"'," + e.ToString() + "," + e.HResult.ToString() + "," + e.Message + ",getdate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
            }


        }  //生成配送中心单据--发货
        public static void CreateOIGNDoc(out String Error, ref SAPbobsCOM.Company oCompanyAdd, DataSet oDataSet, bool IsService) //生成配送中心单据--收货
        {
            Error = string.Empty;
            SAPbobsCOM.Documents oDoc;
            string DocEntry = String.Empty;
            string DocNum = string.Empty;

            try
            {
                foreach (DataRow headRow in oDataSet.Tables["TableHead"].Rows)
                {
                    GetSAPConn(out Error, ref oCompanyAdd);
                    oDoc = oCompanyAdd.GetBusinessObject(BoObjectTypes.oInventoryGenEntry) as SAPbobsCOM.Documents;
                    DocNum = headRow["DocNum"].ToString().Trim();
                    DocEntry = headRow["DocEntry"].ToString().Trim();

                    string DocDate = headRow["DocDate"].ToString().Trim();
                    int BPLID = Int32.Parse(headRow["BPLId"].ToString().Trim());
                    oDoc.UserFields.Fields.Item("U_IsOK").Value = "是";
                    oDoc.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    oDoc.BPL_IDAssignedToInvoice = BPLID;
                    oDoc.UserFields.Fields.Item("U_WMSDocNum").Value = DocNum;
                    oDoc.Comments = "基于收货单草稿：" + DocNum;
                    DataRow[] detailRows = oDataSet.Tables["TableRow"].Select("DocNum = '" + DocNum + "'");
                    foreach (DataRow oDetailRow in detailRows)
                    {
                        oDoc.Lines.ItemCode = oDetailRow["ItemCode"].ToString();
                        oDoc.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                        oDoc.Lines.WarehouseCode = oDetailRow["whsCode"].ToString();
                        oDoc.Lines.LineTotal = double.Parse(oDetailRow["LineTotal"].ToString());
                        DataRow[] detailbatchnumber = oDataSet.Tables["TableDetail"].Select("DocNum = '" + DocNum + "' and LineNum = '" + oDetailRow["LineNum"].ToString() + "'");
                        foreach (DataRow lineRow in detailbatchnumber)
                        {

                            oDoc.Lines.BatchNumbers.Quantity = double.Parse(lineRow["Quantity"].ToString());
                            oDoc.Lines.BatchNumbers.BatchNumber = lineRow["DistNumber"].ToString();
                            string MnfDate = lineRow["MnfDate"].ToString();
                            string ExpDate = lineRow["ExpDate"].ToString();
                            if (!MnfDate.Equals(""))
                            {
                                oDoc.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(MnfDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            }

                            if (!ExpDate.Equals(""))
                            {
                                oDoc.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(ExpDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            }
                            
                            oDoc.Lines.BatchNumbers.Add();

                        }
                        oDoc.Lines.Add();
                    }
                    oCompanyAdd.StartTransaction();
                    oDoc.Add();

                    int errCode = 0;
                    string errMsg = String.Empty;
                    oCompanyAdd.GetLastError(out errCode, out errMsg);
                    if (!errCode.Equals(0))
                    {
                        if (IsService)
                        {
                            string sqlcmd =
                                 "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','配送中心报溢','" + DocNum + "'," + oDoc.GetAsXML() + "," + errCode.ToString() + "," + errMsg + ",getdate())";
                            Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                            continue;
                        }
                        else
                        {
                            MessageBox.Show(errMsg);
                            continue;
                        }
                    }

                    SAPbobsCOM.Recordset oRec = oCompanyAdd.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    oRec.Command.Name = "SBO_SP_TransactionNotification";
                    oRec.Command.Parameters.Item(1).Value = oCompanyAdd.GetNewObjectType();

                    oRec.Command.Parameters.Item(2).Value = "A";
                    oRec.Command.Parameters.Item(3).Value = 1;
                    oRec.Command.Parameters.Item(4).Value = "DocEntry";
                    oRec.Command.Parameters.Item(5).Value = oCompanyAdd.GetNewObjectKey();
                    oRec.Command.Execute();
                    string oReCErrorMsg = oRec.Fields.Item(1).Value.ToString().Trim();
                    string oReCErrorCode = oRec.Fields.Item(0).Value.ToString().Trim();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);
                    if (errCode.Equals(0) && oReCErrorCode.Equals("0"))
                    {
                        string sqlcmd =
                            "UPDATE IDX_PO_TRANSACTION SET INTERFACEFLAG = 'Y' WHERE PONO = '" + DocNum + "'";
                        bool ReSult2 = Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                        if (ReSult2)
                        {

                            oCompanyAdd.EndTransaction(BoWfTransOpt.wf_Commit);

                        }
                        else
                        {
                            oCompanyAdd.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }

                    }
                    else
                    {
                        string sqlcmd = "update IDX_PO_TRANSACTION set InterfaceFlag = 'N',InterfaceReason = '" + errMsg + "' where  PONO = '" + DocNum + "' and Potype = 'BY' ";
                        Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                        if (IsService)
                        {
                            sqlcmd =
                               "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','配送中心报溢','" + DocNum + "'," + oDoc.GetAsXML() + "," + errCode.ToString() + "," + errMsg + ",getdate())";
                            Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                        }
                        else
                        {
                            MessageBox.Show(errMsg);
                        }
                    }

                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDoc);

                }
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                         "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','配送中心报溢','" + DocNum + "'," + e.ToString() + "," + e.HResult.ToString() + "," + e.Message + ",getdate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
            }


        }  

        public static bool CreateshpDocCG(out String Error, ref SAPbobsCOM.Company oCompany, DataSet oDataSet, string wmsTable, string wmsDocNumField, string WmsDocTypeField, string WmsDocType,bool IsService)
        {
            Error = string.Empty;
            string DocNum = String.Empty,PuBPLName = String.Empty,SaBPLName = String.Empty,DocEntry =string.Empty;
            SAPbobsCOM.Documents oDLN = oCompany.GetBusinessObject(BoObjectTypes.oDeliveryNotes) as SAPbobsCOM.Documents;
            SAPbobsCOM.Documents oPDN = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseDeliveryNotes) as SAPbobsCOM.Documents;
            try
            {
                foreach (DataRow headRow in oDataSet.Tables["TableHead"].Rows)
                {
                    int oDLnerrCode = 0, oPDNerrCode = 0;
                    string oDLNerrMsg = String.Empty, oPDNerrMsg = String.Empty;
                    string oReCErrorMsg_ODLN = String.Empty;
                    string oReCErrorCode_ODLN = String.Empty;
                    string oReCErrorMsg_OPDN = String.Empty;
                    string oReCErrorCode_OPDN = String.Empty;
              oDLN = oCompany.GetBusinessObject(BoObjectTypes.oDeliveryNotes) as SAPbobsCOM.Documents;
                     oPDN = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseDeliveryNotes) as SAPbobsCOM.Documents;
                    DocNum = headRow["DocNum"].ToString().Trim();
                    DocEntry = headRow["DocEntry"].ToString().Trim();
                    string DocDate = headRow["DocDate"].ToString().Trim();
                    PuBPLName= headRow["PuBPLId"].ToString().Trim();
                    SaBPLName= headRow["SaBPLId"].ToString().Trim();
                    oDLN.CardCode = headRow["SaCardCode"].ToString().Trim();
                    oDLN.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    oDLN.BPL_IDAssignedToInvoice = Int32.Parse(headRow["SaBPLId"].ToString().Trim());
                    oDLN.UserFields.Fields.Item("U_WMSDocNum").Value = DocNum;
                    oDLN.Comments = "基于门店发货计划：" + DocNum;

                    oPDN.CardCode = headRow["PuCardCode"].ToString().Trim();
                    oPDN.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    oPDN.BPL_IDAssignedToInvoice = Int32.Parse(headRow["PuBPLId"].ToString().Trim());
                    oPDN.UserFields.Fields.Item("U_WMSDocNum").Value = DocNum;
                    oPDN.Comments = "基于门店发货计划：" + DocNum;

                    DataRow[] detailRows=oDataSet.Tables["TableRow"].Select("DocNum = '" + DocNum + "'");
                    foreach (DataRow oDetailRow in detailRows)
                    {
                        string ItemCode = oDetailRow["ItemCode"].ToString(), BpuWhs = oDetailRow["BpuWhs"].ToString(), 
                            BsaWhs = oDetailRow["BsaWhs"].ToString(),Price = oDetailRow["Price"].ToString(),
                            PriceAfVat= oDetailRow["PriceAfVat"].ToString(), VatGroupSa= oDetailRow["VatGroupSa"].ToString(),
                            VatPrcnt= oDetailRow["VatPrcnt"].ToString(), VatGroupPu= oDetailRow["VatGroupPu"].ToString();
                        oDLN.Lines.ItemCode = ItemCode;
                        oDLN.Lines.WarehouseCode = oDetailRow["BsaWhs"].ToString();
                        oDLN.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                        oDLN.Lines.VatGroup = oDetailRow["VatGroupSa"].ToString();
                        oDLN.Lines.LineTotal = double.Parse(oDetailRow["SaLineTotal"].ToString());
                        oDLN.Lines.TaxTotal = double.Parse(oDetailRow["SaTaxTotal"].ToString());

                        oPDN.Lines.ItemCode = ItemCode;
                        oPDN.Lines.WarehouseCode = oDetailRow["BpuWhs"].ToString();
                        oPDN.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                        oPDN.Lines.VatGroup = oDetailRow["VatGroupPu"].ToString();
                        oPDN.Lines.LineTotal = double.Parse(oDetailRow["PuLineTotal"].ToString());
                        oPDN.Lines.TaxTotal = double.Parse(oDetailRow["PuTaxTotal"].ToString());

                        
                        DataRow[] detailBat = oDataSet.Tables["TableDetail"].Select("ItemCode = '" + ItemCode + "' and BpuWhs = '" + BpuWhs + "' and BsaWhs = '" + BsaWhs +
                            "' and Price = '" + Price + "' and PriceAfVat = '" + PriceAfVat + "' and VatGroupSa = '" + VatGroupSa +
                            "' and VatPrcnt = '" + VatPrcnt + "' and VatGroupPu = '" + VatGroupPu + "'");
                        foreach (DataRow odetailBat in detailBat)
                        {
                            oDLN.Lines.BatchNumbers.Quantity = double.Parse(odetailBat["Quantity"].ToString());
                            oDLN.Lines.BatchNumbers.BatchNumber = odetailBat["DistNumber"].ToString();
                            //oDLN.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(liodetailBatneRow["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            //oDLN.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(odetailBat["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            oDLN.Lines.BatchNumbers.Add();

                            oPDN.Lines.BatchNumbers.Quantity = double.Parse(odetailBat["Quantity"].ToString());
                            oPDN.Lines.BatchNumbers.BatchNumber = odetailBat["DistNumber"].ToString();
                            //oPDN.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(odetailBat["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            //oPDN.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(odetailBat["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            oPDN.Lines.BatchNumbers.Add();
                        }

                        oDLN.Lines.Add();
                        oPDN.Lines.Add();

                    }

                    oCompany.StartTransaction();
                    oDLN.Add();
                    oCompany.GetLastError(out oDLnerrCode, out oDLNerrMsg);
                    string odlnstring = oDLN.GetAsXML();
                    if (oDLnerrCode.Equals(0))
                    {
                        SAPbobsCOM.Recordset oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        oRec.Command.Name = "SBO_SP_TransactionNotification";
                        oRec.Command.Parameters.Item(1).Value = oCompany.GetNewObjectType();
                        oRec.Command.Parameters.Item(2).Value = "A";
                        oRec.Command.Parameters.Item(3).Value = 1;
                        oRec.Command.Parameters.Item(4).Value = "DocEntry";
                        oRec.Command.Parameters.Item(5).Value = oCompany.GetNewObjectKey();
                        oRec.Command.Execute();
                         oReCErrorMsg_ODLN = oRec.Fields.Item(1).Value.ToString().Trim();
                         oReCErrorCode_ODLN = oRec.Fields.Item(0).Value.ToString().Trim();
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);
                        oPDN.Add();
                        oCompany.GetLastError(out oPDNerrCode, out oPDNerrMsg);
                        if (oPDNerrCode.Equals(0))
                        {
                            string opdnEntry = oCompany.GetNewObjectKey();
                            oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            oRec.Command.Name = "SBO_SP_TransactionNotification";
                            oRec.Command.Parameters.Item(1).Value = oCompany.GetNewObjectType();
                            oRec.Command.Parameters.Item(2).Value = "A";
                            oRec.Command.Parameters.Item(3).Value = 1;
                            oRec.Command.Parameters.Item(4).Value = "DocEntry";
                            oRec.Command.Parameters.Item(5).Value = opdnEntry;
                            oRec.Command.Execute();
                            oReCErrorMsg_OPDN = oRec.Fields.Item(1).Value.ToString().Trim();
                            oReCErrorCode_OPDN = oRec.Fields.Item(0).Value.ToString().Trim();
                            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);

                            string sqlcmd = "Update [@TOPDN] set Status = 'T',U_Transid = "+ opdnEntry + " where DocEntry = " + DocEntry;
                            oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            oRec.DoQuery(sqlcmd);
                        }
                    }

                    if (oDLnerrCode.Equals(0) && oPDNerrCode.Equals(0) && oReCErrorCode_ODLN.Equals("0") &&
                        oReCErrorCode_OPDN.Equals("0"))
                    {
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    }
                    else
                    {
                        try
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }
                        catch (Exception e)
                        {
                           
                        }

                        string sqlcmd = "update " + wmsTable + " set InterfaceFlag = 'N',InterfaceReason = '" + oDLNerrMsg+ oPDNerrMsg + "' where  " + wmsDocNumField + " = '" + DocNum + "' and " + WmsDocTypeField + "= '" + WmsDocType + "'";
                        Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                        if (IsService)
                        {
                            if ((!oDLnerrCode.Equals(0)) || (!oReCErrorCode_ODLN.Equals("0")) )
                            {
                                 sqlcmd =
                                    "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES(" + SaBPLName + ",'销售交货'," + DocNum + ","+oDLN.GetAsXML()+"," + oDLnerrCode.ToString() + "," + oDLNerrMsg+Environment.NewLine+oReCErrorMsg_ODLN + ",getDate())";
                                Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                            }
                            if ((!oPDNerrCode.Equals(0)) || (!oReCErrorCode_OPDN.Equals("0")))
                            {
                                 sqlcmd =
                                    "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES(" + PuBPLName + ",'门店采购收货'," + DocNum + ","+oPDN.GetAsXML()+"," +oPDNerrCode.ToString() + "," + oPDNerrMsg+Environment.NewLine+oReCErrorMsg_OPDN + ",getDate())";
                                Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                            }
                        }
                        else
                        {
                            MessageBox.Show("WMS单号:" + DocNum + Environment.NewLine + "销售交货错误：" + oDLNerrMsg +
                                            Environment.NewLine+oReCErrorMsg_ODLN +"采购收货错误:"+ oPDNerrMsg+Environment.NewLine+oReCErrorMsg_OPDN,"生成单据失败");
                        }
                    }

                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDLN);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oPDN);
                }
            }
            catch (Exception e)
            {
                if (oCompany.InTransaction)
                {
                    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }

                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心发货给门店生成单据失败',''," + DocNum + ",'',0," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDLN);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oPDN);
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
                return false;
            }
            return true;

        }  //配送中心销售，门店采购
        public static bool CreateshpDocCT(out String Error, ref SAPbobsCOM.Company oCompany, DataSet oDataSet, string wmsTable, string wmsDocNumField, string WmsDocTypeField, string WmsDocType,bool IsService)
        {
            Error = string.Empty;
            string DocNum = String.Empty;
            SAPbobsCOM.Documents oDLN = oCompany.GetBusinessObject(BoObjectTypes.oReturns) as SAPbobsCOM.Documents;
            SAPbobsCOM.Documents oPDN = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseReturns) as SAPbobsCOM.Documents;
            try
            {
                foreach (DataRow headRow in oDataSet.Tables["TableHead"].Rows)
                {
                    int oDLnerrCode = 0, oPDNerrCode = 0;
                    string oDLNerrMsg = String.Empty, oPDNerrMsg = String.Empty,DocEntry = String.Empty;
                    string oReCErrorMsg_ODLN = String.Empty;
                    string oReCErrorCode_ODLN = String.Empty;
                    string oReCErrorMsg_OPDN = String.Empty;
                    string oReCErrorCode_OPDN = String.Empty;
                    oDLN = oCompany.GetBusinessObject(BoObjectTypes.oReturns) as SAPbobsCOM.Documents;
                     oPDN = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseReturns) as SAPbobsCOM.Documents;
                    DocNum = headRow["DocNum"].ToString().Trim();
                    DocEntry= headRow["DocEntry"].ToString().Trim();
                    string DocDate = headRow["DocDate"].ToString().Trim();


                    oDLN.CardCode = headRow["SaCardCode"].ToString().Trim();
                    oDLN.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd",
                        System.Globalization.CultureInfo.CurrentCulture);
                    oDLN.BPL_IDAssignedToInvoice = Int32.Parse(headRow["SaBPLId"].ToString().Trim());
                    oDLN.UserFields.Fields.Item("U_WMSDocNum").Value = DocNum;
                    oDLN.Comments = "基于采购退货草稿：" + DocNum;

                    oPDN.CardCode = headRow["PuCardCode"].ToString().Trim();
                    oPDN.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd",
                        System.Globalization.CultureInfo.CurrentCulture);
                    oPDN.BPL_IDAssignedToInvoice = Int32.Parse(headRow["PuBPLId"].ToString().Trim());
                    oPDN.UserFields.Fields.Item("U_WMSDocNum").Value = DocNum;
                    oPDN.Comments = "基于采购退货草稿：" + DocNum;

                    DataRow[] detailRows = oDataSet.Tables["TableRow"].Select("DocNum = '" + DocNum + "'");
                    foreach (DataRow oDetailRow in detailRows)
                    {
                        string ItemCode = oDetailRow["ItemCode"].ToString(),
                            PriceAfVat = oDetailRow["PriceAfVat"].ToString(),
                            VatGroupSa = oDetailRow["VatGroupSa"].ToString(),
                            VatGroupPu = oDetailRow["VatGroupPu"].ToString();
                        oDLN.Lines.ItemCode = ItemCode;
                        oDLN.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                        oDLN.Lines.VatGroup = oDetailRow["VatGroupSa"].ToString();
                        oDLN.Lines.PriceAfterVAT = double.Parse(PriceAfVat);


                        oPDN.Lines.ItemCode = ItemCode;
                        oPDN.Lines.Quantity = double.Parse(oDetailRow["Quantity"].ToString());
                        oPDN.Lines.VatGroup = oDetailRow["VatGroupPu"].ToString();
                        oPDN.Lines.PriceAfterVAT = double.Parse(PriceAfVat);



                        DataRow[] detailBat = oDataSet.Tables["TableDetail"]
                            .Select("ItemCode = '" + ItemCode + "' and PriceAfVat = '" + PriceAfVat + "'");
                        foreach (DataRow odetailBat in detailBat)
                        {
                            oDLN.Lines.BatchNumbers.Quantity = double.Parse(odetailBat["Quantity"].ToString());
                            oDLN.Lines.BatchNumbers.BatchNumber = odetailBat["DistNumber"].ToString();
                            //oDLN.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(liodetailBatneRow["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            //oDLN.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(odetailBat["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            oDLN.Lines.BatchNumbers.Add();

                            oPDN.Lines.BatchNumbers.Quantity = double.Parse(odetailBat["Quantity"].ToString());
                            oPDN.Lines.BatchNumbers.BatchNumber = odetailBat["DistNumber"].ToString();
                            //oPDN.Lines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(odetailBat["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            //oPDN.Lines.BatchNumbers.ExpiryDate = DateTime.ParseExact(odetailBat["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                            oPDN.Lines.BatchNumbers.Add();
                        }

                        oDLN.Lines.Add();
                        oPDN.Lines.Add();

                    }

                    oCompany.StartTransaction();
                    oDLN.Add();
                    oCompany.GetLastError(out oDLnerrCode, out oDLNerrMsg);
                    
                    if (oDLnerrCode.Equals(0))
                    {
                        SAPbobsCOM.Recordset oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        oRec.Command.Name = "SBO_SP_TransactionNotification";
                        oRec.Command.Parameters.Item(1).Value = oCompany.GetNewObjectType();
                        oRec.Command.Parameters.Item(2).Value = "A";
                        oRec.Command.Parameters.Item(3).Value = 1;
                        oRec.Command.Parameters.Item(4).Value = "DocEntry";
                        oRec.Command.Parameters.Item(5).Value = oCompany.GetNewObjectKey();
                        oRec.Command.Execute();
                        oReCErrorMsg_ODLN = oRec.Fields.Item(1).Value.ToString().Trim();
                        oReCErrorCode_ODLN = oRec.Fields.Item(0).Value.ToString().Trim();
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);
                        oPDN.Add();
                        oCompany.GetLastError(out oPDNerrCode, out oPDNerrMsg);
                        if (oPDNerrCode.Equals(0))
                        {
                            string opdnEntry = oCompany.GetNewObjectKey();
                            oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            oRec.Command.Name = "SBO_SP_TransactionNotification";
                            oRec.Command.Parameters.Item(1).Value = oCompany.GetNewObjectType();
                            oRec.Command.Parameters.Item(2).Value = "A";
                            oRec.Command.Parameters.Item(3).Value = 1;
                            oRec.Command.Parameters.Item(4).Value = "DocEntry";
                            oRec.Command.Parameters.Item(5).Value = opdnEntry;
                            oRec.Command.Execute();
                            oReCErrorMsg_OPDN = oRec.Fields.Item(1).Value.ToString().Trim();
                            oReCErrorCode_OPDN = oRec.Fields.Item(0).Value.ToString().Trim();
                            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);

                            string sqlcmd = "Update [ODRF] set DocStatus = 'C' where DocEntry = " + DocEntry;
                            oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            oRec.DoQuery(sqlcmd);
                        }

                        if (oDLnerrCode.Equals(0) && oPDNerrCode.Equals(0) && oReCErrorCode_ODLN.Equals("0") &&
                            oReCErrorCode_OPDN.Equals("0"))
                        {
                            string sqlcmd = "Update IDX_PO_TRANSACTION set INTERFACEFLAG = 'Y' where PONO='" + DocNum +
                                            "'";
                            bool result = Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                            if (result)
                            {
                                if (oCompany.InTransaction)
                                {
                                    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                                }
                            }
                            else
                            {
                                if (oCompany.InTransaction)
                                {
                                    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                                }
                            }
                        }
                        else
                        {
                            if (oCompany.InTransaction)
                            {
                                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }
                           string sqlcmd = "update " + wmsTable + " set InterfaceFlag = 'N',InterfaceReason = '" +
                                     oDLNerrMsg + oPDNerrMsg + "'where  " + wmsDocNumField + " = '" + DocNum +
                                     "' and " + WmsDocTypeField + "= '" + WmsDocType + "'";
                            Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);

                            if (IsService)
                            {
                                if (!oDLnerrCode.Equals(0))
                                {
                                    sqlcmd =
                                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES(" +"" + ",'销售退货'," + DocNum + "," + oDLN.GetAsXML() + "," +
                                        oDLnerrCode.ToString() + "," + oDLNerrMsg + ",getDate())";
                                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                                }

                                if (!oPDNerrCode.Equals(0))
                                {
                                    sqlcmd =
                                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES(" + "PuBPLName" + ",'门店采购退货'," + DocNum + "," + oPDN.GetAsXML() + "," +
                                        oPDNerrCode.ToString() + "," + oPDNerrMsg + ",getDate())";
                                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                                }
                            }
                            else
                            {
                                MessageBox.Show("WMS单号:" + DocNum + Environment.NewLine + "销售退货错误：" + oDLNerrMsg +
                                                Environment.NewLine + "采购退货错误:" + oPDNerrMsg, "生成单据失败");
                            }
                        }

                    }
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDLN);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oPDN);
                }
            }
            catch (Exception e)
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDLN);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oPDN);
                if (oCompany.InTransaction)
                {
                    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }

                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心发货给门店生成单据失败',''," + DocNum + ",'',0," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                }
                else
                {
                    MessageBox.Show(e.ToString());
                }
                
               
                return false;
            }
            return true;

        }  //门店采购退货，配送中心销售退货
        public static DataSet GetDocData(string wmsDb,string wmsDataTable,string wmsTypeField,string wmsDocType,string wmsDocNumField,string sapHeadTable,string sapLineTable,string SAPDoc,string WMSAPI,bool IsService)  //获取配送中心单据数据
        {
            DataSet oDataSet = new DataSet();
            string sqlCmd = String.Empty;
            try
            {
                Common.GetwmsConnStr();
               
                string Prex = String.Empty, LineNum = "LineNum";
                if (sapHeadTable.Contains("@"))
                {
                    Prex = "U_";
                    LineNum = "LineId";
                }

                string wmsLineField = "POLINENO", Quantity = "RECEIVEDQTY";

                if (wmsDataTable.Equals("IDX_SO_Transaction"))
                {
                    wmsLineField = "SOLineNo"; Quantity = "ShippedQty";
                }

                sqlCmd =
                    "SELECT T2.U_WMSDocNum DocNum,T2.DocEntry,T2." + Prex + "CardCode as CardCode,T2." + Prex + "CardName as CardName, CONVERT(NVARCHAR(10),T1.CREATETIME,112) DocDate,T2." + Prex + "BPLId as BPLId,T2." + Prex + "BPLName as BPLName FROM " + wmsDb + ".dbo." + wmsDataTable + " T1 " +
                    "INNER JOIN [" + sapHeadTable + "] T2 ON T1." + wmsDocNumField + " = T2.U_WMSDocNum AND T1." + wmsTypeField + "='" + wmsDocType + "' and isnull(T2.U_wmsDocnum,'')<>'' AND ISNULL(INTERFACEFLAG,'N')<>'Y'"+
                    "GROUP BY  T2.U_WMSDocNum,T2." + Prex + "CardCode,T2." + Prex + "CardName,T2.DocEntry,CONVERT(NVARCHAR(10),T1.CREATETIME,112) ,T2." + Prex + "BPLId,T2." + Prex + "BPLName";
                DataTable oDt = GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                if (oDt.Rows.Count > 0)
                {
                    oDt.TableName = "TableHead";
                    oDataSet.Tables.Add(oDt);
                    oDataSet.AcceptChanges();
                }
                else
                {
                    return oDataSet;
                }
                sqlCmd = "SELECT T2.U_WMSDocNum DocNum,T3.DocEntry,T3." + LineNum + " as LineNum,sum(T1." + Quantity + ") Quantity,T4.ItemCode,T4.ItemName,T4.U_Speic,T4.U_Factory,T4.U_BetDate,T4.InvntryUom,T4.VatGroupPu,T5.Rate/100 as Rate " +
                         " FROM " + wmsDb + ".dbo." + wmsDataTable + " T1 " +
                         " INNER JOIN [" + sapHeadTable + "] T2 ON T1." + wmsDocNumField + " = T2.U_WMSDocNum AND T1." + wmsTypeField + " = '" + wmsDocType + "' and isnull(T2.U_wmsDocnum,'')<>'' AND ISNULL(INTERFACEFLAG,'N')<>'Y'" +
                         " INNER JOIN [" + sapLineTable + "] T3 ON T2.DocEntry = T3.DocEntry AND T3." + LineNum + " = T1." + wmsLineField + " " +
                         " Inner Join OITM T4 on T1.SKU = T4.ItemCode inner join OVTG T5 on t4.VatGroupPu = T5.Code" +
                         " Group by T2.U_WMSDocNum ,T3.DocEntry,T3." + LineNum + ",T4.ItemCode,T4.ItemName,T4.U_Speic,T4.U_Factory,T4.U_BetDate,T4.InvntryUom,T4.VatGroupPu,T5.Rate "
                    ;
                oDt = GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableRow";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                sqlCmd = "SELECT T2.U_WMSDocNum DocNum,T3.DocEntry,T3." + LineNum + " as LineNum,T1." + Quantity + " Quantity,CONVERT(NVARCHAR(10),CONVERT(DATE,T1.Lotatt01),112) MnfDate," +
                         " CONVERT(NVARCHAR(10), CONVERT(DATE, T1.Lotatt02), 112) ExpDate,LEFT(T1.Lotatt04, 36) DistNumber ,T4.ItemCode,T4.ItemName,T4.U_Speic,T4.U_Factory,T4.U_BetDate,T4.InvntryUom,T4.VatGroupPu,T5.Rate/100 as Rate " +
                         " FROM " + wmsDb + ".dbo." + wmsDataTable + " T1 " +
                         " INNER JOIN [" + sapHeadTable + "] T2 ON T1." + wmsDocNumField + " = T2.U_WMSDocNum AND T1." + wmsTypeField + " = '" + wmsDocType + "' and isnull(T2.U_wmsDocnum,'')<>'' AND ISNULL(INTERFACEFLAG,'N')<>'Y'" +
                         " INNER JOIN [" + sapLineTable + "] T3 ON T2.DocEntry = T3.DocEntry AND T3." + LineNum + " = T1." + wmsLineField + " " +
                         " Inner Join OITM T4 on T1.SKU = T4.ItemCode inner join OVTG T5 on t4.VatGroupPu = T5.Code"
                    ;

                oDt = GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableDetail";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                return oDataSet;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('','" + SAPDoc + "','',''," + e.HResult + "," + e.Message + Environment.NewLine + sqlCmd + ")";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    
                }
                else
                {
                    MessageBox.Show(e.Message + Environment.NewLine + sqlCmd);
                }
                oDataSet=new DataSet();
                return oDataSet;
            }
            
        }

        public static bool WriteToWMS(DataSet oDataSet,string UpDocNumsqlCmd,string deleteExitDataCmd, out string exception)
        {
            exception = string.Empty;
            try
            {
                using (SqlConnection wmsConn = new SqlConnection(Program.wmsConn.ConnectionString))
                {
                    wmsConn.Open();
                    SqlTransaction oTransaction = wmsConn.BeginTransaction();
                    if (!deleteExitDataCmd.Equals(""))
                    {
                        SqlCommand deleCommand = wmsConn.CreateCommand();
                        deleCommand.Transaction = oTransaction;
                        deleCommand.CommandText = deleteExitDataCmd;
                        deleCommand.CommandTimeout = 300;
                        deleCommand.ExecuteNonQuery();
                        deleCommand.Dispose();
                    }
                    
                    foreach (DataTable oDt in oDataSet.Tables)
                    {
                        
                        string sqlCmd1 = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" + oDt.TableName + "'";
                        DataTable TableCol = Common.GetDataTable(Program.wmsConn.ConnectionString, sqlCmd1);
                        SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(wmsConn, SqlBulkCopyOptions.Default, oTransaction);
                        oSqlBulkCopy.DestinationTableName = oDt.TableName;
                        oSqlBulkCopy.ColumnMappings.Capacity = TableCol.Rows.Count;
                        oSqlBulkCopy.ColumnMappings.Clear();
                        foreach (DataColumn dataColumn in oDt.Columns)
                        {
                            string ColName = dataColumn.ColumnName;
                            string DbColName = TableCol.Select("COLUMN_NAME = '" + ColName + "'")[0][0].ToString();

                            oSqlBulkCopy.ColumnMappings.Add(ColName, DbColName);
                        }

                        oSqlBulkCopy.WriteToServer(oDt);
                    }

                    if (!UpDocNumsqlCmd.Equals(""))
                    {
                        using (SqlConnection sapConnection = new SqlConnection(Program.SAPConn.ConnectionString))
                        {
                            sapConnection.Open();
                            SqlCommand oSqlCommand = sapConnection.CreateCommand();
                            oSqlCommand.CommandText = UpDocNumsqlCmd;
                            oSqlCommand.CommandTimeout = 3000;
                            oSqlCommand.ExecuteNonQuery();
                            sapConnection.Close();
                        }
                    }

                    oTransaction.Commit();
                    wmsConn.Close();
                }

                return true;
            }
            catch (Exception e)
            {
                exception = e.Message;
                return false;
            }
        }  //写入数据到WMS中间表

        public static void BaseDocumnetOCRD(object IsServiceobj)
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            Common.GetwmsConnStr();
            string RunCmd = String.Empty;
            try
            {
                string sqlCmd = "SELECT '' AS  Owner,CardCode as CustomerID, 'Y' as Active_Flag, 'VE' as CustomerType, CardName as Descr_C, CardFName as Descr_E,LEFT(CardName,50) as ShortName," +
                                "U_PcCode as EasyCode,'' as ADDRESS1_C,'' as ADDRESS2_C,'' as ADDRESS3_C,'' as ADDRESS4_C,'' as COUNTRY,'' as PROVINCE,'' as CITY,'' as ZIP,'' as CONTACT1," +
                                "'' AS PHONE11,'' as PHONE12,'' as TITLE1,'' as FAX1,'' as EMAIL1,'' as CONTACT2,'' as PHONE21,'' as PHONE22,'' as TITLE2,'' as FAX2,'' as EMAIL2, " +
                                "'' AS CURRENCY,'' as UserDefine01,'' as UserDefine02,'' as UserDefine03,'' as UserDefine04,'' as UserDefine05,'' as ROUTE,'' as STOP,GETDATE() as CreateTime," +
                                "null as InterfaceFlag,'' as InterfaceReason ,'' as EDI_Code FROM dbo.OCRD WHERE CardType='S' AND dbo.OCRD.CardCode IN" +
                                "(SELECT KeyValue FROM dbo.WMS_BaseToWMS WHERE Status=0 AND KeyObject=2)";
                RunCmd = sqlCmd;
            DataSet oDataSet = new DataSet();
            oDataSet.Tables.Add(Common.GetDataTable(Program.SAPConn.ConnectionString,sqlCmd));
            if (oDataSet.Tables[0].Rows.Count < 1)
            {
                return ;
            }
                oDataSet.Tables[0].TableName = "IDX_CUSTOMER";
            oDataSet.AcceptChanges();
            string KeyValueList = String.Empty;
            foreach (DataRow oDataRow  in oDataSet.Tables[0].Rows)
            {
                KeyValueList +=",'"+ oDataRow["CustomerID"].ToString().Trim()+"'";
            }
            string sqlCmdUpdate = "Update WMS_BaseToWMS set status=1 where status=0 and keyobject=2 and keyValue in("+KeyValueList.Substring(1)+")";
            RunCmd = sqlCmdUpdate;
            string ErrMsg = String.Empty;
            bool WriteCus = WriteToWMS(oDataSet, sqlCmdUpdate,"", out ErrMsg);
            if (WriteCus.Equals(false) && IsService.Equals(false) )
            {
                MessageBox.Show(ErrMsg);
            }
            return ;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                   string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','供应商档案','',"+ RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    
                }
                else
                {
                    MessageBox.Show(e.ToString());
                    
                }

                return ;
            }
        }  //供应商
        public static void BaseDocumnetOITM(object IsServiceobj)
        {
         bool IsService = Boolean.Parse(IsServiceobj.ToString());
            string RunCmd = String.Empty;
            Common.GetwmsConnStr();
            try
            {
                string sqlCmd = "SELECT '' AS  Owner,'' As CustomerID,ItemCode As SKU,'Y' As ACTIVE_FLAG,ItemName As Descr_C,'' As Descr_E,null As Cube,null As GrossWeight,null As NetWeight,null As Tare," +
                                            "null As Length,null As Width,null As Height,null As CycleClass,null As FreightClass,U_Speic As Alternate_SKU1,ItemCode As Alternate_SKU2,U_PcName As Alternate_SKU3," +
                                            "InvntryUom As Alternate_SKU4,U_Factory AS Alternate_SKU5,null As ReservedField01,null As ReservedField02,U_StCon As ReservedField03,null As ReservedField04," +
                                            "CASE WHEN U_IsCold = '是' THEN 'Y'  else 'N' END AS ReservedField05,U_ConCat  As ReservedField06, NULL AS ReservedField07, NULL As ReservedField08, NULL As ReservedField09," +
                                            "NULL As ReservedField10, NULL As ReservedField11, NULL As ReservedField12,NULL  As ReservedField13, NULL As ReservedField14, NULL As ReservedField15, " +
                                            "NULL As ReservedField16, NULL As ReservedField17, NULL As ReservedField18,CASE WHEN U_IsFistSo = '是' THEN 'Y'  else 'N' END As FirstOP,U_InjeType As MedicalType, " +
                                            "U_AppNo AS ApprovalNo, CASE WHEN U_ConCat='3' THEN 'Y' ELSE 'N' end As SpecialMaintenance,U_IsRegDrugs As SerialNoCatch,'N' As MedicineSpecicalControl,null As UOMQty2," +
                                            "null As UOMQty3,null As UOMQty4,U_BetDate*30 As ShelfLife,GETDATE() As CreateTime,null As InterfaceFlag,'' As InterfaceReason " +
                                            "FROM dbo.OITM WHERE ItemCode IN(SELECT KeyValue FROM dbo.WMS_BaseToWMS WHERE Status=0 AND KeyObject=4)";

                DataSet oDataSet = new DataSet();
                RunCmd = sqlCmd;
                oDataSet.Tables.Add(Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd));
                if (oDataSet.Tables[0].Rows.Count<1)
                {
                    return ;
                }
                oDataSet.Tables[0].TableName = "IDX_SKU";
                oDataSet.AcceptChanges();
                string KeyValueList = String.Empty;
                foreach (DataRow oDataRow in oDataSet.Tables[0].Rows)
                {
                    KeyValueList += ",'" + oDataRow["SKU"].ToString().Trim()+"'";
                }
                string sqlCmdUpdate = "Update WMS_BaseToWMS set status=1 where status=0 and keyobject=4 and keyValue in(" + KeyValueList.Substring(1) + ")";
                RunCmd = sqlCmdUpdate;
                string ErrMsg = String.Empty;
                bool WriteCus = WriteToWMS(oDataSet, sqlCmdUpdate,"", out ErrMsg);
                if (WriteCus.Equals(false) && IsService.Equals(false))
                {
                    MessageBox.Show(ErrMsg);
                }
                return ;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','商品档案',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    
                }
                else
                {
                    MessageBox.Show(e.ToString());
                    
                }

                return ;
            }
            
        }  //商品档案
        public static void BaseDocumnetOBPL(object IsServiceobj)  //分支数据传入WMS为客户
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            string RunCmd = String.Empty;
            Common.GetwmsConnStr();
            try
            {
                string sqlCmd = "SELECT '' AS  Owner,DflCust as CustomerID, 'Y' as Active_Flag, 'CO' as CustomerType, BPLName as Descr_C, null as Descr_E,LEFT(BPLName,50) as ShortName," +
                                "null as EasyCode,'' as ADDRESS1_C,'' as ADDRESS2_C,'' as ADDRESS3_C,'' as ADDRESS4_C,'' as COUNTRY,'' as PROVINCE,'' as CITY,'' as ZIP,'' as CONTACT1," +
                                "'' AS PHONE11,'' as PHONE12,'' as TITLE1,'' as FAX1,'' as EMAIL1,'' as CONTACT2,'' as PHONE21,'' as PHONE22,'' as TITLE2,'' as FAX2,'' as EMAIL2, " +
                                "'' AS CURRENCY,'' as UserDefine01,'' as UserDefine02,'' as UserDefine03,'' as UserDefine04,'' as UserDefine05,'' as ROUTE,'' as STOP,GETDATE() as CreateTime," +
                                "null as InterfaceFlag,'' as InterfaceReason FROM dbo.OBPL WHERE Disabled='N' AND DflCust IN" +
                                "(SELECT KeyValue FROM dbo.WMS_BaseToWMS WHERE Status=0 AND KeyObject=247)";

                DataSet oDataSet = new DataSet();
                oDataSet.Tables.Add(Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd));
                if (oDataSet.Tables[0].Rows.Count < 1)
                {
                    return ;
                }
                oDataSet.Tables[0].TableName = "IDX_CUSTOMER";
                oDataSet.AcceptChanges();
                string KeyValueList = String.Empty;
                foreach (DataRow oDataRow in oDataSet.Tables[0].Rows)
                {
                    KeyValueList += ",'" + oDataRow["CustomerID"].ToString().Trim()+"'";
                }
                string sqlCmdUpdate = "Update WMS_BaseToWMS set status=1 where status=0 and keyobject=2 and keyValue in(" + KeyValueList.Substring(1) + ")";
                string ErrMsg = String.Empty;
                bool WriteCus = WriteToWMS(oDataSet, sqlCmdUpdate,"", out ErrMsg);
                if (WriteCus.Equals(false) && IsService.Equals(false))
                {
                    MessageBox.Show(ErrMsg);
                }
                return ;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','客户档案',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                   
                }
                else
                {
                    MessageBox.Show(e.ToString());
                   
                }

                return ;
            }
            
        }

        public static void CenterPur(object IsServiceobj)
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            string ConnErr = String.Empty;
            string RunCmd = String.Empty;
            Common.GetwmsConnStr();
            try
            {
             SAPbobsCOM.Company oCompany = new CompanyClass();
            bool companyIsConn = Common.GetSAPConn(out ConnErr, ref oCompany);
           
                if (!companyIsConn.Equals(true))
                {
                    if (IsService)
                    {
                        string sqlcmd =
                            "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','采购到货','','',0," + ConnErr + ",GetDate())";
                        RunCmd = sqlcmd;
                        Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    }
                    else
                    {
                        MessageBox.Show("SAP Company 连接失败，错误：" + ConnErr);
                    }
                  
                   
                }
               
                DataSet oDataSet = Common.GetDocData(Program.wmsConn.InitialCatalog, "IDX_PO_Transaction", "POType", "CG", "PONO", "OPOR", "POR1", "OPDN", "IDX_PO_CG", IsService);
                if (oDataSet.Tables.Count<1)
                {
                    return;
                }
                
                string errorMsg = String.Empty;
                Common.CreateCenterDoc(out errorMsg, ref oCompany, true, oDataSet, Program.wmsConn.InitialCatalog, "PONO", "POType", "CG", BoObjectTypes.oPurchaseDeliveryNotes, 22, "OPOR", "IDX_PO_Tran_CG", IsService);
                return ;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','采购收货',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    ConnErr = e.ToString();
                }
                else
                {
                    MessageBox.Show(e.ToString());
                    ConnErr = e.ToString();
                }

                return ;
            }
            
        }  //配送中心采购入库

        public static void CenterRePur(object IsServiceobj)
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            string ConnErr = String.Empty;
            string RunCmd = String.Empty;
            Common.GetwmsConnStr();
            try
            {
                SAPbobsCOM.Company oCompany = new CompanyClass();
                bool companyIsConn = Common.GetSAPConn(out ConnErr, ref oCompany);
                if (!companyIsConn.Equals(true))
                {
                    if (IsService)
                    {
                        string sqlcmd =
                            "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','采购退货','','',0," + ConnErr + ",GetDate())";
                        RunCmd = sqlcmd;
                        Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    }
                    else
                    {
                        MessageBox.Show("SAP Company 连接失败，错误：" + ConnErr);
                    }
                    
                    return ;
                }
                
                DataSet oDataSet = Common.GetDocData(Program.wmsConn.InitialCatalog, "IDX_SO_Transaction", "SOType", "CT", "SONO", "OPRR", "PRR1", "OPRR", "IDX_SO_CT", false);
                if (oDataSet.Tables.Count < 1)
                {
                    return;
                }
                string errorMsg = String.Empty;
                Common.CreateCenterDoc(out errorMsg, ref oCompany, true, oDataSet, Program.wmsConn.InitialCatalog, "SONO", "SOType", "CT", BoObjectTypes.oPurchaseReturns, 234000032, "OPRR", "IDX_PO_Tran_CT", false);
                return ;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','采购收货',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                    ConnErr = e.ToString();
                }
                else
                {
                    MessageBox.Show(e.ToString());
                    ConnErr = e.ToString();
                }

                return ;
            }
            
        }  //配送中心采购退货

        public static void WMSToTOPDN(object IsServiceobj)
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            string BPLName = String.Empty,DocNum = String.Empty;
            //Common.GetwmsConnStr();
            SAPbobsCOM.Company oCompany = new CompanyClass();
            try
            {
                Common.GetwmsConnStr();
                DataSet oDataSet = Common.GetDocData(Program.wmsConn.InitialCatalog, "IDX_SO_Transaction", "SOType", "XS", "SONO", "@DELIVEBPL", "@LIVEBPL1", "DELIVEBPL", "TOPDN", IsService);
                if (oDataSet.Tables.Count<1)
                {
                    return;
                }
                string sqlCmd = "SELECT T1.DflVendor,T1.BPLName, T1.DflWhs FROM dbo.OBPL T1 WHERE T1.MainBPL='Y'";
                DataTable oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                string CenVendorCode = oDt.Rows[0]["DflVendor"].ToString().Trim(),
                    CenVendorName = oDt.Rows[0]["BPLName"].ToString().Trim();
                string CenwhsCode = oDt.Rows[0]["DflWhs"].ToString().Trim();
                sqlCmd = "SELECT PriceDec ,SumDec FROM dbo.OADM";
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                int PriceDec = int.Parse(oDt.Rows[0]["PriceDec"].ToString());
                int SumDec = int.Parse(oDt.Rows[0]["SumDec"].ToString());
                string errMsg = String.Empty;
                
        
                foreach (DataRow oDataRow in oDataSet.Tables["TableHead"].Rows)
                {
                    Common.GetSAPConn(out errMsg, ref oCompany);
                    SAPbobsCOM.GeneralService oGeneralService;
                    SAPbobsCOM.GeneralData oGeneralData;
                    SAPbobsCOM.GeneralDataCollection oSons;
                    SAPbobsCOM.GeneralData oSon;
                    SAPbobsCOM.CompanyService sCmp;
                    sCmp = oCompany.GetCompanyService();

                    //Get a handle to the SM_MOR UDO
                    oGeneralService = sCmp.GetGeneralService("TOPDN");

                    //'Specify data for main UDO
                    oGeneralData =
                        oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData) as
                            SAPbobsCOM.GeneralData;
                    oGeneralData.SetProperty("U_CardCode", CenVendorCode);
                    oGeneralData.SetProperty("U_CardName", CenVendorName);
                    oGeneralData.SetProperty("U_BPLID", oDataRow["BPLId"].ToString());
                    oGeneralData.SetProperty("U_DocDate", DateTime.ParseExact(oDataRow["DocDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture));
                   // oGeneralData.SetProperty("Status", "I");
                   decimal Total = 0m, VatTotal = 0m;
                    DocNum = oDataRow["DocNum"].ToString().Trim();
                    oGeneralData.SetProperty("U_WmsDocNum", DocNum);
                    BPLName = oDataRow["BPLName"].ToString();
                    
                    sqlCmd = "SELECT T1.DflWhs FROM dbo.OBPL T1 WHERE	T1. BPLId=" +
                                      oDataRow["BPLId"].ToString();
                    oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                    string whsCode = oDt.Rows[0][0].ToString();
                    sqlCmd = "SELECT Code FROM dbo.OVTG WHERE Rate=3 AND Category='I' and Inactive='N'"; //税为3%的税组
                    oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                    string VatGroup3 = oDt.Rows[0][0].ToString();
                    sqlCmd = "SELECT Code FROM dbo.OVTG WHERE Rate=0 AND Category='I'  and Inactive='N'";  //税为0的税组
                    oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                    string VatGroup0 = oDt.Rows[0][0].ToString();
                    string DocEntry = oDataRow["DocEntry"].ToString();
                    //Specify data for child UDO
                    DataRow[] lineRows = oDataSet.Tables["TableDetail"]
                        .Select("DocNum = '" + oDataRow["DocNum"].ToString().Trim() + "'");
                    oSons = oGeneralData.Child("TPDN1");
                    foreach (DataRow LineRow in lineRows)
                    {
                        string VatGroup = VatGroup3;
                        decimal PURate = Decimal.Parse(LineRow["Rate"].ToString());
                        decimal Rate = 0m;
                        if (PURate.Equals(0))
                        {
                            VatGroup = VatGroup0;
                            Rate = 0m;
                        }
                        else
                        {
                            Rate = 0.03m;
                        }

                        string ItemCode = LineRow["ItemCode"].ToString();
                        decimal Quantity = decimal.Parse(LineRow["Quantity"].ToString());
                        string DistNumber = LineRow["DistNumber"].ToString();
                        sqlCmd =
                            "SELECT CASE WHEN T1.EvalSystem='B' THEN T2.CostTotal/T2.Quantity else T01.AvgPrice end Price,Isnull(p.U_PForBPLID,0) AddPRate FROM dbo.OITM T1 " +
                            "INNER JOIN dbo.OITW T01 ON T01.ItemCode=T1.ItemCode AND T01.WhsCode='" + CenwhsCode + "' and T1.ItemCode = '" + ItemCode + "' " +
                            " LEFT JOIN dbo.OBTN T2 ON T1.ItemCode=T2.ItemCode and T2.DistNumber = '" + DistNumber + "' "+
                            "LEFT JOIN dbo.OBTQ t3 ON T2.AbsEntry=t3.MdAbsEntry AND t3.Quantity<>0 AND t3.WhsCode='" + CenwhsCode + "' " +
                            "left Join [@PriceForBPLID] p on T1.U_PForBPLID = p.Code";
                        oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                        decimal Price = decimal.Parse(oDt.Rows[0]["Price"].ToString());
                        decimal AddPRate = decimal.Parse(oDt.Rows[0]["AddPRate"].ToString());
                        Price = Math.Round(Price * (1m + PURate)*(1m+ AddPRate), PriceDec, MidpointRounding.AwayFromZero); //根据商品档案 上的采购税率，还原库存成本价为 含税价  并乘以 加价比例
                        decimal LineTotal = Math.Round(Quantity * Price, SumDec, MidpointRounding.AwayFromZero);
                        Total += LineTotal;
                        decimal PriceAfVat = Math.Round(Price * (1m + Rate), PriceDec, MidpointRounding.AwayFromZero);
                        decimal Gtotal = Math.Round(Price * (1m + Rate) * Quantity, SumDec, MidpointRounding.AwayFromZero);
                        VatTotal += Gtotal;
                        oSon = oSons.Add();
                        oSon.SetProperty("U_BaseDoc", LineRow["DocEntry"].ToString());
                        oSon.SetProperty("U_BaseLine", LineRow["LineNum"].ToString());
                        oSon.SetProperty("U_Baseobj", "DELIVEBPL");
                        oSon.SetProperty("U_ItemCode", ItemCode);
                        oSon.SetProperty("U_Dscription", LineRow["ItemName"].ToString());
                        oSon.SetProperty("U_Speic", LineRow["U_Speic"].ToString());
                        oSon.SetProperty("U_Factory", LineRow["U_Factory"].ToString());
                        oSon.SetProperty("U_unit1", LineRow["InvntryUom"].ToString());
                        oSon.SetProperty("U_whsCode", whsCode);
                        oSon.SetProperty("U_Quantity", Quantity.ToString());
                        oSon.SetProperty("U_Price", Price.ToString());
                        oSon.SetProperty("U_LineTotal", LineTotal.ToString());
                        oSon.SetProperty("U_VatGroup", VatGroup);
                        oSon.SetProperty("U_VatPrcnt", (Rate * 100).ToString());
                        oSon.SetProperty("U_PriceAfVat", PriceAfVat.ToString());
                        oSon.SetProperty("U_Gtotal", Gtotal.ToString());
                        oSon.SetProperty("U_DistNumber", DistNumber);
                        oSon.SetProperty("U_ExpDate", DateTime.ParseExact(LineRow["ExpDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture));
                        oSon.SetProperty("U_MnfDate", DateTime.ParseExact(LineRow["MnfDate"].ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture));
                        oSon.SetProperty("U_BetDate", LineRow["U_BetDate"].ToString());
                    }
                    oGeneralData.SetProperty("U_DocTotal", Total.ToString());
                    oGeneralData.SetProperty("U_WmsDocNum", VatTotal.ToString());
                    oCompany.StartTransaction();
                    //Add records
                    oGeneralService.Add(oGeneralData);
                    int errCode = 0;

                    oCompany.GetLastError(out errCode, out errMsg);
                    if (errCode.Equals(0))
                    {
                        sqlCmd = "select DocEntry from [@TOPDN] with(nolock) where U_WmsDocNum = '"+DocNum+"' order by DocEntry Desc";
                       string  TopdnDocEntry = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd).Rows[0][0].ToString();
                        SAPbobsCOM.Recordset oRec = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        oRec.Command.Name = "SBO_SP_TransactionNotification";
                        oRec.Command.Parameters.Item(1).Value = "DELIVEBPL";
                        oRec.Command.Parameters.Item(2).Value = "A";
                        oRec.Command.Parameters.Item(3).Value = 1;
                        oRec.Command.Parameters.Item(4).Value = "DocEntry";
                        oRec.Command.Parameters.Item(5).Value = TopdnDocEntry;
                        oRec.Command.Execute();
                        string oReCErrorMsg = oRec.Fields.Item(1).Value.ToString().Trim();
                        string oReCErrorCode = oRec.Fields.Item(0).Value.ToString().Trim();
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRec);
                        if (errCode.Equals(0) && oReCErrorCode.Equals("0"))
                        {
                           string sqlcmd =
                                "UPDATE IDX_SO_TRANSACTION SET INTERFACEFLAG = 'Y' WHERE SONO = '" + DocNum + "'";
                            bool result2 = Common.ExecSqlcmd(Program.wmsConn.ConnectionString, sqlcmd);
                            
                            if (result2)
                            {
                                oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                                sqlcmd =
                                    "UPDATE [@TOPDN]  SET Status = 'I' WHERE DocEntry = '" + TopdnDocEntry + "'";
                                bool result3 = Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                            }
                            else
                            {
                                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }

                        }
                    }
                    else
                    {
                        if (oCompany.InTransaction)
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }
                        if (IsService)
                        {
                            string sqlcmd =
                                "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES(" + oDataRow["BPLName"].ToString() + ",'开发_采购收货',''," + oGeneralData.ToXMLString() + "," + errCode.ToString() + "," + errMsg + ",GetDate())";
                            Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                        }
                        else
                        {
                            MessageBox.Show(errMsg + Environment.NewLine + oGeneralData.ToXMLString());
                        }
                    }
                }

                return ;
            }
            catch (Exception exception)
            {
                try
                {
                    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                catch (Exception e)
                {
                   
                }
               
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES(" +BPLName + ",'开发_采购收货',"+DocNum+",''," + exception.HResult.ToString() + "," + exception.Message + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                }
                else
                {
                    MessageBox.Show("分支:"+BPLName+Environment.NewLine+"WMS单号:"+DocNum+Environment.NewLine+exception.ToString());
                }

                return ;
            }
        }  //生成门店开发_采购收货

        public static void CenterToShp(object IsServiceobj)  //配送中心发货给门店
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            Common.GetwmsConnStr();
            string RunCmd = String.Empty;
            try
            {
                DataSet oDataSet = new DataSet();
                //T2 分支为店铺分支，T3分支为配送中心
                string sqlCmd = "SELECT CONVERT(NVARCHAR(10),T1.U_DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry,T2.DflCust SaCardCode,T3.BPLId SaBPLId,T3.DflVendor PuCardCode, T2.BPLId PuBPLId FROM [dbo].[@TOPDN] T1 " +
                " INNER JOIN dbo.OBPL T2 ON T1.U_BPLID = T2.BPLId and T1.Status = 'P' " +
                " AND  isnull(T1.U_wmsDocnum,'')<>'' " +
                " Left join OBPL T3 on T3.MainBPL = 'Y'";
                RunCmd = sqlCmd;
                DataTable oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
               if (oDt.Rows.Count <1)
               {
                    return;
                }
                oDt.TableName = "TableHead";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();

                sqlCmd =
                    "SELECT Bpu.DflWhs BpuWhs,Bsa.DflWhs BsaWhs, T1.U_WmsDocNum DocNum , T1.DocEntry , T2.U_ItemCode ItemCode,sum(T2.U_Quantity) Quantity,t2.U_Price Price,t2.U_PriceAfVat PriceAfVat,sum(t2.U_LineTotal) PuLineTotal,sum(T2.U_Gtotal) Gtotal," +
                    "T5.Code VatGroupSa,T2.U_VatPrcnt VatPrcnt, T2.U_VatGroup VatGroupPu ,sum(T2.U_Gtotal-T2.U_LineTotal) as PuTaxTotal,sum(T2.U_Gtotal/(1+T3.Rate/100)) SaLineTotal, sum(T2.U_Gtotal-(T2.U_Gtotal/(1+T3.Rate/100))) SaTaxTotal " +
                    "FROM [dbo].[@TOPDN] T1 " +
                    "INNER JOIN [dbo].[@TPDN1] T2 ON T1.DocEntry=T2.DocEntry AND T1.Status='P' and isnull(T1.U_wmsDocnum,'')<>'' " +
                    " Inner Join OBPL Bpu on  T1.U_BPLID=Bpu.BPLID" +
                    " inner join OBPL Bsa on Bsa.MainBPL='Y'" +
                    "INNER JOIN dbo.OITM T4 ON T2.U_ItemCode=T4.ItemCode " +
                    "INNER JOIN dbo.OVTG T3 ON T4.VatGroupPu = T3.Code AND T3.Inactive = 'N' AND T3.Category = 'I' " +
                    "INNER JOIN dbo.OVTG T5 ON T3.Rate = T5.Rate AND T5.Category = 'O' AND T5.Inactive = 'N' " +
                    " group by Bpu.DflWhs ,Bsa.DflWhs , T1.U_WmsDocNum , T1.DocEntry , T2.U_ItemCode ,t2.U_Price ,t2.U_PriceAfVat,T5.Code ,T2.U_VatPrcnt , T2.U_VatGroup  ";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableRow";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();

                sqlCmd =
                    "SELECT Bpu.DflWhs BpuWhs,Bsa.DflWhs BsaWhs, T1.U_WmsDocNum DocNum , T1.DocEntry , T2.U_ItemCode ItemCode,T2.U_Quantity Quantity,t2.U_Price Price,t2.U_PriceAfVat PriceAfVat,t2.U_LineTotal PuLineTotal,T2.U_Gtotal Gtotal,T2.U_DistNumber DistNumber,convert(varchar(10),T2.U_ExpDate,112) ExpDate,Convert(varchar(10),T2.U_MnfDate,112) MnfDate ," +
                    "T5.Code VatGroupSa,T2.U_VatPrcnt VatPrcnt, T2.U_VatGroup VatGroupPu ,T2.U_Gtotal-T2.U_LineTotal as PuTaxTotal,T2.U_Gtotal/(1+T3.Rate/100) SaLineTotal, T2.U_Gtotal-(T2.U_Gtotal/(1+T3.Rate/100)) SaTaxTotal " +
                    "FROM [dbo].[@TOPDN] T1 " +
                    "INNER JOIN [dbo].[@TPDN1] T2 ON T1.DocEntry=T2.DocEntry AND T1.Status='P' and isnull(T1.U_wmsDocnum,'')<>'' " +
                    " Inner Join OBPL Bpu on  T1.U_BPLID=Bpu.BPLID" +
                    " inner join OBPL Bsa on Bsa.MainBPL='Y'" +
                    "INNER JOIN dbo.OITM T4 ON T2.U_ItemCode=T4.ItemCode " +
                    "INNER JOIN dbo.OVTG T3 ON T4.VatGroupPu = T3.Code AND T3.Inactive = 'N' AND T3.Category = 'I' " +
                    "INNER JOIN dbo.OVTG T5 ON T3.Rate = T5.Rate AND T5.Category = 'O' AND T5.Inactive = 'N'";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableDetail";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                string ConnMsg = String.Empty;
                SAPbobsCOM.Company oCompany = new CompanyClass();
                bool SAPConn = Common.GetSAPConn(out ConnMsg, ref oCompany);
                if (SAPConn)
                {
                    Common.CreateshpDocCG(out ConnMsg, ref oCompany, oDataSet, "IDX_SO_Transaction", "SONO", "SoType", "XS", IsService);
                    return ;
                }

                return ;
            }
            catch (Exception e)
            {
                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','门店发货',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);
                   
                }
                else
                {
                    MessageBox.Show(e.ToString());
                   
                }

                return ;
            }
            
        }

        public static void ShpToCenter(object IsServiceobj) //门店退回配送中心
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            Common.GetwmsConnStr();
            string RunCmd = string.Empty;
            try
            {
                DataSet oDataSet = new DataSet();
                //T2 分支为店铺分支，T3分支为配送中心
               string sqlCmd = "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry,T2.DflCust SaCardCode,T3.BPLId SaBPLId,T3.DflVendor PuCardCode, T2.BPLId PuBPLId FROM [dbo].[ODRF] T1 " +
                " INNER JOIN dbo.OBPL T2 ON T1.BPLID = T2.BPLId and T1.U_IsOK = '是' and T1.objType = 21 and isnull(T1.U_wmsDocnum,'')<>'' " +
                "INNER JOIN ["+Program.wmsConn.InitialCatalog+ "].dbo.IDX_PO_TRANSACTION T0 ON T1.U_WMSDocNum=T0.PONO AND T0.POTYPE='XT' AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                " Left join OBPL T3 on T3.MainBPL = 'Y' " +
                "GROUP BY CONVERT(NVARCHAR(10),T1.DocDate,112) ,T1.U_WmsDocNum  ,T1.DocEntry ,T2.DflCust ,T3.BPLId ,T3.DflVendor , T2.BPLId ";
                RunCmd = sqlCmd;
                DataTable oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                if (oDt.Rows.Count<1)
                {
                    return;
                }
                oDt.TableName = "TableHead";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                //税组规则：配送中心销售税率等于采购税率，门店采购税率：配送中心物料采购税率不为0则为3%,否则为0
                sqlCmd =
                    "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry,"+
                "T02.ItemCode,SUM(T2.ReceivedQty) Quantity,T00.PRICE PriceAfVat ,s02.Code VatGroupSa,S03.Code VatGroupPu" +
                " FROM [ODRF] T1 INNER JOIN DRF1 T2 ON T2.DocEntry = T1.DocEntry AND T1.ObjType = 21 AND ISNULL(T1.U_WMSDocNum,'')<> '' AND T1.DocStatus = 'O' and T1.U_IsOK = '是' "+
                "INNER JOIN OITM T02 ON T2.ItemCode = T02.ItemCode "+
                "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_DETAILS T00 ON T00.PONO = T1.U_WmsDocNum AND T00.POLINENO = T2.LineNum " +
                "INNER JOIN[" +Program.wmsConn.InitialCatalog+ "].dbo.IDX_PO_TRANSACTION T0 ON T00.PONO = T0.PONO and T0.PoLineNO=T00.POLINENO AND T0.POTYPE = 'XT'  AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                "INNER JOIN OVTG s01 ON s01.Code=t02.VatGroupPu  --配送中心采购税组 " +Environment.NewLine+
                "INNER JOIN OVTG s02 ON s02.Rate=s01.Rate AND s02.Category='O' AND s02.Inactive='N' --配送中心销售税组"+Environment.NewLine+
                "INNER JOIN OVTG S03 ON S03.Rate=CASE WHEN s01.Rate<>0 THEN 3 ELSE 0 END AND S03.Inactive='N' AND S03.Category='I'  --门店采购税组"+Environment.NewLine+
                "GROUP BY CONVERT(NVARCHAR(10), T1.DocDate, 112) ,T1.U_WmsDocNum ,T1.DocEntry ,t02.ItemCode,T00.PRICE,s02.Code,S03.Code";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableRow";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();

                sqlCmd =
                    "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry," +
                    "T02.ItemCode,SUM(T2.ReceivedQty) Quantity,T00.PRICE PriceAfVat ,T0.Lotatt04 AS DistNumber " +
                    " FROM [ODRF] T1 INNER JOIN DRF1 T2 ON T2.DocEntry = T1.DocEntry AND T1.ObjType = 21 AND ISNULL(T1.U_WMSDocNum,'')<> '' AND T1.DocStatus = 'O' and T1.U_IsOK = '是' " +
                    "INNER JOIN OITM T02 ON T2.ItemCode = T02.ItemCode " +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_DETAILS T00 ON T00.PONO = T1.U_WmsDocNum AND T00.POLINENO = T2.LineNum  " +
                    "INNER JOIN[" +Program.wmsConn.InitialCatalog+ "].dbo.IDX_PO_TRANSACTION T0 ON T00.PONO = T0.PONO and T0.PoLineNO=T00.POLINENO AND T0.POTYPE = 'XT'  AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                    "GROUP BY CONVERT(NVARCHAR(10), T1.DocDate, 112) ,T1.U_WmsDocNum ,T1.DocEntry ,t02.ItemCode,T00.PRICE,T0.Lotatt04";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableDetail";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                string ConnMsg = String.Empty;
                SAPbobsCOM.Company oCompany = new CompanyClass();
                bool SAPConn = Common.GetSAPConn(out ConnMsg, ref oCompany);
                if (SAPConn)
                {
                    Common.CreateshpDocCT(out ConnMsg, ref oCompany, oDataSet, "IDX_PO_Transaction", "PONO", "PoType", "XT", IsService);
                }

                return ;
            }
            catch (Exception e)
            {

                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','门店退货',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);

                }
                else
                {
                    MessageBox.Show(e.ToString());

                }

                return ;
            }
           
        }
        public static void CenScrap(object IsServiceobj) //配送中心报废
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            Common.GetwmsConnStr();
            string RunCmd = string.Empty;
            try
            {
                DataSet oDataSet = new DataSet();
               
                string sqlCmd = "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry ,T1.BPLId FROM [dbo].[ODRF] T1 " +
                 " INNER JOIN dbo.OBPL T2 ON T1.BPLID = T2.BPLId  and T1.objType = 60 and isnull(T1.U_wmsDocnum,'')<>'' and T2.MainBPL = 'Y' " +
                 "INNER JOIN [" + Program.wmsConn.InitialCatalog + "].dbo.IDX_SO_TRANSACTION T0 ON T1.U_WMSDocNum=T0.SONO AND T0.SOTYPE=case when T1.U_WMSType='报废' then  'BF' else 'BS' end AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                 "GROUP BY CONVERT(NVARCHAR(10),T1.DocDate,112) ,T1.U_WmsDocNum  ,T1.DocEntry ,T1.BPLId ";
                RunCmd = sqlCmd;
                DataTable oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                if (oDt.Rows.Count < 1)
                {
                    return;
                }
                oDt.TableName = "TableHead";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
               
                sqlCmd =
                    "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry," +
                "T2.ItemCode,SUM(T0.ShippedQty) Quantity,T2.whsCode,T2.LineNum,T2.LineTotal " +
                " FROM [ODRF] T1 INNER JOIN DRF1 T2 ON T2.DocEntry = T1.DocEntry AND T1.ObjType = 60 AND ISNULL(T1.U_WMSDocNum,'')<> '' AND T1.DocStatus = 'O' " +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_SO_DETAILS T00 ON T00.SONO = T1.U_WmsDocNum AND T00.SOLINENO = T2.LineNum " +
                "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_SO_TRANSACTION T0 ON T00.SONO = T0.SONO and T0.SoLineNO=T00.SOLINENO AND T0.SOTYPE = case when T1.U_WMSType='报废' then  'BF' else 'BS' end  AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                    "GROUP BY CONVERT(NVARCHAR(10), T1.DocDate, 112) ,T1.U_WmsDocNum ,T1.DocEntry ,t2.ItemCode,T2.whsCode,T2.LineNum,T2.LineTotal";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableRow";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();

                sqlCmd =
                    "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry," +
                    "T2.ItemCode,SUM(T0.ShippedQty) Quantity,T0.Lotatt04 AS DistNumber,T2.LineNum " +
                    "FROM [ODRF] T1 INNER JOIN DRF1 T2 ON T2.DocEntry = T1.DocEntry AND T1.ObjType = 60 AND ISNULL(T1.U_WMSDocNum,'')<> '' AND T1.DocStatus = 'O'" +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_SO_DETAILS T00 ON T00.SONO = T1.U_WmsDocNum AND T00.SOLINENO = T2.LineNum  " +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_SO_TRANSACTION T0 ON T00.SONO = T0.SONO and T0.SoLineNO=T00.SOLINENO AND T0.SOTYPE = case when T1.U_WMSType='报废' then  'BF' else 'BS' end  AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                    "GROUP BY CONVERT(NVARCHAR(10), T1.DocDate, 112) ,T1.U_WmsDocNum ,T1.DocEntry ,t2.ItemCode,T0.Lotatt04,T2.LineNum";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableDetail";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                string ConnMsg = String.Empty;
                SAPbobsCOM.Company oCompany = new CompanyClass();
                bool SAPConn = Common.GetSAPConn(out ConnMsg, ref oCompany);
                if (SAPConn)
                {
                    Common.CreateOIGEDoc(out ConnMsg, ref oCompany, oDataSet,IsService);
                }

                return;
            }
            catch (Exception e)
            {

                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','门店退货',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);

                }
                else
                {
                    MessageBox.Show(e.ToString());

                }

                return;
            }

        }
        public static void CenAddStock(object IsServiceobj) //配送中心报溢
        {
            bool IsService = Boolean.Parse(IsServiceobj.ToString());
            Common.GetwmsConnStr();
            string RunCmd = string.Empty;
            try
            {
                DataSet oDataSet = new DataSet();

                string sqlCmd = "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry ,T1.BPLId FROM [dbo].[ODRF] T1 " +
                 " INNER JOIN dbo.OBPL T2 ON T1.BPLID = T2.BPLId  and T1.objType = 59 and isnull(T1.U_wmsDocnum,'')<>'' and T2.MainBPL = 'Y' " +
                 "INNER JOIN [" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_TRANSACTION T0 ON T1.U_WMSDocNum=T0.PONO AND T0.POTYPE='BY' AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                 "GROUP BY CONVERT(NVARCHAR(10),T1.DocDate,112) ,T1.U_WmsDocNum  ,T1.DocEntry ,T1.BPLId ";
                RunCmd = sqlCmd;
                DataTable oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                if (oDt.Rows.Count < 1)
                {
                    return;
                }
                oDt.TableName = "TableHead";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();

                sqlCmd =
                    "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry," +
                "T2.ItemCode,SUM(T0.ReceivedQty) Quantity,T2.whsCode,T2.LineNum,T2.LineTotal " +
                " FROM [ODRF] T1 INNER JOIN DRF1 T2 ON T2.DocEntry = T1.DocEntry AND T1.ObjType = 59 AND ISNULL(T1.U_WMSDocNum,'')<> '' AND T1.DocStatus = 'O' " +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_DETAILS T00 ON T00.PONO = T1.U_WmsDocNum AND T00.POLINENO = T2.LineNum " +
                "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_TRANSACTION T0 ON T00.PONO = T0.PONO and T0.PoLineNO=T00.POLINENO AND T0.POTYPE = 'BY'  AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                    "GROUP BY CONVERT(NVARCHAR(10), T1.DocDate, 112) ,T1.U_WmsDocNum ,T1.DocEntry ,t2.ItemCode,T2.whsCode,T2.LineNum,T2.LineTotal";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableRow";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();

                sqlCmd =
                    "SELECT CONVERT(NVARCHAR(10),T1.DocDate,112) AS DocDate,T1.U_WmsDocNum DocNum ,T1.DocEntry DocEntry," +
                    "T2.ItemCode,SUM(T0.ReceivedQty) Quantity,T0.Lotatt04 AS DistNumber,T2.LineNum,case when T0.Lotatt01='' then '' else  convert(varchar(10), convert(date,T0.Lotatt01),112) end MnfDate," +
                    "case when T0.Lotatt02='' then '' else  convert(varchar(10),convert(date,T0.Lotatt02),112) end ExpDate " +
                    "FROM [ODRF] T1 INNER JOIN DRF1 T2 ON T2.DocEntry = T1.DocEntry AND T1.ObjType = 59 AND ISNULL(T1.U_WMSDocNum,'')<> '' AND T1.DocStatus = 'O'" +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_DETAILS T00 ON T00.PONO = T1.U_WmsDocNum AND T00.POLINENO = T2.LineNum  " +
                    "INNER JOIN[" + Program.wmsConn.InitialCatalog + "].dbo.IDX_PO_TRANSACTION T0 ON T00.PONO = T0.PONO and T0.PoLineNO=T00.POLINENO AND T0.POTYPE = 'BY'  AND ISNULL(T0.INTERFACEFLAG,'N')<>'Y' " +
                    "GROUP BY CONVERT(NVARCHAR(10), T1.DocDate, 112) ,T1.U_WmsDocNum ,T1.DocEntry ,t2.ItemCode,T0.Lotatt04,T2.LineNum,case when T0.Lotatt01='' then '' else  convert(varchar(10), convert(date,T0.Lotatt01),112) end,case when T0.Lotatt02='' then '' else  convert(varchar(10),convert(date,T0.Lotatt02),112) end";
                RunCmd = sqlCmd;
                oDt = Common.GetDataTable(Program.SAPConn.ConnectionString, sqlCmd);
                oDt.TableName = "TableDetail";
                oDataSet.Tables.Add(oDt);
                oDataSet.AcceptChanges();
                string ConnMsg = String.Empty;
                SAPbobsCOM.Company oCompany = new CompanyClass();
                bool SAPConn = Common.GetSAPConn(out ConnMsg, ref oCompany);
                if (SAPConn)
                {
                    Common.CreateOIGNDoc(out ConnMsg, ref oCompany, oDataSet, IsService);
                }

                return;
            }
            catch (Exception e)
            {

                if (IsService)
                {
                    string sqlcmd =
                        "INSERT dbo.WMS_ImportErr(BPLName, DocType, WMSDocNum, DocXML, errCode, errMsg, CreateTime)VALUES('配送中心','报溢',''," + RunCmd + "," + e.HResult.ToString() + "," + e.ToString() + ",getDate())";
                    Common.ExecSqlcmd(Program.SAPConn.ConnectionString, sqlcmd);

                }
                else
                {
                    MessageBox.Show(e.ToString());

                }

                return;
            }

        }
    }

    class Ouser
    {
        public  string UserCode = String.Empty;
        public  bool Supper = false;
        public  bool IsPur = false;
        public  bool IsRet = false;
        public  bool IsSAL = false;
        public  bool IsRet4SAL = false;
        public bool IsCenOIGE = false;
        public bool IsCenOIGN = false;

    }
}

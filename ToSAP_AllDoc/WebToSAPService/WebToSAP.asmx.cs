﻿using System;
using System.Data;
using System.Web.Services;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Services;
using Newtonsoft.Json;
using SAPbobsCOM;

namespace WebToSAPService
{
    /// <summary>
    /// WebToSAP 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
     [System.Web.Script.Services.ScriptService]
    public class WebToSAP : System.Web.Services.WebService
    {
        public  SqlConnection FwSqlConnection = new SqlConnection();
        public  SqlConnection SAPsqlConnection = new SqlConnection();
        public  SAPbobsCOM.Company SAPCompany = new SAPbobsCOM.Company();
        public static SqlConnectionStringBuilder FWSqlConnStr = new SqlConnectionStringBuilder();
        public  SqlConnectionStringBuilder SAPsqlConnStr = new SqlConnectionStringBuilder();
        public string InterFaceSet = string.Empty;
        public string CorpSet = string.Empty;
        public string shpCode_1 = string.Empty;
        public static string oeXists = string.Empty;
        public string FWuserSAPuser = string.Empty;
        public static string ProgramVersion = String.Empty;

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void LogToSAP(string oUserCode, string shpCode, string ToSAPini)
        {
            try
            {
               string reslut=String.Empty;
               JObject reslutjobj;
               Session["oUser"] = byteToString(oUserCode);
               setConnstr(byteToString(ToSAPini));
                if (!ConnFWsql())
                {
                    reslut = GetJson("1", "ERP SQL 连接失败!请检查WEB配置!","0");
                    Context.Response.Headers.Add("ResultMsg", reslut);
                   Context.Response.Write("");
                   // Context.ApplicationInstance.CompleteRequest();
                    return;
                   
                }
                GetAppInit(FWSqlConnStr.ConnectionString);
                reslut = GetSAPConnect(byteToString(shpCode), byteToString(ToSAPini));
                Context.Response.Headers.Add("ResultMsg", reslut);
                Context.Response.Write("");
                
                // Context.ApplicationInstance.CompleteRequest();
                return;
            }
            catch (Exception e)
            {
                string reslut = GetJson("1", "登陆SAP失败,错误:" + e.ToString(),"0");
                Context.Response.Headers.Add("ResultMsg", reslut);
                Context.Response.Write(reslut);
                
                // Context.ApplicationInstance.CompleteRequest();
                return;
            }
            
        }

        [WebMethod(EnableSession = true)]
        public void GetSAPDB(string ToSAPini)
        {
            SAPbobsCOM.Company oCompany = new SAPbobsCOM.Company();
            JObject iniJObject = JObject.Parse(byteToString(ToSAPini));
            oCompany.Server  = ((JObject)iniJObject["SAP"])["SAPDBserver"].ToString().Trim();
            string SAPDBTYPE = ((JObject)iniJObject["SAP"])["SAPDBTYPE"].ToString().Trim();
            switch (SAPDBTYPE.Trim())
            {
                case "SQL2008":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "SQL2012":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "SQL2014":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "SQL2016":
                    oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    break;
                default:
                    oCompany.DbServerType = BoDataServerTypes.dst_HANADB;
                    break;
            }
            string sapdbpwd= ((JObject)iniJObject["SAP"])["SAPDBpwd"].ToString().Trim();
            oCompany.DbPassword = Decrypt(sapdbpwd);
            oCompany.DbUserName = ((JObject)iniJObject["SAP"])["SAPDBuser"].ToString().Trim();
            oCompany.LicenseServer = ((JObject)iniJObject["SAP"])["SAPLicensesever"].ToString().Trim();
            oCompany.SLDServer =
                "https://" + oCompany.LicenseServer.Replace(":30000", ":40000");
            DataTable SAPDB = new DataTable("MYdata");
            SAPDB.Columns.Add("公司名称", typeof(string));
            SAPDB.Columns.Add("数据库", typeof(string));
            SAPbobsCOM.Recordset oRec = oCompany.GetCompanyList();
            for (int i = 0; i < oRec.RecordCount; i++)
            {
                DataRow oDataRow = SAPDB.NewRow();
                oDataRow[0] = oRec.Fields.Item("cmpName").Value.ToString().Trim();
                oDataRow[1] = oRec.Fields.Item("dbName").Value.ToString().Trim();
                SAPDB.Rows.Add(oDataRow);
                oRec.MoveNext();
            }
            string reslut = GetJson("0", JsonConvert.SerializeObject(SAPDB), "0");
            Context.Response.Headers.Add("ResultMsg", reslut);
           Context.Response.Write(reslut);
           Context.ApplicationInstance.CompleteRequest();
        }

        [WebMethod(EnableSession = true)]
        public void DisConnectSAP(string oUserCode, string shpCode)
        {
            if (Context.Session["SAPCompany"] != null)
            {
                SAPCompany = (SAPbobsCOM.Company)Context.Session["SAPCompany"];
                if (SAPCompany.Connected)
                {
                    SAPCompany.Disconnect();
                    string FWuserSAPuser = (string)Context.Session["FWuserSAPuser"];
                    oeXists = oeXists.Replace(FWuserSAPuser, "");
                    
                }
                string FWuserSAPuser1 = (string)Context.Session["FWuserSAPuser"];
                oeXists = oeXists.Replace(FWuserSAPuser1, "");
            }

            if (byteToString(shpCode) == "")
            {
                string sqlCmd = "SELECT * FROM [ToSAP_SynUser] where FWuser = '" + byteToString(oUserCode) + "'";
                DataTable oDataTable = GetDataTable(FWSqlConnStr.ConnectionString, sqlCmd);
               string SAPuser = oDataTable.Rows[0]["SAPuser"].ToString().Trim();
                string[] userexit = oeXists.Split(';');
                foreach (string userexit1 in userexit)
                {
                    if (userexit1.IndexOf(SAPuser) > 0)
                    {
                        oeXists = oeXists.Replace(userexit1, "");
                    }
                }
            }
            
            GC.Collect();
            string reslut=GetJson("0", "断开SAP连接成功","0");
            Context.Response.Headers.Add("ResultMsg", reslut);
            Context.Response.Write("");
            Context.ApplicationInstance.CompleteRequest();
             return;
        }

        [WebMethod(EnableSession = true)]
        public void importToSAP(string importXML,string objType0,string ValueDt)
        {
            SqlConnection oSqlConn = (SqlConnection)Session["erpSqlConn"];
            SqlTransaction oSqlTransaction = oSqlConn.BeginTransaction(IsolationLevel.ReadUncommitted);
            SAPCompany = (SAPbobsCOM.Company)Context.Session["SAPCompany"];
            try
            {
                string importXML1 = byteToString(importXML);
                int errCode = 0;
                string errMsg = "";
                

                string obj = byteToString(objType0);
                switch (obj)
            {
                case "业务伙伴主数据":
                 
                    SAPbobsCOM.BusinessPartners oPartners = (SAPbobsCOM.BusinessPartners)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    oPartners.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oPartners);
                    break;
                case "物料主数据":
                    SAPbobsCOM.Items oTems = (SAPbobsCOM.Items)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    oTems.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oTems);
                    break;
                case "成本中心":
                    SAPbobsCOM.ProfitCentersService sapobj =(ProfitCentersService)SAPCompany.GetCompanyService().GetBusinessService(ServiceTypes.ProfitCentersService);
                    sapobj.AddProfitCenter((ProfitCenter)sapobj.GetDataInterfaceFromXMLString(importXML1));
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(sapobj);
                    break;
                case "物料组":
                    SAPbobsCOM.ItemGroups oItemGroups =
                        (SAPbobsCOM.ItemGroups)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    oItemGroups.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oItemGroups);
                    break;
                case "仓库主数据":
                    SAPbobsCOM.Warehouses oWarehouses =
                        (SAPbobsCOM.Warehouses) SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    oWarehouses.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWarehouses);
                    break;
                case "修改仓库主数据":
                    SAPbobsCOM.Warehouses oWarehouses1 =
                        (SAPbobsCOM.Warehouses)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);

                    SAPbobsCOM.Warehouses oWarehouses2 =
                        (SAPbobsCOM.Warehouses)SAPCompany.GetBusinessObject(BoObjectTypes.oWarehouses);
                    oWarehouses2.GetByKey(oWarehouses1.WarehouseCode);
                    oWarehouses2.BusinessPlaceID = oWarehouses1.BusinessPlaceID;
                    oWarehouses2.Update();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWarehouses1);
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWarehouses2);
                    break;
                    case "分支主数据":
                    SAPbobsCOM.BusinessPlaces objectFrom =(SAPbobsCOM.BusinessPlaces)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    objectFrom.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(objectFrom);
                    break;
                case "单据":
                        if (!SAPCompany.InTransaction)
                        {
                            SAPCompany.StartTransaction();
                        }

                        SAPbobsCOM.Documents oDocuments = (SAPbobsCOM.Documents)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    oDocuments.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oDocuments);
                    break;
                case "库存转储":
                    if (!SAPCompany.InTransaction)
                    {
                        SAPCompany.StartTransaction();
                    }
                    SAPbobsCOM.StockTransfer oWToW = (SAPbobsCOM.StockTransfer)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                    oWToW.Add();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWToW);
                    break;
                    case "凭证":
                        if (!SAPCompany.InTransaction)
                        {
                            SAPCompany.StartTransaction();
                        }
                        SAPbobsCOM.JournalEntries oJur = (SAPbobsCOM.JournalEntries)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
                        
                        oJur.Add();
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oJur);
                        break;
                    case "凭单":
                        if (!SAPCompany.InTransaction)
                        {
                            SAPCompany.StartTransaction();
                        }
                        SAPbobsCOM.JournalVouchers oJur1 = (SAPbobsCOM.JournalVouchers)SAPCompany.GetBusinessObjectFromXML(importXML1, 0);

                        oJur1.Add();
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oJur1);
                        break;
                    default:
                        Context.Response.Write(GetJson("1", "WebService 无此接口: " + byteToString(objType0),"0"));
                         Context.ApplicationInstance.CompleteRequest();
                        return;
                    break;
                }

               SAPCompany.GetLastError(out errCode,out errMsg);
               string NewobjKey = SAPCompany.GetNewObjectKey();


                byte[] contentData = System.Convert.FromBase64String(ValueDt);
                byte[] uwrapBytes = LZ4.LZ4Codec.Unwrap(contentData);
                string contentString = System.Text.Encoding.Unicode.GetString(uwrapBytes);
                DataTable oDt = JsonConvert.DeserializeObject<DataTable>(contentString);

                if (errCode.Equals(0) || errCode.Equals(-10) && !(obj.Equals("单据") || obj.Equals("库存转储")) || errMsg.Contains("此条目已在下表中")) /*数据导入成功*/
                {
                   
                    bool IsRollBack = false;
                    foreach (DataRow oDtRow in oDt.Rows)
                    {
                        //doctype, docdescr, docentry, bplid, docdate, sapdoctype, sapdocdescr, sapdocentry, usercode, sapcompany, curversion
                        string DocType = oDtRow["DocType"].ToString();
                        string SAPDocType = oDtRow["SAPDocType"].ToString();
                        string GrpBPLID = oDtRow["GrpBPLID"].ToString();
                        string absDocEntry = oDtRow["absDocEntry"].ToString();
                        string DocNum = oDtRow["DocNum"].ToString();
                        string DocDate = oDtRow["DocDate"].ToString();
                         ProgramVersion = oDtRow["ProgramVersion"].ToString();
                        /*插入导入 成功标识*/
                        bool IsInsert= ToSAP_ImportOK(oSqlConn, DocType, DocNum, absDocEntry, GrpBPLID,
                            DocDate, SAPDocType, "", NewobjKey, Session["oUser"].ToString(),
                            SAPCompany.CompanyDB, oSqlTransaction);
                            if (IsInsert.Equals(false)) /*标识插入失败*/
                            {
                                IsRollBack = true;
                                errCode = -5201314;
                                errMsg = "插入标识失败！";
                                continue;
                            }
                    }

                    if (IsRollBack.Equals(true))
                    {/*回滚数据*/
                        try
                        {
                            oSqlTransaction.Rollback();
                        }
                        catch (Exception e)
                        {
                           
                        }
                        try
                        {
                            if (SAPCompany.InTransaction)
                            {
                                SAPCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }
                               
                        }
                        catch (Exception e)
                        {

                        }

                    }
                    else
                    {/*保存数据*/
                        try
                        {
                            oSqlTransaction.Commit();
                        }
                        catch (Exception e)
                        {

                        }
                        try
                        {
                            if (SAPCompany.InTransaction)
                            {
                                SAPCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                            }
                            
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
                else /*数据导入失败*/
                {
                    string DocNum = String.Empty,DocType = string.Empty,SAPDocType = String.Empty,DocDate = String.Empty,shpCode= String.Empty;
                    
                    foreach (DataRow oDtRow in oDt.Rows)
                    {
                        shpCode = oDtRow["shpCode"].ToString();
                        DocType = oDtRow["DocType"].ToString();
                        DocDate = oDtRow["DocDate"].ToString();
                        SAPDocType = oDtRow["SAPDocType"].ToString();
                        DocNum += "," + oDtRow["DocNum"].ToString();
                    }
                    ToSAP_ImportErr(oSqlConn, shpCode, "","",DateTime.Now.ToString("yyyy-MM-dd"), DocType,"",DocNum.Substring(1),"",DocDate,SAPDocType,errMsg+Environment.NewLine +importXML1,oSqlTransaction);
                    oSqlTransaction.Commit();
                }

                #region  分支主数据 手动添加操作员分支权限
                bool IsAsBPL = false;
                try
                {
                    string sqlCmd1 = "select AutoAsnBPL from OUSR";
                    DataTable allUser = GetDataTable(SAPsqlConnStr.ConnectionString, sqlCmd1);
                    IsAsBPL = true;
                }
                catch (Exception e)
                {

                    IsAsBPL = false;
                }
                if (obj.Equals("分支主数据") && IsAsBPL.Equals(false))
                {
                    SAPsqlConnStr = (SqlConnectionStringBuilder)Context.Session["SAPConn"];
                    string sqlCmd = "select USERID from OUSR where GROUPS=0 and Locked='N'";
                    DataTable allUser = GetDataTable(SAPsqlConnStr.ConnectionString, sqlCmd);
                    for (int i = 0; i < allUser.Rows.Count; i++)
                    {
                        SAPbobsCOM.Users oUsers = SAPCompany.GetBusinessObject(BoObjectTypes.oUsers) as SAPbobsCOM.Users;
                        int UserId = Int32.Parse(allUser.Rows[i][0].ToString());
                        if (oUsers.GetByKey(UserId))
                        {
                            sqlCmd =
                                "select BPLId from OBPL where Disabled='N' and BPLId not in (select BPLId from USR6 T0 where T0.UserCode='" + oUsers.UserCode + "')";
                            DataTable oBplTable = GetDataTable(SAPsqlConnStr.ConnectionString, sqlCmd);
                            if (oBplTable.Rows.Count > 0)
                            {
                                for (int j = 0; j < oBplTable.Rows.Count; j++)
                                {
                                    int BPLId = Int32.Parse(oBplTable.Rows[j][0].ToString());
                                    oUsers.UserBranchAssignment.BPLID = BPLId;
                                    oUsers.UserBranchAssignment.Add();
                                }
                                oUsers.Update();

                            }

                        }
                    }


                }

                #endregion
                GC.Collect();
                string reslut = GetJson(errCode.ToString(), errCode == 0 ? "数据导入成功!" + NewobjKey : "数据导入失败!错误：" + errMsg, "0");
                Context.Response.Headers.Add("ResultMsg", reslut);
                Context.Response.Write("");
                 Context.ApplicationInstance.CompleteRequest();
                 return;
               
            }
            catch (Exception e)
            {
                try
                {
                    oSqlTransaction.Rollback();
                    try
                    {
                        if (SAPCompany.InTransaction)
                        {
                            SAPCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }
                            
                    }
                    catch (Exception)
                    {
                    }
                }
                catch (Exception exception)
                {
                   
                }
                
                string reslut= GetJson("1", "数据导入失败!异常:" + e.ToString(),"0");
                Context.Response.Headers.Add("ResultMsg", reslut);
                Context.Response.Write("");
                Context.ApplicationInstance.CompleteRequest();
                 return;
            }

        }

        [WebMethod(EnableSession = true)]
        public string CommitData(String IsRollBack)
        {
            int errCode=0;
            SAPCompany = (SAPbobsCOM.Company)Context.Session["SAPCompany"];
            String errMsg=String.Empty,IsRoll =  byteToString(IsRollBack);
            if (SAPCompany.InTransaction)
            {
                if (IsRoll.Equals("Y"))
                {
                    SAPCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
                else
                {
                    SAPCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                SAPCompany.GetLastError(out errCode,out errMsg);
                if (errCode.Equals(0))
                {
                    return "0_数据Commit成功！";
                }
                else
                {
                    return "1_数据Commit失败："+errMsg;
                }
            }
            else
            {
                return "1_数据Commit失败：SAP系统不在事务处理中!";
            }

        }

        [WebMethod(EnableSession = true)]
        public void DeletItem(string ItemCode)
        {
           SAPbobsCOM.Company oCompany= (SAPbobsCOM.Company)Context.Session["SAPCompany"];
            SAPbobsCOM.Items oItems = (SAPbobsCOM.Items)oCompany.GetBusinessObject(BoObjectTypes.oItems);
            if (oItems.GetByKey(byteToString(ItemCode)))
            {
                if (oItems.Remove() == 0)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oItems);
                    string reslut= GetJson("0", "删除成功!:" + byteToString(ItemCode),"0");
                    Context.Response.Headers.Add("ResultMsg", reslut);
                    Context.Response.Write("");
                    Context.ApplicationInstance.CompleteRequest();
                    return;
                }
                else
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oItems);
                    string reslut= GetJson("1", "删除失败!:" + byteToString(ItemCode) + " " + SAPCompany.GetLastErrorDescription(),"0");
                    Context.Response.Headers.Add("ResultMsg", reslut);
                    Context.Response.Write("");
                    Context.ApplicationInstance.CompleteRequest();
                    return;
                }
            }

            string reslut1= GetJson("0","成功","0");
            Context.Response.Headers.Add("ResultMsg", reslut1);
            Context.Response.Write("");
            Context.ApplicationInstance.CompleteRequest();
             return;
        }

        [WebMethod(EnableSession = true)]
        public void AddField(string AddFieldxml)
        {
            string importXML1 = byteToString(AddFieldxml);
            int errCode = 0;
            string errMsg = "";
            SAPCompany = (SAPbobsCOM.Company)Context.Session["SAPCompany"];
            SAPbobsCOM.UserFieldsMD oUserFieldsMd= (SAPbobsCOM.UserFieldsMD) SAPCompany.GetBusinessObjectFromXML(importXML1, 0);
            oUserFieldsMd.Add();
            SAPCompany.GetLastError(out errCode, out errMsg);
            GC.Collect();
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oUserFieldsMd);
            string reslut= GetJson(errCode.ToString(),
                errCode == 0
                    ? "字段添加成功!" + SAPCompany.GetNewObjectKey()
                    : "字段添加失败!错误:" + errCode.ToString() + "/" + errMsg,"0");
            Context.Response.Headers.Add("ResultMsg", reslut);
            Context.Response.Write("");
            Context.ApplicationInstance.CompleteRequest();
             return;

        }

        public void setConnstr(string ToSAPini)
        {
            JObject ToSAPJson = JObject.Parse(ToSAPini);
            FWSqlConnStr.DataSource = ((JObject)ToSAPJson["FW"])["FWDBserver"].ToString().Trim();
            FWSqlConnStr.InitialCatalog = ((JObject)ToSAPJson["FW"])["FWDB"].ToString().Trim();
            FWSqlConnStr.UserID = ((JObject)ToSAPJson["FW"])["FWDBuser"].ToString().Trim();
            string fwPwd = ((JObject)ToSAPJson["FW"])["FWDBpwd"].ToString().Trim();
            FWSqlConnStr.Password = Decrypt(fwPwd);
            FWSqlConnStr.MaxPoolSize = 512;
            
            FwSqlConnection.ConnectionString = FWSqlConnStr.ConnectionString;
        }

        public bool ConnFWsql()
        {
            FwSqlConnection.ConnectionString = FWSqlConnStr.ConnectionString;
            try
            {
                FwSqlConnection.Open();
                Session["erpSqlConn"] = FwSqlConnection;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public void GetAppInit(string oSqlConnectionstr)
        {
            string sqlCmd = "SELECT *  FROM [ToSAP_Appinit] where 1=1";
            DataTable oDataTable = GetDataTable(FWSqlConnStr.ConnectionString, sqlCmd);
            try
            {
                InterFaceSet = oDataTable.Select("ParaCode ='InterFaceMode'")[0]["Value1"].ToString().Trim();
                CorpSet = oDataTable.Select("ParaCode ='Corp'")[0]["Value1"].ToString().Trim();
               string shpCode1 = oDataTable.Select("ParaCode ='Shop'")[0]["Value1"].ToString().Trim();
                //Program.DocBranch = oDataTable.Select("ParaCode ='DocBranch'")[0]["Value1"].ToString().Trim();
            }
            catch (Exception e)
            {
            }
        }
        public string GetSAPConnect( string shpCode, string connStr)
        {
            if (Session["SAPCompany"] != null)
            {
                SAPCompany = (SAPbobsCOM.Company)Session["SAPCompany"];
            }

            Session["shpCode"] = shpCode;
            string SAPDB = GetSAPDBName(FWSqlConnStr.ConnectionString, shpCode);
            JObject reslut = new JObject();
            JObject ToSAPJson = JObject.Parse(connStr);
            string sqlCmd = "SELECT * FROM [ToSAP_SynUser] where FWuser = '" + Session["oUser"].ToString() + "'";
            DataTable oDataTable = GetDataTable(FWSqlConnStr.ConnectionString, sqlCmd);
            if (oDataTable.Rows.Count < 1)
            {

                return GetJson("1", "SAP连接失败,未配置同步账号","0");

            }
            string SAPuser = oDataTable.Rows[0]["SAPuser"].ToString().Trim();

            if (SAPCompany.Connected && SAPCompany.CompanyDB == SAPDB && SAPCompany.UserName==SAPuser)
            {
                return  GetJson("0", "SAP连接成功","0");
                
            }
           
            string[] userExist = oeXists.Split(';');
            foreach (var userExist1 in userExist)
            {
                int i = userExist1.IndexOf(SAPuser);
                if (i > 0)
                {
                    return  GetJson("1", "SAP账号" + SAPuser + "正在同步数据, 请稍后重试!用户" + userExist1.Split(':')[0].ToString(),"0");
                }
            }
            SAPCompany.Disconnect();
            oeXists=oeXists.Replace(Session["oUser"].ToString(), "").Replace(";;","");
            oeXists += ";" + Session["oUser"].ToString() + ":" + SAPuser;
            FWuserSAPuser = ";" +Session["oUser"].ToString() + ":" + SAPuser;
            Context.Session["FWuserSAPuser"] = FWuserSAPuser;
            SAPCompany.Server = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            string SAPDBTYPE = ((JObject)ToSAPJson["SAP"])["SAPDBTYPE"].ToString().Trim();
            switch (SAPDBTYPE.Trim())
            {
                case "SQL2008":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "SQL2012":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "SQL2014":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "SQL2016":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    break;
                default:
                    SAPCompany.DbServerType = BoDataServerTypes.dst_HANADB;
                    break;
            }

            SAPCompany.DbPassword = Decrypt(((JObject)ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
            SAPCompany.DbUserName = ((JObject)ToSAPJson["SAP"])["SAPDBuser"].ToString().Trim();
            SAPCompany.LicenseServer = ((JObject)ToSAPJson["SAP"])["SAPLicensesever"].ToString().Trim();
            SAPCompany.SLDServer =
                "https://" + SAPCompany.LicenseServer.Replace(":30000", ":40000") ;
            SAPCompany.CompanyDB = SAPDB;
            SAPCompany.UserName = SAPuser;
            SAPCompany.Password = Decrypt(oDataTable.Rows[0]["SAPPwd"].ToString().Trim());
            SAPCompany.language = BoSuppLangs.ln_Chinese;

            SAPsqlConnStr.DataSource = SAPCompany.Server;
            SAPsqlConnStr.MaxPoolSize = 200;
            SAPsqlConnStr.UserID = SAPCompany.DbUserName;
            SAPsqlConnStr.Password = Decrypt(((JObject)ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
            SAPsqlConnStr.InitialCatalog = SAPDB;
            Context.Session["SAPConn"] = SAPsqlConnStr;

            if (SAPCompany.Connect() == 0)
            {
                SAPCompany.XMLAsString = true;
                SAPCompany.XmlExportType = BoXmlExportTypes.xet_ExportImportMode;
                Context.Session["SAPCompany"] = SAPCompany;
                SAPsqlConnection.ConnectionString = SAPsqlConnStr.ConnectionString;
                SAPsqlConnection.Open();
                Context.Session["SAPsqlConnection"] = SAPsqlConnection ;
                string reslut1 = GetJson("0", "SAP 连接成功", "0");

                return reslut1;


            }

            // MessageBox.Show("SAP 连接失败:\n" + SAPCompany.GetLastErrorDescription());
          string  reslut2 = GetJson(SAPCompany.GetLastErrorCode().ToString(), "SAP连接失败" + SAPCompany.GetLastErrorDescription(), "0");
          return reslut2;


        }
        [WebMethod(EnableSession = true)]
        public string SAPConnectTest(string ToSAPini)
        {
            JObject ToSAPJson = JObject.Parse(byteToString(ToSAPini));
            SAPCompany.Server = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            string SAPDBTYPE = ((JObject)ToSAPJson["SAP"])["SAPDBTYPE"].ToString().Trim();
            switch (SAPDBTYPE.Trim())
            {
                case "SQL2008":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "SQL2012":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "SQL2014":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "SQL2016":
                    SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    break;
                default:
                    SAPCompany.DbServerType = BoDataServerTypes.dst_HANADB;
                    break;
            }

            SAPCompany.DbPassword = Decrypt(((JObject)ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
            SAPCompany.DbUserName = ((JObject)ToSAPJson["SAP"])["SAPDBuser"].ToString().Trim();
            SAPCompany.LicenseServer = ((JObject)ToSAPJson["SAP"])["SAPLicensesever"].ToString().Trim();
            SAPCompany.SLDServer =
                "https://" + SAPCompany.LicenseServer.Replace(":30000", ":40000");
            SAPCompany.CompanyDB = ((JObject)ToSAPJson["SAP"])["SAPDB"].ToString().Trim();
            SAPCompany.UserName = ((JObject)ToSAPJson["SAP"])["SAPUSER"].ToString().Trim(); 
            SAPCompany.Password = Decrypt(((JObject)ToSAPJson["SAP"])["SAPPWD"].ToString().Trim());
            SAPCompany.language = BoSuppLangs.ln_Chinese;

            if (SAPCompany.Connect() == 0)
            {
                string reslut1 = GetJson("0", "SAP 连接成功!", "0");
                Context.Response.Headers.Add("ResultMsg", reslut1);
                Context.Response.Write("");
                return reslut1;

            }
            string reslut2 = GetJson(SAPCompany.GetLastErrorCode().ToString(), "SAP连接失败：" + SAPCompany.GetLastErrorDescription(), "0");
            Context.Response.Headers.Add("ResultMsg", reslut2);
            Context.Response.Write("");
            return reslut2;
        }

        [WebMethod(EnableSession = true)]
        public void UpdateItem(string ItemCode, string ItemName, string itemGroup)
        {
            SAPCompany = (SAPbobsCOM.Company) Session["SAPCompany"];
            SAPbobsCOM.Items oItems = (SAPbobsCOM.Items)SAPCompany.GetBusinessObject(BoObjectTypes.oItems);
            string reslut = String.Empty;
            if (oItems.GetByKey(byteToString(ItemCode)))
            {
                SAPbobsCOM.Recordset oGroups = (SAPbobsCOM.Recordset)SAPCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                string sqlCmd = "SELECT ItmsGrpCod FROM OITB WHERE ItmsGrpNam='"+byteToString(itemGroup)+"' ";
                oGroups.DoQuery(sqlCmd);
                if (oGroups.RecordCount>0)
                {
                    oItems.ItemsGroupCode = int.Parse(oGroups.Fields.Item(0).Value.ToString());
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oGroups);
                }

                oItems.ItemName = byteToString(ItemName);
                if (oItems.Update() == 0)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oItems);
                    reslut= GetJson("0"," 更新成功!:" + byteToString(ItemCode),"0");
                    Context.Response.Headers.Add("ResultMsg", reslut);
                    Context.Response.Write("");
                    Context.ApplicationInstance.CompleteRequest();
                    return;
                }
                else
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oItems);
                    reslut= GetJson("1", " 更新失败!:" + byteToString(ItemCode)+" 错误:"+ SAPCompany.GetLastErrorDescription(),"0");
                    Context.Response.Headers.Add("ResultMsg", reslut);
                    Context.Response.Write("");
                    Context.ApplicationInstance.CompleteRequest();
                    return;
                }
            }
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oItems);
            reslut= GetJson("1", "未找到物料主数据：" + byteToString(ItemCode),"0");
            Context.Response.Headers.Add("ResultMsg", reslut);
            Context.Response.Write("");
            Context.ApplicationInstance.CompleteRequest();
            return;
        }
        public  void GetSAPsqlConn(string connStr)
        {
            JObject ToSAPJson = JObject.Parse(connStr);
            SAPsqlConnStr.DataSource = ((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim();
            SAPsqlConnStr.UserID = ((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim();
            SAPsqlConnStr.Password = Decrypt(((JObject)ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
            SAPsqlConnStr.InitialCatalog = GetSAPDBName(FWSqlConnStr.ConnectionString, shpCode_1);
            SAPsqlConnStr.MaxPoolSize = 512;
            SAPsqlConnection.ConnectionString = SAPsqlConnStr.ConnectionString;
            try
            {
                SAPsqlConnection.Open();
            }
            catch (Exception)
            {
               
            }
            
        }


        public string GetSAPDBName(string  sqlConnStr, string shopCode)
        {
            string sqlCmd = string.Empty;
            if (InterFaceSet == "按分支")
            {
                sqlCmd = "SELECT Value1 FROM ToSAP_Appinit WHERE ParaCode='SAPFIDB'";
            }
            else
            {
                sqlCmd = string.Format("SELECT BranchCode FROM ToSAP_BranchDetail T1  WHERE T1.WhsCode = '{0}'", shopCode);
            }
            DataTable oDataTable = GetDataTable(sqlConnStr, sqlCmd);

            return oDataTable.Rows.Count > 0 ? oDataTable.Rows[0][0].ToString().Trim() : string.Empty;
        }
        public string Decrypt(string str)
        {
            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            byte[] key = Encoding.UTF8.GetBytes("Stray520");

            byte[] data = Convert.FromBase64String(str);

            MemoryStream MStream = new MemoryStream();


            CryptoStream CStream = new CryptoStream(MStream, descsp.CreateDecryptor(key, key), CryptoStreamMode.Write);

            CStream.Write(data, 0, data.Length);

            CStream.FlushFinalBlock();

            return Encoding.UTF8.GetString(MStream.ToArray());
        }
        public static DataTable GetDataTable(string Connstr, string sqlcmd)
        {
            
            using (SqlConnection oConnection = new SqlConnection(Connstr))
            {
               // oConnection.Open();
                DataTable oDataTable = new DataTable("MYdata");
                SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(sqlcmd, oConnection);
                oSqlDataAdapter.SelectCommand.CommandTimeout = 0;
                oSqlDataAdapter.Fill(oDataTable);
                oSqlDataAdapter.Dispose();
                oConnection.Dispose();
                return oDataTable;
            }
        }

        public string byteToString(string ostring)
        {
            byte[] oUserCode1 =LZ4.LZ4Codec.Unwrap(Convert.FromBase64String(ostring));
            string oUserCode2 = System.Text.Encoding.Unicode.GetString(oUserCode1);
            return oUserCode2;
        }

        public string GetJson(string errCode, string errMsg,string sapKey)
        {
            JObject ojson = new JObject();
            ojson.Add("errCode",errCode);
            ojson.Add("errMsg", errMsg);
            string Version1 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ojson.Add("WebVersion", Version1);
            ojson.Add("sapKey",sapKey);
            byte[] content_data = System.Text.Encoding.UTF8.GetBytes(ojson.ToString());
            byte[] wrapBytes = LZ4.LZ4Codec.Wrap(content_data);
            string contentBase64String = System.Convert.ToBase64String(wrapBytes);
            return contentBase64String;
        }
        
        public bool ToSAP_ImportOK(SqlConnection oSqlConnection, string DocType, string DocDescr,
            string DocEntry, string BPLID, string DocDate, string SAPDocType, string SAPDocDescr, string SAPDocEntry,
            string UserCode, string SAPCompany,SqlTransaction oTransaction)
        {
           string sqlCmd = string.Empty;
            try
            {
                 
                sqlCmd =
                    "INSERT ToSAP_ImportOK(DocType, DocDescr, DocEntry, BPLID,DocDate, SAPDocType, SAPDocDescr, SAPDocEntry, UserCode,SAPCompany,CurVersion,ImportDate) VALUES(" +
                    "N'" + DocType + "',N'" + DocDescr + "',N'" + DocEntry + "',N'" + BPLID + "',N'" + DocDate + "',N'" + SAPDocType +
                    "',N'" + SAPDocDescr + "',N'" + SAPDocEntry + "',N'" + UserCode + "',N'" + SAPCompany + "','" +
                     ProgramVersion+ "','"+DateTime.Now.ToString()+"')";
                if (ExecSqlcmd(oTransaction, oSqlConnection, sqlCmd) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\InsertError.Txt", e.Message + Environment.NewLine + sqlCmd);
                ToSAP_ImportErr(oSqlConnection, "插入标识错误", "", BPLID, DateTime.Now.ToString("yyyy-MM-dd"), DocType,
                    DocDescr, DocEntry, DocEntry,
                    DocDate, SAPDocType, e.Message + Environment.NewLine + sqlCmd, oTransaction);
                return false;
            }

            return false;
        }

        public static void ToSAP_ImportErr(SqlConnection oSqlConnection, string ShpCode, string WhsCode, string BPLID,
            string ImportDate, string DocType, string DocDescr, string DocNum, string DocEntry, string DocDate,
            string SAPDocType, string SAPError,SqlTransaction oTran)
        {
            string sqlCmd =
                "INSERT ToSAP_ImportErr(ShpCode, WhsCode,BPLID, ImportDate, DocType, DocDescr, DocNum, DocEntry, DocDate, SAPDocType, SAPError) VALUES(" +
                "N'" + ShpCode + "',N'" + WhsCode + "',N'" + BPLID + "',GetDate(),N'" + DocType + "',N'" + DocDescr +
                "',N'" + DocNum + "',N'" + DocEntry + "',N'" + DocDate + "',N'" + SAPDocType + "',N'" +
                SAPError.Replace("'", "\"") + "')";
            ExecSqlcmd(oTran, oSqlConnection, sqlCmd);
            
        }

        public static void LogFileWrite(string filename, string LogTxt)
        {
            StreamWriter oFileWriter = new StreamWriter(filename, true);
            oFileWriter.Write(LogTxt);
            oFileWriter.Flush();
            oFileWriter.Close();
            oFileWriter.Dispose();
        }
        public static int ExecSqlcmd(SqlTransaction oSqlTransaction, SqlConnection oSqlConnection, string Sqlcmd)
        {
            int Rows = 0;
            SqlCommand oSqlCommand = oSqlConnection.CreateCommand();
            oSqlCommand.CommandTimeout = 3000;
            oSqlCommand.CommandText = Sqlcmd;
            oSqlCommand.Transaction = oSqlTransaction;
            Rows = oSqlCommand.ExecuteNonQuery();
            oSqlCommand.Dispose();
            return Rows;
        }

    }
}

﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Timers;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;
using Quartz;
using Quartz.Collection;
using Quartz.Impl;
using SAPbobsCOM;
using Timer = System.Timers.Timer;

namespace ToSAP
{
    [DisallowConcurrentExecution]
    internal partial class PlanToSAPser : ServiceBase
    {
        public static string LastTime = string.Empty;
        public static StdSchedulerFactory factory;
        public static IScheduler oScheduler;

        public PlanToSAPser()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CreateJob();
            var oTimer = new Timer(300000);
            oTimer.Elapsed += OTimer_Elapsed;oTimer.AutoReset = true;
            oTimer.Enabled = true;
            oTimer.Start();
        }

        private void OTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CreateJob();
        }
        protected override void OnStop()
        {
            oScheduler.Shutdown();
            oScheduler.Clear();
        }

        public static void CreateJob()
        {
            Common.Common.GetFWSqlConn(null);
            Program.oUSER = "Admin";
            Common.Common.GetAppInit(null);
            string sqlCmd = string.Empty;
            if (LastTime == string.Empty)
            {
                sqlCmd = "SELECT * FROM ToSAP_PlanToSAP ";
            }
            else
            {
                sqlCmd = "SELECT  * FROM ToSAP_PlanToSAP (NOLOCK) where changedate>'" + LastTime + "'";
            }
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (!(oDataTable.Rows.Count > 0))
            {
                return;
            }
            var props = new NameValueCollection();
            props.Add("quartz.scheduler.instanceName", "Stray");
            props.Add("quartz.threadPool.threadCount", "1");
            props.Add("quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz");

            factory = new StdSchedulerFactory(props);
            
            oScheduler = factory.GetScheduler();

            oScheduler.Start();

            LastTime = oDataTable.Rows[0]["ChangeDate"].ToString();
            oScheduler.Clear();

            var dictionary = new System.Collections.Generic.Dictionary<IJobDetail, ISet<ITrigger>>();

            foreach (DataRow odataRow in oDataTable.Rows)
            {
                var shpCode = odataRow["WSCode"].ToString();
                var synInt = odataRow["synInt"].ToString();
                var shpCron = odataRow["synTime"].ToString().Trim();
                var shpEntry = odataRow["shpEntry"].ToString();
                var shpName = odataRow["WSName"].ToString();
                var oJobDetail =
                    JobBuilder.Create<dataToSAP>().WithIdentity("Job" + shpCode)
                        .UsingJobData("synInt", synInt)
                        .UsingJobData("shpCode", shpCode)
                        .UsingJobData("shpEntry", shpEntry)
                        .UsingJobData("shpName",shpName)
                        .Build();

                var oTrigger = TriggerBuilder.Create()
                    .WithIdentity("Job" + shpCode + "Trigger", "Job" + shpCode + "Trigger")
                    .WithCronSchedule(shpCron)
                    .ForJob(oJobDetail.Key)
                    .StartNow()
                    .Build();


                var oCollection =
                    new HashSet<ITrigger>(new ITrigger[1] { oTrigger });
                dictionary.Add(oJobDetail, oCollection);
            }

            oScheduler.ScheduleJobs(dictionary, true);
            oDataTable.Dispose();
            GC.Collect();
        }

        [DisallowConcurrentExecution]
        public class dataToSAP : IJob
        {
            public  void Execute(IJobExecutionContext context)
            {
                var shpCode = context.JobDetail.JobDataMap.GetString("shpCode");
                var shpName = context.JobDetail.JobDataMap.GetString("shpName");
                var synInttmp = context.JobDetail.JobDataMap.GetString("synInt");
                var synInt = synInttmp == string.Empty ? 0 : Convert.ToDouble(synInttmp);
                var shpEntry = context.JobDetail.JobDataMap.GetString("shpEntry");
                var BPLID = string.Empty;
                Program.DispWhsShp = shpName.Contains("店铺") ? "店铺" : "仓库";
                if (Program.InterFaceSet == "按分支")
                {
                    string sqlCmd1 =
                        "SELECT T1.BranchName FROM ToSAP_BranchTitle T1 LEFT JOIN ToSAP_BranchDetail T2 ON T2.BranchCode = T1.BranchCode WHERE T2.WhsCode = '" +
                        shpCode + "'";
                    if (Program.DispWhsShp.Equals("店铺") && Program.Shop.Equals("是"))
                    {
                        sqlCmd1 =
                            "SELECT T1.shpName BranchName FROM OSHP T1 WHERE T1.shpCode='" +
                            shpCode + "'";
                    }

                    if ((!Program.DispWhsShp.Equals("店铺")) && Program.Shop.Equals("是"))
                    {
                        sqlCmd1 =
                            "SELECT Value2 BranchName  FROM ToSAP_Appinit T1 WHERE ParaCode='CorpSub'";
                    }

                    var BranchName = Common.Common
                        .GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null).Rows[0][0].ToString();
                    sqlCmd1 = "SELECT BPLId FROM OBPL WHERE BPLName = '" + BranchName + "' AND Disabled = 'N'";
                    try
                    {
                        BPLID = Common.Common
                            .GetDataTable(Program.SAPsqlConnStr.ConnectionString, sqlCmd1, null).Rows[0][0].ToString();
                    }
                    catch (Exception exception)
                    {

                    }

                }


                string sqlCmd =
                    "SELECT T1.*,T2.*,T3.*,T4.DataType,T4.FWName FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface LEFT JOIN ToSAP_FWobject T4 ON T1.ERPobject=T4.FWCode where T3.DIobjType<>'单据'";
                DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                   bool reslut1=  Common.Common.WebLogSAP(shpCode, Program.oUSER, null);
                    var SAPDIinterface = oDataRow["SAPDIinterface"].ToString().Trim();
                    var DiobjType = oDataRow["DIobjType"].ToString().Trim();
                    var SAPDITableName = oDataRow["DTable"].ToString().Trim();
                    var SAPDB = Common.Common.GetSAPDBName(Program.FwSqlConnection, shpCode);
                    var RoleName = oDataRow["ERPobject"] + oDataRow["SAPobject"].ToString();
                    var oCheckEdit = new CheckEdit();
                    oCheckEdit.Name = RoleName;
                    oCheckEdit.Text = oDataRow["FWName"] + "-->" + oDataRow["SAPobjName"];
                    var ERPobject = oDataRow["ERPobject"].ToString();
                    var SAPobject = oDataRow["SAPobject"].ToString();
                    try
                    {
                        DataSet oDataSet;
                        oDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit.Name, SAPDIinterface,
                            shpCode, DiobjType, DiobjType, Program.shpCode, SAPDB, SAPDITableName, null);
                        string result;
                        if (oDataSet.Tables[SAPDITableName].Rows.Count == 0)
                        {
                            continue;
                        }

                        int RowCount = 0;
                        switch (DiobjType)
                        {
                            #region 成本中心

                            case "成本中心":
                                Common.Common.sendDataToWeb(Program.serviceurl, "62", "成本中心", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;

                            #endregion

                            case "仓库":
                                Common.Common.sendDataToWeb(Program.serviceurl, "64", "仓库主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;
                            case "业务伙伴":
                                Common.Common.sendDataToWeb(Program.serviceurl, "2", "业务伙伴主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;
                            case "物料主数据":
                                Common.Common.sendDataToWeb(Program.serviceurl, "4", "物料主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;

                            #region 分支主数据

                            case "分支主数据":
                                sqlCmd =
                                    "SELECT BranchCode KeyField , 'C'+BranchCode Cuskey,'G'+BranchCode Venkey, BranchCode,BranchName," +
                                    "(SELECT BranchCode BPLID, BranchName BPLName,  WhsCode1 DefaultWarehouseID, WhsCode1 DefaultResourceWarehouseID, Country  FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH, ROOT('BusinessPlaces')) busxml," +
                                    "(SELECT WhsCode1 WarehouseCode, WhsName1 WarehouseName FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH,ROOT('Warehouses')) Whsxml," +
                                    "(SELECT CusCode1 CardCode, CusName1 CardName,'C' CardType FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH,ROOT('BusinessPartners')) cusXml," +
                                    "(SELECT VenCode1 CardCode, VenName1 CardName,'S' CardType FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH,ROOT('BusinessPartners')) VenXml" +
                                    "  FROM ToSAP_Branch T1 WHERE IntFace = '" + Program.InterFaceSet + "' AND SHOP='" +
                                    Program.shpCode +
                                    "' and NOT EXISTS(SELECT * FROM ToSAP_ImportOK T00 WHERE  T1.BranchCode=T00.DocEntry AND T00.DocType='Branch' AND SAPDocType = 'OBPL' AND T00.SAPCompany ='" +
                                    SAPDB + "')";
                                oDataSet.Clear();
                                oDataSet.Tables.Remove("OBPL");
                                oDataSet.AcceptChanges();
                                oDataSet.Tables.Add(Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString,
                                    sqlCmd, null));


                                sqlCmd =
                                    " SELECT Value1 KeyField, 'C'+Value1 Cuskey,'G'+Value1 Venkey,Value1 BranchCode,Value2 BranchName," +
                                    "   (SELECT value1 BPLID, Value2 BPLName, value1 DefaultWarehouseID, value1 DefaultResourceWarehouseID, 'CN' Country  FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH, ROOT('BusinessPlaces')) busxml," +
                                    "(SELECT value1 WarehouseCode, Value2 WarehouseName FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH,ROOT('Warehouses')) Whsxml," +
                                    "(SELECT 'K' + value1 CardCode,Value2 CardName,'C' CardType FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH,ROOT('BusinessPartners')) cusXml," +
                                    "(SELECT 'G' + value1 CardCode,Value2 CardName,'S' CardType FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH,ROOT('BusinessPartners')) VenXml" +
                                    "   FROM ToSAP_Appinit T1 WHERE T1.ParaCode = 'CorpSub' and NOT EXISTS(SELECT * FROM ToSAP_ImportOK T00 WHERE  T1.Value1=T00.DocEntry AND T00.DocType='Branch' AND SAPDocType = 'OBPL' AND T00.SAPCompany ='" +
                                    SAPDB + "')";
                                DataTable oDataTable1 =
                                    Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                                if (oDataTable.Rows.Count != 0)
                                {
                                    oDataSet.Tables[0].Merge(oDataTable1);
                                    oDataSet.AcceptChanges();
                                }

                                if (oDataSet.Tables[0].Rows.Count == 0)
                                {
                                    continue;
                                }

                                oDataSet.Tables[0].Columns["Whsxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].TableName = "OWHS";
                                oDataSet.AcceptChanges();
                                oCheckEdit.Text = "分支主数据-->仓库";

                                Common.Common.sendDataToWeb(Program.serviceurl, "64", "仓库主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);

                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Whsxml";
                                oDataSet.Tables[0].Columns["Cusxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].Columns["KeyField"].ColumnName = "OLDKey";
                                oDataSet.Tables[0].Columns["Cuskey"].ColumnName = "KeyField";
                                oDataSet.Tables[0].TableName = "OCRD";
                                oDataSet.AcceptChanges();

                                oCheckEdit.Text = "分支主数据-->客户";
                                Common.Common.sendDataToWeb(Program.serviceurl, "2", "业务伙伴主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);

                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Cusxml";
                                oDataSet.Tables[0].Columns["Venxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].Columns["KeyField"].ColumnName = "Cuskey";
                                oDataSet.Tables[0].Columns["Venkey"].ColumnName = "KeyField";
                                oDataSet.AcceptChanges();


                                oCheckEdit.Text = "分支主数据-->供应商";
                                Common.Common.sendDataToWeb(Program.serviceurl, "2", "业务伙伴主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);

                                oDataSet.Tables[0].Columns["KeyField"].ColumnName = "Venkey";
                                oDataSet.Tables[0].Columns["OLDKey"].ColumnName = "KeyField";
                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Venxml";
                                oDataSet.Tables[0].Columns["Busxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].TableName = "OBPL";
                                oDataSet.AcceptChanges();
                                oCheckEdit.Text = "分支主数据-->分支主数据";

                                Common.Common.sendDataToWeb(Program.serviceurl, "247", "分支主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);

                                #endregion

                                break;
                            case "物料组":
                                Common.Common.sendDataToWeb(Program.serviceurl, "52", "物料组", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;
                        }

                        oDataSet.Dispose();
                        GC.Collect();

                    }
                    catch (Exception e)
                    {
                        Common.Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, null, string.Empty,
                            DateTime.Now.ToString("yyyy-MM-dd"), ERPobject, null, null, null, null, SAPobject,
                            "店铺或仓库:" + shpCode + "导入出错" + e);
                        Common.Common.weblogOutSAP(shpCode, Program.oUSER);
                        GC.Collect();
                    }
                }

                string oContion = "T3.DIobjType='单据' ";
                if (Program.DocToSAP == "零售单")
                {
                    oContion = " and T4.MasterTb<>'OSAL' AND " + oContion + "UNION ALL " +
                               " SELECT TOP 1 T1.DocEntry,'OSAL' ERPobject,'ToSAP' SAPobject,T1.Contion,T1.SumImport,T1.IsSum,T1.IsCollect,T1.checkCost,T1.ToOrder,T2.*,T3.*,T4.DataType,T4.FWName FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject = T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface = T3.DIInterface LEFT JOIN ToSAP_FWobject T4 ON T1.ERPobject = T4.FWCode WHERE T3.DIobjType='单据' and t4.MasterTb = 'OSAL'";

                }
                oContion+= " ORDER BY T1.ToOrder ";
                sqlCmd =
                    "SELECT T1.*,T2.*,T3.*,T4.DataType,T4.FWName FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface LEFT JOIN ToSAP_FWobject T4 ON T1.ERPobject=T4.FWCode where 1=1 "+oContion;
                oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                  bool result1=   Common.Common.WebLogSAP(shpCode, Program.oUSER, null);
                    var DiobjType = oDataRow["DIobjType"].ToString().Trim();
                    var RoleName = oDataRow["ERPobject"] + oDataRow["SAPobject"].ToString();
                    var oCheckEdit = new CheckEdit();
                    oCheckEdit.Name = RoleName;
                    oCheckEdit.Text = oDataRow["FWName"] + "-->" + oDataRow["SAPobjName"];
                    var stDate1 = Convert.ToDateTime("2000-01-01");
                    var enDate1 = DateTime.Now.AddDays(-1 * synInt);
                    var SAPDB = Common.Common.GetSAPDBName(Program.FwSqlConnection, shpCode);
                    string SAPDI = "", SAPDITableName = "", ERPobject = "", SAPobject = "", SAPDIinterface = "";
                    DataSet oDataSet = new DataSet();
                    if (Program.DocToSAP.Equals("零售单") && oCheckEdit.Name.Equals("OSALToSAP"))
                    {
                        SAPDI = "Documents";
                        SAPDITableName = "OSAL";
                        string sqlCmd1 =
                            "SELECT T1.ERPobject+T1.SAPobject oChedit, T1.* FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_Fwobject T2 ON t1.ERPobject=T2.FWCode WHERE T2.MasterTb='OSAL'";
                        DataTable tmpTable =
                            Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null);

                        foreach (DataRow oDataRow1 in tmpTable.Rows)
                        {
                            sqlCmd1 = "SELECT * FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface WHERE ERPobject+SAPobject = '" +
                                      oDataRow1["oChedit"] + "'";
                            DataTable oMasDt1 = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null);
                            oCheckEdit.Name = oDataRow1["oChedit"].ToString();
                            DataSet tmpDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit, shpCode,
                                Program.InterFaceSet, Program.CorpSet, Program.shpCode, stDate1.ToString("yyyy-MM-dd"),
                                enDate1.ToString("yyyy-MM-dd"), shpEntry, BPLID, null, SAPDITableName,"");

                            oDataSet.Merge(tmpDataSet, true);
                            oDataSet.AcceptChanges();
                        }

                        oCheckEdit.Name = "OSALToSAP";
                    }
                    else{
                         sqlCmd =
                            "SELECT * FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface WHERE ERPobject+SAPobject = '" +
                            oCheckEdit.Name + "'";
                        var oMasDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                        SAPDIinterface = oMasDt.Rows[0]["SAPDIinterface"].ToString().Trim();
                        SAPDI = oMasDt.Rows[0]["DI"].ToString().Trim();

                        SAPDITableName = oMasDt.Rows[0]["DTable"].ToString().Trim();
                        ERPobject = oMasDt.Rows[0]["ERPobject"].ToString().Trim();
                        SAPobject = oMasDt.Rows[0]["SAPobject"].ToString().Trim();

                        oDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit, shpCode,
                            Program.InterFaceSet, Program.CorpSet, Program.shpCode, stDate1.ToString("yyyy-MM-dd"),
                            enDate1.ToString("yyyy-MM-dd"), shpEntry, BPLID, null, null,"");
                    }

                    if (oDataSet.Tables[0].Rows.Count == 0)
                    {
                        continue;
                    }

                    try
                    {
                        int objecttype = 0;
                        switch (SAPDI)
                        {
                            case "Documents":
                                if (!SAPDIinterface.Equals(""))
                                {
                                    BoObjectTypes oBoObjectTypes = (BoObjectTypes)Enum.Parse(typeof(BoObjectTypes), SAPDIinterface);
                                    objecttype = (int)(oBoObjectTypes);
                                }

                                Common.Common.sendDataToWeb(Program.serviceurl, objecttype.ToString(), "单据", oDataSet, shpCode, shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;
                            case "StockTransfer":
                                Common.Common.sendDataToWeb(Program.serviceurl, objType: "67", "库存转储", oDataSet, shpCode, shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, null,BPLID);
                                break;
                        }
                        oDataSet.Dispose();
                    }
                    catch (Exception e1)
                    {
                        Common.Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, null, string.Empty,
                            DateTime.Now.ToString("yyyy-MM-dd"), ERPobject, null, null, null, null, SAPobject,
                            "店铺或仓库:" + shpCode + "导入出错" + e1);
                        Common.Common.weblogOutSAP(shpCode,Program.oUSER);
                        GC.Collect();
                    }
                    
                }
                Common.Common.weblogOutSAP(shpCode,Program.oUSER);
                    GC.Collect();
                }

            }
        }
    }


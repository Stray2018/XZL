﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ToSAP.Init;

namespace ToSAP.Set
{
    public partial class mSet : XtraForm
    {
        public mSet()
        {
            InitializeComponent();
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void mSet_Load(object sender, EventArgs e)
        {
            //SAPbobsCOM.Company SAPCompany = new CompanyClass();
            FWDB.Enabled = false;
           // SAPDB.Enabled = false;
            Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
            var oFileInfo = new FileInfo(Application.StartupPath + "/ToSAP.ini");
            if (oFileInfo.Exists)
            {
                oFileInfo = null;
                var oFileReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
                var ToSAPstr = oFileReader.ReadToEnd();
                oFileReader.Close();
                oFileReader.Dispose();
                var ToSAPCon = JObject.Parse(ToSAPstr);

                FWDBserver.Text = ((JObject) ToSAPCon["FW"])["FWDBserver"].ToString();
                FWDBuser.Text = ((JObject) ToSAPCon["FW"])["FWDBuser"].ToString();
                FWDBpwd.Text = Common.Common.Decrypt(((JObject) ToSAPCon["FW"])["FWDBpwd"].ToString());
                FWDB.Text = ((JObject) ToSAPCon["FW"])["FWDB"].ToString();
                SAPLicensesever.Text = ((JObject) ToSAPCon["SAP"])["SAPLicensesever"].ToString();
                SAPDBserver.Text = ((JObject) ToSAPCon["SAP"])["SAPDBserver"].ToString();
                SAPDBTYPE.Text = ((JObject) ToSAPCon["SAP"])["SAPDBTYPE"].ToString();
                //SAPCompany.Server = SAPDBserver.Text.Trim();
                SAPDBuser.Text = ((JObject) ToSAPCon["SAP"])["SAPDBuser"].ToString();
                SAPDBpwd.Text = Common.Common.Decrypt(((JObject) ToSAPCon["SAP"])["SAPDBpwd"].ToString());
                if (SAPLicensesever.Text.Equals("") || SAPDBTYPE.Text.Equals("") || SAPDBserver.Text.Equals("") ||
                    SAPDBserver.Text.Equals("") || SAPDBuser.Text.Equals("") || SAPDBpwd.Text.Equals(""))
                {
                    return;
                }
                //switch (SAPDBTYPE.Text.Trim())
                //{
                //    case "SQL2008":
                //        SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                //        break;
                //    case "SQL2012":
                //        SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                //        break;
                //    case "SQL2014":
                //        SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                //        break;
                //    case "SQL2016":
                //        SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                //        break;
                //    default:
                //        SAPCompany.DbServerType = BoDataServerTypes.dst_HANADB;
                //        break;
                //}

                //SAPCompany.DbPassword = SAPDBpwd.Text;
                //SAPCompany.DbUserName = SAPDBuser.Text;
                //SAPDB.Properties.DisplayMember = "数据库";
                //SAPDB.Properties.ValueMember = "数据库";
                //SAPDB.AutoSizeInLayoutControl = true;

                //SAPDB.Properties.DataSource =Common.Common.GetSAPDB(SAPCompany);
                SAPDB.Properties.DisplayMember = "数据库";
                SAPDB.Properties.ValueMember = "数据库";
                SAPDB.AutoSizeInLayoutControl = true;

                
                JObject ToSAPJson = JObject.Parse(ToSAPstr);
                string serUrl = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
                if (serUrl.Contains(","))
                {
                    serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
                }

                 string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";
                //string serviceurl = "http://127.0.0.1:2899/WebToSAP.asmx";
                Dictionary<string, string> oDictionary = new Dictionary<string, string>();
                oDictionary.Add("ToSAPini", ToSAPstr);

                string importxml = Common.Common.postString("GetSAPDB", oDictionary,null);
                string DBString = Common.Common.soapPost(serviceurl, importxml, "Y", "")["errMsg"].ToString();
                SAPDB.Properties.DataSource = JsonConvert.DeserializeObject(DBString, typeof(DataTable));
                SAPDB.EditValue = ((JObject) ToSAPCon["SAP"])["SAPDB"].ToString();


                SAPUSER.Text = ((JObject) ToSAPCon["SAP"])["SAPUSER"].ToString();
                SAPPWD.Text = Common.Common.Decrypt(((JObject) ToSAPCon["SAP"])["SAPPWD"].ToString());
            }
        }

        private void SQLConnTest_Click(object sender, EventArgs e)
        {
            if (Program.FwSqlConnection.State == ConnectionState.Open)
            {
                Program.FwSqlConnection.Close();
                Program.FwSqlConnection.Dispose();
            }

            
            Program.FWSqlConnStr.DataSource = @FWDBserver.Text.Trim();
            Program.FWSqlConnStr.InitialCatalog = "MASTER";
            Program.FWSqlConnStr.UserID = FWDBuser.Text.Trim();
            Program.FWSqlConnStr.Password = FWDBpwd.Text.Trim();

            Program.FwSqlConnection.ConnectionString = Program.FWSqlConnStr.ConnectionString;
            try
            {
                Program.FwSqlConnection.Open();
                if (Program.FwSqlConnection.State == ConnectionState.Open)
                {
                    MessageBox.Show("SQL 服务器连接成功!");
                    FWDB.Enabled = true;
                    FWDB.Properties.Items.Clear();
                    DataTable oDataTable = Common.Common.GetFWDB(Program.FwSqlConnection);
                    foreach (DataRow companylist in oDataTable.Rows)
                    {
                        FWDB.Properties.Items.Add(companylist[1]);
                    }
                    Program.FwSqlConnection.Close();
                    Program.FwSqlConnection.Dispose();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("SQL连接失败!" + "\n" + exception.Message);
                FWDB.Enabled = false;
            }
        }

        private void FWDB_Click(object sender, EventArgs e)
        {
        }

        private void FWDB_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void SAPDB_Click(object sender, EventArgs e)
        {
        }

        private void SAPConnTest_Click(object sender, EventArgs e)
        {
          saveConfigIni();
            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            string serUrl = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            if (serUrl.Contains(","))
            {
                serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
            }

            string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            oDictionary.Add("ToSAPini", connStr);

            string importxml =Common.Common.postString("SAPConnectTest", oDictionary,null);  //Logon To SAP
            string LogTosapResult = Common.Common.soapPost(serviceurl, importxml, "Y", null)["errMsg"].ToString();
            MessageBox.Show(LogTosapResult);
        }
        private void SAPDB_MouseClick(object sender, MouseEventArgs e)
        {
            string FWDBserver = this.FWDBserver.Text.Trim();
            string FWDBuser = this.FWDBuser.Text.Trim();
            string FWDBpwd = this.FWDBpwd.Text.Trim();
            string FWDB = this.FWDB.Text.Trim();
            string SAPDBTYPE1 = SAPDBTYPE.Text.Trim();
            string SAPLicensesever = this.SAPLicensesever.Text.Trim();
            string SAPDBserver = this.SAPDBserver.Text.Trim();
            string SAPDBuser = this.SAPDBuser.Text.Trim();
            string SAPDBpwd = this.SAPDBpwd.Text.Trim();
            string SAPDB1 = this.SAPDB.Text.Trim();
            string SAPUSER=this.SAPUSER.Text.Trim();
            string SAPPWD = this.SAPPWD.Text.Trim();
            Common.Common.saveConfigIni(FWDBserver,FWDBuser,FWDBpwd,FWDB,SAPDBTYPE1,SAPLicensesever,SAPDBserver,SAPDBuser,SAPDBpwd,SAPDB1,SAPUSER,SAPPWD);
            SAPDB.Properties.DisplayMember = "数据库";
            SAPDB.Properties.ValueMember = "数据库";
            SAPDB.AutoSizeInLayoutControl = true;

            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            string serUrl = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            if (serUrl.Contains(","))
            {
                serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
            }

     
           string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";
            //string serviceurl = "http://127.0.0.1:2899/WebToSAP.asmx";
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            oDictionary.Add("ToSAPini", connStr);

            string importxml = Common.Common.postString("GetSAPDB", oDictionary,null); 
            string DBString = Common.Common.soapPost(serviceurl, importxml, "Y", "")["errMsg"].ToString();
            SAPDB.Properties.DataSource = JsonConvert.DeserializeObject(DBString, typeof(DataTable));

        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            saveConfigIni();
           MessageBox.Show("配置保存成功!" + Environment.NewLine + "继续初始化数据库......");
            var oMessageForm = new ShowMessageForm();
            oMessageForm.ShowDialog(this);
            Close();
        }

        private void saveConfigIni()
        {
            var oFileInfo = new FileInfo(Application.StartupPath + "/ToSAP.ini");
            if (oFileInfo.Exists)
            {
                oFileInfo.Delete();
            }
            oFileInfo = null;
            JObject ToSAPCon = new JObject();
            ToSAPCon.Add(
                new JProperty("FW",
                    new JObject(
                        new JProperty("FWDBserver", FWDBserver.Text.Trim()),
                        new JProperty("FWDBuser", FWDBuser.Text.Trim()),
                        new JProperty("FWDBpwd", Common.Common.Encrypt(FWDBpwd.Text.Trim())),
                        new JProperty("FWDB", FWDB.Text.Trim())
                    )
                )
            );
            ToSAPCon.Add(
                new JProperty("SAP",
                    new JObject(
                        new JProperty("SAPDBTYPE", SAPDBTYPE.Text.Trim()),
                        new JProperty("SAPDBserver", SAPDBserver.Text.Trim()),
                        new JProperty("SAPDBuser", SAPDBuser.Text.Trim()),
                        new JProperty("SAPDBpwd", Common.Common.Encrypt(SAPDBpwd.Text.Trim())),
                        new JProperty("SAPLicensesever", SAPLicensesever.Text.Trim()),
                        new JProperty("SAPDB", SAPDB.EditValue),
                        new JProperty("SAPUSER", SAPUSER.Text.Trim()),
                        new JProperty("SAPPWD", Common.Common.Encrypt(SAPPWD.Text.Trim()))
                    ))
            );

            var oFileWriter = new StreamWriter(Application.StartupPath + "/ToSAP.ini");
            oFileWriter.Write(ToSAPCon.ToString());
            oFileWriter.Flush();
            oFileWriter.Close();
            oFileWriter.Dispose();
        }
    }
}

﻿namespace ToSAP.Set
{
    partial class mSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.CancelOK = new System.Windows.Forms.Button();
            this.saveOK = new System.Windows.Forms.Button();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.SAPConnTest = new System.Windows.Forms.Button();
            this.SAPLicensesever = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.SAPUSER = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.SAPPWD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.SAPDBserver = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.SAPDBTYPE = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SAPDBuser = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.SAPDBpwd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.SAPDB = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.FWDB = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SQLConnTest = new System.Windows.Forms.Button();
            this.FWDBuser = new DevExpress.XtraEditors.TextEdit();
            this.FWDBpwd = new DevExpress.XtraEditors.TextEdit();
            this.FWDBserver = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SAPLicensesever.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPUSER.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPPWD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBserver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBTYPE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBuser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBpwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FWDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FWDBuser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FWDBpwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FWDBserver.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.CancelOK);
            this.groupControl1.Controls.Add(this.saveOK);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Location = new System.Drawing.Point(6, -4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(604, 324);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // CancelOK
            // 
            this.CancelOK.Location = new System.Drawing.Point(333, 276);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(75, 23);
            this.CancelOK.TabIndex = 3;
            this.CancelOK.Text = "关闭";
            this.CancelOK.UseVisualStyleBackColor = true;
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // saveOK
            // 
            this.saveOK.Location = new System.Drawing.Point(158, 276);
            this.saveOK.Name = "saveOK";
            this.saveOK.Size = new System.Drawing.Size(75, 23);
            this.saveOK.TabIndex = 2;
            this.saveOK.Text = "保存";
            this.saveOK.UseVisualStyleBackColor = true;
            this.saveOK.Click += new System.EventHandler(this.saveOK_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.SAPConnTest);
            this.groupControl3.Controls.Add(this.SAPLicensesever);
            this.groupControl3.Controls.Add(this.labelControl12);
            this.groupControl3.Controls.Add(this.SAPUSER);
            this.groupControl3.Controls.Add(this.labelControl11);
            this.groupControl3.Controls.Add(this.SAPPWD);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.SAPDBserver);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.SAPDBTYPE);
            this.groupControl3.Controls.Add(this.SAPDBuser);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.SAPDBpwd);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.SAPDB);
            this.groupControl3.Location = new System.Drawing.Point(6, 110);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(566, 144);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "SAP 系统信息";
            // 
            // SAPConnTest
            // 
            this.SAPConnTest.Location = new System.Drawing.Point(474, 24);
            this.SAPConnTest.Name = "SAPConnTest";
            this.SAPConnTest.Size = new System.Drawing.Size(75, 23);
            this.SAPConnTest.TabIndex = 20;
            this.SAPConnTest.Text = "连接测试";
            this.SAPConnTest.UseVisualStyleBackColor = true;
            this.SAPConnTest.Click += new System.EventHandler(this.SAPConnTest_Click);
            // 
            // SAPLicensesever
            // 
            this.SAPLicensesever.Location = new System.Drawing.Point(308, 25);
            this.SAPLicensesever.Name = "SAPLicensesever";
            this.SAPLicensesever.Size = new System.Drawing.Size(129, 20);
            this.SAPLicensesever.TabIndex = 11;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(225, 28);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(76, 14);
            this.labelControl12.TabIndex = 16;
            this.labelControl12.Text = "许可证服务器:";
            // 
            // SAPUSER
            // 
            this.SAPUSER.Location = new System.Drawing.Point(307, 91);
            this.SAPUSER.Name = "SAPUSER";
            this.SAPUSER.Size = new System.Drawing.Size(64, 20);
            this.SAPUSER.TabIndex = 15;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(6, 94);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(52, 14);
            this.labelControl11.TabIndex = 10;
            this.labelControl11.Text = "获取帐套:";
            // 
            // SAPPWD
            // 
            this.SAPPWD.Location = new System.Drawing.Point(461, 91);
            this.SAPPWD.Name = "SAPPWD";
            this.SAPPWD.Properties.PasswordChar = '*';
            this.SAPPWD.Size = new System.Drawing.Size(100, 20);
            this.SAPPWD.TabIndex = 16;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(224, 94);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(66, 14);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "SAP 用户名:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(387, 94);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(54, 14);
            this.labelControl10.TabIndex = 17;
            this.labelControl10.Text = "SAP 密码:";
            // 
            // SAPDBserver
            // 
            this.SAPDBserver.Location = new System.Drawing.Point(88, 57);
            this.SAPDBserver.Name = "SAPDBserver";
            this.SAPDBserver.Size = new System.Drawing.Size(129, 20);
            this.SAPDBserver.TabIndex = 12;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(5, 60);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(76, 14);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "数据库服务器:";
            // 
            // SAPDBTYPE
            // 
            this.SAPDBTYPE.Location = new System.Drawing.Point(89, 25);
            this.SAPDBTYPE.Name = "SAPDBTYPE";
            this.SAPDBTYPE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SAPDBTYPE.Properties.Items.AddRange(new object[] {
            "SQL2008",
            "SQL2012",
            "SQL2014",
            "SQL2016",
            "HANA"});
            this.SAPDBTYPE.Size = new System.Drawing.Size(129, 20);
            this.SAPDBTYPE.TabIndex = 13;
            // 
            // SAPDBuser
            // 
            this.SAPDBuser.Location = new System.Drawing.Point(307, 57);
            this.SAPDBuser.Name = "SAPDBuser";
            this.SAPDBuser.Size = new System.Drawing.Size(64, 20);
            this.SAPDBuser.TabIndex = 13;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(8, 24);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(64, 14);
            this.labelControl7.TabIndex = 7;
            this.labelControl7.Text = "数据库版本:";
            // 
            // SAPDBpwd
            // 
            this.SAPDBpwd.Location = new System.Drawing.Point(461, 57);
            this.SAPDBpwd.Name = "SAPDBpwd";
            this.SAPDBpwd.Properties.PasswordChar = '*';
            this.SAPDBpwd.Size = new System.Drawing.Size(100, 20);
            this.SAPDBpwd.TabIndex = 14;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(224, 60);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(76, 14);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "数据库用户名:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(387, 60);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(64, 14);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "数据库密码:";
            // 
            // SAPDB
            // 
            this.SAPDB.Location = new System.Drawing.Point(89, 91);
            this.SAPDB.Name = "SAPDB";
            this.SAPDB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SAPDB.Properties.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.SAPDB.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SAPDB.Properties.NullText = "";
            this.SAPDB.Properties.PopupResizeMode = DevExpress.XtraEditors.Controls.ResizeMode.LiveResize;
            this.SAPDB.Size = new System.Drawing.Size(129, 20);
            this.SAPDB.TabIndex = 11;
            this.SAPDB.Click += new System.EventHandler(this.SAPDB_Click);
            this.SAPDB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SAPDB_MouseClick);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.FWDB);
            this.groupControl2.Controls.Add(this.SQLConnTest);
            this.groupControl2.Controls.Add(this.FWDBuser);
            this.groupControl2.Controls.Add(this.FWDBpwd);
            this.groupControl2.Controls.Add(this.FWDBserver);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Location = new System.Drawing.Point(6, 24);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(566, 80);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Forwin 系统信息";
            // 
            // FWDB
            // 
            this.FWDB.Location = new System.Drawing.Point(89, 57);
            this.FWDB.Name = "FWDB";
            this.FWDB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FWDB.Size = new System.Drawing.Size(129, 20);
            this.FWDB.TabIndex = 9;
            this.FWDB.Click += new System.EventHandler(this.FWDB_Click);
            this.FWDB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FWDB_MouseClick);
            // 
            // SQLConnTest
            // 
            this.SQLConnTest.Location = new System.Drawing.Point(225, 54);
            this.SQLConnTest.Name = "SQLConnTest";
            this.SQLConnTest.Size = new System.Drawing.Size(75, 26);
            this.SQLConnTest.TabIndex = 8;
            this.SQLConnTest.Text = "连接测试";
            this.SQLConnTest.UseVisualStyleBackColor = true;
            this.SQLConnTest.Click += new System.EventHandler(this.SQLConnTest_Click);
            // 
            // FWDBuser
            // 
            this.FWDBuser.Location = new System.Drawing.Point(307, 28);
            this.FWDBuser.Name = "FWDBuser";
            this.FWDBuser.Size = new System.Drawing.Size(64, 20);
            this.FWDBuser.TabIndex = 5;
            // 
            // FWDBpwd
            // 
            this.FWDBpwd.Location = new System.Drawing.Point(461, 28);
            this.FWDBpwd.Name = "FWDBpwd";
            this.FWDBpwd.Properties.PasswordChar = '*';
            this.FWDBpwd.Size = new System.Drawing.Size(100, 20);
            this.FWDBpwd.TabIndex = 6;
            // 
            // FWDBserver
            // 
            this.FWDBserver.Location = new System.Drawing.Point(89, 28);
            this.FWDBserver.Name = "FWDBserver";
            this.FWDBserver.Size = new System.Drawing.Size(129, 20);
            this.FWDBserver.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(387, 31);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(64, 14);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "数据库密码:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(224, 31);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(76, 14);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "数据库用户名:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 60);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "获取数据库:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "数据库服务器:";
            // 
            // mSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 327);
            this.Controls.Add(this.groupControl1);
            this.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "mSet";
            this.Text = "数据库配置";
            this.Load += new System.EventHandler(this.mSet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SAPLicensesever.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPUSER.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPPWD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBserver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBTYPE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBuser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDBpwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FWDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FWDBuser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FWDBpwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FWDBserver.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Button SQLConnTest;
        private DevExpress.XtraEditors.TextEdit FWDBuser;
        private DevExpress.XtraEditors.TextEdit FWDBpwd;
        private DevExpress.XtraEditors.TextEdit FWDBserver;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit SAPDBTYPE;
        private DevExpress.XtraEditors.TextEdit SAPDBuser;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit SAPDBpwd;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit FWDB;
        private System.Windows.Forms.Button SAPConnTest;
        private DevExpress.XtraEditors.TextEdit SAPLicensesever;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit SAPUSER;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit SAPPWD;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit SAPDBserver;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private System.Windows.Forms.Button CancelOK;
        private System.Windows.Forms.Button saveOK;
        private DevExpress.XtraEditors.LookUpEdit SAPDB;
    }
}
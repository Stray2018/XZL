﻿namespace ToSAP
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.mInit = new DevExpress.XtraBars.BarSubItem();
            this.AppInit = new DevExpress.XtraBars.BarButtonItem();
            BranchSet = new DevExpress.XtraBars.BarButtonItem();
            this.FWsynObjectSet = new DevExpress.XtraBars.BarButtonItem();
            this.FWobjData = new DevExpress.XtraBars.BarButtonItem();
            this.SAPsynObjectSet = new DevExpress.XtraBars.BarButtonItem();
            this.SAPDIData = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.FWToSAPSet = new DevExpress.XtraBars.BarButtonItem();
            this.ToSAPUserSet = new DevExpress.XtraBars.BarButtonItem();
            this.PlanToSAP = new DevExpress.XtraBars.BarButtonItem();
            this.mQuery = new DevExpress.XtraBars.BarSubItem();
            this.iRoleNSYN = new DevExpress.XtraBars.BarButtonItem();
            this.iQuery = new DevExpress.XtraBars.BarButtonItem();
            this.mSet = new DevExpress.XtraBars.BarButtonItem();
            this.ToSYN = new DevExpress.XtraBars.BarButtonItem();
            this.CheckUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.iWXP = new DevExpress.XtraBars.BarCheckItem();
            this.iOffXP = new DevExpress.XtraBars.BarCheckItem();
            this.iOff2K = new DevExpress.XtraBars.BarCheckItem();
            this.iOff2003 = new DevExpress.XtraBars.BarCheckItem();
            this.iDefault = new DevExpress.XtraBars.BarCheckItem();
            this.iDocNSYN = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabbedMdiManager2 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager2)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ToSYN,
            this.mInit,
            this.iWXP,
            this.iOffXP,
            this.iOff2K,
            this.iOff2003,
            this.iDefault,
            this.mSet,
            BranchSet,
            this.FWsynObjectSet,
            this.SAPDIData,
            this.FWToSAPSet,
            this.PlanToSAP,
            this.iDocNSYN,
            this.iRoleNSYN,
            this.iQuery,
            this.mQuery,
            this.barButtonItem1,
            this.AppInit,
            this.ToSAPUserSet,
            this.FWobjData,
            this.SAPsynObjectSet,
            this.CheckUpdate,
            this.barButtonItem2});
            this.barManager.MainMenu = this.bar1;
            this.barManager.MaxItemId = 47;
            this.barManager.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mInit),
            new DevExpress.XtraBars.LinkPersistInfo(this.mQuery),
            new DevExpress.XtraBars.LinkPersistInfo(this.mSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToSYN),
            new DevExpress.XtraBars.LinkPersistInfo(this.CheckUpdate)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // mInit
            // 
            this.mInit.Caption = "初始化";
            this.mInit.Id = 1;
            this.mInit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.AppInit),
            new DevExpress.XtraBars.LinkPersistInfo(BranchSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.FWsynObjectSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.FWobjData),
            new DevExpress.XtraBars.LinkPersistInfo(this.SAPsynObjectSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.SAPDIData),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.FWToSAPSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToSAPUserSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.PlanToSAP)});
            this.mInit.Name = "mInit";
            // 
            // AppInit
            // 
            this.AppInit.Caption = "初始化配置";
            this.AppInit.Id = 8;
            this.AppInit.Name = "AppInit";
            this.AppInit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AppInit_ItemClick);
            // 
            // BranchSet
            // 
            BranchSet.Caption = "分支门店配置";
            BranchSet.Id = 32;
            BranchSet.Name = "BranchSet";
            BranchSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BranchSet_ItemClick);
            // 
            // FWsynObjectSet
            // 
            this.FWsynObjectSet.Caption = "ERP 数据对象设置";
            this.FWsynObjectSet.Id = 33;
            this.FWsynObjectSet.Name = "FWsynObjectSet";
            this.FWsynObjectSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FWsynObjectSet_ItemClick);
            // 
            // FWobjData
            // 
            this.FWobjData.Caption = "ERP 数据字段设置";
            this.FWobjData.Id = 43;
            this.FWobjData.Name = "FWobjData";
            this.FWobjData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FWobjData_ItemClick);
            // 
            // SAPsynObjectSet
            // 
            this.SAPsynObjectSet.Caption = "SAP DI对象设置";
            this.SAPsynObjectSet.Id = 44;
            this.SAPsynObjectSet.Name = "SAPsynObjectSet";
            this.SAPsynObjectSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SAPsynObjectSet_ItemClick);
            // 
            // SAPDIData
            // 
            this.SAPDIData.Caption = "SAP DI 对象属性设置";
            this.SAPDIData.Id = 34;
            this.SAPDIData.Name = "SAPDIData";
            this.SAPDIData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SAPDIData_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "品类_物料组对应设置";
            this.barButtonItem2.Id = 46;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // FWToSAPSet
            // 
            this.FWToSAPSet.Caption = "同步逻辑设置";
            this.FWToSAPSet.Id = 35;
            this.FWToSAPSet.Name = "FWToSAPSet";
            this.FWToSAPSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FWToSAPSet_ItemClick);
            // 
            // ToSAPUserSet
            // 
            this.ToSAPUserSet.Caption = "同步帐号设置";
            this.ToSAPUserSet.Id = 42;
            this.ToSAPUserSet.Name = "ToSAPUserSet";
            this.ToSAPUserSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ToSAPUserSet_ItemClick);
            // 
            // PlanToSAP
            // 
            this.PlanToSAP.Caption = "自动同步设置";
            this.PlanToSAP.Id = 36;
            this.PlanToSAP.Name = "PlanToSAP";
            this.PlanToSAP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PlanToSAP_ItemClick);
            // 
            // mQuery
            // 
            this.mQuery.Caption = "数据查询";
            this.mQuery.Id = 37;
            this.mQuery.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.iRoleNSYN),
            new DevExpress.XtraBars.LinkPersistInfo(this.iQuery)});
            this.mQuery.Name = "mQuery";
            // 
            // iRoleNSYN
            // 
            this.iRoleNSYN.Caption = "未同步业务查询";
            this.iRoleNSYN.Id = 39;
            this.iRoleNSYN.Name = "iRoleNSYN";
            this.iRoleNSYN.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iRoleNSYN_ItemClick);
            // 
            // iQuery
            // 
            this.iQuery.Caption = "同步错误查询";
            this.iQuery.Id = 40;
            this.iQuery.Name = "iQuery";
            this.iQuery.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iQuery_ItemClick);
            // 
            // mSet
            // 
            this.mSet.Caption = "数据库配置";
            this.mSet.Id = 31;
            this.mSet.Name = "mSet";
            this.mSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mSet_ItemClick);
            // 
            // ToSYN
            // 
            this.ToSYN.Caption = "数据同步";
            this.ToSYN.Id = 0;
            this.ToSYN.Name = "ToSYN";
            this.ToSYN.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ToSYN_ItemClick);
            // 
            // CheckUpdate
            // 
            this.CheckUpdate.Caption = "检查更新";
            this.CheckUpdate.Id = 45;
            this.CheckUpdate.Name = "CheckUpdate";
            this.CheckUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.CheckUpdate_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 438);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 414);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 414);
            // 
            // iWXP
            // 
            this.iWXP.Caption = "WindowsXP";
            this.iWXP.Description = "WindowsXP";
            this.iWXP.Id = 25;
            this.iWXP.Name = "iWXP";
            // 
            // iOffXP
            // 
            this.iOffXP.Caption = "OfficeXP";
            this.iOffXP.Description = "OfficeXP";
            this.iOffXP.Id = 26;
            this.iOffXP.Name = "iOffXP";
            // 
            // iOff2K
            // 
            this.iOff2K.Caption = "Office2000";
            this.iOff2K.Description = "Office2000";
            this.iOff2K.Id = 27;
            this.iOff2K.Name = "iOff2K";
            // 
            // iOff2003
            // 
            this.iOff2003.Caption = "Office2003";
            this.iOff2003.Description = "Office2003";
            this.iOff2003.Id = 28;
            this.iOff2003.Name = "iOff2003";
            // 
            // iDefault
            // 
            this.iDefault.Caption = "Default";
            this.iDefault.Description = "Default";
            this.iDefault.Id = 29;
            this.iDefault.Name = "iDefault";
            // 
            // iDocNSYN
            // 
            this.iDocNSYN.Caption = "未同步单据查询";
            this.iDocNSYN.Id = 38;
            this.iDocNSYN.Name = "iDocNSYN";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 41;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // xtraTabbedMdiManager2
            // 
            this.xtraTabbedMdiManager2.MdiParent = this;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "Main";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "ToSAP";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem ToSYN;
        private DevExpress.XtraBars.BarSubItem mInit;
        private DevExpress.XtraBars.BarButtonItem AppInit;
        private DevExpress.XtraBars.BarCheckItem iWXP;
        private DevExpress.XtraBars.BarCheckItem iOffXP;
        private DevExpress.XtraBars.BarCheckItem iOff2K;
        private DevExpress.XtraBars.BarCheckItem iOff2003;
        private DevExpress.XtraBars.BarCheckItem iDefault;
        private DevExpress.XtraBars.BarButtonItem FWsynObjectSet;
        private DevExpress.XtraBars.BarButtonItem SAPDIData;
        private DevExpress.XtraBars.BarButtonItem FWToSAPSet;
        private DevExpress.XtraBars.BarButtonItem PlanToSAP;
        private DevExpress.XtraBars.BarButtonItem mSet;
        private DevExpress.XtraBars.BarButtonItem iDocNSYN;
        private DevExpress.XtraBars.BarButtonItem iRoleNSYN;
        private DevExpress.XtraBars.BarButtonItem iQuery;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarSubItem mQuery;
        private DevExpress.XtraBars.BarButtonItem ToSAPUserSet;
        private DevExpress.XtraBars.BarButtonItem FWobjData;
        private DevExpress.XtraBars.BarButtonItem SAPsynObjectSet;
        private DevExpress.XtraBars.BarButtonItem CheckUpdate;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        public static DevExpress.XtraBars.BarButtonItem BranchSet;
    }
}

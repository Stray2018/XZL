﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToSAP.ToSYN
{
    public partial class ThreadDie : Form
    {
        public ThreadDie()
        {
            InitializeComponent();
        }

        private void ThreadDie_Shown(object sender, EventArgs e)
        {
            while (Program.thread.IsAlive)
            {
                Application.DoEvents();
            }
            this.Close();}

        private void ThreadDie_Load(object sender, EventArgs e)
        {
            
        }
    }
}

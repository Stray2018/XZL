﻿namespace ToSAP.ToSYN
{
    partial class ToSYN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToSYN));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.shopList = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.AreaS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chooseArea = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Area1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Chs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.chooseshp = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.WhsCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WhsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Entry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.oChoosArea = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkAll = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkCorp = new DevExpress.XtraEditors.CheckEdit();
            this.DispMessage = new System.Windows.Forms.RichTextBox();
            this.checkAll3 = new DevExpress.XtraEditors.CheckEdit();
            this.CancelOK = new System.Windows.Forms.Button();
            this.ErrQuery = new System.Windows.Forms.Button();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.stDate = new DevExpress.XtraEditors.DateEdit();
            this.enDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseshp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oChoosArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkCorp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAll3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(116, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(52, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "开始日期:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(331, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "结束日期:";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl1.Controls.Add(this.shopList);
            this.groupControl1.Location = new System.Drawing.Point(8, 35);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(389, 406);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "门店选择";
            // 
            // shopList
            // 
            this.shopList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.shopList.Location = new System.Drawing.Point(3, 19);
            this.shopList.MainView = this.gridView1;
            this.shopList.Name = "shopList";
            this.shopList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.oChoosArea,
            this.chooseshp,
            this.chooseArea});
            this.shopList.Size = new System.Drawing.Size(380, 384);
            this.shopList.TabIndex = 0;
            this.shopList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.AreaS,
            this.Area1,
            this.Chs,
            this.WhsCode,
            this.WhsName,
            this.Entry});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.shopList;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 40;
            this.gridView1.OptionsSelection.CheckBoxSelectorField = "Chs";
            this.gridView1.OptionsSelection.EnableAppearanceHotTrackedRow = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsSelection.ResetSelectionClickOutsideCheckboxSelector = true;
            this.gridView1.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsSelection.ShowCheckBoxSelectorInPrintExport = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 22;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // AreaS
            // 
            this.AreaS.Caption = "区域选择";
            this.AreaS.ColumnEdit = this.chooseArea;
            this.AreaS.FieldName = "AreaS";
            this.AreaS.Name = "AreaS";
            this.AreaS.OptionsColumn.AllowEdit = false;
            this.AreaS.OptionsColumn.AllowMove = false;
            this.AreaS.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.AreaS.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.AreaS.Visible = true;
            this.AreaS.VisibleIndex = 0;
            // 
            // chooseArea
            // 
            this.chooseArea.AutoHeight = false;
            this.chooseArea.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.CheckBox;
            this.chooseArea.Name = "chooseArea";
            this.chooseArea.ValueChecked = "Y";
            this.chooseArea.ValueGrayed = "N";
            this.chooseArea.ValueUnchecked = "N";
            // 
            // Area1
            // 
            this.Area1.Caption = "区域";
            this.Area1.FieldName = "Area1";
            this.Area1.Name = "Area1";
            this.Area1.OptionsColumn.AllowEdit = false;
            this.Area1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Area1.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.Area1.Visible = true;
            this.Area1.VisibleIndex = 1;
            // 
            // Chs
            // 
            this.Chs.Caption = "店铺选择";
            this.Chs.ColumnEdit = this.chooseshp;
            this.Chs.FieldName = "Chs";
            this.Chs.Name = "Chs";
            this.Chs.OptionsColumn.AllowEdit = false;
            this.Chs.OptionsColumn.AllowMove = false;
            this.Chs.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Chs.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.Chs.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.Chs.Visible = true;
            this.Chs.VisibleIndex = 2;
            // 
            // chooseshp
            // 
            this.chooseshp.AutoHeight = false;
            this.chooseshp.Name = "chooseshp";
            this.chooseshp.ValueChecked = "Y";
            this.chooseshp.ValueGrayed = "N";
            this.chooseshp.ValueUnchecked = "N";
            // 
            // WhsCode
            // 
            this.WhsCode.Caption = "店铺编码";
            this.WhsCode.FieldName = "WhsCode";
            this.WhsCode.Name = "WhsCode";
            this.WhsCode.OptionsColumn.AllowEdit = false;
            this.WhsCode.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.WhsCode.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.WhsCode.Visible = true;
            this.WhsCode.VisibleIndex = 3;
            this.WhsCode.Width = 89;
            // 
            // WhsName
            // 
            this.WhsName.Caption = "店铺名称";
            this.WhsName.FieldName = "WhsName";
            this.WhsName.Name = "WhsName";
            this.WhsName.OptionsColumn.AllowEdit = false;
            this.WhsName.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.True;
            this.WhsName.Visible = true;
            this.WhsName.VisibleIndex = 4;
            this.WhsName.Width = 141;
            // 
            // Entry
            // 
            this.Entry.Caption = "内部ID";
            this.Entry.FieldName = "Entry";
            this.Entry.Name = "Entry";
            this.Entry.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.False;
            // 
            // oChoosArea
            // 
            this.oChoosArea.AutoHeight = false;
            this.oChoosArea.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.oChoosArea.LookAndFeel.UseDefaultLookAndFeel = false;
            this.oChoosArea.Name = "oChoosArea";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl2.AutoSize = true;
            this.groupControl2.Controls.Add(this.checkAll);
            this.groupControl2.Location = new System.Drawing.Point(403, 35);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(246, 406);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "主数据同步";
            // 
            // checkAll
            // 
            this.checkAll.Location = new System.Drawing.Point(11, 26);
            this.checkAll.Name = "checkAll";
            this.checkAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkAll.Properties.Appearance.Options.UseFont = true;
            this.checkAll.Properties.Caption = "全选";
            this.checkAll.Size = new System.Drawing.Size(150, 18);
            this.checkAll.TabIndex = 0;
            this.checkAll.CheckStateChanged += new System.EventHandler(this.checkAll_CheckStateChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.checkCorp);
            this.groupControl3.Controls.Add(this.DispMessage);
            this.groupControl3.Controls.Add(this.checkAll3);
            this.groupControl3.Location = new System.Drawing.Point(670, 35);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(438, 406);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = "业务单据同步";
            // 
            // checkCorp
            // 
            this.checkCorp.Location = new System.Drawing.Point(178, 26);
            this.checkCorp.Name = "checkCorp";
            this.checkCorp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCorp.Properties.Appearance.Options.UseFont = true;
            this.checkCorp.Properties.Caption = "Corp";
            this.checkCorp.Size = new System.Drawing.Size(75, 18);
            this.checkCorp.TabIndex = 3;
            // 
            // DispMessage
            // 
            this.DispMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DispMessage.AutoWordSelection = true;
            this.DispMessage.Location = new System.Drawing.Point(132, 171);
            this.DispMessage.Name = "DispMessage";
            this.DispMessage.Size = new System.Drawing.Size(300, 235);
            this.DispMessage.TabIndex = 2;
            this.DispMessage.Text = "";
            this.DispMessage.Visible = false;
            this.DispMessage.TextChanged += new System.EventHandler(this.DispMessage_TextChanged);
            // 
            // checkAll3
            // 
            this.checkAll3.Location = new System.Drawing.Point(6, 26);
            this.checkAll3.Name = "checkAll3";
            this.checkAll3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.checkAll3.Properties.Appearance.Options.UseFont = true;
            this.checkAll3.Properties.Caption = "全选";
            this.checkAll3.Size = new System.Drawing.Size(166, 18);
            this.checkAll3.TabIndex = 1;
            this.checkAll3.CheckedChanged += new System.EventHandler(this.checkAll3_CheckedChanged);
            // 
            // CancelOK
            // 
            this.CancelOK.Location = new System.Drawing.Point(472, 8);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(75, 23);
            this.CancelOK.TabIndex = 8;
            this.CancelOK.Text = "关闭";
            this.CancelOK.UseVisualStyleBackColor = true;
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // ErrQuery
            // 
            this.ErrQuery.Location = new System.Drawing.Point(596, 8);
            this.ErrQuery.Name = "ErrQuery";
            this.ErrQuery.Size = new System.Drawing.Size(75, 23);
            this.ErrQuery.TabIndex = 9;
            this.ErrQuery.Text = "错误查询";
            this.ErrQuery.UseVisualStyleBackColor = true;
            this.ErrQuery.Click += new System.EventHandler(this.ErrQuery_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(728, 8);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 14;
            this.simpleButton1.Text = "关闭消息";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "删除物料主数据";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(357, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "开始同步";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.button4);
            this.groupControl4.Controls.Add(this.button3);
            this.groupControl4.Controls.Add(this.button1);
            this.groupControl4.Controls.Add(this.simpleButton1);
            this.groupControl4.Controls.Add(this.button2);
            this.groupControl4.Controls.Add(this.ErrQuery);
            this.groupControl4.Controls.Add(this.CancelOK);
            this.groupControl4.Location = new System.Drawing.Point(8, 469);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.ShowCaption = false;
            this.groupControl4.Size = new System.Drawing.Size(1114, 37);
            this.groupControl4.TabIndex = 17;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(133, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(99, 23);
            this.button4.TabIndex = 18;
            this.button4.Text = "更新物料主数据";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(845, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 23);
            this.button3.TabIndex = 17;
            this.button3.Text = "清除登陆状态";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // stDate
            // 
            this.stDate.EditValue = null;
            this.stDate.Location = new System.Drawing.Point(175, 13);
            this.stDate.Name = "stDate";
            this.stDate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.stDate.Properties.Appearance.Options.UseBackColor = true;
            this.stDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.stDate.Size = new System.Drawing.Size(137, 22);
            this.stDate.TabIndex = 18;
            // 
            // enDate
            // 
            this.enDate.EditValue = null;
            this.enDate.Location = new System.Drawing.Point(390, 12);
            this.enDate.Name = "enDate";
            this.enDate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.enDate.Properties.Appearance.Options.UseBackColor = true;
            this.enDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.enDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.enDate.Size = new System.Drawing.Size(159, 22);
            this.enDate.TabIndex = 19;
            // 
            // ToSYN
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1120, 503);
            this.Controls.Add(this.enDate);
            this.Controls.Add(this.stDate);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("ToSYN.IconOptions.Icon")));
            this.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ToSYN";
            this.Text = "数据同步";
            this.Load += new System.EventHandler(this.ToSYN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shopList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseshp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oChoosArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkCorp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAll3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl shopList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn WhsCode;
        private DevExpress.XtraGrid.Columns.GridColumn WhsName;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private System.Windows.Forms.Button CancelOK;
        private System.Windows.Forms.Button ErrQuery;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit oChoosArea;
        private DevExpress.XtraEditors.CheckEdit checkAll;
        private DevExpress.XtraEditors.CheckEdit checkAll3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.RichTextBox DispMessage;
        private DevExpress.XtraGrid.Columns.GridColumn Entry;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.Button button3;
        private DevExpress.XtraEditors.DateEdit stDate;
        private DevExpress.XtraEditors.DateEdit enDate;
        private DevExpress.XtraEditors.CheckEdit checkCorp;
        private System.Windows.Forms.Button button4;
        private DevExpress.XtraGrid.Columns.GridColumn AreaS;
        private DevExpress.XtraGrid.Columns.GridColumn Area1;
        private DevExpress.XtraGrid.Columns.GridColumn Chs;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chooseArea;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chooseshp;
    }
}
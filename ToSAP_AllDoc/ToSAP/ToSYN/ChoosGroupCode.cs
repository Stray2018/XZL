﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.ToSYN
{
    public partial class ChoosGroupCode : Form
    {
        public static GridView ShpView;
        public static RichTextBox ORichTextBox;
        public ChoosGroupCode()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChoosGroupCode_Load(object sender, EventArgs e)
        {
            string sqlCmd = "select 'N' as ch1, GroupCode,GroupName from OITG order by GroupCode";
            Common.Common.GetFWSqlConn(null);
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            dataGridView1.DataSource = oDataTable;
            


        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string GroupCode = String.Empty;
            for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
            {
                if (dataGridView1.Rows[i].Cells["ch1"].Value.Equals("Y"))
                {
                    GroupCode += "," + dataGridView1.Rows[i].Cells["GroupCode"].Value.ToString();
                }
            }
            
            this.Close();

            Common.Common.upDateItm("'"+GroupCode.Substring(1)+"'",ShpView,ORichTextBox);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Services;
using System.Web.Services.Description;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Newtonsoft.Json.Linq;
using SAPbobsCOM;
using ToSAP.Query;
using ToSAP.Set;
using DataRow = System.Data.DataRow;

namespace ToSAP.ToSYN
{
    public partial class ToSYN : XtraForm
    {
        public ToSYN()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;

        }

        public static CookieContainer CookiesContainer = new CookieContainer();
        private DataTable WhsData = new DataTable();
        private void ToSYN_Load(object sender, EventArgs e)
        {
            int screenint = Convert.ToInt32(Screen.PrimaryScreen.Bounds.Width * 0.9);
            this.button4.Left = screenint / 7 * 1;
            this.button2.Left = screenint / 7 * 2;
            this.CancelOK.Left = screenint / 7 * 3;
            this.ErrQuery.Left = screenint / 7 * 4;
            this.simpleButton1.Left = screenint / 7 * 5;
            this.button3.Left = screenint / 7 * 6;
            
            Program.GroupC3Select = 0;
            Common.Common.GetFWSqlConn(this);
            DispMessage.Visible = false;
            DispMessage.HideSelection = false;
            stDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(-1);
            enDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddDays(-1);
            checkCorp.Visible = false;
            try
            {
                WhsData = Common.Common.oUserAccessShop(Program.FwSqlConnection, Program.oUSER);
                DataColumn[] primColumns = new DataColumn[1];
                    primColumns[0]= WhsData.Columns["Entry"];
                    WhsData.PrimaryKey = primColumns;
                shopList.DataSource = WhsData;
                WhsData.DefaultView.Sort = "Area1,WhsCode";
                string oContion = "";

                if (Program.DocToSAP.Equals("零售单"))
                {
                    oContion =
                        " and T1.ERPobject<>'OSAL'  UNION ALL SELECT top 1 'OSALToSAP','零售单-->SAP单据(根据同步逻辑)' ,T1.ToOrder from ToSAP_Role_Mas T1 where T1.ERPobject = 'OSAL' ";
                }

                string sqlCmd =
                    "SELECT T1.ERPobject+T3.SAPobjCode 'RolCode', T2.FWName+'-->'+T3.SAPobjName 'RolName' FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode LEFT JOIN ToSAP_DIobject T3 ON T1.SAPobject=T3.SAPobjCode WHERE T2.DataType <>'单据' order by T1.ToOrder ";
                DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                var L = 0;
                groupControl2.SendToBack();
                foreach (DataRow oRow in oDataTable.Rows)
                {
                    var oCheckEdit = new CheckEdit();
                    oCheckEdit.Name = oRow[0].ToString().Trim();
                    oCheckEdit.Text = oRow[1].ToString().Trim();
                    oCheckEdit.Location = new Point(10, 50 + L * 22);
                    oCheckEdit.Width = 240;

                    oCheckEdit.BringToFront();
                    groupControl2.Controls.Add(oCheckEdit);
                    L++;
                }

                if (Program.CorpSet.Equals("是"))
                {
                    checkAll3.Text = "店铺数据同步接口";
                    sqlCmd =
                        "SELECT T1.ERPobject+T3.SAPobjCode 'RolCode', T2.FWName+'-->'+T3.SAPobjName 'RolName' ,T1.ToOrder FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode " +
                        "LEFT JOIN ToSAP_DIobject T3 ON T1.SAPobject=T3.SAPobjCode where T3.SAPobjName not like '%集团%' and T2.DataType ='单据' " +
                        oContion + " order by T1.ToOrder";
                    oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                    L = 0;
                    groupControl3.SendToBack();
                    int Grop3Height = groupControl3.Height;
                    int showit = 0;
                    foreach (DataRow oRow in oDataTable.Rows)
                    {
                        var oCheckEdit = new CheckEdit();
                        oCheckEdit.Name = oRow[0].ToString().Trim();
                        oCheckEdit.Text = oRow[1].ToString().Trim();

                        if ((70 + L * 22) > Grop3Height)
                        {
                            L = 0;
                            showit++;
                        }
                        oCheckEdit.Width = screenint / 4 - 5;
                        oCheckEdit.Location = new Point(10 + showit * oCheckEdit.Width, 50 + L * 22);

                        if (Program.DocToSAP.Equals("所有单"))
                        {
                            oCheckEdit.Enabled = false;
                            oCheckEdit.CheckState = CheckState.Unchecked;
                        }

                        oCheckEdit.BringToFront();
                        groupControl3.Controls.Add(oCheckEdit);
                        L++;
                    }

                    checkCorp.Visible = true;
                    checkCorp.Name = "Corp";
                    checkCorp.Text ="集团数据同步接口";
                    checkCorp.Width = screenint / 4 - 5;
                    checkCorp.CheckStateChanged += OCheckEdit1_CheckStateChanged;
                    L=L+2;
                    if ((70 + L * 22) > Grop3Height)
                    {
                        L = 0;
                        showit++;
                    }
                    checkCorp.Location = new Point(10 + showit * checkCorp.Width, 50 + L * 22);
                    checkCorp.CheckState = CheckState.Unchecked;
                    checkCorp.Enabled = true;
                    checkCorp.BringToFront();
                    
                    L++;
                    sqlCmd =
                        "SELECT T1.ERPobject+T3.SAPobjCode 'RolCode', T2.FWName+'-->'+T3.SAPobjName 'RolName' ,T1.ToOrder FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode " +
                        "LEFT JOIN ToSAP_DIobject T3 ON T1.SAPobject=T3.SAPobjCode where T3.SAPobjName  like '%集团%' and T2.DataType ='单据' " +
                        oContion + " order by T1.ToOrder";
                    oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                    
                    foreach (DataRow oRow in oDataTable.Rows)
                    {
                        var oCheckEdit = new CheckEdit();
                        oCheckEdit.Name =oRow[0].ToString().Trim();
                        oCheckEdit.Text = oRow[1].ToString().Trim();

                        if ((70 + L * 22) > Grop3Height)
                        {
                            L = 0;
                            showit++;
                        }
                        oCheckEdit.Width = screenint / 4 - 5;
                        oCheckEdit.Location = new Point(10 + showit * oCheckEdit.Width, 50 + L * 22);

                        if (Program.DocToSAP.Equals("所有单"))
                        {
                            oCheckEdit.Enabled = false;
                            oCheckEdit.CheckState = CheckState.Unchecked;
                        }
                        
                        oCheckEdit.BringToFront();
                        groupControl3.Controls.Add(oCheckEdit);
                        L++;
                    }
                }
                else
                {
                    sqlCmd =
                        "SELECT T1.ERPobject+T3.SAPobjCode 'RolCode', T2.FWName+'-->'+T3.SAPobjName 'RolName' ,T1.ToOrder FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode LEFT JOIN ToSAP_DIobject T3 ON T1.SAPobject=T3.SAPobjCode where  T2.DataType ='单据' " +
                        oContion + " order by T1.ToOrder";
                    oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                    L = 0;
                    groupControl3.SendToBack();
                    int Grop3Height = groupControl3.Height;
                    int showit = 0;
                    foreach (DataRow oRow in oDataTable.Rows)
                    {
                        var oCheckEdit = new CheckEdit();
                        oCheckEdit.Name = oRow[0].ToString().Trim();
                        oCheckEdit.Text = oRow[1].ToString().Trim();

                        if ((70 + L * 22) > Grop3Height)
                        {
                            L = 0;
                            showit++;
                        }
                        oCheckEdit.Width = screenint / 4 - 5;
                        oCheckEdit.Location = new Point(10 + showit * oCheckEdit.Width, 50 + L * 22);

                        if (Program.DocToSAP.Equals("所有单"))
                        {
                            oCheckEdit.Enabled = false;
                            oCheckEdit.CheckState = CheckState.Unchecked;
                        }

                        oCheckEdit.BringToFront();
                        groupControl3.Controls.Add(oCheckEdit);
                        L++;
                    }
                }
                

                string cookieFile = Application.StartupPath + "\\CookieContainer.bin"; //cookie保存位置
                FileInfo oFileInfo = new FileInfo(cookieFile);
                if (oFileInfo.Exists)
                {
                    oFileInfo.Delete();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("配置错误,请先检查配置!" + Environment.NewLine + exception.Message);
            }
        }

        private void OCheckEdit1_CheckStateChanged(object sender, EventArgs e)
        {
            foreach (var oControl in groupControl3.Controls)
                {
                    if (oControl.GetType() == typeof(CheckEdit) && !(((CheckEdit)oControl).Name == "Corp") && ((CheckEdit)oControl).Text.Contains("集团"))
                    {
                        ((CheckEdit) oControl).CheckState = checkCorp.CheckState;
                    }
                }
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void checkAll_CheckStateChanged(object sender, EventArgs e)
        {
            foreach (var oControl in groupControl2.Controls)
            {
                if (oControl.GetType() == typeof(CheckEdit) && !(((CheckEdit) oControl).Name == "checkAll"))
                {
                    ((CheckEdit) oControl).CheckState = checkAll.CheckState;
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DispMessage.Clear();
            DispMessage.Visible = false;
           // DispMessage.Clear();
        }

        private void DispMessage_TextChanged(object sender, EventArgs e)
        {
            DispMessage.Refresh();
        }

        private void checkAll3_CheckedChanged(object sender, EventArgs e)
        {
            foreach (var oControl in groupControl3.Controls)
            {
                if (oControl.GetType() == typeof(CheckEdit) && (!(((CheckEdit) oControl).Name == "checkAll3")) &&  (!((CheckEdit)oControl).Text.Contains("集团")) )
                {
                    ((CheckEdit) oControl).CheckState = checkAll3.CheckState;
                }
            }
        }

        private void ErrQuery_Click(object sender, EventArgs e)
        {
            if (!Common.Common.IsExistWindow("ErrQuery", this))
            {
                var oNewDocNsyn = new ErrQuery();
                oNewDocNsyn.MdiParent = MdiParent;
                oNewDocNsyn.Text = "同步错误查询";
                oNewDocNsyn.Show();
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
           // gridView1.InvertRowSelection(gridView1.FocusedRowHandle);
        }

        private  void  button1_Click(object sender, EventArgs e)
        {
            string MessStr = "请确认开始同步:" + stDate.DateTime.ToString("yyyy-MM-dd") + "到:" +
                             enDate.DateTime.ToString("yyyy_MM-dd") + "的数据!";
            if (MessageBox.Show(MessStr, "确认", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            string serUrl = ((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            //serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
            //string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";

            for (int i = 0; i < gridView1.RowCount; i++)
               
            {
                string shpChs = gridView1.GetRowCellValue(i, "Chs").ToString();
                if (!shpChs.Equals("Y"))
                {
                    continue;
                }
                string shpCode = gridView1.GetRowCellValue(i, "WhsCode").ToString();
                string sqlCmd = "select ItemCode from oitm";
                Program.SAPsqlConnStr.DataSource = ((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
                Program.SAPsqlConnStr.UserID = ((JObject) ToSAPJson["SAP"])["SAPDBuser"].ToString().Trim();
                Program.SAPsqlConnStr.Password =
                    Common.Common.Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
                Program.SAPsqlConnStr.InitialCatalog = Common.Common.GetSAPDBName(Program.FwSqlConnection, shpCode);
                DataTable oDataTable = Common.Common.GetDataTable(Program.SAPsqlConnStr.ConnectionString, sqlCmd, null);
                DispMessage.Visible = true;

                Dictionary<string, string> oDictionary = new Dictionary<string, string>();
                oDictionary.Add("oUserCode", Program.oUSER);
                oDictionary.Add("shpCode", shpCode);
                oDictionary.Add("ToSAPini", connStr);

                string importxml = Common.Common.postString("LogToSAP", oDictionary,null);
                DispMessage.AppendText("正在连接SAP!:  " + DateTime.Now.ToString() + Environment.NewLine);

                try
                {
                   JObject LogTosapResult =  Common.Common.soapPost(Program.serviceurl, importxml, "Y", shpCode);
                    if (LogTosapResult["errCode"].ToString().Equals("0"))
                    {
                        DispMessage.AppendText("连接完成:  " + LogTosapResult["errMsg"].ToString() + " " + DateTime.Now.ToString() + Environment.NewLine);
                    }
                    else
                    {
                        DispMessage.AppendText("连接失败:  " + LogTosapResult["errMsg"].ToString() + " " + DateTime.Now.ToString() + Environment.NewLine);
                        return;
                    }

                }
                catch (Exception exception)
                {
                    DispMessage.AppendText("连接失败:  " + DateTime.Now.ToString() + exception.ToString() +
                                           Environment.NewLine);
                }

                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ItemCode = oDataRow[0].ToString();
                    DispMessage.AppendText("正在删除:" + ItemCode + " " + DateTime.Now.ToString() + Environment.NewLine);
                    //WebToSAP.DeletItem(ItemCode);
                    Dictionary<string, string> iDictionary = new Dictionary<string, string>();
                    iDictionary.Add("ItemCode", ItemCode);
                    string importxml1 = Common.Common.postString("DeletItem", iDictionary,null);
                    string deleteResult = Common.Common.soapPost(Program.serviceurl, importxml1, "N", shpCode)["errMsg"].ToString();
                    DispMessage.AppendText(deleteResult + " " + DateTime.Now.ToString() + Environment.NewLine);
                    DispMessage.ScrollToCaret();

                }

                Common.Common.weblogOutSAP(shpCode, Program.oUSER);
            }

        }

        public IWin32Window OMainWin32Window;

        private void button2_Click(object sender, EventArgs e)
        {

            Program.checkList = string.Empty;
            for (int i = 0; i < groupControl3.Controls.Count; i++)
            {
                if (!(groupControl3.Controls[i].GetType() == typeof(CheckEdit)))
                {
                    continue;
                }

                var oCheckEdit = (CheckEdit)groupControl3.Controls[i];
                if (oCheckEdit.CheckState != CheckState.Checked || oCheckEdit.Name == "checkAll3" || oCheckEdit.Name == "Corp")
                {
                    continue;
                }
                else
                {
                    Program.checkList += ",'" + oCheckEdit.Name + "'";
                }
            }
            if (!Program.checkList.Equals(string.Empty))
            {
                string sqlCmd03 = "select  * from ToSAP_Role_Mas T1 where T1.SumImport='Y' and T1.ERPobject+T1.SAPobject in(" + Program.checkList.Substring(1) + ")";
                Program.DispWhsShp = Program.showOWHS;
                DataTable sumimportData = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd03, null);

                int sumimportRule = sumimportData.Rows.Count;
                sumimportData.Clear();
                sqlCmd03 = "select  * from ToSAP_Role_Mas T1 where T1.ERPobject+T1.SAPobject in(" + Program.checkList.Substring(1) + ")";

                sumimportData = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd03, null);
                int importrule = sumimportData.Rows.Count;

                if ((sumimportRule > 0 && !(importrule == sumimportRule)) || sumimportRule > 1)
                {

                    MessageBox.Show("选择了汇总导入规则和非汇总导入规则 或者选择了多个汇总导入规则，不允许执行导入操作！");
                    button2.Text = "开始同步";
                    return;
                }
            }
            


            if (this.button2.Text.Equals("停止同步"))
            {
                if (Program.thread.IsAlive)
                {
                    Program.threadDie = 1;
                   ThreadDie oThreadDie = new ThreadDie();
                    oThreadDie.ShowDialog(this);
                    // Program.thread.Abort();
                }
                this.button2.Text = "开始同步";
                this.button2.Refresh();
                DispMessage.SelectionColor = Color.Red;
                DispMessage.AppendText("线程终止成功");
                //Common.Common.weblogOutSAP(null, Program.oUSER);
                return;
            }
            Program.threadDie = 0;
            int shpChoosCount = 0;
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                string shpChs = gridView1.GetRowCellValue(i, "Chs").ToString();
                if (shpChs.Equals("Y"))
                {
                    ++shpChoosCount;
                }
            }

            if (shpChoosCount==0)
            {
                MessageBox.Show("未选择店铺或仓库,请选择!");
                return;
            }

            DateTime stDate1, enDate1;

            try
            {
                stDate1 = DateTime.Parse(stDate.Text);
                enDate1 = DateTime.Parse(enDate.Text);
                Program.pstDate = stDate.Text;
                Program.penDate = enDate.Text;
            }
            catch (Exception)
            {
                MessageBox.Show("开始日期或结束日期未选择,请选择!");
                return;
            }

            string MessStr = "请确认开始同步:" + stDate.DateTime.ToString("yyyy-MM-dd") + "到:" +
                             enDate.DateTime.ToString("yyyy-MM-dd") + "的数据!";
            if (MessageBox.Show(MessStr, "确认", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            OMainWin32Window = this;
            checkCorp.SendToBack();
            DispMessage.BringToFront();
            DispMessage.Visible = true;
           // DispMessage.Clear();
            this.button2.Text = "停止同步";
            //Program.thread = new Thread(new ThreadStart(BasicDocToSAP).Invoke);
            //Program.thread.SetApartmentState(ApartmentState.STA);
            //Program.thread.IsBackground = true;
            //Program.thread.Start();

            Program.thread = new Thread(new ThreadStart(BasicDocToSAP).Invoke);
            Program.thread.SetApartmentState(ApartmentState.STA);
            Program.thread.IsBackground = true;
            Program.thread.Start();

        }


        private void button3_Click(object sender, EventArgs e)
        {
            Common.Common.weblogOutSAP(null, Program.oUSER);
            MessageBox.Show("清除SAP用户登陆状态成功！");
        }

        private  void BasicDocToSAP()
        {
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                try
                {
                    string shpChs = gridView1.GetRowCellValue(i, "Chs").ToString();
                    if (!shpChs.Equals("Y"))
                    {
                        continue;
                    }
                    // DispMessage.Clear();
                    Common.Common.GetFWSqlConn(this);
                    string shpCode = gridView1.GetRowCellValue(i,"WhsCode").ToString();
                    string shpName = gridView1.GetRowCellValue(i, "WhsName").ToString();
                    Program.DispWhsShp = shpName.Contains("店铺") ? "店铺" : "仓库";
                    string shpEntry = gridView1.GetRowCellValue(i, "Entry").ToString();
                    string BPLID1 = Common.Common.GetBPLID(shpCode,shpEntry);
                    var SAPDB = Common.Common.GetSAPDBName(Program.FwSqlConnection, shpCode);
                    Common.Common.CreateSHPTable(Program.oUSER);

                    string sqlCmd03 = "INSERT ToSAP_OSHP_" + Program.oUSER +
                                      "(shpEntry,shpCode,shpName,BPLID,SAPCompany) VALUES('" + shpEntry + "','" +
                                      shpCode + "','" + shpName + "','" + BPLID1 + "','" + SAPDB + "')";
                    Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd03);

                    Dictionary<string, string> oDictionary = new Dictionary<string, string>();
                    bool WebLogSAP =  Common.Common.WebLogSAP(shpCode, Program.oUSER, DispMessage);
                    if (!WebLogSAP)
                    {
                        this.button2.Text = "开始同步";
                        return;
                    }

                    var BPLID = string.Empty;



                    for (var j = 0; j < groupControl2.Controls.Count; j++)
                    {
                        var oCheckEdit =
                            (CheckEdit) groupControl2.Controls[j];
                        if (oCheckEdit.CheckState != CheckState.Checked || oCheckEdit.Name == "checkAll")
                        {
                            continue;
                        }

                        DispMessage.SelectionColor = Color.Black;
                        DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  同步规则!" + oCheckEdit.Text +
                                               Environment.NewLine);
                        DispMessage.ScrollToCaret();
                        string sqlCmd =
                            "SELECT * FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface WHERE ERPobject+SAPobject = '" +
                            oCheckEdit.Name + "'";
                        var oMasDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,
                            DispMessage);


                        DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  获取规则对象" +
                                               DateTime.Now.ToString() + Environment.NewLine);
                        DispMessage.ScrollToCaret();

                        var SAPDIinterface = oMasDt.Rows[0]["SAPDIinterface"].ToString().Trim();
                        var DiobjType = oMasDt.Rows[0]["DIobjType"].ToString().Trim();
                        var SAPDITableName = oMasDt.Rows[0]["DTable"].ToString().Trim();
                        string ERPobject = oMasDt.Rows[0]["ERPobject"].ToString().Trim();
                        string SAPobject = oMasDt.Rows[0]["SAPobject"].ToString().Trim();

                        string SAPDI = oMasDt.Rows[0]["DI"].ToString().Trim();
                        DataSet oDataSet;
                        DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  开始查询数据:" +
                                               DateTime.Now.ToString() + Environment.NewLine);
                        DispMessage.ScrollToCaret();
                        oDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit.Name, SAPDIinterface,
                            shpCode, DiobjType, DiobjType, Program.shpCode, SAPDB, SAPDITableName, DispMessage);
                        string result;
                        if (oDataSet.Tables[SAPDITableName].Rows.Count == 0)
                        {
                            // continue;
                        }

                        int RowCount = 0;
                        switch (SAPDI)
                        {
                            //成本中心
                            case "ProfitCenter":
                                Common.Common.sendDataToWeb(Program.serviceurl, "62", "成本中心", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                break;
                            //仓库
                            case "Warehouses":
                                Common.Common.sendDataToWeb(Program.serviceurl, "64", "仓库主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                break;
                            //业务伙伴主数据
                            case "BusinessPartners":
                                Common.Common.sendDataToWeb(Program.serviceurl, "2", "业务伙伴主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                break;
                            //物料主数据
                            case "Items":
                                Common.Common.sendDataToWeb(Program.serviceurl, "4", "物料主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                break;

                            #region 分支主数据

                            case "BusinessPlaces":
                                
                                sqlCmd =
                                    "SELECT BranchCode KeyField , 'C'+BranchCode Cuskey,'G'+BranchCode Venkey, BranchCode,BranchName," +
                                    "(SELECT BranchCode BPLID, BranchName BPLName,BranchCode BPLNameForeign , WhsCode1 DefaultWarehouseID, WhsCode1 DefaultResourceWarehouseID, Country  FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH, ROOT('BusinessPlaces')) busxml," +
                                    "(SELECT WhsCode1 WarehouseCode, WhsName1 WarehouseName FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH,ROOT('Warehouses')) Whsxml," +
                                    "(SELECT CusCode1 CardCode, CusName1 CardName,'C' CardType FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH,ROOT('BusinessPartners')) cusXml," +
                                    "(SELECT VenCode1 CardCode, VenName1 CardName,'S' CardType FROM ToSAP_Branch T100 WHERE T1.branchcode = T100.BranchCode FOR XML PATH,ROOT('BusinessPartners')) VenXml" +
                                    "  FROM ToSAP_Branch T1 WHERE IntFace = '" + Program.InterFaceSet + "' AND SHOP='" +
                                    Program.Shop +
                                    "' and NOT EXISTS(SELECT * FROM ToSAP_ImportOK T00 WHERE  T1.BranchCode=T00.DocEntry AND T00.DocType='Branch' AND SAPDocType = 'OBPL' AND T00.SAPCompany ='" +
                                    SAPDB + "')";
                                oDataSet.Clear();
                                oDataSet.Tables.Remove("OBPL");
                                oDataSet.AcceptChanges();
                                oDataSet.Tables.Add(Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString,
                                    sqlCmd, DispMessage));


                                sqlCmd =
                                    " SELECT Value1 KeyField, 'C'+Value1 Cuskey,'G'+Value1 Venkey,Value1 BranchCode,Value2 BranchName," +
                                    "   (SELECT value1 BPLID, Value2 BPLName, value1 BPLNameForeign,value1 DefaultWarehouseID, value1 DefaultResourceWarehouseID, 'CN' Country  FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH, ROOT('BusinessPlaces')) busxml," +
                                    "(SELECT value1 WarehouseCode, Value2 WarehouseName FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH,ROOT('Warehouses')) Whsxml," +
                                    "(SELECT 'K' + value1 CardCode,Value2 CardName,'C' CardType FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH,ROOT('BusinessPartners')) cusXml," +
                                    "(SELECT 'G' + value1 CardCode,Value2 CardName,'S' CardType FROM ToSAP_Appinit T2 WHERE T1.Value1 = T2.Value1 AND T2.ParaCode = 'CorpSub' FOR XML PATH,ROOT('BusinessPartners')) VenXml" +
                                    "   FROM ToSAP_Appinit T1 WHERE T1.ParaCode = 'CorpSub' and NOT EXISTS(SELECT * FROM ToSAP_ImportOK T00 WHERE  T1.Value1=T00.DocEntry AND T00.DocType='Branch' AND SAPDocType = 'OBPL' AND T00.SAPCompany ='" +
                                    SAPDB + "')";
                                DataTable oDataTable =
                                    Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,
                                        DispMessage);
                                if (oDataTable.Rows.Count != 0)
                                {
                                    oDataSet.Tables[0].ImportRow(oDataTable.Rows[0]);
                                    oDataSet.AcceptChanges();
                                }

                                oDataTable.Clear();
                                string mContion = oMasDt.Rows[0]["Contion"].ToString();
                                sqlCmd =
                                    "  SELECT T1.shpCode KeyField, 'C'+T1.shpCode Cuskey,'G'+T1.shpCode Venkey,T1.shpCode BranchCode,T1.shpName BranchName,  " +
                                    " (SELECT T2.shpCode BPLID, T2.shpName BPLName,shpCode BPLNameForeign, T2.shpCode DefaultWarehouseID, T2.shpCode DefaultResourceWarehouseID, 'CN' Country  FROM OSHP T2 WHERE T1.shpCode = T2.shpCode " + mContion + " FOR XML PATH, ROOT('BusinessPlaces')) busxml, " +
                                    " (SELECT T2.shpCode WarehouseCode, T2.shpName WarehouseName FROM OSHP T2 WHERE T1.shpCode = T2.shpCode " + mContion + "  FOR XML PATH,ROOT('Warehouses')) Whsxml," +
                                    " (SELECT 'K' + shpCode CardCode,shpName CardName,'C' CardType FROM OSHP T2 WHERE T1.shpCode = T2.shpCode  " + mContion + " FOR XML PATH,ROOT('BusinessPartners')) cusXml," +
                                    " (SELECT 'G' + shpCode CardCode,T2.shpName CardName,'S' CardType FROM OSHP T2 WHERE T1.shpCode = T2.shpCode  " + mContion + " FOR XML PATH,ROOT('BusinessPartners')) VenXml " +
                                    " FROM OSHP T1 WHERE  1=1 "+ mContion + " and EXISTS(SELECT * FROM ToSAP_Appinit WHERE ParaCode = 'Shop' AND Value1 = '是') and " +
                                    "  NOT EXISTS(SELECT * FROM ToSAP_ImportOK T00 WHERE  T1.shpCode = T00.DocEntry AND T00.DocType = 'Branch' AND SAPDocType = 'OBPL' AND T00.SAPCompany = '" + SAPDB + "')";
                                oDataTable =
                                    Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd.Replace("T1.",""),
                                        DispMessage);
                                if (oDataTable.Rows.Count != 0)
                                {
                                    oDataSet.Tables[0].ImportRow(oDataTable.Rows[0]);
                                    oDataSet.AcceptChanges();
                                }

                                //if (oDataSet.Tables[0].Rows.Count == 0)
                                //{
                                //    continue;
                                //}
                                oDataSet.Tables[0].Columns["Whsxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].TableName = "OWHS";
                                oDataSet.AcceptChanges();
                                oCheckEdit.Text = "分支主数据-->仓库";
                                SAPDITableName = "OWHS";
                                SAPobject = "OBPL_OWHS";
                                Common.Common.sendDataToWeb(Program.serviceurl, "64", "仓库主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);

                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Whsxml";
                                oDataSet.Tables[0].Columns["Busxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].TableName = "OBPL";
                                oDataSet.AcceptChanges();
                                oCheckEdit.Text = "分支主数据-->分支主数据";
                                SAPDITableName = "OBPL";
                                SAPobject = "OBPL";
                                Common.Common.sendDataToWeb(Program.serviceurl, "247", "分支主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);

                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Busxml";
                                oDataSet.Tables[0].Columns["Cusxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].Columns["KeyField"].ColumnName = "OLDKey";
                                oDataSet.Tables[0].Columns["Cuskey"].ColumnName = "KeyField";
                                oDataSet.Tables[0].TableName = "OCRD";
                                oDataSet.AcceptChanges();
                                SAPDITableName = "OCRD";
                                oCheckEdit.Text = "分支主数据-->客户";
                                SAPobject = "OBPL_OCRD_C";
                                Common.Common.sendDataToWeb(Program.serviceurl, "2", "业务伙伴主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);

                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Cusxml";
                                oDataSet.Tables[0].Columns["Venxml"].ColumnName = "xmlobj";
                                oDataSet.Tables[0].Columns["KeyField"].ColumnName = "Cuskey";
                                oDataSet.Tables[0].Columns["Venkey"].ColumnName = "KeyField";
                                oDataSet.AcceptChanges();

                                oCheckEdit.Text = "分支主数据-->供应商";
                                SAPobject = "OBPL_OCRD_S";
                                Common.Common.sendDataToWeb(Program.serviceurl, "2", "业务伙伴主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);

                                oDataSet.Tables[0].Columns["KeyField"].ColumnName = "Venkey";
                                oDataSet.Tables[0].Columns["OLDKey"].ColumnName = "KeyField";
                                oDataSet.Tables[0].Columns["xmlobj"].ColumnName = "Venxml";


                                sqlCmd =
                                    "SELECT 'Y' change,U1.WhsCode BranchCode,U1.WhsName,(SELECT T1.WhsCode WarehouseCode,T1.WhsName WarehouseName,T2.BPLId BusinessPlaceID FROM OWHS T1 INNER JOIN OBPL T2 ON T1.WhsCode = T2.DflWhs WHERE t1.WhsCode = U1.WhsCode  GROUP BY T1.WhsCode,t1.WhsName,t2.BPLId FOR XML PATH, ROOT('Warehouses')) xmlobj " +
                                    "FROM OWHS U1 WHERE U1.WhsCode IN(SELECT DflWhs FROM OBPL)";
                                oDataSet.Reset();
                                oDataSet.AcceptChanges();
                                oDataSet.Tables.Add(Common.Common.GetDataTable(Program.SAPsqlConnStr.ConnectionString,
                                    sqlCmd, DispMessage));
                                oDataSet.Tables[0].TableName = "OWHS";
                                oDataSet.AcceptChanges();
                                oCheckEdit.Text = "修改仓库的所属分支";
                                SAPDITableName = "OWHS";
                                SAPobject = "OBPL_OWHS";
                                Common.Common.sendDataToWeb(Program.serviceurl, "64", "修改仓库主数据", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                oCheckEdit.Text = "分支主数据-->分支主数据";

                                #endregion

                                break;
                            //物料组
                            case "ItemGroups":
                                Common.Common.sendDataToWeb(Program.serviceurl, "52", "物料组", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                break;
                        }

                        oDataSet.Dispose();

                    }


                    DispMessage.SelectionColor = Color.Black;
                    DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  数据同步完成!" + Environment.NewLine);
                    button2.Text = "开始同步";
                    this.button2.Refresh();
                    DispMessage.ScrollToCaret();
                    Common.Common.weblogOutSAP(shpCode, Program.oUSER); //断开Web SAP 连接
                    if (Program.InterFaceSet.Equals("按分支")){break;}
                   // Program.thread.Abort();
                }
                catch (Exception exception)
                {
                    DispMessage.SelectionColor = Color.OrangeRed;
                    DispMessage.AppendText("Try Error:" + exception.Message + Environment.NewLine);
                    DispMessage.ScrollToCaret();
                    button2.Text = "开始同步";
                    Common.Common.weblogOutSAP(null, Program.oUSER);
                    //Program.thread.Abort();
                }

                

            }

           

            if (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据"))
            {

                if (checkAll3.Checked || checkCorp.Checked ||(!Program.checkList.Equals(string.Empty)))
                {
                    Program.thread = new Thread(new ThreadStart(allToSAP).Invoke);
                    Program.thread.SetApartmentState(ApartmentState.STA);
                    Program.thread.IsBackground = true;
                    Program.thread.Start();
                }

            }
            else
            {
                Program.thread = new Thread(new ThreadStart(StartToSAP).Invoke);
                Program.thread.SetApartmentState(ApartmentState.STA);
                Program.thread.IsBackground = true;
                Program.thread.Start();
            }
        }

        private  void StartToSAP()

        {
            DateTime stDate1, enDate1;
            this.button2.Text = "停止同步";
            try
            {
                stDate1 = DateTime.Parse(stDate.Text);
                enDate1 = DateTime.Parse(enDate.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("开始日期或结束日期未选择,请选择!");
                return;
            }

            Common.Common.GetFWSqlConn(this);

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                try
                {
                    string shpChs = gridView1.GetRowCellValue(i, "Chs").ToString();
                    if (!shpChs.Equals("Y"))
                    {
                       continue; 
                    }
                        // DispMessage.Clear();
                        
                     string shpCode = gridView1.GetRowCellValue(i,"WhsCode").ToString();
                    string shpName = gridView1.GetRowCellValue(i, "WhsName").ToString();
                    Program.DispWhsShp = shpName.Contains("店铺") ? "店铺" : "仓库";
                    string shpEntry = gridView1.GetRowCellValue(i, "Entry").ToString();
                    string BPLID = Common.Common.GetBPLID(shpCode,shpEntry);
                    var SAPDB = Common.Common.GetSAPDBName(Program.FwSqlConnection, shpCode);
                    Common.Common.CreateSHPTable(Program.oUSER);
                    
                    string sqlCmd03 = "INSERT ToSAP_OSHP_" + Program.oUSER + "(shpEntry,shpCode,shpName,BPLID,SAPCompany) VALUES('" + shpEntry + "','" + shpCode + "','" + shpName + "','" + BPLID + "','"+ SAPDB + "')";
                    Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd03);
                    
                    Dictionary<string, string> oDictionary = new Dictionary<string, string>();
                    bool WebLogSAP =  Common.Common.WebLogSAP(shpCode, Program.oUSER, DispMessage);
                    if (!WebLogSAP)
                    {
                        this.button2.Text = "开始同步";return;
                    }

                    //var BPLID = Common.Common.GetBPLID(shpCode, shpEntry);

                    //if (Program.InterFaceSet == "按分支")
                    //{
                    //    DispMessage.SelectionColor = Color.Black;
                    //    DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  获取分支信息" + Environment.NewLine);
                    //    DispMessage.ScrollToCaret();
                    //    string sqlCmd1 =
                    //        "SELECT T1.BranchName FROM ToSAP_BranchTitle T1 LEFT JOIN ToSAP_BranchDetail T2 ON T2.BranchCode = T1.BranchCode WHERE T2.WhsCode = '" +
                    //        shpCode + "'";
                    //    if (Program.DispWhsShp.Equals("店铺") && Program.Shop.Equals("是"))
                    //    {
                    //        sqlCmd1 =
                    //            "SELECT T1.shpName BranchName FROM OSHP T1 WHERE T1.shpCode='" +
                    //            shpCode + "'";
                    //    }

                    //    if ((!Program.DispWhsShp.Equals("店铺")) && Program.Shop.Equals("是"))
                    //    {
                    //        sqlCmd1 =
                    //            "SELECT Value2 BranchName  FROM ToSAP_Appinit T1 WHERE ParaCode='CorpSub'";
                    //    }

                    //    string BranchName = Common.Common
                    //        .GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, DispMessage).Rows[0][0]
                    //        .ToString();
                    //    sqlCmd1 = "SELECT BPLId FROM OBPL WHERE BPLName = '" + BranchName + "' AND Disabled = 'N'";
                    //    try
                    //    {
                    //        BPLID = Common.Common
                    //            .GetDataTable(Program.SAPsqlConnStr.ConnectionString, sqlCmd1, DispMessage).Rows[0][0]
                    //            .ToString();
                    //    }
                    //    catch (Exception exception)
                    //    {
                    //        DispMessage.SelectionColor = Color.Red;
                    //        DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  获取分支信息出错，请检查!" +
                    //                               Environment.NewLine);
                    //        DispMessage.ScrollToCaret();
                    //        Common.Common.weblogOutSAP(null,Program.oUSER);
                    //        this.button2.Text = "开始同步";
                    //        return;
                            
                    //    }
                   // }

                    #region 不统一导入

                    for (var j = 0; j < groupControl3.Controls.Count; j++)
                    {
                        if (!(groupControl3.Controls[j].GetType() == typeof(CheckEdit)))
                        {
                            continue;
                        }

                        var oCheckEdit =
                            (CheckEdit) groupControl3.Controls[j];
                        if (oCheckEdit.CheckState != CheckState.Checked || oCheckEdit.Name == "checkAll3" ||oCheckEdit.Name == "Corp")
                        {
                            continue;
                        }

                        DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  同步规则!" + oCheckEdit.Text +
                                               Environment.NewLine);
                        DispMessage.ScrollToCaret();
                        DataSet oDataSet = new DataSet();
                        string SAPDI = "", SAPDITableName = "", ERPobject = "", SAPobject = "", SAPDIinterface = "";
                        //检查零售数据,存在数量为负金额为正  数量为正 金额为负的单据
                        string masTable1 = "";
                        string sqlCmd01 =
                            "SELECT T2.MasterTb FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_Fwobject T2 ON T1.ERPobject=T2.FWCode WHERE T1.ERPobject+T1.SAPobject='" +
                            oCheckEdit.Name + "'";
                        DataTable oDataTable1 =
                            Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd01, null);
                        if (oDataTable1.Rows.Count > 0)
                        {
                            masTable1 = oDataTable1.Rows[0][0].ToString();
                        }

                        if (oCheckEdit.Name.Equals("OSALToSAP") || masTable1.Equals("OSAL"))
                        {
                            string shpEntrySelect = "";
                            string sqlCmd02 =
                                "SELECT * FROM OCAG2 T1 INNER JOIN CAG3 T2 ON T2.DocEntry = T1.DocEntry WHERE T2.SDate>='" +
                                stDate1.ToString("yyyy-MM-dd") + "' AND T2.EDate <= '" +
                                enDate1.ToString("yyyy-MM-dd") + "'";
                            DataTable oCagTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString,
                                sqlCmd02, null);
                            if (oCagTable.Rows.Count == 0 && Program.oLDMat.Equals("销售退货模式"))
                            {
                                MessageBox.Show("未做成本核定,请先核定成本再同步零售数据!");
                                return;
                            }

                            for (int k = 0; k < gridView1.RowCount; k++)
                            {
                                string shpChs1 = gridView1.GetRowCellValue(k, "Chs").ToString();
                                if (!shpChs.Equals("Y"))
                                {
                                    continue;
                                }
                                shpEntrySelect +=
                                    ",'" + gridView1.GetRowCellValue(k, "Entry").ToString() +
                                    "'";
                            }

                            string sqlCmd00 = //查询零售单有 数量为正金额为负或数量为负金额为正的单据
                                "SELECT T1.DocNum,T2.LineNum,T3.ItemCode,T3.ItemName,T2.Qty1+T2.Qty2 Qty,T2.LineTotal FROM OITM T3 INNER JOIN SAL1 T2 ON T3.DocEntry=T2.ItemEntry AND (((T2.Qty1+T2.Qty2)<0 AND T2.LineTotal>0) OR ((T2.Qty1+T2.Qty2)>0 AND T2.LineTotal<0)) " +
                                "INNER JOIN OSAL T1 ON T1.DocEntry=T2.DocEntry and 10000+T1.shpEntry in(" +
                                shpEntrySelect.Substring(1) + ") and CONVERT(DATE,T1.ExitDate) >=CONVERT(DATE, '" +
                                stDate1.ToString("yyyy-MM-dd") +
                                "',121) AND CONVERT(DATE,T1.ExitDate )<= CONVERT(DATE,'" +
                                enDate1.ToString("yyyy-MM-dd") + "',121) ";
                            DataTable oDts = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd00,
                                null);
                            if (oDts.Rows.Count > 0)
                            {
                                ShowFormHandler delShowForm = new ShowFormHandler(showErrWindow);
                                this.BeginInvoke(delShowForm, new Object[] {oDts});
                                button2.Text = "开始同步";
                                Common.Common.weblogOutSAP(shpCode, Program.oUSER); //断开Web SAP 连接
                                return;
                            }

                        }

                        #endregion
                        string obj = "单据";
                        if (Program.DocToSAP.Equals("零售单") && oCheckEdit.Name.Equals("OSALToSAP"))
                        {
                            SAPDI = "Documents";
                            SAPDITableName = "OSAL";
                            string sqlCmd1 =
                                "SELECT T1.ERPobject+T1.SAPobject oChedit, T1.* FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_Fwobject T2 ON t1.ERPobject=T2.FWCode WHERE T2.MasterTb='OSAL'";
                            DataTable tmpTable =
                                Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null);

                            foreach (DataRow oDataRow in tmpTable.Rows)
                            {
                                sqlCmd1 =
                                    "SELECT * FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface WHERE ERPobject+SAPobject = '" +
                                    oDataRow["oChedit"] + "'";
                                DataTable oMasDt1 = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString,
                                    sqlCmd1, DispMessage);
                                oCheckEdit.Name = oDataRow["oChedit"].ToString();
                                DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  获取规则对象" +
                                                       Environment.NewLine);
                                DispMessage.ScrollToCaret();


                                DataSet tmpDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit, shpCode,
                                    Program.InterFaceSet, Program.CorpSet, Program.shpCode,
                                    stDate1.ToString("yyyy-MM-dd"),
                                    enDate1.ToString("yyyy-MM-dd"), "ToSAP_OSHP_"+Program.oUSER, BPLID, DispMessage, SAPDITableName,"0");

                                oDataSet.Merge(tmpDataSet, true);
                                oDataSet.AcceptChanges();
                            }

                            oCheckEdit.Name = "OSALToSAP";
                        }
                        else
                        {
                            string sqlCmd =
                                "SELECT * FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject=T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface=T3.DIInterface WHERE ERPobject+SAPobject = '" +
                                oCheckEdit.Name + "'";
                            var oMasDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,
                                DispMessage);
                            obj = oMasDt.Rows[0]["DIobjType"].ToString();

                            DispMessage.AppendText(
                                Program.DispWhsShp + ":" + shpCode + "  获取规则对象" + Environment.NewLine);
                            DispMessage.ScrollToCaret();

                            SAPDIinterface = oMasDt.Rows[0]["SAPDIinterface"].ToString().Trim();
                            SAPDI = oMasDt.Rows[0]["DI"].ToString().Trim();

                            SAPDITableName = oMasDt.Rows[0]["DTable"].ToString().Trim();
                            ERPobject = oMasDt.Rows[0]["ERPobject"].ToString().Trim();
                            SAPobject = oMasDt.Rows[0]["SAPobject"].ToString().Trim();
                            oDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit, shpCode,
                                Program.InterFaceSet, Program.CorpSet, Program.shpCode, stDate1.ToString("yyyy-MM-dd"),
                                enDate1.ToString("yyyy-MM-dd"), "ToSAP_OSHP_"+Program.oUSER, BPLID, DispMessage, null,"0");
                        }


                        if (oDataSet.Tables[0].Rows.Count == 0)
                        {
                            continue;
                        }

                        int objecttype = 0;
                        switch (SAPDI)
                        {
                            case "Documents":
                            case "JournalEntries":
                                if (!SAPDIinterface.Equals(""))
                                {
                                    BoObjectTypes oBoObjectTypes =
                                        (BoObjectTypes) Enum.Parse(typeof(BoObjectTypes), SAPDIinterface);
                                    objecttype = (int) (oBoObjectTypes);
                                }

                                Common.Common.sendDataToWeb(Program.serviceurl, objecttype.ToString(), obj, oDataSet,
                                    shpCode, shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName,
                                    DispMessage, BPLID);
                                break;
                            case "StockTransfer":
                                Common.Common.sendDataToWeb(Program.serviceurl, "67", "库存转储", oDataSet, shpCode,
                                    shpEntry, oCheckEdit, ERPobject, SAPobject, SAPDB, SAPDITableName, DispMessage,
                                    BPLID);
                                break;
                        }

                        oDataSet.Dispose();
                    }

                    DispMessage.SelectionColor = Color.Black;
                    DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  数据同步完成!" + Environment.NewLine);
                    DispMessage.ScrollToCaret();
                    Common.Common.weblogOutSAP(shpCode, Program.oUSER); //断开Web SAP 连接
                }
                catch (Exception exception)
                {
                    DispMessage.SelectionColor = Color.OrangeRed;
                    DispMessage.AppendText("Try Error:" + exception.Message + Environment.NewLine);
                    DispMessage.ScrollToCaret();
                    button2.Text = "开始同步";
                    Common.Common.weblogOutSAP(null, Program.oUSER);
                    Program.thread.Abort();
                }
            }

            DispMessage.SelectionColor = Color.Black;
            DispMessage.AppendText("  数据同步完成!" + Environment.NewLine);
            DispMessage.ScrollToCaret();
            button2.Text = "开始同步";
            Program.thread.Abort();
        }


        public delegate void ShowFormHandler(DataTable oDataTable);

        private void showErrWindow(DataTable oDataTable)
        {
            OSALErr osalErr = new OSALErr();

            OSALErr.ODataTable = oDataTable.Copy();
            OSALErr.ODataTable.AcceptChanges();
            osalErr.ShowDialog(OMainWin32Window);
        }

        private  void allToSAP()
        {
            DateTime stDate1, enDate1;
            this.button2.Text = "停止同步";
            try
            {
                stDate1 = DateTime.Parse(stDate.Text);
                enDate1 = DateTime.Parse(enDate.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("开始日期或结束日期未选择,请选择!");
                return;
            }
            try
            {
               Common.Common.GetFWSqlConn(this);
            string shpCode = string.Empty, shpEntry = string.Empty, BPLID = string.Empty;
               
                //检查是旧料成本模式为销售发货模式时是否做了成本核定

                DispMessage.AppendText( " 正在检查成本核定!" + Environment.NewLine);
            DispMessage.ScrollToCaret();
            string sqlCmd02 = "SELECT T1.StartDate FROM OCAG2 T1 INNER JOIN CAG3 T2 ON T2.DocEntry = T1.DocEntry  " +
                              "AND '"+stDate.Text+"' BETWEEN T1.StartDate AND T1.EndDate " +
                              "AND '"+enDate.Text+"' BETWEEN T1.StartDate AND T1.EndDate";
            DataTable oCagTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd02, null);
            if (oCagTable.Rows.Count == 0 && Program.oLDMat.Equals("销售退货模式"))
            {
                DispMessage.SelectionColor = Color.Red;
                DispMessage.AppendText(" 未做成本核定,请先核定成本再同步数据!" + Environment.NewLine);
                button2.Text = "开始同步";
                MessageBox.Show("未做成本核定,请先核定成本再同步数据!");
                return;}
            //成本核定检查完成

            DispMessage.AppendText(" 正在检查零售单!" + Environment.NewLine);
            DispMessage.ScrollToCaret();
            //检查零售数据,存在数量为负金额为正  数量为正 金额为负的单据
            string shpEntrySelect = "";
            Common.Common.GetFWSqlConn(null);
               sqlCmd02 = "";
               Common.Common.CreateSHPTable(Program.oUSER);
            for (int k = 0; k < gridView1.RowCount; k++)  //获取选中店铺
            {
                if(Program.threadDie==1) { Program.thread.Abort(); return; }
               string shpChs =gridView1.GetRowCellValue(k, "Chs").ToString();
              
               if (!shpChs.Equals("Y"))
               {
                   continue;
               }

                string shpCode1 = gridView1.GetRowCellValue(k, "WhsCode").ToString();
                string shpName1 = gridView1.GetRowCellValue(k, "WhsName").ToString();
                string shpEntry1 = gridView1.GetRowCellValue(k, "Entry").ToString();
                string BPLID1 = Common.Common.GetBPLID(shpCode1, shpEntry1);
                    shpCode = shpCode1;
                string SAPDB = Common.Common.GetSAPDBName(Program.FwSqlConnection, shpCode);
                sqlCmd02 = "INSERT ToSAP_OSHP_" + Program.oUSER + "(shpEntry,shpCode,shpName,BPLID,SAPCompany) VALUES('" + shpEntry1 + "','" + shpCode1 + "','" + shpName1 + "','" + BPLID1 + "','"+SAPDB+"')";
                Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd02);

               shpEntrySelect +=
                    ",'" + shpEntry1 + "'";
            }

            string sqlCmd00 =   //查询零售单有 数量为正金额为负或数量为负金额为正的单据
                "SELECT T1.DocNum,T2.LineNum,T3.ItemCode,T3.ItemName,T2.Qty1+T2.Qty2 Qty,T2.LineTotal FROM OITM T3 INNER JOIN SAL1 T2 ON T3.DocEntry=T2.ItemEntry AND (((T2.Qty1+T2.Qty2)<0 AND T2.LineTotal>0) OR ((T2.Qty1+T2.Qty2)>0 AND T2.LineTotal<0)) " +
                "INNER JOIN OSAL T1 ON T1.DocEntry=T2.DocEntry  and  10000+T1.shpEntry in(" +
                shpEntrySelect.Substring(1) + ") and CONVERT(DATE,T1.ExitDate) >=CONVERT(DATE, '" +
                stDate1.ToString("yyyy-MM-dd") + "',121) AND CONVERT(DATE,T1.ExitDate )<= CONVERT(DATE,'" +
                enDate1.ToString("yyyy-MM-dd") + "',121) ";
            DataTable oDts = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd00, null);
            if (oDts.Rows.Count > 0)
            {
                ShowFormHandler delShowForm = new ShowFormHandler(showErrWindow);
                this.BeginInvoke(delShowForm, new Object[] { oDts });
                button2.Text = "开始同步";
                DispMessage.SelectionColor = Color.Red;
                DispMessage.AppendText(" 存在非正常零售单据，请修正!" + Environment.NewLine);
                return;
            }
            //零售单正负检查完成
            //查询所有同步规则
            bool ReslutLog =  Common.Common.WebLogSAP(shpCode, Program.oUSER, DispMessage);
            if (ReslutLog == false)
            {
                button2.Text = "开始同步";
                Common.Common.weblogOutSAP(null, Program.oUSER);
                Program.thread.Abort();
                return;
            };
            DataSet oDataSet = new DataSet();
               oDataSet = ALLToSAPGetData("ToSAP_OSHP_" + Program.oUSER, stDate1.ToString("yyyy-MM-dd"),
                   enDate1.ToString("yyyy-MM-dd"), DispMessage);

            Common.Common.sendDataToWeb(Program.serviceurl, "", "单据", oDataSet, "", "0", checkAll3, "", "", "", "ODOC", DispMessage, "");
                DispMessage.SelectionColor = Color.Black;
                DispMessage.AppendText(Program.DispWhsShp + ":" + shpCode + "  数据同步完成!" + Environment.NewLine);
                DispMessage.ScrollToCaret();
                Common.Common.weblogOutSAP(null, Program.oUSER); //断开Web SAP 连接
               button2.Text = "开始同步";
            }
            catch (Exception e)
            {
                DispMessage.SelectionColor = Color.OrangeRed;
                DispMessage.AppendText("同步错误:" + e.Message + Environment.NewLine);
                DispMessage.ScrollToCaret();
                button2.Text = "开始同步";
                Common.Common.weblogOutSAP(null, Program.oUSER);
                Program.thread.Abort();
            };
        }
        private DataSet ALLToSAPGetData (string shpEntryTable,string stDate,string enDate,RichTextBox oRichTextBox)
        {
            Common.Common.GetFWSqlConn(this);
            DataSet shpDataSet = new DataSet();
            string sqlCmd00 = String.Empty;
            if (checkAll3.Checked)
            {
                sqlCmd00 =
                    "select T1.DocEntry,T1.ERPobject,T1.SAPobject,T2.FWName,T3.SAPobjName from ToSAP_Role_Mas T1 " +
                    " inner join ToSAP_Fwobject T2 on T1.ERPobject=T2.FWCode and T2.DataType='单据' " +
                    " inner join ToSAP_Diobject T3 on T1.SAPobject=T3.SAPobjCode where T3.SAPobjName not like '%集团%' ORDER BY T1.ToOrder";
            }

            if (checkCorp.Checked)
            {
                if (!sqlCmd00.Equals(String.Empty))
                {
                    sqlCmd00 += " union all";
                }
                sqlCmd00 +=
                    "select T1.DocEntry,T1.ERPobject,T1.SAPobject,T2.FWName,T3.SAPobjName from ToSAP_Role_Mas T1 " +
                    " inner join ToSAP_Fwobject T2 on T1.ERPobject=T2.FWCode and T2.DataType='单据' " +
                    " inner join ToSAP_Diobject T3 on T1.SAPobject=T3.SAPobjCode where T3.SAPobjName like '%集团%' ORDER BY T1.ToOrder";
            }

            if (checkAll3.CheckState == CheckState.Checked || checkCorp.CheckState == CheckState.Checked || (!Program.checkList.Equals(String.Empty)))
            {
                
                sqlCmd00 = "select T1.DocEntry,T1.ERPobject,T1.SAPobject,T2.FWName,T3.SAPobjName from ToSAP_Role_Mas T1 " +
                           " inner join ToSAP_Fwobject T2 on T1.ERPobject=T2.FWCode and T2.DataType='单据' " +
                           " inner join ToSAP_Diobject T3 on T1.SAPobject=T3.SAPobjCode where T1.ERPobject+T1.SAPobject in("+Program.checkList.Substring(1)+") ORDER BY T1.ToOrder";
            }
            DataTable oDts = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd00, null);
            foreach (DataRow oDtsRow in oDts.Rows)
            {
                
                string erpObjName = oDtsRow["FWName"].ToString();
                string sapObjName = oDtsRow["SAPobjName"].ToString();
                CheckEdit oCheckEdit = new CheckEdit();
                oCheckEdit.Name = oDtsRow["ERPobject"].ToString() + oDtsRow["SAPobject"].ToString();
                oCheckEdit.Text = erpObjName + "-->" + sapObjName;

                oRichTextBox.SelectionColor = Color.Black;
                oRichTextBox.AppendText(" 正在查询数据：" + erpObjName + "-->" + sapObjName + Environment.NewLine);
                DataSet tmpDataSet = Common.Common.GetImportData(Program.FwSqlConnection, oCheckEdit, "ALL",
                    Program.InterFaceSet, Program.CorpSet, Program.shpCode, stDate,enDate, shpEntryTable, "", oRichTextBox, "ODOC","0");
                shpDataSet.Merge(tmpDataSet);
                shpDataSet.AcceptChanges();
                if (Program.threadDie == 1) { Program.thread.Abort(); return shpDataSet; }
            }
            return shpDataSet;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ChoosGroupCode.ORichTextBox = DispMessage;
            ChoosGroupCode.ShpView = gridView1;
            ChoosGroupCode oCode = new ChoosGroupCode();
            oCode.ShowDialog(this);
        }



        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.RowHandle==-1)
            {
                return;
            }
            if (e.Column.Name.Equals("AreaS"))
            {
                string AreaName = gridView1.GetRowCellValue(e.RowHandle, "Area1").ToString();
                string CurrChoose = gridView1.GetRowCellValue(e.RowHandle, "AreaS").ToString();
                DataRow[] oRow = WhsData.Select("Area1 = '"+AreaName+"'");
                string isChs = CurrChoose.Equals("Y") ? "N" : "Y";
                for (int i = 0; i < oRow.Length; i++)
                {
                    string Entry =oRow[i]["Entry"].ToString();
                    WhsData.Rows.Find(Entry)["AreaS"] = isChs;
                    WhsData.Rows.Find(Entry)["chs"] = isChs;
                    
                }

            }

            
            if (e.Column.Name.Equals("Chs"))
            {
                
                string CurrChoose = gridView1.GetRowCellValue(e.RowHandle, "Chs").ToString();
                string isChs = CurrChoose.Equals("Y") ? "N" : "Y";
                string Entry = gridView1.GetRowCellValue(e.RowHandle, "Entry").ToString();
                WhsData.Rows.Find(Entry)["chs"] = isChs;
                WhsData.Rows.Find(Entry)["AreaS"] = isChs;

            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //if (e.Column.Name.Equals("AreaS"))
            //{
            //    string AreaName = gridView1.GetRowCellValue(e.RowHandle, "Area1").ToString();
            //    string CurrChoose = gridView1.GetRowCellValue(e.RowHandle, "AreaS").ToString();
            //    DataRow[] oRow = WhsData.Select("Area1 = '" + AreaName + "'");
            //    string isChs = CurrChoose.Equals("Y") ? "N" : "Y";
            //    for (int i = 0; i < oRow.Length; i++)
            //    {
            //        string Entry = oRow[i]["Entry"].ToString();
            //        WhsData.Rows.Find(Entry)["AreaS"] = isChs;
            //        WhsData.Rows.Find(Entry)["chs"] = isChs;

            //    }

            //}


            //if (e.Column.Name.Equals("Chs"))
            //{
            //    string CurrChoose = gridView1.GetRowCellValue(e.RowHandle, "Chs").ToString();
            //    string isChs = CurrChoose.Equals("Y") ? "N" : "Y";
            //    string Entry = gridView1.GetRowCellValue(e.RowHandle, "Entry").ToString();
            //    WhsData.Rows.Find(Entry)["chs"] = isChs;

            //}
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView detailGrid = (sender as GridView);
            GridHitInfo hitInfo = (detailGrid.CalcHitInfo((e as MouseEventArgs).Location));
            if (hitInfo.Column.Name.Equals("Chs") && hitInfo.RowHandle<0)

            {
                
                string isChs = gridView1.GetRowCellValue(0, "Chs").ToString().Equals("Y") ? "N" : "Y";
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    WhsData.Rows[i]["Areas"] = isChs;
                    WhsData.Rows[i]["Chs"] = isChs;

                }
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            //if(e.Button == MouseButtons.Left && e.Clicks == 2)
            //{
            //    GridHitInfo oGrid = gridView1.CalcHitInfo(e.X, e.Y);
            //    if (oGrid.Column.Name.Equals("Chs"))
            //    {
            //        string isChs = gridView1.GetRowCellValue(0,"Chs").ToString().Equals("Y") ? "N" : "Y";
            //        for (int i = 0; i < gridView1.DataRowCount; i++)
            //        {
            //            WhsData.Rows[i]["Areas"] = isChs;
            //            WhsData.Rows[i]["Chs"] = isChs;

            //        }
            //    }
            //}
        }
    }

}
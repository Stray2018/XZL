﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ToSAP
{
    public partial class RunQuery : DevExpress.XtraEditors.XtraForm
    {
        public RunQuery()
        {
            InitializeComponent();
        }

        private void RunQuery_Load(object sender, EventArgs e)
        {
            Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
        }
    }
}
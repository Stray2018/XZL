﻿using System.ComponentModel;

namespace ToSAP.Common
{
    public partial class JOBdetail : Component
    {
        public JOBdetail()
        {
            InitializeComponent();
        }

        public JOBdetail(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using DevExpress.Accessibility;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using Newtonsoft.Json.Linq;
using DataRow = System.Data.DataRow;
using System.Net;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using LZ4;
using Newtonsoft.Json;
using SAPbobsCOM;
using Exception = System.Exception;
using LZ4Codec = LZ4ps.LZ4Codec;
using Message = System.Windows.Forms.Message;
using DevExpress.DataProcessing.InMemoryDataProcessor;
using DevExpress.XtraRichEdit.Import.Html;

namespace ToSAP.Common
{
    internal class Common
    {
        public static bool TruncateTable(SqlTransaction oTransaction, SqlConnection oSqlConnection,
            string TableName)
        {
            SqlCommand oSqlCommand = oSqlConnection.CreateCommand();
            oSqlCommand.CommandText = "TRUNCATE TABLE " + TableName;
            if (oTransaction != null)
            {
                oSqlCommand.Transaction = oTransaction;
            }
            oSqlCommand.ExecuteNonQuery();
            return true;
        }

        public static bool IsInt(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }

        public static bool InsertDataToTable(SqlTransaction oTransaction, SqlConnection oSqlConnection,
            DataTable oDataTable, string TableName)
        {
            try
            {
                SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(oSqlConnection, SqlBulkCopyOptions.Default,
                    oTransaction == null ? null : oTransaction);
                oSqlBulkCopy.BulkCopyTimeout = 0;

                oSqlBulkCopy.DestinationTableName = TableName;
                //foreach (DataColumn oColumn in oDataTable.Columns)
                //{
                //    string columnName = oColumn.ColumnName;
                //    oSqlBulkCopy.ColumnMappings.Add(columnName, columnName);
                //}
                oSqlBulkCopy.WriteToServer(oDataTable);
                oSqlBulkCopy.Close();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }

            
        }

        public static bool InsertDataToTable(SqlConnection oSqlConnection, DataTable oDataTable,
            string TableName)
        {
            SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(oSqlConnection, SqlBulkCopyOptions.KeepIdentity, null);

            oSqlBulkCopy.DestinationTableName = TableName;
            foreach (DataColumn oColumn in oDataTable.Columns)
            {
                string columnName = oColumn.ColumnName;
                oSqlBulkCopy.ColumnMappings.Add(columnName, columnName);
            }

            string sql = "SET IDENTITY_INSERT " + TableName + " ON";
            ExecSqlcmd(oSqlConnection, sql);



            oSqlBulkCopy.WriteToServer(oDataTable);
            oSqlBulkCopy.Close();
            sql = "SET IDENTITY_INSERT " + TableName + " off";
            ExecSqlcmd(oSqlConnection, sql);
            return true;
        }

        public static int ExecSqlcmd(SqlConnection oSqlConnection, string Sqlcmd)
        {
            int Rows = 0;
            SqlCommand oSqlCommand = oSqlConnection.CreateCommand();
            oSqlCommand.CommandText = Sqlcmd;
            oSqlCommand.CommandTimeout = 6000;
           Rows= oSqlCommand.ExecuteNonQuery();
            oSqlCommand.Dispose();
            return Rows;
        }

        public static int
        ExecSqlcmd(SqlTransaction oSqlTransaction, SqlConnection oSqlConnection, string Sqlcmd)
        {
            int Rows = 0;
            SqlCommand oSqlCommand = oSqlConnection.CreateCommand();
            oSqlCommand.CommandText = Sqlcmd;
            oSqlCommand.Transaction = oSqlTransaction;
           Rows= oSqlCommand.ExecuteNonQuery();
            oSqlCommand.Dispose();
            return Rows;
        }

        public static bool GetFWSqlConn(Form parForm)
        {
            if (Program.FwSqlConnection.State == ConnectionState.Open)
            {
                return true;
            }
            FileInfo oFileInfo = new FileInfo(Application.StartupPath + "/ToSAP.ini");
            if (oFileInfo.Exists)
            {
                oFileInfo = null;
                StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
                string connStr = oStReader.ReadToEnd();
                oStReader.Close();
                oStReader.Dispose();
                JObject ToSAPJson = JObject.Parse(connStr);
                Program.FWSqlConnStr.DataSource = ((JObject) ToSAPJson["FW"])["FWDBserver"].ToString().Trim();
                Program.FWSqlConnStr.InitialCatalog = ((JObject) ToSAPJson["FW"])["FWDB"].ToString().Trim();
                Program.FWSqlConnStr.UserID = ((JObject) ToSAPJson["FW"])["FWDBuser"].ToString().Trim();
                string fwPwd = ((JObject) ToSAPJson["FW"])["FWDBpwd"].ToString().Trim();
                Program.FWSqlConnStr.Password = Decrypt(fwPwd);
                Program.FWSqlConnStr.IntegratedSecurity = false;
                Program.FWSqlConnStr.ConnectTimeout = 60000;
                Program.FWSqlConnStr.MaxPoolSize = 200;
                Program.FwSqlConnection.ConnectionString = Program.FWSqlConnStr.ConnectionString;
               

                try
                {
                    Program.FwSqlConnection.Open();
                    return true;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    ToSAP.Set.mSet oSet = new ToSAP.Set.mSet();
                    oSet.Top = Screen.PrimaryScreen.Bounds.Height / 2 - oSet.Height / 2;
                    oSet.Left = Screen.PrimaryScreen.Bounds.Width / 2 - oSet.Width / 2;
                    if (parForm != null)
                    {
                        oSet.ShowDialog(parForm);
                    }
                    return false;
                }
            }
            {
                ToSAP.Set.mSet oSet = new ToSAP.Set.mSet();
                oSet.Top = Screen.PrimaryScreen.Bounds.Height / 2 - oSet.Height / 2;
                oSet.Left = Screen.PrimaryScreen.Bounds.Width / 2 - oSet.Width / 2;
                oSet.ShowDialog(parForm);
                return false;
            }
        }

        public static string Encrypt(string str)
        {
            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            byte[] key = Encoding.UTF8.GetBytes("Stray520");

            byte[] data = Encoding.UTF8.GetBytes(str);

            MemoryStream MStream = new MemoryStream();


            CryptoStream CStream = new CryptoStream(MStream, descsp.CreateEncryptor(key, key), CryptoStreamMode.Write);

            CStream.Write(data, 0, data.Length);

            CStream.FlushFinalBlock();

            return Convert.ToBase64String(MStream.ToArray());
        }

        public static string Decrypt(string str)
        {
            DESCryptoServiceProvider descsp = new DESCryptoServiceProvider();

            byte[] key = Encoding.UTF8.GetBytes("Stray520");

            byte[] data = Convert.FromBase64String(str);

            MemoryStream MStream = new MemoryStream();


            CryptoStream CStream = new CryptoStream(MStream, descsp.CreateDecryptor(key, key), CryptoStreamMode.Write);

            CStream.Write(data, 0, data.Length);

            CStream.FlushFinalBlock();

            return Encoding.UTF8.GetString(MStream.ToArray());
        }

        public static bool IsExistWindow(string formname, Form oForms)
        {
            foreach (Form mdiChild in oForms.MdiChildren)
            {
                if (mdiChild.Name.Equals(formname))
                {
                    mdiChild.Focus();
                    return true;
                }
            }
            return false;
        }

        public static DataTable GetFWDB(SqlConnection oSqlConnection)
        {
            DataTable FWDB = new DataTable("FWDB");
            FWDB.Columns.Add("公司名称", typeof(string));
            FWDB.Columns.Add("数据库", typeof(string));
            FWDB.Columns.Add("版本", typeof(string));
            FWDB.Columns.Add("创建日期", typeof(string));

            string sqlcmd =
                "SELECT name AS 'DBName' FROM  sys.sysdatabases WHERE 1=1 and name NOT IN('master','tempdb','model','msdb','SBO-COMMON')";
            DataTable DBall = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlcmd,null);
            foreach (DataRow dbname in DBall.Rows)
            {
                string sqlcmd2 = "SELECT name FROM [" + dbname[0] + "].sys.tables WHERE 1=1 and name = 'COMP'";
                DataTable checkTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlcmd2,null);
                if (checkTable.Rows.Count > 0)
                {
                    string sqlcmd3 = "SELECT CompanyName,CreateDate,Version FROM [" + dbname[0] +"].dbo.COMP where 1=1";
                    DataTable FWdbtemp = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlcmd3,null);
                    DataRow oDataRow = FWDB.NewRow();
                    oDataRow[0] = FWdbtemp.Rows[0]["CompanyName"];
                    oDataRow[1] = dbname[0];
                    oDataRow[2] = FWdbtemp.Rows[0]["Version"];
                    oDataRow[3] = FWdbtemp.Rows[0]["CreateDate"];
                    FWDB.Rows.Add(oDataRow);
                }
            }

            return FWDB;
        }

        public static void saveConfigIni(string FWDBserver,string FWDBuser,string FWDBpwd,string FWDB,string SAPDBTYPE,string SAPLicensesever,string SAPDBserver,string SAPDBuser,string SAPDBpwd,string SAPDB,string SAPUSER,string SAPPWD)
        {
            var oFileInfo = new FileInfo(Application.StartupPath + "/ToSAP.ini");
            if (oFileInfo.Exists)
            {
                oFileInfo.Delete();
            }
            oFileInfo = null;
            var ToSAPCon = new JObject();
            ToSAPCon.Add(
                new JProperty("FW",
                    new JObject(
                        new JProperty("FWDBserver", FWDBserver.Trim()),
                        new JProperty("FWDBuser", FWDBuser.Trim()),
                        new JProperty("FWDBpwd", Encrypt(FWDBpwd.Trim())),
                        new JProperty("FWDB", FWDB.Trim())
                    )
                )
            );
            ToSAPCon.Add(
                new JProperty("SAP",
                    new JObject(
                        new JProperty("SAPDBTYPE", SAPDBTYPE.Trim()),
                        new JProperty("SAPDBserver", SAPDBserver.Trim()),
                        new JProperty("SAPDBuser", SAPDBuser.Trim()),
                        new JProperty("SAPDBpwd", Encrypt(SAPDBpwd.Trim())),
                        new JProperty("SAPLicensesever", SAPLicensesever.Trim()),
                        new JProperty("SAPDB", SAPDB),
                        new JProperty("SAPUSER", SAPUSER.Trim()),
                        new JProperty("SAPPWD", Encrypt(SAPPWD.Trim()))
                    ))
            );

            var oFileWriter = new StreamWriter(Application.StartupPath + "/ToSAP.ini");
            oFileWriter.Write(ToSAPCon.ToString());
            oFileWriter.Flush();
            oFileWriter.Close();
            oFileWriter.Dispose();
        }
        public static DataTable GetDataTable(string Connstr, string sqlcmd,RichTextBox oRichTextBox)
        {
            sendMessage(oRichTextBox,DateTime.Now.ToString()+"正在执行查询:"+sqlcmd,false);
            using (SqlConnection oConnection = new SqlConnection(Connstr))
            {
                oConnection.Open();
                DataTable oDataTable = new DataTable("MYdata");
                //SqlCommand oConn = oConnection.CreateCommand();
                //oConn.CommandText = sqlcmd;
                //oConn.CommandTimeout = 0;
                //SqlDataReader DataReader = oConn.ExecuteReader();
                //oDataTable.Load(DataReader);
                //DataReader.Close();
                SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(sqlcmd, oConnection);
                oSqlDataAdapter.SelectCommand.CommandTimeout = 0;
                oSqlDataAdapter.Fill(oDataTable);
                oSqlDataAdapter.Dispose();
                oConnection.Close();
                sendMessage(oRichTextBox, DateTime.Now.ToString() + "查询完成" , false);
                return oDataTable;
            }
        }


        /// <summary> Convert XML Document to XDocument </summary>
        /// <param name="xmlDocument">Attached XML Document</param>
        public static XDocument fwToXDocument(XmlDocument xmlDocument)
        {
            using (XmlNodeReader xmlNodeReader = new XmlNodeReader(xmlDocument))
            {
                xmlNodeReader.MoveToContent();
                return XDocument.Load(xmlNodeReader);
            }
        }

        public static DataTable
        clipboardExcelToDataTable(bool blnFirstRowHasHeader = true)
        {
            string strTime = "S " + DateTime.Now.ToString("mm:ss:fff");
            IDataObject clipboard = Clipboard.GetDataObject();
            if (!clipboard.GetDataPresent("XML Spreadsheet"))
            {
                return null;
            }
            strTime += "\r\nRead " + DateTime.Now.ToString("mm:ss:fff");
            StreamReader streamReader = new StreamReader((MemoryStream) clipboard.GetData("XML Spreadsheet"));
            strTime += "\r\nFinish read " + DateTime.Now.ToString("mm:ss:fff");
            streamReader.BaseStream.SetLength(streamReader.BaseStream.Length - 1);

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(streamReader.ReadToEnd());
            strTime += "\r\nRead XML Document " + DateTime.Now.ToString("mm:ss:fff");

            string ssNs = "urn:schemas-microsoft-com:office:spreadsheet";
            DataTable dtData = new DataTable();

            var linqRows = fwToXDocument(xmlDocument).Descendants(ssNs + "Row").ToList();

            for (int x = 0; x < linqRows.Max(a => a.Descendants(ssNs + "Cell").Count()); x++)
            {
                dtData.Columns.Add("Column {0}" + (x + 1));
            }
            int intCol = 0;

            DataRow drCurrent;

            linqRows.ForEach(rowElement =>
                {
                    intCol = 0;
                    drCurrent = dtData.Rows.Add();
                    rowElement.Descendants(ssNs + "Cell")
                    .ToList()
                    .ForEach(cell => drCurrent[intCol++] = cell.Value);
                });

            if (blnFirstRowHasHeader)
            {
                var x = 0;
                foreach (DataColumn dcCurrent in dtData.Columns)
                {
                    dcCurrent.ColumnName = dtData.Rows[0][x++].ToString();
                }
                dtData.Rows.RemoveAt(0);
            }

            strTime += "\r\nF " + DateTime.Now.ToString("mm:ss:fff");

            return dtData;
        }

        public static bool InsertDataToDataSource(GridView oGridView, GridControl oGridControl)
        {
            DataTable GrdDt = (DataTable) oGridControl.DataSource;

            DataTable oDataTable = clipboardExcelToDataTable();
            if (oDataTable == null)
            {
                return false;
            }
            int oldRowsCount = GrdDt.Rows.Count;
            for (int i = 0; i < oDataTable.Columns.Count; i++)
            {
                int CurrRowsCount = oldRowsCount - 1;
                string CurrCOLName = string.Empty;
                int colindwx = 0;
                for (int j0 = 0; j0 < oGridView.Columns.Count; j0++)
                {
                    if (oGridView.Columns[j0].Caption == oDataTable.Columns[i].ColumnName)
                    {
                        CurrCOLName = oGridView.Columns[j0].FieldName;
                        colindwx = j0;
                        break;
                    }
                }
                if (CurrCOLName.Equals(string.Empty))
                {
                    MessageBox.Show("表中不存在以下列:" + oDataTable.Columns[i].ColumnName + "\n请确认EXCEL中列标题是否正确!");
                    return false;
                }

                for (int j = 0; j < oDataTable.Rows.Count; j++)
                {
                    CurrRowsCount += 1;
                    if (i == 0)
                    {
                        DataRow oDataRow = GrdDt.NewRow();
                        GrdDt.Rows.Add(oDataRow);
                    }

                    GrdDt.Rows[CurrRowsCount - 1][CurrCOLName] = oDataTable.Rows[j][i].ToString().Trim();
                }
            }

            return true;
        }

        public static DataTable GetAuthorData(SqlConnection oSqlConnection, string UserCode)
        {
            string sqlCmd =
                "SELECT T1.* FROM OPWD T1 LEFT JOIN OUSR T2 ON T1.UserEntry=T2.DocEntry WHERE 1=1 and T2.UserCode='" +
                UserCode + "'";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            return oDataTable;
        }

        public static string
        GetOldDocentry(SqlConnection oSqlConnection, string oTable, string oUserCode)
        {
            string sqlCmd = "DECLARE @str NVARCHAR(max);" +
                         "SELECT @str = '';" +
                         "WITH Dept AS(" +
                         "SELECT DocEntry, DocEntry oldDocEntry, DepCode, DepName FROM CMP1 WHERE DepLevel = 1 " +
                         "UNION ALL " +
                         "SELECT t1.DocEntry, t2.DocEntry, t2.DepCode, t2.DepName FROM Dept t1 INNER JOIN CMP1 t2 ON t1.DepCode = t2.BaseCode) " +
                         "SELECT @str = @str + ',' + CONVERT(NVARCHAR(100), DocEntry) FROM " + oTable +
                         " WHERE 1=1 and DepEntry IN(SELECT oldDocEntry FROM Dept WHERE CONVERT(VARCHAR(100), DocEntry * -1) IN(SELECT ShpDeploy FROM OPWD INNER JOIN OUSR ON OUSR.DocEntry = OPWD.UserEntry WHERE UserCode = '" +
                         oUserCode + "'));" +
                         "SELECT RIGHT(@str, LEN(@str) - 1); ";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            return oDataTable.Rows[0][0].ToString();
        }

        public static string GetInterFaceMode(SqlConnection oSqlConnection)
        {
            string sqlCmd = "SELECT Value1 FROM ToSAP_Appinit WHERE 1=1 and ParaCode = 'InterFaceMode'";

            return GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null).Rows[0][0].ToString().Trim();
        }

        public static void GetAppInit(SqlConnection oSqlConnection)
        {
            string sqlCmd = "SELECT *  FROM [ToSAP_Appinit] where 1=1";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            if (oDataTable.Rows.Count < 4)
            {
                MessageBox.Show("未做初始化设置,请设置保存后重新登陆!");
                return;
            }

            try
            {
                Program.InterFaceSet = oDataTable.Select("ParaCode ='InterFaceMode'")[0]["Value1"].ToString().Trim();
                Program.CorpSet = oDataTable.Select("ParaCode ='Corp'")[0]["Value1"].ToString().Trim();
                Program.showOWHS = oDataTable.Select("ParaCode ='showOWHS'")[0]["Value1"].ToString().Trim();
                Program.DocToSAP = oDataTable.Select("ParaCode ='DocToSAP'")[0]["Value1"].ToString().Trim();
                Program.Shop = oDataTable.Select("ParaCode ='Shop'")[0]["Value1"].ToString().Trim();

                Program.oLDMat = oDataTable.Select("ParaCode ='oLDMat'")[0]["Value1"].ToString().Trim();
                try
                {
                    Program.ErrOP = oDataTable.Select("ParaCode ='ErrOP'")[0]["Value1"].ToString().Trim();
                }
                catch (Exception e)
                {
                   
                }
               
                StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
                string connStr = oStReader.ReadToEnd();
                oStReader.Close();
                oStReader.Dispose();
                JObject ToSAPJson = JObject.Parse(connStr);
                string serUrl = ((JObject)ToSAPJson["FW"])["FWDBserver"].ToString().Trim();
                if (serUrl.Contains(","))
                {
                    serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
                }
                

                Program.serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";

                //Program.DocBranch = oDataTable.Select("ParaCode ='DocBranch'")[0]["Value1"].ToString().Trim();
            }
            catch (Exception e)
            {
            }
        }


        //public static bool GetSAPConnect(SqlConnection oSqlConnection, string shpCode)
        //{
        //    string SAPDB = GetSAPDBName(oSqlConnection, shpCode);
        //    if (SAPCompany.Connected && SAPCompany.CompanyDB == SAPDB)
        //    {
        //        return true;
        //    }
        //    SAPCompany.Disconnect();

        //    StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
        //    string connStr = oStReader.ReadToEnd();
        //    oStReader.Close();
        //    oStReader.Dispose();
        //    JObject ToSAPJson = JObject.Parse(connStr);
        //    string sqlCmd = "SELECT * FROM [ToSAP_SynUser] where FWuser = '" + Program.oUSER + "'";
        //    DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
        //    if (oDataTable.Rows.Count < 1)
        //    {
        //        MessageBox.Show("未配置同步用户,请配置!");
        //        ToSAP.Init.synUser oNewDocNsyn = new ToSAP.Init.synUser();
        //        oNewDocNsyn.MdiParent = Form.ActiveForm;
        //        oNewDocNsyn.Text = "同步帐号设置";
        //        oNewDocNsyn.Show();
        //        return false;
        //    }

        //    SAPCompany.Server = ((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
        //    string SAPDBTYPE = ((JObject) ToSAPJson["SAP"])["SAPDBTYPE"].ToString().Trim();
        //    switch (SAPDBTYPE.Trim())
        //    {
        //        case "SQL2008":
        //            SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
        //            break;
        //        case "SQL2012":
        //            SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
        //            break;
        //        case "SQL2014":
        //            SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
        //            break;
        //        case "SQL2016":
        //            SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
        //            break;
        //        case "SQL2017":
        //            SAPCompany.DbServerType = BoDataServerTypes.dst_MSSQL2017;
        //            break;
        //        default:
        //            SAPCompany.DbServerType = BoDataServerTypes.dst_HANADB;
        //            break;
        //    }

        //    SAPCompany.DbPassword = Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
        //    SAPCompany.DbUserName = ((JObject) ToSAPJson["SAP"])["SAPDBuser"].ToString().Trim();
        //    SAPCompany.LicenseServer = ((JObject) ToSAPJson["SAP"])["SAPLicensesever"].ToString().Trim();
        //    SAPCompany.SLDServer =
        //        "https://" + SAPCompany.LicenseServer.Replace(":30000", string.Empty) + ":40000";
        //    SAPCompany.CompanyDB = SAPDB;
        //    SAPCompany.UserName = oDataTable.Rows[0]["SAPuser"].ToString().Trim();
        //    SAPCompany.Password = Decrypt(oDataTable.Rows[0]["SAPPwd"].ToString().Trim());
        //    SAPCompany.language = BoSuppLangs.ln_Chinese;

        //    Program.SAPsqlConnStr.DataSource = SAPCompany.Server;
        //    Program.SAPsqlConnStr.UserID = SAPCompany.DbUserName;
        //    Program.SAPsqlConnStr.Password = Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
        //    Program.SAPsqlConnStr.InitialCatalog = SAPDB;

        //    if (SAPCompany.Connect() == 0)
        //    {
        //        SAPCompany.XMLAsString = true;
        //        SAPCompany.XmlExportType = BoXmlExportTypes.xet_ExportImportMode;
        //        return true;
        //    }

        //    MessageBox.Show("SAP 连接失败:\n" + SAPCompany.GetLastErrorDescription());
        //    return false;
        //}

        //public static Company GetSAPCompany(SqlConnection oSqlConnection, string shpCode)
        //{
        //    SAPbobsCOM.Company oCompany = new Company();
        //    string SAPDB = GetSAPDBName(oSqlConnection, shpCode);
        //    StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
        //    string connStr = oStReader.ReadToEnd();
        //    oStReader.Close();
        //    oStReader.Dispose();
        //    JObject ToSAPJson = JObject.Parse(connStr);
        //    string sqlCmd = "SELECT * FROM [ToSAP_SynUser] where FWuser = 'admin'";
        //    DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
        //    oCompany.Server =((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
        //    string SAPDBTYPE = ((JObject) ToSAPJson["SAP"])["SAPDBTYPE"].ToString().Trim();
        //    switch (SAPDBTYPE.Trim())
        //    {
        //        case "SQL2008":
        //            oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
        //            break;
        //        case "SQL2012":
        //            oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
        //            break;
        //        case "SQL2014":
        //            oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
        //            break;
        //        case "SQL2016":
        //            oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
        //            break;
        //        default:
        //            oCompany.DbServerType = BoDataServerTypes.dst_HANADB;
        //            break;
        //    }

        //    oCompany.DbPassword = Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
        //    oCompany.DbUserName = ((JObject) ToSAPJson["SAP"])["SAPDBuser"].ToString().Trim();
        //    oCompany.LicenseServer = ((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
        //    oCompany.SLDServer = "https://" + oCompany.LicenseServer.Replace(":30000", string.Empty) + ":40000";
        //    oCompany.CompanyDB = SAPDB;
        //    oCompany.UserName = oDataTable.Rows[0]["SAPuser"].ToString().Trim();
        //    oCompany.Password = Decrypt(oDataTable.Rows[0]["SAPPwd"].ToString().Trim());
        //    oCompany.language = BoSuppLangs.ln_Chinese;

        //    Program.SAPsqlConnStr.DataSource = oCompany.Server;
        //    Program.SAPsqlConnStr.UserID = oCompany.DbUserName;
        //    Program.SAPsqlConnStr.Password = Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
        //    Program.SAPsqlConnStr.InitialCatalog = SAPDB;

        //    if (oCompany.Connect() == 0)
        //    {
        //        oCompany.XMLAsString = true;
        //        oCompany.XmlExportType = BoXmlExportTypes.xet_ExportImportMode;
        //        return oCompany;
        //    }

        //    return null;
        //}

        public static void GetSAPsqlConn(string shpCode)
        {
            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            Program.SAPsqlConnStr.DataSource = ((JObject) ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            Program.SAPsqlConnStr.UserID = ((JObject)ToSAPJson["SAP"])["SAPDBuser"].ToString().Trim(); ;
            Program.SAPsqlConnStr.Password = Decrypt(((JObject) ToSAPJson["SAP"])["SAPDBpwd"].ToString().Trim());
            Program.SAPsqlConnStr.InitialCatalog = Common.GetSAPDBName(Program.FwSqlConnection,shpCode);
            Program.SAPsqlConnStr.MaxPoolSize = 200;
        }


        public static string GetSAPDBName(SqlConnection oSqlConnection, string shopCode)
        {
            string sqlCmd = string.Empty;
            if (Program.InterFaceSet == "按分支")
            {
                sqlCmd = "SELECT Value1 FROM ToSAP_Appinit WHERE ParaCode='SAPFIDB'";
            }
            else
            {
                sqlCmd = "SELECT BranchCode FROM ToSAP_BranchDetail T1  WHERE T1.WhsCode = '" + shopCode + "'";
            }
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);

            return oDataTable.Rows.Count > 0 ? oDataTable.Rows[0][0].ToString().Trim() : string.Empty;
        }

        public static string
        GetImportFieldsStr(SqlConnection oSqlConnection, string Rolestr, string DataType,RichTextBox oRichTextBox)
        {
            string sqlCmd = "SELECT " +
                         "CASE WHEN  FWField = 'Fixed' or FWField = 'Combfield' THEN  FWFieldDesc  " +
                         "WHEN T2.IsSum = 'Y' THEN 'Sum( T00.'+ T2.FWField +')'" +
                         "ELSE ISNULL(T2.CodePrefix,'') + '+T00.' + FWField end " +
                         "+ ' as '+ SAPDI" +
                         "  FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry  AND T2.DataType = '" +
                         DataType + "'" +
                         "WHERE  T2.KeyField<>'Y' and T1.ERPobject + T1.SAPobject = '" + Rolestr + "' ";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);

            string Result = string.Empty;
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                Result += "," + oDataRow[0].ToString().Trim();
            }
            return Result.Length > 1 ? Result.Substring(1) : Result;
        }

        public static string GetImportFieldsStr(SqlConnection oSqlConnection, string Rolestr, string DataType,
            string ContionSql, string sumImport, string IsSum, string Collect,RichTextBox oRichTextBox)
        {
            string ColAlias = string.Empty;
            string Result = string.Empty;
            string sqlCmd = string.Empty;
            string SQLStr = " and T2.SAPDI <>'notImport' and T2.SAPDI<> 'InCmpBch' and T1.ERPobject + T1.SAPobject = '" + Rolestr + "' ";
            string CodePrefix = string.Empty;
            string oldDataType = DataType;
            switch (DataType)
            {
                case "主表":
                    ColAlias = "T100.";
                    CodePrefix = " ISNULL(T2.CodePrefix, '')";
                    SQLStr += " and T2.KeyField = 'N' and T2.FWDataType <> '日期'  ";
                    sqlCmd =
                        "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3 ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                        Rolestr + "' ";
                   DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);

                    foreach (DataRow oDataRow in oData.Rows)
                    {
                        Result += "," + oDataRow[1] + ColAlias + oDataRow[0] + "," + (sumImport=="Y" ? IsSum : "0")+ ") as '" +
                                  oDataRow[2]+"'";
                    }
                    break;
                case "子表":

                    SQLStr += "  and T2.KeyField = 'N' and T2.SAPDI<>'LineNum' ";
                    break;
                case "序列号":
                    ColAlias = "T102.";
                    SQLStr += " and T2.KeyField='N'";
                    break;
            }

            //if (DataType == "主表" || DataType == "子表")
            //{
            //    if (sumImport == "Y")
            //    {
            //        SQLStr += "  and (T2.IsGroup ='Y' or T2.IsSum='Y')";
            //    }
            //    else
            //    {
            //        SQLStr += " and T2.SAPDI <>'InCmpBch' and T2.SAPDI <>'DocEntry'";
            //    }
            //}
            if (oldDataType.Equals("序列号show") ||oldDataType.Equals("序列号Group") )
            {
                if (Collect == "Y")
                {
                    SQLStr += "  and (T2.IsGroup ='Y') ";
                    DataType = "子表";
                    ColAlias = "T101.";
                }
                else
                {
                    SQLStr += " and T2.KeyField='Y'";
                    DataType = "序列号";
                    ColAlias = "T102.";
                }
            }

            sqlCmd =
                "SELECT T1.IsCollect,T2.FWField,T2.FWFieldDesc,T2.FWDataType,T2.SAPDI,T2.IsSum,T2.IsGroup,T2.KeyField,T2.CodePrefix,T2.IsMasTbl,isnull(T3.SQLstr,'') SQLstr  FROM ToSAP_Role_Mas T1 (noLock) LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry LEFT JOIN ToSAP_ERPDataType T3 ON T2.FWDataType=T3.dataType WHERE 1=1 AND T2.DataType = '" +
                DataType + "' " + SQLStr;
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            if (oldDataType.Equals("序列号Group"))
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string FWField = oDataRow["FWField"].ToString();
                    string IsMasTbl = oDataRow["IsMasTbl"].ToString();
                    if (IsMasTbl.Equals("Y"))
                    { Result += ",T100." + FWField; }
                    else
                    {
                        Result += ",T101." + FWField;
                    }
                    
                }
            }
            else
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ColAlias_new = string.Empty;

                    string FWField = oDataRow["FWField"].ToString();
                    string FWFieldDesc = oDataRow["FWFieldDesc"].ToString();

                    string SAPDI = oDataRow["SAPDI"].ToString();
                    string newIsSum = oDataRow["IsSum"].ToString();
                    oDataRow["IsGroup"].ToString();

                    string newCodePrefix = oDataRow["CodePrefix"].ToString();
                    string IsMasTbl = oDataRow["IsMasTbl"].ToString();
                    string SQLstr = oDataRow["SQLstr"].ToString();
                    string LnkStr = string.Empty;
                    if (SAPDI.Equals("notImport") || oldDataType.Equals("序列号show"))
                    {
                        SAPDI = FWField;
                    }
                    if (DataType.Equals("子表"))
                    {
                        if (IsMasTbl.Equals("Y"))
                        {
                            ColAlias_new = "T100." + FWField;
                        }
                        else
                        {
                            ColAlias_new = "T101." + FWField;
                        }
                    }
                    if (!DataType.Equals("子表"))
                    {
                        ColAlias_new = ColAlias + FWField;
                    }

                    if (FWField.Equals("Combfield"))  //组合字段
                    {
                       
                        ColAlias_new = FWFieldDesc.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.","T103.");

                    }
                    LnkStr = SQLstr + "(" + ColAlias_new + ") as '" + SAPDI+"'";

                    if (FWField.Equals("Qty"))
                    {
                        LnkStr = SQLStr + " as '" + SAPDI+"'";
                    }

                    if (FWField.Equals("Fixed"))
                    {
                        LnkStr = FWFieldDesc + " as '" + SAPDI+"'";
                    }
                    if (newIsSum.Equals("Y"))
                    {
                        LnkStr = "sum(" + ColAlias_new + ") as '" + SAPDI+"'";
                        if (FWField.Equals("Combfield"))  //组合字段
                        {
                            
                            ColAlias_new = FWFieldDesc.Replace("T1.", "T100.").Replace("T2.","T101.").Replace("T3.","T103.");
                            LnkStr = "sum(" + ColAlias_new + ") as '" + SAPDI+"'";
                        }
                        if (FWField.Equals("Qty"))
                        {
                            LnkStr = "sum(" + SQLstr + ") as '" + SAPDI+"'";
                        }

                    }

                    if (!newCodePrefix.Equals(string.Empty))
                    {
                        LnkStr =   newCodePrefix + "+" + LnkStr;
                    }
                    Result += "," + LnkStr;
                }

               // return Result.Length > 1 ? Result.Substring(1) : Result;
            }

            return Result.Length > 1 ? Result.Substring(1) : Result;
        }
        public static string GetImportFieldsStr(SqlConnection oSqlConnection, string Rolestr, string DataType,
    string ContionSql, string sumImport, string IsSum, string Collect, string isDebit, RichTextBox oRichTextBox)
        {
            string ColAlias = string.Empty;
            string Result = string.Empty;
            string sqlCmd = string.Empty;
            string SQLStr = " and T2.SAPDI <>'notImport' and T2.SAPDI<> 'InCmpBch' and T1.ERPobject + T1.SAPobject = '" + Rolestr + "' ";
            string CodePrefix = string.Empty;
            string oldDataType = DataType;
            switch (DataType)
            {
                case "主表":
                    ColAlias = "T100.";
                    CodePrefix = " ISNULL(T2.CodePrefix, '') ";
                    SQLStr += " and T2.KeyField = 'N' and T2.FWDataType <> '日期'  ";
                    sqlCmd =
                        "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3 ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                        Rolestr + "' ";
                    DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, oRichTextBox);

                    foreach (DataRow oDataRow in oData.Rows)
                    {
                        Result += "," + oDataRow[1] + ColAlias + oDataRow[0] + "," + (sumImport == "Y" ? IsSum : "0") + ") as '" +
                                  oDataRow[2] + "'";
                    }
                    break;
                case "子表":

                    SQLStr += "  and T2.KeyField = 'N' and T2.SAPDI<>'LineNum' and T2.SAPDI <> 'PrimaryFormItems' ";
                    break;
                case "序列号":
                    ColAlias = "T102.";
                    SQLStr += " and T2.KeyField='N'";
                    break;
            }

            //if (DataType == "主表" || DataType == "子表")
            //{
            //    if (sumImport == "Y")
            //    {
            //        SQLStr += "  and (T2.IsGroup ='Y' or T2.IsSum='Y')";
            //    }
            //    else
            //    {
            //        SQLStr += " and T2.SAPDI <>'InCmpBch' and T2.SAPDI <>'DocEntry'";
            //    }
            //}
            if (oldDataType.Equals("序列号show") || oldDataType.Equals("序列号Group"))
            {
                if (Collect == "Y")
                {
                    SQLStr += "  and (T2.IsGroup ='Y') ";
                    DataType = "子表";
                    ColAlias = "T101.";
                }
                else
                {
                    SQLStr += " and T2.KeyField='Y'";
                    DataType = "序列号";
                    ColAlias = "T102.";
                }
            }

            sqlCmd =
                "SELECT T1.IsCollect,T2.FWField,T2.FWFieldDesc,T2.FWDataType,T2.SAPDI,T2.IsSum,T2.IsGroup,T2.KeyField,T2.CodePrefix,T2.IsMasTbl,isnull(T3.SQLstr,'') SQLstr  " +
                "FROM ToSAP_Role_Mas T1 (noLock) LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry " +
                "LEFT JOIN ToSAP_ERPDataType T3 ON T2.FWDataType=T3.dataType " +
                "WHERE 1=1 AND T2.isDebit = '"+isDebit+"' and T2.DataType = '" + DataType + "' " + SQLStr;
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, oRichTextBox);
            if (oldDataType.Equals("序列号Group"))
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string FWField = oDataRow["FWField"].ToString();
                    string IsMasTbl = oDataRow["IsMasTbl"].ToString();
                    if (IsMasTbl.Equals("Y"))
                    { Result += ",T100." + FWField; }
                    else
                    {
                        Result += ",T101." + FWField;
                    }

                }
            }
            else
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ColAlias_new = string.Empty;

                    string FWField = oDataRow["FWField"].ToString();
                    string FWFieldDesc = oDataRow["FWFieldDesc"].ToString();

                    string SAPDI = oDataRow["SAPDI"].ToString();
                    string newIsSum = oDataRow["IsSum"].ToString();
                    oDataRow["IsGroup"].ToString();

                    string newCodePrefix = oDataRow["CodePrefix"].ToString();
                    string IsMasTbl = oDataRow["IsMasTbl"].ToString();
                    string SQLstr = oDataRow["SQLstr"].ToString();
                    string LnkStr = string.Empty;
                    if (SAPDI.Equals("notImport") || oldDataType.Equals("序列号show"))
                    {
                        SAPDI = FWField;
                    }
                    if (DataType.Equals("子表"))
                    {
                        if (IsMasTbl.Equals("Y"))
                        {
                            ColAlias_new = "T100." + FWField;
                        }
                        else
                        {
                            ColAlias_new = "T101." + FWField;
                        }
                    }
                    if (!DataType.Equals("子表"))
                    {
                        ColAlias_new = ColAlias + FWField;
                    }

                    if (FWField.Equals("Combfield"))  //组合字段
                    {

                        ColAlias_new = FWFieldDesc.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.");

                    }
                    LnkStr = SQLstr + "(" + ColAlias_new + ") as '" + SAPDI + "'";

                    if (FWField.Equals("Qty"))
                    {
                        LnkStr = SQLStr + " as '" + SAPDI + "'";
                    }

                    if (FWField.Equals("Fixed"))
                    {
                        LnkStr = FWFieldDesc + " as '" + SAPDI + "'";
                    }
                    if (newIsSum.Equals("Y"))
                    {
                        LnkStr = "sum(" + ColAlias_new + ") as '" + SAPDI + "'";
                        if (FWField.Equals("Combfield"))  //组合字段
                        {

                            ColAlias_new = FWFieldDesc.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.");
                            LnkStr = "sum(" + ColAlias_new + ") as '" + SAPDI + "'";
                        }
                        if (FWField.Equals("Qty"))
                        {
                            LnkStr = "sum(" + SQLstr + ") as '" + SAPDI + "'";
                        }

                    }

                    if (!newCodePrefix.Equals(string.Empty))
                    {
                        LnkStr =  newCodePrefix + "+" + LnkStr;
                    }
                    Result += "," + LnkStr;
                }

                // return Result.Length > 1 ? Result.Substring(1) : Result;
            }

            return Result.Length > 1 ? Result.Substring(1) : Result;
        }

        public static string GetImportFildsPrimary(SqlConnection oSqlConnection, string Rolestr, string DataType,
   string ContionSql, string sumImport, string IsSum, string Collect, string isDebit, RichTextBox oRichTextBox)
        {
            string ColAlias = string.Empty;
            string Result = string.Empty;
            string sqlCmd = string.Empty;
            string SQLStr = " and T2.SAPDI = 'PrimaryFormItems' and T1.ERPobject + T1.SAPobject = '" + Rolestr + "' ";
            string CodePrefix = string.Empty;
            string oldDataType = DataType;
            switch (DataType)
            {
                case "主表":
                    ColAlias = "T100.";
                    CodePrefix = " ISNULL(T2.CodePrefix, '')";
                    SQLStr += " and T2.KeyField = 'N' and T2.FWDataType <> '日期'  ";
                    sqlCmd =
                        "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3 ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                        Rolestr + "' ";
                    DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, oRichTextBox);

                    foreach (DataRow oDataRow in oData.Rows)
                    {
                        Result += "," + oDataRow[1] + ColAlias + oDataRow[0] + "," + (sumImport == "Y" ? IsSum : "0") + ") as '" +
                                  oDataRow[2] + "'";
                    }
                    break;
                case "子表":
                    break;
                case "序列号":
                    ColAlias = "T102.";
                    SQLStr += " and T2.KeyField='N'";
                    break;
            }

            //if (DataType == "主表" || DataType == "子表")
            //{
            //    if (sumImport == "Y")
            //    {
            //        SQLStr += "  and (T2.IsGroup ='Y' or T2.IsSum='Y')";
            //    }
            //    else
            //    {
            //        SQLStr += " and T2.SAPDI <>'InCmpBch' and T2.SAPDI <>'DocEntry'";
            //    }
            //}
            if (oldDataType.Equals("序列号show") || oldDataType.Equals("序列号Group"))
            {
                if (Collect == "Y")
                {
                    SQLStr += "  and (T2.IsGroup ='Y') ";
                    DataType = "子表";
                    ColAlias = "T101.";
                }
                else
                {
                    SQLStr += " and T2.KeyField='Y'";
                    DataType = "序列号";
                    ColAlias = "T102.";
                }
            }

            sqlCmd =
                "SELECT T1.IsCollect,T2.FWField,T2.FWFieldDesc,T2.FWDataType,T2.SAPDI,T2.IsSum,T2.IsGroup,T2.KeyField,T2.CodePrefix,T2.IsMasTbl,isnull(T3.SQLstr,'') SQLstr  " +
                "FROM ToSAP_Role_Mas T1 (noLock) LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry " +
                "LEFT JOIN ToSAP_ERPDataType T3 ON T2.FWDataType=T3.dataType " +
                "WHERE 1=1 AND T2.isDebit = '" + isDebit + "' and T2.DataType = '" + DataType + "' " + SQLStr;
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, oRichTextBox);
            if (oldDataType.Equals("序列号Group"))
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string FWField = oDataRow["FWField"].ToString();
                    string IsMasTbl = oDataRow["IsMasTbl"].ToString();
                    if (IsMasTbl.Equals("Y"))
                    { Result += ",T100." + FWField; }
                    else
                    {
                        Result += ",T101." + FWField;
                    }

                }
            }
            else
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ColAlias_new = string.Empty;

                    string FWField = oDataRow["FWField"].ToString();
                    string FWFieldDesc = oDataRow["FWFieldDesc"].ToString();

                    string SAPDI = "CashFlowLineItemID"; //oDataRow["SAPDI"].ToString();
                    string newIsSum = oDataRow["IsSum"].ToString();
                    oDataRow["IsGroup"].ToString();

                    string newCodePrefix = oDataRow["CodePrefix"].ToString();
                    string IsMasTbl = oDataRow["IsMasTbl"].ToString();
                    string SQLstr = oDataRow["SQLstr"].ToString();
                    string LnkStr = string.Empty;
                    if (SAPDI.Equals("notImport") || oldDataType.Equals("序列号show"))
                    {
                        SAPDI = FWField;
                    }
                    if (DataType.Equals("子表"))
                    {
                        if (IsMasTbl.Equals("Y"))
                        {
                            ColAlias_new = "T100." + FWField;
                        }
                        else
                        {
                            ColAlias_new = "T101." + FWField;
                        }
                    }
                    if (!DataType.Equals("子表"))
                    {
                        ColAlias_new = ColAlias + FWField;
                    }

                    if (FWField.Equals("Combfield"))  //组合字段
                    {

                        ColAlias_new = FWFieldDesc.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.");

                    }
                    LnkStr = SQLstr + "(" + ColAlias_new + ") as '" + SAPDI + "'";

                    if (FWField.Equals("Qty"))
                    {
                        LnkStr = SQLStr + " as '" + SAPDI + "'";
                    }

                    if (FWField.Equals("Fixed"))
                    {
                        LnkStr = FWFieldDesc + " as '" + SAPDI + "'";
                    }
                    if (newIsSum.Equals("Y"))
                    {
                        LnkStr = "sum(" + ColAlias_new + ") as '" + SAPDI + "'";
                        if (FWField.Equals("Combfield"))  //组合字段
                        {

                            ColAlias_new = FWFieldDesc.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.");
                            LnkStr = "sum(" + ColAlias_new + ") as '" + SAPDI + "'";
                        }
                        if (FWField.Equals("Qty"))
                        {
                            LnkStr = "sum(" + SQLstr + ") as '" + SAPDI + "'";
                        }

                    }

                    if (!newCodePrefix.Equals(string.Empty))
                    {
                        LnkStr =  newCodePrefix + "+" + LnkStr;
                    }
                    Result += "," + LnkStr;
                }

                // return Result.Length > 1 ? Result.Substring(1) : Result;
            }

            return Result.Length > 1 ? Result.Substring(1) : Result;
        }

        public static string GetImportGroupFld(SqlConnection oSqlConnection, string Rolestr, string DataType,RichTextBox oRichTextBox)
        {
            string sqlCmd =
                "SELECT case when FwField = 'Combfield' then FWFieldDesc else 'T1.'+FWField end FROM ToSAP_Role_Mas T1  (noLock)  LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry  AND T2.DataType = '" +
                DataType + "'  WHERE T2.IsGroup='Y' and T1.ERPobject+T1.SAPobject='" + Rolestr +
                "'";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);

            string Result = string.Empty;
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                Result += "," + oDataRow[0].ToString().Trim();
            }
            return Result.Length > 1 ? Result.Substring(1) : Result;
        }

        public static string GetImportGroupFld(SqlConnection oSqlConnection, string Rolestr, string DataType,
            string sumImport, string IsSum, string Collect, string T100Link,RichTextBox oRichTextBox)
        {
            string sqlCmd = string.Empty;
            int i = 1;
            string SQLStr = "  and T2.DataType='" + DataType +
                         "' and T2.IsGroup = 'Y' and T1.ERPobject + T1.SAPobject = '" + Rolestr + "' ";
            SQLStr += " and T2.FWField <>'LineNum' and T2.FWField <>'Fixed' and T2.SAPDI <> 'DocDate'";
            sqlCmd =
                "SELECT FWField, ISNULL(SQLstr,'')+'(' fields ,T2.IsMasTbl,T2.FWFieldDesc,T2.SAPDI ,T2.CodePrefix FROM ToSAP_Role_Mas T1  (noLock)  LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock)  ON T2.FWDataType=T3.dataType where 1=1 " +
                SQLStr;
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            string Result = string.Empty;
            if (DataType.Equals("序列号Out"))
            {
                SQLStr = " and T2.KeyField = 'N' and T2.SAPDI != 'BaseLineNumber' and T1.ERPobject + T1.SAPobject = '" +
                         Rolestr + "' ";
                sqlCmd =
                    "SELECT T2.SAPDI  FROM ToSAP_Role_Mas T1  (noLock)  LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry  AND T2.DataType = '序列号' LEFT JOIN ToSAP_ERPDataType T3  (noLock)  ON T2.FWDataType = T3.dataType  where 1=1 " +
                    SQLStr;
                oDataTable.Clear();
                oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    Result += ",U1." + oDataRow[0].ToString().Trim();
                }
                return Result.Length > 1 ? Result.Substring(1) : Result;
            }

            if (T100Link == "N")  //显示字段
            {
                //if (sumImport == "N" && DataType == "主表")
                //{
                //    Result += ",T1.DocEntry";
                //}
                sqlCmd =
                    "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1  (noLock)  LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock)  ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                    Rolestr + "' ";
                DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
                foreach (DataRow oDataRow in oData.Rows)
                {
                    Result += "," + oDataRow[1] + "T1." + oDataRow[0] + "," +(sumImport=="Y"? IsSum:"0") + ") as '" + oDataRow[2]+"'";
                }
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string IsMasTbl = oDataRow[2].ToString().Trim();
                    i = 1;
                    if (IsMasTbl != "Y")
                    {
                        switch (DataType)
                        {
                            case "子表":
                                i++;
                                break;
                            case "序列号":
                                i += 2;
                                break;
                        }
                    }

                    string combfields = oDataRow[0].ToString();
                    if (combfields.Equals("Combfield"))
                    {
                        string combValue = oDataRow["FWFieldDesc"].ToString();
                        Result += "," + combValue + " as '" + oDataRow["SAPDI"].ToString().Trim()+"'";
                    }
                    else
                    {
                        Result += ","+ oDataRow["CodePrefix"].ToString().Trim()  + oDataRow[1].ToString().Trim() + "T" + i + "." +
                                  oDataRow[0].ToString().Trim() + ") as '" + oDataRow[0].ToString().Trim()+"'";
                    }
                    
                }

                return Result.Length > 1 ? Result.Substring(1) : Result;
            }
            if (T100Link == "N1")  //分组字段,显示字段
            {
                //if (sumImport == "N" && DataType == "主表")
                //{
                //    Result += ",DocEntry";
                //}
                sqlCmd =
                    "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1  (noLock) LEFT JOIN ToSAP_Role_Sub T2  (noLock) ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock) ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                    Rolestr + "' ";
                DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, oRichTextBox);
                foreach (DataRow oDataRow in oData.Rows)
                {
                    Result += "," + oDataRow[2] ;
                }
                i = 1;
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string IsMasTbl = oDataRow[2].ToString().Trim();
                    
                    string combfields = oDataRow[0].ToString();
                    if (combfields.Equals("Combfield"))
                    {
                        Result += ","+ oDataRow["SAPDI"].ToString().Trim();
                        i++;
                    }
                    else
                    {
                        Result += "," + oDataRow[0].ToString().Trim() ;
                    }

                }

                return Result.Length > 1 ? Result.Substring(1) : Result;
            }
            if (T100Link == "Y")  //内部连接
            {
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string combfields = oDataRow[0].ToString();
                    if (!combfields.Equals("Combfield"))
                    {
                        Result += " and " + oDataRow[1].ToString().Trim() + "T" + i + "." +
                                  oDataRow[0].ToString().Trim() + ") = " + oDataRow[1].ToString().Trim() + "T" +
                                  (i + 99) + "." + oDataRow[0].ToString().Trim() + ")";
                    }
                    else
                    {
                        string FWFieldDesc = oDataRow["FWFieldDesc"].ToString().Trim();
                        Result += " and " + FWFieldDesc + " = " +
                                  FWFieldDesc.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.","T103.");

                    }
                    
                }
                if (DataType == "主表")
                {
                    sqlCmd =
                        "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1  (noLock) LEFT JOIN ToSAP_Role_Sub T2  (noLock) ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock) ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                        Rolestr + "' ";
                    DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
                    foreach (DataRow oDataRow in oData.Rows)
                    {
                        Result += " and " + oDataRow[1] + "T1." + oDataRow[0] + "," + (sumImport == "Y" ? IsSum : "0") + ") = " + oDataRow[1] +
                                  "T100." + oDataRow[0] + "," + (sumImport == "Y" ? IsSum : "0") + ")";
                    }
                    if (sumImport == "N")
                    {
                        Result += " and T1.DocEntry = T100.DocEntry";
                    }
                }

                return Result;
            }

            if (T100Link == "G1") //外部 group by
            {
                if (DataType == "主表")
                {
                    //if (sumImport.Equals("N"))
                    //{
                    //    Result += ",T1.DocEntry";
                    //}
                    sqlCmd =
                        "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1  (noLock)  LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock)  ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                        Rolestr + "' ";
                    DataTable oData1 = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
                    foreach (DataRow oDataRow in oData1.Rows)
                    {
                        Result += "," + oDataRow[1] + "T1." + oDataRow[0] + "," + (sumImport == "Y" ? IsSum : "0") + ") ";
                    }
                    
                }

                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string IsMasTbl = oDataRow[2].ToString().Trim();
                    i = 1;
                    if (IsMasTbl != "Y")
                    {
                        switch (DataType)
                        {
                            case "子表":
                                i++;
                                break;
                            case "序列号":
                                i += 2;
                                break;
                        }
                    }
                    string combfields = oDataRow[0].ToString();
                    if (!combfields.Equals("Combfield"))
                    {
                        Result += "," + oDataRow[1].ToString().Trim() + "T" + i + "." +
                                  oDataRow[0].ToString().Trim() + ") ";
                    }
                    else
                    {
                        string combValue = oDataRow["FWFieldDesc"].ToString();
                        Result += "," + combValue;
                    }

                    
                }

                return Result.Length > 1 ? Result.Substring(1) : Result;
            }

            if (T100Link == "G2")  //内部Group BY
            {
                if (Collect.Equals("N") && DataType != "主表")
                {
                    Result += ",T101.LineNum";
                }
                if (DataType == "主表")
                {
                    sqlCmd =
                        "SELECT T2.FWField, ISNULL(SQLstr, '') + '(' Fields,T2.SAPDI FROM ToSAP_Role_Mas T1  (noLock)  LEFT JOIN ToSAP_Role_Sub T2  (noLock)  ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock) ON T2.FWDataType=T3.dataType  WHERE T2.FWDataType = '日期' and T1.ERPobject + T1.SAPobject = '" +
                        Rolestr + "' ";
                    DataTable oData1 = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
                    foreach (DataRow oDataRow in oData1.Rows)
                    {
                        Result += "," + oDataRow[1] + "T100." + oDataRow[0] + "," + (sumImport == "Y" ? IsSum : "0") + ") ";
                    }
                }

                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string IsMasTbl = oDataRow[2].ToString().Trim();
                    i = 1;
                    if (IsMasTbl != "Y")
                    {
                        switch (DataType)
                        {
                            case "子表":
                                i++;
                                break;
                            case "序列号":
                                i += 2;
                                break;
                        }
                    }
                    string combfields = oDataRow[0].ToString();
                    if (!combfields.Equals("Combfield"))
                    {
                        Result += "," + oDataRow[1].ToString().Trim() + "T" + (i + 99) + "." +
                                  oDataRow[0].ToString().Trim() + ") ";
                    }
                    else
                    {
                        string combValue = oDataRow["FWFieldDesc"].ToString();
                        Result += "," + combValue.Replace("T1.","T100.").Replace("T2.","T101.").Replace("T3.", "T103."); ;
                    }
                    
                }

                return Result.Length > 1 ? Result.Substring(1) : Result;
            }

            if (T100Link == "Serial")
            {
                SQLStr = "  and T1.ERPobject + T1.SAPobject = '" + Rolestr + "'";
                if (Collect == "Y")
                {
                    SQLStr += " and T2.IsGroup='Y' AND T2.DataType = '子表' ";
                }
                else
                {
                    SQLStr += "  and T2.KeyField = 'Y' AND T2.DataType = '序列号' ";
                }
                sqlCmd =
                    "SELECT T2.FWField,T2.SAPDI FROM ToSAP_Role_Mas T1  (noLock) LEFT JOIN ToSAP_Role_Sub T2  (noLock) ON T2.DocEntry = T1.DocEntry  LEFT JOIN ToSAP_ERPDataType T3  (noLock) ON T2.FWDataType=T3.dataType where 1=1 " +
                    SQLStr;
                oDataTable.Clear();
                oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string Fields = oDataRow[0].ToString().Trim();
                    //if (Fields.Equals("notImport"))
                    //{
                    //    Fields = oDataRow[0].ToString().Trim();
                    //}
                    Result += " and U0." + Fields + " = U1." + Fields;
                }

                return Result.Substring(4);
            }

            return string.Empty;
        }


        public static DataSet GetImportData(SqlConnection oSqlConnection, string Rolestr,
            string Diinterface, string ShpCode, string Interfacemode, string DiobjType, string ShopSet, string SAPDBName,
            string SAPDITableName,RichTextBox oRichTextBox)
        {
            GC.Collect();
            string sqlCmd = "SELECT * FROM ToSAP_Role_Mas WHERE ERPobject+SAPobject='" + Rolestr + "'";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            sqlCmd = string.Empty;
            string ERPobj = oDataTable.Rows[0]["ERPobject"].ToString();
            string SAPobj = oDataTable.Rows[0]["SAPobject"].ToString();
            string RolesDocentry = oDataTable.Rows[0]["DocEntry"].ToString();
            string mContion = String.Empty;
            if (!SAPobj.Equals("OBPL"))
            {
                mContion= oDataTable.Rows[0]["Contion"].ToString();
            }
                
            oDataTable.Dispose();
            sqlCmd = "SELECT * FROM ToSAP_FWobject  WHERE FWCode = '" + ERPobj + "'";
            oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            sqlCmd = string.Empty;
            string MasterTb = oDataTable.Rows[0]["MasterTb"].ToString();
            string SubTb = oDataTable.Rows[0]["SubTb"].ToString();
            string SerTb = oDataTable.Rows[0]["SerTb"].ToString();
            
            oDataTable.Dispose();
            sqlCmd = "SELECT DISTINCT DataType FROM ToSAP_Role_Sub WHERE DocEntry = '" + RolesDocentry + "'";
            oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            DataSet oDataSet = new DataSet();
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                string DataType = oDataRow[0].ToString();
                string ImportFieldsStr = GetImportFieldsStr(oSqlConnection, Rolestr, DataType,oRichTextBox);
                string ImportGroupFld = GetImportGroupFld(oSqlConnection, Rolestr, DataType,oRichTextBox);
                string GetDataFromTab = string.Empty;
                string SAPTableName = string.Empty;
                switch (DataType)
                {
                    case "序列号":
                        GetDataFromTab = SerTb;
                        SAPTableName = "OSRN";
                        break;
                    case "子表":
                        GetDataFromTab = SubTb;
                        SAPTableName = SAPDITableName.Substring(1) + "1";
                        break;
                    default:
                        GetDataFromTab = MasterTb;
                        SAPTableName = SAPDITableName;
                        break;
                }

                if (DiobjType.Equals("物料主数据"))
                {
                    sqlCmd = "EXEC CreateOITMTemp";
                    Common.GetFWSqlConn(null);
                    Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
                    GetDataFromTab = "ToSAP_temp_OITM";
                }
                sqlCmd =
                    "SELECT CASE WHEN  t2.FWField = 'Combfield' THEN t2.FWFieldDesc ELSE 'T1.'+ t2.FWField end FROM ToSAP_Role_Mas t1 LEFT JOIN ToSAP_Role_Sub t2 ON t2.DocEntry = t1.DocEntry WHERE T2.DataType = '" +
                    DataType + "' and t2.keyfield='Y' AND t1.ERPobject+t1.SAPobject='" +
                    Rolestr + "'";
                string KeyField = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox).Rows[0][0].ToString();
                string isxmlContin = " "+mContion.Replace("T1.","T00.")+  " and " + KeyField.Replace("T1.","T00.") + " = " + KeyField + " for xml path,ROOT('" +
                                  Diinterface + "') ";

                string Result1 = "SELECT  " + ImportFieldsStr + "  from  " + GetDataFromTab + " T00  where 1=1 " +
                              isxmlContin;


                string Contion = string.Empty;
                if (Program.InterFaceSet == "按公司" && (Diinterface == "ProfitCenter" || Diinterface == "Warehouses" ))
                {
                    Contion = " and " + KeyField +
                              " in (SELECT WhsCode FROM ToSAP_BranchDetail WHERE BranchCode = '" + SAPDBName + "') ";
                }

                Result1 = "select " + KeyField + " as KeyField, " + ImportGroupFld + " ,(  " + Result1 +
                          " ) as xmlobj  from " + GetDataFromTab +
                          " T1 WHERE  NOT EXISTS(SELECT DocEntry FROM ToSAP_ImportOK U00 WHERE U00.DocEntry=" +
                          KeyField + " AND U00.DocType='" + ERPobj + "' and U00.SAPDocType = '" + SAPobj +
                          "' and  U00.SAPCompany = '" + SAPDBName + "') " + Contion + mContion + " group by " +
                          ImportGroupFld;

                Result1 = Result1.Replace("SAPDBName", SAPDBName);
                DataTable ResultData = GetDataTable(Program.FWSqlConnStr.ConnectionString, Result1,oRichTextBox);
                ResultData.TableName = SAPTableName;
                oDataSet.Tables.Add(ResultData);
            }

            return oDataSet;
        }


        public static DataSet GetImportData(SqlConnection oSqlConnection, CheckEdit Rolestr, string ShpCode,
            string Interfacemode, string CorpSet, string ShopSet, string stDate, string enDate, string shpEntry,
            string BPLID,RichTextBox oRichTextBox,string DITable,string ReDocEntry)
        {
            GC.Collect();
            if (oRichTextBox != null)
            {
                //if (oRichTextBox.TextLength > 100000)
                //{
                //    oRichTextBox.Clear();
                //}
            }
               
            string sqlCmd =
                "SELECT * FROM  ToSAP_Role_Mas T1 (NOLOCK) LEFT JOIN ToSAP_DIobject T2 (NOLOCK) ON T1.SAPobject = T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 (NOLOCK) " +
                "ON T2.SAPDIinterface = T3.DIInterface WHERE T1.ERPobject + T2.SAPobjCode ='" +Rolestr.Name + "'";
            DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            sqlCmd = string.Empty;
            string ERPobj = oDataTable.Rows[0]["ERPobject"].ToString();
            string SAPobj = oDataTable.Rows[0]["SAPobject"].ToString();
            string SAPDI = oDataTable.Rows[0]["DI"].ToString();
            string DocEntry1 = oDataTable.Rows[0]["DocEntry"].ToString();
            string ContionSql = oDataTable.Rows[0]["Contion"].ToString() == string.Empty ? string.Empty : "  " + oDataTable.Rows[0]["Contion"];
            string SumImport = oDataTable.Rows[0]["SumImport"].ToString();
            string IsSum = oDataTable.Rows[0]["IsSum"].ToString().Trim();
            string SAPDITableName = DITable==null? oDataTable.Rows[0]["DTable"].ToString():DITable;
            string GetSAPCost = oDataTable.Rows[0]["SAPCost"].ToString();
            string Collect = oDataTable.Rows[0]["IsCollect"].ToString();
            string checkCost = oDataTable.Rows[0]["checkCost"].ToString();
            string ToOrder  = oDataTable.Rows[0]["ToOrder"].ToString();
            string SAPDIinterface = oDataTable.Rows[0]["SAPDIinterface"].ToString();
            string orderByFields= String.Empty;
            string SAPDBName = GetSAPDBName(oSqlConnection, ShpCode);
            string DocStatus = oDataTable.Rows[0]["DocStatus"].ToString(); DocStatus = DocStatus == "" ? "审核" : DocStatus;
            string isDraft = oDataTable.Rows[0]["isDraft"].ToString();
            string DocCon =String.Empty;
            string DateField = String.Empty;
            switch (DocStatus)
            {
                case "保存":
                    break;
                    DocCon = "";
                case "审核":
                    DocCon = " and Isnull(T1.Audit,'')<>''";
                    break;
                case "复核":
                    DocCon = " and Isnull(T1.Recheck,'')<>''";
                    break;
                    
            }

            if (GetSAPCost.Equals("Y") && (!ReDocEntry.Equals("0") && (Program.DocToSAP.Equals("所有单")|| Program.DocToSAP.Equals("选择单据"))) )
            {
                sqlCmd = "SELECT T2.FWField FROM ToSAP_Role_Mas T1 (NOLOCK) " +
                    "INNER JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T1.DocEntry = T2.DocEntry AND T2.DataType = '主表' AND T2.SAPDI = 'DocDate' AND t1.ERPobject + T1.SAPobject = '" + Rolestr.Name+"'";
                 DateField = Common.GetDataTable( Program.FWSqlConnStr.ConnectionString,sqlCmd,null).Rows[0][0].ToString();
                //sqlCmd = "SELECT T2.FWField FROM ToSAP_Role_Mas T1 (NOLOCK) " +
                //         "INNER JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T1.DocEntry = T2.DocEntry AND T2.DataType = '子表' AND T2.SAPDI = 'WarehouseCode' AND t1.ERPobject + T1.SAPobject = '" + Rolestr.Name + "'";

                sqlCmd = "SELECT CASE WHEN  t2.FWField = 'Combfield' THEN t2.FWFieldDesc when isnull(T3.SQLstr,'')!='' and T3.dataType!='数量' then T3.SQLstr+'('+ case when T2.IsMasTbl='Y' then 'T1.' else 'T2.' end+ T2.FWField+')' ELSE t2.FWField end FROM ToSAP_Role_Mas T1 (NOLOCK) " +
                         "INNER JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T1.DocEntry = T2.DocEntry AND T2.DataType = '子表' AND T2.SAPDI = 'WarehouseCode' AND t1.ERPobject + T1.SAPobject = '" + Rolestr.Name + "' " +
                         "inner join ToSAP_ERPDataType T3 on T2.FWDataType=T3.dataType";
                
                string whsField = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null).Rows[0][0].ToString();
              
               sqlCmd = "EXEC ToSAP_GetSAPCost @erpsap = '" + Rolestr.Name + "', @stDate = '" + stDate + "', @enDate = '" + enDate + "', @whsCode = '" + ShpCode + "', @SAPDBName = '" + SAPDBName + "', @DateField = '" + DateField + "',@whsField = '" + whsField.Replace("'","") + "',@shpEntry = '" + shpEntry + "',@ReDocEntry = "+ReDocEntry;
  
                if (oRichTextBox != null )
                {

                    oRichTextBox.AppendText("获取单据成本中,请稍等"+Environment.NewLine+sqlCmd+Environment.NewLine);

                    
                }
                Common.GetFWSqlConn(null);
                Common.ExecSqlcmd( Program.FwSqlConnection,sqlCmd);
                if (oRichTextBox != null)
                {
                    oRichTextBox.AppendText("获取单据成本完成" + Environment.NewLine);
                }
            }
            
            oDataTable.Dispose();
            sqlCmd = "SELECT * FROM ToSAP_FWobject (NOLOCK) WHERE FWCode = '" + ERPobj + "'";
            oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            string MasterTb = oDataTable.Rows[0]["MasterTb"].ToString();
            string SubTb = oDataTable.Rows[0]["SubTb"].ToString();
            string SerTb = oDataTable.Rows[0]["SerTb"].ToString();
            string SubDI = string.Empty;

            string CardCode = string.Empty;
            sqlCmd = "SELECT CASE when T2.FWField='Combfield' THEN t2.FWFieldDesc ELSE CASE WHEN ISNULL(T2.CodePrefix,'')<>'' THEN  T2.CodePrefix +'+'  ELSE '' end +T3.SQLstr+'(T1.'+T2.FWField+')' END CardCode FROM ToSAP_Role_Mas T1 (NOLOCK) " +
                     "INNER JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T1.DocEntry = T2.DocEntry AND T2.DataType = '主表' AND T2.SAPDI = 'CardCode' " +
                     " LEFT JOIN ToSAP_ERPDataType T3 (NOLOCK) ON T2.FWDataType=T3.dataType " +
                     " where t1.ERPobject + T1.SAPobject = '" + Rolestr.Name + "'";
            DataTable CardTable= Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (CardTable.Rows.Count > 0)
            {
                CardCode = CardTable.Rows[0][0].ToString();
            }
            
            string GrpBPLID = "N",BPLIDSql = String.Empty;
            string Contion = " and not exists(SELECT U0.DocType FROM ToSAP_ImportOK U0 (NOLOCK) WHERE U0.DocType='" + ERPobj +
                             "' AND U0.DocEntry = Convert(varchar(100),T1.DocEntry)" + (CardCode.Equals("")?"":"+"+CardCode) +
                             " AND U0.SAPDocType = '" + SAPobj + "' AND SAPCompany = '" +SAPDBName + "')";
            sqlCmd = "SELECT T2.FWField,T2.FWFieldDesc FROM ToSAP_Role_Mas T1 (NOLOCK) " +
                     "INNER JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T1.DocEntry = T2.DocEntry AND T2.DataType = '主表' AND T2.SAPDI = 'BPL_IDAssignedToInvoice' AND t1.ERPobject + T1.SAPobject = '" + Rolestr.Name + "'";
            DataTable oDataTable11 = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (oDataTable11.Rows.Count > 0)
            {
                Contion = " and not exists(SELECT U0.DocType FROM ToSAP_ImportOK U0 (NOLOCK) WHERE U0.DocType='" + ERPobj +
                          "' AND U0.DocEntry = Convert(varchar(100),T1.DocEntry)" + (CardCode.Equals("") ? "" : "+" + CardCode) + " AND U0.SAPDocType = '" + SAPobj + "' AND SAPCompany = '" +
                          SAPDBName + "' and U0.BPLID = "+oDataTable11.Rows[0]["FWFieldDesc"].ToString().Replace("'BPLID_In'","T4.BPLID")+" )";
                GrpBPLID = "Y";
                BPLIDSql = oDataTable11.Rows[0]["FWFieldDesc"].ToString();
            }
            sqlCmd =
                "SELECT CASE WHEN  T2.FWField = 'Combfield' THEN T2.FWFieldDesc ELSE 'T1.'+T2.FWField end FROM ToSAP_Role_Mas T1 (NOLOCK) LEFT JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject = '" +
                Rolestr.Name + "' AND T2.SAPDI = 'InCmpBch'";

            string ConField = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox).Rows[0][0].ToString();
            //Contion += " and " + ConField + " =" + shpEntry;
            sqlCmd = "SELECT case when T2.FWField='Combfield' then T2.FWFieldDesc else  T2.FWField end FROM ToSAP_Role_Mas T1 (NOLOCK) LEFT JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject = '" + Rolestr.Name+ "' AND T2.DataType='主表' AND T2.SAPDI='DocDate'";
            if (SAPDI.Equals("JournalEntries"))
            {
                sqlCmd = "SELECT case when T2.FWField='Combfield' then T2.FWFieldDesc else  T2.FWField end FROM ToSAP_Role_Mas T1 (NOLOCK) LEFT JOIN ToSAP_Role_Sub T2 (NOLOCK) ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject = '" + Rolestr.Name + "' AND T2.DataType='主表' AND T2.SAPDI='ReferenceDate'";
            }
            string DateFields = GetDataTable(Program.FWSqlConnStr.ConnectionString,sqlCmd,null).Rows[0][0].ToString().Replace("T1.","");
            
            Contion += DocCon+" and  '" + stDate + "' <=  CONVERT(date,T1." + DateFields + ",121)" + " and '" + enDate + "' >=  CONVERT(date,T1." + DateFields + ",121)";

            if (!ReDocEntry.Equals("0"))
            {
                Contion += "and T1.DocEntry = " + ReDocEntry;
            }
            
               
            DataSet oDataSet = new DataSet();
            string KeyField = GetKeyField(oSqlConnection, Rolestr.Name,oRichTextBox);
            string Result1 = string.Empty;


            string RangeDate = "'"+stDate + "~" + enDate+"'";
            string MasImportField =
                GetImportFieldsStr(oSqlConnection, Rolestr.Name, "主表", string.Empty, SumImport, IsSum, Collect, oRichTextBox);
            string MasShowField =
                GetImportGroupFld(oSqlConnection, Rolestr.Name, "主表", SumImport, IsSum, Collect, "N",oRichTextBox);
            string MasOutsideGroupBy =
                GetImportGroupFld(oSqlConnection, Rolestr.Name, "主表", SumImport, IsSum, Collect, "G1",oRichTextBox);
            string MasInsideGroupBy =
                GetImportGroupFld(oSqlConnection, Rolestr.Name, "主表", SumImport, IsSum, Collect, "G2",oRichTextBox);
            string MasLinkField =
                GetImportGroupFld(oSqlConnection, Rolestr.Name, "主表", SumImport, IsSum, Collect, "Y",oRichTextBox);
            string ContionSqlInside =
                ContionSql.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.");
            //ContionSqlInside += " and " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.") + " =" + shpEntry;
            string ContionInside = Contion.Replace("T1.", "T100.").Replace("T2.", "T101.").Replace("T3.", "T103.");

            if (SumImport.Equals("Y"))
            {
                MasShowField += ",ROW_NUMBER() OVER( ORDER BY " + MasOutsideGroupBy + ") DocEntry,ROW_NUMBER() OVER( ORDER BY " + MasOutsideGroupBy + ") keyfield ";
            }

            string DocObject = "";

            orderByFields = MasOutsideGroupBy;
            int objecttype = 67;
            if (SAPDI.Equals("Documents") || SAPDI.Equals("JournalEntries"))
            {
                BoObjectTypes oBoObjectTypes =
                    (BoObjectTypes)Enum.Parse(typeof(BoObjectTypes), SAPDIinterface);
                objecttype = (int)(oBoObjectTypes);
            }
            DocObject = objecttype.ToString().Trim();
            if (isDraft.Equals("Y"))
            {
                switch (SAPDI)
                {
                    case "Documents":
                        objecttype = 112;
                        break;
                    default:
                        objecttype = 28;
                        break;
                }
            }
            if (ShpCode.Equals("ALL") && !SumImport.Equals("Y"))
            {
                
                MasShowField = "T4.SAPCompany,T4.shpCode importshpCode, T1.DocEntry keyfield, T1.DocEntry, T1.DocDate,T1.DocNum,convert(date,T1." + DateFields+",121) DocDate1";
                MasOutsideGroupBy = "T4.SAPCompany,T4.shpCode,T1.DocEntry,T1.DocDate,T1.DocNum,convert(date,T1." + DateFields+",121)," +  MasOutsideGroupBy;
                orderByFields = "T1.DocEntry,convert(date,T1."+ DateFields+ ",121),T1.DocDate,T1.DocNum";
                }
           
            if (GrpBPLID.Equals("Y"))
            {
                MasShowField +=","+ BPLIDSql.Replace("'BPLID_In'", "convert(varchar(10),T4.BPLID)") + " as  BPL_IDAssignedToInvoice ";
               // MasOutsideGroupBy += "," + BPLIDSql;
            }else if ((!BPLID.Equals("")) && GrpBPLID.Equals("N"))
            {
                if (SAPDI.Equals("Documents"))
                {
                    MasImportField += ",convert(varchar(10), '" + BPLID + "') as BPL_IDAssignedToInvoice ";
                }
            }else if (Program.InterFaceSet.Equals("按分支") && BPLID.Equals(""))
            {
                if (SAPDI.Equals("Documents"))
                {
                    MasImportField += ",convert(varchar(10),T4.BPLID) as BPL_IDAssignedToInvoice ";
                }
            }

            if (!CardCode.Equals(string.Empty))
            {
                MasShowField += "," + CardCode + " CardCodeIns";
            }
            else
            {
                MasShowField += ",'' CardCode ";
            }

            

            if (checkCost.Equals("Y"))
            {
                MasShowField += ",CASE WHEN EXISTS(SELECT T30.DocEntry FROM "+SubTb+ " T30 (NOLOCK) inner JOIN OITM T31 (NOLOCK) ON T31.DocEntry = T30.ItemEntry AND t31.A12<>'移动平均' and T31.Cost<>T30.LineTotal AND T30.DocEntry=T1.DocEntry) THEN 'N' ELSE 'Y' END checkCost";
             }
            else
            {
                MasShowField += ",'Y' checkCost ";
            }
            if (SumImport.Equals("N"))
            {
                MasShowField += ",CASE WHEN '"+SAPDI+ "' not in( 'StockTransfer','JournalEntries' ) and  EXISTS(SELECT DocEntry FROM ToSAP_Role_Mas (NOLOCK) WHERE ERPobject='" + ERPobj + "' AND SAPCost='Y') " +
                                    " and (EXISTS(SELECT DocEntry FROM ToSAP_DocCost (NOLOCK) WHERE DocType = '" + MasterTb + "' and Canimport=0 AND DocEntry = T1.DocEntry) OR not EXISTS(SELECT DocEntry FROM ToSAP_DocCost(NOLOCK) WHERE DocType='" + MasterTb + "' AND DocEntry=T1.DocEntry) ) THEN 'N' ELSE 'Y' END AS SAPCost";
            }
            else
            {
                MasShowField += ",'Y' SAPCost ";
            }
            if (SAPDI.Equals("Documents") || SAPDI.Equals("StockTransfer"))
            {
                MasShowField += ",T4.BPLID as BPLID ";
                MasOutsideGroupBy += ",T4.BPLID";
            }

            if (ShpCode.Equals("ALL") && !SumImport.Equals("Y"))
            {
                MasInsideGroupBy += ",T104.BPLID";
            }

            if (isDraft.Equals("Y") && SAPDI.Equals("Documents"))
            {
                MasImportField += "," + DocObject + " as DocObjectCode";
            }

            MasShowField += ",'"+ objecttype.ToString()+ "' objecttype," + ToOrder+ " ToOrder,'"+ Rolestr.Text+"' RoleDesc,CASE WHEN '" + SAPDI + "' not in( 'StockTransfer','JournalEntries' ) and  EXISTS(SELECT DocEntry FROM ToSAP_Role_Mas (NOLOCK) WHERE ERPobject='" + ERPobj + "' AND SAPCost='Y') then 'Y' else 'N' end GetSAPCost";
            MasShowField += ",'"+ERPobj+"' as ERPobj,'" + SAPobj + "' as SAPobj,'"+ DocEntry1 + "' as Sort1";
            if (SAPDI.Equals("JournalEntries"))
            {
                MasShowField += ",convert(date, T1." + DateFields + ", 121) DocDate1";
                MasOutsideGroupBy += ",convert(date, T1." + DateFields + ", 121)";
            }
            string oitm1 = " inner join OITM T103 (NOLOCK) ON T101.ItemEntry = T103.DocEntry ";
            string oitm2 = " inner join OITM T3 (NOLOCK) on T2.ItemEntry = T3.DocEntry ";
            if (SAPDI.Equals("JournalEntries"))
            {
                oitm1 = string.Empty;
                oitm2 = string.Empty;
            }
            Result1 = "SELECT " + MasShowField + " ,(SELECT  " + MasImportField + " FROM  " + MasterTb +
                      " T100  (NOLOCK) inner join " + SubTb + " T101  (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                     oitm1 +
                      " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                      " WHERE 1 = 1  " + ContionSqlInside + " " + ContionInside +" "+ MasLinkField +
                      " Group By " + MasInsideGroupBy +
                      " FOR XML PATH,ROOT('" + SAPDI + "')) xmlobj FROM  " + MasterTb + " T1 (NOLOCK) inner join " + SubTb +
                      " T2 (NOLOCK) on T1." + KeyField + " = T2." + KeyField +
                     oitm2 +
                      " inner Join " + shpEntry + " T4 on " + ConField + "=T4.shpEntry" +
                      " WHERE 1 = 1 " + ContionSql + " " + Contion +
                      " group by " + MasOutsideGroupBy +
                      " order by " + orderByFields;
            Result1 = Result1.Replace("RangeDate",  RangeDate).Replace("SAPDBName", SAPDBName).Replace("'BPLID_In'",ShpCode=="ALL"?"T104.BPLID" :"'"+BPLID+"'");
            oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, Result1,oRichTextBox);
            oDataTable.TableName = SAPDITableName;
            oDataSet.Tables.Add(oDataTable);

            sqlCmd = "SELECT  T1.DocEntry as absDocEntry , " + MasShowField + " FROM " + MasterTb +
                     " T1 (NOLOCK) INNER JOIN " + SubTb + " T2 (NOLOCK) ON T2.DocEntry = T1.DocEntry " +
                    oitm2 +
                     " inner Join " + shpEntry + " T4 on " + ConField + "=T4.shpEntry" +
                     " WHERE 1 = 1 " + ContionSql + " " + Contion +
                     " GROUP BY T1.DocEntry," + MasOutsideGroupBy +
                     " Order By T1.DocEntry";


            //sqlCmd = "SELECT DocEntry as absDocEntry ,DocNum," + MasShowField + " FROM " + MasterTb +" T1 WHERE 1=1  " +  Contion +" and T1.DocEntry in(SELECT DISTINCT T1.DocEntry FROM "+MasterTb+" T1 LEFT JOIN "+SubTb+" T2 ON T2.DocEntry = T1.DocEntry WHERE 1=1 "+ ContionSql+")";
            //sqlCmd = "SELECT T1.DocEntry as absDocEntry ,T1.DocNum "+ MasShowField + "FROM "+ MasterTb + " T1 LEFT JOIN "+SubTb+" T2 ON T2.DocEntry = T1.DocEntry where 1=1 " + ContionSql + " " + Contion;
            sqlCmd = sqlCmd.Replace("RangeDate", RangeDate).Replace("SAPDBName", SAPDBName).Replace("'BPLID_In'", ShpCode == "ALL" ? "T4.BPLID" : "'" + BPLID + "'"); ;
            oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            oDataTable.TableName = "DocList";
            oDataSet.Tables.Add(oDataTable);
            if (SumImport.Equals("Y"))
            {
                string masshowN1 = GetImportGroupFld(oSqlConnection, Rolestr.Name, "主表", SumImport, IsSum, Collect, "N1", oRichTextBox);
                string[] showFields = masshowN1.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (DataRow oDataRow in oDataSet.Tables[SAPDITableName].Rows)
                {
                    string selectstr = "";
                    string DocEntry = oDataRow["DocEntry"].ToString();
                    foreach (string showField in showFields)
                    {
                        selectstr += @" and " + showField + "='" + oDataRow[showField].ToString()+"'";
                    }
                    DataRow[] NewDataRow = oDataSet.Tables["DocList"].Select(@selectstr.Substring(4));
                    foreach (DataRow newDataRow in NewDataRow)
                    {
                        newDataRow["DocEntry"] = DocEntry;
                    }
                    oDataSet.Tables["DocList"].AcceptChanges();
                }
            }
            //子表导入数据
            string SubGroupBy =
                GetImportGroupFld(oSqlConnection, Rolestr.Name, "子表", SumImport, IsSum, Collect, "G2", oRichTextBox);  //子表Group BY 字段
            string Rowstr = ",ROW_NUMBER() OVER(PARTITION BY " + MasInsideGroupBy + " ORDER BY " + MasInsideGroupBy + ",T101.LineNum" +
                            ")-1 as LineNum ";
            if (SumImport.Equals("Y"))
            {
                Rowstr = ",ROW_NUMBER() OVER(PARTITION BY " + MasInsideGroupBy + " ORDER BY " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) + ",dbo.ToSAP_GetItmCode(T101.ItemEntry)" +
                         ")-1 as LineNum ";
            }
            string SubImportField = string.Empty;
            string subimportfieldIsdebit = string.Empty;
            string subimportPrimarydebit = string.Empty;
            string subimportPrimaryCredit = string.Empty;
            switch (SAPDI)
            {
                case "Documents":
                    SubDI = "Document_Lines";
                    SubImportField =
                GetImportFieldsStr(oSqlConnection, Rolestr.Name, "子表", string.Empty, SumImport, IsSum, Collect, oRichTextBox);
                    SubImportField += Rowstr;  //子表加行号,序列号导入的BaseLine
                    break;
                case "StockTransfer":
                    SubDI = "StockTransfer_Lines";
                    SubImportField =
                GetImportFieldsStr(oSqlConnection, Rolestr.Name, "子表", string.Empty, SumImport, IsSum, Collect, oRichTextBox);
                    SubImportField += Rowstr;  //子表加行号,序列号导入的BaseLine
                    break;
                case "JournalEntries":
                    SubDI = "JournalEntries_Lines";
                    SubImportField =
                GetImportFieldsStr(oSqlConnection, Rolestr.Name, "子表", string.Empty, SumImport, IsSum, Collect,"Y", oRichTextBox);
                    subimportfieldIsdebit =
                    GetImportFieldsStr(oSqlConnection, Rolestr.Name, "子表", string.Empty, SumImport, IsSum, Collect, "N", oRichTextBox);
                    subimportPrimarydebit=
                    GetImportFildsPrimary(oSqlConnection, Rolestr.Name, "子表", string.Empty, SumImport, IsSum, Collect, "Y", oRichTextBox);
                    subimportPrimaryCredit =
                    GetImportFildsPrimary(oSqlConnection, Rolestr.Name, "子表", string.Empty, SumImport, IsSum, Collect, "N", oRichTextBox);

                    break;
            }

            
            //string Result2 = "SELECT " + MasShowField + " ,(SELECT  " + SubImportField + " FROM  " + MasterTb +
            //              " T100 (NOLOCK) inner join " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
            //             " inner join OITM T103 (NOLOCK) ON T101.ItemEntry = T103.DocEntry " +
            //            " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
            //              " WHERE 1 = 1  " + ContionSqlInside + " " + MasLinkField +" "+ ContionInside+
            //              " Group By " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
            //              " order by " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
            //              " FOR XML PATH,ROOT('" + SubDI + "')) xmlobj " +
            //              "FROM  " + MasterTb + " T1 (NOLOCK) inner join " +
            //              SubTb + " T2 (NOLOCK) on T1." + KeyField + " = T2." + KeyField +
            //               " inner join OITM T3 (NOLOCK) ON T2.ItemEntry = T3.DocEntry " +
            //               " inner Join " + shpEntry + " T4 on " + ConField + "=T4.shpEntry" +
            //              " WHERE 1 = 1  " + ContionSql + " " + Contion +
            //              " group by " + MasOutsideGroupBy +
            //              " order by " + orderByFields;

            string Result21 = "SELECT " + MasShowField + " ,"+(SAPDI.Equals("JournalEntries") ?"'<"+ SubDI + ">'+":"") +" (SELECT  " + SubImportField + " FROM  " + MasterTb +
                          " T100 (NOLOCK) inner join " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                         oitm1 +
                        " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                          " WHERE 1 = 1  " + ContionSqlInside + " " + MasLinkField + " " + ContionInside +
                          " Group By " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                          " order by " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                          " FOR XML PATH ";
            string Result22 = string.Empty;
            if (SAPDI.Equals("JournalEntries"))
            {
                string primaryItemSql = string.Empty, primaryItemSqlCredit = string.Empty;
                if (!subimportPrimarydebit.Equals(string.Empty))
                {
                    primaryItemSql = " (SELECT  " + subimportPrimarydebit + ", row_number() over (order by "+ MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) + ")-1 as JDTLineId" + " FROM  " + MasterTb +
                          " T100 (NOLOCK) inner join " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                         oitm1 +
                        " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                          " WHERE 1 = 1  " + ContionSqlInside + " " + MasLinkField + " " + ContionInside +
                          " Group By " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                          " order by " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                          " FOR XML PATH)";
                }
                if (!subimportPrimaryCredit.Equals(string.Empty))
                {
                    primaryItemSql += "  +(SELECT  " + subimportPrimaryCredit + ",(select count(1) from "+ MasterTb +" aa (NOLOCK) inner join " + SubTb + " aa1 (NOLOCK) ON aa1." + KeyField + " = aa." + KeyField  +ContionSqlInside.Replace("T1.","aa.").Replace("T2.","aa1.") + " " + MasLinkField.Replace("T1.", "aa.").Replace("T2.", "aa1.") + " " + ContionInside.Replace("T1.", "aa.").Replace("T2.", "aa1.") + ") + row_number() over (order by " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) + ")-1 as JDTLineId" + " FROM  " + MasterTb +
                                             " T100 (NOLOCK) inner join " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                                            oitm1 +
                                           " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                                             " WHERE 1 = 1  " + ContionSqlInside + " " + MasLinkField + " " + ContionInside +
                                             " Group By " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                                             " order by " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                                             " FOR XML PATH)";
                }
                if (!primaryItemSql.Equals(string.Empty))
                {
                    primaryItemSql = "+'<PrimaryFormItems>'+" + primaryItemSql + "+'</PrimaryFormItems>'";
                }
                    Result22 = ")+(SELECT  " + subimportfieldIsdebit + " FROM  " + MasterTb +
                          " T100 (NOLOCK) inner join " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                         oitm1 +
                        " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                          " WHERE 1 = 1  " + ContionSqlInside + " " + MasLinkField + " " + ContionInside +
                          " Group By " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                          " order by " + MasInsideGroupBy + (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +
                          " FOR XML PATH)"+(SAPDI.Equals("JournalEntries") ?"+'</"+ SubDI + ">'":"")+ primaryItemSql+ " xmlobj ";
            }
            else
            {
                Result22 = ",ROOT('" + SubDI + "')) xmlobj ";
            }
            string Result23 = " FROM  " + MasterTb + " T1 (NOLOCK) inner join " +
                          SubTb + " T2 (NOLOCK) on T1." + KeyField + " = T2." + KeyField +
                           oitm2 +
                           " inner Join " + shpEntry + " T4 on " + ConField + "=T4.shpEntry" +
                          " WHERE 1 = 1  " + ContionSql + " " + Contion +
                          " group by " + MasOutsideGroupBy +
                          " order by " + orderByFields;

            string Result2 = Result21+Result22+Result23;
            Result2 = Result2.Replace("RangeDate", RangeDate).Replace("SAPDBName", SAPDBName).Replace("'BPLID_In'", ShpCode == "ALL" ? "T104.BPLID" : "'" + BPLID + "'"); ;
            oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, Result2,oRichTextBox);

            oDataTable.TableName = SAPDITableName.Substring(1) + "1";
            oDataSet.Tables.Add(oDataTable);

            sqlCmd = "select T1.DocEntry from ToSAP_Role_Mas T1 inner join ToSAP_Role_Sub T2 on T1.DocEntry=T2.DocEntry and T2.DataType='序列号' and T1.DocEntry="+DocEntry1;
            DataTable existsData = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (existsData.Rows.Count > 0)
            {
                string SerLnkSubstr = GetSerLnkSubstr(oSqlConnection, Rolestr.Name, oRichTextBox);  //序列号和子表的内部关联字段
                string SerImportField = GetImportFieldsStr(oSqlConnection, Rolestr.Name, "序列号", string.Empty, SumImport, IsSum,
                    Collect, oRichTextBox);
                string SerImportFieldOut =
                    GetImportGroupFld(oSqlConnection, Rolestr.Name, "序列号Out", string.Empty, SumImport, IsSum, Collect, oRichTextBox);
                string SerSubstr = GetImportFieldsStr(oSqlConnection, Rolestr.Name, "序列号show", string.Empty, SumImport, IsSum, Collect,
                    oRichTextBox);//序列号显示字段
                string SerSubGroupstr = GetImportFieldsStr(oSqlConnection, Rolestr.Name, "序列号Group", string.Empty, SumImport, IsSum, Collect,
                    oRichTextBox);
                string SerLnkSubStrOut =
                    GetImportGroupFld(oSqlConnection, Rolestr.Name, "序列号", SumImport, IsSum, Collect,
                        "Serial", oRichTextBox);  //序列号和子表的外部关联字段
                int ROWNumberStart = SubImportField.IndexOf("ROW_NUMBER()");
                int LineNumStart = SubImportField.IndexOf("as LineNum");

                string Result3 = "SELECT " + MasShowField + "," +
                              "(SELECT  U0.BaseLineNumber," + SerImportFieldOut + " FROM(SELECT " +
                                 SerSubstr.Replace("T102", "T101") + "," +
                                 SubImportField.Substring(ROWNumberStart, LineNumStart - ROWNumberStart) +
                                 "BaseLineNumber" +
                              " FROM  " + MasterTb + " T100 (NOLOCK) INNER JOIN " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                               " inner join OITM T103 (NOLOCK) ON T101.ItemEntry = T103.DocEntry " +
                                " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                              " WHERE 1 = 1  " + ContionSqlInside + " " + MasLinkField + " " + ContionInside +
                              " Group By " + MasInsideGroupBy +  //主表Group
                                 (SubGroupBy == string.Empty ? " " : "," + SubGroupBy) +  //子表Group
                                 (SerSubGroupstr == string.Empty ? " " : "," + SerSubGroupstr) +  //序列号表Group
                                 ") U0 " +
                              "INNER JOIN(SELECT " + SerSubstr + " ," + SerImportField +
                              " FROM " + MasterTb + " T100 (NOLOCK) INNER JOIN " + SubTb + " T101 (NOLOCK) ON T101." + KeyField + " = T100." + KeyField +
                              " INNER JOIN " + SerTb + " T102 (NOLOCK) ON " + SerLnkSubstr +
                              " inner join OITM T103 (NOLOCK) ON T102.ItemEntry=T103.DocEntry AND ISNULL(T103.A12,'')<> '移动平均'" +
                                 " inner Join " + shpEntry + " T104 on " + ConField.Replace("T1.", "T100.").Replace("T2.", "T101.") + "=T104.shpEntry" +
                              " WHERE 1 = 1 " + ContionSqlInside + " " + MasLinkField + " " + ContionInside +
                              " ) U1 ON " + SerLnkSubStrOut + " FOR XML PATH, ROOT('SerialNumbers')) xmlobj" +
                              " FROM " + MasterTb + " T1 (NOLOCK) INNER JOIN " + SubTb + " T2 (NOLOCK) ON T2." + KeyField + " = T1." + KeyField +
                              " inner join OITM T3 on T3.DocEntry = T2.ItemEntry " +
                              " inner Join " + shpEntry + " T4 on " + ConField + "=T4.shpEntry" +
                              " WHERE 1 = 1  " + ContionSql + " " + Contion +
                              " group by " + MasOutsideGroupBy +
                              " order by " + orderByFields;
                Result3 = Result3.Replace("RangeDate", RangeDate).Replace("SAPDBName", SAPDBName).Replace("'BPLID_In'", ShpCode == "ALL" ? "T104.BPLID" : "'" + BPLID + "'"); ;
                oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, Result3, oRichTextBox);
                oDataTable.TableName = "OSRN";
                oDataSet.Tables.Add(oDataTable);
            }
            
            return oDataSet;
        }


        public static string GetLinkKey(SqlConnection oSqlConnection, string Rolestr, string DataType,RichTextBox oRichTextBox)
        {
            string Result = string.Empty;
            int i = 0;
            switch (DataType)
            {
                case "子表":
                    i = 1;
                    break;
                case "序列号":
                    i = 2;
                    break;
            }

            string sqlCmd =
                "SELECT T2.FWField FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject = '" +
                Rolestr + "' AND T2.DataType='" + DataType + "' AND T2.KeyField='Y'";
            DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            foreach (DataRow oDatarow in oData.Rows)
            {
                Result += " and T" + i + "." + oDatarow[0] + "= T" + (i + 1) + "." + oDatarow[0];
            }
            return Result;
        }

        public static int ToSAP_ImportOK(SqlConnection oSqlConnection, string DocType, string DocDescr,
            string DocEntry,string BPLID, string DocDate, string SAPDocType, string SAPDocDescr, string SAPDocEntry,
            string UserCode, string SAPCompany,SqlTransaction oTransaction)
        {
            int ResultRowsCount = 0;
            string sqlCmd = string.Empty;
            try
                {
                    Common.GetFWSqlConn(null);
                    sqlCmd =
                        "INSERT ToSAP_ImportOK(DocType, DocDescr, DocEntry, BPLID,DocDate, SAPDocType, SAPDocDescr, SAPDocEntry, UserCode,SAPCompany,CurVersion) VALUES(" +
                        "N'" + DocType + "',N'" + DocDescr + "',N'" + DocEntry + "',N'" + BPLID + "',N'" + DocDate +
                        "',N'" + SAPDocType +
                        "',N'" + SAPDocDescr + "',N'" + SAPDocEntry + "',N'" + UserCode + "',N'" + SAPCompany + "','" +
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "')";

                    ResultRowsCount = ExecSqlcmd(oTransaction, oSqlConnection, sqlCmd);
                    if (ResultRowsCount<1)
                    {
                    ToSAP_ImportErr(oSqlConnection, "插入标识错误", "", BPLID, DateTime.Now.ToString("yyyy-MM-dd"), DocType,
                        DocDescr, DocEntry, DocEntry,
                        DocDate, SAPDocType, "插入数据错误:" + Environment.NewLine + sqlCmd);
                }
                }
                catch (Exception e)
                {
                    LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\InsertError.Txt",
                        e.Message + Environment.NewLine + sqlCmd);
                    ToSAP_ImportErr(oSqlConnection, "插入标识错误", "", BPLID, DateTime.Now.ToString("yyyy-MM-dd"), DocType,
                        DocDescr, DocEntry, DocEntry,
                        DocDate, SAPDocType, e.Message + Environment.NewLine + sqlCmd);

                    ResultRowsCount = 0;
                }

            return ResultRowsCount;
        }

        public static int ToSAP_ImportOK(SqlConnection oSqlConnection, string DocType, string DocDescr,
            string DocEntry, string BPLID, string DocDate, string SAPDocType, string SAPDocDescr, string SAPDocEntry,
            string UserCode, string SAPCompany)
        {
            int ResultRowsCount = 0;
            string sqlCmd = string.Empty;
            try
            {
                Common.GetFWSqlConn(null);
                sqlCmd =
                    "INSERT ToSAP_ImportOK(DocType, DocDescr, DocEntry, BPLID,DocDate, SAPDocType, SAPDocDescr, SAPDocEntry, UserCode,SAPCompany,CurVersion) VALUES(" +
                    "N'" + DocType + "',N'" + DocDescr + "',N'" + DocEntry + "',N'" + BPLID + "',N'" + DocDate +
                    "',N'" + SAPDocType +
                    "',N'" + SAPDocDescr + "',N'" + SAPDocEntry + "',N'" + UserCode + "',N'" + SAPCompany + "','" +
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "')";
                SqlConnection oSqlConn = new SqlConnection(Program.FWSqlConnStr.ConnectionString);
                oSqlConn.Open();
                    ResultRowsCount = ExecSqlcmd(oSqlConn, sqlCmd);
                    oSqlConn.Close();
            }
            catch (Exception e)
            {
                LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\InsertError.Txt",
                    e.Message + Environment.NewLine + sqlCmd);
                ToSAP_ImportErr(Program.FwSqlConnection, "插入标识错误", "", BPLID, DateTime.Now.ToString("yyyy-MM-dd"), DocType,
                    DocDescr, DocEntry, DocEntry,
                    DocDate, SAPDocType, e.Message + Environment.NewLine + sqlCmd);
                ResultRowsCount = 0;
            }

            return ResultRowsCount;
        }

        public static void ToSAP_ImportErr(SqlConnection oSqlConnection, string ShpCode, string WhsCode,string BPLID,
            string ImportDate, string DocType, string DocDescr, string DocNum, string DocEntry, string DocDate,
            string SAPDocType, string SAPError)
        {
            string sqlCmd =
                "INSERT ToSAP_ImportErr(ShpCode, WhsCode,BPLID, ImportDate, DocType, DocDescr, DocNum, DocEntry, DocDate, SAPDocType, SAPError) VALUES(" +
                "N'" + ShpCode + "',N'" + WhsCode + "',N'"+BPLID+"',GetDate(),N'" + DocType + "',N'" + DocDescr +
                "',N'" + DocNum + "',N'" + DocEntry + "',N'" + DocDate + "',N'" + SAPDocType + "',N'" +
                SAPError.Replace("'", "\"") + "')";
            using (SqlConnection oSqlConn = new SqlConnection(Program.FWSqlConnStr.ConnectionString))
            {
                oSqlConn.Open();
                ExecSqlcmd(oSqlConn, sqlCmd);
            }
        }


     
        public static DataTable oUserAccessShop(SqlConnection oSqlConnection, string oUSER)
        {
            DataTable oAuth = GetAuthorData(oSqlConnection, oUSER);
            DataTable WhsData;
            string sqlCmd;
            string DocEtnryIn = oAuth.Rows[0]["WhsDeploy"].ToString().Trim();
            if (DocEtnryIn.Equals(""))
            {
                DocEtnryIn = "''";
            }
                sqlCmd = "SELECT N'N' AreaS,address Area1,  N'N'Chs, WhsCode,'[仓库]'+WhsName WhsName,DocEntry Entry FROM OWHS WHERE  '" + Program.showOWHS+"'  like '%仓库%' and ('" +
                     oAuth.Rows[0]["WhsAll"].ToString().Trim() + "' = 'True' OR DocEntry IN(" +
                     DocEtnryIn + "))";
            WhsData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);

            string ShpDeploy = oAuth.Rows[0]["ShpDeploy"].ToString().Trim();
            string oneStr = ShpDeploy.Length < 1 ? "" : ShpDeploy.Substring(0, 1);
            string oCondition = " OR DocEntry in (" + oAuth.Rows[0]["ShpDeploy"].ToString().Trim() + ")";
            if (oneStr.Equals(""))
            {
                oCondition = "";
            }
            if (oneStr.Equals("-"))
            {
                oCondition = " OR DocEntry in (" + GetOldDocentry(oSqlConnection, "OSHP", oUSER) + ")";
            }

            string Canshow = Program.Shop.Equals("是")
                ? " and 1=1"
                : " and shpCode in(select WhsCode from ToSAP_BranchDetail where Disable='N' and WhsType = '店铺')";
            sqlCmd = "SELECT N'N' AreaS, CityArea Area1, N'N' Chs,shpCode  WhsCode ,'[店铺]'+shpName WhsName ,10000+Docentry Entry FROM OSHP where shpName not like '%后仓%' and IsInvalid=0 and '" + Program.showOWHS + "'  like '%店铺%' and  ('" +
                     oAuth.Rows[0]["ShpAll"].ToString().Trim() + "' = 'True' " +
                     oCondition+")" +Canshow;
            DataTable shpData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            foreach (DataRow oRow in shpData.Rows)
            {
                WhsData.ImportRow(oRow);
            }
            WhsData.AcceptChanges();
            DataView oDataView = WhsData.DefaultView;
            oDataView.Sort = "WhsCode";
            DataTable oDataTable = oDataView.ToTable("new_dara", false);
            
            return oDataTable;
        }

        public static void saveXmltoFile(string xmlStr, string ofile)
        {
            XmlDocument oXmlDocument = new XmlDocument();
            oXmlDocument.LoadXml(xmlStr);
            oXmlDocument.Save(ofile);
        }

        public static void SAPFieldAdd(string shpCode,string oUser)
        {
           bool WebLogSAP= Common.WebLogSAP(shpCode,oUser,null);
            if (WebLogSAP==false)
            {
                MessageBox.Show("登陆SAP失败,请检查同步帐号配置和SAP服务器信息配置!");
                return;
            }
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            string importXML =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>152</Object><Version>2</Version></AdmInfo><UserFieldsMD><row><Name>FWDocNum</Name><Type>db_Alpha</Type><Description>ERP单号</Description><SubType>st_None</SubType><TableName>ORDR</TableName><Size>200</Size><Mandatory>tNO</Mandatory></row></UserFieldsMD></BO></BOM>";
            oDictionary.Add("AddFieldxml", importXML);
            string importxml = postString("AddField", oDictionary,null);
            JObject result =  soapPost(Program.serviceurl, importxml,"N",shpCode);

           oDictionary.Clear();
            importXML =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>152</Object><Version>2</Version></AdmInfo><UserFieldsMD><row><Name>FWDocType</Name><Type>db_Alpha</Type><Description>ERP单据类型</Description><SubType>st_None</SubType><TableName>ORDR</TableName><Size>200</Size><Mandatory>tNO</Mandatory></row></UserFieldsMD></BO></BOM>";
            oDictionary.Add("AddFieldxml", importXML);
            importxml = postString("AddField", oDictionary, null);
            result =  soapPost(Program.serviceurl, importxml,"N",shpCode);

            oDictionary.Clear();
            importXML =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>152</Object><Version>2</Version></AdmInfo><UserFieldsMD><row><Name>shpCode</Name><Type>db_Alpha</Type><Description>店铺编码</Description><SubType>st_None</SubType><TableName>ORDR</TableName><Size>200</Size><Mandatory>tNO</Mandatory></row></UserFieldsMD></BO></BOM>";
            oDictionary.Add("AddFieldxml", importXML);
            importxml = postString("AddField", oDictionary, null);
            result =  soapPost(Program.serviceurl, importxml, "N", shpCode);

            oDictionary.Clear();
            importXML =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>152</Object><Version>2</Version></AdmInfo><UserFieldsMD><row><Name>FWDocNum</Name><Type>db_Alpha</Type><Description>ERP单号</Description><SubType>st_None</SubType><TableName>RDR1</TableName><Size>200</Size><Mandatory>tNO</Mandatory></row></UserFieldsMD></BO></BOM>";
            oDictionary.Add("AddFieldxml", importXML);
            importxml = postString("AddField", oDictionary, null);
            result =  soapPost(Program.serviceurl, importxml,"N",shpCode);

            oDictionary.Clear();
            importXML =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>152</Object><Version>2</Version></AdmInfo><UserFieldsMD><row><Name>FWDocType</Name><Type>db_Alpha</Type><Description>ERP单据类型</Description><SubType>st_None</SubType><TableName>RDR1</TableName><Size>200</Size><Mandatory>tNO</Mandatory></row></UserFieldsMD></BO></BOM>";
            oDictionary.Add("AddFieldxml", importXML);
            importxml = postString("AddField", oDictionary, null);
            result = soapPost(Program.serviceurl, importxml,"N",shpCode);

            oDictionary.Clear();
            importXML =
                "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>152</Object><Version>2</Version></AdmInfo><UserFieldsMD><row><Name>FWDocLine</Name><Type>db_Alpha</Type><Description>ERP单据行</Description><SubType>st_None</SubType><TableName>RDR1</TableName><Size>200</Size><Mandatory>tNO</Mandatory></row></UserFieldsMD></BO></BOM>";
            oDictionary.Add("AddFieldxml", importXML);
            importxml = postString("AddField", oDictionary, null);
            result =  soapPost(Program.serviceurl, importxml,"N",shpCode);
            weblogOutSAP(shpCode,oUser);
            MessageBox.Show("初始化完成!");

        }

        public static string GetKeyField(SqlConnection oSqlConnection, string RoleName,RichTextBox oRichTextBox)
        {
            string sqlCmd =
                "SELECT T2.FWField FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject ='" +
                RoleName + "' AND T2.KeyField = 'Y' AND T2.DataType='主表'";
            return GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox).Rows[0][0].ToString();
        }

        public static string GetSerLnkSubstr(SqlConnection oSqlConnection, string RoleName,RichTextBox oRichTextBox)
        {
            string Result = string.Empty;
            string sqlCmd =
                "SELECT T2.FWField FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject ='" +
                RoleName + "' AND T2.KeyField = 'Y' AND T2.DataType='序列号'";
            DataTable oData = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,oRichTextBox);
            foreach (DataRow oDataRow in oData.Rows)
            {
                Result += " and T101." + oDataRow[0] + "=T102." + oDataRow[0];
            }
            return Result.Substring(4);
        }

        public static DataSet GetAccDataSet(string sqlCmd, string TabName)
        {
            string cCnn = "";
            //if (System.Environment.Is64BitProcess)
            //{
            //    cCnn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Application.StartupPath + "\\Template.mdb";
            //}
            //else
            //{
            //  cCnn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\Template.mdb"; 
            //}

            //OleDbConnection objCnn = new OleDbConnection(cCnn);
            //OleDbDataAdapter da = new OleDbDataAdapter(sqlCmd, objCnn);

            //try
            //{
            //    da.Fill(ds, TabName);
            //    return ds;
            //}
            //catch (Exception e)
            //{
            //    MessageBox.Show("请安装AccessDatabaseEngine"+System.Environment.NewLine+e.ToString());
            //    return null;
            //}
            DataSet ds = new DataSet();
            try
            {
                System.Data.SQLite.SQLiteConnection oSqLiteConnection = new SQLiteConnection();
                oSqLiteConnection.ConnectionString = "data source=" + AppDomain.CurrentDomain.BaseDirectory + "Template.db";
                oSqLiteConnection.Open();

                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlCmd, oSqLiteConnection);
                DataTable table = new DataTable(TabName);
                adapter.Fill(table);
                ds.Tables.Add(table);
                oSqLiteConnection.Close();
                return ds;
            }
            catch (Exception e)
            {
                return null;

            }
           
        }
        public static DataSet GetAccDataSet(string TabName)
        {
            string cCnn = "";
            if (System.Environment.Is64BitProcess)
            {
                cCnn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Application.StartupPath + "\\Template.mdb";
            }
            else
            {
                cCnn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\Template.mdb";
            }
            OleDbConnection objCnn = new OleDbConnection(cCnn);
            DataSet ds = new DataSet();
            try
            {
                objCnn.Open();
               objCnn.Close();
                return ds;
            }
            catch (Exception e)
            {
                MessageBox.Show("请安装程序目录下的AccessDatabaseEngine_X64" + System.Environment.NewLine + e.ToString());
                return null;
            }
        }

        public static void APPinit(Form oForms)
        {
            
            string sqlCmd1 = "select * from objlist order by addorder, objtype,objname";
            DataSet oDataSet = GetAccDataSet(sqlCmd1, "OBJN");
            if (oDataSet== null) { return;}
            foreach (DataRow oDataRow in oDataSet.Tables[0].Rows)
            {
                string objtype1 = oDataRow["objType"].ToString();
                string objtype1Str = "";
                switch (objtype1)
                {
                    case "TB":
                        objtype1Str = "正在创建表";
                    break;
                    case "VIEW":
                        objtype1Str = "正在创建视图";
                        break;
                    case "IN":
                        objtype1Str = "正在创索引";
                        break;
                    case "PR":
                        objtype1Str = "正在创建存储过程";
                        break;
                    case "FN":
                        objtype1Str = "正在创建存函数";
                        break;

                }
                //Form.ActiveForm.Refresh();
                if (!TableIsExist(Program.FwSqlConnection, oDataRow["objName"].ToString(), objtype1))
                {
                    oForms.Show();
                    ToSAP.Init.ShowMessageForm.label1.Text = objtype1Str;
                    ToSAP.Init.ShowMessageForm.label1.Refresh();
                    ToSAP.Init.ShowMessageForm.label2.Text = oDataRow["objName"].ToString();
                    ToSAP.Init.ShowMessageForm.label2.Refresh();

                    string sql_Cmd = oDataRow["SQL"].ToString();
                    Common.GetFWSqlConn(null);
                    ExecSqlcmd(Program.FwSqlConnection, sql_Cmd);
                }
            }
            //string cCnn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Application.StartupPath + "\\Template.mdb";
            //var cCnn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\Template.mdb";
            //OleDbConnection objCnn = new OleDbConnection(cCnn);
            //objCnn.Open();
            SQLiteConnection objCnn = new SQLiteConnection("data source ="+AppDomain.CurrentDomain.BaseDirectory+"Template.db");
            objCnn.Open();

            DataTable alloDt = objCnn.GetSchema("TABLES");
            DataRow[] resultRows = alloDt.Select("TABLE_TYPE = 'TABLE'");
            foreach (DataRow oDataRow in resultRows)
            {
                string TableName = oDataRow["TABLE_NAME"].ToString();
                if (TableName.ToUpper().Equals("OBJLIST") || TableName.ToUpper().Equals("SQLVERSION"))
                {
                    continue;
                }
               
                if (!TableIsExist(Program.FwSqlConnection, TableName, "DataExt"))
                {
                    oForms.Show();
                    DataTable ColunmList = objCnn.GetSchema("columns", new string[] { null, null, TableName });
                    string sqlCmd2 = "";
                    foreach (DataRow oColunmListRow in ColunmList.Rows)
                    {
                        string columnName = oColunmListRow["ColUMN_NAME"].ToString();
                        if (!columnName.Equals("ID"))
                        {
                            sqlCmd2 += ',' + oColunmListRow["ColUMN_NAME"].ToString();
                        }

                    }

                    sqlCmd1 = "select " + sqlCmd2.Substring(1) + " from " + TableName;
                    ToSAP.Init.ShowMessageForm.label1.Text = "正在导入初始数据";
                    ToSAP.Init.ShowMessageForm.label1.Refresh();
                    ToSAP.Init.ShowMessageForm.label2.Text = TableName;
                    ToSAP.Init.ShowMessageForm.label2.Refresh();
                    DataSet newDataset = GetAccDataSet(sqlCmd1, "Data1");
                    foreach (DataRow okDt in newDataset.Tables[0].Rows)
                    {
                        for (int i = 0; i < okDt.ItemArray.Length; i++)
                        {
                            okDt[i] = okDt.IsNull(i) ? "" : okDt[i];
                        }
                    }
                    string sqlCmd3 =
                        "SELECT name FROM sys.columns T1 WHERE	object_id=OBJECT_ID('"+ TableName + "') AND T1.is_identity=1";
                    DataTable tmoDt=GetDataTable(Program.FWSqlConnStr.ConnectionString,sqlCmd3,null);
                    if (tmoDt.Rows.Count > 0)
                    {
                        Common.GetFWSqlConn(null);
                        string sqlCmd4 = "SET IDENTITY_INSERT " + TableName + " OFF";
                        ExecSqlcmd(Program.FwSqlConnection, sqlCmd4);
                        InsertDataToTable(null, Program.FwSqlConnection, newDataset.Tables[0], TableName);
                        sqlCmd4 = "SET IDENTITY_INSERT " + TableName + " ON";
                        ExecSqlcmd(Program.FwSqlConnection, sqlCmd4);
                        continue;
                    }
                    tmoDt=new DataTable();
                     sqlCmd3 =
                        "SELECT name FROM sys.columns T1 WHERE	object_id=OBJECT_ID('" + TableName + "')";
                     tmoDt = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd3, null);
                    if (tmoDt.Rows.Count > 0)
                    {
                        //string sqlCmd4 = "SET IDENTITY_INSERT " + TableName + " OFF";
                        //ExecSqlcmd(Program.FwSqlConnection, sqlCmd4);
                        InsertDataToTable(null, Program.FwSqlConnection, newDataset.Tables[0], TableName);
                        //sqlCmd4 = "SET IDENTITY_INSERT " + TableName + " ON";
                        //ExecSqlcmd(Program.FwSqlConnection, sqlCmd4);
                    }
                }
            }
            objCnn.Close();
        }
        // 0 继续  1 退出程序
        public static int AlterDataBase()
        {
            string LocalSqlVersion = "0";
            string sqlCmd0 = "select Version1 from SQLVersion  order by Version1 Desc LIMIT 1 ";
            DataSet oDataSet1 = GetAccDataSet(sqlCmd0, "SQL1");
            try
            {
                LocalSqlVersion = oDataSet1.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception e)
            {
               
            }

            Common.GetFWSqlConn(null);
            string sqlDataBaseVersion = "0";
            int InPara = 0;
            string sqlCmd = "SELECT Value1 FROM ToSAP_Appinit WHERE ParaCode='SQLVersion'";
            DataTable oDataTable = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (oDataTable.Rows.Count > 0)
            {
                sqlDataBaseVersion = oDataTable.Rows[0][0].ToString();
                if(LocalSqlVersion.Equals(sqlDataBaseVersion))
                {return 0;}
                InPara = 1;
            }
            string sqlCmd1 = "select * from SQLVersion  where Version1>'"+ sqlDataBaseVersion + "'";
            DataSet oDataSet = GetAccDataSet(sqlCmd1, "SQL1");
            if (oDataSet == null) { return 1; }

            string sqlNewVersion = "0";
            if (oDataSet.Tables["SQL1"].Rows.Count > 0)
            {
                sqlCmd1 = "select Version1 from SQLVersion  order by Version1 Desc LIMIT 1 ";
                sqlNewVersion = GetAccDataSet(sqlCmd1, "SQL1").Tables["SQL1"].Rows[0][0].ToString();
            }
           
            try
            {
                Common.GetFWSqlConn(null);
                foreach (DataRow oDataRow in oDataSet.Tables["SQL1"].Rows)
                {
                    string sqlCmd4 = oDataRow["sqlCmd"].ToString();
                    try
                    {
                        ExecSqlcmd(Program.FwSqlConnection, sqlCmd4);
                    }
                    catch (Exception e)
                    {
                       
                    }

                   
                }
                
                if (InPara == 1)
                {
                    sqlCmd = "Update ToSAP_Appinit set Value1='"+ sqlNewVersion + "' where ParaCode ='SQLVersion' ";
                    
                }
                else
                {
                    sqlCmd = "INSERT ToSAP_Appinit(ParaCode, ParaName, Value1, Value2) VALUES('SQLVersion', '接口程序SQL版本', '"+sqlNewVersion+"', '')";
                }
                ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
                return  sqlNewVersion.Equals(LocalSqlVersion)?0:1;
            }
            catch (Exception e)
            {
                MessageBox.Show("升级数据库失败!"+System.Environment.NewLine+ e.ToString());
                return 1;
            }
            
        }

        public static bool TableIsExist(SqlConnection oSqlConnection, string TableName, string dataType)
        {
            string sqlCmd = string.Empty;
            if (dataType == "TB")
            {
                sqlCmd = "SELECT name FROM sys.tables WHERE name = '" + TableName + "'";
                DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                if (oDataTable.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }

            if (dataType == "IN")
            {
                sqlCmd = "SELECT name FROM sys.indexes WHERE name =  '" + TableName + "'";
                DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                if (oDataTable.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }

            if (dataType == "FN")
            {
                sqlCmd = "select Name FROM dbo.sysobjects where name = '" + TableName + "'" +
                         " and xtype in (N'FN', N'IF', N'TF')";
                DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                if (oDataTable.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }
            if (dataType == "PR")
            {
                sqlCmd = "SELECT * FROM sys.procedures WHERE name =  '" + TableName + "'" ;
                DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                if (oDataTable.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }

            if (dataType == "DataExt")
            {
                sqlCmd = "SELECT TOP 1 * FROM  " + TableName;
                DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                if (oDataTable.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }
            if(dataType=="VIEW")
            {
                sqlCmd = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '" + TableName+"'";
                DataTable oDataTable = GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                if (oDataTable.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }

            return true;
        }

        public static void LogFileWrite(string filename, string LogTxt)
        {
            StreamWriter oFileWriter = new StreamWriter(filename, true);
            oFileWriter.Write(LogTxt);
            oFileWriter.Flush();
            oFileWriter.Close();
            oFileWriter.Dispose();
        }

        public static string ReadTxTFirstRecord(string filename)
        {
            
            string[] str =  File.ReadAllLines(filename);
            if (str.Length > 0)
            {
                return str[0].ToString();
            }

            return "0";
        }

        public static void sendMessage(RichTextBox oRichTextBox,string shpCode,CheckBox Rolestr,string RowCountAll,string RowCount,string showMessage,bool err)
        {
            if (oRichTextBox == null)
            {
                return;

            }
            oRichTextBox.SelectionColor = Color.Black;
            if (err)
            {
                oRichTextBox.SelectionColor = Color.Red;
            }
            oRichTextBox.AppendText("店铺:" + shpCode + "正在同步:" + Rolestr.Text + "   数据:" + RowCountAll + "/" +RowCount + " :" + showMessage+ Environment.NewLine);
            oRichTextBox.ScrollToCaret();
        }
        public static void sendMessage(RichTextBox oRichTextBox, string shpCode, string showMessage, bool err)
        {
            if (oRichTextBox == null)
            {
                return;

            }
            oRichTextBox.SelectionColor = Color.Black;
            if (err)
            {
                oRichTextBox.SelectionColor = Color.Red;
            }
            oRichTextBox.AppendText(Program.DispWhsShp+":" + shpCode + ":" + showMessage + Environment.NewLine);
            oRichTextBox.ScrollToCaret();
        }
        public static void sendMessage(RichTextBox oRichTextBox, string showMessage, bool err)
        {
            if (oRichTextBox == null)
            {
                return;

            }
            oRichTextBox.SelectionColor = Color.Black;
            if (err)
            {
                oRichTextBox.SelectionColor = Color.Red;
            }
            oRichTextBox.AppendText(Program.DispWhsShp + ":"+showMessage + Environment.NewLine);
            oRichTextBox.ScrollToCaret();
        }
        public static void WriteCookiesToDisk(string file, CookieContainer cookieJar)
        {
            using (Stream stream = File.OpenWrite(file))
            {
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, cookieJar);
                    stream.Close();
                }
                catch (Exception e)
                {
                }
            }
        }
        public static CookieContainer ReadCookiesFromDisk(string file)
        {
            try
            {
                using (Stream stream = File.Open(file,FileMode.Open,FileAccess.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    CookieContainer oContainer= (CookieContainer)formatter.Deserialize(stream);
                    stream.Close();
                    return oContainer;
                }
            }
            catch (Exception e)
            {
                return new CookieContainer();
            }
        }

        public static JObject soapPost (string serviceurl, string soap,string ReLog,string shopCode)
        {
            GC.Collect();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serviceurl);  //定义http
            //启用cookie
            string cookieFile =  Application.StartupPath + "\\CookieContainer" + shopCode + ".bin";  //cookie保存位置
            if (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据"))
            {
                cookieFile = Application.StartupPath + "\\CookieContainer" + "ALL" + ".bin";  //cookie保存位置
            }
             
            FileInfo oFileInfo = new FileInfo(cookieFile);
            request.CookieContainer = new CookieContainer();
            if (oFileInfo.Exists)
            {
                if (ReLog.Equals("Y"))
                {
                    oFileInfo.Delete();
                }
                else
                {
                    request.CookieContainer = ReadCookiesFromDisk(cookieFile); //读取保存的cookie 
                }
            }
            request.Proxy = null;
            request.ContentType = "application/soap+xml; charset=utf-8";
            request.Method = "POST";
            request.Timeout = 300000;
            request.ContinueTimeout = 300000;
            request.ReadWriteTimeout = 300000;
            request.ServicePoint.ConnectionLimit = int.MaxValue;
            request.KeepAlive = true;
            //webRequest.Host = webRequest.RequestUri.Host;
            request.ContentLength = Encoding.UTF8.GetBytes(soap).Length; //soap.Length;

            //request.CookieContainer = CookiesContainer;
            HttpWebResponse webResponse;
            try
            {
                using (Stream requestStream = request.GetRequestStream())
                {
                    byte[] paramBytes = Encoding.UTF8.GetBytes(soap);
                    requestStream.Write(paramBytes, 0, paramBytes.Length);
                    requestStream.Close();
                }

                request.ContentType = "text/xml";
                webResponse =  request.GetResponse() as HttpWebResponse;
                byte[] contentData = System.Convert.FromBase64String(webResponse.Headers["ResultMsg"]);
                byte[] uwrapBytes = LZ4.LZ4Codec.Unwrap(contentData);
                string contentString = System.Text.Encoding.UTF8.GetString(uwrapBytes);
                WriteCookiesToDisk(cookieFile, request.CookieContainer);
                request = null;
                webResponse.Dispose();
                webResponse = null;
                return JObject.Parse( contentString);
                // CookiesContainer = request.CookieContainer;
            }
            catch (WebException e)
            {
                JObject oerrJObject = new JObject();
                webResponse = (HttpWebResponse)e.Response;
                string err="";
                try
                {
                    StreamReader sr1 = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8);
                    err = sr1.ReadToEnd();
                    sr1.Close();
                }
                catch (Exception exception)
                {
                    
                }
                oerrJObject.Add("errCode","-5201314");
                oerrJObject.Add("errMsg",e.ToString()+Environment.NewLine+err+Environment.NewLine+e.ToString());
                oerrJObject.Add("WebVersion","");
                //webResponse.Dispose();
                webResponse = null;

                return oerrJObject;
            }

            
        }
        public static string postString(string method, Dictionary<string, string> oDictionary,DataTable Valuedt)
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            soap.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">");
            soap.Append("<soap12:Body>");
            soap.Append("<" + method + " xmlns = \"http://tempuri.org/\">");
            foreach (var oDictionary1 in oDictionary)
            {
                byte[] value1 = System.Text.Encoding.Unicode.GetBytes(oDictionary1.Value);
                string value2 = Convert.ToBase64String(LZ4.LZ4Codec.Wrap(value1));
                soap.Append("<" + oDictionary1.Key + ">" + value2 + "</" + oDictionary1.Key + ">");
            }
            string Valuedt1 = String.Empty;
            if (Valuedt != null)
            {
                Valuedt1 = JsonConvert.SerializeObject(Valuedt);
                byte[] dtBytes =
                    System.Text.Encoding.Unicode.GetBytes(Valuedt1);
                byte[] wrapBytes = LZ4.LZ4Codec.Wrap(dtBytes);
                string contentBase64String = System.Convert.ToBase64String(wrapBytes);
                soap.Append("<ValueDt>" + contentBase64String + "</ValueDt>");
            }
            soap.Append("</" + method + ">");
            soap.Append("</soap12:Body>");
            soap.Append("</soap12:Envelope>");
          
            return soap.ToString();
        }

        public  static void sendDataToWeb(string serviceurl, string objType, string obj, DataSet oDataSet, string shpCode, string shpEntry, CheckEdit oCheckEdit, 
            string ERPobject, string SAPobject, string SAPDB, string DiDataTable,RichTextBox oRichTextBox,string BPLID)
        {
            GC.Collect();
           // oRichTextBox.Clear();
            string sqlCmd = "", keyField = "keyField", checkCost = "", xmlhead = "", xmltitle = "", DispMesTxt = "";
            if (!oCheckEdit.Name.Equals("checkAll3"))
            {
                DispMesTxt = oCheckEdit.Text;
                if (!oCheckEdit.Name.Equals("OSALToSAP"))
                {
                    sqlCmd =
                        "SELECT T2.FWField ,T1.checkCost FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject='" +
                        oCheckEdit.Name + "' AND T2.DataType='主表' AND T2.KeyField='Y'";

                    DataTable cxData = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, oRichTextBox);
                    keyField = cxData.Rows[0][0].ToString();
                    checkCost = cxData.Rows[0][1].ToString();
                    if (oDataSet.Tables[DiDataTable].Columns.IndexOf(keyField) < 0)
                    {
                        keyField = "keyField";
                    }
                    xmlhead = "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>XXoo</Object><Version>2</Version></AdmInfo></BO></BOM>";
                    XmlDocument oXmlDocument = new XmlDocument();
                    oXmlDocument.LoadXml(xmlhead);
                    oXmlDocument.SelectSingleNode("/BOM/BO/AdmInfo/Object").InnerText = objType;
                    xmltitle = oXmlDocument.InnerXml;
                }
            }
            

            if ((obj.Equals("单据") || obj.Equals("凭证")) && !Program.DocToSAP.Equals("所有单") && !Program.DocToSAP.Equals("选择单据"))
            {
                oDataSet.Tables[DiDataTable].TableName = DiDataTable + "STRAY";
                oDataSet.Tables[DiDataTable + "STRAY"].Columns.Add("CanImport", typeof(string));
                oDataSet.Tables[DiDataTable + "STRAY"].Columns.Add("XH_PK", typeof(int));
                int xh_PK = 0;
                foreach (DataRow oDataRowda in oDataSet.Tables[DiDataTable + "STRAY"].Rows)
                {
                    oDataRowda["XH_PK"] = xh_PK;
                    xh_PK += 1;
                }
                oDataSet.AcceptChanges();
                DataView dv = oDataSet.Tables[DiDataTable + "STRAY"].DefaultView;
                dv.Sort = "DocEntry Asc,Sort1 Asc";
                DataTable dt2 = dv.ToTable();
                dt2.TableName = DiDataTable;
                oDataSet.Tables.Add(dt2);
                DataColumn[] primaryCol = new DataColumn[1];
                primaryCol[0] = oDataSet.Tables[DiDataTable].Columns["XH_PK"];
                oDataSet.Tables[DiDataTable].PrimaryKey = primaryCol;
                oDataSet.Tables.Remove(DiDataTable + "STRAY");
                oDataSet.AcceptChanges();
            }

            if ((obj.Equals("单据") || obj.Equals("凭证")) && (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据")))
            {
                oDataSet.Tables[DiDataTable].TableName = DiDataTable + "STRAY";
                oDataSet.Tables[DiDataTable + "STRAY"].Columns.Add("ToOrder0",typeof(int));
                oDataSet.Tables[DiDataTable + "STRAY"].Columns.Add("XH_PK", typeof(int));
                oDataSet.Tables[DiDataTable + "STRAY"].Columns.Add("CanImport", typeof(string));
                int xh_PK = 0;
                foreach (DataRow oDataRowda in oDataSet.Tables[DiDataTable + "STRAY"].Rows)
                {
                    oDataRowda["ToOrder0"] = Convert.ToInt64(oDataRowda["ToOrder"]);
                    oDataRowda["DocDate"] = Convert.ToDateTime(oDataRowda["DocDate"]);
                    oDataRowda["XH_PK"] = xh_PK;
                    xh_PK += 1;
                }
                oDataSet.AcceptChanges();
                
                DataView dv = oDataSet.Tables[DiDataTable + "STRAY"].DefaultView;
                dv.Sort = "DocDate1 ASC , DocDate Asc ,ToOrder0 Asc";
                DataTable dt2 = dv.ToTable();
                dt2.TableName = DiDataTable;
                oDataSet.Tables.Add(dt2);
                DataColumn[] primaryCol = new DataColumn[1];
                primaryCol[0] = oDataSet.Tables[DiDataTable].Columns["XH_PK"];
                oDataSet.Tables[DiDataTable].PrimaryKey = primaryCol;
                oDataSet.Tables.Remove(DiDataTable + "STRAY");
                oDataSet.AcceptChanges();
            }


            int RowCount = 0;
            foreach (DataRow odataRow in oDataSet.Tables[DiDataTable].Rows)
            {
               
                if (Program.threadDie == 1) {Program.thread.Abort(); return; }

                if (oRichTextBox.Lines.Length > 10000)
                {
oRichTextBox.Clear();
                }
                Application.DoEvents();
                GC.Collect();
                RowCount++;
                string erpsapobj = "";
                string GrpBPLID = BPLID;
                if (odataRow.Table.Columns.Contains("BPL_IDAssignedToInvoice"))
                {
                    GrpBPLID = odataRow["BPL_IDAssignedToInvoice"].ToString().Trim();
                }
                if (odataRow.Table.Columns.Contains("SAPCompany"))
                {
                    SAPDB = odataRow["SAPCompany"].ToString().Trim();
                }
                string Descstring = string.Empty;
                if(odataRow.Table.Columns.Contains("importshpCode"))
                {
                    shpCode = odataRow["importshpCode"].ToString();
                }
               
                if ((Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据")) && (obj.Equals("单据")|| obj.Equals("凭证")))
                {
                    xmlhead = "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>XXoo</Object><Version>2</Version></AdmInfo></BO></BOM>";
                    XmlDocument oXmlDocument = new XmlDocument();
                    oXmlDocument.LoadXml(xmlhead);
                    oXmlDocument.SelectSingleNode("/BOM/BO/AdmInfo/Object").InnerText = odataRow["objecttype"].ToString();
                    xmltitle = oXmlDocument.InnerXml;
                    Descstring=" "+ odataRow["RoleDesc"].ToString()+"  "; 
                }

                string GetSAPCost = "", SAPCostOK = "N", CanImport = "Y";//CanImport 判断行数据是否需要导入，主要用在店调店、仓调仓、回仓单上，如果出库单未成功，则入库单不导入
                if (obj.Equals("单据") || obj.Equals("凭证") || obj.Equals("库存转储"))
                {
                    erpsapobj = odataRow["ERPobj"].ToString() + odataRow["SAPobj"].ToString();
                    ERPobject = odataRow["ERPobj"].ToString();
                    SAPobject = odataRow["SAPobj"].ToString();
                    GetSAPCost = odataRow["GetSAPCost"].ToString();
                    SAPCostOK = odataRow["SAPCost"].ToString();
                    CanImport = odataRow["CanImport"].ToString().Trim().Equals("N") ? "N" : "Y";
                    
                }
                if (oCheckEdit.Name.Equals("OSALToSAP"))
                {
                    sqlCmd =
                        "SELECT T2.FWField ,T1.checkCost FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject='" +
                        erpsapobj + "' AND T2.DataType='主表' AND T2.KeyField='Y'";

                    DataTable cxData = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                    keyField = cxData.Rows[0][0].ToString();
                    checkCost = cxData.Rows[0][1].ToString();
                    if (oDataSet.Tables[DiDataTable].Columns.IndexOf(keyField) < 0)
                    {
                        keyField = "keyField";
                    }

                    sqlCmd =
                        " SELECT * FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_DIobject T2 ON T1.SAPobject = T2.SAPobjCode LEFT JOIN ToSAP_DIinterface T3 ON T2.SAPDIinterface = T3.DIInterface WHERE ERPobject+SAPobject = '" +
                        erpsapobj + "'";
                    DataTable objData = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                    string SAPDIinterface = objData.Rows[0]["SAPDIinterface"].ToString();
                    DispMesTxt = "零售单-->" + objData.Rows[0]["SAPobjName"].ToString(); ;
                    BoObjectTypes oBoObjectTypes = (BoObjectTypes)Enum.Parse(typeof(BoObjectTypes), SAPDIinterface);
                    int objecttype = (int)(oBoObjectTypes);

                    xmlhead = "<?xml version=\"1.0\" encoding=\"UTF-16\"?><BOM><BO><AdmInfo><Object>XXoo</Object><Version>2</Version></AdmInfo></BO></BOM>";
                    XmlDocument oXmlDocument = new XmlDocument();
                    oXmlDocument.LoadXml(xmlhead);
                    oXmlDocument.SelectSingleNode("/BOM/BO/AdmInfo/Object").InnerText = objecttype.ToString();
                    xmltitle = oXmlDocument.InnerXml;
                }
                string CardCode = "",CardCodeDsr=string.Empty;

                if (odataRow.Table.Columns.Contains("CardCode"))
                {
                    CardCode = " 业务伙伴:" + odataRow["CardCode"].ToString();
                    CardCodeDsr = odataRow["CardCode"].ToString();
                }
                string importDocNum = "";
                if (odataRow.Table.Columns.Contains("DocNum"))
                {
                    importDocNum = "单号:" + odataRow["DocNum"];
                }
                Descstring += CardCode + " " + importDocNum;
                if (oRichTextBox != null)
                {
                    oRichTextBox.SelectionColor = Color.Black;
                    oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "正在同步: " + DispMesTxt + "   数据: " + oDataSet.Tables[DiDataTable].Rows.Count + "/" + RowCount + " :" + odataRow[keyField] + " " + Descstring +" "+DateTime.Now.ToLongTimeString()+ Environment.NewLine);
                    oRichTextBox.ScrollToCaret();
                }
                if (CanImport.Equals("N"))
                {
                    if (oRichTextBox != null)
                    {
                        oRichTextBox.SelectionColor = Color.Red;
                        oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt + "   数据: " + odataRow[keyField] + " " + Descstring + " 获取SAP成本规则单据未同步成功,请检查!" + Environment.NewLine);
                        
                    }
                    Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, string.Empty, GrpBPLID, DateTime.Now.ToString("yyyy-MM-dd"),
                            ERPobject, string.Empty, importDocNum.Length >= 3 ? importDocNum.Substring(3) : "", odataRow[keyField].ToString(), DateTime.Now.ToString("yyyy-MM-dd"), SAPobject, Descstring + "  获取SAP成本规则单据未同步成功,请检查!");
                    continue;
                }
                if (checkCost.Equals("Y"))
                {
                    if (odataRow["checkCost"].ToString() == "N" && (obj.Equals("单据") || obj.Equals("凭证") || obj.Equals("库存转储")))
                    {
                        oRichTextBox.SelectionColor = Color.Red;
                        oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt + "   数据: " + odataRow[keyField] + " " + Descstring + " 入库成本不正常,请检查!" + Environment.NewLine);
                        Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, string.Empty, GrpBPLID, DateTime.Now.ToString("yyyy-MM-dd"),
                            ERPobject, string.Empty, importDocNum.Length>=3?importDocNum.Substring(3):"", odataRow[keyField].ToString(), DateTime.Now.ToString("yyyy-MM-dd"), SAPobject, Descstring + " 入库成本不正常,请检查!" );
                        continue;
                    }

                }

                
                DataSet tmpDataSet = new DataSet();
                if (GetSAPCost.Equals("Y")  && (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据")))  //重新查询取数
                {
                    CheckEdit oCheckEdit1 = new CheckEdit();
                    oCheckEdit1.Name = ERPobject + SAPobject;
                    oCheckEdit1.Text = ERPobject + SAPobject;
                    string ReDocEntry = odataRow["DocEntry"].ToString();
                    string DocDate = odataRow["DocDate"].ToString().Substring(0,10);
                    string whsCode = odataRow["importshpCode"].ToString();
                    
                    tmpDataSet =GetImportData(Program.FwSqlConnection, oCheckEdit1, "ALL",
                        Program.InterFaceSet, Program.CorpSet, Program.shpCode, Program.pstDate, Program.penDate, "ToSAP_OSHP_" + Program.oUSER, GrpBPLID, null, "ODOC", ReDocEntry);
                    SAPCostOK = tmpDataSet.Tables["ODOC"].Rows[0]["SAPCost"].ToString();
                }

                if (obj.Equals("单据") || obj.Equals("凭证") || obj.Equals("库存转储"))
                {
                    if (SAPCostOK == "N")
                    {
                        string Errmsg = "";
                        oRichTextBox.SelectionColor = Color.Red;
                        if (GetSAPCost == "Y")
                        {
                            Errmsg = ",获取成本失败!请检查SAP库存!";
                            oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt +
                                                    "   数据: " + odataRow[keyField] + " " + Descstring +
                                                    " ,获取成本失败!请检查SAP库存!" + Environment.NewLine);
                        }
                        else
                        {
                            Errmsg = ",设置获取SAP成本规则的单据未同步,请先同步!";
                            oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt + "   数据: " + odataRow[keyField] + " " + Descstring + " ,设置获取SAP成本规则的单据未同步,请先同步!" + Environment.NewLine);

                        }
                        Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, string.Empty, GrpBPLID, DateTime.Now.ToString("yyyy-MM-dd"),
                            ERPobject, string.Empty, importDocNum.Length >= 3 ? importDocNum.Substring(2) : "", odataRow[keyField].ToString(), DateTime.Now.ToString("yyyy-MM-dd"), SAPobject, Descstring + Errmsg);
                        if (Program.ErrOP.Equals("终止"))
                        {
                            return;
                        }
                        if (Program.ErrOP.Equals("提示"))
                        {
                            if (MessageBox.Show(importDocNum + "同步错误,是否继续?"+Environment.NewLine+Errmsg, "错误", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                return;
                            }
                        }
                            continue;
                    }
                    
                }

                int i = xmltitle.IndexOf("</AdmInfo>") + 10;
                string ImportXmlStr = xmltitle.Substring(0, i);

                if (GetSAPCost.Equals("Y") && (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据")))  //重新获取数据后取数
                {
                    ImportXmlStr += tmpDataSet.Tables["ODOC"].Rows[0]["xmlobj"] + "</BO></BOM>";
                }
                else
                {
                    ImportXmlStr += odataRow["xmlobj"] + "</BO></BOM>";
                }
               

                string DocEntry = "", SAPobj = "", fillstr = "";
                if (obj.Equals("单据") || obj.Equals("凭证")  || obj.Equals("库存转储"))
                {
                    ImportXmlStr = ImportXmlStr.Replace("</BO></BOM>", "");
                    DocEntry = odataRow["DocEntry"].ToString();
                    SAPobj = odataRow["SAPobj"].ToString().Trim();
                    fillstr = "DocEntry='" + DocEntry + "' and SAPobj='" + SAPobj + "' and ERPobj = '"+ERPobject+"'";
                    if (odataRow.Table.Columns.Contains("BPL_IDAssignedToInvoice"))
                    {

                        GrpBPLID = odataRow["BPL_IDAssignedToInvoice"].ToString().Trim();
                        if (!GrpBPLID.Equals(""))
                        {
                            fillstr = "DocEntry='" + DocEntry + "' and  ERPobj = '" + ERPobject + "' and SAPobj ='" + SAPobj + "' and BPL_IDAssignedToInvoice = '" + GrpBPLID + "'";
                        }
                        
                    }

                    string SubStr = string.Empty;
                    if (GetSAPCost.Equals("Y") && (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据")))
                    {
                        SubStr =
                            tmpDataSet.Tables["DOC1"].Select(fillstr)[0]["xmlobj"].ToString();
                    }
                    else
                    {
                        SubStr =
                            oDataSet.Tables[DiDataTable.Substring(1) + "1"].Select(fillstr)[0]["xmlobj"].ToString();
                    }

                    ImportXmlStr += SubStr;


                    string OSRN = string.Empty;
                    DataRow[] odtRow ;
                    if (oDataSet.Tables.Contains("OSRN")){
                        if (GetSAPCost.Equals("Y") && (Program.DocToSAP.Equals("所有单") || Program.DocToSAP.Equals("选择单据")))
                        {
                            odtRow = tmpDataSet.Tables["OSRN"].Select(fillstr);
                        }
                        else
                        {
                            odtRow = oDataSet.Tables["OSRN"].Select(fillstr);
                        }
                        if (odtRow.Count() > 0)
                        {
                            ImportXmlStr += odtRow[0]["xmlobj"].ToString();
                        }
                       
                    }
                    

                    ImportXmlStr += "</BO></BOM>";
                }
                if (obj.Equals("成本中心"))
                {
                    xmltitle = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>";
                    ImportXmlStr = xmltitle + odataRow["xmlobj"].ToString().Replace("<row>", string.Empty)
                                       .Replace("</row>", string.Empty);
                }

                if (obj.Equals("物料主数据"))
                {
                    string ItemCode = odataRow["ItemCode"].ToString();
                    string sqlCmd1 = "SELECT itemGroup FROM ToSAP_Temp_OITM WHERE  ItemCode = '" + ItemCode + "'";
                    DataTable itemGroupData = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null);
                    if (itemGroupData.Rows.Count > 0)
                    {
                        GetSAPsqlConn(shpCode); //初始化SAP SQL 连接
                        string itemGroup = itemGroupData.Rows[0][0].ToString();
                        sqlCmd1 = "SELECT ItmsGrpCod FROM OITB WHERE  ItmsGrpNam = '" + itemGroup + "'";
                        DataTable oItemTable = Common.GetDataTable(Program.SAPsqlConnStr.ConnectionString, sqlCmd1, null);
                        if (oItemTable.Rows.Count > 0)
                        {
                            string ItemGroupCode = oItemTable.Rows[0][0].ToString();
                            XmlDocument oXmlDocument1 = new XmlDocument();
                            oXmlDocument1.LoadXml(ImportXmlStr);
                            XmlNode root = oXmlDocument1.SelectSingleNode("/BOM/BO/Items/row");
                            XmlElement oXmlElement = oXmlDocument1.CreateElement("ItemsGroupCode");
                            oXmlElement.InnerText = ItemGroupCode;
                            root.AppendChild(oXmlElement);
                            // SAPobj.ItemsGroupCode = ItemGroupCode;
                            ImportXmlStr = oXmlDocument1.InnerXml;

                        }
                        else
                        {
                            oRichTextBox.SelectionColor = Color.Red;
                            oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt + "   数据: " + ItemCode + " " + Descstring + " SAP 系统中未找到物料组,请检查SAP物料组设置!!" + Environment.NewLine);
                            Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, string.Empty, GrpBPLID, DateTime.Now.ToString("yyyy-MM-dd"),
                                ERPobject, string.Empty, ItemCode, odataRow[keyField].ToString(), DateTime.Now.ToString("yyyy-MM-dd"), SAPobject, " SAP 系统中未找到物料组,请检查SAP物料组设置!!");
                            continue;
                        }
                    }
                    else
                    {
                        oRichTextBox.SelectionColor = Color.Red;
                        oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt + "   数据: " + ItemCode + " " + Descstring + " 物料分类未设置SAP对应物料组,请联系:Stray!" + Environment.NewLine);

                        Common.ToSAP_ImportErr(Program.FwSqlConnection, shpCode, string.Empty, GrpBPLID, DateTime.Now.ToString("yyyy-MM-dd"),
                            ERPobject, string.Empty, ItemCode, odataRow[keyField].ToString(), DateTime.Now.ToString("yyyy-MM-dd"), SAPobject, " 物料分类未设置SAP对应物料组,请联系:Stray!");
                        continue;
                    }
                }
                Dictionary<string, string> oDictionary = new Dictionary<string, string>();
                oDictionary.Add("importXML", ImportXmlStr);
                string objType1 = oDataSet.Tables[DiDataTable].Columns.IndexOf("objecttype") > 0 ? odataRow["objecttype"].ToString() : obj;
                string objtype = string.Empty;
                switch(objType1){
                    case "67":
                        objtype = "库存转储";
                        break;
                    case "30":
                        objtype = "凭证";
                        break;
                    case "28":
                        objtype = "凭单";
                        break;
                    default:
                        objtype = obj;
                        break;

                }
                oDictionary.Add("objType0", objtype);
                DataTable Valuedt = new DataTable();
                Valuedt.Columns.Add("DocType", typeof(String));
                Valuedt.Columns.Add("SAPDocType", typeof(String));
                Valuedt.Columns.Add("GrpBPLID", typeof(String));
                Valuedt.Columns.Add("absDocEntry", typeof(String));
                Valuedt.Columns.Add("DocNum", typeof(String));
                Valuedt.Columns.Add("DocDate", typeof(String));
                Valuedt.Columns.Add("shpCode", typeof(String));
                Valuedt.Columns.Add("ProgramVersion", typeof(String));
                string ProgramVersion = Application.ProductVersion;
                string DocNum = String.Empty;
               if (obj.Equals("单据") || obj.Equals("凭证") || obj.Equals("库存转储"))
                {
                    DataRow[] dataRows = oDataSet.Tables["DocList"].Select(fillstr);
                    foreach (DataRow odataRow1 in dataRows)
                    {
                        DataRow valueRow = Valuedt.NewRow();
                        valueRow["DocType"] = ERPobject;
                        valueRow["SAPDocType"] = SAPobject;
                        valueRow["GrpBPLID"] = GrpBPLID;
                        valueRow["absDocEntry"] =
                            odataRow1["absDocEntry"].ToString() + (oDataSet.Tables[DiDataTable].Columns.IndexOf("CardCodeIns") > 0
                                ? odataRow1["CardCodeIns"].ToString() : "");
                        valueRow["DocNum"] = odataRow1.Table.Columns.IndexOf("DocNum") > 0 ? odataRow1["DocNum"] : "";
                        valueRow["DocDate"] =odataRow.Table.Columns.IndexOf("DocDate1")>0 ? odataRow["DocDate1"].ToString(): odataRow["DocDate"].ToString();
                        valueRow["shpCode"] = shpCode;
                        valueRow["ProgramVersion"] = ProgramVersion;
                        Valuedt.Rows.Add(valueRow);
                        Valuedt.AcceptChanges();
                    }
                }
               else
               {
                   DataRow valueRow = Valuedt.NewRow();
                   valueRow["DocType"] = ERPobject;
                   valueRow["SAPDocType"] = SAPobject;
                   valueRow["GrpBPLID"] = GrpBPLID;
                   valueRow["absDocEntry"] = odataRow[keyField].ToString();
                   valueRow["DocNum"] = odataRow.Table.Columns.IndexOf("DocNum") > 0 ? odataRow["DocNum"] : "";
                   valueRow["DocDate"] = odataRow.Table.Columns.IndexOf("DocDate1") > 0 ? odataRow["DocDate1"].ToString() : DateTime.Now.ToString("yyyy-MM-dd");
                   valueRow["shpCode"] = shpCode;
                   valueRow["ProgramVersion"] = ProgramVersion;
                    Valuedt.Rows.Add(valueRow);
                   Valuedt.AcceptChanges();
                }

                string importxml = postString("importToSAP", oDictionary, Valuedt);
               
                JObject resultJObject =  soapPost(serviceurl, importxml,"N",shpCode);
                Common.GetFWSqlConn(null);
                string errCode = resultJObject["errCode"].ToString();
                string errMsg = resultJObject["errMsg"].ToString();
                if (errCode.Equals("0") || errCode.Equals("-10") && !(obj.Equals("单据") || obj.Equals("凭证") || obj.Equals("库存转储")) || errMsg.Contains("此条目已在下表中"))
                {
                    
                    if (oRichTextBox != null)
                    {
                        oRichTextBox.SelectionColor = Color.Green;
                        oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步成功: " + DispMesTxt + "   数据: " + odataRow[keyField]  + Environment.NewLine);
                    }
                }
                else
                {
                    if (oRichTextBox != null)
                    {
                        oRichTextBox.SelectionColor = Color.Red;
                        oRichTextBox.AppendText(Program.DispWhsShp + ":" + shpCode + "数据同步失败: " + DispMesTxt + "   数据: " + odataRow[keyField] + " " + errMsg + Environment.NewLine);
                    }
                        
                     if (obj.Equals("单据") || obj.Equals("凭证") || obj.Equals("库存转储"))
                    {
                        if (GetSAPCost.Equals("Y")){

                            DataRow[] dataRows = oDataSet.Tables["DocList"].Select(fillstr);
                            foreach (DataRow odataRow1 in dataRows)
                            {
                                DataRow[] Query_1 = oDataSet.Tables[DiDataTable].Select("DocNum = '" + odataRow1["DocNum"] +"' and ERPobj = '"+ERPobject+"'");
                                foreach (DataRow odataRow2 in Query_1)
                                {
                                    oDataSet.Tables[DiDataTable].Rows.Find(odataRow2["XH_PK"])["CanImport"] = "N";
                                }  
                            }
                            oDataSet.Tables[DiDataTable].AcceptChanges();
                        }
                        if (Program.ErrOP.Equals("终止"))
                        {
                            return;
                        }
                        if (Program.ErrOP.Equals("提示"))
                        {
                            if (MessageBox.Show( importDocNum + "同步错误,是否继续?"+Environment.NewLine+errMsg, "错误", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                return;
                            }
                        }
                    }
                }

            }

        }

        public static bool WebLogSAP(string shpCode,string oUser,RichTextBox oRichTextBox)
        {
            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            string serUrl = ((JObject)ToSAPJson["FW"])["FWDBserver"].ToString().Trim();
            if (serUrl.Contains(","))
            {
                serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
            }
          
          string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";
           //string serviceurl = "http://127.0.0.1:2899/WebToSAP.asmx";
            GetSAPsqlConn(shpCode);
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            oDictionary.Add("oUserCode", oUser);
            oDictionary.Add("shpCode", shpCode);
            oDictionary.Add("ToSAPini", connStr);

            string importxml = postString("LogToSAP", oDictionary, null);  //Logon To SAP
            if (oRichTextBox != null)
            {
                oRichTextBox.SelectionColor = Color.Black;
                oRichTextBox.AppendText("正在连接SAP!:  " + DateTime.Now.ToString() + Environment.NewLine);
            }
            Application.DoEvents();
            try
            {
                JObject LogTosapResult = soapPost(serviceurl, importxml,"Y",shpCode);
                string VersionWebCur = LogTosapResult["WebVersion"].ToString();
                string VersionWebSet = "5.2.0.6";
                if (!VersionWebSet.Equals(VersionWebCur))
                {
                    if (oRichTextBox != null)
                    {
                        oRichTextBox.SelectionColor = Color.Red;
                        oRichTextBox.AppendText("连接失败:WebService 版本不正确，请升级WebService！当前WebSerivce 版本: "+ VersionWebCur +"   需求版本："+VersionWebSet+"  "+ DateTime.Now.ToString() +Environment.NewLine);
                        weblogOutSAP(shpCode,oUser);
                    }
                    return false;
                }
                    if (LogTosapResult["errCode"].ToString().Equals("0"))
                    {
                        if (oRichTextBox != null)
                        {
                            oRichTextBox.AppendText("连接完成:  " +  DateTime.Now.ToString() + Environment.NewLine);
                        }
   
                        return true;
                    }
                    else
                    {
                        if (oRichTextBox != null)
                        {
                            oRichTextBox.SelectionColor = Color.Red;
                            oRichTextBox.AppendText("连接失败:  " + LogTosapResult["errMsg"].ToString() + " " + DateTime.Now.ToString() +
                                                    Environment.NewLine);
                        }
                        return false;
                    }
               
            }
            catch (Exception exception)
            {
                if (oRichTextBox != null)
                {
                    oRichTextBox.SelectionColor = Color.Red;
                    oRichTextBox.AppendText("连接失败:  " + DateTime.Now.ToString() + exception.ToString() + Environment.NewLine);
                }
                return false;

            }

            return false;
        }

        public  static void weblogOutSAP(string shpCode,string oUser)
        {
            Dictionary<string,string> oDictionary = new Dictionary<string, string>();
            oDictionary.Add("oUserCode", oUser);
            oDictionary.Add("shpCode", shpCode==null?"":shpCode);
            string importxml = postString("DisConnectSAP", oDictionary, null);
            soapPost(Program.serviceurl, importxml,"N",shpCode);
            string cookieFile = Application.StartupPath + "\\CookieContainer"+shpCode+".bin";  //cookie保存位置
            FileInfo oFileInfo = new FileInfo(cookieFile);
            if (oFileInfo.Exists)
            {
                oFileInfo.Delete();
            }
            GC.Collect();
           
        }

        public static void CreateSHPTable(string oUser)
        {
            Common.GetFWSqlConn(null);
            string sqlCmd = "IF EXISTS(SELECT * FROM sys.tables WHERE name=\'ToSAP_OSHP_"+oUser+"\')\r\nBEGIN\r\nDROP TABLE ToSAP_OSHP_"+oUser+"\r\nEND\r\n\r\nCREATE TABLE ToSAP_OSHP_"+oUser+ "  \r\n(\r\nshpEntry INT,\r\nshpCode VARCHAR(50),\r\nshpName VARCHAR(100),\r\nBPLID varchar(10),\r\nSAPCompany VARCHAR(100)\r\n);CREATE INDEX shpEntryIndex ON ToSAP_OSHP_"+oUser+"(shpEntry ASC)";
            Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);

        }

        public static string GetBPLID( string shpCode,string shpEntry)
        {
            string BPLID = String.Empty;
            if (Program.InterFaceSet == "按分支")
            {
                string sqlCmd1 = "SELECT dbo.ToSAP_GetCB("+ shpEntry + ")";
                    //"SELECT T1.BranchName FROM ToSAP_BranchTitle T1 LEFT JOIN ToSAP_BranchDetail T2 ON T2.BranchCode = T1.BranchCode WHERE T2.WhsCode = '" +shpCode + "'";
                //if (Program.DispWhsShp.Equals("店铺") && Program.Shop.Equals("是"))
                //{
                //    sqlCmd1 =
                //        "SELECT T1.shpName BranchName FROM OSHP T1 WHERE T1.shpCode='" +shpCode + "'";
                //}

                //if ((!Program.DispWhsShp.Equals("店铺")) && Program.Shop.Equals("是"))
                //{
                //    sqlCmd1 =
                //        "SELECT Value2 BranchName  FROM ToSAP_Appinit T1 WHERE ParaCode='CorpSub'";
                //}
                Common.GetSAPsqlConn(shpCode);
               
                try
                {
                    var BranchName = Common
                        .GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null).Rows[0][0].ToString();
                    sqlCmd1 = "SELECT BPLId FROM OBPL WHERE BPLName = '" + BranchName + "' AND Disabled = 'N'";
                    BPLID = Common
                        .GetDataTable(Program.SAPsqlConnStr.ConnectionString, sqlCmd1, null).Rows[0][0].ToString();
                    return BPLID;
                }
                catch (Exception exception)
                {
                    return BPLID;

                }
            }
            return BPLID;
        }

        public static void upDateItm(string GroupCode,GridView gridView1,RichTextBox DispMessage)
        {
            string MessStr = "请确认更新物料主数据!";
            if (MessageBox.Show(MessStr, "确认", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            string serUrl = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            //serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
            //string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                string shpChs = gridView1.GetRowCellValue(i, "Chs").ToString();
                if (!shpChs.Equals("Y"))
                {
                    continue;
                }
                string shpCode = gridView1.GetRowCellValue(i, "WhsCode").ToString();
                string sqlCmd = "SELECT * FROM dbo.fn_GetUpdateItm("+GroupCode+") ";
              
                DataTable oDataTable = Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                DispMessage.Visible = true;

                Dictionary<string, string> oDictionary = new Dictionary<string, string>();
                oDictionary.Add("oUserCode", Program.oUSER);
                oDictionary.Add("shpCode", shpCode);
                oDictionary.Add("ToSAPini", connStr);

                string importxml = Common.postString("LogToSAP", oDictionary, null);
                DispMessage.AppendText("正在连接SAP!:  " + DateTime.Now.ToString() + Environment.NewLine);

                try
                {
                    JObject LogTosapResult = Common.soapPost(Program.serviceurl, importxml, "Y", shpCode);
                    if (LogTosapResult["errCode"].ToString().Equals("0"))
                    {
                        DispMessage.AppendText("连接完成:  " +  DateTime.Now.ToString() + Environment.NewLine);
                    }
                    else
                    {
                        DispMessage.AppendText("连接失败:  " + LogTosapResult["errMsg"].ToString() + " " + DateTime.Now.ToString() + Environment.NewLine);
                        return;
                    }

                }
                catch (Exception exception)
                {
                    DispMessage.AppendText("连接失败:  " + DateTime.Now.ToString() + exception.ToString() + Environment.NewLine);
                }

                foreach (DataRow oDataRow in oDataTable.Rows)
                {
                    string ItemCode = oDataRow["ItemCode"].ToString();
                    string ItemName = oDataRow["ItemName"].ToString();
                    string itemGroup = oDataRow["itemGroup"].ToString();
                    DispMessage.AppendText("正在更新 店铺："+shpCode+"  物料：" + ItemCode + " " + DateTime.Now.ToString() + Environment.NewLine);
                    //WebToSAP.DeletItem(ItemCode);
                    Dictionary<string, string> iDictionary = new Dictionary<string, string>();
                    iDictionary.Add("ItemCode", ItemCode);
                    iDictionary.Add("ItemName", ItemName);
                    iDictionary.Add("itemGroup", itemGroup);
                    string importxml1 = Common.postString("UpdateItem", iDictionary, null);
                    JObject deleteResult = Common.soapPost(Program.serviceurl, importxml1, "N", shpCode);
                    DispMessage.AppendText(deleteResult.ToString() + " " + DateTime.Now.ToString() + Environment.NewLine);
                    DispMessage.ScrollToCaret();

                }
                DispMessage.AppendText("更新完成" + " " + DateTime.Now.ToString() + Environment.NewLine);
                DispMessage.ScrollToCaret();
                Common.weblogOutSAP(shpCode, Program.oUSER);
            }
        }
    }
}

﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.UserSkins;

namespace ToSAP
{
    internal static class Program
    {
        public static SqlConnection FwSqlConnection = new SqlConnection();
        //   public static Company SAPCompany = new SAPbobsCOM.Company();
        public static SqlConnectionStringBuilder FWSqlConnStr = new SqlConnectionStringBuilder();
        public static SqlConnectionStringBuilder SAPsqlConnStr = new SqlConnectionStringBuilder();
        public static string oUSER = string.Empty;
        public static string InterFaceSet = string.Empty;
        public static string CorpSet = string.Empty;
        public static string shpCode = string.Empty;
        public static string DocBranch = string.Empty;
        public static string CronValue = string.Empty;
        public static int PlanToSAPsetRow = 0;
        public static int PlanToSAPshow = 0;
        public static string showOWHS = string.Empty;
        public static string DocToSAP = string.Empty;
        public static string serviceurl = string.Empty;
        public static string Shop = string.Empty;
        public static string DispWhsShp = string.Empty;
        public static string oLDMat = string.Empty;
        public static string ErrOP = String.Empty;
        public static Thread thread ;
        public static int threadDie = 0;
        public static int GroupC3Select = 0;
        public static  string checkList = String.Empty;
        public static string pstDate = String.Empty, penDate = string.Empty;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] server)
        {
            
            if (server.Length > 0)
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {new PlanToSAPser()};
                ServiceBase.Run(ServicesToRun);
            }
            else
            {
                // We are running as administrator
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Logon());
            }
            
        }
        }
}

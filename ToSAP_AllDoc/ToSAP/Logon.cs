﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using AutoUpdate;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using ToSAP.Set;
namespace ToSAP
{
    public partial class Logon : XtraForm
    {
        public Logon()
        {
           InitializeComponent();
        }

        private void XtraForm1_Load(object sender, EventArgs e)
        {
            //int CurrProcessID = Process.GetCurrentProcess().Id;
            //Process[] oProcess = Process.GetProcessesByName("ToSAP");
            //for (int i = 0; i < oProcess.Length; i++)
            //{
            //    int ProcessID = oProcess[i].Id;
            //    if (!ProcessID.Equals(CurrProcessID))
            //    {
            //        MessageBox.Show(this, "已有程序运行,程序不允许同时运行多个!");
            //        Process.GetCurrentProcess().Kill();
            //        GC.Collect();
            //        return;
            //    }
            //}
            Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
            // UpdateProgram();
            Common.Common.GetFWSqlConn(this);
            int Reslut = Common.Common.AlterDataBase();
            if (Reslut.Equals(1))
            {
                MessageBox.Show("请先升级数据库,然后运行程序!");
                Process.GetCurrentProcess().Kill();
            }
        }

        private void UpdateProgram()
        {
            AppUpdater oAppUpdater = new AppUpdater();
            XmlFiles updaterXmlFiles = null;
            int Err = -1;

            string locXML = Application.StartupPath + "\\AutoUpdaterList.xml";
            try
            {

                updaterXmlFiles = new XmlFiles(locXML);
            }
            catch
            {
                Err++;
                MessageBox.Show("更新配置错误,请检查更新配置!");
                return;
            }

                oAppUpdater.UpdaterUrl = "http://xzl-sap.top:61567/AutoUpdaterList.xml";  // updaterXmlFiles.GetNodeValue("//Url") + "/AutoUpdaterList.xml";
            string tempUpdatePath = "";
            try
            {
                tempUpdatePath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + updaterXmlFiles.FindNode("//Application").Attributes["applicationId"].Value + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                oAppUpdater.DownAutoUpdateFile(tempUpdatePath);
            }
            catch
            {
                Err++;
                MessageBox.Show("更新配置错误,请检查更新配置!");
                return;

            }
            string serXML = tempUpdatePath + "\\AutoUpdaterList.xml"; ;
            if (Err > -1)
            {
                MessageBox.Show("更新配置错误,请检查更新配置!");
                return;
            }
            Hashtable htUpdateFile = new Hashtable();
            int i = oAppUpdater.CheckForUpdate(serXML, locXML, out htUpdateFile);
            if (i > 0)
            {
                string appPath = Application.StartupPath + "\\AutoUpdate.exe";
                Process.Start(appPath);
            }
        }
    private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LogonTosystem();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var oMSet = new mSet();
            oMSet.ShowDialog(this);
        }

        private void oPwd_Enter(object sender, EventArgs e)
        {
            LogonTosystem();
        }

        private void LogonTosystem()
        {
            
            if (Program.FwSqlConnection.State != ConnectionState.Open)
            {
                Common.Common.GetFWSqlConn(null);
            }

            string SQLstr = String.Empty;
            string Computer = Environment.MachineName;
            IPHostEntry hostEntry = Dns.GetHostEntry(Computer);
            string IP = String.Empty;
            try
            {
                for (int i = 0; i < hostEntry.AddressList.Length; i++)
                {
                    if (hostEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                        IP = hostEntry.AddressList[i].ToString();
                        break;

                    }
                }

                SQLstr = "select usercode,computer,ip from ToSAP_UserLogin where UserCode = '" + oUser.Text.Trim() + "'";
                DataTable oDs = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, SQLstr, null);
                if (oDs.Rows.Count > 0 && checkEdit1.CheckState != CheckState.Checked)
                {
                    Computer = oDs.Rows[0]["computer"].ToString();
                    IP = oDs.Rows[0]["ip"].ToString();
                    MessageBox.Show("用户：" + oUser.Text.Trim() + " 已登陆，不允许同一用户多次登陆！\n登陆计算机：" + Computer + "\n IP:" +
                                    IP);
                    return;
                }

                if (oDs.Rows.Count > 0 && checkEdit1.CheckState == CheckState.Checked)
                {
                    string LogComputer = oDs.Rows[0]["computer"].ToString();
                    string LogIP = oDs.Rows[0]["ip"].ToString();
                    if (!(Computer.Equals(LogComputer) && LogIP.Equals(IP)))
                    {
                        MessageBox.Show("强制登陆必须在同一台电脑上登陆！\n原登陆登陆计算机：" + LogComputer + "\n IP:" +
                                        LogIP);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                
            }

            SQLstr = "SELECT UserCode,UserName,Password FROM OUSR WHERE UserCode ='" + oUser.Text.Trim() + "'";
            Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, SQLstr, null);

            SQLstr = "SELECT UserCode,UserName FROM OUSR WHERE UserCode ='" + oUser.Text.Trim() +
                         "'  AND ISNULL(Password,'') ='" + oPwd.Text.Trim() + "'";
            if (Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, SQLstr, null).Rows.Count > 0)
            {
                Program.oUSER = oUser.Text.Trim();
                var oMain = new Main();
                Visible = false;
                Hide();
                SQLstr = "delete ToSAP_UserLogin where UserCode = '"+oUser.Text.Trim()+"'";
                try
                {
                    Common.Common.ExecSqlcmd(Program.FwSqlConnection, SQLstr);
                }
                catch (Exception e)
                {
                   
                }
                SQLstr = "INSERT ToSAP_UserLogin ( UserCode ,Computer ,IP ) VALUES ( '"+oUser.Text.Trim()+"' , '"+Computer+"' ,  '"+IP+"')";
                try
                {
                    Common.Common.ExecSqlcmd(Program.FwSqlConnection, SQLstr);
                }
                catch (Exception e)
                {

                }
                oMain.ShowDialog(this);
            }
            else
            {
                MessageBox.Show("登陆失败,请检查用户名和密码!\n  用户名和密码与业务系统一致!");
            }
        }

        private void oPwd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                LogonTosystem();
            }
        }

        private void Logon_Activated(object sender, EventArgs e)
        {
          oUser.Focus();
        }
        
    }
}

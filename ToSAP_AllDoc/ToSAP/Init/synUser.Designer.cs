﻿namespace ToSAP.Init
{
    partial class synUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.UserCode = new DevExpress.XtraEditors.TextEdit();
            this.UserName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.SAPPwd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.SAPUser = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.saveOK = new DevExpress.XtraEditors.SimpleButton();
            this.CancelOK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.UserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPPwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPUser.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "当前用户编码:";
            // 
            // UserCode
            // 
            this.UserCode.Enabled = false;
            this.UserCode.Location = new System.Drawing.Point(96, 7);
            this.UserCode.Name = "UserCode";
            this.UserCode.Size = new System.Drawing.Size(100, 20);
            this.UserCode.TabIndex = 1;
            // 
            // UserName
            // 
            this.UserName.Enabled = false;
            this.UserName.Location = new System.Drawing.Point(304, 7);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(100, 20);
            this.UserName.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(221, 10);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "当前用户名:";
            // 
            // SAPPwd
            // 
            this.SAPPwd.Location = new System.Drawing.Point(304, 48);
            this.SAPPwd.Name = "SAPPwd";
            this.SAPPwd.Properties.PasswordChar = '*';
            this.SAPPwd.Size = new System.Drawing.Size(100, 20);
            this.SAPPwd.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(221, 51);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 14);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "SAP 密码:";
            // 
            // SAPUser
            // 
            this.SAPUser.Location = new System.Drawing.Point(96, 48);
            this.SAPUser.Name = "SAPUser";
            this.SAPUser.Size = new System.Drawing.Size(100, 20);
            this.SAPUser.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(13, 51);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(50, 14);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "SAP帐号:";
            // 
            // saveOK
            // 
            this.saveOK.Location = new System.Drawing.Point(51, 108);
            this.saveOK.Name = "saveOK";
            this.saveOK.Size = new System.Drawing.Size(75, 23);
            this.saveOK.TabIndex = 8;
            this.saveOK.Text = "保存";
            this.saveOK.Click += new System.EventHandler(this.saveOK_Click);
            // 
            // CancelOK
            // 
            this.CancelOK.Location = new System.Drawing.Point(173, 108);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(75, 23);
            this.CancelOK.TabIndex = 9;
            this.CancelOK.Text = "关闭";
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(304, 108);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(100, 23);
            this.simpleButton1.TabIndex = 10;
            this.simpleButton1.Text = "测试SAP连接";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // synUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 176);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.CancelOK);
            this.Controls.Add(this.saveOK);
            this.Controls.Add(this.SAPPwd);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.SAPUser);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.UserName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.UserCode);
            this.Controls.Add(this.labelControl1);
            this.Name = "synUser";
            this.Text = "同步帐号设置";
            this.Load += new System.EventHandler(this.synUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPPwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPUser.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit UserCode;
        private DevExpress.XtraEditors.TextEdit UserName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit SAPPwd;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit SAPUser;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton saveOK;
        private DevExpress.XtraEditors.SimpleButton CancelOK;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
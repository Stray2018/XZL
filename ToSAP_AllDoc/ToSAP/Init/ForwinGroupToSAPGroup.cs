﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace ToSAP.Init
{
    public partial class ForwinGroupToSAPGroup : DevExpress.XtraEditors.XtraForm
    {
        public ForwinGroupToSAPGroup()
        {
            InitializeComponent();
        }

        private void ForwinGroupToSAPGroup_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = "select GroupName,GroupCode,InvType from [ToSAP_InvType] " +
                            " union all " +
                            " select GroupName, GroupCode,'' from OITG where GroupCode not in (select ToSAP_InvType.groupCode from ToSAP_InvType) " +
                            " order by groupCode";
            DataTable oDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            gridControl1.DataSource = oDt;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            SqlTransaction oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                Common.Common.TruncateTable(oSqlTransaction, Program.FwSqlConnection, TableName: "ToSAP_InvType");
                DataTable oDataTable = (DataTable)gridControl1.DataSource;
                if (oDataTable.Rows[oDataTable.Rows.Count - 1]["GroupCode"].ToString().Trim() == string.Empty)
                {
                    oDataTable.Rows[oDataTable.Rows.Count - 1].Delete();
                }
                bool result = Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_InvType");
                if (result)
                {
                    MessageBox.Show("数据保存成功!");
                    oSqlTransaction.Commit();
                }

                oSqlTransaction.Dispose();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Rollback();
                oSqlTransaction.Dispose();
                MessageBox.Show(exception.Message);
            }
        }
    }
}
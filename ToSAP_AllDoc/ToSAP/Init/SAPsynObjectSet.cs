﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Init
{
    public partial class SAPsynObjectSet : XtraForm
    {
        public SAPsynObjectSet()
        {
            InitializeComponent();
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Common.Common.InsertDataToDataSource(gridView1, gridControl1);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridView1.DeleteSelectedRows();
        }

        private void gridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            var rowid = gridView1.FocusedRowHandle + 1;
            var ss = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["SAPobjCode"]);
            if (e.FocusedColumn.FieldName == "SAPobjName" && gridView1.RowCount == rowid && !ss.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void SAPsynObjectSet_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);

            SAPDIinterface.ColumnEdit = repositoryItemLookUpEdit1;
            repositoryItemLookUpEdit1.DisplayMember = "DIDescr";
            repositoryItemLookUpEdit1.ValueMember = "DIInterface";
            string sqlCmd = "SELECT * FROM [ToSAP_DIinterface] ORDER BY DIDescr";
            var DiSource = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);

            DiSource.Columns["DIInterface"].Caption = "DI接口类型";
            DiSource.Columns["DIDescr"].Caption = "DI接口类型描述";
            DiSource.Columns["DIobjSer"].Caption = "对象/服务";
            DiSource.Columns["DIobjType"].Caption = "DI对象描述";
            DiSource.Columns["DTable"].Caption = "DI接口主表";
            DiSource.Columns["DI"].Caption = "DI对象";
            DiSource.Columns["DI_Lines"].Caption = "DI行对象";

            repositoryItemLookUpEdit1.PopupWidth = Convert.ToInt32(Width * 0.8);
            repositoryItemLookUpEdit1.DataSource = DiSource;

            sqlCmd = "SELECT * FROM [ToSAP_DIobject] ORDER BY SAPobjName";
            DataTable odDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataRow oDataRow = odDataTable.NewRow();
            odDataTable.Rows.Add(oDataRow);
            gridControl1.DataSource = odDataTable;
            gridView1.IndicatorWidth = 40;
        }

        private void gridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            var oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                Common.Common.TruncateTable(oSqlTransaction, Program.FwSqlConnection, "ToSAP_DIobject");
                DataTable oDataTable = (DataTable) gridControl1.DataSource;
                if (oDataTable.Rows[oDataTable.Rows.Count - 1]["SAPobjCode"].ToString().Trim() == string.Empty)
                {
                    oDataTable.Rows[oDataTable.Rows.Count - 1].Delete();
                }
                bool result = Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_DIobject");
                if (result)
                {
                    MessageBox.Show("数据保存成功!");
                    oSqlTransaction.Commit();
                }

                oSqlTransaction.Dispose();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Dispose();
                MessageBox.Show(exception.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json.Linq;

namespace ToSAP.Init
{
    public partial class synUser : XtraForm
    {
        public synUser()
        {
            InitializeComponent();
        }

        private void synUser_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd =
                "SELECT UserCode,UserName ,isnull(T2.SAPuser,'') SAPUser,isnull(t2.SAPPwd,'') SAPPwd FROM OUSR T1 LEFT JOIN [ToSAP_SynUser] T2 ON t1.UserCode=t2.FWuser WHERE T1.UserCode = '" +
                Program.oUSER + "'";
            System.Data.DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            UserCode.Text = Program.oUSER;
            UserName.Text = oDataTable.Rows[0]["UserName"].ToString();
            SAPUser.Text = oDataTable.Rows[0]["SAPUser"].ToString();
            var SAPPwds = oDataTable.Rows[0]["SAPPwd"].ToString();
            SAPPwd.Text = SAPPwds.Equals(string.Empty) ? string.Empty : Common.Common.Decrypt(SAPPwds);
            oDataTable.Dispose();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = "SELECT * FROM [ToSAP_SynUser] WHERE FWUser='" + Program.oUSER + "'";
            System.Data.DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (oDataTable.Rows.Count > 0)
            {
                sqlCmd = "DELETE [ToSAP_SynUser] WHERE FWUser='" + Program.oUSER + "'";
                Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
            }

            oDataTable.Clear();
            DataRow oDataRow = oDataTable.NewRow();
            oDataRow["FWuser"] = UserCode.Text.Trim();
            oDataRow["SAPUser"] = SAPUser.Text.Trim();
            oDataRow["SAPPwd"] = Common.Common.Encrypt(SAPPwd.Text.Trim());
            oDataTable.Rows.Add(oDataRow);
            Common.Common.InsertDataToTable(null, Program.FwSqlConnection, oDataTable, "ToSAP_SynUser");
            oDataTable.Dispose();
            MessageBox.Show("保存成功!");
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConTest_Click(object sender, EventArgs e)
        {
           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            StreamReader oStReader = new StreamReader(Application.StartupPath + "/ToSAP.ini");
            string connStr = oStReader.ReadToEnd();
            oStReader.Close();
            oStReader.Dispose();
            JObject ToSAPJson = JObject.Parse(connStr);
            string serUrl = ((JObject)ToSAPJson["SAP"])["SAPDBserver"].ToString().Trim();
            if (serUrl.Contains(","))
            {
                serUrl = serUrl.Substring(0, serUrl.IndexOf(","));
            }

            ((JObject) ToSAPJson["SAP"])["SAPUSER"] = SAPUser.Text.Trim();
            ((JObject) ToSAPJson["SAP"])["SAPPWD"] = Common.Common.Encrypt(SAPPwd.Text.Trim());
            string serviceurl = "http://" + serUrl + ":2899/WebToSAP.asmx";
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            oDictionary.Add("ToSAPini", ToSAPJson.ToString());

            string importxml = Common.Common.postString("SAPConnectTest", oDictionary,null);  //Logon To SAP
            JObject LogTosapResult = Common.Common.soapPost(serviceurl, importxml, "Y", null);
            MessageBox.Show(LogTosapResult["errMsg"].ToString());
        }
    }
}

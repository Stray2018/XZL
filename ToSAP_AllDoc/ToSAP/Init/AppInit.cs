﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ToSAP.Init
{
    public partial class AppInit : XtraForm
    {
        public AppInit()
        {
            InitializeComponent();
        }

        private void listBoxControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string AppSqlStr = "SELECT * FROM ToSAP_Appinit WHERE 1 = 2";
            DataTable AppinitData = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, AppSqlStr,null);
            DataRow oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "InterFaceMode";
            oDataRow["ParaName"] = "接口模式";
            oDataRow["Value1"] = InterFaceMode.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);

            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "Corp";
            oDataRow["ParaName"] = "集团管理";
            oDataRow["Value1"] = Corp.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);

            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "Shop";
            oDataRow["ParaName"] = "门店单独管理";
            oDataRow["Value1"] = Shop.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);

            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "SqlVersion";
            oDataRow["ParaName"] = "SQL版本";
            oDataRow["Value1"] = sqlversion.Text;
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);


            if (SAPFIDB.Visible)
            {
                oDataRow = AppinitData.NewRow();
                oDataRow["ParaCODE"] = "SAPFIDB";
                oDataRow["ParaName"] = "SAP 财务数据库";
                oDataRow["Value1"] = SAPFIDB.Text.Trim();
                oDataRow["Value2"] = string.Empty;
                AppinitData.Rows.Add(oDataRow);
            }

            if (CorpSub.Visible)
            {
                oDataRow = AppinitData.NewRow();
                oDataRow["ParaCODE"] = "CorpSub";
                oDataRow["ParaName"] = "集团分支";
                oDataRow["Value1"] = CorpSub.Text.Trim();
                oDataRow["Value2"] = CorpSubName.Text.Trim();
                AppinitData.Rows.Add(oDataRow);
            }

            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "showOWHS";
            oDataRow["ParaName"] = "同步界面显示店铺仓库";
            oDataRow["Value1"] = showOWHS.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);
            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "DocToSAP";
            oDataRow["ParaName"] = "单据单统一同步";
            oDataRow["Value1"] = DocToSAP.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);

            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "oLDMat";
            oDataRow["ParaName"] = "旧料旧饰成本模式";
            oDataRow["Value1"] = oLDMat.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);

            oDataRow = AppinitData.NewRow();
            oDataRow["ParaCODE"] = "ErrOP";
            oDataRow["ParaName"] = "单据同步错误操作方式";
            oDataRow["Value1"] = ErrOP.Text.Trim();
            oDataRow["Value2"] = string.Empty;
            AppinitData.Rows.Add(oDataRow);

            Common.Common.TruncateTable(null, Program.FwSqlConnection, "ToSAP_Appinit");
            Common.Common.InsertDataToTable(null, Program.FwSqlConnection, AppinitData, "ToSAP_Appinit");
            Program.InterFaceSet = InterFaceMode.Text;
            Program.CorpSet = Corp.Text;
            Program.shpCode = Shop.Text;
            Program.showOWHS = showOWHS.Text;
            Program.DocToSAP = DocToSAP.Text;
            if (Common.Common.GetInterFaceMode(Program.FwSqlConnection) != "按分支")
            {
               Main.BranchSet.Caption = "公司门店设置";
            }
            MessageBox.Show("保存成功!");
        }

        private void InterFaceMode_SelectedValueChanged(object sender, EventArgs e)
        {
            if (InterFaceMode.SelectedIndex != 1)
            {
                SAPFIDB.Visible = false;
                SAPDB.Visible = false;
                CorpLable.Text = "集团数据库:";
                Corp.SelectedIndex = 1;
                Corp.Enabled = false;
                CorpSub.Visible = false;
                CorpSubName.Visible = false;
                Shop.SelectedIndex = 1;
                Shop.Enabled = false;
            }
            else
            {
                SAPFIDB.Visible = true;
                SAPDB.Visible = true;
                CorpLable.Text = "集团分支:";
                CorpLable.Visible = true;
                Corp.Enabled = true;
                CorpSub.Visible = true;
                CorpSubName.Visible = true;
                Corp.SelectedIndex = 0;
                Shop.SelectedIndex = 1;
                Shop.Enabled = true;
            }
        }

        private void Corp_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Corp.SelectedIndex != 0)
            {
                CorpLable.Visible = false;
                CorpSub.Visible = false;
                CorpSubName.Visible = false;
            }
            else
            {
                CorpLable.Visible = true;
                CorpSub.Visible = true;
                CorpSubName.Visible = true;
            }
        }

        private void AppInit_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = "SELECT * FROM ToSAP_Appinit";
            try
            {
                DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                if (oDataTable.Rows.Count > 0)
                {
                    InterFaceMode.Text = oDataTable.Select("ParaCode = 'InterFaceMode'")[0]["Value1"].ToString();
                    showOWHS.Text=oDataTable.Select("ParaCode = 'showOWHS'")[0]["Value1"].ToString();
                    if (InterFaceMode.Text == "按公司")
                    {
                        SAPDB.Visible = false;
                        SAPFIDB.Visible = false;
                        CorpLable.Text = "集团数据库:";
                    }
                    else
                    {
                        SAPDB.Visible = true;
                        SAPFIDB.Visible = true;
                        SAPFIDB.Text = oDataTable.Select("ParaCode = 'SAPFIDB'")[0]["Value1"].ToString();
                        CorpLable.Text = "集团分支:";
                    }

                    Corp.Text = oDataTable.Select("ParaCode = 'Corp'")[0]["Value1"].ToString();
                    if (Corp.Text == "是")
                    {
                        CorpLable.Visible = true;
                        CorpSub.Visible = true;
                        CorpSubName.Visible = true;
                        CorpSub.Text = oDataTable.Select("ParaCode = 'CorpSub'")[0]["Value1"].ToString();
                        CorpSubName.Text = oDataTable.Select("ParaCode = 'CorpSub'")[0]["Value2"].ToString();
                    }
                    else
                    {
                        CorpLable.Visible = false;
                        CorpSub.Visible = false;
                        CorpSubName.Visible = false;
                    }

                    Shop.Text = oDataTable.Select("ParaCode = 'Shop'")[0]["Value1"].ToString();
                    DocToSAP.Text = oDataTable.Select("ParaCode = 'DocToSAP'")[0]["Value1"].ToString();
                    oLDMat.Text = oDataTable.Select("ParaCode = 'oLDMat'")[0]["Value1"].ToString();
                    ErrOP.Text = "继续";
                    try
                    {
                        ErrOP.Text =  oDataTable.Select("ParaCode = 'ErrOP'")[0]["Value1"].ToString();
                    }
                    catch (Exception exception)
                    {
                       
                    }

                    try
                    {
                        sqlversion.Text = oDataTable.Select("ParaCode = 'SqlVersion'")[0]["Value1"].ToString();
                    }
                    catch (Exception exception)
                    {
                        
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);}
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string shpCode = SAPFIDB.Text;
                
          Common.Common.SAPFieldAdd(shpCode, Program.oUSER);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
           
                using (System.Data.SQLite.SQLiteConnection oSqLiteConn = new SQLiteConnection())
                {
                    try
                    {
                        oSqLiteConn.ConnectionString = "data source=" + AppDomain.CurrentDomain.BaseDirectory + "Template.db";
                        oSqLiteConn.Open();
                        string sqlCmd0 =
                            "SELECT name FROM sys.tables WHERE name  IN('ToSAP_Appinit','ToSAP_DIinterface','ToSAP_Diobject','ToSAP_DIProp','ToSAP_ERPDataType','ToSAP_FWFldDesc','ToSAP_Fwobject','ToSAP_Role_Mas','ToSAP_Role_Sub') ORDER BY name ";
                        DataTable oSDts =
                            Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd0, null);
                        foreach (DataRow osDtsRow in oSDts.Rows)
                        {
                            string TableName = osDtsRow[0].ToString();
                        DataTable ColunmList = oSqLiteConn.GetSchema("columns", new string[] { null, null, TableName });
                            string sqlCmdsql = "";
                        string sqlCmdssqlite = "";
                            foreach (DataRow oColunmListRow in ColunmList.Rows)
                            {
                                string columnName = oColunmListRow["ColUMN_NAME"].ToString();
                                if (!columnName.Equals("ID"))
                                {
                                    if (columnName.Equals("DocEntry") || columnName.Equals("IsSum") || columnName.Equals("ToOrder"))
                                    {
                                        sqlCmdsql += ",Convert(Varchar(20)," + oColunmListRow["ColUMN_NAME"].ToString()+ ") as "+oColunmListRow["ColUMN_NAME"];
                                        sqlCmdssqlite += ',' + oColunmListRow["ColUMN_NAME"].ToString();
                                }
                                    else
                                    {
                                        sqlCmdsql +=  ',' + oColunmListRow["ColUMN_NAME"].ToString(); 
                                        sqlCmdssqlite += ',' + oColunmListRow["ColUMN_NAME"].ToString();
                                   
                                }
                                    
                                }
                                else
                                {
                                    sqlCmdssqlite += ',' + oColunmListRow["ColUMN_NAME"].ToString();
                            }


                            }

                            string sqlCmd1 = "select " + sqlCmdsql.Substring(1) + " from " + TableName;
                            string sqlCmd2 = "select " + sqlCmdssqlite.Substring(1) + " from " + TableName;
                        DataTable oDt =
                                Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null);
                            
                            oDt.AcceptChanges();
                            oDt.TableName = TableName;
                            SQLiteCommand oSqLiteCommand = new SQLiteCommand(oSqLiteConn);
                            oSqLiteCommand.CommandText = "Delete From " + TableName;
                            oSqLiteCommand.ExecuteNonQuery();
                            SQLiteDataAdapter oSqLiteDataAdapter = new SQLiteDataAdapter(sqlCmd2,oSqLiteConn);
                        DataTable ds = new DataTable();
                            oSqLiteDataAdapter.Fill(ds);
                            ds.Merge(oDt);
                            ds.TableName = TableName;
                       
                        //DataColumn oDc = new DataColumn();
                        //oDc.ColumnName = "ID";
                        //oDc.DataType = typeof(int);
                        //    ds.Columns.Add(oDc);
                        // ds.Columns.Add("ID", Type.GetType("int"));
                        int i = 1;
                            foreach (DataRow dsRow in ds.Rows)
                            {
                                dsRow["ID"] = i;
                                i++;
                              
                            }
                            ds.AcceptChanges();
                        foreach (DataRow dsRow in ds.Rows)
                            {
                               
                                dsRow.SetAdded();
                            }
                        oSqLiteDataAdapter.TableMappings.Add(TableName, TableName);
                        SQLiteCommandBuilder scb = new SQLiteCommandBuilder(oSqLiteDataAdapter);
                            oSqLiteDataAdapter.InsertCommand = scb.GetInsertCommand();

                            oSqLiteDataAdapter.Update(ds);
                        }
                        oSqLiteConn.Close();
                        }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message);
                        oSqLiteConn.Close();
                    }
                    
                }
          
        }
    }
}

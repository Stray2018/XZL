﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.DataProcessing.InMemoryDataProcessor;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Init
{
    public partial class FWToSAPSet : XtraForm
    {
        public static string mouseClick = string.Empty;
        public static int newData = 1;

        public FWToSAPSet()
        {
            InitializeComponent();
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FWToSAPSet_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);

            clearGridData();

            showOKRoleLst();
            saveOK.Text = "确定";

            string sqlCmd = "SELECT * FROM [ToSAP_FWobject]";
            DataTable erpDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            erpDt.Columns["FWCode"].Caption = "编码";
            erpDt.Columns["FWName"].Caption = "描述";
            erpDt.Columns["MasterTb"].Caption = "主表";
            erpDt.Columns["DataType"].Caption = "数据类型";
            erpDt.Columns["SubTb"].Caption = "子表";
            erpDt.Columns["SerTb"].Caption = "序列号表";
            erpDt.Columns["Memo"].Caption = "备注";

            ERPobject.Properties.DataSource = erpDt;
            ERPobject.Properties.DisplayMember = "FWName";
            ERPobject.Properties.ValueMember = "FWCode";
            ERPobject.Properties.PopupFormWidth = Convert.ToInt32(Width * 0.8);
            SetGridLookUpEditMoreColumnFilter(ERPobject);


            sqlCmd = "SELECT * FROM [ToSAP_DIobject]";
            DataTable SAPDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            SAPDt.Columns["SAPobjCode"].Caption = "编码";
            SAPDt.Columns["SAPobjName"].Caption = "描述";
            SAPDt.Columns["SAPDIinterface"].Caption = "DI 接口";
            SAPDt.Columns["Memo"].Caption = "备注";

            SAPobject.Properties.DataSource = SAPDt;
            SAPobject.Properties.DisplayMember = "SAPobjName";
            SAPobject.Properties.ValueMember = "SAPobjCode";
            SAPobject.Properties.PopupFormWidth = Convert.ToInt32(Width * 0.8);
            SetGridLookUpEditMoreColumnFilter(SAPobject);

            gridView2.IndicatorWidth = 40;
            gridView3.IndicatorWidth = 40;
            gridView1.IndicatorWidth = 40;
            Collect.EditValue = "N";
            checkCost.EditValue = "N";
            SAPCost.EditValue = "N";
            listBoxControl2.Visible = false;
            if (Program.DocToSAP.Equals("所有单"))  //所有单据一起导入时不显示汇总设置按钮
            {
                IsGroup.Visible = false;
            }
            else
            {
                IsGroup.Visible = true;
            }
        }

        /// <summary>
        /// 设置GridLookUpEdit多列过滤
        /// </summary>
        /// <param name="repGLUEdit">GridLookUpEdit的知识库，eg:gridlookUpEdit.Properties</param>
        private void  SetGridLookUpEditMoreColumnFilter(GridLookUpEdit repGLUEdit)
        {
            repGLUEdit.EditValueChanging += (sender, e) =>
            {
                BeginInvoke(new MethodInvoker(() =>
                    {
                        var edit = sender as GridLookUpEdit;
                        var view = edit.Properties.View;

                        var extraFilter = view.GetType()
                        .GetField("extraFilter", BindingFlags.NonPublic | BindingFlags.Instance);
                        var columnsOperators = new List<CriteriaOperator>();
                        foreach (GridColumn col in view.VisibleColumns)
                        {
                            if (col.Visible && col.ColumnType == typeof(string))
                            {
                                columnsOperators.Add(new FunctionOperator(FunctionOperatorType.Contains,
                                new OperandProperty(col.FieldName),
                                new OperandValue(edit.Text)));
                            }
                        }
                        var filterCondition = new GroupOperator(GroupOperatorType.Or, columnsOperators).ToString();
                        extraFilter.SetValue(view, filterCondition);

                        var ApplyColumnsFilterEx = view.GetType().GetMethod("ApplyColumnsFilterEx",
                        BindingFlags.NonPublic | BindingFlags.Instance);
                        ApplyColumnsFilterEx.Invoke(view, null);
                    }));
            };
        }

        private void SetGridLookUpEditMoreColumnFilter(RepositoryItemGridLookUpEdit repGLUEdit)
        {
            repGLUEdit.EditValueChanging += (sender, e) =>
            {
                BeginInvoke(new MethodInvoker(() =>
                    {
                        var edit = sender as GridLookUpEdit;
                        var view = edit.Properties.View;

                        var extraFilter = view.GetType()
                        .GetField("extraFilter", BindingFlags.NonPublic | BindingFlags.Instance);
                        var columnsOperators = new List<CriteriaOperator>();
                        foreach (GridColumn col in view.VisibleColumns)
                        {
                            if (col.Visible && col.ColumnType == typeof(string))
                            {
                                columnsOperators.Add(new FunctionOperator(FunctionOperatorType.Contains,
                                new OperandProperty(col.FieldName),
                                new OperandValue(edit.Text)));
                            }
                        }
                        var filterCondition = new GroupOperator(GroupOperatorType.Or, columnsOperators).ToString();
                        extraFilter.SetValue(view, filterCondition);

                        var ApplyColumnsFilterEx = view.GetType().GetMethod("ApplyColumnsFilterEx",
                        BindingFlags.NonPublic | BindingFlags.Instance);
                        ApplyColumnsFilterEx.Invoke(view, null);
                    }));
            };
        }

        private void setFwFieldsDt()
        {
            string Erpobjectstr = ERPobject.EditValue == null ? string.Empty : ERPobject.EditValue.ToString();
            string sqlCmd =
                "SELECT T2.FldName,T2.FldDesc,T2.Memo FROM ToSAP_FWobject T1 left JOIN ToSAP_FWFldDesc T2 ON T2.DataType = T1.DataType or T2.DataType = '固定值' WHERE T1.FWCode='" +
                Erpobjectstr + "'";
            DataTable fwFldDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            fwFldDt.Columns["FldName"].Caption = "字段名";
            fwFldDt.Columns["FldDesc"].Caption = "字段描述";
            fwFldDt.Columns["Memo"].Caption = "备注";
            MFWFieldrepos.DataSource = fwFldDt;
            MFWFieldrepos.DisplayMember = "FldName";
            MFWFieldrepos.ValueMember = "FldName";
            MFWFieldrepos.PopupFormWidth = Convert.ToInt32(Tabpane１
            .Width * 0.8);
            SetGridLookUpEditMoreColumnFilter(MFWFieldrepos);

            SFWFieldrepos.DataSource = fwFldDt;
            SFWFieldrepos.DisplayMember = "FldName";
            SFWFieldrepos.ValueMember = "FldName";
            SFWFieldrepos.PopupFormWidth = Convert.ToInt32(Tabpane１
            .Width * 0.8);
            SetGridLookUpEditMoreColumnFilter(SFWFieldrepos);

            TFWField.ColumnEdit = TFWFieldrepos;
            TFWFieldrepos.DataSource = fwFldDt;
            TFWFieldrepos.DisplayMember = "FldName";
            TFWFieldrepos.ValueMember = "FldName";
            TFWFieldrepos.PopupFormWidth = Convert.ToInt32(Tabpane１
            .Width * 0.8);
            SetGridLookUpEditMoreColumnFilter(TFWFieldrepos);

            if (Erpobjectstr.Equals(string.Empty))
            {
                return;
            }
            sqlCmd = "SELECT DataType  FROM ToSAP_FWobject WHERE FWCode = '" + Erpobjectstr + "'";
            string DataType = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null).Rows[0][0]
                .ToString();
            if (DataType == "单据")
            {
                IsGroup.Visible = true;
            }
            else
            {
                IsGroup.Visible = false;
            }
            IsGroup.CheckState = CheckState.Unchecked;
        }

        private void setSADIDt()
        {
            string SAPobjectsgtr = SAPobject.EditValue == null ? string.Empty : SAPobject.EditValue.ToString();
            string sqlCmd =
                "SELECT T3.DIProperty,T3.DIProDesc,T3.Memo FROM [ToSAP_DIobject] T1 LEFT JOIN [ToSAP_DIinterface] T2 ON T1.SAPDIinterface=T2.DIInterface LEFT JOIN ToSAP_DIProp T3 ON T2.DIobjType= T3.DIobjType WHERE T1.SAPobjCode = '" +
                SAPobjectsgtr + "' AND T3.DItype='主表'";
            DataTable SAPFldDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            SAPFldDt.Columns["DIProperty"].Caption = "DI属性";
            SAPFldDt.Columns["DIProDesc"].Caption = "DI描述";
            SAPFldDt.Columns["Memo"].Caption = "备注";
            MSAPDI.ColumnEdit = MSAPDIrepos;
            MSAPDIrepos.DataSource = SAPFldDt;
            MSAPDIrepos.DisplayMember = "DIProperty";
            MSAPDIrepos.ValueMember = "DIProperty";
            MSAPDIrepos.PopupFormWidth = Convert.ToInt32(Tabpane１
            .Width * 0.65);
            MSAPDIrepos.TextEditStyle = TextEditStyles.Standard;
            SetGridLookUpEditMoreColumnFilter(MSAPDIrepos);

            sqlCmd =
                "SELECT T3.DIProperty,T3.DIProDesc,T3.Memo ,T2.DI FROM [ToSAP_DIobject] T1 LEFT JOIN [ToSAP_DIinterface] T2 ON T1.SAPDIinterface=T2.DIInterface LEFT JOIN ToSAP_DIProp T3 ON T2.DIobjType= T3.DIobjType WHERE T1.SAPobjCode = '" +
                SAPobjectsgtr + "' AND T3.DItype='行'";
            DataTable SAPFldDtS = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            SAPFldDtS.Columns["DIProperty"].Caption = "DI属性";
            SAPFldDtS.Columns["DIProDesc"].Caption = "DI描述";
            SAPFldDtS.Columns["Memo"].Caption = "备注";
            SSAPDIrepos.DataSource = SAPFldDtS;
            SSAPDIrepos.DisplayMember = "DIProperty";
            SSAPDIrepos.ValueMember = "DIProperty";
            SSAPDIrepos.PopupFormWidth = Convert.ToInt32(Tabpane１.Width * 0.65);
            SSAPDIrepos.TextEditStyle = TextEditStyles.Standard;
            SetGridLookUpEditMoreColumnFilter(SSAPDIrepos);
            if (SAPFldDtS.Rows.Count > 0)
            {
                string sapDI = SAPFldDtS.Rows[0]["DI"].ToString();
                if (sapDI.Equals("JournalEntries"))
                {
                    gridView2.Columns["isDebit"].Visible = true;
                }
                else
                {
                    gridView2.Columns["isDebit"].Visible = false;
                }
            }

            sqlCmd =
                "SELECT T3.DIProperty,T3.DIProDesc,T3.Memo FROM [ToSAP_DIobject] T1 LEFT JOIN [ToSAP_DIinterface] T2 ON T1.SAPDIinterface=T2.DIInterface LEFT JOIN ToSAP_DIProp T3 ON T2.DIobjType= T3.DIobjType WHERE T1.SAPobjCode = '" +
                SAPobjectsgtr + "' AND T3.DItype='序列号'";
            DataTable SAPFldDtT = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            SAPFldDtT.Columns["DIProperty"].Caption = "DI属性";
            SAPFldDtT.Columns["DIProDesc"].Caption = "DI描述";
            SAPFldDtT.Columns["Memo"].Caption = "备注";
            TSAPDIrepos.DataSource = SAPFldDtT;
            TSAPDIrepos.DisplayMember = "DIProperty";
            TSAPDIrepos.ValueMember = "DIProperty";
            TSAPDIrepos.PopupFormWidth = Convert.ToInt32(Tabpane１.Width * 0.65);
            TSAPDIrepos.TextEditStyle = TextEditStyles.Standard;
            SetGridLookUpEditMoreColumnFilter(TSAPDIrepos);
            
            

        }

        private void ERPobject_EditValueChanged(object sender, EventArgs e)
        {
            setFwFieldsDt();
        }

        private void SAPobject_EditValueChanged(object sender, EventArgs e)
        {
            setSADIDt();
        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "FWField")
            {
                DataTable oDataTable = (DataTable) MFWFieldrepos.DataSource;
                string MFWFieldvalue = gridView1.GetFocusedRowCellValue("FWField").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("FldName='{0}'", MFWFieldvalue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string FWFldDescstr = row["FldDesc"].ToString();

                    gridView1.SetRowCellValue(e.RowHandle, "FWFieldDesc", FWFldDescstr);
                }
            }

            if (e.Column.FieldName == "SAPDI")
            {
                DataTable oDataTable = (DataTable) MSAPDIrepos.DataSource;
                string SAPDIValue = gridView1.GetFocusedRowCellValue("SAPDI").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("DIProperty='{0}'", SAPDIValue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string SAPDescValue = row["DIProDesc"].ToString();

                    gridView1.SetRowCellValue(e.RowHandle, "SAPDIDesc", SAPDescValue);
                }
            }
            saveOK.Text =saveOK.Text.Equals("确定")? "更新": saveOK.Text;
        }

        private void gridView2_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "FWField")
            {
                DataTable oDataTable = (DataTable) SFWFieldrepos.DataSource;
                string MFWFieldvalue = gridView2.GetFocusedRowCellValue("FWField").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("FldName='{0}'", MFWFieldvalue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string FWFldDescstr = row["FldDesc"].ToString();

                    gridView2.SetRowCellValue(e.RowHandle, "FWFieldDesc", FWFldDescstr);
                }
            }

            if (e.Column.FieldName == "SAPDI")
            {
                DataTable oDataTable = (DataTable) SSAPDIrepos.DataSource;
                string SAPDIValue = gridView2.GetFocusedRowCellValue("SAPDI").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("DIProperty='{0}'", SAPDIValue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string SAPDescValue = row["DIProDesc"].ToString();

                    gridView2.SetRowCellValue(e.RowHandle, "SAPDIDesc", SAPDescValue);
                }
            }

            saveOK.Text = saveOK.Text.Equals("确定") ? "更新" : saveOK.Text;
        }

        private void gridView3_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "FWField")
            {
                DataTable oDataTable = (DataTable) TFWFieldrepos.DataSource;
                string MFWFieldvalue = gridView3.GetFocusedRowCellValue("FWField").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("FldName='{0}'", MFWFieldvalue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string FWFldDescstr = row["FldDesc"].ToString();

                    gridView3.SetRowCellValue(e.RowHandle, "FWFieldDesc", FWFldDescstr);
                }
            }

            if (e.Column.FieldName == "SAPDI")
            {
                DataTable oDataTable = (DataTable) TSAPDIrepos.DataSource;
                string SAPDIValue = gridView3.GetFocusedRowCellValue("SAPDI").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("DIProperty='{0}'", SAPDIValue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string SAPDescValue = row["DIProDesc"].ToString();

                    gridView3.SetRowCellValue(e.RowHandle, "SAPDIDesc", SAPDescValue);
                }
            }
            saveOK.Text = saveOK.Text.Equals("确定") ? "更新" : saveOK.Text;
        }


        private void gridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }


        private void gridView1_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView1.FocusedRowHandle + 1;
            var fwtemp = string.Empty;
            var saptemp = string.Empty;

            try
            {
                fwtemp = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["FWField"]);
            }
            catch (Exception exception)
            {
            }

            try
            {
                saptemp = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["SAPDI"]);
            }
            catch (Exception exception)
            {
            }

            if (e.FocusedColumn.FieldName == "SAPDIDesc" && gridView1.RowCount == rowid && !fwtemp.Equals(string.Empty) &&
                !saptemp.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void gridView2_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView2.FocusedRowHandle + 1;
            var fwtemp = string.Empty;
            var saptemp = string.Empty;

            try
            {
                fwtemp = gridView2.GetRowCellDisplayText(rowid - 1, gridView2.Columns["FWField"]);
            }
            catch (Exception exception)
            {
            }

            try
            {
                saptemp = gridView2.GetRowCellDisplayText(rowid - 1, gridView2.Columns["SAPDI"]);
            }
            catch (Exception exception)
            {
            }

            if (e.FocusedColumn.FieldName == "SAPDIDesc" && gridView2.RowCount == rowid && !fwtemp.Equals(string.Empty) &&
                !saptemp.Equals(string.Empty))
            {
                gridView2.AddNewRow();
                gridView2.FocusedRowHandle = rowid - 1;
            }
        }

        private void gridView3_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView3.FocusedRowHandle + 1;
            var fwtemp = string.Empty;
            var saptemp = string.Empty;

            try
            {
                fwtemp = gridView3.GetRowCellDisplayText(rowid - 1, gridView3.Columns["FWField"]);
            }
            catch (Exception exception)
            {
            }

            try
            {
                saptemp = gridView3.GetRowCellDisplayText(rowid - 1, gridView3.Columns["SAPDI"]);
            }
            catch (Exception exception)
            {
            }

            if (e.FocusedColumn.FieldName == "SAPDIDesc" && gridView3.RowCount == rowid && !fwtemp.Equals(string.Empty) &&
                !saptemp.Equals(string.Empty))
            {
                gridView3.AddNewRow();
                gridView3.FocusedRowHandle = rowid - 1;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                switch (mouseClick)
                {
                    case "1":
                        Common.Common.InsertDataToDataSource(gridView1, GridControl1);
                        break;
                    case "2":
                        Common.Common.InsertDataToDataSource(gridView2, gridControl2);
                        break;
                    case "3":
                        Common.Common.InsertDataToDataSource(gridView3, gridControl3);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (mouseClick)
            {
                case "1":
                    gridView1.DeleteSelectedRows();
                    break;
                case "2":
                    gridView2.DeleteSelectedRows();
                    break;
                case "3":
                    gridView3.DeleteSelectedRows();
                    break;
                default:
                    break;
            }
        }

        private void gridControl2_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                mouseClick = "2";
                Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }

        private void GridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            mouseClick = "1";
            if (e.Button == MouseButtons.Right)
            {
                Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }

        private void gridControl3_MouseClick(object sender, MouseEventArgs e)
        {
            mouseClick = "3";
            if (e.Button == MouseButtons.Right)
            {
                Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }

        private void gridView2_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView3_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ERPobject.EditValue = null;
            SAPobject.EditValue = null;
            AttCon.Text = string.Empty;
            IsGroup.CheckState = CheckState.Unchecked;
            Collect.CheckState = CheckState.Unchecked;
            SAPCost.CheckState = CheckState.Unchecked;
            isDraft.CheckState = CheckState.Unchecked;
            saveOK.Text = "保存";
            clearGridData();
            newData = 1;
        }

        private void clearGridData()
        {
            string sqlCmd = "SELECT * FROM ToSAP_Role_Sub T1 WHERE T1.DataType = '子表' AND 1=2";
            DataTable Grd2Dt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataColumn oDataColumn1 = Grd2Dt.Columns["DataType"];
            oDataColumn1.DefaultValue = "子表";
            DataColumn oDataColumn2 = Grd2Dt.Columns["IsSum"];
            oDataColumn2.DefaultValue = "N";
            DataColumn oDataColumn21 = Grd2Dt.Columns["KeyField"];
            oDataColumn21.DefaultValue = "N";
            DataColumn oDataColumn22 = Grd2Dt.Columns["IsGroup"];
            oDataColumn22.DefaultValue = "N";
            DataColumn oDataColumn23 = Grd2Dt.Columns["IsMasTbl"];
            oDataColumn23.DefaultValue = "N";
            DataColumn oDataColumn25 = Grd2Dt.Columns["isDebit"];
            oDataColumn25.DefaultValue = "N";
            DataColumn oDataColumn24 = Grd2Dt.Columns["CodePrefix"];
            oDataColumn24.DefaultValue = "";
            DataRow oDataRow = Grd2Dt.NewRow();
            Grd2Dt.Rows.Add(oDataRow);
            gridControl2.DataSource = Grd2Dt;



            sqlCmd = "SELECT * FROM ToSAP_Role_Sub T1 WHERE T1.DataType = '序列号' AND 1=2";
            DataTable Grd3Dt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            DataColumn oDataColumn3 = Grd3Dt.Columns["DataType"];
            oDataColumn3.DefaultValue = "序列号";
            DataColumn oDataColumn4 = Grd3Dt.Columns["IsSum"];
            oDataColumn4.DefaultValue = "N";
            DataColumn oDataColumn41 = Grd3Dt.Columns["KeyField"];
            oDataColumn41.DefaultValue = "N";
            DataColumn oDataColumn42 = Grd3Dt.Columns["IsGroup"];
            oDataColumn42.DefaultValue = "N";
            DataColumn oDataColumn45 = Grd3Dt.Columns["isDebit"];
            oDataColumn45.DefaultValue = "N";

            DataColumn oDataColumn43 = Grd3Dt.Columns["IsMasTbl"];
            oDataColumn43.DefaultValue = "N";
            DataColumn oDataColumn44 = Grd3Dt.Columns["CodePrefix"];
            oDataColumn44.DefaultValue = "";
            oDataRow = Grd3Dt.NewRow();
            Grd3Dt.Rows.Add(oDataRow);
            gridControl3.DataSource = Grd3Dt;



            sqlCmd = "SELECT * FROM ToSAP_Role_Sub T1 WHERE T1.DataType = '主表' AND 1=2";
            DataTable Grd1Dt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            DataColumn oDataColumn = Grd1Dt.Columns["DataType"];
            oDataColumn.DefaultValue = "主表";
            DataColumn oDataColumn5 = Grd1Dt.Columns["IsSum"];
            oDataColumn5.DefaultValue = "N";
            DataColumn oDataColumn51 = Grd1Dt.Columns["KeyField"];
            oDataColumn51.DefaultValue = "N";
            DataColumn oDataColumn52 = Grd1Dt.Columns["IsGroup"];
            oDataColumn52.DefaultValue = "N";
            DataColumn oDataColumn53 = Grd1Dt.Columns["CodePrefix"];
            oDataColumn53.DefaultValue = "";
            DataColumn oDataColumn55 = Grd1Dt.Columns["isDebit"];
            oDataColumn55.DefaultValue = "N";
            DataColumn oDataColumn54 = Grd1Dt.Columns["IsMasTbl"];
            oDataColumn54.DefaultValue = "N";
            oDataRow = Grd1Dt.NewRow();
            Grd1Dt.Rows.Add(oDataRow);
            GridControl1.DataSource = Grd1Dt;

            repositoryItemComboBox3.Items.Clear();
            repositoryItemComboBox6.Items.Clear();
            repositoryItemComboBox7.Items.Clear();
            sqlCmd = "SELECT dataType FROM ToSAP_ERPDataType";
            DataTable oDataTable1 = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            foreach (DataRow oDataRow1 in oDataTable1.Rows)
            {
                repositoryItemComboBox3.Items.Add(oDataRow1[0].ToString().Trim());
                repositoryItemComboBox6.Items.Add(oDataRow1[0].ToString().Trim());
                repositoryItemComboBox7.Items.Add(oDataRow1[0].ToString().Trim());
            }
        }

        private void showOKRoleLst()
        {
            listBoxControl1.MultiColumn = true;
            var sqlList =
                "SELECT T1.DocEntry,t2.FWName+'-->'+T3.SAPobjName AS 'Descr' FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_FWobject T2 ON T1.ERPobject = T2.FWCode LEFT JOIN ToSAP_DIobject T3 ON T1.SAPobject=T3.SAPobjCode ORDER BY T2.FWName,t3.SAPobjName";
            DataTable lstDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlList, null);
            listBoxControl1.DisplayMember = "Descr";
            listBoxControl1.ValueMember = "DocEntry";
            listBoxControl1.DataSource = lstDt;
            listBoxControl1.SelectedIndex = -1;
            //simpleButton1.PerformClick();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = "SELECT MAX(DocEntry)+1 DocEntry FROM ToSAP_Role_Mas";
            string DocEntry = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null).Rows[0][0]
                .ToString();
            if (newData == 0)
            {
                DocEntry = listBoxControl1.SelectedValue.ToString();
            }
            sqlCmd = "SELECT * FROM ToSAP_Role_Mas where 1=2";
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            DataRow oDataRow = oDataTable.NewRow();
            oDataRow["DocEntry"] = DocEntry;
               oDataRow["ERPobject"] = ERPobject.EditValue.ToString();
            oDataRow["SAPobject"] = SAPobject.EditValue.ToString();
            oDataRow["Contion"] = AttCon.Text.Trim();
            oDataRow["IsSum"] = GroupType.SelectedIndex;
            oDataRow["SumImport"] = IsGroup.EditValue;
            oDataRow["IsCollect"] = Collect.EditValue;
            oDataRow["checkCost"] = checkCost.EditValue;
            oDataRow["ToOrder"] = ToOrder.EditValue;
            oDataRow["SAPCost"] = SAPCost.EditValue;
            oDataRow["Remarks"] = richTextBox1.Text;
            oDataRow["DocStatus"] = DocStatus.Text;
            oDataRow["isDraft"] = isDraft.EditValue;
            oDataTable.Rows.Add(oDataRow);

            DataTable mTable = (DataTable) GridControl1.DataSource;
            mTable.AcceptChanges();
            DataTable okDataTable = mTable.Clone();
            if (mTable.Rows.Count < 2)
            {
                MessageBox.Show("主表数据为空,不允许保存!");
                return;
            }

            okDataTable.Clear();
            foreach (DataRow oRow in mTable.Rows)
            {
                if (!oRow["FWField"].ToString().Equals(string.Empty) && !oRow["SAPDI"].ToString().Equals(string.Empty))
                {
                    oRow["DocEntry"] = DocEntry;
                    okDataTable.ImportRow(oRow);
                }
            }
            DataTable STable = (DataTable) gridControl2.DataSource;
            STable.AcceptChanges();
            foreach (DataRow oRow in STable.Rows)
            {
                if (!oRow["FWField"].ToString().Equals(string.Empty) && !oRow["SAPDI"].ToString().Equals(string.Empty))
                {
                    oRow["DocEntry"] = DocEntry;
                    okDataTable.ImportRow(oRow);
                }
            }
            DataTable TTable = (DataTable) gridControl3.DataSource;
            TTable.AcceptChanges();
            foreach (DataRow oRow in TTable.Rows)
            {
                if (!oRow["FWField"].ToString().Equals(string.Empty) && !oRow["SAPDI"].ToString().Equals(string.Empty))
                {
                    oRow["DocEntry"] = DocEntry;
                    okDataTable.ImportRow(oRow);
                }
            }
            SqlTransaction oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                if (newData == 0)
                {
                    string sqlCmdd = "DELETE FROM ToSAP_Role_Mas WHERE DocEntry = '" + DocEntry + "'";
                    Common.Common.ExecSqlcmd(oSqlTransaction, Program.FwSqlConnection, sqlCmdd);
                    sqlCmdd = "DELETE FROM ToSAP_Role_Sub WHERE DocEntry = '" + DocEntry + "'";
                    Common.Common.ExecSqlcmd(oSqlTransaction, Program.FwSqlConnection, sqlCmdd);
                }

                bool Error1=
                Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, okDataTable,
                    "ToSAP_Role_Sub");
                bool error2=
                Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable, "ToSAP_Role_Mas");
                if (Error1 && error2)
                {
                    oSqlTransaction.Commit();
                    MessageBox.Show("数据保存成功!");
                    //ERPobject.EditValue = null;
                    //SAPobject.EditValue = null;
                    //AttCon.Text = string.Empty;
                    //clearGridData();
                    newData = 0;
                    saveOK.Text = "确定";
                    showOKRoleLst();
                }
                else
                {
                    oSqlTransaction.Rollback();
                }

                
            }
            catch (Exception exception)
            {
                oSqlTransaction.Rollback();
                MessageBox.Show(exception.Message);
            }
        }

        private void listBoxControl1_MouseClick(object sender, MouseEventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    newData = 0;
                    string sqlCmd = "SELECT T1.*,T3.DI FROM ToSAP_Role_Mas T1 inner join ToSAP_Diobject T2 on T1.SAPobject = T2.SAPobjCode inner join ToSAP_DIinterface T3 on T2.SAPDIinterface=T3.DIInterface WHERE T1.DocEntry = '" + listBoxControl1.SelectedValue + "'";
                    DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                    ERPobject.EditValue = oDataTable.Rows[0]["ERPobject"].ToString().Trim();
                    SAPobject.EditValue = oDataTable.Rows[0]["SAPobject"].ToString().Trim();
                    GroupType.SelectedIndex = Convert.ToInt32(oDataTable.Rows[0]["IsSum"].ToString());
                    IsGroup.EditValue = oDataTable.Rows[0]["SumImport"].ToString().Trim();
                    AttCon.Text = oDataTable.Rows[0]["Contion"].ToString();
                    Collect.EditValue = oDataTable.Rows[0]["IsCollect"].ToString().Trim();
                    checkCost.EditValue = oDataTable.Rows[0]["checkCost"].ToString().Trim();
                    ToOrder.EditValue = oDataTable.Rows[0]["ToOrder"].ToString().Trim();
                    SAPCost.EditValue = oDataTable.Rows[0]["SAPCost"].ToString().Trim();
                    richTextBox1.Text = oDataTable.Rows[0]["Remarks"].ToString().Trim();
                    DocStatus.Text = oDataTable.Rows[0]["DocStatus"].ToString().Trim();
                    isDraft.EditValue = oDataTable.Rows[0]["isDraft"].ToString().Trim();
                    string DocEntry = listBoxControl1.SelectedValue.ToString();
                    string sapDI= oDataTable.Rows[0]["DI"].ToString().Trim();
                    loadGridData(DocEntry, sapDI);
                    saveOK.Text = "确定";
                }

                if (e.Button == MouseButtons.Right)
                {
                    Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                    popupMenu2.ShowPopup(newPoint);
                }
            }
            catch (Exception exception)
            {
            }
        }

        private void loadGridData(string DocEntry,string sapobj)
        {
            if (sapobj.Equals("JournalEntries"))
            {
                gridView2.Columns["isDebit"].Visible = true;
            }else
            {
                gridView2.Columns["isDebit"].Visible = false;
            }
            string sqlCmd = "SELECT * FROM ToSAP_Role_Sub T1 WHERE T1.DataType = '子表' AND DocEntry = '" +
                         DocEntry + "'";
            DataTable Grd2Dt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            DataColumn oDataColumn1 = Grd2Dt.Columns["DataType"];
            oDataColumn1.DefaultValue = "子表";
            DataColumn oDataColum2 = Grd2Dt.Columns["IsSum"];
            oDataColum2.DefaultValue = "N";
            DataColumn oDataColum21 = Grd2Dt.Columns["KeyField"];
            oDataColum21.DefaultValue = "N";
            DataColumn oDataColumn42 = Grd2Dt.Columns["IsGroup"];
            oDataColumn42.DefaultValue = "N";
            DataColumn oDataColumn43 = Grd2Dt.Columns["IsMasTbl"];
            oDataColumn43.DefaultValue = "N";
            DataColumn oDataColumn44 = Grd2Dt.Columns["CodePrefix"];
            oDataColumn44.DefaultValue = "";
            DataColumn oDataColumn45 = Grd2Dt.Columns["isDebit"];
            oDataColumn45.DefaultValue = "N";
            DataRow oDataRow = Grd2Dt.NewRow();
            Grd2Dt.Rows.Add(oDataRow);
            gridControl2.DataSource = Grd2Dt;

            sqlCmd = "SELECT * FROM ToSAP_Role_Sub T1 WHERE T1.DataType = '序列号' AND DocEntry = '" +
                     DocEntry + "'";
            DataTable Grd3Dt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            DataColumn oDataColumn2 = Grd3Dt.Columns["DataType"];
            oDataColumn2.DefaultValue = "序列号";
            DataColumn oDataColumn22 = Grd3Dt.Columns["IsSum"];
            oDataColumn22.DefaultValue = "N";
            DataColumn oDataColumn23 = Grd3Dt.Columns["KeyField"];
            oDataColumn23.DefaultValue = "N";
            DataColumn oDataColumn24 = Grd3Dt.Columns["IsGroup"];
            oDataColumn24.DefaultValue = "N";
            DataColumn oDataColumn25 = Grd3Dt.Columns["IsMasTbl"];
            oDataColumn25.DefaultValue = "N";
            DataColumn oDataColumn26 = Grd3Dt.Columns["CodePrefix"];
            oDataColumn26.DefaultValue = "";
            DataColumn oDataColumn27 = Grd3Dt.Columns["isDebit"];
            oDataColumn27.DefaultValue = "N";
            oDataRow = Grd3Dt.NewRow();
            Grd3Dt.Rows.Add(oDataRow);
            gridControl3.DataSource = Grd3Dt;

            sqlCmd = "SELECT * FROM ToSAP_Role_Sub T1 WHERE T1.DataType = '主表' AND DocEntry = '" +
                     DocEntry+ "'";
            DataTable Grd1Dt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            DataColumn oDataColumn = Grd1Dt.Columns["DataType"];
            oDataColumn.DefaultValue = "主表";
            DataColumn oDataColumn3 = Grd1Dt.Columns["IsSum"];
            oDataColumn3.DefaultValue = "N";
            DataColumn oDataColumn31 = Grd1Dt.Columns["KeyField"];
            oDataColumn31.DefaultValue = "N";
            DataColumn oDataColumn32 = Grd1Dt.Columns["IsGroup"];
            oDataColumn32.DefaultValue = "N";
            DataColumn oDataColumn33 = Grd1Dt.Columns["CodePrefix"];
            oDataColumn33.DefaultValue = "";
            DataColumn oDataColumn34 = Grd1Dt.Columns["IsMasTbl"];
            oDataColumn34.DefaultValue = "N";
            DataColumn oDataColumn35 = Grd1Dt.Columns["isDebit"];
            oDataColumn35.DefaultValue = "N";
            oDataRow = Grd1Dt.NewRow();
            Grd1Dt.Rows.Add(oDataRow);
            GridControl1.DataSource = Grd1Dt;
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            DialogResult confirm = MessageBox.Show("确认删除当前规则?", "删除数据", MessageBoxButtons.YesNo);
            if (confirm == DialogResult.Yes)
            {
                try
                {
                    string sqlCmd = "DELETE FROM ToSAP_Role_Sub WHERE DocEntry = '" + listBoxControl1.SelectedValue + "'";
                    Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
                    sqlCmd = "DELETE FROM ToSAP_Role_Mas WHERE DocEntry = '" + listBoxControl1.SelectedValue + "'";
                    Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);

                    showOKRoleLst();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void checkEdit1_CheckStateChanged(object sender, EventArgs e)
        {
            if (IsGroup.CheckState == CheckState.Checked)
            {
                GroupType.Visible = true;
                Collect.Visible = true;
                SAPCost.Visible = false;
            }
            else
            {
                GroupType.Visible = false;
                Collect.Visible = false;
                Collect.EditValue = "N";
                SAPCost.Visible = true;
            }
        }

        private void IsGroup_Click(object sender, EventArgs e)
        {
            if (Program.DocToSAP.Equals("零售单"))
            {
                string erpobject = ERPobject.EditValue.ToString();
                if (erpobject=="")
                {
                    MessageBox.Show("请先选择ERP对象!");
                    return;
                }

                string oSql = "SELECT MasterTb FROM ToSAP_Fwobject WHERE FWCode='"+ erpobject + "'";
                string masTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, oSql, null).Rows[0][0].ToString();
                if (masTable.Equals("OSAL"))
                {
                    MessageBox.Show("设置统一同步零售单时,零售单不允许汇总同步!");
                    return;
                }

            }
            

        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
           var sqlList =
                "SELECT T1.DocEntry,t2.FWName+'-->'+T3.SAPobjName AS 'Descr' FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_FWobject T2 ON T1.ERPobject = T2.FWCode LEFT JOIN ToSAP_DIobject T3 ON T1.SAPobject=T3.SAPobjCode WHERE T2.DataType='单据' ORDER BY T2.FWName,t3.SAPobjName";
            DataTable lstDt = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlList, null);
            listBoxControl2.DisplayMember = "Descr";
            listBoxControl2.ValueMember = "DocEntry";
            listBoxControl2.DataSource = lstDt;
            listBoxControl2.Top = simpleButton2.Top - 430;
            listBoxControl2.Width = 300;
            listBoxControl2.MultiColumn = false;
            listBoxControl2.Visible = true;
            
            }



        private void listBoxControl2_Click(object sender, EventArgs e)
        {
            string DocEntry = listBoxControl2.SelectedValue.ToString();
            loadGridData(DocEntry,"");
            listBoxControl2.Visible = false;
        }

        private void memoEdit1_Leave(object sender, EventArgs e)
        {
            gridView1.SetRowCellValue(RowId, "FWFieldDesc", memoEdit1.Text);
            memoEdit1.Visible = false;
        }

        public static int RowId = 0;

        private void memoEdit2_Leave(object sender, EventArgs e)
        {
            gridView2.SetRowCellValue(RowId, "FWFieldDesc", memoEdit2.Text);
            memoEdit2.Visible = false;
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                if (e.HitInfo.Column.Name.Equals("MFWFieldDesc") && e.Clicks == 2)
                {
                    memoEdit1.Visible = false;
                    memoEdit1.Location = new Point(e.X + 10, e.Y + 10);
                    memoEdit1.Height = GridControl1.Height / 2;
                    memoEdit1.Width = GridControl1.Width / 2;
                    memoEdit1.Text = gridView1.GetFocusedDisplayText();
                    RowId = e.RowHandle;//gridView1.FocusedRowHandle;
                    memoEdit1.Visible = true;
                }
            }
            catch (Exception exception)
            {
              
            }
            
            
        }

        private void gridView2_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                if (e.HitInfo.Column.Name.Equals("FWFieldDesc") && e.Clicks == 2)
                {
                    memoEdit2.Visible = false;
                    memoEdit2.Location = new Point(e.X + 10, e.Y + 10);
                    memoEdit2.Height = GridControl1.Height / 2;
                    memoEdit2.Width = GridControl1.Width / 2;
                    memoEdit2.Text = gridView2.GetFocusedDisplayText();
                    RowId = e.RowHandle;
                    memoEdit2.Visible = true;
                }
            }
            catch (Exception exception)
            {
            }
            
                
        }
    }
}

﻿namespace ToSAP.Init
{
    partial class FWToSAPSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MFWFieldrepos = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.MSAPDIrepos = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.AttCon = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Tabpane１ = new DevExpress.XtraBars.Navigation.TabPane();
            this.MasTable = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.listBoxControl2 = new DevExpress.XtraEditors.ListBoxControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MFWField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MFWFieldDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MSAPDI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MSAPDIDescr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MFWDataType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MKeyField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MIsGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MMemo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SubTable = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SFWField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SFWFieldrepos = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FWFieldDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SSAPDI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SSAPDIrepos = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SSAPDIDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FWDataType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.SKeyField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.isDebit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.SIsGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.IsSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.IsMasTbl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemComboBox11 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemPopupContainerEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemComboBox10 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemGridLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SerTable = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TFWField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TFWFieldrepos = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TFWFieldDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TSAPDI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TSAPDIrepos = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TSAPDIDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TFWDataType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.TKeyField = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemGridLookUpEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.listBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.saveOK = new DevExpress.XtraEditors.SimpleButton();
            this.CancelOK = new DevExpress.XtraEditors.SimpleButton();
            this.ERPobject = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SAPobject = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.popupMenu2 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.GroupType = new DevExpress.XtraEditors.RadioGroup();
            this.IsGroup = new DevExpress.XtraEditors.CheckEdit();
            this.Collect = new DevExpress.XtraEditors.CheckEdit();
            this.checkCost = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.ToOrder = new DevExpress.XtraEditors.TextEdit();
            this.SAPCost = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.DocStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.isDraft = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.MFWFieldrepos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MSAPDIrepos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttCon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabpane１)).BeginInit();
            this.Tabpane１.SuspendLayout();
            this.MasTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SubTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SFWFieldrepos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSAPDIrepos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.SerTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TFWFieldrepos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TSAPDIrepos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERPobject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPobject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Collect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDraft.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // MFWFieldrepos
            // 
            this.MFWFieldrepos.AutoHeight = false;
            this.MFWFieldrepos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MFWFieldrepos.ImmediatePopup = true;
            this.MFWFieldrepos.Name = "MFWFieldrepos";
            this.MFWFieldrepos.NullText = "";
            this.MFWFieldrepos.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.MFWFieldrepos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // MSAPDIrepos
            // 
            this.MSAPDIrepos.AutoHeight = false;
            this.MSAPDIrepos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MSAPDIrepos.ImmediatePopup = true;
            this.MSAPDIrepos.Name = "MSAPDIrepos";
            this.MSAPDIrepos.NullText = "";
            this.MSAPDIrepos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "数值",
            "店铺",
            "仓库",
            "供应商",
            "客户",
            "商品主数据",
            "固定值"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit3.NullText = "N";
            this.repositoryItemCheckEdit3.ValueChecked = "Y";
            this.repositoryItemCheckEdit3.ValueGrayed = "N";
            this.repositoryItemCheckEdit3.ValueUnchecked = "N";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "数值",
            "自定义字段"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox4.Items.AddRange(new object[] {
            "数值",
            "店铺",
            "仓库",
            "供应商",
            "客户",
            "商品主数据",
            "固定值"});
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox5.Items.AddRange(new object[] {
            "数值",
            "自定义字段"});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(32, 11);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "ERP对象:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(183, 11);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "SAP对象:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(231, 39);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(52, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "附加条件:";
            // 
            // AttCon
            // 
            this.AttCon.Location = new System.Drawing.Point(279, 39);
            this.AttCon.Name = "AttCon";
            this.AttCon.Size = new System.Drawing.Size(308, 20);
            this.AttCon.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(232, 72);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(502, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "注:附加条件为SQL Where后的条件,例如: and T1.Docdate>=\'2018-01-01\'  主表:T1,子表:T2,OITM:T3";
            // 
            // Tabpane１
            // 
            this.Tabpane１.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tabpane１.Controls.Add(this.MasTable);
            this.Tabpane１.Controls.Add(this.SubTable);
            this.Tabpane１.Controls.Add(this.SerTable);
            this.Tabpane１.Location = new System.Drawing.Point(162, 89);
            this.Tabpane１.Name = "Tabpane１";
            this.Tabpane１.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.MasTable,
            this.SubTable,
            this.SerTable});
            this.Tabpane１.RegularSize = new System.Drawing.Size(864, 337);
            this.Tabpane１.SelectedPage = this.MasTable;
            this.Tabpane１.Size = new System.Drawing.Size(864, 337);
            this.Tabpane１.TabIndex = 7;
            this.Tabpane１.Text = "Tabpane1";
            this.Tabpane１.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gridControl2_MouseClick);
            // 
            // MasTable
            // 
            this.MasTable.AutoSize = true;
            this.MasTable.Caption = "主表";
            this.MasTable.Controls.Add(this.memoEdit1);
            this.MasTable.Controls.Add(this.listBoxControl2);
            this.MasTable.Controls.Add(this.GridControl1);
            this.MasTable.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.MasTable.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MasTable.Name = "MasTable";
            this.MasTable.Size = new System.Drawing.Size(864, 306);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(20, 63);
            this.memoEdit1.MenuManager = this.barManager1;
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(274, 228);
            this.memoEdit1.TabIndex = 50;
            this.memoEdit1.Visible = false;
            this.memoEdit1.Leave += new System.EventHandler(this.memoEdit1_Leave);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManager1.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1036, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 472);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1036, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 472);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1036, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 472);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "粘贴";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "删除";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "删除";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // listBoxControl2
            // 
            this.listBoxControl2.HorizontalScrollbar = true;
            this.listBoxControl2.Location = new System.Drawing.Point(337, 38);
            this.listBoxControl2.MultiColumn = true;
            this.listBoxControl2.Name = "listBoxControl2";
            this.listBoxControl2.Size = new System.Drawing.Size(173, 265);
            this.listBoxControl2.TabIndex = 49;
            this.listBoxControl2.Click += new System.EventHandler(this.listBoxControl2_Click);
            // 
            // GridControl1
            // 
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.gridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(864, 306);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.GridControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GridControl1_MouseClick);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MFWField,
            this.MFWFieldDesc,
            this.MSAPDI,
            this.MSAPDIDescr,
            this.MFWDataType,
            this.MKeyField,
            this.MIsGroup,
            this.MMemo});
            this.gridView1.DetailHeight = 325;
            this.gridView1.GridControl = this.GridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            this.gridView1.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gridView1_CustomDrawRowIndicator);
            this.gridView1.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gridView1_FocusedColumnChanged);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            // 
            // MFWField
            // 
            this.MFWField.Caption = "ERP字段";
            this.MFWField.ColumnEdit = this.MFWFieldrepos;
            this.MFWField.FieldName = "FWField";
            this.MFWField.MinWidth = 17;
            this.MFWField.Name = "MFWField";
            this.MFWField.Visible = true;
            this.MFWField.VisibleIndex = 0;
            this.MFWField.Width = 62;
            // 
            // MFWFieldDesc
            // 
            this.MFWFieldDesc.Caption = "ERP字段描述";
            this.MFWFieldDesc.FieldName = "FWFieldDesc";
            this.MFWFieldDesc.MinWidth = 17;
            this.MFWFieldDesc.Name = "MFWFieldDesc";
            this.MFWFieldDesc.Visible = true;
            this.MFWFieldDesc.VisibleIndex = 1;
            this.MFWFieldDesc.Width = 70;
            // 
            // MSAPDI
            // 
            this.MSAPDI.Caption = "SAPDI属性";
            this.MSAPDI.ColumnEdit = this.MSAPDIrepos;
            this.MSAPDI.FieldName = "SAPDI";
            this.MSAPDI.MinWidth = 17;
            this.MSAPDI.Name = "MSAPDI";
            this.MSAPDI.Visible = true;
            this.MSAPDI.VisibleIndex = 2;
            this.MSAPDI.Width = 66;
            // 
            // MSAPDIDescr
            // 
            this.MSAPDIDescr.Caption = "SAPDI属性描述";
            this.MSAPDIDescr.FieldName = "SAPDIDesc";
            this.MSAPDIDescr.MinWidth = 17;
            this.MSAPDIDescr.Name = "MSAPDIDescr";
            this.MSAPDIDescr.Visible = true;
            this.MSAPDIDescr.VisibleIndex = 3;
            this.MSAPDIDescr.Width = 93;
            // 
            // MFWDataType
            // 
            this.MFWDataType.Caption = "ERP数据类型";
            this.MFWDataType.ColumnEdit = this.repositoryItemComboBox3;
            this.MFWDataType.FieldName = "FWDataType";
            this.MFWDataType.MinWidth = 17;
            this.MFWDataType.Name = "MFWDataType";
            this.MFWDataType.Visible = true;
            this.MFWDataType.VisibleIndex = 4;
            this.MFWDataType.Width = 80;
            // 
            // MKeyField
            // 
            this.MKeyField.Caption = "主键";
            this.MKeyField.ColumnEdit = this.repositoryItemCheckEdit3;
            this.MKeyField.FieldName = "KeyField";
            this.MKeyField.MinWidth = 17;
            this.MKeyField.Name = "MKeyField";
            this.MKeyField.ToolTip = "唯一值,一个规则中只有一个\\n";
            this.MKeyField.Visible = true;
            this.MKeyField.VisibleIndex = 5;
            this.MKeyField.Width = 32;
            // 
            // MIsGroup
            // 
            this.MIsGroup.Caption = "分组字段";
            this.MIsGroup.ColumnEdit = this.repositoryItemCheckEdit3;
            this.MIsGroup.FieldName = "IsGroup";
            this.MIsGroup.MinWidth = 17;
            this.MIsGroup.Name = "MIsGroup";
            this.MIsGroup.ToolTip = "汇闷导入时的Group by字段";
            this.MIsGroup.Visible = true;
            this.MIsGroup.VisibleIndex = 6;
            this.MIsGroup.Width = 51;
            // 
            // MMemo
            // 
            this.MMemo.Caption = "主数据编码前缀";
            this.MMemo.FieldName = "CodePrefix";
            this.MMemo.MinWidth = 17;
            this.MMemo.Name = "MMemo";
            this.MMemo.ToolTip = "用于在值内容前面增加固定内容,比如:仓库编码为001,而在SAP中为W001,则此字段填写W";
            this.MMemo.Visible = true;
            this.MMemo.VisibleIndex = 7;
            this.MMemo.Width = 85;
            // 
            // SubTable
            // 
            this.SubTable.AutoSize = true;
            this.SubTable.Caption = "子表";
            this.SubTable.Controls.Add(this.memoEdit2);
            this.SubTable.Controls.Add(this.gridControl2);
            this.SubTable.Name = "SubTable";
            this.SubTable.Size = new System.Drawing.Size(864, 300);
            // 
            // memoEdit2
            // 
            this.memoEdit2.Location = new System.Drawing.Point(393, 70);
            this.memoEdit2.MenuManager = this.barManager1;
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Size = new System.Drawing.Size(267, 214);
            this.memoEdit2.TabIndex = 2;
            this.memoEdit2.Visible = false;
            this.memoEdit2.Leave += new System.EventHandler(this.memoEdit2_Leave);
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox11,
            this.repositoryItemPopupContainerEdit4,
            this.repositoryItemButtonEdit2,
            this.repositoryItemComboBox10,
            this.repositoryItemComboBox6,
            this.SFWFieldrepos,
            this.SSAPDIrepos,
            this.repositoryItemGridLookUpEdit6,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemCheckEdit7});
            this.gridControl2.Size = new System.Drawing.Size(864, 300);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridControl2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gridControl2_MouseClick);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SFWField,
            this.FWFieldDesc,
            this.SSAPDI,
            this.SSAPDIDesc,
            this.FWDataType,
            this.SKeyField,
            this.isDebit,
            this.SIsGroup,
            this.IsSum,
            this.IsMasTbl});
            this.gridView2.DetailHeight = 325;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView2_RowClick);
            this.gridView2.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gridView2_CustomDrawRowIndicator);
            this.gridView2.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gridView2_FocusedColumnChanged);
            this.gridView2.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView2_CellValueChanged);
            // 
            // SFWField
            // 
            this.SFWField.Caption = "ERP字段";
            this.SFWField.ColumnEdit = this.SFWFieldrepos;
            this.SFWField.FieldName = "FWField";
            this.SFWField.MinWidth = 17;
            this.SFWField.Name = "SFWField";
            this.SFWField.Visible = true;
            this.SFWField.VisibleIndex = 0;
            this.SFWField.Width = 78;
            // 
            // SFWFieldrepos
            // 
            this.SFWFieldrepos.AutoHeight = false;
            this.SFWFieldrepos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SFWFieldrepos.ImmediatePopup = true;
            this.SFWFieldrepos.Name = "SFWFieldrepos";
            this.SFWFieldrepos.NullText = "";
            this.SFWFieldrepos.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.SFWFieldrepos.PopupView = this.gridView7;
            this.SFWFieldrepos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // gridView7
            // 
            this.gridView7.DetailHeight = 325;
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // FWFieldDesc
            // 
            this.FWFieldDesc.Caption = "ERP字段描述";
            this.FWFieldDesc.FieldName = "FWFieldDesc";
            this.FWFieldDesc.MinWidth = 17;
            this.FWFieldDesc.Name = "FWFieldDesc";
            this.FWFieldDesc.Visible = true;
            this.FWFieldDesc.VisibleIndex = 1;
            this.FWFieldDesc.Width = 85;
            // 
            // SSAPDI
            // 
            this.SSAPDI.Caption = "SAPDI属性";
            this.SSAPDI.ColumnEdit = this.SSAPDIrepos;
            this.SSAPDI.FieldName = "SAPDI";
            this.SSAPDI.MinWidth = 17;
            this.SSAPDI.Name = "SSAPDI";
            this.SSAPDI.Visible = true;
            this.SSAPDI.VisibleIndex = 2;
            this.SSAPDI.Width = 74;
            // 
            // SSAPDIrepos
            // 
            this.SSAPDIrepos.AutoHeight = false;
            this.SSAPDIrepos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SSAPDIrepos.ImmediatePopup = true;
            this.SSAPDIrepos.Name = "SSAPDIrepos";
            this.SSAPDIrepos.NullText = "";
            this.SSAPDIrepos.PopupView = this.gridView9;
            this.SSAPDIrepos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // gridView9
            // 
            this.gridView9.DetailHeight = 325;
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            // 
            // SSAPDIDesc
            // 
            this.SSAPDIDesc.Caption = "SAPDI属性描述";
            this.SSAPDIDesc.FieldName = "SAPDIDesc";
            this.SSAPDIDesc.MinWidth = 17;
            this.SSAPDIDesc.Name = "SSAPDIDesc";
            this.SSAPDIDesc.Visible = true;
            this.SSAPDIDesc.VisibleIndex = 3;
            this.SSAPDIDesc.Width = 100;
            // 
            // FWDataType
            // 
            this.FWDataType.Caption = "ERP数据类型";
            this.FWDataType.ColumnEdit = this.repositoryItemComboBox6;
            this.FWDataType.FieldName = "FWDataType";
            this.FWDataType.MinWidth = 17;
            this.FWDataType.Name = "FWDataType";
            this.FWDataType.Visible = true;
            this.FWDataType.VisibleIndex = 4;
            this.FWDataType.Width = 93;
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.Items.AddRange(new object[] {
            "数值",
            "店铺",
            "仓库",
            "供应商",
            "客户",
            "商品主数据",
            "固定值"});
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // SKeyField
            // 
            this.SKeyField.Caption = "主表关联字段";
            this.SKeyField.ColumnEdit = this.repositoryItemCheckEdit2;
            this.SKeyField.FieldName = "KeyField";
            this.SKeyField.MinWidth = 17;
            this.SKeyField.Name = "SKeyField";
            this.SKeyField.ToolTip = "选中后,此字段不导入SAP,如有相同信息要导入,则须要在配置一行";
            this.SKeyField.Visible = true;
            this.SKeyField.VisibleIndex = 6;
            this.SKeyField.Width = 99;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit2.ValueChecked = "Y";
            this.repositoryItemCheckEdit2.ValueGrayed = "N";
            this.repositoryItemCheckEdit2.ValueUnchecked = "N";
            // 
            // isDebit
            // 
            this.isDebit.Caption = "借方";
            this.isDebit.ColumnEdit = this.repositoryItemCheckEdit7;
            this.isDebit.FieldName = "isDebit";
            this.isDebit.Name = "isDebit";
            this.isDebit.ToolTip = "数据借借贷方，借方选中，贷方不选";
            this.isDebit.Visible = true;
            this.isDebit.VisibleIndex = 5;
            this.isDebit.Width = 40;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit7.NullText = "N";
            this.repositoryItemCheckEdit7.ValueChecked = "Y";
            this.repositoryItemCheckEdit7.ValueGrayed = "N";
            this.repositoryItemCheckEdit7.ValueUnchecked = "N";
            // 
            // SIsGroup
            // 
            this.SIsGroup.Caption = "分组字段";
            this.SIsGroup.ColumnEdit = this.repositoryItemCheckEdit5;
            this.SIsGroup.FieldName = "IsGroup";
            this.SIsGroup.MinWidth = 17;
            this.SIsGroup.Name = "SIsGroup";
            this.SIsGroup.ToolTip = "合并导入时的Group By字段";
            this.SIsGroup.Visible = true;
            this.SIsGroup.VisibleIndex = 7;
            this.SIsGroup.Width = 70;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit5.ValueChecked = "Y";
            this.repositoryItemCheckEdit5.ValueGrayed = "N";
            this.repositoryItemCheckEdit5.ValueUnchecked = "N";
            // 
            // IsSum
            // 
            this.IsSum.Caption = "汇总字段";
            this.IsSum.ColumnEdit = this.repositoryItemCheckEdit1;
            this.IsSum.FieldName = "IsSum";
            this.IsSum.MinWidth = 17;
            this.IsSum.Name = "IsSum";
            this.IsSum.ToolTip = "求和字段,选中后会加SUM";
            this.IsSum.Visible = true;
            this.IsSum.VisibleIndex = 8;
            this.IsSum.Width = 99;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = "Y";
            this.repositoryItemCheckEdit1.ValueGrayed = "N";
            this.repositoryItemCheckEdit1.ValueUnchecked = "N";
            // 
            // IsMasTbl
            // 
            this.IsMasTbl.Caption = "主表字段";
            this.IsMasTbl.ColumnEdit = this.repositoryItemCheckEdit6;
            this.IsMasTbl.FieldName = "IsMasTbl";
            this.IsMasTbl.MinWidth = 17;
            this.IsMasTbl.Name = "IsMasTbl";
            this.IsMasTbl.Visible = true;
            this.IsMasTbl.VisibleIndex = 9;
            this.IsMasTbl.Width = 101;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit6.NullText = "N";
            this.repositoryItemCheckEdit6.ValueChecked = "Y";
            this.repositoryItemCheckEdit6.ValueGrayed = "N";
            this.repositoryItemCheckEdit6.ValueUnchecked = "N";
            // 
            // repositoryItemComboBox11
            // 
            this.repositoryItemComboBox11.AutoHeight = false;
            this.repositoryItemComboBox11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox11.Name = "repositoryItemComboBox11";
            // 
            // repositoryItemPopupContainerEdit4
            // 
            this.repositoryItemPopupContainerEdit4.AutoHeight = false;
            this.repositoryItemPopupContainerEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit4.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.repositoryItemPopupContainerEdit4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemPopupContainerEdit4.Name = "repositoryItemPopupContainerEdit4";
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            // 
            // repositoryItemComboBox10
            // 
            this.repositoryItemComboBox10.AutoHeight = false;
            this.repositoryItemComboBox10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox10.Items.AddRange(new object[] {
            "数值",
            "自定义字段"});
            this.repositoryItemComboBox10.Name = "repositoryItemComboBox10";
            // 
            // repositoryItemGridLookUpEdit6
            // 
            this.repositoryItemGridLookUpEdit6.AutoHeight = false;
            this.repositoryItemGridLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit6.Name = "repositoryItemGridLookUpEdit6";
            this.repositoryItemGridLookUpEdit6.PopupView = this.gridView6;
            // 
            // gridView6
            // 
            this.gridView6.DetailHeight = 325;
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // SerTable
            // 
            this.SerTable.AutoSize = true;
            this.SerTable.Caption = "序列号表";
            this.SerTable.Controls.Add(this.gridControl3);
            this.SerTable.Name = "SerTable";
            this.SerTable.Size = new System.Drawing.Size(864, 300);
            // 
            // gridControl3
            // 
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox9,
            this.repositoryItemPopupContainerEdit3,
            this.repositoryItemButtonEdit3,
            this.repositoryItemComboBox8,
            this.repositoryItemComboBox7,
            this.TFWFieldrepos,
            this.TSAPDIrepos,
            this.repositoryItemGridLookUpEdit7,
            this.repositoryItemCheckEdit4});
            this.gridControl3.Size = new System.Drawing.Size(864, 300);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            this.gridControl3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gridControl3_MouseClick);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TFWField,
            this.TFWFieldDesc,
            this.TSAPDI,
            this.TSAPDIDesc,
            this.TFWDataType,
            this.TKeyField});
            this.gridView3.DetailHeight = 325;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gridView3_CustomDrawRowIndicator);
            this.gridView3.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.gridView3_FocusedColumnChanged);
            this.gridView3.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView3_CellValueChanged);
            // 
            // TFWField
            // 
            this.TFWField.Caption = "ERP字段";
            this.TFWField.ColumnEdit = this.TFWFieldrepos;
            this.TFWField.FieldName = "FWField";
            this.TFWField.MinWidth = 17;
            this.TFWField.Name = "TFWField";
            this.TFWField.Visible = true;
            this.TFWField.VisibleIndex = 0;
            this.TFWField.Width = 69;
            // 
            // TFWFieldrepos
            // 
            this.TFWFieldrepos.AccessibleDescription = "";
            this.TFWFieldrepos.AutoHeight = false;
            this.TFWFieldrepos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TFWFieldrepos.ImmediatePopup = true;
            this.TFWFieldrepos.Name = "TFWFieldrepos";
            this.TFWFieldrepos.NullText = "";
            this.TFWFieldrepos.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.TFWFieldrepos.PopupView = this.gridView5;
            this.TFWFieldrepos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // gridView5
            // 
            this.gridView5.DetailHeight = 325;
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.ViewCaption = "abc";
            // 
            // TFWFieldDesc
            // 
            this.TFWFieldDesc.Caption = "ERP字段描述";
            this.TFWFieldDesc.FieldName = "FWFieldDesc";
            this.TFWFieldDesc.MinWidth = 17;
            this.TFWFieldDesc.Name = "TFWFieldDesc";
            this.TFWFieldDesc.Visible = true;
            this.TFWFieldDesc.VisibleIndex = 1;
            this.TFWFieldDesc.Width = 76;
            // 
            // TSAPDI
            // 
            this.TSAPDI.Caption = "SAPDI属性";
            this.TSAPDI.ColumnEdit = this.TSAPDIrepos;
            this.TSAPDI.FieldName = "SAPDI";
            this.TSAPDI.MinWidth = 17;
            this.TSAPDI.Name = "TSAPDI";
            this.TSAPDI.Visible = true;
            this.TSAPDI.VisibleIndex = 2;
            this.TSAPDI.Width = 61;
            // 
            // TSAPDIrepos
            // 
            this.TSAPDIrepos.AutoHeight = false;
            this.TSAPDIrepos.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TSAPDIrepos.ImmediatePopup = true;
            this.TSAPDIrepos.Name = "TSAPDIrepos";
            this.TSAPDIrepos.NullText = "";
            this.TSAPDIrepos.PopupView = this.gridView8;
            this.TSAPDIrepos.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            // 
            // gridView8
            // 
            this.gridView8.DetailHeight = 325;
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // TSAPDIDesc
            // 
            this.TSAPDIDesc.Caption = "SAPDI属性描述";
            this.TSAPDIDesc.FieldName = "SAPDIDesc";
            this.TSAPDIDesc.MinWidth = 17;
            this.TSAPDIDesc.Name = "TSAPDIDesc";
            this.TSAPDIDesc.Visible = true;
            this.TSAPDIDesc.VisibleIndex = 3;
            this.TSAPDIDesc.Width = 106;
            // 
            // TFWDataType
            // 
            this.TFWDataType.Caption = "ERP数据类型";
            this.TFWDataType.ColumnEdit = this.repositoryItemComboBox7;
            this.TFWDataType.FieldName = "FWDataType";
            this.TFWDataType.MinWidth = 17;
            this.TFWDataType.Name = "TFWDataType";
            this.TFWDataType.Visible = true;
            this.TFWDataType.VisibleIndex = 4;
            this.TFWDataType.Width = 69;
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox7.Items.AddRange(new object[] {
            "数值",
            "店铺",
            "仓库",
            "供应商",
            "客户",
            "商品主数据",
            "固定值"});
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            // 
            // TKeyField
            // 
            this.TKeyField.Caption = "子表关联字段";
            this.TKeyField.ColumnEdit = this.repositoryItemCheckEdit4;
            this.TKeyField.FieldName = "KeyField";
            this.TKeyField.MinWidth = 17;
            this.TKeyField.Name = "TKeyField";
            this.TKeyField.ToolTip = "选中后不导入SAP,只用于与子表的关联";
            this.TKeyField.Visible = true;
            this.TKeyField.VisibleIndex = 5;
            this.TKeyField.Width = 64;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit4.NullText = "N";
            this.repositoryItemCheckEdit4.ValueChecked = "Y";
            this.repositoryItemCheckEdit4.ValueGrayed = "N";
            this.repositoryItemCheckEdit4.ValueUnchecked = "N";
            // 
            // repositoryItemComboBox9
            // 
            this.repositoryItemComboBox9.AutoHeight = false;
            this.repositoryItemComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox9.Name = "repositoryItemComboBox9";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.repositoryItemPopupContainerEdit3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox8.Items.AddRange(new object[] {
            "数值",
            "自定义字段"});
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            // 
            // repositoryItemGridLookUpEdit7
            // 
            this.repositoryItemGridLookUpEdit7.AutoHeight = false;
            this.repositoryItemGridLookUpEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit7.Name = "repositoryItemGridLookUpEdit7";
            this.repositoryItemGridLookUpEdit7.PopupView = this.gridView10;
            // 
            // gridView10
            // 
            this.gridView10.DetailHeight = 325;
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            // 
            // listBoxControl1
            // 
            this.listBoxControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxControl1.HorizontalScrollbar = true;
            this.listBoxControl1.Location = new System.Drawing.Point(3, 85);
            this.listBoxControl1.MultiColumn = true;
            this.listBoxControl1.Name = "listBoxControl1";
            this.listBoxControl1.Size = new System.Drawing.Size(161, 337);
            this.listBoxControl1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxControl1.TabIndex = 8;
            this.listBoxControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBoxControl1_MouseClick);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(10, 67);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(84, 13);
            this.labelControl5.TabIndex = 9;
            this.labelControl5.Text = "已配置同步逻辑";
            // 
            // saveOK
            // 
            this.saveOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveOK.Location = new System.Drawing.Point(251, 436);
            this.saveOK.Name = "saveOK";
            this.saveOK.Size = new System.Drawing.Size(64, 21);
            this.saveOK.TabIndex = 10;
            this.saveOK.Text = "保存";
            this.saveOK.Click += new System.EventHandler(this.saveOK_Click);
            // 
            // CancelOK
            // 
            this.CancelOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelOK.Location = new System.Drawing.Point(409, 436);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(64, 21);
            this.CancelOK.TabIndex = 11;
            this.CancelOK.Text = "关闭";
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // ERPobject
            // 
            this.ERPobject.Location = new System.Drawing.Point(80, 11);
            this.ERPobject.Name = "ERPobject";
            this.ERPobject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ERPobject.Properties.ImmediatePopup = true;
            this.ERPobject.Properties.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.ERPobject.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ERPobject.Properties.NullText = "";
            this.ERPobject.Properties.PopupView = this.gridLookUpEdit1View;
            this.ERPobject.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.ERPobject.Properties.ValidateOnEnterKey = true;
            this.ERPobject.Size = new System.Drawing.Size(97, 20);
            this.ERPobject.TabIndex = 1;
            this.ERPobject.EditValueChanged += new System.EventHandler(this.ERPobject_EditValueChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.DetailHeight = 325;
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // SAPobject
            // 
            this.SAPobject.Location = new System.Drawing.Point(231, 11);
            this.SAPobject.Name = "SAPobject";
            this.SAPobject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SAPobject.Properties.ImmediatePopup = true;
            this.SAPobject.Properties.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.SAPobject.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SAPobject.Properties.NullText = "";
            this.SAPobject.Properties.PopupSizeable = false;
            this.SAPobject.Properties.PopupView = this.gridView4;
            this.SAPobject.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SAPobject.Size = new System.Drawing.Size(97, 20);
            this.SAPobject.TabIndex = 3;
            this.SAPobject.EditValueChanged += new System.EventHandler(this.SAPobject_EditValueChanged);
            // 
            // gridView4
            // 
            this.gridView4.DetailHeight = 325;
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(144, 55);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(64, 21);
            this.simpleButton1.TabIndex = 16;
            this.simpleButton1.Text = "新增";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // popupMenu2
            // 
            this.popupMenu2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.popupMenu2.Manager = this.barManager1;
            this.popupMenu2.Name = "popupMenu2";
            // 
            // GroupType
            // 
            this.GroupType.EditValue = 0;
            this.GroupType.Location = new System.Drawing.Point(409, 9);
            this.GroupType.MenuManager = this.barManager1;
            this.GroupType.Name = "GroupType";
            this.GroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "按天"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "按月", true, "N")});
            this.GroupType.Size = new System.Drawing.Size(89, 24);
            this.GroupType.TabIndex = 21;
            this.GroupType.ToolTip = "选择按天还是按月汇总";
            this.GroupType.Visible = false;
            // 
            // IsGroup
            // 
            this.IsGroup.Location = new System.Drawing.Point(344, 12);
            this.IsGroup.MenuManager = this.barManager1;
            this.IsGroup.Name = "IsGroup";
            this.IsGroup.Properties.Caption = "汇总导入";
            this.IsGroup.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.IsGroup.Properties.NullText = "N";
            this.IsGroup.Properties.ValueChecked = "Y";
            this.IsGroup.Properties.ValueGrayed = "N";
            this.IsGroup.Properties.ValueUnchecked = "N";
            this.IsGroup.Size = new System.Drawing.Size(64, 18);
            this.IsGroup.TabIndex = 22;
            this.IsGroup.ToolTip = "不选:按ERP中的单据导入,一对一\r\n选中:\r\n   模式:按天: 一个业务伙伴一天的数据导在一张单上\r\n         按月:一个业务伙伴一个月的数据导在一张" +
    "单上\r\n  合并行:在上面的基础上,将物料相同的行合并在一起导入\r\n\r\n注:由于采用个别计价管理,入库类单据不能设置为合并行方\r\n   式导入,因为每个序列号的" +
    "价格可能不一样";
            this.IsGroup.Visible = false;
            this.IsGroup.CheckStateChanged += new System.EventHandler(this.checkEdit1_CheckStateChanged);
            this.IsGroup.Click += new System.EventHandler(this.IsGroup_Click);
            // 
            // Collect
            // 
            this.Collect.Location = new System.Drawing.Point(513, 12);
            this.Collect.MenuManager = this.barManager1;
            this.Collect.Name = "Collect";
            this.Collect.Properties.Caption = "合并行";
            this.Collect.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.Collect.Properties.NullText = "N";
            this.Collect.Properties.ValueChecked = "Y";
            this.Collect.Properties.ValueGrayed = "N";
            this.Collect.Properties.ValueUnchecked = "N";
            this.Collect.Size = new System.Drawing.Size(64, 18);
            this.Collect.TabIndex = 27;
            this.Collect.ToolTip = "相同物料的商品汇总成一行";
            this.Collect.Visible = false;
            // 
            // checkCost
            // 
            this.checkCost.EditValue = DevExpress.Data.UnboundColumnType.Integer;
            this.checkCost.Location = new System.Drawing.Point(602, 32);
            this.checkCost.MenuManager = this.barManager1;
            this.checkCost.Name = "checkCost";
            this.checkCost.Properties.Caption = "校验成本";
            this.checkCost.Properties.DisplayValueChecked = "Y";
            this.checkCost.Properties.DisplayValueGrayed = "N";
            this.checkCost.Properties.DisplayValueUnchecked = "N";
            this.checkCost.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.checkCost.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.checkCost.Properties.NullText = "N";
            this.checkCost.Properties.Tag = "";
            this.checkCost.Properties.ValueChecked = "Y";
            this.checkCost.Properties.ValueGrayed = "N";
            this.checkCost.Properties.ValueUnchecked = "N";
            this.checkCost.Size = new System.Drawing.Size(79, 20);
            this.checkCost.TabIndex = 32;
            this.checkCost.ToolTip = "采购到货和其他入库单校验单价与成本价是否一致,不一致不同步!";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(687, 13);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(52, 13);
            this.labelControl6.TabIndex = 37;
            this.labelControl6.Text = "同步顺序:";
            // 
            // ToOrder
            // 
            this.ToOrder.EditValue = 0;
            this.ToOrder.Location = new System.Drawing.Point(736, 10);
            this.ToOrder.MenuManager = this.barManager1;
            this.ToOrder.Name = "ToOrder";
            this.ToOrder.Size = new System.Drawing.Size(33, 20);
            this.ToOrder.TabIndex = 38;
            // 
            // SAPCost
            // 
            this.SAPCost.EditValue = DevExpress.Data.UnboundColumnType.Integer;
            this.SAPCost.Location = new System.Drawing.Point(602, 8);
            this.SAPCost.MenuManager = this.barManager1;
            this.SAPCost.Name = "SAPCost";
            this.SAPCost.Properties.Caption = "取SAP成本";
            this.SAPCost.Properties.DisplayValueChecked = "Y";
            this.SAPCost.Properties.DisplayValueGrayed = "N";
            this.SAPCost.Properties.DisplayValueUnchecked = "N";
            this.SAPCost.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.SAPCost.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.SAPCost.Properties.NullText = "N";
            this.SAPCost.Properties.Tag = "";
            this.SAPCost.Properties.ValueChecked = "Y";
            this.SAPCost.Properties.ValueGrayed = "N";
            this.SAPCost.Properties.ValueUnchecked = "N";
            this.SAPCost.Size = new System.Drawing.Size(75, 20);
            this.SAPCost.TabIndex = 43;
            this.SAPCost.ToolTip = "选中整单读取SAP中物料的成本价,行中使用combfield,使用函数ToSAP_GetSAPCost,\r\n传参数,单据类型,单据的Docentry,ItemEn" +
    "try 读取,成本为合计,不是单价";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton2.Location = new System.Drawing.Point(545, 436);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(64, 21);
            this.simpleButton2.TabIndex = 48;
            this.simpleButton2.Text = "复制配置";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(816, 9);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(27, 13);
            this.labelControl7.TabIndex = 54;
            this.labelControl7.Text = "备注 ";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(845, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(174, 77);
            this.richTextBox1.TabIndex = 55;
            this.richTextBox1.Text = "";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(687, 32);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(48, 13);
            this.labelControl8.TabIndex = 60;
            this.labelControl8.Text = "单据状态";
            // 
            // DocStatus
            // 
            this.DocStatus.Location = new System.Drawing.Point(736, 32);
            this.DocStatus.MenuManager = this.barManager1;
            this.DocStatus.Name = "DocStatus";
            this.DocStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DocStatus.Properties.Items.AddRange(new object[] {
            "保存",
            "审核",
            "复核"});
            this.DocStatus.Properties.NullText = "审核";
            this.DocStatus.Size = new System.Drawing.Size(49, 20);
            this.DocStatus.TabIndex = 61;
            // 
            // isDraft
            // 
            this.isDraft.EditValue = DevExpress.Data.UnboundColumnType.Integer;
            this.isDraft.Location = new System.Drawing.Point(602, 52);
            this.isDraft.MenuManager = this.barManager1;
            this.isDraft.Name = "isDraft";
            this.isDraft.Properties.Caption = "生成草稿";
            this.isDraft.Properties.DisplayValueChecked = "Y";
            this.isDraft.Properties.DisplayValueGrayed = "N";
            this.isDraft.Properties.DisplayValueUnchecked = "N";
            this.isDraft.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.isDraft.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.isDraft.Properties.NullText = "N";
            this.isDraft.Properties.Tag = "";
            this.isDraft.Properties.ValueChecked = "Y";
            this.isDraft.Properties.ValueGrayed = "N";
            this.isDraft.Properties.ValueUnchecked = "N";
            this.isDraft.Size = new System.Drawing.Size(75, 20);
            this.isDraft.TabIndex = 66;
            this.isDraft.ToolTip = "采购到货和其他入库单校验单价与成本价是否一致,不一致不同步!";
            // 
            // FWToSAPSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1036, 472);
            this.Controls.Add(this.isDraft);
            this.Controls.Add(this.DocStatus);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.SAPCost);
            this.Controls.Add(this.ToOrder);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.checkCost);
            this.Controls.Add(this.Collect);
            this.Controls.Add(this.IsGroup);
            this.Controls.Add(this.GroupType);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.CancelOK);
            this.Controls.Add(this.saveOK);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.listBoxControl1);
            this.Controls.Add(this.Tabpane１);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.AttCon);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.ERPobject);
            this.Controls.Add(this.SAPobject);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FWToSAPSet";
            this.Text = "同步逻辑设置";
            this.Load += new System.EventHandler(this.FWToSAPSet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MFWFieldrepos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MSAPDIrepos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttCon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tabpane１)).EndInit();
            this.Tabpane１.ResumeLayout(false);
            this.Tabpane１.PerformLayout();
            this.MasTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.SubTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SFWFieldrepos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSAPDIrepos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.SerTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TFWFieldrepos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TSAPDIrepos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERPobject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPobject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Collect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDraft.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit AttCon;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraBars.Navigation.TabPane Tabpane１;
        private DevExpress.XtraBars.Navigation.TabNavigationPage MasTable;
        private DevExpress.XtraBars.Navigation.TabNavigationPage SubTable;
        private DevExpress.XtraBars.Navigation.TabNavigationPage SerTable;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl1;
        private DevExpress.XtraGrid.GridControl GridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn MFWField;
        private DevExpress.XtraGrid.Columns.GridColumn MFWFieldDesc;
        private DevExpress.XtraGrid.Columns.GridColumn MSAPDI;
        private DevExpress.XtraGrid.Columns.GridColumn MSAPDIDescr;
        private DevExpress.XtraGrid.Columns.GridColumn MFWDataType;
        private DevExpress.XtraGrid.Columns.GridColumn MMemo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton saveOK;
        private DevExpress.XtraEditors.SimpleButton CancelOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.GridLookUpEdit ERPobject;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit SAPobject;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn SFWField;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit SFWFieldrepos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn FWFieldDesc;
        private DevExpress.XtraGrid.Columns.GridColumn SSAPDI;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit SSAPDIrepos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn SSAPDIDesc;
        private DevExpress.XtraGrid.Columns.GridColumn FWDataType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox10;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox11;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn TFWField;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit TFWFieldrepos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn TFWFieldDesc;
        private DevExpress.XtraGrid.Columns.GridColumn TSAPDI;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit TSAPDIrepos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn TSAPDIDesc;
        private DevExpress.XtraGrid.Columns.GridColumn TFWDataType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox9;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.PopupMenu popupMenu2;
        private DevExpress.XtraGrid.Columns.GridColumn IsSum;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.CheckEdit IsGroup;
        private DevExpress.XtraEditors.RadioGroup GroupType;
        private DevExpress.XtraGrid.Columns.GridColumn MKeyField;
        private DevExpress.XtraGrid.Columns.GridColumn SKeyField;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn TKeyField;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn MIsGroup;
        private DevExpress.XtraGrid.Columns.GridColumn SIsGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit MFWFieldrepos;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit MSAPDIrepos;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.CheckEdit Collect;
        private DevExpress.XtraGrid.Columns.GridColumn IsMasTbl;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.CheckEdit checkCost;
        private DevExpress.XtraEditors.TextEdit ToOrder;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit SAPCost;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl2;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit DocStatus;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit isDraft;
        private DevExpress.XtraGrid.Columns.GridColumn isDebit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
    }
}
﻿namespace ToSAP.Init
{
    partial class AppInit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppInit));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.InterFaceMode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Corp = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Shop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SAPDB = new DevExpress.XtraEditors.LabelControl();
            this.CorpLable = new DevExpress.XtraEditors.LabelControl();
            this.CorpSubName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.saveOK = new System.Windows.Forms.Button();
            this.CancelOK = new System.Windows.Forms.Button();
            this.SAPFIDB = new DevExpress.XtraEditors.TextEdit();
            this.CorpSub = new DevExpress.XtraEditors.TextEdit();
            this.showOWHS = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.DocToSAP = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.oLDMat = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.ErrOP = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sqlversion = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.InterFaceMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Corp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Shop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CorpSubName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPFIDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CorpSub.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showOWHS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocToSAP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oLDMat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sqlversion.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(52, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "接口模式:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(436, 16);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "集团管理:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 52);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(76, 14);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "门店单独管理:";
            // 
            // InterFaceMode
            // 
            this.InterFaceMode.EditValue = "按分支";
            this.InterFaceMode.Location = new System.Drawing.Point(101, 16);
            this.InterFaceMode.Name = "InterFaceMode";
            this.InterFaceMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InterFaceMode.Properties.Items.AddRange(new object[] {
            "按公司",
            "按分支"});
            this.InterFaceMode.Size = new System.Drawing.Size(100, 20);
            this.InterFaceMode.TabIndex = 3;
            this.InterFaceMode.SelectedValueChanged += new System.EventHandler(this.InterFaceMode_SelectedValueChanged);
            // 
            // Corp
            // 
            this.Corp.EditValue = "是";
            this.Corp.Location = new System.Drawing.Point(516, 13);
            this.Corp.Name = "Corp";
            this.Corp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Corp.Properties.Items.AddRange(new object[] {
            "是",
            "否"});
            this.Corp.Size = new System.Drawing.Size(100, 20);
            this.Corp.TabIndex = 4;
            this.Corp.SelectedValueChanged += new System.EventHandler(this.Corp_SelectedValueChanged);
            // 
            // Shop
            // 
            this.Shop.EditValue = "否";
            this.Shop.Location = new System.Drawing.Point(100, 49);
            this.Shop.Name = "Shop";
            this.Shop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Shop.Properties.Items.AddRange(new object[] {
            "是",
            "否"});
            this.Shop.Size = new System.Drawing.Size(100, 20);
            this.Shop.TabIndex = 5;
            // 
            // SAPDB
            // 
            this.SAPDB.Location = new System.Drawing.Point(234, 22);
            this.SAPDB.Name = "SAPDB";
            this.SAPDB.Size = new System.Drawing.Size(62, 14);
            this.SAPDB.TabIndex = 6;
            this.SAPDB.Text = "SAP数据库:";
            // 
            // CorpLable
            // 
            this.CorpLable.Location = new System.Drawing.Point(636, 19);
            this.CorpLable.Name = "CorpLable";
            this.CorpLable.Size = new System.Drawing.Size(52, 14);
            this.CorpLable.TabIndex = 8;
            this.CorpLable.Text = "集团分支:";
            // 
            // CorpSubName
            // 
            this.CorpSubName.Location = new System.Drawing.Point(775, 19);
            this.CorpSubName.Name = "CorpSubName";
            this.CorpSubName.Size = new System.Drawing.Size(107, 20);
            this.CorpSubName.TabIndex = 10;
            // 
            // labelControl5
            // 
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(36, 233);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(846, 169);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = resources.GetString("labelControl5.Text");
            // 
            // saveOK
            // 
            this.saveOK.Location = new System.Drawing.Point(208, 172);
            this.saveOK.Name = "saveOK";
            this.saveOK.Size = new System.Drawing.Size(75, 23);
            this.saveOK.TabIndex = 12;
            this.saveOK.Text = "保存";
            this.saveOK.UseVisualStyleBackColor = true;
            this.saveOK.Click += new System.EventHandler(this.saveOK_Click);
            // 
            // CancelOK
            // 
            this.CancelOK.Location = new System.Drawing.Point(370, 172);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(75, 23);
            this.CancelOK.TabIndex = 13;
            this.CancelOK.Text = "关闭";
            this.CancelOK.UseVisualStyleBackColor = true;
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // SAPFIDB
            // 
            this.SAPFIDB.Location = new System.Drawing.Point(303, 16);
            this.SAPFIDB.Name = "SAPFIDB";
            this.SAPFIDB.Size = new System.Drawing.Size(100, 20);
            this.SAPFIDB.TabIndex = 7;
            // 
            // CorpSub
            // 
            this.CorpSub.Location = new System.Drawing.Point(694, 19);
            this.CorpSub.Name = "CorpSub";
            this.CorpSub.Size = new System.Drawing.Size(75, 20);
            this.CorpSub.TabIndex = 9;
            // 
            // showOWHS
            // 
            this.showOWHS.EditValue = "店铺";
            this.showOWHS.Location = new System.Drawing.Point(306, 52);
            this.showOWHS.Name = "showOWHS";
            this.showOWHS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.showOWHS.Properties.Items.AddRange(new object[] {
            "店铺",
            "仓库",
            "店铺和仓库"});
            this.showOWHS.Size = new System.Drawing.Size(100, 20);
            this.showOWHS.TabIndex = 16;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(228, 55);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(72, 28);
            this.labelControl4.TabIndex = 15;
            this.labelControl4.Text = "同步界面显示\r\n店铺/仓库:";
            // 
            // DocToSAP
            // 
            this.DocToSAP.EditValue = "零售单";
            this.DocToSAP.Location = new System.Drawing.Point(522, 52);
            this.DocToSAP.Name = "DocToSAP";
            this.DocToSAP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DocToSAP.Properties.Items.AddRange(new object[] {
            "零售单",
            "所有单",
            "选择单据",
            "否"});
            this.DocToSAP.Size = new System.Drawing.Size(94, 20);
            this.DocToSAP.TabIndex = 18;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(434, 55);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(88, 14);
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "单据单统一同步:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(516, 174);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 19;
            this.simpleButton1.Text = "初始化SAP";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // oLDMat
            // 
            this.oLDMat.EditValue = "销售退货模式";
            this.oLDMat.Location = new System.Drawing.Point(744, 52);
            this.oLDMat.Name = "oLDMat";
            this.oLDMat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.oLDMat.Properties.Items.AddRange(new object[] {
            "销售退货模式",
            "采购模式"});
            this.oLDMat.Size = new System.Drawing.Size(138, 20);
            this.oLDMat.TabIndex = 21;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(636, 55);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(100, 14);
            this.labelControl7.TabIndex = 20;
            this.labelControl7.Text = "旧料旧饰成本模式:";
            // 
            // ErrOP
            // 
            this.ErrOP.EditValue = "继续";
            this.ErrOP.Location = new System.Drawing.Point(99, 80);
            this.ErrOP.Name = "ErrOP";
            this.ErrOP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ErrOP.Properties.Items.AddRange(new object[] {
            "继续",
            "终止",
            "提示"});
            this.ErrOP.Size = new System.Drawing.Size(100, 20);
            this.ErrOP.TabIndex = 23;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(12, 83);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(72, 14);
            this.labelControl8.TabIndex = 22;
            this.labelControl8.Text = "同步错误处理";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Location = new System.Drawing.Point(824, 408);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 24;
            this.simpleButton2.Text = "哔哔不准动";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // sqlversion
            // 
            this.sqlversion.Location = new System.Drawing.Point(787, 230);
            this.sqlversion.Name = "sqlversion";
            this.sqlversion.Size = new System.Drawing.Size(100, 20);
            this.sqlversion.TabIndex = 25;
            this.sqlversion.Visible = false;
            // 
            // AppInit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 445);
            this.Controls.Add(this.sqlversion);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.ErrOP);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.oLDMat);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.DocToSAP);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.showOWHS);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.CancelOK);
            this.Controls.Add(this.saveOK);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.CorpSubName);
            this.Controls.Add(this.CorpLable);
            this.Controls.Add(this.SAPDB);
            this.Controls.Add(this.Shop);
            this.Controls.Add(this.Corp);
            this.Controls.Add(this.InterFaceMode);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.SAPFIDB);
            this.Controls.Add(this.CorpSub);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "AppInit";
            this.Text = "初始化设置";
            this.Load += new System.EventHandler(this.AppInit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.InterFaceMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Corp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Shop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CorpSubName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SAPFIDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CorpSub.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showOWHS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocToSAP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oLDMat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sqlversion.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit InterFaceMode;
        private DevExpress.XtraEditors.ComboBoxEdit Corp;
        private DevExpress.XtraEditors.ComboBoxEdit Shop;
        private DevExpress.XtraEditors.LabelControl SAPDB;
        private DevExpress.XtraEditors.LabelControl CorpLable;
        private DevExpress.XtraEditors.TextEdit CorpSubName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.Button saveOK;
        private System.Windows.Forms.Button CancelOK;
        private DevExpress.XtraEditors.TextEdit SAPFIDB;
        private DevExpress.XtraEditors.TextEdit CorpSub;
        private DevExpress.XtraEditors.ComboBoxEdit showOWHS;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit DocToSAP;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.ComboBoxEdit oLDMat;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit ErrOP;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.TextEdit sqlversion;
    }
}
﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ToSAP.Init
{
    public partial class ShowMessageForm : XtraForm
    {
        public delegate void ChangeHandle(bool topmost);

        public ShowMessageForm()
        {
            InitializeComponent();
        }

        public event ChangeHandle ChangeHandleMost;

        private void ShowMessageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                ControlBox = false;
                Common.Common.GetFWSqlConn(this);
                Common.Common.APPinit(this);
                Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                Close();
               
            }
            
        }
    }
}

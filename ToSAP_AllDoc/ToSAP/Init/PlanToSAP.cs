﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.ServiceProcess;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Quartz;
using Quartz.Impl;

namespace ToSAP.Init
{
    public partial class PlanToSAP : XtraForm
    {
        public PlanToSAP()
        {
            InitializeComponent();
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PlanToSAP_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            DataTable oDataTable = Common.Common.oUserAccessShop(Program.FwSqlConnection, Program.oUSER);
            oDataTable.Columns.Remove("CHS");
            oDataTable.Columns.Remove("AreaS");
            oDataTable.Columns.Remove("Area1");
            oDataTable.Columns["Entry"].ColumnName = "shpEntry";
            oDataTable.AcceptChanges();
            oDataTable.Columns["WhsCode"].Caption = "编码";
            oDataTable.Columns["WhsName"].Caption = "描述";
            oDataTable.Columns["shpEntry"].Caption = "内部编码";
            repositoryItemGridLookUpEdit1.DataSource = oDataTable;
            repositoryItemGridLookUpEdit1.ValueMember = "WhsCode";
            repositoryItemGridLookUpEdit1.DisplayMember = "WhsName";
            repositoryItemGridLookUpEdit1.PopupFormWidth = Convert.ToInt32(Width * 0.8);

            string sqlCmd = "SELECT * FROM [ToSAP_PlanToSAP] ORDER by WSCode";
            DataTable odDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataRow oDataRow = odDataTable.NewRow();
            odDataTable.Rows.Add(oDataRow);
            gridControl1.DataSource = odDataTable;
            gridView1.IndicatorWidth = 40;
        }

        private void gridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView1.FocusedRowHandle + 1;
            string ss = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["WSName"]);
            if (e.FocusedColumn.FieldName == "WSName" && gridView1.RowCount == rowid && !ss.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "WSCode")
            {
                DataTable oDataTable = (DataTable) repositoryItemGridLookUpEdit1.DataSource;
                string WSCodesValue = gridView1.GetFocusedRowCellValue("WSCode").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("WhsCode ='{0}'", WSCodesValue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string WSNameValue = row["WhsName"].ToString();
                    gridView1.SetRowCellValue(e.RowHandle, "WSName", WSNameValue);
                    string shpEntry = row["shpEntry"].ToString();
                    ((DataTable) gridControl1.DataSource).Rows[e.RowHandle]["shpEntry"] = shpEntry;
                }
            }
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            SqlTransaction oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                Common.Common.TruncateTable(oSqlTransaction, Program.FwSqlConnection, "ToSAP_PlanToSAP");
                DataTable oDataTable = (DataTable) gridControl1.DataSource;
                if (oDataTable.Rows[oDataTable.Rows.Count - 1]["WSCode"].ToString().Trim() == string.Empty)
                {
                    oDataTable.Rows[oDataTable.Rows.Count - 1].Delete();
                }
                oDataTable.AcceptChanges();
                string timeSring = DateTime.Now.ToLongTimeString();
                foreach (DataRow odRow in oDataTable.Rows)
                {
                    odRow["ChangeDate"] = timeSring;
                }
                oDataTable.AcceptChanges();

                bool result = Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_PlanToSAP");
                if (result)
                {
                    MessageBox.Show("数据保存成功!");
                    oSqlTransaction.Commit();
                }

                oSqlTransaction.Dispose();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Rollback();
                oSqlTransaction.Dispose();
                MessageBox.Show(exception.Message);
            }
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedColumn.Name.Equals("synTime"))
            {
                Program.CronValue = gridView1.GetFocusedDisplayText();
                PlanCron oPlanCron = new PlanCron();
                oPlanCron.Left = ActiveForm.Width / 2 - 378;
                oPlanCron.ShowDialog(this);
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            if (gridView1.FocusedColumn.Name.Equals("synTime") && Program.PlanToSAPshow == 0)
            {
                Program.PlanToSAPsetRow = gridView1.FocusedRowHandle;
                Program.PlanToSAPshow = 1;
                Program.CronValue = gridView1.GetFocusedDisplayText();
                PlanCron oPlanCron = new PlanCron();
                oPlanCron.Left = Screen.PrimaryScreen.Bounds.Width / 2 - 378;
                oPlanCron.ShowDialog(this);
            }
        }

        private void gridView1_GotFocus_1(object sender, EventArgs e)
        {
            if (Program.PlanToSAPsetRow >= 0 && Program.PlanToSAPshow == 0 && Program.CronValue != string.Empty)
            {
                gridView1.SetRowCellValue(Program.PlanToSAPsetRow, "synTime", Program.CronValue);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                string serviceFilePath = $"{Application.StartupPath}\\ToSAP.exe";
                string serviceName = "PlanToSAPser";

                if (IsServiceExisted(serviceName))
                {
                    UninstallService(serviceName);
                }
                InstallService(serviceFilePath);
                ServiceStart(serviceName);
            }
            catch (Exception exception)
            {
            }
        }


        private bool IsServiceExisted(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController sc in services)
            {
                if (sc.ServiceName.ToLower() == serviceName.ToLower())
                {
                    return true;
                }
            }
            return false;
        }


        private void InstallService(string serviceFilePath)
        {
            try
            {
                ServiceHelper.Install(
                    "PlanToSAPser",
                    "PlanToSAPser",
                    "\"" + serviceFilePath + "\"" + " /Y",
                    "自动同步数据到SAP",
                    ServiceStartType.Auto,
                    ServiceAccount.LocalService,
                    null
                );
                MessageBox.Show("服务安装完成!");
            }
            catch (Exception e)
            {
                MessageBox.Show("请用管理员身份运行ToSAP后再次安装!" + Environment.NewLine + e.Message);
            }
        }


        private void UninstallService(string serviceFilePath)
        {
            ServiceHelper.Uninstall("PlanToSAPser");
        }


        private void ServiceStart(string serviceName)
        {
            using (ServiceController control = new ServiceController(serviceName))
            {
                if (control.Status == ServiceControllerStatus.Stopped)
                {
                    control.Start();
                }
            }
        }


        private void ServiceStop(string serviceName)
        {
            using (ServiceController control = new ServiceController(serviceName))
            {
                if (control.Status == ServiceControllerStatus.Running)
                {
                    control.Stop();
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string serviceFilePath = $"{Application.StartupPath}\\ToSAP.exe";
            string serviceName = "PlanToSAPser";
            if (IsServiceExisted(serviceName))
            {
                ServiceStop(serviceName);
                UninstallService(serviceFilePath);
            }

            MessageBox.Show("服务卸载完成!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateJob();
        }

        public static void CreateJob()
        {
            Common.Common.GetFWSqlConn(null);
            ISchedulerFactory factory;
            IScheduler oScheduler;
            string LastTime = string.Empty;
            string sqlCmd = string.Empty;
            if (LastTime == string.Empty)
            {
                sqlCmd = "SELECT * FROM ToSAP_PlanToSAP ";
            }
            else
            {
                sqlCmd = "SELECT  * FROM ToSAP_PlanToSAP (NOLOCK) where changedate>'" + LastTime + "'";
            }
            Common.Common.LogFileWrite(AppDomain.CurrentDomain.BaseDirectory+"\\lognew.txt", sqlCmd + Environment.NewLine);
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            if (!(oDataTable.Rows.Count > 0))
            {
                return;
            }
            NameValueCollection props = new NameValueCollection();
            props.Add("quartz.scheduler.instanceName", "Stray");
            props.Add("quartz.threadPool.threadCount", "1");
            props.Add("quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz");

            Common.Common.LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\lognew.txt", "new StdSchedulerFactory()" + Environment.NewLine);
            factory = new StdSchedulerFactory(props);
            Common.Common.LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\lognew.txt", "factory.GetScheduler();" + Environment.NewLine);
            oScheduler = factory.GetScheduler();
            Common.Common.LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\lognew.txt", "factory.GetScheduler();" + Environment.NewLine);
            oScheduler.Start();
            Common.Common.LogFileWrite(AppDomain.CurrentDomain.BaseDirectory + "\\lognew.txt", "OSCHEDULER START" + Environment.NewLine);
            LastTime = oDataTable.Rows[0]["ChangeDate"].ToString();
            oScheduler.Clear();

            Dictionary<IJobDetail,Quartz.Collection.ISet<ITrigger>> dictionary = new System.Collections.Generic.Dictionary<IJobDetail, Quartz.Collection.ISet<ITrigger>>();

            foreach (DataRow odataRow in oDataTable.Rows)
            {
                string shpCode = odataRow["WSCode"].ToString();
                string synInt = odataRow["synInt"].ToString();
                string shpCron = odataRow["synTime"].ToString().Trim();
                string shpEntry = odataRow["shpEntry"].ToString();
                IJobDetail oJobDetail =
                    JobBuilder.Create<PlanToSAPser.dataToSAP>().WithIdentity("Job" + shpCode)
                        .UsingJobData("synInt", synInt)
                        .UsingJobData("shpCode", shpCode)
                        .UsingJobData("shpEntry", shpEntry)
                        .Build();

                ITrigger oTrigger = TriggerBuilder.Create()
                    .WithIdentity("Job" + shpCode + "Trigger", "Job" + shpCode + "Trigger")
                    .WithCronSchedule(shpCron)
                    .ForJob(oJobDetail.Key)
                    .StartNow()
                    .Build();


                Quartz.Collection.HashSet<ITrigger> oCollection = new Quartz.Collection.HashSet<ITrigger>(new ITrigger[1] { oTrigger });
                dictionary.Add(oJobDetail, oCollection);
            }

            oScheduler.ScheduleJobs(dictionary, true);
            oDataTable.Dispose();
            GC.Collect();
        }
    }
}

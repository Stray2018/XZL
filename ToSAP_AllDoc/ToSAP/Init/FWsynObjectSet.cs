﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Init
{
    public partial class FWsynObjectSet : XtraForm
    {
        public FWsynObjectSet()
        {
            InitializeComponent();
        }

        private void FWsynObjectSet_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = "SELECT * FROM [ToSAP_FWobject] ORDER BY DataType,FWCode,MasterTb";
            DataTable odDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataRow oDataRow = odDataTable.NewRow();
            odDataTable.Rows.Add(oDataRow);
            gridControl1.DataSource = odDataTable;
            gridView1.IndicatorWidth = 40;
        }

        private void gridControl1_Load(object sender, EventArgs e)
        {
        }

        private void gridControl1_Leave(object sender, EventArgs e)
        {
        }

        private void gridView1_CustomDrawRowIndicator(object sender,
            RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_FocusedColumnChanged(object sender,
            FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView1.FocusedRowHandle + 1;
            string ss = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["FWCode"]);
            if (e.FocusedColumn.FieldName == "FWName" && gridView1.RowCount == rowid && !ss.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridView1.DeleteSelectedRows();
        }

        private void gridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Common.Common.InsertDataToDataSource(gridView1, gridControl1);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            SqlTransaction oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                Common.Common.TruncateTable(oSqlTransaction, Program.FwSqlConnection, "ToSAP_FWobject");
                DataTable oDataTable = (DataTable) gridControl1.DataSource;
                if (oDataTable.Rows[oDataTable.Rows.Count - 1]["FWCode"].ToString().Trim() == string.Empty)
                {
                    oDataTable.Rows[oDataTable.Rows.Count - 1].Delete();
                }
                bool result = Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_FWobject");
                if (result)
                {
                    MessageBox.Show("数据保存成功!");
                    oSqlTransaction.Commit();
                }

                oSqlTransaction.Dispose();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Rollback();
                oSqlTransaction.Dispose();
                MessageBox.Show(exception.Message);
            }
        }
    }
}

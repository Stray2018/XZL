﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Init
{
    public partial class FWobjData : XtraForm
    {
        public FWobjData()
        {
            InitializeComponent();
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FWobjData_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = "SELECT * FROM [ToSAP_FWFldDesc] ORDER BY DataType, FldName,FldDesc";
            DataTable odDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataRow oDataRow = odDataTable.NewRow();
            odDataTable.Rows.Add(oDataRow);
            gridControl1.DataSource = odDataTable;
            gridView1.IndicatorWidth = 40;
        }

        private void gridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView1.FocusedRowHandle + 1;
            string ss = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["FldName"]);
            if (e.FocusedColumn.FieldName == "FldDesc" && gridView1.RowCount == rowid && !ss.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            SqlTransaction oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                Common.Common.TruncateTable(oSqlTransaction, Program.FwSqlConnection, "ToSAP_FWFldDesc");
                DataTable oDataTable = (DataTable) gridControl1.DataSource;
                if (oDataTable.Rows[oDataTable.Rows.Count - 1]["FldName"].ToString().Trim() == string.Empty)
                {
                    oDataTable.Rows[oDataTable.Rows.Count - 1].Delete();
                }
                bool result = Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_FWFldDesc");
                if (result)
                {
                    MessageBox.Show("数据保存成功!");
                    oSqlTransaction.Commit();
                }

                oSqlTransaction.Dispose();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Rollback();
                oSqlTransaction.Dispose();
                MessageBox.Show(exception.Message);
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Common.Common.InsertDataToDataSource(gridView1, gridControl1);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridView1.DeleteSelectedRows();
        }

        private void gridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }
    }
}

﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Init
{
    public partial class SAPDIData : XtraForm
    {
        public SAPDIData()
        {
            InitializeComponent();
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            var rowid = gridView1.FocusedRowHandle + 1;
            var ss = gridView1.GetRowCellDisplayText(rowid - 1, gridView1.Columns["DIobjType"]);
            if (e.FocusedColumn.FieldName == "DItype" && gridView1.RowCount == rowid && !ss.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Common.Common.InsertDataToDataSource(gridView1, gridControl1);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridView1.DeleteSelectedRows();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            var oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                Common.Common.TruncateTable(oSqlTransaction, Program.FwSqlConnection, "ToSAP_DIProp");
                DataTable oDataTable = (DataTable) gridControl1.DataSource;
                if (oDataTable.Rows[oDataTable.Rows.Count - 1]["DIobjtype"].ToString().Trim() == string.Empty)
                {
                    oDataTable.Rows[oDataTable.Rows.Count - 1].Delete();
                }
                bool result = Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_DIProp");
                if (result)
                {
                    MessageBox.Show("数据保存成功!");
                    oSqlTransaction.Commit();
                }

                oSqlTransaction.Dispose();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Dispose();
                MessageBox.Show(exception.Message);
            }
        }

        private void gridControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu1.ShowPopup(newPoint);
            }
        }

        private void SAPDIData_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            DIobjType.ColumnEdit = repositoryItemComboBox2;

            string sqlCmd = "SELECT DISTINCT DIobjType,DIobjType Descr FROM [ToSAP_DIinterface] ORDER BY DIobjType";
            var DiSource = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            for (var i = 0; i < DiSource.Rows.Count; i++)
            {
                var Itemstr = DiSource.Rows[i][0].ToString();
                repositoryItemComboBox2.Items.Add(Itemstr);
            }

            sqlCmd = "SELECT * FROM [ToSAP_DIProp] ORDER BY DIobjType,DItype,DIProperty";
            var odDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataRow oDataRow = odDataTable.NewRow();
            odDataTable.Rows.Add(oDataRow);
            gridControl1.DataSource = odDataTable;
            gridView1.IndicatorWidth = 40;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Init
{
    public partial class CorpSet : XtraForm
    {
        public static int newData = 1;

        public CorpSet()
        {
            InitializeComponent();
        }

        private void labelControl4_Click(object sender, EventArgs e)
        {
        }

        private void textEdit2_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void labelControl3_Click(object sender, EventArgs e)
        {
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void textEdit4_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void textEdit3_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void BranchSet_Load(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);

            LoadBranchLst();

            initGridDatasource();
            string sqlCmd =
                "SELECT T1.WhsCode,T1.WhsName,'仓库' AS WhsType FROM OWHS T1  WHERE  '" + Program.showOWHS +
                "'  like '%仓库%' UNION ALL SELECT T1.shpCode,T1.shpName,'店铺' AS WhsType FROM OSHP T1 where  '" +
                Program.showOWHS + "'  like '%店铺%'";
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            gWhsCode.ColumnEdit = gWhsCoderepos;
            gWhsCoderepos.DisplayMember = "WhsCode";
            gWhsCoderepos.ValueMember = "WhsCode";
            gWhsCoderepos.DataSource = oDataTable;
            gridView1.IndicatorWidth = 40;
            SetGridLookUpEditMoreColumnFilter(gWhsCoderepos);
        }

        private void LoadBranchLst()
        {
            string sqlCmd = "SELECT BranchCode,'['+BranchCode+']'+BranchName AS 'BranchName' FROM ToSAP_BranchTitle";
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            listBoxControl1.MultiColumn = true;
            listBoxControl1.ValueMember = "BranchCode";
            listBoxControl1.DisplayMember = "BranchName";
            listBoxControl1.DataSource = oDataTable;
        }
        private void SetGridLookUpEditMoreColumnFilter(RepositoryItemGridLookUpEdit repGLUEdit)
        {
            repGLUEdit.EditValueChanging += (sender, e) =>
            {
                BeginInvoke(new MethodInvoker(() =>
                {
                    var edit = sender as GridLookUpEdit;
                    var view = edit.Properties.View;

                    var extraFilter = view.GetType()
                        .GetField("extraFilter", BindingFlags.NonPublic | BindingFlags.Instance);
                    var columnsOperators = new List<CriteriaOperator>();
                    foreach (GridColumn col in view.VisibleColumns)
                    {
                        if (col.Visible && col.ColumnType == typeof(string))
                        {
                            columnsOperators.Add(new FunctionOperator(FunctionOperatorType.Contains,
                                new OperandProperty(col.FieldName),
                                new OperandValue(edit.Text)));
                        }
                    }
                    var filterCondition = new GroupOperator(GroupOperatorType.Or, columnsOperators).ToString();
                    extraFilter.SetValue(view, filterCondition);

                    var ApplyColumnsFilterEx = view.GetType().GetMethod("ApplyColumnsFilterEx",
                        BindingFlags.NonPublic | BindingFlags.Instance);
                    ApplyColumnsFilterEx.Invoke(view, null);
                }));
            };
        }

        private void initGridDatasource()
        {
            string sqlCmd = "SELECT * FROM ToSAP_BranchDetail WHERE 1=2";
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataColumn oDataColumn = oDataTable.Columns["Disable"];
            oDataColumn.DefaultValue = 'N';
            DataRow oDataRow = oDataTable.NewRow();
            oDataTable.Rows.Add(oDataRow);
            gridControl1.DataSource = oDataTable;
        }

        private void gridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "WhsCode")
            {
                DataTable oDataTable = (DataTable) gWhsCoderepos.DataSource;
                string WhsCodeValue = gridView1.GetFocusedRowCellValue("WhsCode").ToString();
                DataRow[] rows = oDataTable.Select(string.Format("WhsCode='{0}'", WhsCodeValue));
                if (rows != null && rows.Length > 0)
                {
                    DataRow row = rows[0];
                    string WhsNameValue = row["WhsName"].ToString();
                    string WhsTypeValue = row["WhsType"].ToString();
                    gridView1.SetRowCellValue(e.RowHandle, "WhsName", WhsNameValue);
                    gridView1.SetRowCellValue(e.RowHandle, "WhsType", WhsTypeValue);
                }
            }
        }

        private void gridView1_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            int rowid = gridView1.FocusedRowHandle + 1;
            string WhsCodeValue = gridView1.GetFocusedRowCellDisplayText("WhsCode");
            if (gridView1.RowCount == rowid && gridView1.FocusedColumn.FieldName == "WhsName" &&
                !WhsCodeValue.Equals(string.Empty))
            {
                gridView1.AddNewRow();
                gridView1.FocusedRowHandle = rowid - 1;
            }
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string sqlCmd = string.Empty;
            string BranCodeValue = BranchCode.Text.Trim();
            if (newData == 0)
            {
                BranCodeValue = listBoxControl1.SelectedValue.ToString();
            }
            sqlCmd = "SELECT * FROM ToSAP_BranchTitle where 1=2";
            DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
            DataRow oDataRow = oDataTable.NewRow();
            oDataRow["BranchCode"] = BranCodeValue;
            oDataRow["BranchName"] = BranchName.Text.Trim();
            oDataRow["WhsCode"] = string.Empty;
            oDataRow["WhsName"] = string.Empty;
            oDataRow["CusCode"] = string.Empty;
            oDataRow["CusName"] = string.Empty;
            oDataRow["VenCode"] = string.Empty;
            oDataRow["VenName"] = string.Empty;
            oDataTable.Rows.Add(oDataRow);

            DataTable detailData = (DataTable) gridControl1.DataSource;

            if (detailData.Rows.Count < 2)
            {
                MessageBox.Show("明细仓库/店铺不允许为空,不允许保存!");
                return;
            }

            foreach (DataRow oRow in detailData.Rows)
            {
                oRow["BranchCode"] = BranCodeValue;
                detailData.AcceptChanges();
                if (oRow["WhsCode"].ToString().Equals(string.Empty))
                {
                    oRow.Delete();
                }
            }

            SqlTransaction oSqlTransaction = Program.FwSqlConnection.BeginTransaction();
            try
            {
                if (newData == 0)
                {
                    string sqlCmdd = "DELETE FROM ToSAP_BranchTitle WHERE BranchCode = '" + BranCodeValue + "'";
                    Common.Common.ExecSqlcmd(oSqlTransaction, Program.FwSqlConnection, sqlCmdd);
                    sqlCmdd = "DELETE FROM ToSAP_Branchdetail  WHERE BranchCode = '" + BranCodeValue + "'";
                    Common.Common.ExecSqlcmd(oSqlTransaction, Program.FwSqlConnection, sqlCmdd);
                }

                Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, oDataTable,
                    "ToSAP_BranchTitle");
                Common.Common.InsertDataToTable(oSqlTransaction, Program.FwSqlConnection, detailData,
                    "ToSAP_BranchDetail");
                oSqlTransaction.Commit();
                MessageBox.Show("数据保存成功!");

                BranchCode.Text = string.Empty;
                BranchName.Text = string.Empty;

                initGridDatasource();
                newData = 1;

                LoadBranchLst();
            }
            catch (Exception exception)
            {
                oSqlTransaction.Rollback();
                MessageBox.Show(exception.Message);
            }
        }

        private void listBoxControl1_MouseClick(object sender, MouseEventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    string BranchCodeValue = listBoxControl1.SelectedValue.ToString();
                    string sqlCmd = "SELECT * FROM ToSAP_BranchTitle WHERE BranchCode = '" + BranchCodeValue + "'";
                    DataTable oDataTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                    BranchCode.Text = oDataTable.Rows[0]["BranchCode"].ToString().Trim();
                    BranchName.Text = oDataTable.Rows[0]["BranchName"].ToString().Trim();

                    sqlCmd = "SELECT * FROM ToSAP_Branchdetail WHERE BranchCode = '" + BranchCodeValue + "'";
                    DataTable detailTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);
                    DataColumn oDataColumn = detailTable.Columns["Disable"];
                    oDataColumn.DefaultValue = "N";
                    detailTable.Rows.Add(detailTable.NewRow());
                    gridControl1.DataSource = detailTable;
                    newData = 0;
                }

                if (e.Button == MouseButtons.Right)
                {
                    Point newPoint = new Point(Cursor.Position.X, Cursor.Position.Y);
                    popupMenu1.ShowPopup(newPoint);
                }
            }
            catch (Exception exception)
            {
            }
        }

        private void newOK_Click(object sender, EventArgs e)
        {
            newData = 1;
            BranchCode.Text = string.Empty;
            BranchName.Text = string.Empty;

            initGridDatasource();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            string BrancodeValue = listBoxControl1.SelectedValue.ToString();
            if (BrancodeValue.Equals(string.Empty))
            {
                MessageBox.Show("请先选中要删除的设置,然后点右键删除!");
                return;
            }

            DialogResult oResult = MessageBox.Show("确认删除此分支设置?", "确认", MessageBoxButtons.YesNo);
            if (oResult == DialogResult.Yes)
            {
                string sqlCmd = "DELETE  FROM ToSAP_BranchTitle WHERE BranchCode = '" + BrancodeValue + "'";
                string sqlCmd2 = "DELETE  FROM ToSAP_Branchdetail WHERE BranchCode = '" + BrancodeValue + "'";
                SqlTransaction oTransaction = Program.FwSqlConnection.BeginTransaction();
                try
                {
                    Common.Common.ExecSqlcmd(oTransaction, Program.FwSqlConnection, sqlCmd);
                    Common.Common.ExecSqlcmd(oTransaction, Program.FwSqlConnection, sqlCmd2);
                    oTransaction.Commit();
                    LoadBranchLst();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    oTransaction.Rollback();
                }
            }
        }

        private  void simpleButton1_Click(object sender, EventArgs e)
        {
            string shpCode =
                gridView1.GetDataRow(gridView1.GetSelectedRows()[0])["WhsCode"].ToString();
            Common.Common.SAPFieldAdd(shpCode, Program.oUSER);
        }
    }
}

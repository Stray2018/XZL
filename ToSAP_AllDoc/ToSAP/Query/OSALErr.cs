﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ToSAP.Query
{
    public partial class OSALErr : DevExpress.XtraEditors.XtraForm
    {
        public static DataTable ODataTable = new DataTable();
        public OSALErr()
        {
            InitializeComponent();
        }

        private void OSALErr_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = ODataTable;
        }
    }
}
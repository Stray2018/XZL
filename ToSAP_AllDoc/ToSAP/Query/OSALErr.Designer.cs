﻿namespace ToSAP.Query
{
    partial class OSALErr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DocNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LineNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Qty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LineTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(5, 5);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(895, 444);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DocNum,
            this.LineNum,
            this.ItemCode,
            this.ItemName,
            this.Qty,
            this.LineTotal});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // DocNum
            // 
            this.DocNum.Caption = "单据编号";
            this.DocNum.FieldName = "DocNum";
            this.DocNum.Name = "DocNum";
            this.DocNum.Visible = true;
            this.DocNum.VisibleIndex = 0;
            // 
            // LineNum
            // 
            this.LineNum.Caption = "行号";
            this.LineNum.FieldName = "LineNum";
            this.LineNum.Name = "LineNum";
            this.LineNum.Visible = true;
            this.LineNum.VisibleIndex = 1;
            // 
            // ItemCode
            // 
            this.ItemCode.Caption = "公司条码";
            this.ItemCode.FieldName = "ItemCode";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.Visible = true;
            this.ItemCode.VisibleIndex = 2;
            // 
            // ItemName
            // 
            this.ItemName.Caption = "商品名称";
            this.ItemName.FieldName = "ItemName";
            this.ItemName.Name = "ItemName";
            this.ItemName.Visible = true;
            this.ItemName.VisibleIndex = 3;
            // 
            // Qty
            // 
            this.Qty.Caption = "数量";
            this.Qty.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Qty.FieldName = "Qty";
            this.Qty.Name = "Qty";
            this.Qty.Visible = true;
            this.Qty.VisibleIndex = 4;
            // 
            // LineTotal
            // 
            this.LineTotal.Caption = "金额";
            this.LineTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LineTotal.FieldName = "LineTotal";
            this.LineTotal.Name = "LineTotal";
            this.LineTotal.Visible = true;
            this.LineTotal.VisibleIndex = 5;
            // 
            // OSALErr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 461);
            this.Controls.Add(this.gridControl1);
            this.Name = "OSALErr";
            this.Text = "零售单据错误数据";
            this.Load += new System.EventHandler(this.OSALErr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn DocNum;
        private DevExpress.XtraGrid.Columns.GridColumn LineNum;
        private DevExpress.XtraGrid.Columns.GridColumn ItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn ItemName;
        private DevExpress.XtraGrid.Columns.GridColumn Qty;
        private DevExpress.XtraGrid.Columns.GridColumn LineTotal;
    }
}
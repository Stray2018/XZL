﻿namespace ToSAP.Query
{
    partial class DocNSYN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.WSCode = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.STDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ENDate = new DevExpress.XtraEditors.DateEdit();
            this.QueryOK = new DevExpress.XtraEditors.SimpleButton();
            this.CancelOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.shpCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.shpName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.objType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DocNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Memo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SAPobj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.WSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 14);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "店铺/仓库:";
            // 
            // WSCode
            // 
            this.WSCode.EditValue = "";
            this.WSCode.Location = new System.Drawing.Point(84, 11);
            this.WSCode.Name = "WSCode";
            this.WSCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WSCode.Properties.NullText = "";
            this.WSCode.Size = new System.Drawing.Size(100, 20);
            this.WSCode.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(198, 14);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 14);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "开始日期:";
            // 
            // STDate
            // 
            this.STDate.EditValue = null;
            this.STDate.Location = new System.Drawing.Point(264, 11);
            this.STDate.Name = "STDate";
            this.STDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.STDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.STDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.STDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.STDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.STDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.STDate.Properties.Mask.EditMask = "";
            this.STDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.STDate.Size = new System.Drawing.Size(100, 20);
            this.STDate.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(378, 14);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(52, 14);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "结束日期:";
            // 
            // ENDate
            // 
            this.ENDate.EditValue = null;
            this.ENDate.Location = new System.Drawing.Point(444, 11);
            this.ENDate.Name = "ENDate";
            this.ENDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ENDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ENDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.ENDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ENDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.ENDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ENDate.Properties.Mask.EditMask = "";
            this.ENDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.ENDate.Size = new System.Drawing.Size(100, 20);
            this.ENDate.TabIndex = 6;
            // 
            // QueryOK
            // 
            this.QueryOK.Location = new System.Drawing.Point(613, 10);
            this.QueryOK.Name = "QueryOK";
            this.QueryOK.Size = new System.Drawing.Size(75, 23);
            this.QueryOK.TabIndex = 7;
            this.QueryOK.Text = "查询";
            this.QueryOK.Click += new System.EventHandler(this.QueryOK_Click);
            // 
            // CancelOK
            // 
            this.CancelOK.Location = new System.Drawing.Point(702, 10);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(75, 23);
            this.CancelOK.TabIndex = 8;
            this.CancelOK.Text = "关闭";
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(13, 40);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(858, 359);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.shpCode,
            this.shpName,
            this.objType,
            this.DocDate,
            this.DocNum,
            this.Memo,
            this.SAPobj});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // shpCode
            // 
            this.shpCode.Caption = "店铺/仓库编码";
            this.shpCode.FieldName = "shpCode";
            this.shpCode.Name = "shpCode";
            this.shpCode.Visible = true;
            this.shpCode.VisibleIndex = 0;
            // 
            // shpName
            // 
            this.shpName.Caption = "店铺/仓库名称";
            this.shpName.FieldName = "shpName";
            this.shpName.Name = "shpName";
            this.shpName.Visible = true;
            this.shpName.VisibleIndex = 1;
            // 
            // objType
            // 
            this.objType.Caption = "数据类型";
            this.objType.FieldName = "FWName";
            this.objType.Name = "objType";
            this.objType.Visible = true;
            this.objType.VisibleIndex = 2;
            // 
            // DocDate
            // 
            this.DocDate.Caption = "单据日期";
            this.DocDate.FieldName = "DocDate";
            this.DocDate.Name = "DocDate";
            this.DocDate.Visible = true;
            this.DocDate.VisibleIndex = 3;
            // 
            // DocNum
            // 
            this.DocNum.Caption = "单据编号/数值";
            this.DocNum.FieldName = "DocNum";
            this.DocNum.Name = "DocNum";
            this.DocNum.Visible = true;
            this.DocNum.VisibleIndex = 4;
            // 
            // Memo
            // 
            this.Memo.Caption = "单据备注";
            this.Memo.FieldName = "Memo";
            this.Memo.Name = "Memo";
            this.Memo.Visible = true;
            this.Memo.VisibleIndex = 5;
            // 
            // SAPobj
            // 
            this.SAPobj.Caption = "SAP单据类型";
            this.SAPobj.FieldName = "SAPobj";
            this.SAPobj.Name = "SAPobj";
            this.SAPobj.Visible = true;
            this.SAPobj.VisibleIndex = 6;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(551, 12);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "集团";
            this.checkEdit1.Size = new System.Drawing.Size(56, 19);
            this.checkEdit1.TabIndex = 10;
            // 
            // DocNSYN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(883, 411);
            this.Controls.Add(this.checkEdit1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.CancelOK);
            this.Controls.Add(this.QueryOK);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.ENDate);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.WSCode);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.STDate);
            this.Name = "DocNSYN";
            this.Text = "未同步业务查询";
            this.Load += new System.EventHandler(this.DocNSYN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.WSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit WSCode;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit STDate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit ENDate;
        private DevExpress.XtraEditors.SimpleButton QueryOK;
        private DevExpress.XtraEditors.SimpleButton CancelOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn shpCode;
        private DevExpress.XtraGrid.Columns.GridColumn shpName;
        private DevExpress.XtraGrid.Columns.GridColumn objType;
        private DevExpress.XtraGrid.Columns.GridColumn DocDate;
        private DevExpress.XtraGrid.Columns.GridColumn DocNum;
        private DevExpress.XtraGrid.Columns.GridColumn Memo;
        private DevExpress.XtraGrid.Columns.GridColumn SAPobj;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
    }
}
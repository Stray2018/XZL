﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace ToSAP.Query
{
    public partial class ErrQuery : XtraForm
    {
        public ErrQuery()
        {
            InitializeComponent();
        }

        private void ErrQuery_Load(object sender, EventArgs e)
        {
            memoEdit1.Visible = false;
            WSCode.Properties.DisplayMember = "WhsName";
            WSCode.Properties.ValueMember = "WhsCode";
            System.Data.DataTable oDataTable = Common.Common.oUserAccessShop(Program.FwSqlConnection, Program.oUSER);
            oDataTable.Columns.Remove("CHS");
            oDataTable.Columns.Remove("Entry");
            oDataTable.Columns.Remove("AreaS");
            oDataTable.Columns.Remove("Area1");
            oDataTable.AcceptChanges();
            WSCode.Properties.DataSource = oDataTable;
            STDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01"));
            ENDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(1).AddDays(-1);
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void QueryOK_Click(object sender, EventArgs e)
        {
            var shpCode = WSCode.EditValue.ToString();
            var StDate = STDate.Text;
            var EnDate = ENDate.Text;
            string sql00 = "";
            if(Program.showOWHS.Equals("店铺"))
            {
                sql00 = "SELECT shpCode, shpName FROM OSHP where shpName not like '%后仓%'";  //店铺  仓库 店铺和仓库
            }
            if (Program.showOWHS.Equals("仓库"))
            {
                sql00 = "SELECT WhsCode, WhsName FROM OWHS";  //店铺  仓库 店铺和仓库
            }
            if (Program.showOWHS.Equals("店铺和仓库"))
            {
                sql00 = "SELECT shpCode, shpName FROM OSHP where shpName not like '%后仓%' UNION ALL SELECT WhsCode, WhsName FROM OWHS";  //店铺  仓库 店铺和仓库
            }
            string sqlCmd =
                "SELECT T1.ShpCode shpCode,U0.shpName ,convert(varchar(100),T1.ImportDate,121) ImportDate ,T2.FWName objType ,T1.DocDate ,T1.DocNum ,T3.SAPobjName SAPobj,T1.SAPError ErrDesc  FROM ToSAP_ImportErr T1" +
                " LEFT JOIN ("+ sql00 + ")U0 ON U0.shpCode = T1.ShpCode" +
                " LEFT JOIN ToSAP_FWobject T2 ON T1.DocType = T2.FWCode" +
                " LEFT JOIN ToSAP_DIobject T3 ON T1.SAPDocType = T3.SAPobjCode" +
                " WHERE(T1.ShpCode = '" + shpCode + "' OR '" + shpCode + "' = '') AND convert(date, T1.ImportDate,121) >= '" + StDate +
                "' AND convert(date, T1.ImportDate,121) <= '" + EnDate + "'";
            gridControl1.DataSource = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
        }

        private void gridControl1_Click(object sender, EventArgs e)
        {
        }

        private void ErrQuery_Click(object sender, EventArgs e)
        {
            memoEdit1.Visible = false;
            memoEdit1.EditValue = string.Empty;
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            if (gridView1.FocusedColumn.Name.Equals("ErrDesc"))
            {
                try
                {
                    memoEdit1.Visible = false;
                    memoEdit1.Text = string.Empty;
                    memoEdit1.MaskBox.AppendText(gridView1.GetDataRow(gridView1.FocusedRowHandle)["ErrDesc"]
                        .ToString());
                    memoEdit1.BackColor = Color.Transparent;
                    memoEdit1.Properties.Appearance.BackColor = Color.Transparent;
                    memoEdit1.Visible = true;
                }
                catch (Exception exception)
                {
                }
            }
            else
            {
                memoEdit1.Visible = false;
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridView1.OptionsClipboard.AllowCopy = DefaultBoolean.True;
            
            this.gridView1.CopyToClipboard();
        }

        private void gridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {Point p= new Point(MousePosition.X,MousePosition.Y);
                popupMenu1.ShowPopup(barManager1,p);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
          Common.Common.GetFWSqlConn(null);
            string shpCode = WSCode.EditValue.ToString();
            string stDate = STDate.EditValue.ToString();
            string enDate = ENDate.EditValue.ToString();
            string sqlCmd = "DELETE ToSAP_ImportErr WHERE (ShpCode = '" + shpCode + "' or '"+shpCode+"'='') AND CONVERT(DATE,ImportDate) BETWEEN '" + stDate + "' AND '" + enDate + "'";
            Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
            QueryOK_Click(sender, e);}
    }
}

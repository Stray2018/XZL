﻿namespace ToSAP.Query
{
    partial class ErrQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.shpCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.shpName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ImportDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.objType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DocDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DocNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SAPobj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ErrDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CancelOK = new DevExpress.XtraEditors.SimpleButton();
            this.QueryOK = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.ENDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.WSCode = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.STDate = new DevExpress.XtraEditors.DateEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(12, 38);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1010, 475);
            this.gridControl1.TabIndex = 18;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.shpCode,
            this.shpName,
            this.ImportDate,
            this.objType,
            this.DocDate,
            this.DocNum,
            this.SAPobj,
            this.ErrDesc});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridView1_RowClick);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // shpCode
            // 
            this.shpCode.Caption = "店铺/仓库编码";
            this.shpCode.FieldName = "shpCode";
            this.shpCode.Name = "shpCode";
            this.shpCode.OptionsColumn.AllowEdit = false;
            this.shpCode.Visible = true;
            this.shpCode.VisibleIndex = 0;
            this.shpCode.Width = 87;
            // 
            // shpName
            // 
            this.shpName.Caption = "店铺/仓库名称";
            this.shpName.FieldName = "shpName";
            this.shpName.Name = "shpName";
            this.shpName.OptionsColumn.AllowEdit = false;
            this.shpName.Visible = true;
            this.shpName.VisibleIndex = 1;
            this.shpName.Width = 90;
            // 
            // ImportDate
            // 
            this.ImportDate.Caption = "同步日期";
            this.ImportDate.FieldName = "ImportDate";
            this.ImportDate.Name = "ImportDate";
            this.ImportDate.Visible = true;
            this.ImportDate.VisibleIndex = 2;
            this.ImportDate.Width = 62;
            // 
            // objType
            // 
            this.objType.Caption = "单据类型";
            this.objType.FieldName = "objType";
            this.objType.Name = "objType";
            this.objType.OptionsColumn.AllowEdit = false;
            this.objType.Visible = true;
            this.objType.VisibleIndex = 3;
            this.objType.Width = 60;
            // 
            // DocDate
            // 
            this.DocDate.Caption = "单据日期";
            this.DocDate.FieldName = "DocDate";
            this.DocDate.Name = "DocDate";
            this.DocDate.OptionsColumn.AllowEdit = false;
            this.DocDate.Visible = true;
            this.DocDate.VisibleIndex = 4;
            this.DocDate.Width = 69;
            // 
            // DocNum
            // 
            this.DocNum.Caption = "单据编号";
            this.DocNum.FieldName = "DocNum";
            this.DocNum.Name = "DocNum";
            this.DocNum.OptionsColumn.AllowEdit = false;
            this.DocNum.Visible = true;
            this.DocNum.VisibleIndex = 5;
            this.DocNum.Width = 131;
            // 
            // SAPobj
            // 
            this.SAPobj.Caption = "SAP单据类型";
            this.SAPobj.FieldName = "SAPobj";
            this.SAPobj.Name = "SAPobj";
            this.SAPobj.OptionsColumn.AllowEdit = false;
            this.SAPobj.Visible = true;
            this.SAPobj.VisibleIndex = 6;
            this.SAPobj.Width = 85;
            // 
            // ErrDesc
            // 
            this.ErrDesc.Caption = "错误描述";
            this.ErrDesc.FieldName = "ErrDesc";
            this.ErrDesc.Name = "ErrDesc";
            this.ErrDesc.OptionsColumn.AllowEdit = false;
            this.ErrDesc.Visible = true;
            this.ErrDesc.VisibleIndex = 7;
            this.ErrDesc.Width = 196;
            // 
            // CancelOK
            // 
            this.CancelOK.Location = new System.Drawing.Point(646, 8);
            this.CancelOK.Name = "CancelOK";
            this.CancelOK.Size = new System.Drawing.Size(75, 23);
            this.CancelOK.TabIndex = 17;
            this.CancelOK.Text = "关闭";
            this.CancelOK.Click += new System.EventHandler(this.CancelOK_Click);
            // 
            // QueryOK
            // 
            this.QueryOK.Location = new System.Drawing.Point(557, 8);
            this.QueryOK.Name = "QueryOK";
            this.QueryOK.Size = new System.Drawing.Size(75, 23);
            this.QueryOK.TabIndex = 16;
            this.QueryOK.Text = "查询";
            this.QueryOK.Click += new System.EventHandler(this.QueryOK_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(377, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(52, 14);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "结束日期:";
            // 
            // ENDate
            // 
            this.ENDate.EditValue = null;
            this.ENDate.Location = new System.Drawing.Point(443, 9);
            this.ENDate.Name = "ENDate";
            this.ENDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ENDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ENDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.ENDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ENDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.ENDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ENDate.Properties.Mask.EditMask = "";
            this.ENDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.ENDate.Size = new System.Drawing.Size(100, 20);
            this.ENDate.TabIndex = 15;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(197, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 14);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "开始日期:";
            // 
            // WSCode
            // 
            this.WSCode.EditValue = "";
            this.WSCode.Location = new System.Drawing.Point(83, 9);
            this.WSCode.Name = "WSCode";
            this.WSCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WSCode.Properties.NullText = "";
            this.WSCode.Size = new System.Drawing.Size(100, 20);
            this.WSCode.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 14);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "店铺/仓库:";
            // 
            // STDate
            // 
            this.STDate.EditValue = null;
            this.STDate.Location = new System.Drawing.Point(263, 9);
            this.STDate.Name = "STDate";
            this.STDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.STDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.STDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.STDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.STDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.STDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.STDate.Properties.Mask.EditMask = "";
            this.STDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.STDate.Size = new System.Drawing.Size(100, 20);
            this.STDate.TabIndex = 13;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit1.Location = new System.Drawing.Point(263, 110);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.memoEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit1.Size = new System.Drawing.Size(481, 382);
            this.memoEdit1.TabIndex = 19;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "复制";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 2;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1034, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 525);
            this.barDockControlBottom.Size = new System.Drawing.Size(1034, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 525);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1034, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 525);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 0;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(739, 9);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 24;
            this.simpleButton1.Text = "删除";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // ErrQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1034, 525);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.CancelOK);
            this.Controls.Add(this.QueryOK);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.ENDate);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.WSCode);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.STDate);
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ErrQuery";
            this.Text = "同步错误查询";
            this.Load += new System.EventHandler(this.ErrQuery_Load);
            this.Click += new System.EventHandler(this.ErrQuery_Click);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ENDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.STDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn shpCode;
        private DevExpress.XtraGrid.Columns.GridColumn shpName;
        private DevExpress.XtraGrid.Columns.GridColumn objType;
        private DevExpress.XtraGrid.Columns.GridColumn DocDate;
        private DevExpress.XtraGrid.Columns.GridColumn DocNum;
        private DevExpress.XtraGrid.Columns.GridColumn SAPobj;
        private DevExpress.XtraGrid.Columns.GridColumn ErrDesc;
        private DevExpress.XtraEditors.SimpleButton CancelOK;
        private DevExpress.XtraEditors.SimpleButton QueryOK;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit ENDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit WSCode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit STDate;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraGrid.Columns.GridColumn ImportDate;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}
﻿using System;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Designer.Utils;
using ToSAP.Init;

namespace ToSAP.Query
{
    public partial class DocNSYN : XtraForm
    {
        public DocNSYN()
        {
            InitializeComponent();
        }

        private void CancelOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DocNSYN_Load(object sender, EventArgs e)
        {
            WSCode.Properties.DisplayMember = "WhsName";
            WSCode.Properties.ValueMember = "WhsCode";
            DataTable oDataTable = Common.Common.oUserAccessShop(Program.FwSqlConnection, Program.oUSER);
            oDataTable.Columns.Remove("CHS");
            oDataTable.Columns.Remove("Entry");
            oDataTable.Columns.Remove("AreaS");
            oDataTable.Columns.Remove("Area1");
            oDataTable.AcceptChanges();
            WSCode.Properties.DataSource = oDataTable;
            STDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(-1);
            ENDate.DateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddDays(-1);
            if(Program.InterFaceSet.Equals("按分支") && Program.CorpSet.Equals("是"))
            {
                checkEdit1.Visible = true;
            }
            else
            {
                checkEdit1.Visible = false;
            }
        }

        private void QueryOK_Click(object sender, EventArgs e)
        {
            Common.Common.GetFWSqlConn(this);
            var ShpCode = WSCode.EditValue.ToString();
            var ShpName = "";
            if (ShpCode.Equals(string.Empty))
            {
                if (MessageBox.Show("未选择店铺,将查询所有店铺未同步业务!", "确认是否继续", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
            }
            RunQuery oRunQuery = new RunQuery();
            oRunQuery.Show(this);
            oRunQuery.Refresh();
            
            var stDate = STDate.Text;
            var enDate = ENDate.Text;


            string sql00 = "";
            if (Program.showOWHS.Equals("店铺"))
            {
                sql00 = "SELECT 10000+DocEntry DocEntry, shpCode, shpName FROM OSHP WHERE IsInvalid=0 and shpName not like '%后仓%' and (shpCode = '"+ ShpCode + "' or '"+ ShpCode + "'='')";  //店铺  仓库 店铺和仓库
            }
            if (Program.showOWHS.Equals("仓库"))
            {
                sql00 = "SELECT DocEntry, WhsCode shpCode, WhsName shpName FROM OWHS where  (whsCode = '" + ShpCode + "' or '" + ShpCode + "'='')";  //店铺  仓库 店铺和仓库
            }
            if (Program.showOWHS.Equals("店铺和仓库"))
            {
                sql00 = "SELECT 10000+DocEntry DocEntry, shpCode, shpName FROM OSHP WHERE IsInvalid=0  and shpName not like '%后仓%' amd(shpCode = '" + ShpCode + "' or '" + ShpCode + "'='') UNION ALL SELECT DocEntry, WhsCode, WhsName FROM OWHS  where  (whsCode = '" + ShpCode + "' or '" + ShpCode + "'='')";  //店铺  仓库 店铺和仓库
            }

            string sqlCmd = string.Empty;
            sqlCmd =
                " SELECT 'shpCode' shpCode,'shpName' shpName, 'FWName' FWName ,Convert(varchar(100),T1.CreateDate) DocDate, t1.WhsCode DocNum,'Memo' Memo,'SAPobjName' SAPobj  FROM OWHS T1 " +
                " WHERE 1=2";
            DataTable ResultData = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd,null);

            DataTable oSHP = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sql00, null);
            int isQueryOne = 1;
            foreach (DataRow shpDataRow in oSHP.Rows)
            {
                ShpCode = shpDataRow["shpCode"].ToString();
                ShpName = shpDataRow["shpName"].ToString();
                string whsEntry = shpDataRow["DocEntry"].ToString();
                sqlCmd =
                    "SELECT t1.ERPobject,T1.SAPobject,T1.Contion,T2.FWName,T3.SAPobjName,T2.MasterTb,CASE WHEN T5.FWField = 'Combfield' THEN T5.FWFieldDesc ELSE 'T1.'+T5.FWField END FWField, T1.Contion,T3.SAPDIinterface FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode " +
                    " INNER JOIN ToSAP_DIobject T3 ON T1.SAPobject = T3.SAPobjCode" +
                    " INNER JOIN ToSAP_Role_Mas T4 ON T4.ERPobject = T1.ERPobject AND T4.SAPobject = T1.SAPobject" +
                    " INNER JOIN ToSAP_Role_Sub T5 ON T4.DocEntry = T5.DocEntry AND T5.DataType = '主表' AND T5.KeyField = 'Y'" +
                    " WHERE T2.DataType <> '单据' ORDER BY T2.FWName";
                var masTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                var SAPcompanyDB = Common.Common.GetSAPDBName(Program.FwSqlConnection, ShpCode);
                if (isQueryOne.Equals(1))
                {
                    foreach (DataRow oDataRow in masTable.Rows)
                    {
                        var ERPobjCode = oDataRow["ERPobject"].ToString();
                        var SAPobjCode = oDataRow["SAPobject"].ToString();
                        var FWName = oDataRow["FWName"].ToString();
                        var oContion = oDataRow["Contion"].ToString();
                        var SAPobjName = oDataRow["SAPobjName"].ToString();
                        var KeyField = oDataRow["FWField"].ToString();
                        var ERPTable = oDataRow["MasterTb"].ToString();
                        var Diinterface = oDataRow["SAPDIinterface"].ToString();
                        if (Program.InterFaceSet == "按公司" && (Diinterface == "ProfitCenter" || Diinterface == "Warehouses"))
                        {
                            oContion += " and " + KeyField +
                                      " in (SELECT WhsCode FROM ToSAP_BranchDetail WHERE BranchCode = '" + SAPcompanyDB + "') ";
                        }
                        sqlCmd = " SELECT '" + ShpCode + "' shpCode,'" + ShpName + "' shpName, '" + FWName +
                                 "' FWName ,convert(varchar(100),'') DocDate, " + KeyField + " DocNum,'' Memo,'" + SAPobjName +
                                 "' SAPobj  FROM " + ERPTable + " T1 " +
                                 " WHERE 1=1 " + oContion + " and NOT EXISTS(SELECT T00.DocEntry FROM ToSAP_ImportOK T00 WHERE " + KeyField +
                                 " = T00.DocEntry AND T00.DocType = '" + ERPobjCode + "' AND T00.SAPDocType = '" + SAPobjCode +
                                 "' AND SAPCompany = '" + SAPcompanyDB + "')";
                        var tmpTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                        ResultData.Merge(tmpTable);
                        ResultData.AcceptChanges();
                    }

                    if (Program.InterFaceSet.Equals("按分支"))
                    {
                        isQueryOne = 0;
                    }
                }
                
                masTable = new DataTable();
                
            sqlCmd =
                "SELECT (SELECT  U0.CodePrefix +CASE WHEN Isnull(U1.SQLstr,'')='' THEN '(' ELSE '+'+ U1.SQLstr+'(' END +CASE WHEN U0.FWField='Combfield' THEN U0.FWFieldDesc ELSE 'T1.'+U0.FWField END +')'  FROM ToSAP_Role_Sub U0 LEFT JOIN ToSAP_ERPDataType U1 ON U1.dataType = U0.FWDataType WHERE U0.DocEntry = T1.DocEntry AND U0.DataType = '主表' AND U0.SAPDI = 'CardCode' AND U0.KeyField = 'N' ) CardCode, t1.ERPobject,T1.SAPobject,T1.Contion,T2.FWName,T3.SAPobjName,T2.MasterTb,T2.SubTb,CASE WHEN T5.FWField = 'Combfield' THEN T5.FWFieldDesc ELSE 'T1.'+T5.FWField END FWField FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode " +
                " INNER JOIN ToSAP_DIobject T3 ON T1.SAPobject = T3.SAPobjCode and T3.SAPobjName NOT LIKE '%集团%'  " +
                " INNER JOIN ToSAP_Role_Mas T4 ON T4.ERPobject = T1.ERPobject AND T4.SAPobject = T1.SAPobject" +
                " INNER JOIN ToSAP_Role_Sub T5 ON T4.DocEntry = T5.DocEntry AND T5.DataType = '主表' AND T5.KeyField = 'Y'" +
                " WHERE T2.DataType = '单据'  ORDER BY T2.FWName";
                if(checkEdit1.CheckState == CheckState.Checked)
                {
                    sqlCmd =
                                    "SELECT (SELECT  U0.CodePrefix+CASE WHEN isnull(U1.SQLstr,'')='' THEN '(' ELSE '+'+ U1.SQLstr+'(' END +CASE WHEN U0.FWField='Combfield' THEN U0.FWFieldDesc ELSE 'T1.'+U0.FWField END +')'  FROM ToSAP_Role_Sub U0 LEFT JOIN ToSAP_ERPDataType U1 ON U1.dataType = U0.FWDataType WHERE U0.DocEntry = T1.DocEntry AND U0.DataType = '主表' AND U0.SAPDI = 'CardCode' AND U0.KeyField = 'N' ) CardCode, t1.ERPobject,T1.SAPobject,T1.Contion,T2.FWName,T3.SAPobjName,T2.MasterTb,T2.SubTb,CASE WHEN T5.FWField = 'Combfield' THEN T5.FWFieldDesc ELSE 'T1.'+T5.FWField END FWField FROM ToSAP_Role_Mas T1 INNER JOIN ToSAP_FWobject T2 ON T1.ERPobject=T2.FWCode " +
                                    " INNER JOIN ToSAP_DIobject T3 ON T1.SAPobject = T3.SAPobjCode and T3.SAPobjName LIKE '%集团%'  " +
                                    " INNER JOIN ToSAP_Role_Mas T4 ON T4.ERPobject = T1.ERPobject AND T4.SAPobject = T1.SAPobject" +
                                    " INNER JOIN ToSAP_Role_Sub T5 ON T4.DocEntry = T5.DocEntry AND T5.DataType = '主表' AND T5.KeyField = 'Y'" +
                                    " WHERE T2.DataType = '单据'  ORDER BY T2.FWName";
                }
            masTable.Clear();
            masTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
            Common.Common.GetSAPsqlConn(ShpCode);
            var BPLID = "";
            if (Program.InterFaceSet == "按分支")
            {
                string sqlCmd1 = "SELECT dbo.ToSAP_GetCB("+whsEntry+")";
                if ( checkEdit1.CheckState == CheckState.Checked)
                {
                    sqlCmd1 = "SELECT Value2 BranchName  FROM ToSAP_Appinit T1 WHERE ParaCode='CorpSub'";
                }

                try
                {
                    var BranchName = Common.Common
                    .GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd1, null).Rows[0][0].ToString();
                sqlCmd1 = "SELECT BPLId FROM OBPL WHERE BPLName = '" + BranchName + "' AND Disabled = 'N'";
                    BPLID = Common.Common
                           .GetDataTable(Program.SAPsqlConnStr.ConnectionString, sqlCmd1, null).Rows[0][0].ToString();
                }
                catch (Exception exception)
                {
                    
                }
            }
            foreach (DataRow oDataRow in masTable.Rows)
            {
                var ERPobjCode = oDataRow["ERPobject"].ToString();
                var SAPobjCode = oDataRow["SAPobject"].ToString();
                var FWName = oDataRow["FWName"].ToString();
                var SAPobjName = oDataRow["SAPobjName"].ToString();
                var KeyField = oDataRow["FWField"].ToString();
                var masterTb = oDataRow["MasterTb"].ToString();
                var SubTb = oDataRow["SubTb"].ToString();
                var Contion = oDataRow["Contion"].ToString();
                var CardCode = oDataRow["CardCode"].ToString();
                CardCode = CardCode == "" ? "+''" : "+"+CardCode;
                sqlCmd =
                    "SELECT CASE WHEN T2.FWField = 'Combfield' THEN T2.FWFieldDesc ELSE 'T1.'+T2.FWField END FWField FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject='" +
                    ERPobjCode + SAPobjCode + "' AND SAPDI = 'InCmpBch' AND T2.DataType = '主表'";
                var InCmpBch = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null).Rows[0][0].ToString();
                sqlCmd =
                    "SELECT CASE WHEN T2.FWField = 'Combfield' THEN T2.FWFieldDesc ELSE 'T1.'+T2.FWField END FWField FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject='" + ERPobjCode + SAPobjCode + "' AND SAPDI = 'DocDate' AND T2.DataType = '主表'";
                var DocDate = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null).Rows[0][0].ToString();
                sqlCmd =
                    "SELECT CASE WHEN T2.FWField = 'Combfield' THEN T2.FWFieldDesc ELSE 'T1.'+T2.FWField END FWField FROM ToSAP_Role_Mas T1 LEFT JOIN ToSAP_Role_Sub T2 ON T2.DocEntry = T1.DocEntry WHERE T1.ERPobject+T1.SAPobject='" + ERPobjCode + SAPobjCode + "' AND SAPDI = 'BPL_IDAssignedToInvoice' AND T2.DataType = '主表'";
                DataTable oDts = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                string BPLID1 = "";
                if (oDts.Rows.Count > 0)
                {
                    BPLID1 = oDts.Rows[0][0].ToString();
                }

                if (BPLID.Equals("3"))
                {
                    int m = 0;
                }
                sqlCmd = " SELECT '" + ShpCode + "' shpCode,'" + ShpName + "' shpName, '" + FWName +
                         "' FWName ,Convert(varchar(10),"+DocDate+",120) DocDate, T1.DocNum DocNum,T1.memo Memo,'" +
                         SAPobjName + "' SAPobj  FROM " + masterTb + "(NoLock) T1 "+ ( Contion.ToLower().IndexOf("t2.") > 0 ? ("Inner Join "+ SubTb + " T2 ON T1.DocEntry=T2.DocEntry ") : "" )+
                         (Contion.ToLower().IndexOf("t3.") > 0 ? ("Inner Join " + SubTb + " T2 ON T1.DocEntry=T2.DocEntry inner join OITM T3 On t2.ItemEntry=t3.DocEntry ") : "") +
                         "INNER JOIN (" + sql00 + ") T4 ON " + InCmpBch + "=T4.DocEntry AND T4.shpCode ='" + ShpCode + "'" + 
                         " WHERE Convert(date,"+ DocDate + ") >='" + stDate + "' AND Convert(Date,"+ DocDate + ") <='" + enDate + "'  " + Contion +
                         " and NOT EXISTS(SELECT T00.DocEntry FROM ToSAP_ImportOK(NoLock) T00 WHERE convert(varchar(100)," + KeyField +")"+CardCode+
                         " = T00.DocEntry AND T00.DocType = '" + ERPobjCode + "' AND T00.SAPDocType = '" + SAPobjCode + "' and T00.BPLID =case when '"+ BPLID1.Replace("'","") + "' = ''  then '' else  '" + BPLID+"' end"+
                         " AND SAPCompany = '" + SAPcompanyDB + "') group by Convert(varchar(10)," + DocDate + ",120) , T1.DocNum ,T1.memo" ;
                    sqlCmd = sqlCmd.Replace("SAPDBName", SAPcompanyDB).Replace("BPLID_In", BPLID);
                var tmpTable = Common.Common.GetDataTable(Program.FWSqlConnStr.ConnectionString, sqlCmd, null);
                ResultData.Merge(tmpTable);
                ResultData.AcceptChanges();
            }

            
            }
            gridControl1.DataSource = ResultData;
            oRunQuery.Close();
            if (ResultData.Rows.Count < 1)
            {
                MessageBox.Show("数据已全部同步完毕,没有未同步的数据!");
            }
        }
    }
}

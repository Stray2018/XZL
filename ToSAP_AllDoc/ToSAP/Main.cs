﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Windows.Forms;
using AutoUpdate;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using ToSAP.Init;
using ToSAP.Query;
using ToSAP.Set;

namespace ToSAP
{
    public partial class Main : XtraForm
    {
        public Main()
        {
            InitializeComponent();
            barManager.ForceInitialize();

        }

        private void Main_Load(object sender, EventArgs e)
        {
            Common.Common.AlterDataBase();
            Common.Common.GetFWSqlConn(this);
            Height = Convert.ToInt32(Screen.PrimaryScreen.Bounds.Height * 0.9);
            Width = Convert.ToInt32(Screen.PrimaryScreen.Bounds.Width * 0.9);
            Top = Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2;
            Left = Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2;
            this.Text += "  DataBase:"+Program.FWSqlConnStr.InitialCatalog +"    Version:"+ System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            try
            {
                if (Common.Common.GetInterFaceMode(Program.FwSqlConnection) != "按分支")
                {
                    BranchSet.Caption = "公司门店设置";
                }
                Common.Common.GetAppInit(null);


                var oNewDocNsyn = new ToSYN.ToSYN();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "数据同步";
                oNewDocNsyn.Show();
            }
            catch (Exception exception)
            {
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            string sqlCmd = "delete ToSAP_UserLogin where usercode = '"+Program.oUSER+"'";
            Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
            Program.FwSqlConnection.Close();
            GC.Collect();
            Process.GetCurrentProcess().Kill();
           
        }


        private void AppInit_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("AppInit", this))
            {
                var oNewDocNsyn = new AppInit();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "初始化设置";
                oNewDocNsyn.Show();
            }
        }

        private void BranchSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (Common.Common.GetInterFaceMode(Program.FwSqlConnection) == "按分支")
            {
                if (!Common.Common.IsExistWindow("BranchSet", this))
                {
                    var oNewDocNsyn = new BranchSet();
                    oNewDocNsyn.MdiParent = this;
                    oNewDocNsyn.Text = "分支门店设置";
                    oNewDocNsyn.Show();
                }
            }
            else
            {
                if (!Common.Common.IsExistWindow("CorpSet", this))
                {
                    var oNewDocNsyn = new CorpSet();
                    oNewDocNsyn.MdiParent = this;
                    oNewDocNsyn.Text = "公司门店设置";
                    oNewDocNsyn.Show();
                }
            }
        }

        private void FWsynObjectSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("FWsynObjectSet", this))
            {
                var oNewDocNsyn = new FWsynObjectSet();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "ERP 数据对象设置";
                oNewDocNsyn.Show();
            }
        }

        private void FWobjData_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("FWobjData", this))
            {
                var oNewDocNsyn = new FWobjData();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "ERP 数据字段设置";
                oNewDocNsyn.Show();
            }
        }

        private void SAPsynObjectSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("SAPsynObjectSet", this))
            {
                var oNewDocNsyn = new SAPsynObjectSet();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "SAP DI 对象设置";
                oNewDocNsyn.Show();
            }
        }

        private void SAPDIData_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("SAPDIData", this))
            {
                var oNewDocNsyn = new SAPDIData();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "SAP DI 对象属性设置";
                oNewDocNsyn.Show();
            }
        }

        private void FWToSAPSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("FWToSAPSet", this))
            {
                var oNewDocNsyn = new FWToSAPSet();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "同步逻辑设置";
                oNewDocNsyn.Show();
            }
        }

        private void ToSAPUserSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("synUser", this))
            {
                var oNewDocNsyn = new synUser();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "同步帐号设置";
                oNewDocNsyn.Show();
            }
        }

        private void PlanToSAP_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("PlanToSAP", this))
            {
                var oNewDocNsyn = new PlanToSAP();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "自动同步设置";
                oNewDocNsyn.Show();
            }
        }

        private void ToSYN_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("ToSYN", this))
            {
                var oNewDocNsyn = new ToSYN.ToSYN();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "数据同步";
                oNewDocNsyn.Height = (int)(Screen.PrimaryScreen.Bounds.Height * 0.8);
                oNewDocNsyn.Show();
            }
        }

        private void mSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("mSet", this))
            {
                var oNewDocNsyn = new mSet();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "数据库配置";
                oNewDocNsyn.Show();
            }
        }

        private void iRoleNSYN_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("DocNSYN", this))
            {
                var oNewDocNsyn = new DocNSYN();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "未同步业务查询";
                oNewDocNsyn.Show();
            }
        }

        private void iQuery_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("ErrQuery", this))
            {
                var oNewDocNsyn = new ErrQuery();
                oNewDocNsyn.MdiParent = this;
                oNewDocNsyn.Text = "同步错误查询";
                oNewDocNsyn.Show();
            }
        }
        private void CheckUpdate_ItemClick(object sender, ItemClickEventArgs e)
        {
            AppUpdater oAppUpdater = new AppUpdater();
            XmlFiles updaterXmlFiles = null;
            int Err = -1;
            
            string locXML = Application.StartupPath + "\\AutoUpdaterList.xml";
            try
            {
                
                updaterXmlFiles = new XmlFiles(locXML);
            }
            catch
            {
                Err++;
            }

           oAppUpdater.UpdaterUrl= updaterXmlFiles.GetNodeValue("//Url")+"/AutoUpdaterList.xml";
            string tempUpdatePath = "";
            try
            {
                tempUpdatePath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + updaterXmlFiles.FindNode("//Application").Attributes["applicationId"].Value + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                oAppUpdater.DownAutoUpdateFile(tempUpdatePath);
            }
            catch
            {
                Err++;

            }
            string serXML = tempUpdatePath+ "\\AutoUpdaterList.xml";;
            if (Err > -1)
            {
                MessageBox.Show("更新配置错误,请检查更新配置!");
                return;
            }
            Hashtable htUpdateFile = new Hashtable();
           int i= oAppUpdater.CheckForUpdate(serXML,locXML,out htUpdateFile);
            if (i > 0)
            {
                string appPath = Application.StartupPath + "\\AutoUpdate.exe";
                Process.Start(appPath);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!Common.Common.IsExistWindow("ForwinGroupToSAPGroup", this))
            {
                var ForwinGroupToSAPGroup = new ForwinGroupToSAPGroup();
                ForwinGroupToSAPGroup.MdiParent = this;
                //ForwinGroupToSAPGroup.Text = "同步错误查询";
                ForwinGroupToSAPGroup.Show();
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string sqlCmd = "delete ToSAP_UserLogin where usercode = '" + Program.oUSER + "'";
                Common.Common.ExecSqlcmd(Program.FwSqlConnection, sqlCmd);
                if (Program.thread.IsAlive)
                {
                    MessageBox.Show("还有任务在执行，请等待！");
                    e.Cancel = true;

                }
            }
            catch (Exception exception)
            {
               
            }
            
        }
    }
}

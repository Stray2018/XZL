﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using SAPbobsCOM;
using SAPbouiCOM.Framework;

namespace BusplaceClose
{

    class Program
    {
        public static SAPbobsCOM.Company oCompany = new CompanyClass();
        public static string BPLId = string.Empty;
        public static string BPLName = String.Empty;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    oApp = new Application(args[0]);
                }
                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();
                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                oCompany =(SAPbobsCOM.Company) SAPbouiCOM.Framework.Application.SBO_Application.Company.GetDICompany();
                CreateField();
                RunKill();
                oApp.Run();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    //Exit Add-On
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    break;
                default:
                    break;
            }
        }

        static void CreateField()
        {
            SAPbobsCOM.UserFieldsMD oUserFieldsMd =(SAPbobsCOM.UserFieldsMD) oCompany.GetBusinessObject(BoObjectTypes.oUserFields);
            oUserFieldsMd.Name = "BusPlaceName";
            oUserFieldsMd.Description = "关帐分支名称";
            oUserFieldsMd.TableName = "OJDT";
            oUserFieldsMd.Type = BoFieldTypes.db_Alpha;
            oUserFieldsMd.SubType = BoFldSubTypes.st_None;
            oUserFieldsMd.EditSize = 120;
            oUserFieldsMd.Add();
        }

        private static void KillThis()
        {
            var hh = DateTime.Now.Hour;
            if (hh.Equals(5))
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private static void RunKill()
        {
            Timer oTimer = new Timer(3600000);
            oTimer.Elapsed += OTimer_Elapsed;
            oTimer.Start();
        }

        private static void OTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            KillThis();
        }
    }
}

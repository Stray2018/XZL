﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;
using Application = SAPbouiCOM.Application;

namespace BusplaceClose
{
    [FormAttribute("60410", "BusplaceClose.b1f")]
    class BusplaceClose : SystemFormBase
    {
        public BusplaceClose()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_1").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Busplace").Specific));
            this.ComboBox0.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox0_ComboSelectAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.LoadBefore += new SAPbouiCOM.Framework.FormBase.LoadBeforeHandler(this.Form_LoadBefore);
            this.LoadAfter += new LoadAfterHandler(this.Form_LoadAfter);

        }

        private void OnCustomInitialize()
        {

        }

        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.ComboBox ComboBox0;

        private void Form_LoadBefore(SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
           // throw new System.NotImplementedException();
            

        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbobsCOM.BusinessPlaces oBusinessPlaces = (SAPbobsCOM.BusinessPlaces)
                Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPlaces);

            SAPbobsCOM.Users oUsers = (SAPbobsCOM.Users)Program.oCompany.GetBusinessObject(BoObjectTypes.oUsers);
            oUsers.GetByKey(Program.oCompany.UserSignature);
            for (int i = 0; i < oUsers.UserBranchAssignment.Count; i++)
            {
                oUsers.UserBranchAssignment.SetCurrentLine(i);
                int BplId = oUsers.UserBranchAssignment.BPLID;
                oBusinessPlaces.GetByKey(BplId);
                string BplName = oBusinessPlaces.BPLName;
                if (oBusinessPlaces.Disabled == BoYesNoEnum.tNO)
                {
                    ComboBox0.ValidValues.Add(BplId.ToString(), BplName);
                }
               
            }

        }

        private SAPbouiCOM.Button Button0;

        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                
                if (this.ComboBox0.Selected==null)
                {
                    BubbleEvent = false;
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("未选择关帐分支，请选择！");
                }
            }
            catch (Exception e)
            {
                BubbleEvent = false;
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
            // throw new System.NotImplementedException();

        }

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
           
        }

        private void ComboBox0_ComboSelectAfter(object sboObject, SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            try
            {

                Program.BPLId = this.ComboBox0.Value;
                Program.BPLName = this.ComboBox0.Selected.Description;
                string stDate = String.Empty;
                string enDate = String.Empty;
                try
                {
                    stDate = ((SAPbouiCOM.ComboBox)this.GetItem("31").Specific).Selected.Description + "-01";

                    string enDatestr = ((SAPbouiCOM.ComboBox)this.GetItem("32").Specific).Selected.Description + "-01";
                     enDate = DateTime.Parse(enDatestr).AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");
                }
                catch (Exception e)
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("关帐期间未选择,请先选择关帐期间!");
                   return;
                }
               
                SAPbobsCOM.Recordset oRec =
                    (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                string sqlCmd =
                    "UPDATE T1 SET T1.U_BusPlaceName = T3.BPLName from OJDT T1 INNER JOIN(SELECT TransId, BPLId FROM JDT1 GROUP BY TransId, BPLId) T2 ON T2.TransId = T1.TransId INNER JOIN OBPL T3 ON T3.BPLId = T2.BPLId " +
                    "WHERE T1.RefDate >= '" + stDate + "' AND T1.RefDate <= '" + enDate + "' AND T3.BPLID = '" + Program.BPLId + "'";
                oRec.DoQuery(sqlCmd);
                ((SAPbouiCOM.Button)this.GetItem("234000001").Specific).Item.Click(BoCellClickType.ct_Regular);
                SAPbouiCOM.Form oChoseForm = SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm;
                ((SAPbouiCOM.Button)(oChoseForm.Items.Item("540002066").Specific)).Item.Click(BoCellClickType.ct_Regular);
                SAPbouiCOM.Form oChoseResult = SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm;
                SAPbouiCOM.Matrix oGrid = (SAPbouiCOM.Matrix)oChoseResult.Items.Item("540000032").Specific;
                int Rowid = 0;
                for (int i = 0; i < oGrid.RowCount; i++)
                {
                    string ColumValue = ((SAPbouiCOM.EditText)oGrid.GetCellSpecific("540000001", i + 1)).Value;
                    if (ColumValue.Contains("关帐分支名称"))
                    {
                        Rowid = i + 1;
                        break;
                    }
                }
                ((SAPbouiCOM.ComboBox)oGrid.Columns.Item("540000003").Cells.Item(Rowid).Specific).Select(0, BoSearchKey.psk_Index);
               ((SAPbouiCOM.ComboBox)oGrid.Columns.Item("540000003").Cells.Item(Rowid).Specific).Select("等于", BoSearchKey.psk_ByDescription);
                ((SAPbouiCOM.ComboBox)oGrid.Columns.Item("540000005").Cells.Item(Rowid).Specific).Select(0, BoSearchKey.psk_ByValue);
                SAPbouiCOM.Framework.Application.SBO_Application.SendKeys("{TAB}");
                ((SAPbouiCOM.ComboBox)oGrid.Columns.Item("540000005").Cells.Item(Rowid).Specific).Select(Program.BPLName, BoSearchKey.psk_ByDescription);
                ((SAPbouiCOM.EditText) oGrid.Columns.Item("1470000017").Cells.Item(Rowid).Specific).Value =Program.BPLName;
                //SAPbouiCOM.Framework.Application.SBO_Application.SendKeys("{TAB}");
                //oChoseResult.Items.Item(1).Click(BoCellClickType.ct_Regular);
                  SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Items.Item("1").Click(BoCellClickType.ct_Regular);
                SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Items.Item("1").Click(BoCellClickType.ct_Regular);

                Button0.Item.Click(BoCellClickType.ct_Regular);
                oChoseResult = SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm;
                ((SAPbouiCOM.EditText) oChoseResult.Items.Item("14").Specific).Active = true;
                ((SAPbouiCOM.ComboBox) oChoseResult.Items.Item("1320000026").Specific).Item.Enabled = true;
               ((SAPbouiCOM.ComboBox)oChoseResult.Items.Item("1320000026").Specific).Select(Program.BPLName, BoSearchKey.psk_ByDescription);
                ((SAPbouiCOM.EditText)oChoseResult.Items.Item("14").Specific).Active = true;
                ((SAPbouiCOM.ComboBox) oChoseResult.Items.Item("1320000026").Specific).Item.Enabled = false;
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.ToString());

            }
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="WebApp1.Logon" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>

<form runat="server">
    <table style ="width: 100%; height: 100%">
        <tr>
            <td>
                <div style="width: 285px; margin: 0 auto;top: 100px; ">
                    <asp:Label ID="Label3" runat="server" Text="用户:"></asp:Label>
                    <asp:TextBox ID="TextBox1" runat="server" Width="230px"></asp:TextBox>
                    <br/>
                    <br/>
                    <asp:Label ID="Label4" runat="server" Text="密码:"></asp:Label>
                    <asp:TextBox ID="TextBox2" runat="server" TextMode="Password" Width="230px"></asp:TextBox>
                    <br />
                    <br/>
                    <br/>
                    <asp:Button ID="oLogon" Text="登陆" runat="server" OnClick="oLogon_OnClick" Height="30px" Width="80px" Font-Bold="True" Font-Size="14pt"/>
                </div>
            </td>
        </tr>
    </table>
            
    </form>
</body>
</html>

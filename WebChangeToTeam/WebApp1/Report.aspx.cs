﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;

namespace WebApp1
{
    public partial class Report : System.Web.UI.Page
    {
        public static string sqlConn = "";
      // public static CrystalReportSource CrystalReportSource1 = new CrystalReportSource();
        protected void Page_Load(object sender, EventArgs e)
        {
            string ReportFile = Server.MapPath("~/bin/Report.rpt");
            CrystalReportSource1.Report.FileName = ReportFile;
            CrystalReportSource1.ReportDocument.Load(ReportFile);
            CrystalReportViewer1.Visible = false;
            if (!IsPostBack)
            {
                //Button2.Enabled = false;

                sqlConn = System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"];
                string userCode = Request.Cookies["StraySAP"].Value;
                //DataTable authorTable = GetAuthorData(userCode);
                //string ShpDeploy = authorTable.Rows[0]["ShpDeploy"].ToString().Trim();
                //string oneStr = ShpDeploy.Length < 1 ? "" : ShpDeploy.Substring(0, 1);
                //string oCondition = " OR DocEntry in (" + authorTable.Rows[0]["ShpDeploy"].ToString().Trim() + ")";
                //if (oneStr.Equals(""))
                //{
                //    oCondition = "";
                //}
                //if (oneStr.Equals("-"))
                //{
                //    oCondition = " OR DocEntry in (" + GetOldDocentry("OSHP", userCode) + ")";
                //    ;
                //}

                //string sqlCmd = "select shpName,shpCode FROM OSHP where '" +
                //                authorTable.Rows[0]["ShpAll"].ToString().Trim() + "' = 'True' " + oCondition;

                string sqlCmd =
                    "SELECT T3.DepCode shpCode,T3.DepName shpName FROM OUSR T1 LEFT JOIN OHEM T2 ON T1.UserCode=T2.UserID LEFT JOIN CMP1 T3 ON T2.DepEntry=T3.DocEntry WHERE T1.UserCode='"+ userCode + "'";
                DataTable shpData = GetDataTable(sqlConn, sqlCmd);
               
                DropDownList1.Items.Clear();
                DropDownList1.DataSource = shpData;
                DropDownList1.DataTextField = "shpName";
                DropDownList1.DataValueField = "shpCode";
                DropDownList1.DataBind();
                DateTime nowtime = DateTime.Now.Date;
                TextBox2.Text = nowtime.ToString("yyyy-MM-dd HH:mm:ss");
                TextBox4.Text = nowtime.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy-MM-dd HH:mm:ss");
                int Hour = DateTime.Now.Hour;
                sqlCmd = "SELECT '' TeamCode,'' TeamDescr UNION ALL SELECT TeamCode,TeamDescr FROM TEAM";
                DataTable oBC = GetDataTable(sqlConn, sqlCmd);
                DropDownList2.Items.Clear();
                DropDownList2.DataSource = oBC;
                DropDownList2.DataTextField = "TeamCode";
                DropDownList2.DataValueField = "TeamDescr";
                DropDownList2.DataBind();
                //string Bc = Hour >= 8 && Hour <= 15 ? "A班" : "B班";
                DropDownList2.Text ="";
                string shpName = shpData.Rows.Count > 0 ? shpData.Rows[0]["shpName"].ToString() : "";
                string sqlConnstr = System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"];
                SqlConnectionStringBuilder oStringBuilder = new SqlConnectionStringBuilder(sqlConnstr);
                CrystalReportSource1.ReportDocument.SetDatabaseLogon(oStringBuilder.UserID, oStringBuilder.Password, oStringBuilder.DataSource, oStringBuilder.InitialCatalog,true);
                CrystalReportSource1.ReportDocument.SetParameterValue("bc", DropDownList2.SelectedItem.Text);
                CrystalReportSource1.ReportDocument.SetParameterValue("stDate", TextBox2.Text);
                CrystalReportSource1.ReportDocument.SetParameterValue("enDate", TextBox4.Text);
                CrystalReportSource1.ReportDocument.SetParameterValue("shpx", shpName);
                CrystalReportViewer1.ReportSource = CrystalReportSource1.ReportDocument;
                PrintDocument fPrintDocument = new PrintDocument();    //获取默认打印机的方法
                Label1.Width = 80;
                string defPrint = fPrintDocument.PrinterSettings.PrinterName;
                //Label5.Font.Size = 8;
                //Label5.Font.Name = "宋休";
                //Label5.Text = "打印机:"+defPrint;
            }
            
        }
        private string
            GetOldDocentry(string oTable, string oUserCode)
        {
            var sqlCmd = "DECLARE @str NVARCHAR(max);" +
                         "SELECT @str = '';" +
                         "WITH Dept AS(" +
                         "SELECT DocEntry, DocEntry oldDocEntry, DepCode, DepName FROM CMP1 WHERE DepLevel = 1 " +
                         "UNION ALL " +
                         "SELECT t1.DocEntry, t2.DocEntry, t2.DepCode, t2.DepName FROM Dept t1 INNER JOIN CMP1 t2 ON t1.DepCode = t2.BaseCode) " +
                         "SELECT @str = @str + ',' + CONVERT(NVARCHAR(100), DocEntry) FROM " + oTable +
                         " WHERE 1=1 and DepEntry IN(SELECT oldDocEntry FROM Dept WHERE CONVERT(VARCHAR(100), DocEntry * -1) IN(SELECT ShpDeploy FROM OPWD INNER JOIN OUSR ON OUSR.DocEntry = OPWD.UserEntry WHERE UserCode = '" +
                         oUserCode + "'));" +
                         "SELECT RIGHT(@str, LEN(@str) - 1); ";
            var oDataTable = GetDataTable(sqlConn, sqlCmd);
            return oDataTable.Rows[0][0].ToString();
        }
        private DataTable GetAuthorData(string UserCode)
        {
            var sqlCmd =
                "SELECT T1.* FROM OPWD T1 LEFT JOIN OUSR T2 ON T1.UserEntry=T2.DocEntry WHERE 1=1 and T2.UserCode='" +
                UserCode + "'";
            var oDataTable = GetDataTable(sqlConn, sqlCmd);
            return oDataTable;
        }
        private DataTable GetDataTable(string Connstr, string sqlcmd)
        {
            using (var oConnection = new SqlConnection(Connstr))
            {
                oConnection.Open();
                var oDataTable = new DataTable("MYdata");
                var oSqlDataAdapter = new SqlDataAdapter(sqlcmd, oConnection);
                oSqlDataAdapter.Fill(oDataTable);
                oSqlDataAdapter.Dispose();
                return oDataTable;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            //string ReportFile = Server.MapPath("/bin/Report.rpt");
            //CrystalReportSource1.Report.FileName = ReportFile;
            //CrystalReportSource1.ReportDocument.Load(ReportFile);
            CrystalReportSource1.ReportDocument.DataSourceConnections.Clear();
            string sqlConnstr = System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"];
            SqlConnectionStringBuilder oStringBuilder = new SqlConnectionStringBuilder(sqlConnstr);
            TableLogOnInfo oLogOnInfo = new TableLogOnInfo();
            oLogOnInfo.ConnectionInfo.ServerName = oStringBuilder.DataSource;
            oLogOnInfo.ConnectionInfo.UserID = oStringBuilder.UserID;
            oLogOnInfo.ConnectionInfo.DatabaseName = oStringBuilder.InitialCatalog;
            oLogOnInfo.ConnectionInfo.Password = oStringBuilder.Password;
            foreach (CrystalDecisions.CrystalReports.Engine.Table tb in CrystalReportSource1.ReportDocument.Database.Tables)
            {
                tb.ApplyLogOnInfo(oLogOnInfo);
            }
           
            CrystalReportSource1.ReportDocument.SetDatabaseLogon(oStringBuilder.UserID, oStringBuilder.Password, oStringBuilder.DataSource, oStringBuilder.InitialCatalog,true);
            CrystalReportSource1.ReportDocument.SetParameterValue("bc", DropDownList2.SelectedItem.Text);
            CrystalReportSource1.ReportDocument.SetParameterValue("stDate", TextBox2.Text);
            CrystalReportSource1.ReportDocument.SetParameterValue("enDate", TextBox4.Text);
            CrystalReportSource1.ReportDocument.SetParameterValue("shpx", DropDownList1.SelectedItem.Text);
           CrystalReportSource1.DataBind();
           CrystalReportViewer1.ReportSource = CrystalReportSource1.ReportDocument;
           ExportOptions exportOpts = new ExportOptions();
            PdfRtfWordFormatOptions pdfOpts = ExportOptions.CreatePdfRtfWordFormatOptions();
            
            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
            exportOpts.ExportFormatOptions = pdfOpts;
            string userCode = Request.Cookies["StraySAP"].Value;
            string exportFile = Server.MapPath("~/bin/ExData/" + userCode + ".pdf");
            CrystalReportSource1.ReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, exportFile);
            string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace("/Report.aspx", "");
            url = url + "/PDFPrievw.aspx?PDFfile=" + userCode;
            Response.Write("<script language='javascript'>window.open('" + url + "');</script>");
            //Button2.Enabled = true;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            PrintDocument fPrintDocument = new PrintDocument();    //获取默认打印机的方法

           string defPrint= fPrintDocument.PrinterSettings.PrinterName;
            CrystalReportSource1.ReportDocument.PrintOptions.PrinterName = defPrint;
            //string sqlConnstr = System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"];
            //SqlConnectionStringBuilder oStringBuilder = new SqlConnectionStringBuilder(sqlConnstr);
            //CrystalReportSource1.ReportDocument.SetDatabaseLogon(oStringBuilder.UserID, oStringBuilder.Password, oStringBuilder.DataSource, oStringBuilder.InitialCatalog);
            //CrystalReportSource1.ReportDocument.SetParameterValue("bc", TextBox1.Text);
            //CrystalReportSource1.ReportDocument.SetParameterValue("date", TextBox2.Text);
            //CrystalReportSource1.ReportDocument.SetParameterValue("shpx", DropDownList1.SelectedItem.Text);
            CrystalReportSource1.ReportDocument.PrintToPrinter(1,false,0,0);
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
           
        }

        protected void Button2_Click1(object sender, EventArgs e)
        {
            //string ReportFile = Server.MapPath("/bin/Report.rpt");
            //CrystalReportSource1.Report.FileName = ReportFile;
            //CrystalReportSource1.ReportDocument.Load(ReportFile);
            string sqlConnstr = System.Web.Configuration.WebConfigurationManager.AppSettings["MyDB"];
            SqlConnectionStringBuilder oStringBuilder = new SqlConnectionStringBuilder(sqlConnstr);
            TableLogOnInfo oLogOnInfo = new TableLogOnInfo();
            oLogOnInfo.ConnectionInfo.ServerName = oStringBuilder.DataSource;
            oLogOnInfo.ConnectionInfo.UserID = oStringBuilder.UserID;
            oLogOnInfo.ConnectionInfo.DatabaseName = oStringBuilder.InitialCatalog;
            oLogOnInfo.ConnectionInfo.Password = oStringBuilder.Password;

            CrystalReportSource1.ReportDocument.SetDatabaseLogon(oStringBuilder.UserID, oStringBuilder.Password, oStringBuilder.DataSource, oStringBuilder.InitialCatalog);
            CrystalReportSource1.ReportDocument.SetParameterValue("bc", DropDownList2.SelectedItem.Text);
            CrystalReportSource1.ReportDocument.SetParameterValue("stDate", TextBox2.Text);
            CrystalReportSource1.ReportDocument.SetParameterValue("enDate", TextBox4.Text);
            CrystalReportSource1.ReportDocument.SetParameterValue("shpx", DropDownList1.SelectedItem.Text);

            ExportOptions exportOpts = new ExportOptions();
            PdfRtfWordFormatOptions pdfOpts = ExportOptions.CreatePdfRtfWordFormatOptions();
            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
            exportOpts.ExportFormatOptions = pdfOpts;
            // CrystalReportSource1.ReportDocument.ExportToHttpResponse(exportOpts, System.Web.HttpContext.Current.Response, false, "");
            string userCode = Request.Cookies["StraySAP"].Value;
            string exportFile = Server.MapPath("~/bin/ExData/"+ userCode+".pdf");
            CrystalReportSource1.ReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, exportFile);
            //Response.Write("<script>window.open(PDFPrievw.aspx','_blank')</script>");

            string url = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString();
            url=url+ "/PDFPrievw.aspx?PDFfile="+ userCode;
            Response.Write("<script language='javascript'>window.open('" + url + "');</script>");
            //Response.Write("<script>window.showModelessDialog('PDFPrievw.aspx?PDFfile="+ userCode + "')</script>");
           // Priview(this, exportFile);
            
        }
        public static void Priview(System.Web.UI.Page p, string inFilePath) 
        {
            p.Response.ContentType = "Application/pdf";
            string fileName = inFilePath.Substring(inFilePath.LastIndexOf('\\') + 1);
            p.Response.AddHeader("content-disposition", "filename=" + fileName);
            p.Response.WriteFile(inFilePath);
            p.Response.End();
        }
    }
}
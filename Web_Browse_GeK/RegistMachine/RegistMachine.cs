﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistMachine
{
    public partial class RegistMachine : Form
    {
        public RegistMachine()
        {
            InitializeComponent();
        }

        private void btnRegist_Click(object sender, EventArgs e)
        {
            //string fileName = string.Empty;
            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //if (openFileDialog.ShowDialog() == DialogResult.OK)
            //{
            //    fileName = openFileDialog.FileName;
            //}
            //else
            //{
            //    return;
            //}
            //string localFileName = string.Concat(
            //    Environment.CurrentDirectory,
            //    Path.DirectorySeparatorChar,
            //    RegistFileHelper.ComputerInfofile);

            //if (txtFile.Text != localFileName)
            //    File.Copy(txtFile.Text, localFileName, true);
            //string computer = RegistFileHelper.ReadComputerInfoFile();
            //EncryptionHelper help = new EncryptionHelper(EncryptionKeyEnum.KeyB);
            //string md5String = help.GetMD5String(computer);
            //string registInfo = help.EncryptString(md5String);
            //RegistFileHelper.WriteRegistFile(registInfo);
            //MessageBox.Show("注册码已生成");
            string computer = txtFile.Text;
            EncryptionHelper help=new EncryptionHelper(EncryptionKeyEnum.KeyB);
            if (ZLF.Checked)
            {
                computer += "ZLF000";
            }
            if (ZDS.Checked)
            {
                computer += "ZDS000";
            }
            if (ALL1.Checked)
            {
                computer += "ZLFZDS";
            }
            //string md5String = help.GetMD5String(computer);
            string registInfo = help.EncryptString(computer);
            textBox1.Text = registInfo;
            Clipboard.SetDataObject(registInfo);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string fileName = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = openFileDialog.FileName;
            }
            else
            {
                return;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(textBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            txtFile.Text = "";
        }
    }
}

﻿namespace RegistMachine
{
    partial class RegistMachine
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegist = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ZLF = new System.Windows.Forms.RadioButton();
            this.ZDS = new System.Windows.Forms.RadioButton();
            this.ALL1 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btnRegist
            // 
            this.btnRegist.Location = new System.Drawing.Point(57, 181);
            this.btnRegist.Name = "btnRegist";
            this.btnRegist.Size = new System.Drawing.Size(63, 23);
            this.btnRegist.TabIndex = 0;
            this.btnRegist.Text = "生成";
            this.btnRegist.UseVisualStyleBackColor = true;
            this.btnRegist.Click += new System.EventHandler(this.btnRegist_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(79, 10);
            this.txtFile.Multiline = true;
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(352, 30);
            this.txtFile.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "注册信息";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(337, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "关闭";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "注册码";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(79, 57);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(352, 54);
            this.textBox1.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(437, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(38, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "复制";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(198, 181);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "新";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ZLF
            // 
            this.ZLF.AutoSize = true;
            this.ZLF.Location = new System.Drawing.Point(79, 126);
            this.ZLF.Name = "ZLF";
            this.ZLF.Size = new System.Drawing.Size(59, 16);
            this.ZLF.TabIndex = 8;
            this.ZLF.TabStop = true;
            this.ZLF.Text = "周六福";
            this.ZLF.UseVisualStyleBackColor = true;
            // 
            // ZDS
            // 
            this.ZDS.AutoSize = true;
            this.ZDS.Location = new System.Drawing.Point(175, 126);
            this.ZDS.Name = "ZDS";
            this.ZDS.Size = new System.Drawing.Size(59, 16);
            this.ZDS.TabIndex = 9;
            this.ZDS.TabStop = true;
            this.ZDS.Text = "周大生";
            this.ZDS.UseVisualStyleBackColor = true;
            // 
            // ALL1
            // 
            this.ALL1.AutoSize = true;
            this.ALL1.Location = new System.Drawing.Point(257, 126);
            this.ALL1.Name = "ALL1";
            this.ALL1.Size = new System.Drawing.Size(47, 16);
            this.ALL1.TabIndex = 10;
            this.ALL1.TabStop = true;
            this.ALL1.Text = "所有";
            this.ALL1.UseVisualStyleBackColor = true;
            // 
            // RegistMachine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 257);
            this.Controls.Add(this.ALL1);
            this.Controls.Add(this.ZDS);
            this.Controls.Add(this.ZLF);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.btnRegist);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegistMachine";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "注册机";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRegist;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RadioButton ZLF;
        private System.Windows.Forms.RadioButton ZDS;
        private System.Windows.Forms.RadioButton ALL1;
    }
}


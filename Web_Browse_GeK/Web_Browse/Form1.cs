﻿using Gecko;
using Gecko.DOM;
using Gecko.WebIDL;
using HtmlAgilityPack;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Web_Browse.Regist;
using static Gecko.ObserverNotifications;

namespace Web_Browse
{
    public partial class Form1 : Form
    {

        private string encryptComputer = string.Empty;
        private bool isRegist = false;
        public static int isLoad = 0;
        public static string registData = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //检查是否注册
            string computer = ComputerInfo.GetComputerInfo();
            encryptComputer = new EncryptionHelper().EncryptString(computer);
            //radioButton1.Checked = true;
            radioButton1.Visible = false;
            radioButton2.Visible = false;
            radioButton3.Visible = false;
            this.textBox1.Enabled = false;
            if (CheckRegist())
            {
               this.Text = this.Text+"     已注册";
                isRegist = true;
                if (registData.Contains("ZLF000"))
                {
                    radioButton1.Visible = false;
                    radioButton2.Visible = false;
                    radioButton3.Visible = false;
                    textBox1.Text = "https://yun.zlf.cn/";
                }
                if (registData.Contains("ZLFZDS"))
                {
                    radioButton1.Visible = true;
                    radioButton2.Visible = true;
                    radioButton3.Visible = true;
                    radioButton1.Enabled = true;
                    radioButton2.Enabled = true;
                    radioButton3.Enabled = true;

                }
                if (registData.Contains("ZDS000"))
                {
                    radioButton1.Visible = true;
                    radioButton2.Visible = false;
                    radioButton3.Visible = true;
                    radioButton1.Enabled = true;
                    radioButton2.Enabled = false;
                    radioButton3.Enabled = true;

                }
            }
            else
            {
                radioButton1.Visible = true;
                radioButton2.Visible = true;
                radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton3.Visible = true;
                radioButton3.Enabled = true;
                this.Text = this.Text+"  未注册,请联系Henry.Huang@scxzlkj.com";
                isRegist = false;

            }

            this.WindowState = FormWindowState.Maximized;
           
            initBrowse(this.textBox1.Text);
        }



        private bool CheckRegist()
        {
            EncryptionHelper helper = new EncryptionHelper();
           // string md5key = helper.GetMD5String(encryptComputer);
            return CheckRegistData(encryptComputer);
        }
        private bool CheckRegistData(string key)
        {
            if (RegistFileHelper.ExistRegistInfofile() == false)
            {
                RegistFileHelper.DeleRegistInfofile();
                isRegist = false;
                return false;
            }
            else
            {
                string info = RegistFileHelper.ReadRegistFile();
                var helper = new EncryptionHelper(EncryptionKeyEnum.KeyB);
                registData = helper.DecryptString(info);
                if (registData.Substring(0,registData.Length-6) == key)
                {
                    isRegist = true;
                    return true;
                }
                else
                {
                    isRegist = false;
                    RegistFileHelper.DeleRegistInfofile();
                    return false;
                }
            }
        }

        public Gecko.GeckoWebBrowser oWebBrowser = new Gecko.GeckoWebBrowser() { Dock = DockStyle.Fill };

        public void initBrowse(string oUri)
        {
            var app_dir = Environment.CurrentDirectory;
            string directory = Path.Combine(app_dir, "Cookies", Environment.CurrentDirectory+"\\Temp\\");//cookie目录
            if (!System.IO.Directory.Exists(directory))
                System.IO.Directory.CreateDirectory(directory);//检测目录是否存在
            Gecko.Xpcom.ProfileDirectory = directory;//绑定cookie目录
         Xpcom.Initialize(Path.Combine(app_dir, "Firefox64"));//初始化 Xpcom
            //browser = new GeckoWebBrowser() { Dock = DockStyle.Fill }; //创建浏览器实例
            this.oWebBrowser.Name = "browser";
            GeckoPreferences.User["gfx.font_rendering.graphite.enabled"] = true;//设置偏好：字体
            GeckoPreferences.User["privacy.donottrackheader.enabled"] = true;//设置浏览器不被追踪
            GeckoPreferences.User["general.useragent.override"] = "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:59.0) Gecko/20100101 Firefox/59.0";
            GeckoPreferences.User["intl.accept_languages"] = "zh-CN,zh;q=0.9,en;q=0.8";//不设置的话默认是英文区
            
            oWebBrowser.UseHttpActivityObserver = true;//开启拦截请求
                                                       // oWebBrowser.ObserveHttpModifyRequest += Browser_ObserveHttpModifyRequest;/
            
            this.panel1.Controls.Add(oWebBrowser);
            oWebBrowser.Dock = DockStyle.Fill;
            NaBrowse(oUri);


        }

        public static int GetMData = 0;

        public void NaBrowse(string oUri)
        {
           
          oWebBrowser.Navigate(oUri);

        }


        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                NaBrowse(this.textBox1.Text);

            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
           
            GC.Collect();
            Process.GetCurrentProcess().Kill();
        }
        private string GetFrameSource()
        {
            string DocumentHtml = string.Empty;
            if (this.textBox1.Text.Contains("chowtaiseng"))
            {
                DocumentHtml = ((Gecko.GeckoHtmlElement)oWebBrowser.Document.DocumentElement).InnerHtml;

                return DocumentHtml;
            }
            else
            {
                string doc = oWebBrowser.Document.Body.InnerHtml;
                return doc;
                //var iframes = oWebBrowser.Document.GetElementsByTagName("iframe");
                //foreach (Gecko.DOM.GeckoIFrameElement v in iframes)
                //{
                //    string sourceCode = string.Empty;
                //    try
                //    {
                //        sourceCode = v.ContentDocument.Body.InnerHtml;
                //    }
                //    catch (Exception e) { }

                //    int oFind = sourceCode.IndexOf("<div class=\"twrap-titletext\">货品明细</div>");
                //    if (oFind > 0)

                //    {

                //        return sourceCode;
                //    }
                //}

            }

            
        }
        [STAThreadAttribute]
        private void refreshData()
        {
            GetMData = 1;
            if (this.textBox1.Text.ToUpper().Contains("POS.ZLF.CN"))
            {
                var iframes = oWebBrowser.Document.GetElementsByTagName("iframe");
                foreach (Gecko.DOM.GeckoIFrameElement v in iframes)
                {
                    string sourceCode = string.Empty;
                    try
                    {
                        sourceCode = v.ContentDocument.Body.InnerHtml;
                    }
                    catch (Exception e) { }

                    int oFind = sourceCode.IndexOf("每页显示记录数");
                    if (oFind > 0)

                       
                    {
                        v.SetAttribute("pageSize", "9999");
                        //Gecko.AutoJSContext oAutoJsContext = new AutoJSContext(oWebBrowser.Window);
                        //oAutoJsContext.EvaluateScript("document.getElementById('pageSize').value=9999;");
                        //oAutoJsContext.EvaluateScript("document.getElementById('pageSize').focus();");
                        
                        Thread.SpinWait(10000);
                        break;
                    }
                }
            }


        }

        public static void LogFileWrite(string filename, string LogTxt)
        {
            StreamWriter oFileWriter = new StreamWriter(filename, true);
            oFileWriter.Write(LogTxt);
            oFileWriter.Flush();
            oFileWriter.Close();
            oFileWriter.Dispose();
        }
        private   void button1_Click(object sender, EventArgs e)
        {
          //refreshData();

           MessageBox.Show(this, "如果数据一页未显示完，请多次获取数据！");
          GetData();
        }
       [STAThread]
        private  void GetData()
        {
            //refreshData();
            GetMData = 0;
            string sourceCode =  GetFrameSource();

            if (sourceCode.Equals(""))
            {
                MessageBox.Show("未找到表格数据!");
                return;
            }
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(sourceCode);
            try
            {
                HtmlNodeCollection collection;
                if (this.textBox1.Text.Contains("chowtaiseng"))
                {
                    collection = doc.DocumentNode.SelectNodes("//table[@class='el-table__header']");
                }
                else
                {
                    collection = doc.DocumentNode.SelectNodes("//table[@class='el-table__header']");
                }

               
                HtmlAgilityPack.HtmlDocument doc1 = new HtmlAgilityPack.HtmlDocument();
                doc1.LoadHtml(collection[0].InnerHtml);
                string Result = "";
                var headers = doc1.DocumentNode.SelectNodes("//tr/th");
                int RowCount = 0;
                foreach (HtmlNode header in headers)
                    Result += (header.InnerText) + "\t"; // create columns from th
                Result += "\n";
                // select rows with td elements 
                //if (this.textBox1.Text.Contains("chowtaiseng"))
                //{
                    collection = doc.DocumentNode.SelectNodes("//table[@class='el-table__body']");
                //}
                //else
                //{
                //   // collection = doc.DocumentNode.SelectNodes("//table[@class='ttable ttable-hover']");
                //}
               
                doc1.LoadHtml(collection[0].InnerHtml);
                foreach (var row in doc1.DocumentNode.SelectNodes("//tr[td]"))
                {
                   
                    if (isRegist)
                    {
                        string[] rowvalue = row.SelectNodes("td").Select(td => td.InnerText.Trim().Replace("\t", "").Replace("\n", "")).ToArray();
                        string Rowstr = string.Join<string>("\t", rowvalue);
                        if (!Rowstr.Contains("合计:"))
                        {
                            Result += Rowstr + "\n";
                        }

                    }
                    else
                    {
                        RowCount++;
                        string[] rowvalue = row.SelectNodes("td").Select(td => td.InnerText.Trim().Replace("\t", "").Replace("\n", "")).ToArray();
                        string Rowstr = string.Join<string>("\t", rowvalue);
                        Result += Rowstr + "\n";
                        if (RowCount >= 2) { break; }
                    }

                }

                Clipboard.SetDataObject(Result.ToString());

                if (!isRegist)
                {
                    MessageBox.Show("未注册程序只能提取2行数据！数据提取完成!");
                }
                else
                {
                    MessageBox.Show("数据提取完成!\n数据多于一页时请多次获取！");
                }


            }
            catch (Exception exception)
            {

                MessageBox.Show("数据提取错误!" + exception.ToString());
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                RegistForm oRegist = new RegistForm();
                oRegist.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("注册失败～\n" + ex.Message);
            }

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (!isRegist)
            {
                MessageBox.Show("程序未注册，只能获取2行数据！");
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                
                this.textBox1.Text = "https://newpos.chowtaiseng.com/";
                //this.textBox1.Focus();
                //SendKeys.Send("{Enter}");
                NaBrowse(this.textBox1.Text);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                //this.textBox1.Enabled = true;
                this.textBox1.Text = "https://yun.zlf.cn/";
                //this.textBox1.Focus();
                // SendKeys.Send("{Enter}");
                // this.textBox1.Enabled = false;
                NaBrowse(this.textBox1.Text);
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
            {

                this.textBox1.Text = "https://cpos.chowtaiseng.com/";
                //this.textBox1.Focus();
                //SendKeys.Send("{Enter}");
                NaBrowse(this.textBox1.Text);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Gecko.AutoJSContext oAutoJsContext = new AutoJSContext(oWebBrowser.Window);
            //oAutoJsContext.EvaluateScript("document.getElementsByClassName(\"btn-next\")[0].click();");
            var next = oWebBrowser.Window.Document.GetElementsByClassName("btn-next")[0];
            var button = (GeckoButtonElement)next;
            button.Click();
        }
    }
}

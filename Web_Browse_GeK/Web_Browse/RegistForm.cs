﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Web_Browse.Regist;

namespace Web_Browse
{
    public partial class RegistForm : Form
    {
        public RegistForm()
        {
            InitializeComponent();
        }

        private void RegistForm_Load(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            button1.Text = "激活";
            string encryptComputer = string.Empty;
            if (!Regist.RegistFileHelper.ExistComputerInfofile())
            {
                string computer = ComputerInfo.GetComputerInfo();
                encryptComputer = new EncryptionHelper().EncryptString(computer);
                RegistFileHelper.WriteComputerInfoFile(encryptComputer);
            }

            encryptComputer = RegistFileHelper.ReadComputerInfoFile();
            textBox1.Text = encryptComputer;
            textBox2.Text = "";

            if (Regist.RegistFileHelper.ExistRegistInfofile())
            {
                textBox2.Text=Regist.RegistFileHelper.ReadRegistFile();
                Regist.MyLicense oMyLicense = new MyLicense(null, null);
                if (oMyLicense.CheckRegistData(textBox1.Text, textBox2.Text) == false)
                {
                    label3.Text = "本软件未注删，请联系Henry.Huang@Lssap.com!";
                    textBox2.Text = "";
                    textBox2.Enabled = true;
                }
                else
                {
                    label3.Text = "软件已注册！";
                    textBox2.Enabled = false;
                    button1.Text = "重新激活";
                }
            }
            else
            {
                label3.Text = "本软件未注删，请联系Henry.Huang@Lssap.com!";
                textBox2.Text = "";
                textBox2.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text.Equals("重新激活"))
            {
                textBox2.Enabled = true;
                textBox2.Text = "";
                string computer = ComputerInfo.GetComputerInfo();
                textBox1.Text = new EncryptionHelper().EncryptString(computer);
                
                button1.Text = "激活";
            }
            else
            {
                Regist.MyLicense oMyLicense = new MyLicense(null,null);
                
                if (oMyLicense.CheckRegistData(textBox1.Text,textBox2.Text)==false)
                {
                    MessageBox.Show("激活码不正确，请联系：Henry.Huang@Lssap.com");
                }
                else
                {
                    label3.Text = "软件已注册！";
                    Regist.RegistFileHelper.WriteRegistFile(textBox2.Text);
                    Regist.RegistFileHelper.WriteComputerInfoFile(textBox1.Text);
                    MessageBox.Show("软件激活成功！请关闭软件重新启动！");
                    this.Owner.Text="HaHa";
                    this.Close();
                    this.Owner.Close();
                    
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(textBox1.Text);
        }
    }
}

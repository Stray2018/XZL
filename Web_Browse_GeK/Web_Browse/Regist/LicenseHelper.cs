﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web_Browse.Regist
{
    [LicenseProvider(typeof(MyLicenseProvider))]
    public partial class LicenseHelper
    {
        private License mLicense = null;

        public LicenseHelper()
        {
            this.mLicense = LicenseManager.Validate(typeof(LicenseHelper), this);
        }
        ~LicenseHelper()    //析构函数，在C#中，不常用。
        {
            if (this.mLicense != null)
            {
                this.mLicense.Dispose();
                this.mLicense = null;
            }
        }
    }
}

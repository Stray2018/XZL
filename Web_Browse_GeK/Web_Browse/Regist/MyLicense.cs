﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web_Browse.Regist
{
    class MyLicense : License
    {
        private String mLicenseKey = null;
        private MyLicenseProvider mProvider = null;
        public MyLicense(MyLicenseProvider provider, String key)
        {
            this.mProvider = provider;
            this.mLicenseKey = key;
        }
        public override string LicenseKey
        {
            get { return this.mLicenseKey; }
        }
        public override void Dispose()
        {
            this.mProvider = null;
            this.mLicenseKey = null;
        }
    public bool CheckRegistData(string key,string info)
        {
          var helper = new EncryptionHelper(EncryptionKeyEnum.KeyB);
                string registData = helper.DecryptString(info);
                if (registData.Substring(0,registData.Length-6) == key)
                {
                    return true;
                }
                else
                {
                    return false;
                }
           }
    }
}

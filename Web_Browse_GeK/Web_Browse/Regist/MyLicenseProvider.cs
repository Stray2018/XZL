﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.ComponentModel.Design;
using System.IO;
using System.Security.Cryptography;

namespace Web_Browse.Regist
{
    //[ReflectionPermission(SecurityAction.Deny, MemberAccess = false, ReflectionEmit = false)]
    internal class MyLicenseProvider : LicenseProvider
    {
        //构造函数
        public MyLicenseProvider()
        { }
        /// <summary>
        /// 获取本机MAC地址 其实这个不管是获取本机硬件参数的任何信息都可以这要能标志该机器即可
        /// </summary>
        private String GetMacAddress()
        {
            String macAddr = null;
            ManagementClass inetAdapter = new ManagementClass("WIN32_NetworkAdapterConfiguration");
            ManagementObjectCollection objList = inetAdapter.GetInstances();
            foreach (ManagementObject mobj in objList)
            {
                if ((bool)mobj["IPEnabled"])
                {
                    macAddr = mobj["MacAddress"].ToString().Replace(":", "-");
                    break;
                }
            }
            return macAddr;
        }


        /// <summary>
        /// 获取Assembly所在目录   获取应用程序所在的目录
        /// </summary>
        private String GetAssemblyPath(LicenseContext context)
        {
            String fileName = null;
            Type type = this.GetType();
            ITypeResolutionService service = (ITypeResolutionService)context.GetService(typeof(ITypeResolutionService));
            if (service != null)
            {
                fileName = service.GetPathOfAssembly(type.Assembly.GetName());
            }
            if (fileName == null)
            {
                fileName = type.Module.FullyQualifiedName;
            }
            return Path.GetDirectoryName(fileName);
        }


        private String Encrypt(String source)     //加密算法，可以用，也可不用，这里为了更安全，就用。
        {
            /**
             * 加密算法
             */
            byte[] keyData = Encoding.ASCII.GetBytes("");
            byte[] ivData = Encoding.ASCII.GetBytes("4iJ9Qw#L");
            MemoryStream stream = new MemoryStream();
            DES desProvider = new DESCryptoServiceProvider();
            CryptoStream cs = new CryptoStream(stream,
            desProvider.CreateEncryptor(keyData, ivData),
            CryptoStreamMode.Write);
            byte[] buffer = Encoding.ASCII.GetBytes(source);
            cs.Write(buffer, 0, buffer.Length);
            cs.FlushFinalBlock();
            cs.Close();
            buffer = stream.GetBuffer();
            stream.Close();
            return Convert.ToBase64String(buffer);
        }


        public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
        {
            MyLicense license = null;

            // 计算MAC地址加密串
            String macAddr = this.GetMacAddress();
            String encrypt = this.Encrypt(macAddr);
            if (context != null)
            {
                if (context.UsageMode == LicenseUsageMode.Runtime)
                {
                    String savedLicenseKey = context.GetSavedLicenseKey(type, null);
                    if (encrypt.Equals(savedLicenseKey))
                    {
                        return new MyLicense(this, encrypt);
                    }
                }
                if (license != null)
                {
                    return license;
                }

                // 打开License文件 'license.dat'
                String path = this.GetAssemblyPath(context);
                String licFile = Path.Combine(path, "license.dat");
                if (File.Exists(licFile))
                {
                    Stream fs = new FileStream(licFile, FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    String readedLicenseKey = sr.ReadToEnd();
                    sr.Close();
                    fs.Close();
                    if (encrypt.Equals(readedLicenseKey))
                    {
                        license = new MyLicense(this, encrypt);
                    }
                }
                if (license != null)
                {
                    context.SetSavedLicenseKey(type, encrypt);
                }
            }
            if (license == null)
            {
                System.Windows.Forms.MessageBox.Show("!!!尚未注册!!!");
                return new MyLicense(this, "evaluate");
            }
            return license;
        }
    }
}

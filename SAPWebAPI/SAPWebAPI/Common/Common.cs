﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using SAPbobsCOM;

namespace SAPWebAPI.Common
{
    public class Common
    {
        public static bool GetCopConnInfo( string sapCompyDB,string userCode,string Password, out JObject errMsg)
        {
            errMsg= GetJson(0, "No Error", "", "");
            if (WebApiApplication.oCompany.Connected && WebApiApplication.oCompany.CompanyDB.Equals(sapCompyDB) &WebApiApplication.oCompany.UserName.Equals(userCode)) return true;
            try
            {
                WebApiApplication.oCompany.Disconnect();
            }
            catch (Exception m)
            {

            }
            string sapconn = System.Web.Configuration.WebConfigurationManager.AppSettings["sapConn"];
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            foreach (string newstr in sapconn.Split(';'))
            {
                string[] oNewstr = newstr.Split('=');
                oDictionary.Add(oNewstr[0].Trim(), oNewstr[1]);
            }
            
            WebApiApplication.oCompany.Server = oDictionary["Server"];
            WebApiApplication.oCompany.SLDServer = oDictionary["SLDServer"];
            WebApiApplication.oCompany.LicenseServer = oDictionary["LicenseServer"];
            SAPbobsCOM.BoDataServerTypes oboDataServer = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                Enum.TryParse<SAPbobsCOM.BoDataServerTypes>(oDictionary["DbServerType"], out oboDataServer);
            WebApiApplication.oCompany.DbServerType = oboDataServer;
            WebApiApplication.oCompany.DbUserName= oDictionary["DbUserName"]; 
            WebApiApplication.oCompany.DbPassword= oDictionary["DbPassword"];
            if (!userCode.Equals(""))
            {
                WebApiApplication.oCompany.UserName = userCode;
                WebApiApplication.oCompany.Password = Password;
            }

            if (sapCompyDB.Equals(""))
            {
                errMsg= GetJson(0, "", "","");
                return true;
            }
            WebApiApplication.oCompany.CompanyDB = sapCompyDB.Equals("")? oDictionary["CompanyDB"]:sapCompyDB;
            WebApiApplication.oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Chinese;
            try
            {
                int errCode;
                string errMsg1;
                WebApiApplication.oCompany.Connect();
                WebApiApplication.oCompany.GetLastError(out errCode, out errMsg1);
                if (!errCode.Equals(0))
                {
                    errMsg = GetJson(errCode, errMsg1, "","");
                    return false;
                }
                return true;
            }
            catch(Exception e)
            {
                errMsg = GetJson(e.HResult, e.Message, "", "");
                return false;
            }
            
        }
        public static JObject GetJson(int errCode, string errMsg, string sapKey,String sData)
        {
            JObject ojson = new JObject();
            ojson.Add("errCode", errCode);
            ojson.Add("errMsg", errMsg);
            ojson.Add("sapKey", sapKey);
            ojson.Add("Data", sData);
            return ojson;
        }
        public static void writeFile(string fileContent)
        {
            StreamWriter streamWriter= new StreamWriter(AppDomain.CurrentDomain.BaseDirectory+ "\\ErrDoc\\Log"+DateTime.Now.ToString("yyyy-MM-dd")+".txt",true);
            streamWriter.Write(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ffff")+ fileContent+"\r\n");
            streamWriter.Flush();
            streamWriter.Close();
        }
        public static string inComeJournalDocentry(string DocEntry)
        {
#if DEBUG
            Common.writeFile("开始查询收款凭证号:");
        #endif
            SAPbobsCOM.Recordset oRec  = WebApiApplication.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            string sqlCmd = "select TransId from ORCT where DocEntry = " + DocEntry;
            oRec.DoQuery(sqlCmd);
            if (oRec.RecordCount>0) { 
            
                string Transid = oRec.Fields.Item(0).Value.ToString();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);
#if DEBUG
                Common.writeFile("结束查询收款凭证号:"+sqlCmd);
#endif
                return Transid;
            }

            return "";
        }
        public static string payoutJournalDocentry(string DocEntry)
        {
            SAPbobsCOM.Recordset oRec = WebApiApplication.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            string sqlCmd = "select TransId from OVPM where DocEntry = " + DocEntry;
            oRec.DoQuery(sqlCmd);
            if (oRec.RecordCount > 0)
            {

                string Transid = oRec.Fields.Item(0).Value.ToString();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);
                return Transid;
            }

            return "";
        }
    }
}
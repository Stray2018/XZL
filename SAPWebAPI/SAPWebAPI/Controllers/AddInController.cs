﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace SAPWebAPI.Controllers
{
    public class AddInController : ApiController
    {

        // POST: api/AddIn
        public JObject Post([FromBody] JObject value)
        {
            try
            {
#if DEBUG
                Common.Common.writeFile("开始生成收款单：" + value.ToString());
#endif
                string CompanyDB = value.SelectToken("CompanyDB").ToString();
                string userCode = value.SelectToken("userCode").ToString();
                string password = value.SelectToken("password").ToString();
                JObject errMsg = new JObject();
                int errCode=0;
                string errMsgSAP="",newKeyAll = "";
#if DEBUG
                Common.Common.writeFile("开始连接公司：" );
#endif
                if (Common.Common.GetCopConnInfo(CompanyDB, userCode, password, out errMsg))
                {
#if DEBUG
                    Common.Common.writeFile("连接公司完成：");
                    Common.Common.writeFile("开始生成无分配单据金额：");
#endif
                    WebApiApplication.oCompany.StartTransaction();
                    double NoDocSum = 0.00;
                    try
                    {
                        NoDocSum = double.Parse(value.SelectToken("Data.NoDocSum").ToString());
                    }
                    catch (Exception)
                    {
                    }
                    if (NoDocSum != 0)
                    {
                        SAPbobsCOM.Payments oPayments = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments) as SAPbobsCOM.Payments;
                        oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                        oPayments.DocDate = DateTime.ParseExact(value.SelectToken("Data.TrsfrDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                        oPayments.DueDate = DateTime.ParseExact(value.SelectToken("Data.DocDueDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                        oPayments.CardCode = value.SelectToken("Data.CardCode").ToString();
                        oPayments.DocType = value.SelectToken("Data.DocType").ToString().Equals("C") ? SAPbobsCOM.BoRcptTypes.rCustomer : SAPbobsCOM.BoRcptTypes.rSupplier;
                        oPayments.Remarks = value.SelectToken("Data.Comments").ToString();
                        oPayments.TransferAccount = value.SelectToken("Data.TrsFrAcct").ToString();
                        oPayments.TransferDate = DateTime.ParseExact(value.SelectToken("Data.TrsfrDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                        string transferref = value.SelectToken("Data.TrsfrRef").ToString();
                        oPayments.TransferReference = transferref.Length >27 ? transferref.Substring(0,27):transferref;
                        oPayments.TransferSum = NoDocSum;
                        string Remarks = "银行业务参考号:" + value.SelectToken("Data.BankAtNumber").ToString() + ";总收款金额:" + double.Parse(value.SelectToken("Data.TrsfrSum").ToString());
                        oPayments.JournalRemarks = Remarks.Length > 50 ? Remarks.Substring(0,50):Remarks;
                        oPayments.Add();
                        int errCode1; string errMsg1;
                        WebApiApplication.oCompany.GetLastError(out errCode1, out errMsg1);
                        if (errCode1.Equals(0))
                        {
                            newKeyAll = newKeyAll + "," + Common.Common.inComeJournalDocentry( WebApiApplication.oCompany.GetNewObjectKey());
                        }
                        errCode = errCode + errCode1; errMsgSAP = errMsgSAP + errMsg1;

                    }
#if DEBUG
                    Common.Common.writeFile("无分配单据金额生成完成");
                    Common.Common.writeFile("开始生成有分配单据金额");
#endif
                    IEnumerable<JToken> stus = value.SelectTokens("Data.LineData[*]");
                    foreach (JToken Item in stus)
                    {
                        
                        SAPbobsCOM.Payments oPayments = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments) as SAPbobsCOM.Payments;
                        oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                        oPayments.DocDate = DateTime.ParseExact(value.SelectToken("Data.TrsfrDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                        oPayments.DueDate = DateTime.ParseExact(value.SelectToken("Data.DocDueDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                        oPayments.CardCode = value.SelectToken("Data.CardCode").ToString();
                        oPayments.DocType = value.SelectToken("Data.DocType").ToString().Equals("C") ? SAPbobsCOM.BoRcptTypes.rCustomer : SAPbobsCOM.BoRcptTypes.rSupplier;
                        oPayments.Remarks = value.SelectToken("Data.Comments").ToString();
                        oPayments.TransferAccount = value.SelectToken("Data.TrsFrAcct").ToString();
                        oPayments.TransferDate = DateTime.ParseExact(value.SelectToken("Data.TrsfrDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                        string transferref = value.SelectToken("Data.TrsfrRef").ToString();
                        oPayments.TransferReference = transferref.Length > 27 ? transferref.Substring(0, 27) : transferref;
                        oPayments.TransferSum = double.Parse(Item.SelectToken("SumApplied").ToString()); //double.Parse(value.SelectToken("Data.TrsfrSum").ToString());
                        string Remarks = "银行业务参考号:" + value.SelectToken("Data.BankAtNumber").ToString() + ";总收款金额:" + double.Parse(value.SelectToken("Data.TrsfrSum").ToString());
                        oPayments.JournalRemarks = Remarks.Length > 50 ? Remarks.Substring(0, 50) : Remarks;

                        SAPbobsCOM.Payments_Invoices oLines = oPayments.Invoices;
                        int ObjType = int.Parse(Item.SelectToken("InvoiceType").ToString());
                        switch (ObjType)
                        {
                            case 13:
                                oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice;
                                oLines.DocEntry = int.Parse(Item.SelectToken("DocEntry").ToString());
                                oLines.SumApplied = double.Parse(Item.SelectToken("SumApplied").ToString());
                                break;
                            case 203:
                                oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_DownPayment;
                                oLines.DocEntry = int.Parse(Item.SelectToken("DocEntry").ToString());
                                oLines.SumApplied = double.Parse(Item.SelectToken("SumApplied").ToString());
                                break;

                            case 15:
                            case 17:
                                #if DEBUG 
                                Common.Common.writeFile("开始生成预收款申请");
                                #endif
                                oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_DownPayment;
                                SAPbobsCOM.Documents dpDoc = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDownPayments) as SAPbobsCOM.Documents;
                                DateTime docdate1 = DateTime.ParseExact(value.SelectToken("Data.TrsfrDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                                dpDoc.DocDate = docdate1;
                                dpDoc.DocDueDate = docdate1;

                                dpDoc.CardCode = value.SelectToken("Data.CardCode").ToString();
                                dpDoc.DownPaymentType = SAPbobsCOM.DownPaymentTypeEnum.dptRequest;
                                //oDoc.SalesPersonCode = int.Parse(value.SelectToken("Data.SalesPersonCode").ToString());
                                dpDoc.Comments = value.SelectToken("Data.Comments").ToString();
                                double dpmPayAmount = double.Parse(Item.SelectToken("SumApplied").ToString());
                                SAPbobsCOM.Recordset oRec = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                                
                                string DocEntry = Item.SelectToken("DocEntry").ToString();
                                string osql = "select DocEntry,LineNum,ObjType,OpenQty Quantity,VatGroup,LineTotal,VatSum,(LineTotal+Vatsum)/Quantity PriceAfVAT from RDR1(nolock) T1 where LineStatus='O' and  DocEntry in (" + DocEntry + ") order by LineTotal,LineNum";
                                if (ObjType.Equals(15))
                                {
                                    osql = "select DocEntry,LineNum,ObjType,OpenQty Quantity,VatGroup,LineTotal,VatSum,(LineTotal+Vatsum)/Quantity PriceAfVAT from DLN1(nolock) T1 where LineStatus='O' and DocEntry in (" + DocEntry + ") order by LineTotal,LineNum";
                                }
                                oRec.DoQuery(osql);
                                int updateprice = 0;
                                if (oRec.RecordCount > 0)
                                {
                                    double Gtotal = 0.00;
                                    for (int i = 0; i < oRec.RecordCount; i++)
                                    {
                                        if (Gtotal >= dpmPayAmount & dpmPayAmount != 0.00) { break; }
                                        SAPbobsCOM.Document_Lines oLine = dpDoc.Lines;
                                        oLine.BaseEntry = int.Parse(oRec.Fields.Item("DocEntry").Value.ToString());
                                        oLine.BaseLine = int.Parse(oRec.Fields.Item("LineNum").Value.ToString());
                                        oLine.BaseType = int.Parse(oRec.Fields.Item("ObjType").Value.ToString());
                                        oLine.Quantity = double.Parse(oRec.Fields.Item("Quantity").Value.ToString());
                                        oLine.VatGroup = oRec.Fields.Item("VatGroup").Value.ToString();
                                        oLine.UnitPrice = double.Parse(oRec.Fields.Item("PriceAfVAT").Value.ToString());
                                        // oLine.TaxTotal = double.Parse(oRec.Fields.Item("VatSum").Value.ToString());

                                        double LineTotal = oLine.Quantity * oLine.UnitPrice;
                                        Gtotal = Gtotal + LineTotal ;
                                        if (dpmPayAmount / (LineTotal ) * 100 < 0.01)
                                        {
                                            oLine.UnitPrice = dpmPayAmount;
                                            oLine.Quantity = 1;
                                           updateprice = 1;

                                        }
                                        
                                        oLine.Add();
                                        oRec.MoveNext();
                                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oLine);
                                    }
                                }
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);
                                if (updateprice==1)
                                {
                                    dpDoc.Comments = dpDoc.Comments + "  预收比例太小，修改行数量和单价!";
                                }
                               
                                dpDoc.DocTotal = dpmPayAmount;
                                string dpDocXML = dpDoc.GetAsXML();
                                dpDoc.Add();
                                WebApiApplication.oCompany.GetLastError(out errCode, out errMsgSAP);
                                if (errCode != 0)
                                {
                                    System.Runtime.InteropServices.Marshal.ReleaseComObject(dpDoc);
                                    return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                                }
                                else
                                {
                                    oLines.DocEntry = int.Parse(WebApiApplication.oCompany.GetNewObjectKey());
                                    oLines.SumApplied = dpmPayAmount;
                                    System.Runtime.InteropServices.Marshal.ReleaseComObject(dpDoc);
                                }
#if DEBUG
                                Common.Common.writeFile("预收款申请生成完成");
#endif
                                break;

                            case 18:
                                oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseInvoice;
                                oLines.DocEntry = int.Parse(Item.SelectToken("DocEntry").ToString());
                                oLines.SumApplied = double.Parse(Item.SelectToken("SumApplied").ToString());
                                break;
                            case 204:
                                oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseDownPayment;
                                oLines.DocEntry = int.Parse(Item.SelectToken("DocEntry").ToString());
                                oLines.SumApplied = double.Parse(Item.SelectToken("SumApplied").ToString());
                                break;
                            default:
                                break;

                        }

                        oLines.Add();

                        oPayments.Add();
                        int errCode1;string errMsg1;
                        WebApiApplication.oCompany.GetLastError(out errCode1, out errMsg1);
                        if (errCode1.Equals(0))
                        {
                            string DocEntryNew = WebApiApplication.oCompany.GetNewObjectKey();
#if DEBUG
                            Common.Common.writeFile("收款单生成完成"+DocEntryNew);
#endif
                            newKeyAll = newKeyAll + "," + Common.Common.inComeJournalDocentry(DocEntryNew);
                        }
                        errCode = errCode + errCode1;errMsgSAP = errMsgSAP + errMsg1;
                    }
                    WebApiApplication.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    int errCode2; string errMsg2;
                    WebApiApplication.oCompany.GetLastError(out errCode2, out errMsg2);
                    errCode = errCode + errCode2; errMsgSAP = errMsgSAP + errMsg2;
                    if (errCode.Equals(0))
                    {
                        WebApiApplication.oCompany.Disconnect();
#if DEBUG
                        Common.Common.writeFile("收款生成完成，返回");
#endif
                        return Common.Common.GetJson(0, "收款单新增成功!", newKeyAll.Substring(1), value.ToString());
                    }
                    else
                    {
                        if (WebApiApplication.oCompany.InTransaction)
                        {
                            WebApiApplication.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            WebApiApplication.oCompany.Disconnect();
                        }
#if DEBUG
                        Common.Common.writeFile("收款生成失败，返回");
#endif
                        return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                    }
                    
                }
                else
                {
                    return errMsg;
                }
            }
            catch (Exception e)
            {
                if (WebApiApplication.oCompany.InTransaction)
                {
                    WebApiApplication.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    WebApiApplication.oCompany.Disconnect();
                }
                return Common.Common.GetJson(e.HResult, e.ToString(), "", value.ToString());
            }
        }
    }
    }

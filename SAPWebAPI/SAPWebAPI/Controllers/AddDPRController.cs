﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace SAPWebAPI.Controllers
{
    public class AddDPRController : ApiController
    {
        // POST: api/AddDPR
        public JObject Post([FromBody] JObject value)
        {
            try
            {
                string CompanyDB = value.SelectToken("CompanyDB").ToString();
                string userCode = value.SelectToken("userCode").ToString();
                string password = value.SelectToken("password").ToString();
                string ObjType = value.SelectToken("ObjType").ToString().Trim();
                JObject errMsg = new JObject();
                int errCode=0;
                string errMsgSAP = string.Empty;
                if (Common.Common.GetCopConnInfo(CompanyDB, userCode, password, out errMsg))
                {
                    SAPbobsCOM.Documents oDoc = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDownPayments) as SAPbobsCOM.Documents;
                    oDoc.DocDate = DateTime.ParseExact(value.SelectToken("Data.DocDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                    oDoc.DocDueDate = DateTime.ParseExact(value.SelectToken("Data.DocDueDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                    oDoc.CardCode = value.SelectToken("Data.CardCode").ToString();
                    oDoc.DownPaymentType = SAPbobsCOM.DownPaymentTypeEnum.dptRequest;
                    //oDoc.SalesPersonCode = int.Parse(value.SelectToken("Data.SalesPersonCode").ToString());
                    oDoc.Comments = value.SelectToken("Data.Comments").ToString();
                    double dpmPayAmount = double.Parse(value.SelectToken("Data.DownPaymentAmount").ToString());
                    double dpmPayPercent = double.Parse(value.SelectToken("Data.DownPaymentPercentage").ToString());
                    SAPbobsCOM.Recordset oRec = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    string PODocEntry = value.SelectToken("Data.PODocEntry").ToString();
                    string osql = "select DocEntry,LineNum,ObjType,Quantity,VatGroup,LineTotal,VatSum from POR1(nolock) T1 where LineStatus='O' and  DocEntry in (" + PODocEntry + ") order by LineTotal,LineNum";
                    if (ObjType.Equals("20"))
                    {
                        osql = "select DocEntry,LineNum,ObjType,Quantity,VatGroup,LineTotal,VatSum from PDN1(nolock) T1 where LineStatus='O' and DocEntry in (" + PODocEntry + ") order by LineTotal,LineNum";
                    }
                    oRec.DoQuery(osql);
                    double Gtotal = 0.00;
                    int updateprice = 0;
                    if (oRec.RecordCount > 0)
                    {
                        
                        for (int i = 0; i < oRec.RecordCount; i++)
                        {
                            if((Gtotal >= dpmPayAmount) & (dpmPayAmount != 0.00)) { break; }
                            SAPbobsCOM.Document_Lines oLine = oDoc.Lines;
                            oLine.BaseEntry = int.Parse(oRec.Fields.Item("DocEntry").Value.ToString());
                            oLine.BaseLine = int.Parse(oRec.Fields.Item("LineNum").Value.ToString());
                            oLine.BaseType = int.Parse(oRec.Fields.Item("ObjType").Value.ToString());
                            oLine.Quantity = double.Parse(oRec.Fields.Item("Quantity").Value.ToString());
                            oLine.VatGroup = oRec.Fields.Item("VatGroup").Value.ToString();

                            double LineTotal= double.Parse(oRec.Fields.Item("LineTotal").Value.ToString());
                            double TaxTotal = double.Parse(oRec.Fields.Item("VatSum").Value.ToString());
                            Gtotal = Gtotal + LineTotal + TaxTotal;
                            if (dpmPayAmount / (LineTotal + TaxTotal) * 100 < 0.01)
                            {
                                oLine.UnitPrice = dpmPayAmount;
                                oLine.Quantity = 1;
                                updateprice = 1;

                            }
                            else
                            {
                                oLine.LineTotal = LineTotal;
                                oLine.TaxTotal = TaxTotal;
                            }
                            
                            
                            oLine.Add();
                            oRec.MoveNext();
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oLine);
                        }
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);
                    oDoc.DocTotal = dpmPayAmount;
                    //if (!dpmPayAmount.Equals(0.00))
                    //{
                    //    oDoc.DocTotal = dpmPayAmount;
                    //}
                    //else
                    //{
                    //    oDoc.DownPaymentPercentage = dpmPayPercent;
                    //}
                    if (updateprice == 1)
                    {
                        oDoc.Comments = oDoc.Comments + ";预付比例太小,修改行金额!";
                    }
                    string newKey = string.Empty;
                    int RuslterrCode = 0;
                    WebApiApplication.oCompany.StartTransaction();
                    errCode= oDoc.Add();
                    if (errCode.Equals(0))
                    {
                         newKey = WebApiApplication.oCompany.GetNewObjectKey().ToString();
                        WebApiApplication.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        WebApiApplication.oCompany.GetLastError(out errCode, out errMsgSAP);
                        if (errCode.Equals(0))
                        {
                            WebApiApplication.oCompany.Disconnect();
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                            return Common.Common.GetJson(0, "预付款申请新增成功!", newKey, value.ToString());
                        }
                        else
                        {
                            if (WebApiApplication.oCompany.InTransaction)
                            {
                                WebApiApplication.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            }
                            oDoc.SaveXML(AppDomain.CurrentDomain.BaseDirectory + "\\ErrDoc\\" + value.SelectToken("Data.PODocEntry").ToString() + ".xml");
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                            return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                        }
                    }
                    else
                    {
                        if (WebApiApplication.oCompany.InTransaction)
                        {
                            WebApiApplication.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        }
                        oDoc.SaveXML(AppDomain.CurrentDomain.BaseDirectory + "\\ErrDoc\\" + value.SelectToken("Data.PODocEntry").ToString() + ".xml");
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                    }

                }
                else
                {
                    return errMsg;
                }
            }
            catch (Exception e)
            {
                Common.Common.writeFile("生成单据失败\r\n"+e.ToString()+"\r\n"+ value.ToString()); 
                return Common.Common.GetJson(e.HResult, e.Message, "", value.ToString());
            }
        }
    }
}

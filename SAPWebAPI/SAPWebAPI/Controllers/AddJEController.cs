﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SAPWebAPI.Controllers
{
    public class AddJEController : ApiController
    {
       
        // POST: api/AddJE
        public JObject Post([FromBody]JObject value)
        {
            try
            {
            string CompanyDB = value.SelectToken("CompanyDB").ToString();
            string userCode = value.SelectToken("userCode").ToString();
            string password = value.SelectToken("password").ToString();
            JObject errMsg =new JObject();
            int errCode;
            string errMsgSAP;
            if(Common.Common.GetCopConnInfo(CompanyDB,userCode,password, out errMsg))
            {
                SAPbobsCOM.JournalEntries ojournal = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries) as SAPbobsCOM.JournalEntries;
                ojournal.ReferenceDate = DateTime.ParseExact(value.SelectToken("Data.RefDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                ojournal.TaxDate =DateTime.ParseExact(value.SelectToken("Data.TaxDate").ToString(),"yyyy-MM-dd",System.Globalization.CultureInfo.CurrentCulture);
                ojournal.DueDate = DateTime.ParseExact(value.SelectToken("Data.DueDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                string memo = value.SelectToken("Data.Memo").ToString();
                    memo = memo.Length >50 ? memo.Substring(0,50) : memo;
                    ojournal.Memo = memo;
                IEnumerable<JToken> stus = value.SelectTokens("Data.LineData[*]").ToArray<JToken>();
                SAPbobsCOM.ChartOfAccounts ofAccounts = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oChartOfAccounts) as SAPbobsCOM.ChartOfAccounts;
                foreach (JToken Item in stus)
                {
                    SAPbobsCOM.JournalEntries_Lines oLines = ojournal.Lines;
                     string Acctount = Item.SelectToken("Account").ToString();
                    oLines.AccountCode = Acctount;
                        oLines.Debit = double.Parse(Item.SelectToken("Debit").ToString());
                        oLines.Credit = double.Parse(Item.SelectToken("Credit").ToString());
                        string LineMemo = Item.SelectToken("LineMemo").ToString();
                        oLines.LineMemo = LineMemo.Length > 50 ? LineMemo.Substring(0,50) : LineMemo; 
                        ofAccounts.GetByKey(Acctount);
                    if(ofAccounts.CashFlowRelevant == SAPbobsCOM.BoYesNoEnum.tYES)
                    {
                            int CFWId = 11;
                            try
                            {
                                CFWId = int.Parse(Item.SelectToken("CFWId").ToString());
                            }
                            catch (Exception)
                            {

                            }
                            if (Acctount.StartsWith("1001"))
                            {
                                if (oLines.Debit != 0) { CFWId = 7; } else { CFWId = 15; }
                            }
                            if (Acctount.StartsWith("1002"))
                            {
                                SAPbobsCOM.Recordset oRc1 = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                                string sql = "select CFWId,CFWName from OCFW(nolock) where CFWId ='"+CFWId.ToString()+"' and Postable='Y'";
                                oRc1.DoQuery(sql);
                                if (oRc1.RecordCount>0)
                                {
                                    string CFWName = oRc1.Fields.Item("CFWName").Value.ToString();
                                    string startA = oLines.Debit != 0 ? "收到的" : "支付的";
                                   string CFWName_new =startA+ CFWName.Substring(3,CFWName.Length - 3);
                                    if (!CFWName.Equals(CFWName_new))
                                    {
                                        sql = "select CFWId,CFWName from OCFW(nolock) where CFWName ='" + CFWName_new + "' and Postable='Y'";
                                        oRc1.DoQuery(sql);
                                        if (oRc1.RecordCount > 0)
                                        {
                                            CFWId = int.Parse(oRc1.Fields.Item("CFWId").Value.ToString());
                                        }
                                    }
                                    
                                }
                               System.Runtime.InteropServices.Marshal.ReleaseComObject(oRc1);
                            }
                        oLines.PrimaryFormItems.CashFlowLineItemID =CFWId;
                    }
                        if (CompanyDB.StartsWith("cdMachinery"))
                        {
                            oLines.CostingCode = Item.SelectToken("ocrCode2").ToString();
                        }
                        else
                        {
                            oLines.CostingCode2 = Item.SelectToken("ocrCode2").ToString();
                        }
                   
                    
                    
                    oLines.Add();
                }
                ojournal.Add();
                WebApiApplication.oCompany.GetLastError(out errCode, out errMsgSAP);
                if (errCode.Equals(0))
                {
                        string newKey = WebApiApplication.oCompany.GetNewObjectKey().ToString();
                        WebApiApplication.oCompany.Disconnect();
                        return Common.Common.GetJson(0, "凭证新增成功!", newKey, value.ToString());
                }
                else
                {
                        WebApiApplication.oCompany.Disconnect();
                    return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                }
                
            } else
            {
                return errMsg;
            }
            }
            catch (Exception e)
            {

                return Common.Common.GetJson(e.HResult, e.Message, "", value.ToString());
            }

        }

    }
}

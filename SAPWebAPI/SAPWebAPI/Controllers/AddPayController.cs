﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace SAPWebAPI.Controllers
{
    public class AddPayController : ApiController
    {
        // POST: api/AddPay
        public JObject Post([FromBody] JObject value)
        {
            try
            {
            string CompanyDB = value.SelectToken("CompanyDB").ToString();
            string userCode = value.SelectToken("userCode").ToString();
            string password = value.SelectToken("password").ToString();
            JObject errMsg = new JObject();
            int errCode;
            string errMsgSAP;
            if (Common.Common.GetCopConnInfo(CompanyDB,userCode,password, out errMsg))
            {
                SAPbobsCOM.Payments oPayments = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments) as SAPbobsCOM.Payments;
                oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_OutgoingPayments;
                oPayments.DocDate = DateTime.ParseExact(value.SelectToken("Data.DocDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                oPayments.DueDate = DateTime.ParseExact(value.SelectToken("Data.DocDueDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                oPayments.CardCode = value.SelectToken("Data.CardCode").ToString();
                oPayments.DocType = value.SelectToken("Data.DocType").ToString().Equals("C") ? SAPbobsCOM.BoRcptTypes.rCustomer : SAPbobsCOM.BoRcptTypes.rSupplier;
                oPayments.Remarks = value.SelectToken("Data.Comments").ToString();
                oPayments.TransferAccount = value.SelectToken("Data.TrsFrAcct").ToString();
                oPayments.TransferDate = DateTime.ParseExact(value.SelectToken("Data.TrsfrDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                oPayments.TransferReference = value.SelectToken("Data.TrsfrRef").ToString();
                oPayments.TransferSum = double.Parse(value.SelectToken("Data.TrsfrSum").ToString());
                IEnumerable<JToken> stus = value.SelectTokens("Data.LineData[*]");
                foreach (JToken Item in stus)
                {
                    SAPbobsCOM.Payments_Invoices oLines = oPayments.Invoices;
                    switch (int.Parse(Item.SelectToken("InvoiceType").ToString()))
                    {
                        case 13:
                            oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice;
                            break;
                        case 203:
                            oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_DownPayment;
                            break;
                        case 18:
                            oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseInvoice;
                            break;
                        case 204:
                            oLines.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseDownPayment;
                            break;
                        default:
                            break;

                    }
                    oLines.DocEntry = int.Parse(Item.SelectToken("DocEntry").ToString());
                    oLines.SumApplied = double.Parse(Item.SelectToken("SumApplied").ToString());
                    oLines.Add();
                }
                oPayments.Add();
                WebApiApplication.oCompany.GetLastError(out errCode, out errMsgSAP);
                if (errCode.Equals(0))
                {
                        string newKey = Common.Common.payoutJournalDocentry(WebApiApplication.oCompany.GetNewObjectKey());
                        WebApiApplication.oCompany.Disconnect();
                    return Common.Common.GetJson(0, "付款单新增成功!", newKey, value.ToString());
                }
                else
                {
                    return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                }

            }
            else
            {
                return errMsg;
            }
            }
            catch (Exception e)
            {

                return Common.Common.GetJson(e.HResult, e.Message, "", value.ToString());
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace SAPWebAPI.Controllers
{
    public class AddArRequestController : ApiController
    {
        // POST: api/AddArRequest
        public JObject Post([FromBody] JObject value)
        {
            try
            {
                string CompanyDB = value.SelectToken("CompanyDB").ToString();
                string userCode = value.SelectToken("userCode").ToString();
                string password = value.SelectToken("password").ToString();
                string ObjType = value.SelectToken("ObjType").ToString().Trim();
                JObject errMsg = new JObject();
                int errCode;
                string errMsgSAP;
                if (Common.Common.GetCopConnInfo(CompanyDB, userCode, password, out errMsg))
                {
                    SAPbobsCOM.Documents oDoc = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDownPayments) as SAPbobsCOM.Documents;
                    oDoc.DocDate = DateTime.ParseExact(value.SelectToken("Data.DocDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                    oDoc.DocDueDate = DateTime.ParseExact(value.SelectToken("Data.DocDueDate").ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                    oDoc.CardCode = value.SelectToken("Data.CardCode").ToString();
                    oDoc.DownPaymentType = SAPbobsCOM.DownPaymentTypeEnum.dptRequest;Default
                    //oDoc.SalesPersonCode = int.Parse(value.SelectToken("Data.SalesPersonCode").ToString());
                    oDoc.Comments = value.SelectToken("Data.Comments").ToString();
                    double dpmPayAmount = double.Parse(value.SelectToken("Data.DownPaymentAmount").ToString());
                    SAPbobsCOM.Recordset oRec = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    string DocEntry = value.SelectToken("Data.DocEntry").ToString();
                    string osql = "select DocEntry,LineNum,ObjType,OpenQty Quantity,VatGroup,LineTotal,VatSum,PriceAfVAT from RDR1(nolock) T1 where LineStatus='O' and  DocEntry in (" + DocEntry + ") order by LineTotal,LineNum";
                    if (ObjType.Equals("15"))
                    {
                        osql = "select DocEntry,LineNum,ObjType,OpenQty Quantity,VatGroup,LineTotal,VatSum,PriceAfVAT from DLN1(nolock) T1 where LineStatus='O' and DocEntry in (" + DocEntry + ") order by LineTotal,LineNum";
                    }
                    oRec.DoQuery(osql);
                    if (oRec.RecordCount > 0)
                    {
                        double Gtotal = 0.00;
                        for (int i = 0; i < oRec.RecordCount; i++)
                        {
                            if(Gtotal >= dpmPayAmount & dpmPayAmount != 0.00) { break; }
                            SAPbobsCOM.Document_Lines oLine = oDoc.Lines;
                            oLine.BaseEntry = int.Parse(oRec.Fields.Item("DocEntry").Value.ToString());
                            oLine.BaseLine = int.Parse(oRec.Fields.Item("LineNum").Value.ToString());
                            oLine.BaseType = int.Parse(oRec.Fields.Item("ObjType").Value.ToString());
                            oLine.Quantity = double.Parse(oRec.Fields.Item("Quantity").Value.ToString());
                            oLine.VatGroup = oRec.Fields.Item("VatGroup").Value.ToString();
                            oLine.PriceAfterVAT = double.Parse(oRec.Fields.Item("PriceAfVAT").Value.ToString());
                           // oLine.TaxTotal = double.Parse(oRec.Fields.Item("VatSum").Value.ToString());
                            Gtotal = Gtotal + oLine.Quantity*oLine.PriceAfterVAT;
                            oLine.Add();
                            oRec.MoveNext();
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oLine);
                        }
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);

                    oDoc.DocTotal = dpmPayAmount;
                    oDoc.Add();

                    WebApiApplication.oCompany.GetLastError(out errCode, out errMsgSAP);
                    if (errCode.Equals(0))
                    {
                        string newKey = WebApiApplication.oCompany.GetNewObjectKey().ToString();
                        WebApiApplication.oCompany.Disconnect();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        return   Common.Common.GetJson(0, "预收款申请新增成功!", newKey, value.ToString());
                    }
                    else
                    {
                        oDoc.SaveXML(AppDomain.CurrentDomain.BaseDirectory+"\\ErrDoc\\"+ value.SelectToken("Data.DocEntry").ToString().Replace(',','_')+".xml");
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                    }
                }
                else
                {
                    return errMsg;
                }
            }
            catch (Exception e)
            {

                return Common.Common.GetJson(e.HResult, e.Message, "", value.ToString());
            }
        }
    }
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SAPWebAPI.Controllers
{
    public class GetCompanyController : ApiController
    {
        JObject errMsg;

        // POST: api/GetCompany
        public JObject Post([FromBody] string value)
        {
            try
            {
                if (Common.Common.GetCopConnInfo("", "", "", out errMsg))
                {
                    SAPbobsCOM.Recordset oRec = WebApiApplication.oCompany.GetCompanyList();
                    //oRec.MoveFirst();
                    JArray sData = new JArray();
                    for (int i = 0; i < oRec.RecordCount; i++)
                    {
                        JObject sonData = new JObject();
                        sonData.Add("CompanyDb", oRec.Fields.Item("dbName").Value.ToString());
                        sonData.Add("CompanyName", oRec.Fields.Item("cmpName").Value.ToString());
                        sData.Add(sonData);
                        oRec.MoveNext();
                    }

                    return Common.Common.GetJson(0, "OK", "", sData.ToString());
                }
                else
                {
                    return errMsg;
                }
            }
            catch (Exception e)
            {
                return Common.Common.GetJson(e.HResult, e.Message, "", "");
            }
        }

    }
}

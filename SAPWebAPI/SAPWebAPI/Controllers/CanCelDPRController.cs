﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Emit;
using System.Web.Http;

namespace SAPWebAPI.Controllers
{
    public class CanCelDPRController : ApiController
    {
        public JObject Post([FromBody] JObject value)
        {
            try
            {
                string CompanyDB = value.SelectToken("CompanyDB").ToString();
                string userCode = value.SelectToken("userCode").ToString();
                string password = value.SelectToken("password").ToString();
                JObject errMsg = new JObject();
                int errCode = 0;
                string errMsgSAP = "";
                if (Common.Common.GetCopConnInfo(CompanyDB, userCode, password, out errMsg))
                {
                    SAPbobsCOM.Documents oDoc = WebApiApplication.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDownPayments) as SAPbobsCOM.Documents;
                    int DocEntry =Convert.ToInt32( value.SelectToken("DocEntry").ToString());
                    if (oDoc.GetByKey(DocEntry))
                    {
                        if (oDoc.DocumentStatus == SAPbobsCOM.BoStatus.bost_Open )
                        {
                            oDoc.Cancel();
                            WebApiApplication.oCompany.GetLastError(out errCode, out errMsgSAP);
                            if (errCode.Equals(0))
                            {
                                string newKey = "0";
                                WebApiApplication.oCompany.Disconnect();
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                                return Common.Common.GetJson(0, "预付款申请取消成功!", newKey, value.ToString());
                            }
                            else
                            {
                                oDoc.SaveXML(AppDomain.CurrentDomain.BaseDirectory + "\\ErrDoc\\DPO" + value.SelectToken("DocEntry").ToString() + ".xml");
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                                return Common.Common.GetJson(errCode, errMsgSAP, "", value.ToString());
                            }
                        } else
                        {
                            return Common.Common.GetJson(9999, "预付款申请不是打开状态或已部分支付，不允许取消!", "", value.ToString());
                        }
                        
                    }
                    return Common.Common.GetJson(100000, "未找到预付款申请单!", "", value.ToString());
                }
                return Common.Common.GetJson(100000, errMsgSAP, "", value.ToString());
            }
            catch (Exception e) {
                return Common.Common.GetJson(e.HResult, e.Message, "", value.ToString());
            }
            
        }
    }
}
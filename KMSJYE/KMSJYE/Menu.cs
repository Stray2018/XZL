﻿using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM.Framework;
using BoFormMode = SAPbouiCOM.BoFormMode;

namespace KMSJYE
{
    class Menu
    {
        public void AddMenuItems()
        {
            SAPbouiCOM.Menus oMenus = null;
            SAPbouiCOM.MenuItem oMenuItem = null;

            oMenus = Application.SBO_Application.Menus;

            SAPbouiCOM.MenuCreationParams oCreationPackage = null;
            oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuItem = Application.SBO_Application.Menus.Item("43744"); // moudles'

            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
            oCreationPackage.UniqueID = "InitUDO";
            oCreationPackage.String = "InitUDO";
            oCreationPackage.Enabled = true;
            oCreationPackage.Position = -1;

            oMenus = oMenuItem.SubMenus;

            try
            {
                //  If the manu already exists this code will fail
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception e)
            {

            }

            try
            {
                // Get the menu collection of the newly added pop-up item
                oMenuItem = Application.SBO_Application.Menus.Item("InitUDO");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "ReadUDOForm";
                oCreationPackage.String = "ReadUDOForm";
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception er)
            { //  Menu already exists
               // Application.SBO_Application.SetStatusBarMessage("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }


            try
            {
                // Get the menu collection of the newly added pop-up item
                oMenuItem = Application.SBO_Application.Menus.Item("2304");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "OPORCal";
                oCreationPackage.String = "开发_采购比价计算";
                oCreationPackage.Position = 0;
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception er)
            { //  Menu already exists
                // Application.SBO_Application.SetStatusBarMessage("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }
            try
            {
                // Get the menu collection of the newly added pop-up item
                oMenuItem = Application.SBO_Application.Menus.Item("2304");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "OPRQSCCreate";
                oCreationPackage.String = "开发_控销订单生成";
                oCreationPackage.Position = 0;
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception er)
            { //  Menu already exists
                // Application.SBO_Application.SetStatusBarMessage("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }

        }

        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction && pVal.MenuUID == "ReadUDOForm")
                {
                    ReadUDOForm activeForm = new ReadUDOForm();
                    activeForm.Show();
                }
                
                if (pVal.BeforeAction && pVal.MenuUID == "Stray_DelRow")
                {
                   
                    CommonApp.Del_Row();
                    
                }

                if (pVal.BeforeAction && pVal.MenuUID=="Ins_Data")
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Freeze(true);
                    CommonApp.Ins_Data();
                    SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Freeze(false);
                }
               
                if (pVal.BeforeAction && pVal.MenuUID == "OPORCal")
                {
                    OPORCal oporCal = new OPORCal();
                    oporCal.Show();

                }
                if (pVal.BeforeAction && pVal.MenuUID == "OPRQSCCreate")
                {
                    CreateOrderIsScale oporCal = new CreateOrderIsScale();
                    oporCal.Show();
                }

                if (pVal.BeforeAction && pVal.MenuUID =="1282")
                {
                    SAPbouiCOM.Form oForm = SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm;
                    if (oForm.TypeEx == "UDO_FT_OPRQS")
                    {
                       oForm.Close();
                       SAPbouiCOM.Framework.Application.SBO_Application.Menus.Item("OPRQS").Activate();
                    }
                    if (oForm.TypeEx == "UDO_FT_TOPDN")
                    {
                        oForm.Close();
                        SAPbouiCOM.Framework.Application.SBO_Application.Menus.Item("TOPDN").Activate();
                    }
                }
               
            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox(ex.ToString(), 1, "Ok", "", "");
            }
        }

    }
}

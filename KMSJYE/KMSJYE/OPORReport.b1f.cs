﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("1470000953", "OPORReport.b1f")]
    class OPORReport : SystemFormBase
    {
        public OPORReport()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.ButtonCombo0 = ((SAPbouiCOM.ButtonCombo)(this.GetItem("Item_0").Specific));
            this.ButtonCombo0.ComboSelectAfter += new SAPbouiCOM._IButtonComboEvents_ComboSelectAfterEventHandler(this.ButtonCombo0_ComboSelectAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("1470000006").Specific));
            this.Matrix0.ChooseFromListAfter += new SAPbouiCOM._IMatrixEvents_ChooseFromListAfterEventHandler(this.Matrix0_ChooseFromListAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.ButtonCombo ButtonCombo0;

        private void OnCustomInitialize()
        {
            ButtonCombo0.ExpandType = BoExpandType.et_DescriptionOnly;
            ButtonCombo0.ValidValues.Add("1110", "首选供应商及价格");
            ButtonCombo0.ValidValues.Add("1111", "上次供应商及价格");
        }

        private void ButtonCombo0_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                string selectValue = ButtonCombo0.Selected.Value;
                ButtonCombo0.Caption = "获取价格";
                ButtonCombo0.Item.Refresh();
                for (int i = 1; i <= Matrix0.RowCount; i++)
                {
                    
                    string ItemCode = ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000003", i)).Value;
                    string sqlCmd = "";
                   
                    try
                    {
                        ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000007", i)).Value = "";
                    }
                    catch (Exception e)
                    {
                       
                    }

                    try
                    {
                        ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000015", i)).Value = "";
                    }
                    catch (Exception e)
                    {
                       
                    }  

                        
                   

                    if (selectValue.Equals("1110"))  //首选供应商及价格
                    {
                        sqlCmd = "SELECT T1.CardCode as CardCode, Price FROM OSPP T1 inner Join OITM T2 on t1.ItemCode=T2.ItemCode and t1.CardCode=T2.CardCode and T1.ItemCode = '"+ItemCode+"'";
                    }
                    else  //上次供应商及价格
                    {
                        sqlCmd =
                            "SELECT TOP 1 T1.CardCode, T2.PriceAfVAT as Price FROM OPDN T1 INNER JOIN PDN1 T2 ON T2.DocEntry = T1.DocEntry AND T2.ItemCode = '" + ItemCode + "' ORDER BY T1.DocEntry DESC";
                    }
                    SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    oRec.DoQuery(sqlCmd);
                    if (oRec.RecordCount > 0)
                    {
                        try
                        {
                            ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000007", i)).Value =
                                oRec.Fields.Item("CardCode").Value.ToString();
                        }
                        catch (Exception e)
                        {
                           
                        }

                        try
                        {
                            ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000015", i)).Value =
                                oRec.Fields.Item("Price").Value.ToString();
                        }
                        catch (Exception e)
                        {
                           
                        }

                        
                    }

                   
                }

               
                
            }
            catch (Exception e)
            {
                ButtonCombo0.Caption = "获取价格";
                ButtonCombo0.Item.Refresh();
            }
            
            
        }

        private SAPbouiCOM.Matrix Matrix0;

        private void Matrix0_ChooseFromListAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (pVal.ColUID.Equals("1470000007"))
            {
                string CardCode = ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000007", pVal.Row)).Value;
                string ItemCode = ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000003", pVal.Row)).Value;
                string sqlCmd = "SELECT T1.CardCode as CardCode, Price FROM OSPP T1 where T1.ItemCode = '" + ItemCode + "' and T1.CardCode ='"+CardCode+"'";
                SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                oRec.DoQuery(sqlCmd);
                if (oRec.RecordCount > 0)
                {
                   
                    try
                    {
                        ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000015", pVal.Row)).Value =
                            oRec.Fields.Item("Price").Value.ToString();
                    }
                    catch (Exception e)
                    {

                    }


                }else
                    try
                    {
                        ((SAPbouiCOM.EditText)Matrix0.GetCellSpecific("1470000015", pVal.Row)).Value ="0";
                    }
                    catch (Exception e)
                    {

                    }

            }

        }
    }
}

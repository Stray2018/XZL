﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.DelCopyForm", "DelCopyForm.b1f")]
    class DelCopyForm : UserFormBase
    {
        public static string sqlCmd = String.Empty;
        public DelCopyForm()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_2").Specific));
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ActivateAfter += new ActivateAfterHandler(this.Form_ActivateAfter);

        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {
            try
            {
                this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 - this.UIAPIRawForm.Width / 2;
                Grid0.DataTable.ExecuteQuery(sqlCmd);
                Grid0.AutoResizeColumns();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
           
        }

        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.Grid Grid0;

        private void Grid0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(pVal.Row))
            {
                Grid0.Rows.SelectedRows.Remove(pVal.Row);
            }
            else
            {
                Grid0.Rows.SelectedRows.Add(pVal.Row);
            }

        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            try
            {
                if (Grid0.Rows.SelectedRows.Count < 1)
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("未选择采购申请单,请选择!");
                    return;
                }
                string DoEntry = String.Empty;
                for (int i = 0; i < Grid0.Rows.SelectedRows.Count; i++)
                {
                    int ChRowID = Grid0.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder);
                    DoEntry += "," + Grid0.DataTable.GetValue("内部单号", ChRowID);
                }

                DoEntry = DoEntry.Substring(1);

                DelCopyData.DocEntry = DoEntry;
                DelCopyData oCopyData = new DelCopyData();
                oCopyData.Show();
                this.UIAPIRawForm.Close();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
            
        }

        private void Form_ActivateAfter(SBOItemEventArg pVal)
        {
            Grid0.AutoResizeColumns();

        }

        private void Button1_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
           this.UIAPIRawForm.Close();

        }
    }
}

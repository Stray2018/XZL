﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("540000988", "OPQT.b1f")]
    class OPQT : SystemFormBase
    {
        public OPQT()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("14").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataAddBefore += new DataAddBeforeHandler(this.Form_DataAddBefore);

        }

        private void Form_DataAddBefore(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            string CdSumDoc = EditText0.Value.Trim();
            string sqlCmd = "SELECT * FROM [@OPRQS] WHERE DocEntry ='"+CdSumDoc+"' AND Status='O'";
            SAPbobsCOM.Recordset oRec =
                Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery(sqlCmd);
            if (!(oRec.RecordCount>0))  //未查到数据
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("采购申请汇总单号填写错误,请重新填写到供应商参考编号中");
                BubbleEvent = false;
            }

        }

        private void OnCustomInitialize()
        {

        }

        private SAPbouiCOM.EditText EditText0;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;
using System.Data;
namespace KMSJYE
{
    [FormAttribute("UDO_FT_TOPDN")]
    class TOPDN : UDOFormBase
    {
        public static DBDataSource TPDN1;
        public static DBDataSource DtTOPDN;
        public TOPDN()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("20_U_E").Specific));
            this.EditText0.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.EditText0_ChooseFromListAfter);
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("21_U_E").Specific));
            this.EditText1.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.EditText1_ChooseFromListAfter);
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.EditText2.ClickBefore += new SAPbouiCOM._IEditTextEvents_ClickBeforeEventHandler(this.EditText2_ClickBefore);
            this.EditText2.KeyDownAfter += new SAPbouiCOM._IEditTextEvents_KeyDownAfterEventHandler(this.EditText2_KeyDownAfter);
            this.ButtonCombo0 = ((SAPbouiCOM.ButtonCombo)(this.GetItem("Item_2").Specific));
            this.ButtonCombo0.ComboSelectAfter += new SAPbouiCOM._IButtonComboEvents_ComboSelectAfterEventHandler(this.ButtonCombo0_ComboSelectAfter);
            this.LinkedButton0 = ((SAPbouiCOM.LinkedButton)(this.GetItem("Item_8").Specific));
            this.LinkedButton0.ClickAfter += new SAPbouiCOM._ILinkedButtonEvents_ClickAfterEventHandler(this.LinkedButton0_ClickAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("0_U_G").Specific));
            this.Matrix0.LinkPressedBefore += new SAPbouiCOM._IMatrixEvents_LinkPressedBeforeEventHandler(this.Matrix0_LinkPressedBefore);
            this.Matrix0.LostFocusAfter += new SAPbouiCOM._IMatrixEvents_LostFocusAfterEventHandler(this.Matrix0_LostFocusAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_10").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_4").Specific));
            this.ComboBox0.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox0_ComboSelectAfter);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_7").Specific));
            this.StaticText0.ClickAfter += new SAPbouiCOM._IStaticTextEvents_ClickAfterEventHandler(this.StaticText0_ClickAfter);
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_3").Specific));
            this.ComboBox1.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox1_ComboSelectAfter);
            this.EditText3 = ((SAPbouiCOM.EditText)(this.GetItem("22_U_E").Specific));
            this.EditText3.ClickAfter += new SAPbouiCOM._IEditTextEvents_ClickAfterEventHandler(this.EditText3_ClickAfter);
            this.EditText4 = ((SAPbouiCOM.EditText)(this.GetItem("Item_9").Specific));
            this.EditText4.ClickAfter += new SAPbouiCOM._IEditTextEvents_ClickAfterEventHandler(this.EditText4_ClickAfter);
            this.CheckBox0 = ((SAPbouiCOM.CheckBox)(this.GetItem("Item_11").Specific));
            this.CheckBox0.ClickAfter += new SAPbouiCOM._ICheckBoxEvents_ClickAfterEventHandler(this.CheckBox0_ClickAfter);
            this.EditText5 = ((SAPbouiCOM.EditText)(this.GetItem("0_U_E").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_13").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.LinkedButton1 = ((SAPbouiCOM.LinkedButton)(this.GetItem("Item_14").Specific));
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_12").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataLoadAfter += new SAPbouiCOM.Framework.FormBase.DataLoadAfterHandler(this.Form_DataLoadAfter);
            this.ActivateAfter += new SAPbouiCOM.Framework.FormBase.ActivateAfterHandler(this.Form_ActivateAfter);
            this.DataAddBefore += new SAPbouiCOM.Framework.FormBase.DataAddBeforeHandler(this.Form_DataAddBefore);
            this.DeactivateBefore += new SAPbouiCOM.Framework.FormBase.DeactivateBeforeHandler(this.Form_DeactivateBefore);
            this.DataUpdateBefore += new DataUpdateBeforeHandler(this.Form_DataUpdateBefore);

        }

        private SAPbouiCOM.EditText EditText0;

        private void EditText0_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                SAPbouiCOM.DataTable oDt = ((SAPbouiCOM.ISBOChooseFromListEventArg)pVal).SelectedObjects;
                if (!oDt.Equals(null))
                {
                    SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN");
                    oDataSource.SetValue("U_CardCode", 0, oDt.GetValue("CardCode", 0).ToString());
                    oDataSource.SetValue("U_CardName", 0, oDt.GetValue("CardName", 0).ToString());
                    ButtonCombo0.Item.Enabled = true;
                    EditText2.Item.Enabled = true;
                }
            }
            catch (Exception e)
            {

            }

        }

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.EnableMenu("1288", true);
            this.UIAPIRawForm.EnableMenu("1289", true);
            this.UIAPIRawForm.EnableMenu("1290", true);
            this.UIAPIRawForm.EnableMenu("1291", true);

            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 - this.UIAPIRawForm.Width / 2;
            EditText2.Item.Height = 30;
            EditText2.FontSize = 25;
            EditText2.TextStyle = 1;

            CheckBox0.Item.Width = 80;
            CheckBox0.Item.Height = 30;
            CheckBox0.Item.FontSize = 25;
            CheckBox0.Item.Left = EditText2.Item.Left + EditText2.Item.Width + 3;

            Button0.Item.Visible = false;
            Button1.Item.Visible = false;
            StaticText0.Item.Visible = false;

            EditText4.Item.Visible = false;
            LinkedButton0.Item.Visible = false;

            ButtonCombo0.ExpandType = BoExpandType.et_DescriptionOnly;
            ButtonCombo0.ValidValues.Add("15", "采购订单");
            // ButtonCombo0.ValidValues.Add("14", "预留发票");

            ComboBox1.ExpandType = BoExpandType.et_DescriptionOnly;
            SAPbobsCOM.BusinessPlaces oPlaces = Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPlaces) as BusinessPlaces;
            SAPbobsCOM.Users oUsers = Program.oCompany.GetBusinessObject(BoObjectTypes.oUsers) as Users;
            oUsers.GetByKey(Program.oCompany.UserSignature);
            for (int i = 0; i < oUsers.UserBranchAssignment.Count; i++)
            {
                oUsers.UserBranchAssignment.SetCurrentLine(i);
                oPlaces.GetByKey(oUsers.UserBranchAssignment.BPLID);
                string BPLID0 = oPlaces.BPLID.ToString();
                string BPLName = oPlaces.BPLName;
                ComboBox1.ValidValues.Add(BPLID0, BPLName);
            }

            SAPbouiCOM.ChooseFromList CardCodeList = this.UIAPIRawForm.ChooseFromLists.Item("CardCode");
            SAPbouiCOM.Conditions cardCodeConditions = CardCodeList.GetConditions();
            SAPbouiCOM.Condition cardCodeCondition = cardCodeConditions.Add();
            cardCodeCondition.Alias = "CardType";
            cardCodeCondition.CondVal = "S";
            cardCodeCondition.Operation = BoConditionOperation.co_EQUAL;
            CardCodeList.SetConditions(cardCodeConditions);

            SAPbouiCOM.ChooseFromList CardNameList = this.UIAPIRawForm.ChooseFromLists.Item("CardName");
            SAPbouiCOM.Conditions cardNameConditions = CardNameList.GetConditions();
            SAPbouiCOM.Condition cardNameCondition = cardNameConditions.Add();
            cardNameCondition.Alias = "CardType";
            cardNameCondition.CondVal = "S";
            cardNameCondition.Operation = BoConditionOperation.co_EQUAL;
            CardNameList.SetConditions(cardNameConditions);

            ComboBox0.ExpandType = BoExpandType.et_DescriptionOnly;
            ComboBox0.DataBind.SetBound(true, "@TOPDN", "Series");
            ComboBox0.ValidValues.LoadSeries("TOPDN", SAPbouiCOM.BoSeriesMode.sf_Add);
            ComboBox0.Select(0, BoSearchKey.psk_Index);
            DtTOPDN = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN");
            if (this.UIAPIRawForm.Mode == BoFormMode.fm_ADD_MODE )
            {
                string series = ComboBox0.Selected.Value;
                int DocNum = this.UIAPIRawForm.BusinessObject.GetNextSerialNumber(series);
                DtTOPDN.SetValue("DocNum", 0, DocNum.ToString());
                EditText3.Value = DateTime.Now.ToString("yyyyMMdd");
            }
            
            this.ButtonCombo0.Item.Enabled = false;
            
            TPDN1 = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TPDN1");
            
            //EditText0.Active = true;
            for (int i = 0; i < TPDN1.Size; i++)
            {
                TPDN1.SetValue("VisOrder", i,(i+1).ToString());
            }

            Matrix0.Columns.Item("BaseLine").Visible = false;
            Matrix0.Columns.Item("Baseobj").Visible = false;
            Matrix0.Columns.Item("BetDate").Visible = false;
            Matrix0.Columns.Item("LineTotal").Visible = false;
            Matrix0.Columns.Item("Price").Visible = false;
            ItemChange();
        }

        private SAPbouiCOM.EditText EditText1;

        private void EditText1_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                SAPbouiCOM.DataTable oDt = ((SAPbouiCOM.ISBOChooseFromListEventArg)pVal).SelectedObjects;
                if (!oDt.Equals(null))
                {

                    SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN");
                    oDataSource.SetValue("U_CardCode", 0, oDt.GetValue("CardCode", 0).ToString());
                    oDataSource.SetValue("U_CardName", 0, oDt.GetValue("CardName", 0).ToString());
                    ButtonCombo0.Item.Enabled = true;
                }
            }
            catch (Exception e)
            {

            }

        }

        private SAPbouiCOM.EditText EditText2;

        private void EditText2_KeyDownAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                bool IsChoose = CheckBox0.Checked;
                #region 查找状态
                if (pVal.CharPressed == 13 &&(!IsChoose))
                {
                    int ColumnsIndex = 0;
                    for (int i = 0; i < Matrix0.Columns.Count; i++)
                    {
                        if (Matrix0.Columns.Item(i).UniqueID == "Quantity")
                        {
                            ColumnsIndex = i;
                            break;
                        }
                    }

                    TPDN1.Offset = DataSourceOffset;
                    for (int i = 0; i < TPDN1.Size; i++)
                    {
                        if ((TPDN1.Offset + 1) == TPDN1.Size)
                        {
                            TPDN1.Offset = 0;
                        }

                        string Tarstring = EditText2.Value.Trim();
                        string ItemCode = TPDN1.GetValue("U_ItemCode", TPDN1.Offset).ToString();
                        string Dscription = TPDN1.GetValue("U_Dscription", TPDN1.Offset);
                        string Speic = TPDN1.GetValue("U_Speic", TPDN1.Offset);
                        string Factory = TPDN1.GetValue("U_Factory", TPDN1.Offset);

                        if (ItemCode.Contains(Tarstring) || Dscription.Contains(Tarstring) || Speic.Contains(Tarstring) || Factory.Contains(Tarstring))
                        {
                            EditText2.Value = "";
                            Matrix0.SelectRow(TPDN1.Offset + 1, true, false);
                            Matrix0.SetCellFocus(TPDN1.Offset + 1, ColumnsIndex);
                            
                            this.UIAPIRawForm.Refresh();
                            break;
                        }

                        TPDN1.Offset = TPDN1.Offset + 1;
                    }
#endregion
                }

                #region 新增状态
                if (pVal.CharPressed == 13 && (IsChoose))
                {
                    string TagString = EditText2.Value.Trim();
                    string CardCode = EditText0.Value;
                    string BPLID = ComboBox1.Selected.Value;
                    
                    DtTOPDN = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN");
                    string DocEntry = DtTOPDN.GetValue("DocEntry", 0);
                    string sqlCmd1 =
                        "SELECT T1.DocEntry ,t1.LineNum ,T1.ObjType ,T1.ItemCode ,T1.Dscription ,T1.unitMsr,T1.U_Speic ,T4.U_BetDate,T1.U_Factory,T1.WhsCode ,T1.Quantity-isnull(T2.Quantity,0) Quantity,T1.VatGroup ,T1.VatPrcnt ,T1.Price ,T1.PriceAfVAT ,T3.CardName " +
                        " FROM POR1 T1 INNER JOIN OPOR T3 ON T3.DocEntry = T1.DocEntry AND T3.DocStatus='O' " +
                        " inner join OITM T4 on T1.ItemCode=T4.ItemCode and (T4.ItemCode like '%"+ TagString + "%' or T4.ItemName like '%" + TagString + "%' or T4.U_Speic like '%" + TagString + "%' or T4.U_Factory like '%" + TagString + "%' or T4.U_PcName like '%" + TagString + "%' or T4.U_PcFactory like '%" + TagString + "%' )" +
                        " LEFT JOIN (SELECT U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj,SUM(U0.U_Quantity) Quantity " +
                        " FROM [@TPDN1] U0 where U0.DocEntry <> " +DocEntry +
                        " GROUP BY U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj) T2 ON T1.DocEntry=T2.U_BaseDoc AND T1.LineNum=T2.U_BaseLine AND T1.ObjType=T2.U_Baseobj " +
                        " WHERE T3.CardCode = '"+CardCode+"' and T3.BPLID = '"+BPLID+"'  AND T1.LineStatus='O' AND T1.Quantity-isnull(T2.Quantity,0)>0 order by T1.DocEntry ,t1.LineNum";
                    SAPbouiCOM.DataTable oDt = this.UIAPIRawForm.DataSources.DataTables.Item("LoadData");
                        oDt.ExecuteQuery(sqlCmd1);
                    if (!oDt.IsEmpty  && oDt.Rows.Count==1)
                    {
                        int i = TPDN1.Size-1;
                        TPDN1.InsertRecord(i+1);
                        TPDN1.SetValue("U_BaseDoc", i, oDt.GetValue("DocEntry", 0).ToString());
                        TPDN1.SetValue("U_BaseLine", i, oDt.GetValue("LineNum", 0).ToString());
                        TPDN1.SetValue("U_Baseobj", i, oDt.GetValue("ObjType", 0).ToString());
                        TPDN1.SetValue("U_ItemCode", i, oDt.GetValue("ItemCode", 0).ToString());
                        TPDN1.SetValue("U_Dscription", i, oDt.GetValue("Dscription", 0).ToString());
                        TPDN1.SetValue("U_Speic", i, oDt.GetValue("U_Speic", 0).ToString());
                        TPDN1.SetValue("U_Factory", i, oDt.GetValue("U_Factory", 0).ToString());
                        TPDN1.SetValue("U_unit1", i, oDt.GetValue("unitMsr", 0).ToString());
                        TPDN1.SetValue("U_whsCode", i, oDt.GetValue("WhsCode", 0).ToString());
                        decimal Qty = Convert.ToDecimal(oDt.GetValue("Quantity", 0));
                        TPDN1.SetValue("U_Quantity", i, Qty.ToString());
                        decimal priceValue = Convert.ToDecimal(oDt.GetValue("Price", 0));

                        TPDN1.SetValue("U_Price", i, priceValue.ToString());
                        decimal LineTotal = Qty * priceValue;
                        TPDN1.SetValue("U_LineTotal", i, (Qty * priceValue).ToString());
                        TPDN1.SetValue("U_VatGroup", i, oDt.GetValue("VatGroup", 0).ToString());
                        TPDN1.SetValue("U_VatPrcnt", i, oDt.GetValue("VatPrcnt", 0).ToString());
                        decimal priceAFValue = Convert.ToDecimal(oDt.GetValue("PriceAfVAT", 0));
                        decimal Gtotal = Qty * priceAFValue;
                        TPDN1.SetValue("U_PriceAfVat", i, priceAFValue.ToString());
                        TPDN1.SetValue("U_Gtotal", i, (Qty * priceAFValue).ToString());
                        TPDN1.SetValue("U_BetDate", i, oDt.GetValue("U_BetDate", 0).ToString());
                        TPDN1.SetValue("VisOrder", i, (i + 1).ToString());
                        decimal newLineTotal = Convert.ToDecimal(DtTOPDN.GetValue("U_DocTotal", 0)) + LineTotal;
                        decimal newGTotal = Convert.ToDecimal(DtTOPDN.GetValue("U_GTotal", 0)) + Gtotal;
                        TPDN1.SetValue("VisOrder", TPDN1.Size - 1, (TPDN1.Size).ToString());
                        DtTOPDN.SetValue("U_DocTotal", 0, newLineTotal.ToString());
                        DtTOPDN.SetValue("U_GTotal", 0, newGTotal.ToString());
                        this.UIAPIRawForm.Freeze(true);
                        this.Matrix0.LoadFromDataSource();
                        this.UIAPIRawForm.Refresh();
                        this.UIAPIRawForm.Freeze(false);
                        oDt = null;
                       // AddTableFields.ReleaseCom(oDt);
                        //((SAPbouiCOM.EditText)this.Matrix0.GetCellSpecific("Quantity",TPDN1.Size-1)).Active=true;
                        this.Matrix0.SetCellFocus(TPDN1.Size-1,9);
                    }

                    if (oDt.Rows.Count>1)
                    {
                        CopyFromData.IsChoose = true;
                        CopyFromData.sqlCmd = sqlCmd1;
                        CopyFromData oCopyFromData = new CopyFromData();
                        oCopyFromData.Show();
                    }
                }
                #endregion

            }
            catch (Exception e)
            {
                
            }
           

        }

        private SAPbouiCOM.ButtonCombo ButtonCombo0;

        private void ButtonCombo0_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                string sqlCmd = string.Empty;
                if (ButtonCombo0.Selected.Value == "15")  //采购订单
                {
                    string BPLID1 = ComboBox1.Selected.Value;
                    string CardCode1 = EditText0.Value;
                    // sqlCmd =
                    //     "SELECT T1.DocEntry 内部单号,T1.CardCode 供应商编码,T1.CardName 供应商名称,T2.BPLName 分支,T1.SupplCode 补充代码,T1.NumAtCard 客户参考编号,T1.DocDate 订单日期 FROM OPOR T1 INNER JOIN OBPL T2 ON T1.BPLId=T2.BPLId WHERE T1.CardCode='" +
                    //     CardCode1 + "' AND T1.BPLId='" + BPLID1 + "' " +
                    //     "AND T1.DocEntry IN(SELECT M0.DocEntry FROM POR1 M0 LEFT JOIN ("+
                    //" SELECT U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj,SUM(U0.U_Quantity) Quantity FROM[@TPDN1] U0 GROUP BY U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj "+
                    //  "   ) M1 ON M0.DocEntry = M1.U_BaseDoc AND M0.LineNum = M1.U_BaseLine AND M0.ObjType = M1.U_Baseobj AND M0.Quantity < ISNULL(M1.Quantity, 0) AND M0.LineStatus = 'O')";
                    sqlCmd =
                        "SELECT T1.DocEntry ,T1.CardCode ,T1.CardName ,T2.BPLName ,T1.SupplCode ,T1.NumAtCard ,T1.DocDate  FROM OPOR T1 INNER JOIN OBPL T2 ON T1.BPLId=T2.BPLId  AND T1.DocStatus='O' WHERE T1.CardCode='" +
                        CardCode1 + "' AND T1.BPLId='" + BPLID1 + "' " +
                        "AND T1.DocEntry IN(SELECT M0.DocEntry FROM POR1 M0 LEFT JOIN (" +
                        " SELECT U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj,SUM(U0.U_Quantity) Quantity FROM[@TPDN1] U0 GROUP BY U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj " +
                        "   ) M1 ON M0.DocEntry = M1.U_BaseDoc AND M0.LineNum = M1.U_BaseLine AND M0.ObjType = M1.U_Baseobj AND M0.Quantity < ISNULL(M1.Quantity, 0) AND M0.LineStatus = 'O')";
                }
                ButtonCombo0.Caption = "复制从";
                CopyFrom.sqlCmd = sqlCmd;
                CopyFrom oCopyFrom = new CopyFrom();
                oCopyFrom.Show();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("错误：" + e.Message);
            }
            

        }

        private SAPbouiCOM.LinkedButton LinkedButton0;

        private void LinkedButton0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
           

        }

        private  SAPbouiCOM.Matrix Matrix0;

        private void Matrix0_LostFocusAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                Matrix0.FlushToDataSource();
                int Rowint = pVal.Row - 1;
                if (pVal.ColUID == "Quantity")
                {
                    decimal DocTotal = 0, Gtotal = 0;
                    decimal priceValue = Convert.ToDecimal(TPDN1.GetValue("U_Price", Rowint));
                    decimal priceAValue = Convert.ToDecimal(TPDN1.GetValue("U_PriceAfVat", Rowint));
                    decimal Qty = Convert.ToDecimal(TPDN1.GetValue("U_Quantity", Rowint));
                    TPDN1.SetValue("U_LineTotal", Rowint, (Qty * priceValue).ToString());
                    TPDN1.SetValue("U_GTotal", Rowint, (Qty * priceAValue).ToString());
                    for (int i = 0; i < TPDN1.Size; i++)
                    {
                        DocTotal += Convert.ToDecimal(TPDN1.GetValue("U_LineTotal", i));
                        Gtotal += Convert.ToDecimal(TPDN1.GetValue("U_Gtotal", i));
                    }
                    DtTOPDN.SetValue("U_DocTotal", 0, DocTotal.ToString());
                    DtTOPDN.SetValue("U_Gtotal", 0, Gtotal.ToString());
                    TPDN1.Offset = Rowint;
                    
                }

                string MnfDDte1 = TPDN1.GetValue("U_MnfDate", Rowint), ExpDate1 = TPDN1.GetValue("U_ExpDate", Rowint);
                if (pVal.ColUID == "MnfDate" && ExpDate1.Equals(""))
                {
                    string datetime1 = TPDN1.GetValue("U_MnfDate", Rowint);
                    if (!datetime1.Equals(""))
                    {
                        DateTime MnfDate = DateTime.ParseExact(datetime1, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                        int Days = Convert.ToInt32(TPDN1.GetValue("U_BetDate", Rowint));
                        TPDN1.SetValue("U_ExpDate", Rowint, MnfDate.AddMonths(Days).ToString("yyyyMMdd"));
                    }
                    
                   
                }
                if (pVal.ColUID == "ExpDate" && MnfDDte1.Equals(""))
                {
                    string datetime1 = TPDN1.GetValue("U_ExpDate", Rowint);
                    if (!datetime1.Equals(""))
                    {
                        DateTime ExpDate = DateTime.ParseExact(datetime1, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                        int Days = -1 * Convert.ToInt32(TPDN1.GetValue("U_BetDate", Rowint));
                        TPDN1.SetValue("U_MnfDate", Rowint, ExpDate.AddMonths(Days).ToString("yyyyMMdd"));
                    }
                       
                    
                }
                if (pVal.ColUID == "DistNumber")
                {
                    string DistNumber = TPDN1.GetValue("U_DistNumber", Rowint);
                    string ItemCode = TPDN1.GetValue("U_ItemCode", Rowint);
                    if (!DistNumber.Equals(""))
                    {
                        string sqlCmd =
                            "SELECT CONVERT(VARCHAR(10),T1.ExpDate,112) ExpDate, CONVERT(VARCHAR(10),T1.MnfDate,112) MnfDate FROM OBTN T1 WHERE T1.ItemCode='"+ ItemCode + "' AND T1.DistNumber='"+ DistNumber + "'";
                        SAPbobsCOM.Recordset oRec =
                            Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        oRec.DoQuery(sqlCmd);
                        if (oRec.RecordCount>0)
                        {
                           
                            TPDN1.SetValue("U_MnfDate", Rowint, oRec.Fields.Item("MnfDate").Value.ToString());
                            TPDN1.SetValue("U_ExpDate", Rowint, oRec.Fields.Item("ExpDate").Value.ToString());
                        }
                        else
                        {
                            TPDN1.SetValue("U_MnfDate", Rowint, null);
                            TPDN1.SetValue("U_ExpDate", Rowint, null);
                        }
                       
                    }

                    
                }
                this.UIAPIRawForm.Freeze(true);
                Matrix0.LoadFromDataSourceEx(false);
                this.UIAPIRawForm.Freeze(false);
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("错误：" + e.Message);
            }
            

        }

        private SAPbouiCOM.Button Button0;

        private void Form_DataLoadAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            ItemChange();

        }

        private void ItemChange()
        {
            try
            {
                SAPbouiCOM.DBDataSource oDs = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN") as SAPbouiCOM.DBDataSource;
                string CardCode = oDs.GetValue("U_CardCode", 0).ToString().Trim();
                if (CardCode.Equals("")) { return;}
                string Transid = oDs.GetValue("U_Transid", 0).ToString().Trim();

                Button0.Item.Enabled = false;
                Button0.Item.Visible = false;
                Button1.Item.Enabled = false;
                Button1.Item.Visible = false;
                Matrix0.Item.Enabled = true;
                this.UIAPIRawForm.Items.Item("1").Enabled = true;
                this.ButtonCombo0.Item.Visible = false;
                if (Transid.Equals(""))
                {
                    StaticText0.Item.Visible = false;
                    LinkedButton0.Item.Visible = false;
                    EditText4.Item.Visible = false;
                }
                else
                {
                    StaticText0.Item.Visible = true;
                    LinkedButton0.Item.Visible = true;
                    EditText4.Item.Visible = true;
                }

                string Status = oDs.GetValue("Status", 0).Trim();
                if (Status.Equals("P"))
                {
                    Button0.Item.Visible = true;
                    Button0.Item.Enabled = true;
                    Matrix0.Item.Enabled = false;
                }
                
                if (Status.Equals("O"))
                {
                    Button1.Item.Visible = true;
                    Button1.Item.Enabled = true;
                    Matrix0.Item.Enabled = true;
                    CheckBox0.Item.Enabled = true;
                }
                
                this.EditText0.Item.Enabled = false;
                this.EditText1.Item.Enabled = false;
                this.ComboBox1.Item.Enabled = false;
                this.EditText2.Item.Enabled = true;
                CheckBox0.Item.Enabled = Matrix0.Item.Enabled == true ? true : false;
                EditText2.Item.Enabled = Matrix0.Item.Enabled == true ? true : false;
                TPDN1.InsertRecord(TPDN1.Size);
                TPDN1.SetValue("VisOrder", TPDN1.Size - 1, TPDN1.Size.ToString());
                this.Matrix0.LoadFromDataSourceEx(false);
                if (Status.Equals("T") || Status.Equals("C"))
                {
                    this.UIAPIRawForm.Mode = BoFormMode.fm_VIEW_MODE;
                }
                else
                {
                    this.UIAPIRawForm.Mode = BoFormMode.fm_OK_MODE;
                }

                if (this.UIAPIRawForm.Mode == BoFormMode.fm_ADD_MODE ||
                    this.UIAPIRawForm.Mode == BoFormMode.fm_VIEW_MODE)
                {
                    Button2.Item.Visible = false;
                    Button2.Item.Enabled = true;
                }
                else
                {
                    Button2.Item.Visible = true;
                    Button2.Item.Enabled = true;
                }
            }
            catch (Exception e)
            {
                
            }
        }

        private SAPbouiCOM.ComboBox ComboBox0;

        private void ComboBox0_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (this.UIAPIRawForm.Mode == BoFormMode.fm_ADD_MODE)
            {

                SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN");
                int DocNum1 = this.UIAPIRawForm.BusinessObject.GetNextSerialNumber(ComboBox0.Selected.Value, "TOPDN");
                oDataSource.SetValue("DocNum", 0, DocNum1.ToString());
                this.UIAPIRawForm.Refresh();
            }

        }

        private StaticText StaticText0;

        private void StaticText0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            

        }

        private ComboBox ComboBox1;

        private void ComboBox1_ComboSelectAfter(object sboObject, SBOItemEventArg pVal)
        {
            

        }

        private EditText EditText3;

        private void EditText3_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
           

        }

        private void Form_ActivateAfter(SBOItemEventArg pVal)
        {
            Matrix0.LoadFromDataSourceEx(false);
        

        }

        private EditText EditText4;

        private void EditText4_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
          

        }

        private CheckBox CheckBox0;

        private void CheckBox0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (CheckBox0.Checked)
            {
                ButtonCombo0.Item.Enabled = true;
            }
            else
            {
                ButtonCombo0.Item.Enabled = false;
            }

        }

        private EditText EditText5;

        private void Form_DataAddBefore(ref BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (TPDN1.GetValue("U_BaseDoc",0).Equals(""))
            {
                BubbleEvent = false;
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("第一行数据为空,不允许保存!");
                return;
            }

            for (int i = TPDN1.Size-1; i >= 0; i--)
            {
                if (TPDN1.GetValue("U_BaseDoc",i).Equals(String.Empty))
                {
                    TPDN1.RemoveRecord(i);
                }
            }

            if (TPDN1.Size<1)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("行数据为空，不能保存！");
                BubbleEvent = false;
            }

        }

        private ComboBox ComboBox2;
        private Button Button1;

        private void Button1_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            string sqlCmd = "UPDATE [@TOPDN] Set Status = 'I' where DocEntry = " +
                            DtTOPDN.GetValue("DocEntry", 0).Trim();
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery(sqlCmd);
            SAPbouiCOM.Framework.Application.SBO_Application.ActivateMenuItem("1304");
            AddTableFields.ReleaseCom(oRec);
        }

        private LinkedButton LinkedButton1;

        private void Form_DeactivateBefore(SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
          this.Matrix0.FlushToDataSource();

        }

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            MsgShow oShow = new MsgShow();
            try
            {
                ((StaticText) oShow.UIAPIRawForm.Items.Item("Item_0").Specific).Caption = "正在生成采购收货，请稍等……";
                oShow.Show();
                SAPbobsCOM.Documents oDoc =
                    Program.oCompany.GetBusinessObject(BoObjectTypes.oPurchaseDeliveryNotes) as SAPbobsCOM.Documents;

                oDoc.CardCode = DtTOPDN.GetValue("U_CardCode", 0);
                oDoc.BPL_IDAssignedToInvoice = Convert.ToInt32(DtTOPDN.GetValue("U_BPLID", 0).ToString());
                oDoc.DocDate = DateTime.ParseExact(EditText3.Value, "yyyyMMdd",
                    System.Globalization.CultureInfo.CurrentCulture);
                SAPbobsCOM.Recordset oRecItem =
                    Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                string DocEntry = DtTOPDN.GetValue("DocEntry", 0);
                string sqlCmd =
                    "SELECT T1.U_whsCode, T1.U_VatPrcnt,T1.U_VatGroup,T1.U_ItemCode, t1.U_BaseDoc,T1.U_BaseLine,T1.U_Baseobj,T1.U_PriceAfVat,SUM(T1.U_Quantity) AS Quantity " +
                    "FROM [@TPDN1] T1 WHERE T1.DocEntry=" + DocEntry +
                    " GROUP BY t1.U_BaseDoc,T1.U_BaseLine,T1.U_Baseobj,T1.U_PriceAfVat,T1.U_ItemCode,T1.U_VatPrcnt,T1.U_VatGroup,T1.U_whsCode";
                oRecItem.DoQuery(sqlCmd);

                for (int i = 0; i < oRecItem.RecordCount; i++)
                {
                    SAPbobsCOM.Document_Lines oLines = oDoc.Lines;
                    oLines.BaseEntry = Convert.ToInt32(oRecItem.Fields.Item("U_BaseDoc").Value);
                    oLines.BaseLine = Convert.ToInt32(oRecItem.Fields.Item("U_BaseLine").Value);
                    oLines.BaseType = Convert.ToInt32(oRecItem.Fields.Item("U_Baseobj").Value);
                    oLines.Quantity = Convert.ToDouble(oRecItem.Fields.Item("Quantity").Value);
                    oLines.TaxCode = oRecItem.Fields.Item("U_VatGroup").Value.ToString();
                    oLines.TaxPercentagePerRow = Convert.ToDouble(oRecItem.Fields.Item("U_VatPrcnt").Value);
                    oLines.PriceAfterVAT = Convert.ToDouble(oRecItem.Fields.Item("U_PriceAfVat").Value);
                    //oLines.DiscountPercent = 0;
                    oLines.WarehouseCode = oRecItem.Fields.Item("U_whsCode").Value.ToString();
                    
                    SAPbobsCOM.Items oItm =
                        Program.oCompany.GetBusinessObject(BoObjectTypes.oItems) as SAPbobsCOM.Items;
                    if (!oItm.GetByKey(oRecItem.Fields.Item("U_ItemCode").Value.ToString()))
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("获取商品信息失败!");
                        return;
                    }

                    if (oItm.ManageBatchNumbers == BoYesNoEnum.tYES)
                    {
                        string InvUnit = oItm.InventoryUOM;
                        double purUNitPer = oItm.PurchaseItemsPerUnit;
                        AddTableFields.ReleaseCom(oItm);
                        SAPbobsCOM.Recordset oRecBatchNum =
                            Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        sqlCmd =
                            "SELECT T1.U_Quantity,T1.U_DistNumber,convert(VARCHAR(10),T1.U_ExpDate,112) U_ExpDate,convert(VARCHAR(10),T1.U_MnfDate,112) U_MnfDate,T1.U_unit1  FROM [@TPDN1] T1 WHERE T1.DocEntry=" + DocEntry + " and T1.U_BaseDoc=" +
                            oLines.BaseEntry.ToString() + " AND T1.U_BaseLine=" + oLines.BaseLine.ToString() +
                            " AND T1.U_Baseobj='" + oLines.BaseType.ToString() + "' ORDER BY T1.LineId";
                        oRecBatchNum.DoQuery(sqlCmd);
                        if (oRecBatchNum.RecordCount > 0)
                        {
                            oRecBatchNum.MoveFirst();
                            for (int j = 0; j < oRecBatchNum.RecordCount; j++)
                            {
                                oLines.BatchNumbers.BatchNumber = oRecBatchNum.Fields.Item("U_DistNumber").Value.ToString();

                                oLines.BatchNumbers.ExpiryDate = DateTime.ParseExact(
                                    oRecBatchNum.Fields.Item("U_ExpDate").Value.ToString(), "yyyyMMdd",
                                    System.Globalization.CultureInfo.CurrentCulture);
                                oLines.BatchNumbers.ManufacturingDate = DateTime.ParseExact(
                                    oRecBatchNum.Fields.Item("U_MnfDate").Value.ToString(), "yyyyMMdd",
                                    System.Globalization.CultureInfo.CurrentCulture);
                                string DocUnit = oRecBatchNum.Fields.Item("U_unit1").Value.ToString().Trim();
                                double DocQty = Convert.ToDouble(oRecBatchNum.Fields.Item("U_Quantity").Value);
                                double Quantity = InvUnit.Equals(DocUnit) ? DocQty : DocQty * purUNitPer;
                                oLines.BatchNumbers.Quantity = Quantity;
                                oLines.BatchNumbers.Add();
                                oRecBatchNum.MoveNext();
                            }

                            AddTableFields.ReleaseCom(oRecBatchNum);
                        }
                        else
                        {
                            SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(
                                "批次信息未填写!\r\n 行:" + TPDN1.Offset.ToString() + "  商品编码:" +
                                oRecItem.Fields.Item("U_ItemCode").Value.ToString());
                            return;
                        }
                    }

                    oLines.Add();
                    AddTableFields.ReleaseCom(oLines);
                    oRecItem.MoveNext();

                }

                AddTableFields.ReleaseCom(oRecItem);

                if (oDoc.Add() != 0)
            {
                oShow.UIAPIRawForm.Close();
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(
                    "生成采购收货单失败\r\n" + Program.oCompany.GetLastErrorDescription());
            }
            else
            {
                 sqlCmd = "Update [@TOPDN] set U_Transid = '"+Program.oCompany.GetNewObjectKey()+"',Status = 'T' where DocEntry = " +
                                DtTOPDN.GetValue("DocEntry", 0);
                SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                oRec.DoQuery(sqlCmd);
                
                AddTableFields.ReleaseCom(oRec);
                string CardCode = DtTOPDN.GetValue("U_CardCode", 0);
                SAPbobsCOM.BusinessPartners oBusinessPartners = Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners) as SAPbobsCOM.BusinessPartners;
                oBusinessPartners.GetByKey(CardCode);
                if (oBusinessPartners.UserFields.Fields.Item("U_IsOneInSk").Value.Equals("否"))
                {
                    string ssqlcm = "select U_BaseDoc from [@TPDN1] T1 where T1.DocEntry = " + DocEntry +" group by T1.U_BaseDoc";
                    SAPbobsCOM.Recordset oRecordset = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                    oRecordset.DoQuery(ssqlcm);
                    if (oRecordset.RecordCount>0)
                    {
                            for (int i = 0; i < oRecordset.RecordCount; i++)
                            {
                                string DocEntry1 = oRecordset.Fields.Item(0).Value.ToString();
                                SAPbobsCOM.Documents oDocuments =
                                    Program.oCompany.GetBusinessObject(BoObjectTypes.oPurchaseOrders) as
                                        SAPbobsCOM.Documents;
                                if (oDocuments.GetByKey(Convert.ToInt32(DocEntry1)))
                                {
                                    oDocuments.Close();
                                }
                                oRecordset.MoveNext();
                                
                            }
                        }
                    }
            }
            AddTableFields.ReleaseCom(oDoc);
            oShow.UIAPIRawForm.Close();
            SAPbouiCOM.Framework.Application.SBO_Application.ActivateMenuItem("1304");
            }
            catch (Exception e)
            {
                oShow.UIAPIRawForm.Close();
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
        }

        private void Form_DataUpdateBefore(ref BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            for (int i = TPDN1.Size - 1; i >= 0; i--)
            {
                if (TPDN1.GetValue("U_BaseDoc", i).Equals(String.Empty))
                {
                    TPDN1.RemoveRecord(i);
                }
            }
            if (TPDN1.Size < 1)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("行数据为空，不能保存！");
                BubbleEvent = false;
            }

        }

        public int DataSourceOffset = 0;
        private void EditText2_ClickBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            DataSourceOffset = Matrix0.GetCellFocus().rowIndex;

        }

        private Button Button2;

        private void Button2_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            { 
                TPDN1 = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TPDN1");
            float LineTotal = 0, Gtotal = 0;
            for (int i = 0; i < TPDN1.Size; i++)
            {
                string DocEntry = TPDN1.GetValue("U_BaseDoc", i);
                string LineNum = TPDN1.GetValue("U_BaseLine", i);
                try
                {
                    int k = int.Parse(DocEntry);
                    if (!(k > 0)) { return;}
                }
                catch (Exception e)
                {
                   continue;
                }

                string sqlCmd = "SELECT T1.Price,T1.PriceAfVAT FROM POR1 T1 WHERE T1.DocEntry='"+DocEntry+"' AND T1.LineNum='"+LineNum+"'";
                SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                oRec.DoQuery(sqlCmd);
                if (oRec.RecordCount>0)
                {
                    float Qty = float.Parse(TPDN1.GetValue("U_Quantity", i));
                    float Price = float.Parse(oRec.Fields.Item("Price").Value.ToString());
                    float PriceAfVAT = float.Parse(oRec.Fields.Item("PriceAfVAT").Value.ToString());
                    TPDN1.SetValue("U_LineTotal",i,(Qty*Price).ToString());
                    TPDN1.SetValue("U_Gtotal", i, (Qty * PriceAfVAT).ToString());
                    LineTotal +=float.Parse(TPDN1.GetValue("U_LineTotal",i));
                    Gtotal += float.Parse(TPDN1.GetValue("U_Gtotal", i));
                }

               
            }
            DtTOPDN = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TOPDN");
            DtTOPDN.SetValue("U_DocTotal", 0, LineTotal.ToString());
            DtTOPDN.SetValue("U_Gtotal", 0, Gtotal.ToString());
            this.Matrix0.LoadFromDataSourceEx(false);
            this.UIAPIRawForm.Mode = BoFormMode.fm_UPDATE_MODE;
            }
            catch (Exception e)
            {
               this.UIAPIRawForm.Menu.Item("1304").Activate();
            }

        }

        private void Matrix0_LinkPressedBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            string LinkoBj = this.UIAPIRawForm.DataSources.DBDataSources.Item("@TPDN1").GetValue("U_Baseobj", pVal.Row - 1).ToString();
            SAPbouiCOM.LinkedButton oLink;
            SAPbouiCOM.Column oColumn = this.Matrix0.Columns.Item("BaseDoc");
            oLink = oColumn.ExtendedObject as SAPbouiCOM.LinkedButton;

            oLink.LinkedObjectType = LinkoBj;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.BatchQty", "BatchQty.b1f")]
    class BatchQty : UserFormBase
    {
        public static  string sqlCmd = String.Empty,unitMsr = String.Empty;
        public static  string ItemCode = String.Empty, ItemName = String.Empty,AppNo = String.Empty,Factory = String.Empty,Speic = String.Empty;
        public static int CurrRowIndex = 0;
        

        public BatchQty()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_1").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_3").Specific));
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_4").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_5").Specific));
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_6").Specific));
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_7").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_8").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.StaticText3 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_9").Specific));
            this.StaticText4 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_10").Specific));
            this.EditText3 = ((SAPbouiCOM.EditText)(this.GetItem("Item_11").Specific));
            this.EditText4 = ((SAPbouiCOM.EditText)(this.GetItem("Item_12").Specific));
            this.StaticText5 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_13").Specific));
            this.EditText5 = ((SAPbouiCOM.EditText)(this.GetItem("Item_14").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.StaticText StaticText0;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 - this.UIAPIRawForm.Width / 2;
            this.UIAPIRawForm.DataSources.UserDataSources.Item("ItemCode").Value = ItemCode;
            this.UIAPIRawForm.DataSources.UserDataSources.Item("ItemName").Value = ItemName;
            this.UIAPIRawForm.DataSources.UserDataSources.Item("whsCode").Value = Program.CenterWhs;
            this.UIAPIRawForm.DataSources.DataTables.Item("BatchDe").ExecuteQuery(sqlCmd);
        }

        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.StaticText StaticText2;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.Grid Grid0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
           this.UIAPIRawForm.Close();

        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                int Row = CurrRowIndex-1;
                for (int i = 0; i < Grid0.Rows.SelectedRows.Count; i++)
                {
                    int SelectROWLine = Grid0.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder);
                    string ComQty = Grid0.DataTable.GetValue("可发数量", SelectROWLine).ToString();
                    string DistNumber = Grid0.DataTable.GetValue("批号", SelectROWLine).ToString();
                    string MnfDate = Grid0.DataTable.GetValue("生产日期", SelectROWLine).ToString();
                    string ExpDate = Grid0.DataTable.GetValue("有效期至", SelectROWLine).ToString();
                    string Quantity = Grid0.DataTable.GetValue("发货数量", SelectROWLine).ToString();
                    if ((CurrRowIndex+i) != 1)
                    {
                        DELIVEBPL.LineSource.InsertRecord(CurrRowIndex + i);
                       
                        DELIVEBPL.LineSource.SetValue("LineId", Row, (CurrRowIndex + 1).ToString());
                    }
                    DELIVEBPL.LineSource.SetValue("U_ItemCode", Row + i, ItemCode);
                    DELIVEBPL.LineSource.SetValue("U_ItemName", Row + i , ItemName);
                    DELIVEBPL.LineSource.SetValue("U_Speic", Row + i , Speic);
                    DELIVEBPL.LineSource.SetValue("U_AppNo", Row + i , AppNo);
                    DELIVEBPL.LineSource.SetValue("U_Factory", Row + i , Factory);
                    DELIVEBPL.LineSource.SetValue("U_unitMsr", Row + i , unitMsr);
                    DELIVEBPL.LineSource.SetValue("U_ComQty", Row + i, ComQty);
                    DELIVEBPL.LineSource.SetValue("U_DistNumber", Row + i, DistNumber);
                    DELIVEBPL.LineSource.SetValue("U_MnfDate", Row + i, MnfDate);
                    DELIVEBPL.LineSource.SetValue("U_ExpDate", Row + i , ExpDate);
                    DELIVEBPL.LineSource.SetValue("U_Quantity", Row + i , Quantity);
                }
                this.UIAPIRawForm.Close();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
            
           

        }

        private StaticText StaticText3;

        private void Grid0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            if (!pVal.ColUID.Equals("发货数量"))
            {
                if (Grid0.Rows.IsSelected(pVal.Row))
                {
                    Grid0.Rows.SelectedRows.Remove(pVal.Row);
                }
                else
                {
                    Grid0.Rows.SelectedRows.Add(pVal.Row);
                }
                
            }

        }

        private StaticText StaticText4;
        private EditText EditText3;
        private EditText EditText4;
        private StaticText StaticText5;
        private EditText EditText5;
    }
}

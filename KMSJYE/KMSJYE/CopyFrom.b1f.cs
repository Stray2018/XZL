﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.CopyFrom", "CopyFrom.b1f")]
    class CopyFrom : UserFormBase
    {
        public CopyFrom()
        {
        }

        public static string sqlCmd = string.Empty;
        
        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
           
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Grid Grid0;

        private void OnCustomInitialize()
        {
            SAPbouiCOM.DataTable oDt = this.UIAPIRawForm.DataSources.DataTables.Item("CxData");
            oDt.ExecuteQuery(sqlCmd);
            Grid0.DataTable = oDt;
            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 -
                                     this.UIAPIRawForm.Width / 2;
            Grid0.SelectionMode = BoMatrixSelect.ms_Auto;
            Grid0.RowHeaders.Width = 15;
            Grid0.Item.Enabled = false;
            Grid0.Columns.Item("DocEntry").TitleObject.Caption = "内部订单号";
            Grid0.Columns.Item("CardCode").TitleObject.Caption = "供应商编码";
            Grid0.Columns.Item("CardName").TitleObject.Caption = "供应商名称";
            Grid0.Columns.Item("BPLName").TitleObject.Caption = "分支";
            Grid0.Columns.Item("SupplCode").TitleObject.Caption = "补充代码";
            Grid0.Columns.Item("NumAtCard").TitleObject.Caption = "客户参考编号";
            Grid0.Columns.Item("DocDate").TitleObject.Caption = "订单日期";

        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        private void Button1_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();
            

        }

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                string DocEntry = String.Empty;
                int SelectRowsCount = Grid0.Rows.SelectedRows.Count;
                if (SelectRowsCount < 1)
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("未选择数据,请选择!");
                    return;
                }

                for (int i = 0; i < SelectRowsCount; i++)
                {
                    int Rows1 = Grid0.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder);
                    DocEntry += Grid0.DataTable.GetValue("DocEntry", Rows1).ToString() + ",";
                }

                DocEntry = DocEntry.Substring(0, DocEntry.Length - 1);
                //string sqlCmd1 =
                //    "SELECT T1.DocEntry 内部单号,t1.LineNum 行号,T1.ObjType 对象,T1.ItemCode 商品编码,T1.Dscription 商品描述,T1.unitMsr 单位,T1.WhsCode 仓库,T1.Quantity-isnull(T2.Quantity,0) 数量,T1.VatGroup 税码,T1.VatPrcnt 税率,T1.Price 不含税单价,T1.PriceAfVAT 含税价,T3.CardName 供应商" +
                //    " FROM POR1 T1 INNER JOIN OPOR T3 ON T3.DocEntry = T1.DocEntry AND T3.DocStatus='O' LEFT JOIN (SELECT U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj,SUM(U0.U_Quantity) Quantity " +
                //    " FROM [@TPDN1] U0 GROUP BY U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj) T2 ON T1.DocEntry=T2.U_BaseDoc AND T1.LineNum=T2.U_BaseLine AND T1.ObjType=T2.U_Baseobj " +
                //    " WHERE T1.DocEntry IN("+DocEntry+") AND T1.LineStatus='O' AND T1.Quantity-isnull(T2.Quantity,0)>0";

                string sqlCmd1 =
                    "SELECT T1.DocEntry ,t1.LineNum ,T1.ObjType ,T1.ItemCode ,T1.Dscription ,T1.unitMsr,T1.U_Speic ,T4.U_BetDate,T1.U_Factory,T1.WhsCode ,T1.Quantity-isnull(T2.Quantity,0) Quantity,T1.VatGroup ,T1.VatPrcnt ,T1.Price ,T1.PriceAfVAT ,T3.CardName " +
                    " FROM POR1 T1 INNER JOIN OPOR T3 ON T3.DocEntry = T1.DocEntry AND T3.DocStatus='O' " +
                    " inner join OITM T4 on T1.ItemCode=T4.ItemCode " +
                    " LEFT JOIN (SELECT U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj,SUM(U0.U_Quantity) Quantity " +
                    " FROM [@TPDN1] U0 GROUP BY U0.U_BaseDoc,U0.U_BaseLine,U0.U_Baseobj) T2 ON T1.DocEntry=T2.U_BaseDoc AND T1.LineNum=T2.U_BaseLine AND T1.ObjType=T2.U_Baseobj " +
                    " WHERE T1.DocEntry IN(" + DocEntry + ") AND T1.LineStatus='O' AND T1.Quantity-isnull(T2.Quantity,0)>0 order by T1.DocEntry ,t1.LineNum";

                CopyFromData.sqlCmd = sqlCmd1;
                CopyFromData oFromData = new CopyFromData();

                oFromData.Show();
                this.UIAPIRawForm.Close();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
            
        }

        private void Grid0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(pVal.Row))
            {
                Grid0.Rows.SelectedRows.Remove(pVal.Row);
            }
            else
            {
                Grid0.Rows.SelectedRows.Add(pVal.Row);
            }
          

        }

        private void Button2_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            

        }
    }
}

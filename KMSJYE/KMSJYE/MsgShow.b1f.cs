﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.MsgShow", "MsgShow.b1f")]
    class MsgShow : UserFormBase
    {
        public MsgShow()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.StaticText StaticText0;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.Left =
                (SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width - this.UIAPIRawForm.Width) / 2;
            this.StaticText0.Caption = "正在生成订单，请稍等……";
            this.StaticText0.Item.Height = 50;
            this.StaticText0.Item.FontSize = 33;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.CopyFromData", "CopyFromData.b1f")]
    class CopyFromData : UserFormBase
    {
        public static string sqlCmd = String.Empty;
        public static bool IsChoose = false;
        public CopyFromData()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Grid Grid0;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 -
                                     this.UIAPIRawForm.Width / 2;
            SAPbouiCOM.DataTable oDt = this.UIAPIRawForm.DataSources.DataTables.Item("CxData");
            oDt.ExecuteQuery(sqlCmd);
            Grid0.DataTable = oDt;
            Grid0.SelectionMode = BoMatrixSelect.ms_Auto;
            Grid0.RowHeaders.Width = 15;
            Grid0.Item.Enabled = false;
          

            Grid0.Columns.Item("DocEntry").TitleObject.Caption = "内部单号";
            Grid0.Columns.Item("LineNum").TitleObject.Caption = "行号";
            Grid0.Columns.Item("ObjType").TitleObject.Caption = "对象";
            Grid0.Columns.Item("ItemCode").TitleObject.Caption = "商品编码";
            Grid0.Columns.Item("Dscription").TitleObject.Caption = "商品描述";
            Grid0.Columns.Item("unitMsr").TitleObject.Caption = "单位";
            Grid0.Columns.Item("WhsCode").TitleObject.Caption = "仓库";
            Grid0.Columns.Item("Quantity").TitleObject.Caption = "数量";
            Grid0.Columns.Item("VatGroup").TitleObject.Caption = "税码";
            Grid0.Columns.Item("VatPrcnt").TitleObject.Caption = "税率";
            Grid0.Columns.Item("Price").TitleObject.Caption = "不含税价";
            Grid0.Columns.Item("PriceAfVAT").TitleObject.Caption = "含税价";
            Grid0.Columns.Item("CardName").TitleObject.Caption = "供应商";
            Grid0.Columns.Item("U_Speic").TitleObject.Caption = "规格";
            Grid0.Columns.Item("U_Factory").TitleObject.Caption = "规格"; 
            Grid0.Columns.Item("U_BetDate").TitleObject.Caption = "保质期限";
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();

        }

        private Button Button2;

        private void Button2_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(0))
            {
                Grid0.Rows.SelectedRows.Clear();
            }
            else
            {
                Grid0.Rows.SelectedRows.AddRange(0,Grid0.Rows.Count);
            }

        }

        private void Grid0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(pVal.Row))
            {
                Grid0.Rows.SelectedRows.Remove(pVal.Row);
            }
            else
            {
                Grid0.Rows.SelectedRows.Add(pVal.Row);
            }

        }

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                decimal LineTotal = 0, Gtotal = 0;
                if (!IsChoose)
                {
                    TOPDN.TPDN1.Clear();
                    TOPDN.TPDN1.InsertRecord(0);
                }

                int RowId = TOPDN.TPDN1.Size - 1;
                for (int i = 0; i < Grid0.Rows.SelectedRows.Count; i++)
                {
                    //  T1.DocEntry ,t1.LineNum ,T1.ObjType ,T1.ItemCode ,T1.Dscription ,T1.unitMsr ,T1.WhsCode ,T1.Quantity - isnull(T2.Quantity, 0) Quantity,T1.VatGroup ,T1.VatPrcnt ,
                    // T1.Price ,T1.PriceAfVAT ,T3.CardName
                    int SelectRowId = Grid0.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder);
                    TOPDN.TPDN1.InsertRecord(i + RowId);
                    TOPDN.TPDN1.SetValue("U_BaseDoc", i + RowId, Grid0.DataTable.GetValue("DocEntry", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_BaseLine", i + RowId, Grid0.DataTable.GetValue("LineNum", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_Baseobj", i + RowId, Grid0.DataTable.GetValue("ObjType", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_ItemCode", i + RowId, Grid0.DataTable.GetValue("ItemCode", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_Dscription", i + RowId, Grid0.DataTable.GetValue("Dscription", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_Speic", i + RowId, Grid0.DataTable.GetValue("U_Speic", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_Factory", i + RowId, Grid0.DataTable.GetValue("U_Factory", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_unit1", i + RowId, Grid0.DataTable.GetValue("unitMsr", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_whsCode", i + RowId, Grid0.DataTable.GetValue("WhsCode", SelectRowId).ToString());
                    decimal Qty = Convert.ToDecimal(Grid0.DataTable.GetValue("Quantity", SelectRowId));
                    TOPDN.TPDN1.SetValue("U_Quantity", i + RowId, Qty.ToString());
                    decimal priceValue = Convert.ToDecimal(Grid0.DataTable.GetValue("Price", SelectRowId));

                    TOPDN.TPDN1.SetValue("U_Price", i + RowId, priceValue.ToString());
                    LineTotal += Qty * priceValue;
                    TOPDN.TPDN1.SetValue("U_LineTotal", i + RowId, (Qty * priceValue).ToString());
                    TOPDN.TPDN1.SetValue("U_VatGroup", i + RowId, Grid0.DataTable.GetValue("VatGroup", SelectRowId).ToString());
                    TOPDN.TPDN1.SetValue("U_VatPrcnt", i + RowId, Grid0.DataTable.GetValue("VatPrcnt", SelectRowId).ToString());
                    decimal priceAFValue = Convert.ToDecimal(Grid0.DataTable.GetValue("PriceAfVAT", SelectRowId));
                    Gtotal += Qty * priceAFValue;
                    TOPDN.TPDN1.SetValue("U_PriceAfVat", i + RowId, priceAFValue.ToString());
                    TOPDN.TPDN1.SetValue("U_Gtotal", i + RowId, (Qty * priceAFValue).ToString());
                    //TOPDN.TPDN1.SetValue("U_DistNumber", i, Grid0.DataTable.GetValue("基本单号", i).ToString());
                    //TOPDN.TPDN1.SetValue("U_ExpDate", i, Grid0.DataTable.GetValue("基本单号", i).ToString());
                    //TOPDN.TPDN1.SetValue("U_MnfDate", i, Grid0.DataTable.GetValue("基本单号", i).ToString());
                    TOPDN.TPDN1.SetValue("U_BetDate", i + RowId, Grid0.DataTable.GetValue("U_BetDate", SelectRowId).ToString());

                }

                decimal newLineTotal = Convert.ToDecimal(TOPDN.DtTOPDN.GetValue("U_DocTotal", 0)) + LineTotal;
                decimal newGTotal = Convert.ToDecimal(TOPDN.DtTOPDN.GetValue("U_GTotal", 0)) + Gtotal;
                for (int j = 0; j < TOPDN.TPDN1.Size; j++)
                {
                    TOPDN.TPDN1.SetValue("VisOrder", j, (j + 1).ToString());
                }
                TOPDN.DtTOPDN.SetValue("U_DocTotal", 0, newLineTotal.ToString());
                TOPDN.DtTOPDN.SetValue("U_GTotal", 0, newGTotal.ToString());
                // TOPDN.Matrix0.LoadFromDataSource();
                this.UIAPIRawForm.Close();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
            
        }
    }
}

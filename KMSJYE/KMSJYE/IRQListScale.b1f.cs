﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.IRQListScale", "IRQListScale.b1f")]
    class IRQListScale : UserFormBase
    {
        public IRQListScale()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_2").Specific));
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.Button3 = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.Button3.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button3_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 - this.UIAPIRawForm.Width / 2;
            Grid0.SelectionMode = BoMatrixSelect.ms_Auto;
            Grid0.Item.Enabled = false;
            string sqlCmd =
                "SELECT T1.DocEntry,T1.DocDate,t1.ReqName FROM OPRQ T1 INNER JOIN PRQ1 T2 ON T2.DocEntry = T1.DocEntry AND T1.DocStatus='O' INNER JOIN OITM T3 ON T2.ItemCode=T3.ItemCode AND T3.U_IsCSale='是' GROUP BY T1.DocEntry ,T1.DocDate , T1.ReqName";
            Grid0.DataTable.ExecuteQuery(sqlCmd);
            Grid0.Columns.Item("DocEntry").TitleObject.Caption = "采购申请单号";
            Grid0.Columns.Item("DocDate").TitleObject.Caption = "采购申日期";
            Grid0.Columns.Item("ReqName").TitleObject.Caption = "店铺";
        }

        private SAPbouiCOM.Button Button1;

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();
            

        }

        private SAPbouiCOM.Grid Grid0;
        private Button Button2;

        private void Button2_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            Grid0.Rows.SelectedRows.AddRange(0,Grid0.Rows.Count);
            
        }

        private Button Button3;

        private void Button3_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
           Grid0.Rows.SelectedRows.Clear();

        }

        private void Grid0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(pVal.Row))
            {
                Grid0.Rows.SelectedRows.Remove(pVal.Row);
            }
            else
            {
                Grid0.Rows.SelectedRows.Add(pVal.Row);
            }
           

        }

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            string DocEntry = String.Empty;
            CreateOrderIsScale.OPRQList=String.Empty;
            for (int i = 0; i < Grid0.Rows.SelectedRows.Count; i++)
            {
                string ChooseDocEntry = Grid0.DataTable.GetValue("DocEntry",  Grid0.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder)).ToString();
                DocEntry += "," + ChooseDocEntry;
            }

            CreateOrderIsScale.OPRQList = DocEntry.Substring(1);
            this.UIAPIRawForm.Close();
        }
    }
}

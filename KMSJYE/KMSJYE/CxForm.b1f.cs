﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.CxForm", "CxForm.b1f")]
    class CxForm : UserFormBase
    {
        public static  string sqlCmd = String.Empty;
        public static string TitleName = string.Empty;
        public static Dictionary<string, string> LinkDictionary;
        public CxForm()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_1").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {

        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {
            Grid0.Item.Enabled = false;
            
            this.UIAPIRawForm.Title = TitleName;
            Grid0.DataTable.ExecuteQuery(sqlCmd);
            foreach (var olink in LinkDictionary.AsParallel())
            {
                Grid0.Columns.Item(olink.Key).Type = BoGridColumnType.gct_EditText;
                SAPbouiCOM.EditTextColumn textColumn = Grid0.Columns.Item(olink.Key) as SAPbouiCOM.EditTextColumn;
                textColumn.LinkedObjectType = olink.Value;
            }
            
            Grid0.RowHeaders.Width = 40;
            Grid0.RowHeaders.TitleObject.Caption = "序号";
            for (int i = 0; i < Grid0.Rows.Count; i++)
            {
                Grid0.RowHeaders.SetText(i,(i+1).ToString());
                //if (i%2!=0)
                //{
                //    Grid0.CommonSetting.SetRowBackColor(i,Color.Aqua.ToArgb());
                //} 
            }

            this.UIAPIRawForm.State = BoFormStateEnum.fs_Maximized;
                Grid0.AutoResizeColumns();
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
           this.UIAPIRawForm.Close();

        }

        private SAPbouiCOM.Grid Grid0;
    }
}

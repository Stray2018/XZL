﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using SAPbobsCOM;
using SAPbouiCOM;
using Application = SAPbouiCOM.Framework.Application;
using Company = SAPbobsCOM.Company;
using CompanyClass = SAPbobsCOM.CompanyClass;
using Exception = System.Exception;

namespace KMSJYE
{
    class Program
    {
        public static Company oCompany = new CompanyClass();
        public static string TypeDValue = string.Empty;
        public static string CenterWhs = String.Empty;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    oApp = new Application(args[0]);
                }
                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();
                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);
                oCompany = SAPbouiCOM.Framework.Application.SBO_Application.Company.GetDICompany() as SAPbobsCOM.Company;
                oCompany.XmlExportType = BoXmlExportTypes.xet_ExportImportMode;
                oCompany.XMLAsString = true;
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                Application.SBO_Application.FormDataEvent += new _IApplicationEvents_FormDataEventEventHandler(SBO_Application_FormDataEvent);
                Application.SBO_Application.ItemEvent += new _IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent); 
               Application.SBO_Application.RightClickEvent += new SAPbouiCOM._IApplicationEvents_RightClickEventEventHandler(SBO_Application_RightClickEvent);
               
                    Application.SBO_Application.MetadataAutoRefresh = true;
               String CurrVersion = "6";
               SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
               string osSql = "IF EXISTS(SELECT * FROM sys.tables T1 WHERE T1.name='B1ConfigSet') SELECT * FROM B1ConfigSet";
                oRec.DoQuery(osSql);
                if (oRec.RecordCount>0)
                {
                    int sysVersion = Convert.ToInt32(oRec.Fields.Item(0).Value.ToString());
                    if (int.Parse(CurrVersion)>sysVersion)
                    {
                        try
                        {
                            oCompany.StartTransaction();
                            osSql = "IF EXISTS(SELECT * FROM B1ConfigSet) BEGIN UPDATE B1ConfigSet SET overs='" +
                                    CurrVersion + "' END ELSE BEGIN INSERT B1ConfigSet(oVers)VALUES('" + CurrVersion + "') end";
                            oRec.DoQuery(osSql);
                            // 门店发货计划 查询明细数据 存储过程 Stray_GetDataDetailOPRQ
                            osSql =
                                "IF EXISTS(select * from dbo.sysobjects where id = object_id(N'[dbo].[Stray_GetDataDetailOPRQ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROC [Stray_GetDataDetailOPRQ]";
                            oRec.DoQuery(osSql);
                            osSql = "CREATE PROC [dbo].[Stray_GetDataDetailOPRQ]\r\n(\r\n\t@Docentry VARCHAR(max),\r\n\t@whsCode VARCHAR(100)\r\n)\r\nAS\r\nSELECT ROW_NUMBER()OVER(PARTITION BY t1.ItemCode ORDER BY T2.ExpDate,t2.DistNumber) xh , T1.ItemCode,T1.Quantity NeedQty,T0.ItemName,T0.U_AppNo,t0.U_Speic,T0.U_Factory,T0.InvntryUom unitMsr,T2.DistNumber,T2.MnfDate,T2.ExpDate,T2.StockQty,t2.BPLIdQty,t2.ComQty ,CONVERT(FLOAT,0) DevQty \r\nINTO #StrayTempDataOPRQ FROM \r\n(SELECT K0.ItemCode,SUM(K0.Quantity) Quantity FROM dbo.PRQ1 K0 WHERE K0.DocEntry IN(SELECT tn FROM dbo.fn_split(@Docentry,',')) AND K0.LineStatus='O' GROUP BY K0.ItemCode) T1 \r\nLEFT JOIN dbo.OITM T0 ON T0.ItemCode = T1.ItemCode\r\nLEFT JOIN (\r\nSELECT U0.DistNumber,CONVERT(DATE,U0.MnfDate,121) MnfDate,CONVERT(DATE,U0.ExpDate,121) ExpDate, U1.WhsCode,U1.ItemCode,U1.Quantity StockQty,U2.Quantity BPLIdQty,U1.Quantity-ISNULL(U2.Quantity,0) ComQty  \r\nFROM dbo.OBTN U0 INNER JOIN dbo.OBTQ U1 ON U0.AbsEntry=U1.MdAbsEntry AND U1.WhsCode=@whsCode AND U1.Quantity<>0\r\nLEFT JOIN (SELECT M2.U_DistNumber,SUM(M2.U_Quantity) Quantity FROM [dbo].[@DELIVEBPL] M1 \r\nINNER JOIN [dbo].[@LIVEBPL1] M2 ON M2.DocEntry = M1.DocEntry AND M1.Status<>'C' GROUP BY M2.U_DistNumber) U2 ON U0.DistNumber=U2.U_DistNumber\r\n)T2 ON T2.ItemCode = T1.ItemCode  WHERE T2.DistNumber IS NOT NULL\r\nORDER BY T1.ItemCode,T2.ExpDate,t2.DistNumber;\r\n UPDATE #StrayTempDataOPRQ SET devQty= CASE WHEN ComQty>=NeedQty THEN NeedQty ELSE ComQty end WHERE xh=1\r\n DECLARE @Count INT =(SELECT MAX(xh) FROM #StrayTempDataOPRQ),@CurrLine int = 2;\r\nWHILE @CurrLine <=@Count\r\n\tBEGIN\r\n\tUPDATE T0 SET T0.devQty= CASE WHEN T1.SumDevQty>= T0.NeedQty THEN 0 WHEN T0.NeedQty>=T1.SumComQty THEN T0.ComQty ELSE T0.NeedQty- T1.SumDevQty end FROM #StrayTempDataOPRQ T0 INNER JOIN(\r\n\t\t\tSELECT M1.xh,m1.ItemCode,M1.NeedQty,M1.ComQty,\r\n\t\t\t(SELECT SUM(U0.ComQty) FROM #StrayTempDataOPRQ U0 WHERE U0.xh<=M1.xh AND U0.ItemCode=M1.itemcode) SumComQty,\r\n\t\t\t(SELECT SUM(U0.DevQty) FROM #StrayTempDataOPRQ U0 WHERE U0.xh<M1.xh AND U0.ItemCode=M1.itemcode) SumDevQty\r\n\t\t\tFROM #StrayTempDataOPRQ M1 WHERE M1.xh=@CurrLine\r\n\t\t\t)T1 ON T0.ItemCode=T1.ItemCode AND T0.xh=T1.xh AND T0.xh=@CurrLine; \r\nSET @CurrLine=@CurrLine+1 END \r\n";
                            osSql += "SELECT ItemCode 物料编码,ItemName 物料描述,unitMsr 单位, U_Speic 规格,U_Factory 产地,U_AppNo 批准文号,NeedQty 需求数量,CONVERT(VARCHAR(40),'') 批号,CONVERT(DATE,null) 生产日期,CONVERT(DATE,null)　有效期至,CONVERT(FLOAT,null) 库存数量,CONVERT(FLOAT,null) 已确认发货数量,CONVERT(FLOAT,null) 可发货数量,CONVERT(FLOAT,null) 本次发货数量, CONVERT(INT,xh) xh,  0 AS isDetail FROM #StrayTempDataOPRQ WHERE xh=1 \r\nUNION ALL \r\nSELECT ItemCode 物料编码 ,ItemName 物料描述,unitMsr 单位, U_Speic 规格,U_Factory 产地,U_AppNo 批准文号,NeedQty 需求数量, DistNumber 批号,CONVERT(date,MnfDate,112) 生产日期, CONVERT(date,ExpDate,112) 有效期至,StockQty 库存数量,BPLIdQty 已确认发货数量,ComQty 可发货数量,DevQty 本次发货数量, CONVERT(INT,xh), 1 AS isDetail FROM #StrayTempDataOPRQ\r\n  ORDER BY ItemCode,CONVERT(INT,xh),isDetail;\r\n DROP TABLE #StrayTempDataOPRQ";
                                oRec.DoQuery(osSql);

                            //函数 fn_split
                            osSql =
                                    "if exists (select * from sysobjects where xtype='TF' and name='fn_split') DROP FUNCTION fn_split";
                                oRec.DoQuery(osSql);
                                osSql = "CREATE FUNCTION [dbo].[fn_split](@SourceSql varchar(max),@StrSeprate varchar(10))\r\nreturns @temp table(tn varchar(100))\r\n--实现split功能 的函数\r\nas \r\nbegin\r\ndeclare @i int\r\nset @SourceSql=rtrim(ltrim(@SourceSql))\r\nset @i=charindex(@StrSeprate,@SourceSql)\r\nwhile @i>=1\r\nbegin\r\ninsert @temp values(left(@SourceSql,@i-1))\r\nset @SourceSql=substring(@SourceSql,@i+1,len(@SourceSql)-@i)\r\nset @i=charindex(@StrSeprate,@SourceSql)\r\nend\r\nif @SourceSql<>'\\'\r\ninsert @temp values(@SourceSql)\r\nreturn \r\nend";
                                oRec.DoQuery(osSql);
                                osSql =
                                    "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'CreareWMS2B1Table' AND type='P') DROP PROC CreareWMS2B1Table";
                                oRec.DoQuery(osSql);
                                osSql =
                                    "CREATE PROC CreareWMS2B1Table\r\nAS\r\nCREATE TABLE [dbo].[WMS_BaseToWMS](\r\n\t[KeyValue] [NVARCHAR](255) NULL,\r\n\t[KeyObject] [NVARCHAR](50) NULL,\r\n\t[KeyTrType] [NVARCHAR](1) NULL,\r\n\t[Status] [INT] NULL\r\n) ON [PRIMARY];CREATE TABLE [dbo].[WMS_ImportErr](\r\n\t[BPLName] [NVARCHAR](100) NULL,\r\n\t[DocType] [NVARCHAR](50) NULL,\r\n\t[WMSDocNum] [NVARCHAR](50) NULL,\r\n\t[DocXML] [NVARCHAR](MAX) NULL,\r\n\t[errCode] [INT] NULL,\r\n\t[errMsg] [NVARCHAR](MAX) NULL,\r\n\t[CreateTime] [DATETIME] NULL\r\n) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];CREATE INDEX KeyValueIndex ON dbo.WMS_BaseToWMS(KeyTrType,Status) INCLUDE(KeyValue);CREATE TABLE [dbo].[ImportWMSOK](\r\n\t[SAPDoc] [VARCHAR](50) NULL,\r\n\t[WMSAPI] [VARCHAR](50) NULL,\r\n\t[SAPDocEntry] [BIGINT] NULL\r\n) ON [PRIMARY];\r\nCREATE INDEX DocIndex ON ImportWMSOK(SAPDoc,WMSAPI,SAPDocEntry);";
                                oRec.DoQuery(osSql);
                                osSql = "EXEC CreareWMS2B1Table;";
                                oRec.DoQuery(osSql);
                            //SAP 存储过程更新
                            osSql =
                                "ALTER proc [dbo].[SBO_SP_PostTransactionNotice]\r\n@object_type nvarchar(30), \t\t\t\t-- SBO Object Type\r\n@transaction_type nchar(1),\t\t\t-- [A]dd, [U]pdate, [D]elete, [C]ancel, C[L]ose\r\n@num_of_cols_in_key int,\r\n@list_of_key_cols_tab_del nvarchar(255),\r\n@list_of_cols_val_tab_del nvarchar(255)\r\nAS\r\n";
                            osSql += "begin\r\n-- Return values\r\ndeclare @error  int\t\t\t\t-- Result (0 for no error)\r\ndeclare @error_message nvarchar (200) \t\t-- Error string to be displayed\r\nselect @error = 0\r\nselect @error_message = N'Ok'\r\n";
                            osSql += "--------------------------------------------------------------------------------------------------------------------------------\r\n";
                            osSql += "IF @object_type='2' AND @transaction_type IN('A','U')\r\nBEGIN\r\nUPDATE OCRD SET U_PcCode = dbo.fn_GetPy(CardName) WHERE CardCode=@list_of_cols_val_tab_del\r\nINSERT dbo.WMS_BaseToWMS(KeyValue, KeyObject, KeyTrType, Status)VALUES(  @list_of_cols_val_tab_del,@object_type,@transaction_type,0 )\r\nend \r\n";
                            osSql += "IF @object_type='4' AND @transaction_type IN('A','U')\r\nBEGIN\r\nUPDATE OITM SET U_VatSales=VatGourpSa, U_PcFactory = dbo.fn_GetPy(U_Factory),U_PcName=dbo.fn_GetPy(ItemName) WHERE ItemCode=@list_of_cols_val_tab_del\r\nINSERT dbo.WMS_BaseToWMS(KeyValue, KeyObject, KeyTrType, Status)VALUES(  @list_of_cols_val_tab_del,@object_type,@transaction_type,0 )\r\nEND\r\n";
                            osSql += "IF @object_type='247' AND @transaction_type IN('A','U')\r\nBEGIN\r\n\tINSERT dbo.WMS_BaseToWMS(KeyValue, KeyObject, KeyTrType, Status)VALUES(  @list_of_cols_val_tab_del,@object_type,@transaction_type,0 )\r\nEND\r\n";
                            osSql += "IF @object_type IN('59','60','1470000113','540000006','22','20','21','18','19','15','16','13','14')  AND @transaction_type IN('A','U') --采购报价\r\nBEGIN\r\nDECLARE @Sql VARCHAR( max)\r\nSET @Sql = 'UPDATE T1 SET U_Speic=T2.U_Speic,U_Factory=T2.U_Factory,U_CntcPrsn = T2.U_CntcPrsn FROM '+\r\n\t   CASE WHEN @object_type = '59' THEN 'IGN1' --收货\r\n\t\t\twhen @object_type = '60' THEN 'IGE1'  --发货\r\n\t\t\twhen @object_type = '1470000113' THEN 'PRQ1'  --采购申请\r\n\t\t\twhen @object_type = '540000006' THEN 'PQT1'  --采购报价\r\n\t\t\twhen @object_type = '22' THEN 'POR1'  --采购订单\r\n\t\t\twhen @object_type = '20' THEN 'PDN1'  --采购收货\r\n\t\t\twhen @object_type = '21' THEN 'RPD1'  --采购退货\r\n\t\t\twhen @object_type = '18' THEN 'PCH1'  --应付发票\r\n\t\t\twhen @object_type = '19' THEN 'RPC1'  --应付贷项凭证\r\n\t\t\twhen @object_type = '15' THEN 'DLN1'  --销售交货\r\n\t\t\twhen @object_type = '16' THEN 'RDN1'  --销售退货\r\n\t\t\twhen @object_type = '13' THEN 'INV1'  --应收发票\r\n\t\t\twhen @object_type = '14' THEN 'RIN1'  --应收贷项凭证\r\n\t\t\tend+' T1 INNER JOIN OITM T2 ON T1.ItemCode=T2.ItemCode AND t1.DocEntry='+@list_of_cols_val_tab_del\r\nEXEC(@Sql)\r\nend\r\nselect @error=0, @error_message = @list_of_cols_val_tab_del;\r\n\r\nend";
                            oRec.DoQuery(osSql);

                            AddTableFields.ReleaseCom(oRec);
                            bool istrue = oCompany.InTransaction;
                            CommonApp.initField();
                            istrue = oCompany.InTransaction;
                            CommonApp.InitApp();
                            istrue = oCompany.InTransaction;
                            oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        }
                        catch (Exception e)
                        {
                            SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
                            if (oCompany.InTransaction)
                            {
                                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }
                        }
                       
                    }
                }
                else
                {
                    try
                    {

                        oCompany.StartTransaction();
                        osSql =
                            "IF EXISTS(SELECT * FROM sys.objects WHERE name = 'CreateB1ConfigTable' AND type='P') DROP PROC CreateB1ConfigTable;";
                        oRec.DoQuery(osSql);
                        osSql =
                            "CREATE PROC [dbo].[CreateB1ConfigTable]\r\nWITH ENCRYPTION\r\nAS \r\nCREATE TABLE B1ConfigSet(oVers int);";
                        oRec.DoQuery(osSql);
                        osSql = "EXEC CreateB1ConfigTable;";
                        oRec.DoQuery(osSql);
                        osSql = "IF EXISTS(SELECT * FROM B1ConfigSet) BEGIN UPDATE B1ConfigSet SET overs='" +
                                CurrVersion + "' END ELSE BEGIN INSERT B1ConfigSet(oVers)VALUES('" + CurrVersion +
                                "') end";
                        oRec.DoQuery(osSql);
                        AddTableFields.ReleaseCom(oRec);
                        CommonApp.initField();
                        CommonApp.InitApp();
                        oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        string Err = Program.oCompany.GetLastErrorDescription();
                    }
                    catch (Exception e)
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
                        if (oCompany.InTransaction)
                        {
                            oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        }
                    }
                }

                RunKill();
                 oApp.Run();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
        }

        private static void SBO_Application_ItemEvent(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.ItemUID =="U_IsOK")
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(FormUID).Items.Item("U_IsOK").Enabled = false;
                    
                }

                if (pVal.EventType == BoEventTypes.et_GOT_FOCUS && pVal.FormTypeEx == "-150" &&
                    (pVal.ItemUID.StartsWith("U_TypeD")) && pVal.BeforeAction == false && pVal.ActionSuccess == true)
                {
                    SAPbouiCOM.Form oForm = SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(FormUID);

                    TypeDValue = ((SAPbouiCOM.EditText)oForm.Items.Item("U_TypeD").Specific).Value;
                }
                if (pVal.EventType == BoEventTypes.et_COMBO_SELECT && pVal.FormTypeEx == "-150" &&
                    (pVal.ItemUID.StartsWith("U_IsMHJ")) && pVal.BeforeAction == false && pVal.ActionSuccess)
                {
                    SAPbouiCOM.Form oForm = SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(FormUID);

                    string IsMHJ = ((SAPbouiCOM.ComboBox)oForm.Items.Item("U_IsMHJ").Specific).Selected.Value;
                    if (IsMHJ.Equals("是"))
                    {
                        ((SAPbouiCOM.EditText)oForm.Items.Item("U_MHJQty").Specific).Value ="2";
                    }
                    else
                    {
                        ((SAPbouiCOM.EditText)oForm.Items.Item("U_MHJQty").Specific).Value = "";
                    }
                }
                

                if (pVal.EventType == BoEventTypes.et_LOST_FOCUS && pVal.FormType == -150 &&
                    (pVal.ItemUID.StartsWith("U_TypeD"))&& pVal.BeforeAction == false)
                {
                    SAPbouiCOM.Form oForm = SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(FormUID);

                    string U_typeD = ((SAPbouiCOM.EditText) oForm.Items.Item("U_TypeD").Specific).Value;
                    if (!TypeDValue.Equals(U_typeD))
                    {
                        string sqlCmd = "SELECT * FROM [@TYPESET] T1 WHERE T1.U_TypeD = '" + U_typeD + "'";
                        SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        oRec.DoQuery(sqlCmd);
                        if (oRec.RecordCount > 0)
                        {
                            try
                            {
                                ((SAPbouiCOM.EditText)oForm.Items.Item("U_TypeC").Specific).Value =
                                    oRec.Fields.Item("U_TypeC").Value.ToString();
                            }
                            catch (Exception e)
                            {

                            }
                            try
                            {
                                ((SAPbouiCOM.EditText)oForm.Items.Item("U_TypeB").Specific).Value =
                                    oRec.Fields.Item("U_TypeB").Value.ToString();
                            }
                            catch (Exception e)
                            {

                            }
                            try
                            {
                                ((SAPbouiCOM.ComboBox)oForm.Items.Item("U_TypeA").Specific).Select(oRec.Fields.Item("U_TypeA").Value.ToString(),BoSearchKey.psk_ByValue);
                            }
                            catch (Exception e)
                            {

                            }
                            AddTableFields.ReleaseCom(oRec);

                        }
                    }

                   
                }

                if (pVal.EventType == BoEventTypes.et_FORM_LOAD && pVal.BeforeAction && pVal.FormTypeEx == "150" )
                {
                   // CreateU_type("U_TypeD");
                    CreateU_type("U_TypeC");
                    CreateU_type("U_TypeB");
                   // CreateU_type("U_TypeA");

                }
            }
            catch (Exception e)
            {
                
            }
            
        }

        private static void CreateU_type(string U_Type)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.MetadataAutoRefresh = true;
            SAPbobsCOM.FormattedSearches oSearches =
                                    Program.oCompany.GetBusinessObject(BoObjectTypes.oFormattedSearches) as
                                        SAPbobsCOM.FormattedSearches;
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            string sqlCmd = "SELECT * FROM CSHS T1 WHERE T1.FormID = 150 AND T1.ItemID='"+U_Type+"'";
            bool Typeexists = false;
            oRec.DoQuery(sqlCmd);
            if (oRec.RecordCount > 0)
            {
                Typeexists = true;
                string IndexID = oRec.Fields.Item("IndexID").Value.ToString();
                sqlCmd = "SELECT MAX(LineNum) LineNum FROM CUVV WHERE IndexID="+IndexID;
                oRec.DoQuery(sqlCmd);
                string maxLine = "0";
                if (oRec.RecordCount > 0)
                {
                    maxLine = oRec.Fields.Item(0).Value.ToString();
                }
                sqlCmd =
                    "INSERT CUVV ( IndexID , Value , LineNum ) "+
                "SELECT '"+IndexID+"',T1."+U_Type+",ROW_NUMBER() OVER(ORDER BY T1."+U_Type+") - 1 + "+maxLine+" FROM[@TYPESET] T1 WHERE NOT EXISTS(SELECT * FROM CUVV T0 WHERE T0.IndexID = '"+IndexID+"' AND T1.U_TypeD = T0.Value) GROUP BY T1."+U_Type+" order by T1."+U_Type;
                oRec.DoQuery(sqlCmd);
                AddTableFields.ReleaseCom(oRec);
                AddTableFields.ReleaseCom(oSearches);

            }
            else
            {
                sqlCmd = "SELECT T1."+U_Type+" FROM [@TYPESET] T1 GROUP BY T1."+U_Type+" ORDER BY T1."+U_Type;
                oRec.DoQuery(sqlCmd);
                if (oRec.RecordCount > 0)
                {
                    oSearches.FormID = "150";
                    oSearches.ItemID = U_Type;
                    oSearches.ColumnID = "-1";
                    oSearches.Action = BoFormattedSearchActionEnum.bofsaValidValues;
                    for (int i = 0; i < oRec.RecordCount; i++)
                    {

                        oSearches.UserValidValues.FieldValue = oRec.Fields.Item(0).Value.ToString();
                        oSearches.UserValidValues.Add();
                        oRec.MoveNext();
                    }
                    AddTableFields.ReleaseCom(oRec);
                    oSearches.Add();
                     string mm = Program.oCompany.GetLastErrorDescription();
                        AddTableFields.ReleaseCom(oSearches);
                }
            }

            
        }
        private static void SBO_Application_FormDataEvent(ref BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (BusinessObjectInfo.BeforeAction && BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.FormTypeEx == "182" && BusinessObjectInfo.Type == "21")
                {
                    string IsOK = Application.SBO_Application.Forms.ActiveForm.DataSources.DBDataSources.Item("ORPD")
                        .GetValue("U_IsOK", 0).ToString();
                    if (!IsOK.Equals("是"))
                    {
                        Application.SBO_Application.MessageBox("采购退货单，录入后请先点右键保存为草稿，传给门店审核后再添加！");
                        BubbleEvent = false;
                    }
                }

                
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox(e.Message);
                BubbleEvent = false;
            }
           
        }

        private static void SBO_Application_RightClickEvent(ref ContextMenuInfo eventinfo, out bool bubbleevent)
        {
            bubbleevent = true;
            SAPbouiCOM.Form oForm =
                SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(eventinfo.FormUID);
            if (oForm.TypeEx == "UDO_FT_TOPDN" && eventinfo.ItemUID == "0_U_G" && eventinfo.Row > 0 )
            {
                if (eventinfo.BeforeAction == true)
                {
                    SAPbouiCOM.MenuItem oMenuItem = null;
                    SAPbouiCOM.Menus oMenus = null;


                    try
                    {
                        CommonApp.TopdnForm = oForm;
                        CommonApp.RowInt = eventinfo.Row;
                        CommonApp.oMatrix =
                            SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Items.Item(
                                eventinfo.ItemUID).Specific as SAPbouiCOM.Matrix;
                        ((SAPbouiCOM.EditText) SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Items
                            .Item("Item_1").Specific).Active = true;
                        SAPbouiCOM.MenuCreationParams oCreationPackage = null;
                        oCreationPackage =
                            ((SAPbouiCOM.MenuCreationParams) (SAPbouiCOM.Framework.Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));

                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                        oCreationPackage.UniqueID = "Ins_Data";
                        oCreationPackage.String = "插入行";
                        oCreationPackage.Enabled = true;

                        oMenuItem = SAPbouiCOM.Framework.Application.SBO_Application.Menus.Item("1280"); // Data'
                        oMenus = oMenuItem.SubMenus;
                        oMenus.AddEx(oCreationPackage);

                        oMenuItem = null;
                        oMenus = null;

                        oCreationPackage =
                            ((SAPbouiCOM.MenuCreationParams)(SAPbouiCOM.Framework.Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));

                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                        oCreationPackage.UniqueID = "Stray_DelRow";
                        oCreationPackage.String = "删除行";
                        oCreationPackage.Enabled = true;

                        oMenuItem = SAPbouiCOM.Framework.Application.SBO_Application.Menus.Item("1280"); // Data'
                        oMenus = oMenuItem.SubMenus;
                        oMenus.AddEx(oCreationPackage);

                        oMenuItem = null;
                        oMenus = null;

                        

                    }
                    catch (Exception ex)
                    {
                       
                    }
                    try
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.Menus.RemoveEx("TOPDN_Add_Line");

                    }
                    catch (Exception ex)
                    {

                    }
                    try
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.Menus.RemoveEx("TOPDN_Remove_Line");

                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    SAPbouiCOM.MenuItem oMenuItem = null;
                    SAPbouiCOM.Menus oMenus = null;


                    try
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.Menus.RemoveEx("Ins_Data");

                    }
                    catch (Exception ex)
                    {
                       
                    }

                    try
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.Menus.RemoveEx("Stray_DelRow");

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                    
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    Process.GetCurrentProcess().Kill();
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    Process.GetCurrentProcess().Kill();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    Process.GetCurrentProcess().Kill();
                    break;
               
                default:
                    Process.GetCurrentProcess().Kill();
                    break;
            }

            try
            {
                if (!Program.oCompany.Connected)
                {
                    Process.GetCurrentProcess().Kill();
                    
                }
            }
            catch (Exception e)
            {
               
            }
        }

        private static void KillThis()
        {
            var hh = DateTime.Now.Hour;
            if (hh.Equals(5))
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private static void RunKill()
        {
            Timer oTimer = new Timer(3600000);
            oTimer.Elapsed += OTimer_Elapsed;
            oTimer.Start();

        }

        private static void OTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            KillThis();
        }
    }
}

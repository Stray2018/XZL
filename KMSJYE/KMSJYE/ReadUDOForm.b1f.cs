﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using SAPbobsCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("InitUDO", "ReadUDOForm.b1f")]
    class ReadUDOForm : UserFormBase
    {
        public ReadUDOForm()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {

        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                SAPbobsCOM.Recordset oRecordset =
                    Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                string sqlCmd = "select Code,Name from OUDO";
                oRecordset.DoQuery(sqlCmd);
                oRecordset.MoveFirst();
                for (int i = 0; i < oRecordset.RecordCount; i++)
                {
                    SAPbobsCOM.UserObjectsMD objects =
                        Program.oCompany.GetBusinessObject(BoObjectTypes.oUserObjectsMD) as SAPbobsCOM.UserObjectsMD;
                    string UDOCode = oRecordset.Fields.Item("Code").Value.ToString();
                    objects.GetByKey(UDOCode);
                    
                    StreamWriter oStreamWriter = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + UDOCode + ".xml");
                    oStreamWriter.Write(objects.FormSRF);
                    oStreamWriter.Flush();
                    oStreamWriter.Close();
                    oStreamWriter.Dispose();
                   
                    AddTableFields.ReleaseCom(objects);
                    oRecordset.MoveNext();
                }

                AddTableFields.ReleaseCom(oRecordset);
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.ToString());
            }
        }
    }
}
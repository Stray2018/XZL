﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using SAPbouiCOM.Framework;
using SAPbobsCOM;
using SAPbouiCOM;

namespace KMSJYE
{
    [FormAttribute("UDO_FT_DELIVEBPL")]
    class DELIVEBPL : UDOFormBase
    {
        public static SAPbouiCOM.DBDataSource LineSource;
        public DELIVEBPL()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_0").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_2").Specific));
            this.ComboBox1.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox1_ComboSelectAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("0_U_G").Specific));
            this.Matrix0.MatrixLoadAfter += new SAPbouiCOM._IMatrixEvents_MatrixLoadAfterEventHandler(this.Matrix0_MatrixLoadAfter);
            this.Matrix0.MatrixLoadBefore += new SAPbouiCOM._IMatrixEvents_MatrixLoadBeforeEventHandler(this.Matrix0_MatrixLoadBefore);
            this.Matrix0.ChooseFromListBefore += new SAPbouiCOM._IMatrixEvents_ChooseFromListBeforeEventHandler(this.Matrix0_ChooseFromListBefore);
            this.Matrix0.ChooseFromListAfter += new SAPbouiCOM._IMatrixEvents_ChooseFromListAfterEventHandler(this.Matrix0_ChooseFromListAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.LoadBefore += new SAPbouiCOM.Framework.FormBase.LoadBeforeHandler(this.Form_LoadBefore);
            this.ActivateAfter += new SAPbouiCOM.Framework.FormBase.ActivateAfterHandler(this.Form_ActivateAfter);
            this.DeactivateBefore += new SAPbouiCOM.Framework.FormBase.DeactivateBeforeHandler(this.Form_DeactivateBefore);
            this.DataAddBefore += new SAPbouiCOM.Framework.FormBase.DataAddBeforeHandler(this.Form_DataAddBefore);
            this.DataLoadAfter += new SAPbouiCOM.Framework.FormBase.DataLoadAfterHandler(this.Form_DataLoadAfter);
            this.ActivateBefore += new SAPbouiCOM.Framework.FormBase.ActivateBeforeHandler(this.Form_ActivateBefore);
            this.DataUpdateBefore += new SAPbouiCOM.Framework.FormBase.DataUpdateBeforeHandler(this.Form_DataUpdateBefore);
            this.LoadAfter += new LoadAfterHandler(this.Form_LoadAfter);

        }

        private SAPbouiCOM.ComboBox ComboBox0;

        private void OnCustomInitialize()
        {
            try
            {
                this.UIAPIRawForm.EnableMenu("1288", true);
                this.UIAPIRawForm.EnableMenu("1289", true);
                this.UIAPIRawForm.EnableMenu("1290", true);
                this.UIAPIRawForm.EnableMenu("1291", true);
                LineSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@LIVEBPL1");

                this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 - this.UIAPIRawForm.Width / 2;
                ComboBox1.ExpandType = BoExpandType.et_ValueOnly;


                SAPbobsCOM.BusinessPlaces oPlaces = Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPlaces) as BusinessPlaces;
                SAPbobsCOM.Users oUsers = Program.oCompany.GetBusinessObject(BoObjectTypes.oUsers) as Users;
                oUsers.GetByKey(Program.oCompany.UserSignature);
                for (int i = 0; i < oUsers.UserBranchAssignment.Count; i++)
                {
                    oUsers.UserBranchAssignment.SetCurrentLine(i);
                    oPlaces.GetByKey(oUsers.UserBranchAssignment.BPLID);
                    if (oPlaces.Disabled == BoYesNoEnum.tNO)
                    {
                        string BPLID0 = oPlaces.BPLID.ToString();
                        string BPLName = oPlaces.BPLName;
                        ComboBox1.ValidValues.Add(BPLName, BPLID0);
                        if (oPlaces.MainBPL == BoYesNoEnum.tYES)
                        {
                            Program.CenterWhs = oPlaces.DefaultWarehouseID;
                        }
                    }

                }
                ComboBox0.ExpandType = BoExpandType.et_DescriptionOnly;
                ComboBox0.DataBind.SetBound(true, "@DELIVEBPL", "Series");
                ComboBox0.ValidValues.LoadSeries("DELIVEBPL", SAPbouiCOM.BoSeriesMode.sf_View);
                ComboBox0.Select(0, BoSearchKey.psk_Index);
            }
            catch (Exception e)
            {
               
            }
            

        }

        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.ComboBox ComboBox1;

        private void Form_LoadBefore(SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //throw new System.NotImplementedException();

        }

        private void ComboBox1_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                string BPLName = ComboBox1.Selected.Value;
                string BPLId = ComboBox1.Selected.Description;
                SAPbobsCOM.BusinessPlaces oPlaces = Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPlaces) as BusinessPlaces;
                oPlaces.GetByKey(Int32.Parse(BPLId));
                SAPbobsCOM.BusinessPartners oPartners = Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners) as BusinessPartners;
                oPartners.GetByKey(oPlaces.DefaultCustomerID);
                SAPbouiCOM.DBDataSource oSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@DELIVEBPL");
                oSource.SetValue("U_CardCode", 0, oPartners.CardCode);
                oSource.SetValue("U_CardName", 0, oPartners.CardName);

                oSource.SetValue("U_BPLId", 0, BPLId);
                oSource.SetValue("U_Address2", 0, oPlaces.Street + oPlaces.StreetNo);
                oSource.SetValue("U_DocDate", 0, DateTime.Now.ToString("yyyyMMdd"));
                oSource.SetValue("U_DocDueDate", 0, DateTime.Now.AddDays(2).ToString("yyyyMMdd"));
                oSource.SetValue("Status", 0, "O");
                ComboBox0.ExpandType = BoExpandType.et_DescriptionOnly;
                ComboBox0.DataBind.SetBound(true, "@DELIVEBPL", "Series");
                ComboBox0.ValidValues.LoadSeries("DELIVEBPL", SAPbouiCOM.BoSeriesMode.sf_Add);
                ComboBox0.Select(0, BoSearchKey.psk_Index);

                if (this.UIAPIRawForm.Mode == BoFormMode.fm_ADD_MODE)
                {
                    string series = ComboBox0.Selected.Value;
                    int DocNum = this.UIAPIRawForm.BusinessObject.GetNextSerialNumber(series);
                    oSource.SetValue("DocNum", 0, DocNum.ToString());
                }
                this.UIAPIRawForm.Refresh();

            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }

        }

        private Matrix Matrix0;

        private void Matrix0_ChooseFromListAfter(object sboObject, SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            try
            {
                SAPbouiCOM.DataTable oDt = ((SAPbouiCOM.ISBOChooseFromListEventArg)pVal).SelectedObjects;
                if (!oDt.Equals(null))
                {
                    string ItemCode = oDt.GetValue("ItemCode", 0).ToString();
                    string ItemName = oDt.GetValue("ItemName", 0).ToString();
                    string AppNo = oDt.GetValue("U_AppNo", 0).ToString();
                    string Factory = oDt.GetValue("U_Factory", 0).ToString();
                    string Speic = oDt.GetValue("U_Speic", 0).ToString();
                    string unitMsr = oDt.GetValue("InvntryUom", 0).ToString();

                    SAPbobsCOM.Items oItems =
                        Program.oCompany.GetBusinessObject(BoObjectTypes.oItems) as SAPbobsCOM.Items;
                    if (oItems.GetByKey(ItemCode))
                    {
                        if (oItems.ManageBatchNumbers == BoYesNoEnum.tYES)
                        {
                            string sqlCmd = "SELECT T1.DistNumber 批号,CONVERT(VARCHAR(10),T1.MnfDate,112) 生产日期,CONVERT(VARCHAR(10),T1.ExpDate,112) 有效期至,T2.Quantity-ISNULL(T3.Qty,0) 可发数量, null 发货数量 FROM dbo.OBTN T1 INNER JOIN dbo.OBTQ T2 ON T1.AbsEntry=T2.MdAbsEntry AND T2.Quantity<>0 AND T1.ItemCode='" + ItemCode + "' AND T2.WhsCode='" + Program.CenterWhs + "'" +
                                            " LEFT JOIN (SELECT U1.U_ItemCode, U1.U_DistNumber, SUM(U1.U_Quantity) Qty FROM[dbo].[@DELIVEBPL] U0 INNER JOIN[dbo].[@LIVEBPL1] U1 ON U1.DocEntry = U0.DocEntry AND U0.Status <> 'C' GROUP BY U1.U_ItemCode, U1.U_DistNumber)" +
                                            " T3 ON T1.ItemCode = T3.U_ItemCode AND T1.DistNumber = T3.U_DistNumber";
                            SAPbouiCOM.DataTable oTable = this.UIAPIRawForm.DataSources.DataTables.Item("BatchDe");
                            oTable.ExecuteQuery(sqlCmd);
                            if (oTable.Rows.Count ==1)
                            {
                               
                                LineSource.InsertRecord(pVal.Row);
                                LineSource.SetValue("LineId", pVal.Row, (pVal.Row+1).ToString());
                                LineSource.SetValue("U_ItemCode", pVal.Row - 1, ItemCode);
                                LineSource.SetValue("U_ItemName", pVal.Row - 1, ItemName);
                                LineSource.SetValue("U_Speic", pVal.Row - 1, Speic);
                                LineSource.SetValue("U_AppNo", pVal.Row - 1, AppNo);
                                LineSource.SetValue("U_Factory", pVal.Row - 1, Factory);
                                LineSource.SetValue("U_unitMsr", pVal.Row - 1, unitMsr);
                                LineSource.SetValue("U_ComQty",pVal.Row-1, oTable.GetValue("可发数量",0).ToString());
                                LineSource.SetValue("U_DistNumber", pVal.Row - 1, oTable.GetValue("批号", 0).ToString());
                                LineSource.SetValue("U_MnfDate", pVal.Row - 1, oTable.GetValue("生产日期", 0).ToString());
                                LineSource.SetValue("U_ExpDate", pVal.Row - 1, oTable.GetValue("有效期至", 0).ToString());

                                Matrix0.LoadFromDataSourceEx(false);
                            }
                            if (oTable.Rows.Count > 1)
                            {
                                BatchQty.CurrRowIndex = pVal.Row;
                                BatchQty.ItemCode = ItemCode;
                                BatchQty.ItemName = ItemName;
                                BatchQty.AppNo = AppNo;
                                BatchQty.Factory = Factory;
                                BatchQty.Speic = Speic;
                                BatchQty.unitMsr = unitMsr;
                                BatchQty.sqlCmd = sqlCmd;
                                BatchQty oBatchQty = new BatchQty();
                                oBatchQty.Show();
                            }
                            
                        }
                    }

                    
                }
            }
            catch (Exception e)
            {
                
            }

        }

        private void Form_ActivateAfter(SBOItemEventArg pVal)
        {
            //this.UIAPIRawForm.Freeze(true);
            for (int i = 0; i < LineSource.Size; i++)
            {
                LineSource.SetValue("LineId",i,(i+1).ToString());
            }
            Matrix0.LoadFromDataSourceEx(false);
            this.UIAPIRawForm.Freeze(false);

        }

        private void Form_DeactivateBefore(SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.Matrix0.FlushToDataSource();

        }

        private void Matrix0_ChooseFromListBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            Matrix0.FlushToDataSource();

        }

        private void Form_DataAddBefore(ref BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = DataCheck();
            //throw new System.NotImplementedException();
            
           

        }
        private bool DataCheck()
        {
           bool BubbleEvent = true;
            try
            {
                /*删除空行数据*/
                for (int i = Matrix0.RowCount - 1; i >= 0; i--)
                {
                    if (LineSource.GetValue("U_ItemCode", i).ToString().Equals(""))
                    {
                        LineSource.RemoveRecord(i);
                        continue;
                    }

                    string Quantity = LineSource.GetValue("U_Quantity", i).ToString();
                    if (double.Parse(Quantity) == 0)
                    {
                        SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("第" + (i + 1).ToString() + "行，物料：" +
                            LineSource.GetValue("U_ItemCode", i).ToString() + "  发货数量为0，请确认!");
                        this.UIAPIRawForm.Freeze(true);
                        LineSource.InsertRecord(LineSource.Size);
                        LineSource.SetValue("LineId", LineSource.Size - 1, LineSource.Size.ToString());
                        this.Matrix0.LoadFromDataSourceEx(false);
                        this.UIAPIRawForm.Freeze(false);
                        BubbleEvent = false;
                        return BubbleEvent;
                    }
                }
                /*判断是否选择分支*/
                if (ComboBox1.Selected.Value.Equals(""))
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("分支未选择，请选择！");
                    BubbleEvent = false;
                    return BubbleEvent;
                }
                //判断行中是否存在数据
                if (LineSource.Size == 0)
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("未录入数据，请确认！");
                    this.UIAPIRawForm.Freeze(true);
                    LineSource.InsertRecord(0);
                    LineSource.SetValue("LineId", LineSource.Size - 1, LineSource.Size.ToString());
                    this.Matrix0.LoadFromDataSourceEx(false);
                    this.UIAPIRawForm.Freeze(false);
                    BubbleEvent = false;
                    return BubbleEvent;
                }
                return BubbleEvent;
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
                BubbleEvent = false;
                return BubbleEvent;
            }
        }
        private void Form_DataLoadAfter(ref BusinessObjectInfo pVal)
        {
            //throw new System.NotImplementedException();
           
            try
            {
                string Status = this.UIAPIRawForm.DataSources.DBDataSources.Item("@DELIVEBPL").GetValue("Status", 0).Trim();
                if (Status.Equals("P") || Status.Equals("C"))
                {
                    this.UIAPIRawForm.Mode = BoFormMode.fm_VIEW_MODE;
                    Button0.Item.Visible = false;
                    Button1.Item.Visible = false;
                }
                else
                {
                    this.UIAPIRawForm.Mode = BoFormMode.fm_OK_MODE;
                    Button0.Item.Visible = true;
                    Button1.Item.Visible = true;
                }
                LineSource.InsertRecord(LineSource.Size);
                LineSource.SetValue("LineId", LineSource.Size - 1, LineSource.Size.ToString());
                this.Matrix0.LoadFromDataSourceEx(false);
            }
            catch (Exception e)
            {

            }

        }

        private void Form_ActivateBefore(SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            this.UIAPIRawForm.Freeze(true);

        }

        private void Matrix0_MatrixLoadBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
           // throw new System.NotImplementedException();
           this.UIAPIRawForm.Freeze(true);
        }

        private void Matrix0_MatrixLoadAfter(object sboObject, SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            this.UIAPIRawForm.Freeze(false);
        }

        private Button Button0;

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            try
            {
                this.Matrix0.SetCellFocus(1,1);
                string BPLId = this.UIAPIRawForm.DataSources.DBDataSources.Item("@DELIVEBPL").GetValue("U_BPLId", 0);
                if (BPLId.Equals(string.Empty))
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("分支未选择，请先选择分支!");
                    return;
                }

                string sqlCmd =
                    "SELECT T1.BPLName 分支,T1.DocNum 单号,T1.DocDate 单据日期,T1.ReqDate 必须日期,T1.Comments 备注 ,T1.DocEntry 内部单号 FROM dbo.OPRQ T1 WHERE T1.BPLId='" + BPLId + "' AND T1.DocStatus='O'";
                DelCopyForm.sqlCmd = sqlCmd;
                DelCopyForm oDelCopyForm = new DelCopyForm();
                oDelCopyForm.Show();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
           
        }

        private void Form_DataUpdateBefore(ref BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = DataCheck();
           

        }

        private Button Button1;

        private void Button1_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            string sqlCmd = "UPDATE [@DELIVEBPL] Set Status = 'P' where DocEntry = " + this.UIAPIRawForm.DataSources.DBDataSources.Item("@DELIVEBPL").GetValue("DocEntry", 0).Trim();
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery(sqlCmd);
            SAPbouiCOM.Framework.Application.SBO_Application.ActivateMenuItem("1304");
            AddTableFields.ReleaseCom(oRec);

        }

        private void Form_LoadAfter(SBOItemEventArg pVal)
        {
            
            

        }
    }
}

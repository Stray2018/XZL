﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;

namespace KMSJYE
{
    class AddTableFields
    {
        #region 添加B1数据表方法
        /// <summary>
        /// AddTableToB1DB
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="tableDes"></param>
        /// <param name="tableType"></param>
        /// <returns>success_flag</returns>
        public static void AddB1Table(SAPbobsCOM.Company oCompany, string tableName, string tableDes, BoUTBTableType tableType)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Start Create Table:" + tableName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            SAPbobsCOM.UserTablesMD oUserTablesMd =
                (SAPbobsCOM.UserTablesMD)oCompany.GetBusinessObject(BoObjectTypes.oUserTables);
            
            if (!oUserTablesMd.GetByKey(tableName))
            {
                oUserTablesMd.TableName = tableName;
                oUserTablesMd.TableDescription = tableDes;
                oUserTablesMd.TableType = tableType;

                if (oUserTablesMd.Add() == 0)
                {
                    ReleaseCom(oUserTablesMd);
                    SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                  
                }
                else
                {

                    ReleaseCom(oUserTablesMd);
                    SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName+"  Err:"+oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                   
                }
            }
            else
            {
                ReleaseCom(oUserTablesMd);
                SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Table:" + tableName+" Is already Exists", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
               
            }
        }
        #endregion
        #region 添加B1字段到数据表方法
        /// <summary>
        /// AddFieldsToB1DBTable
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="fieldName"></param>
        /// <param name="fieldDes"></param>
        /// <param name="fieldTypes"></param>
        /// <param name="subTypes"></param>
        /// <param name="length"></param>
        /// <returns>success_flag</returns>
        public static bool AddB1Fields(SAPbobsCOM.Company oCompany, string tableName, string fieldName, string fieldDes, BoFieldTypes fieldTypes, BoFldSubTypes subTypes, int? length, Dictionary<string, string> oDictionary)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Start Create Fields:" + tableName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            SAPbobsCOM.UserFieldsMD oUserFieldsMD =
                (SAPbobsCOM.UserFieldsMD)oCompany.GetBusinessObject(BoObjectTypes.oUserFields);
            string fieldId = fieldID(tableName, fieldName);
            bool fieldExists = false;
            if (!fieldId.Equals(""))
            {
                fieldExists = oUserFieldsMD.GetByKey(tableName, Convert.ToInt32(fieldId));
            }
            oUserFieldsMD.TableName = tableName;
            oUserFieldsMD.Name = fieldName;
            oUserFieldsMD.Description = fieldDes;
            oUserFieldsMD.Type = fieldTypes;
            oUserFieldsMD.SubType = subTypes;
           
            if (oDictionary != null && !fieldExists)
            {
                foreach (KeyValuePair<string, string> keyValuePair in oDictionary)
                {
                    oUserFieldsMD.ValidValues.Value = keyValuePair.Key;
                    oUserFieldsMD.ValidValues.Description = keyValuePair.Value;
                    oUserFieldsMD.ValidValues.Add();
                }
                if (fieldName == "Quantity")
                {
                    oUserFieldsMD.DefaultValue = "1";
                }
            }
            if (length.HasValue)
            {
                oUserFieldsMD.EditSize = length.Value;
            }
            // oUserFieldsMD.SubType = subTypes;

            if (fieldExists)
            {
                if (oUserFieldsMD.Update() == 0)
                {
                    ReleaseCom(oUserFieldsMD);
                    SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName + "  Create Fields:" + fieldName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return true;
                }
                else
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.SetStatusBarMessage(oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Long, true);
                    ReleaseCom(oUserFieldsMD);
                    return false;
                }
            }
            else
            {
                if (oUserFieldsMD.Add() == 0)
                {
                    ReleaseCom(oUserFieldsMD);
                    SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName + "  Create Fields:" + fieldName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return true;
                }
                else
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.SetStatusBarMessage(oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Long, true);
                    ReleaseCom(oUserFieldsMD);
                    return false;
                }
            }
        }
        #endregion
        public static bool AddB1Fields(SAPbobsCOM.Company oCompany, string tableName, string fieldName, string fieldDes, BoFieldTypes fieldTypes, BoFldSubTypes subTypes, int? length, Dictionary<string, string> oDictionary,string LinkType, string LinkObjName, UDFLinkedSystemObjectTypesEnum oLinkEnum)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Start Create Fields:" + tableName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            SAPbobsCOM.UserFieldsMD oUserFieldsMD =
                (SAPbobsCOM.UserFieldsMD)oCompany.GetBusinessObject(BoObjectTypes.oUserFields);
            string fieldId = fieldID(tableName, fieldName);
            bool fieldExists = false;
            if (!fieldId.Equals(""))
            {
                fieldExists= oUserFieldsMD.GetByKey(tableName, Convert.ToInt32(fieldId));
            }
                oUserFieldsMD.TableName = tableName;
            oUserFieldsMD.Name = fieldName;
            oUserFieldsMD.Description = fieldDes;
            oUserFieldsMD.Type = fieldTypes;
            oUserFieldsMD.SubType = subTypes;

            if (oDictionary != null && !fieldExists)
            {
                foreach (KeyValuePair<string, string> keyValuePair in oDictionary)
                {
                    oUserFieldsMD.ValidValues.Value = keyValuePair.Key;
                    oUserFieldsMD.ValidValues.Description = keyValuePair.Value;
                    oUserFieldsMD.ValidValues.Add();
                }
                if (fieldName == "Quantity")
                {
                    oUserFieldsMD.DefaultValue = "1";
                }
            }
            if (length.HasValue)
            {
                oUserFieldsMD.EditSize = length.Value;
            }
            // oUserFieldsMD.SubType = subTypes;
            switch (LinkType)
            {
                case "LinkTable":
                    oUserFieldsMD.LinkedTable = LinkObjName;
                    break;
                case "LinkOUDO":
                    oUserFieldsMD.LinkedUDO = LinkObjName;
                    break;
                case "LinkObjSYS":
                    oUserFieldsMD.LinkedSystemObject = oLinkEnum;
                    break;
               
            }
           
            if (fieldExists)
            {
                if (oUserFieldsMD.Update() == 0)
                {
                    ReleaseCom(oUserFieldsMD);
                    SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName + "  Create Fields:" + fieldName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return true;
                }
                else
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.SetStatusBarMessage(oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Long, true);
                    ReleaseCom(oUserFieldsMD);
                    return false;
                }
            }
            else
            {
                if (oUserFieldsMD.Add() == 0)
                {
                    ReleaseCom(oUserFieldsMD);
                    SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName + "  Create Fields:" + fieldName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    return true;
                }
                else
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.SetStatusBarMessage(oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Long, true);
                    ReleaseCom(oUserFieldsMD);
                    return false;
                }
            }
               
        }

        public static string fieldID(string TableName, string FieldName)
        {
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            string osSql = "SELECT FieldID FROM CUFD WHERE TableID = '"+TableName+ "' AND AliasID = '" + FieldName+"'";
            oRec.DoQuery(osSql);
            if (oRec.RecordCount>0)
            {
                string Resultstr= oRec.Fields.Item(0).Value.ToString();
                ReleaseCom(oRec);
                return Resultstr;

            }
ReleaseCom(oRec);
            return "";
        }
        #region 添加对象可查找列
        public static void AddCanFindColumns(SAPbobsCOM.UserObjectMD_FindColumns Columns, string ColumnAlias, string ColumnDescription)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Set Find Fields:" + ColumnAlias, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            for (int i = 0; i < Columns.Count; i++)
            {
                Columns.SetCurrentLine(i);
                if (Columns.ColumnAlias == ColumnAlias)
                {
                    Columns.ColumnAlias = ColumnAlias;
                    Columns.ColumnDescription = ColumnDescription;
                    return;
                }
            }
            Columns.ColumnAlias = ColumnAlias;
            Columns.ColumnDescription = ColumnDescription;
            Columns.Add();
        }

        #endregion

        #region 添加对象默认字段
        public static void AddFormColumns(SAPbobsCOM.UserObjectMD_FormColumns Columns, string ColumnAlias, string ColumnDescription, BoYesNoEnum YoS,int j)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Set Default Fields:" + ColumnAlias, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            for (int i = 0; i < Columns.Count; i++)
            {
                Columns.SetCurrentLine(i);
                if (Columns.FormColumnAlias == ColumnAlias)
                {
                    Columns.FormColumnAlias = ColumnAlias;
                    Columns.FormColumnDescription = ColumnDescription;
                    Columns.Editable = YoS;
                    Columns.SonNumber = j;
                    return;
                }
            }
            Columns.FormColumnAlias = ColumnAlias;
            Columns.FormColumnDescription = ColumnDescription;
            Columns.Editable = YoS;
            Columns.SonNumber = j;
            Columns.Add();
        }
        #endregion

        #region 添加对象默认字段
        public static void AddChildFormColumns(SAPbobsCOM.UserObjectMD_EnhancedFormColumns Columns, string ColumnAlias, string ColumnDescription, BoYesNoEnum editable,BoYesNoEnum ColIsus ,int j)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Set Default Column:"+ColumnAlias,BoMessageTime.bmt_Short,BoStatusBarMessageType.smt_Success);
            for (int i = 0; i < Columns.Count; i++)
            {
                Columns.SetCurrentLine(i);
                if (Columns.ColumnAlias == ColumnAlias)
                {
                    Columns.ColumnAlias = ColumnAlias;
                    Columns.ColumnDescription = ColumnDescription;
                    Columns.Editable = editable;
                    Columns.ColumnIsUsed = ColIsus;
                    Columns.ChildNumber = j;
                    return;
                }
            }
            Columns.ColumnAlias = ColumnAlias;
            Columns.ColumnDescription = ColumnDescription;
            Columns.Editable = editable;
            Columns.ColumnIsUsed = ColIsus;
            Columns.ChildNumber = j;
            Columns.Add();
        }
        #endregion
        #region 添加对象子表

        public static void AddChildTable(SAPbobsCOM.UserObjectMD_ChildTables childTables,string ChildTablename)
        {

            for (int i = 0; i < childTables.Count; i++)
            {
             childTables.SetCurrentLine(i);
              if(childTables.TableName==ChildTablename)
              {return;}
            }
            childTables.TableName = ChildTablename;
            childTables.LogTableName = "A" + ChildTablename;
            childTables.Add();
            
        }
        #endregion
        public static void GetChildTableSonNumber(SAPbobsCOM.UserObjectMD_ChildTables childTables,string ChildTableName,out int songnumber)
        {
            songnumber = 0;
            for (int i=0;i<childTables.Count;i++)
            {
                childTables.SetCurrentLine(i);
                if (childTables.TableName == ChildTableName)
                {
                    songnumber = childTables.SonNumber+1;
                    return;
                }
            }
        }
        public static void ReleaseCom(Object o)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
        }
    }
}

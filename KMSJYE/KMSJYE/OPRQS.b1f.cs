﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("UDO_FT_OPRQS")]
    class OPRQS : UDOFormBase
    {
        public static SAPbouiCOM.DBDataSource OPRQSdb;
        public OPRQS()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_0").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_1").Specific));
            this.ComboBox0.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox0_ComboSelectAfter);
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("21_U_E").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("20_U_E").Specific));
            this.EditText2.KeyDownBefore += new SAPbouiCOM._IEditTextEvents_KeyDownBeforeEventHandler(this.EditText2_KeyDownBefore);
            this.EditText2.LostFocusAfter += new SAPbouiCOM._IEditTextEvents_LostFocusAfterEventHandler(this.EditText2_LostFocusAfter);
            this.EditText2.PressedAfter += new SAPbouiCOM._IEditTextEvents_PressedAfterEventHandler(this.EditText2_PressedAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("0_U_G").Specific));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_2").Specific));
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_3").Specific));
            this.Matrix1 = ((SAPbouiCOM.Matrix)(this.GetItem("1_U_G").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {

        }

        private SAPbouiCOM.EditText EditText0;

        private void OnCustomInitialize()
        {
            ComboBox0.ExpandType = BoExpandType.et_DescriptionOnly;
            if (this.UIAPIRawForm.Mode == BoFormMode.fm_ADD_MODE)
            {
                ComboBox0.ValidValues.LoadSeries("OPRQS", SAPbouiCOM.BoSeriesMode.sf_Add);
                EditText1.Value = DateTime.Now.ToString("yyyyMMdd");
                ComboBox0.Select(0, BoSearchKey.psk_Index);

                string series = ComboBox0.Selected.Value;

                int DocNum = this.UIAPIRawForm.BusinessObject.GetNextSerialNumber(series);
                OPRQSdb = this.UIAPIRawForm.DataSources.DBDataSources.Item("@OPRQS");
                OPRQSdb.SetValue("DocNum", 0, DocNum.ToString());
            }
            else
            {
                ComboBox0.ValidValues.LoadSeries("OPRQS", SAPbouiCOM.BoSeriesMode.sf_View);
            }

           
            ComboBox1.ExpandType = BoExpandType.et_DescriptionOnly;
            ComboBox1.Item.Enabled = false;

        }

        private SAPbouiCOM.ComboBox ComboBox0;

        private void ComboBox0_ComboSelectAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (this.UIAPIRawForm.Mode == BoFormMode.fm_ADD_MODE)
            {

                SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@OPRQS");
                int DocNum1 = this.UIAPIRawForm.BusinessObject.GetNextSerialNumber(ComboBox0.Selected.Value, "OPRQS");
                oDataSource.SetValue("DocNum", 0, DocNum1.ToString());
                this.UIAPIRawForm.Refresh();
            }

        }

        private EditText EditText1;
        private EditText EditText2;

        private void EditText2_PressedAfter(object sboObject, SBOItemEventArg pVal)
        {
           

        }

        private void EditText2_LostFocusAfter(object sboObject, SBOItemEventArg pVal)
        {
            
            string DocString = OPRQSdb.GetValue("U_OPRQDoc", 0);
            string[] DocList = DocString.Split(',');
            SAPbouiCOM.DBDataSource PRQ2S = this.UIAPIRawForm.DataSources.DBDataSources.Item("@PRQ2S");
            for (int i = 0; i < DocList.Length; i++)
            {
                PRQ2S.InsertRecord(i);
                PRQ2S.SetValue("U_OPRQDoc",i,DocList[i]);

            }
            string sqlCmd = "SELECT T1.ItemCode U_ItemCode,T1.Dscription U_ItemName,T1.U_Speic,T1.U_Factory,T1.unitMsr U_unitMsr,sum(T1.Quantity) U_Quantity,T1.U_CntcPrsn ,t2.U_IsCSale FROM PRQ1 T1 INNER JOIN OITM T2 ON T1.ItemCode=T2.ItemCode WHERE T1.DocEntry IN("+ DocString + ") " +
                            "group by T1.ItemCode ,T1.Dscription ,T1.U_Speic,T1.U_Factory,T1.unitMsr ,T1.U_CntcPrsn ,t2.U_IsCSale";
            SAPbouiCOM.DataTable oDt = this.UIAPIRawForm.DataSources.DataTables.Item("CxData");
            oDt.ExecuteQuery(sqlCmd);
            string xx = oDt.SerializeAsXML(BoDataTableXmlSelect.dxs_DataOnly);
            xx = xx.Replace("DataTable", "dbDataSources").Replace("CxData", "@PRQ1S").Replace("ColumnUid", "uid").Replace("Row","row").Replace("Cell","cell").Replace("Value","value");
            try
            {

                this.UIAPIRawForm.DataSources.DBDataSources.Item("@PRQ1S").LoadFromXML(xx);
            }
            catch (Exception e)
            {

            }
         
          Matrix1.LoadFromDataSourceEx(false);
            Matrix0.LoadFromDataSourceEx(false);
        }

        private Matrix Matrix0;

        private void EditText2_KeyDownBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = false;
            if (pVal.CharPressed == 9)
            {
                OPRQList oprqList = new OPRQList();
                oprqList.Show();
            }
        }

        private StaticText StaticText0;
        private ComboBox ComboBox1;
        private Matrix Matrix1;
        private Button Button0;

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            string DocEntry = this.UIAPIRawForm.DataSources.DBDataSources.Item("@OPRQS").GetValue("DocEntry", 0);
           Dictionary<string ,string> linkDictionary = new Dictionary<string, string>();
            linkDictionary.Add("物料编号", "4");
            linkDictionary.Add("申请单号", "1470000113");
            CxForm.LinkDictionary = linkDictionary;
            CxForm.TitleName = "采购申请无报价物料";
            CxForm.sqlCmd= "SELECT T4.ItemCode 物料编号,T4.Dscription 物料描述,T4.U_Speic 规格,T4.unitMsr 单位,T4.Quantity 申请数量,T4.U_CntcPrsn 联系人,t4.U_Factory 产地,T1.U_IsCsale 控销,t5.BPLName 申请分支,T5.DocDate 申请日期,T5.DocEntry 申请单号 FROM [@PRQ1S] T1 LEFT JOIN PQT1 T2 ON T1.U_ItemCode=T2.ItemCode LEFT JOIN OPQT T3 ON T2.DocEntry=T3.DocEntry AND T3.NumAtCard='"+DocEntry+"' "+
            "LEFT JOIN PRQ1 T4 ON T1.U_ItemCode = T4.ItemCode AND T4.DocEntry IN(SELECT U_OPRQDoc FROM[@PRQ2S] WHERE DocEntry = '"+DocEntry+"') LEFT JOIN OPRQ T5 ON T5.DocEntry = T4.DocEntry "+
            "WHERE T1.DocEntry = '"+DocEntry+"' AND T2.ItemCode IS NULL";
            CxForm oCxForm = new CxForm();
            oCxForm.Show();
          


        }
    }
}

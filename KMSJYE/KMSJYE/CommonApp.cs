﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SAPbobsCOM;
using SAPbouiCOM;

namespace KMSJYE
{
    class CommonApp
    {
        public static SAPbouiCOM.Form TopdnForm = null;
        public static int RowInt = 0;
        public static SAPbouiCOM.Matrix oMatrix;

        public static void InitApp()
        {
            SAPbobsCOM.Recordset oRecordset =
                Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRecordset.DoQuery("SELECT GroupCode FROM OCRG WHERE GroupType='S' and GroupName in('生产商','批发商')");
            if (oRecordset.RecordCount != 2)
            {

                oRecordset.DoQuery("SELECT GroupCode FROM OCRG WHERE GroupType='S'");
                if (oRecordset.RecordCount > 0)
                {
                    SAPbobsCOM.BusinessPartnerGroups oGroups =
                        Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups) as
                            SAPbobsCOM.BusinessPartnerGroups;
                    for (int i = 0; i < oRecordset.RecordCount; i++)
                    {
                        oGroups =
                            Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups) as
                                SAPbobsCOM.BusinessPartnerGroups;
                        int groupCode = int.Parse(oRecordset.Fields.Item(0).Value.ToString());
                        if (i == 0)
                        {
                            oGroups.GetByKey(groupCode);
                            oGroups.Name = "生产商";
                            oGroups.Update();
                        }
                        else
                        {
                            oGroups.GetByKey(groupCode);
                            oGroups.Remove();
                        }

                        AddTableFields.ReleaseCom(oGroups);
                        oRecordset.MoveNext();
                    }

                    oGroups =
                        Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups) as
                            SAPbobsCOM.BusinessPartnerGroups;
                    oGroups.Name = "批发商";
                    oGroups.Type = BoBusinessPartnerGroupTypes.bbpgt_VendorGroup;
                    oGroups.Add();
                    AddTableFields.ReleaseCom(oGroups);

                }
            }

            AddTableFields.ReleaseCom(oRecordset);

            #region 创建供应商资质管理对象

            SAPbobsCOM.UserObjectsMD SUQUA1 =
                (SAPbobsCOM.UserObjectsMD) Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);

            bool haveOBJ1 = SUQUA1.GetByKey("SUQUA");

            #region 设置主表属性

            SUQUA1.Code = "SUQUA"; //对象代码
            SUQUA1.Name = "供应商资质信息"; //对象名称
            SUQUA1.ObjectType = SAPbobsCOM.BoUDOObjType.boud_MasterData; //绑定数据类型
            SUQUA1.TableName = "SUQUA"; //表名

            SUQUA1.CanFind = SAPbobsCOM.BoYesNoEnum.tYES; //是否可查找
            SUQUA1.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO;
            SUQUA1.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES;
            SUQUA1.CanClose = SAPbobsCOM.BoYesNoEnum.tYES;
            SUQUA1.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES; //创建默认form
            SUQUA1.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO;
            SUQUA1.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES;
            // SUQUA1.ManageSeries = BoYesNoEnum.tNO;
            SUQUA1.MenuItem = SAPbobsCOM.BoYesNoEnum.tYES;
            SUQUA1.MenuCaption = "开发_供应商资质信息";
            SUQUA1.MenuUID = "SUQUA";
            SUQUA1.FatherMenuID = 2304;
            SUQUA1.Position = 0;
            SUQUA1.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
            FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + SUQUA1.Code + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader oReader1 = new StreamReader(fs, System.Text.Encoding.UTF8);
            SUQUA1.FormSRF = oReader1.ReadToEnd();
            oReader1.Close();
            oReader1.Dispose();
            fs.Close();
            fs.Dispose();

            #endregion

            #region 设置子表

            AddTableFields.AddChildTable(SUQUA1.ChildTables, "UQUA1");

            #region 设置查找字段

            AddTableFields.AddCanFindColumns(SUQUA1.FindColumns, "Code", "供应商编码");
            AddTableFields.AddCanFindColumns(SUQUA1.FindColumns, "Name", "供应商名称");

            #endregion

            #region 设置表格默认字段


            AddTableFields.AddFormColumns(SUQUA1.FormColumns, "Code", "供应商编码", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(SUQUA1.FormColumns, "Name", "供应商名称", BoYesNoEnum.tYES, 0);

            #endregion

            AddTableFields.AddFormColumns(SUQUA1.FormColumns, "U_QuaName", "资质证书", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(SUQUA1.FormColumns, "U_QuaDoc", "资质证书文档", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(SUQUA1.FormColumns, "U_QuaExp", "过期日期", BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(SUQUA1.EnhancedFormColumns, "U_QuaName", "资质证书", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(SUQUA1.EnhancedFormColumns, "U_QuaDoc", "资质证书文档", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(SUQUA1.EnhancedFormColumns, "U_QuaExp", "过期日期", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);

            #endregion

            if (!haveOBJ1)
            {
                SUQUA1.Add();
            }
            else
            {
                SUQUA1.Update();
            }

            string err1 = Program.oCompany.GetLastErrorDescription();
            AddTableFields.ReleaseCom(SUQUA1);




            #endregion


            #region 创建采购收货单对象

            SAPbobsCOM.UserObjectsMD objectsMd =
                (SAPbobsCOM.UserObjectsMD) Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);

            bool haveOBJ = objectsMd.GetByKey("TOPDN");

            #region 设置主表属性

            objectsMd.Code = "TOPDN"; //对象代码
            objectsMd.Name = "采购收货_开发"; //对象名称
            objectsMd.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document; //绑定数据类型
            objectsMd.TableName = "TOPDN"; //表名
            objectsMd.CanFind = SAPbobsCOM.BoYesNoEnum.tYES; //是否可查找
            objectsMd.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO;
            objectsMd.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.CanClose = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES; //创建默认form
            objectsMd.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO;
            objectsMd.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.ManageSeries = BoYesNoEnum.tYES;
            objectsMd.MenuItem = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.MenuCaption = "开发_采购收货";
            objectsMd.MenuUID = "TOPDN";
            objectsMd.FatherMenuID = 2304;
            objectsMd.Position = 0;
            objectsMd.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
             fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + objectsMd.Code + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
             StreamReader oReader2 = new StreamReader(fs, System.Text.Encoding.UTF8);
            objectsMd.FormSRF = oReader2.ReadToEnd();
            oReader2.Close();
            oReader2.Dispose();
            fs.Close();
            fs.Dispose();

            #endregion

            #region 设置子表

            AddTableFields.AddChildTable(objectsMd.ChildTables, "TPDN1");

            #endregion

            #region 设置查找字段

            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "Remark", "备注");
            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "U_CardCode", "供应商");
            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "U_CardName", "名称");
            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "U_BPLID", "分支");

            #endregion

            #region 设置表格默认字段

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_BaseDoc", "基本单据", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_BaseLine", "基本单据行", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Baseobj", "基本单据类型", BoYesNoEnum.tNO, 1);

            #endregion

            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_BaseDoc", "基本单据", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_BaseLine", "基本单据行", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_Baseobj", "基本单据类型", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);


            AddTableFields.AddFormColumns(objectsMd.FormColumns, "DocEntry", "DocEntry", BoYesNoEnum.tNO, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "DocNum", "单据编号", BoYesNoEnum.tNO, 0);

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_CardCode", "供应商", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_CardName", "名称.", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_DocDate", "收货日期", BoYesNoEnum.tYES,
                0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_BPLID", "分支", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_DocTotal", "不含税金额", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Gtotal", "含税金额", BoYesNoEnum.tYES,
                0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "Remark", "备注", BoYesNoEnum.tYES, 0);
            if (!haveOBJ)
            {
                objectsMd.Add();
            }
            else
            {
                objectsMd.Update();
            }

            err1 = Program.oCompany.GetLastErrorDescription();
            AddTableFields.ReleaseCom(objectsMd);

            #endregion

            #region 采购申请汇总单
            objectsMd =
                (SAPbobsCOM.UserObjectsMD)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);

            haveOBJ = objectsMd.GetByKey("OPRQS");

            #region 设置主表属性

            objectsMd.Code = "OPRQS"; //对象代码
            objectsMd.Name = "开发_采购申请汇总单"; //对象名称
            objectsMd.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document; //绑定数据类型
            objectsMd.TableName = "OPRQS"; //表名
            objectsMd.CanFind = SAPbobsCOM.BoYesNoEnum.tYES; //是否可查找
            objectsMd.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO;
            objectsMd.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.CanClose = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES; //创建默认form
            objectsMd.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO;
            objectsMd.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.ManageSeries = BoYesNoEnum.tYES;
            objectsMd.MenuItem = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.MenuCaption = "开发_采购申请汇总单";
            objectsMd.MenuUID = "OPRQS";
            objectsMd.FatherMenuID = 2304;
            objectsMd.Position = 0;
            objectsMd.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
            fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + objectsMd.Code + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader oReader3 = new StreamReader(fs, System.Text.Encoding.UTF8);

            objectsMd.FormSRF = oReader3.ReadToEnd();
            oReader3.Close();
            oReader3.Dispose();
            fs.Close();
            fs.Dispose();

            #endregion

            #region 设置子表

            AddTableFields.AddChildTable(objectsMd.ChildTables, "PRQ1S");
            AddTableFields.AddChildTable(objectsMd.ChildTables, "PRQ2S");

            #endregion

            #region 设置查找字段

            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "Remark", "备注");
            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "U_DocDate", "日期");
            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "U_OPRQDoc", "申请单号");


            #endregion

            #region 设置表格默认字段

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_ItemCode", "物料编码", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_ItemName", "物料描述", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Speic", "规格", BoYesNoEnum.tNO, 1);

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Factory", "产地", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_unitMsr", "单位", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_CntcPrsn", "联系人", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_IsCSale", "控销", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_OPRQDoc", "申请单号", BoYesNoEnum.tNO, 2);

            #endregion

            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_ItemCode", "物料编码", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_ItemName", "物料描述", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_Speic", "规格", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_Factory", "产地", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_unitMsr", "单位", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_CntcPrsn", "联系人", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_IsCSale", "控销", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_OPRQDoc", "申请单号", BoYesNoEnum.tNO,
                BoYesNoEnum.tYES, 2);

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "DocEntry", "DocEntry", BoYesNoEnum.tNO, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "Remark", "备注", BoYesNoEnum.tNO, 0);

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_DocDate", "日期", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_OPRQDoc", "申请单号.", BoYesNoEnum.tYES, 0);

            if (!haveOBJ)
            {
                objectsMd.Add();
            }
            else
            {
                objectsMd.Update();
            }

            err1 = Program.oCompany.GetLastErrorDescription();

            AddTableFields.ReleaseCom(objectsMd);


            #endregion


            #region 门店发货计划
            objectsMd =
                (SAPbobsCOM.UserObjectsMD)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);

            haveOBJ = objectsMd.GetByKey("DELIVEBPL");

            objectsMd.Code = "DELIVEBPL"; //对象代码
            objectsMd.Name = "开发_门店发货计划单"; //对象名称
            objectsMd.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document; //绑定数据类型
            objectsMd.TableName = "DELIVEBPL"; //表名
            objectsMd.CanFind = SAPbobsCOM.BoYesNoEnum.tYES; //是否可查找
            objectsMd.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO;
            objectsMd.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.CanClose = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES; //创建默认form
            objectsMd.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO;
            objectsMd.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.ManageSeries = BoYesNoEnum.tYES;
            objectsMd.MenuItem = SAPbobsCOM.BoYesNoEnum.tYES;
            objectsMd.MenuCaption = "开发_门店发货计划单";
            objectsMd.MenuUID = "DELIVEBPL";
            objectsMd.FatherMenuID = 2048;
            objectsMd.Position = 0;
            objectsMd.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
            fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + objectsMd.Code + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            oReader3 = new StreamReader(fs, System.Text.Encoding.UTF8);

            objectsMd.FormSRF = oReader3.ReadToEnd();
            oReader3.Close();
            oReader3.Dispose();
            fs.Close();
            fs.Dispose();

            #region 设置子表

            AddTableFields.AddChildTable(objectsMd.ChildTables, "LIVEBPL1");

            #endregion

            #region 设置查找字段

            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "Remark", "备注");
            AddTableFields.AddCanFindColumns(objectsMd.FindColumns, "U_BPLName", "分支名称");

            #endregion

            #region 设置表格默认字段

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_ItemCode", "物料编码", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_ItemName", "物料描述", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Speic", "规格", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Factory", "产地", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_AppNo", "批准文号", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_unitMsr", "单位", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_ComQty", "可发数量", BoYesNoEnum.tNO, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Quantity", "数量", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_DistNumber", "批号", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_ExpDate", "有效期至", BoYesNoEnum.tYES, 1);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_MnfDate", "生产日期", BoYesNoEnum.tYES, 1);
            #endregion

            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_ItemCode", "物料编码", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_ItemName", "物料描述", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_Speic", "规格", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_Factory", "产地", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_unitMsr", "单位", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_AppNo", "批准文号", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_ComQty", "可发数量", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_Quantity", "数量", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_DistNumber", "批号", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_ExpDate", "有效期至", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);
            AddTableFields.AddChildFormColumns(objectsMd.EnhancedFormColumns, "U_MnfDate", "生产日期", BoYesNoEnum.tYES,
                BoYesNoEnum.tYES, 1);

            AddTableFields.AddFormColumns(objectsMd.FormColumns, "DocEntry", "DocEntry", BoYesNoEnum.tNO, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_Address2", "发货地址", BoYesNoEnum.tNO, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "DocNum", "单号", BoYesNoEnum.tNO, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "Remark", "备注", BoYesNoEnum.tNO, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_DocDate", "单据日期", BoYesNoEnum.tYES, 0);
            AddTableFields.AddFormColumns(objectsMd.FormColumns, "U_DocDueDate", "发货日期.", BoYesNoEnum.tYES, 0);

            if (!haveOBJ)
            {
                objectsMd.Add();
            }
            else
            {
                objectsMd.Update();
            }

            err1 = Program.oCompany.GetLastErrorDescription();

            AddTableFields.ReleaseCom(objectsMd);


#endregion



        }

        public static void initField()
        {
            bool istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "OCRD", "PcCode", "拼音码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            istrue = Program.oCompany.InTransaction;
            Dictionary<string, string> iForm = new Dictionary<string, string>();
            iForm.Add("实佳", "实佳");
            iForm.Add("嘉益祥", "嘉益祥");
            AddTableFields.AddB1Fields(Program.oCompany, "OCRD", "Ifrom", "信息来源", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, iForm);
            AddTableFields.AddB1Fields(Program.oCompany, "OCRD", "SUPQUA", "供应商资质记录", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null,"LinkOUDO", "SUQUA",UDFLinkedSystemObjectTypesEnum.ulBanks);
            AddTableFields.AddB1Fields(Program.oCompany, "PDN1", "Speic", "规格", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 30, null);
            AddTableFields.AddB1Fields(Program.oCompany, "PDN1", "Factory", "产地", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "PDN1", "CntcPrsn", "联系人", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OJDT", "BusPlaceName", "关帐分支名称", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Table(Program.oCompany, "SUQTYPE", "资质证书信息", BoUTBTableType.bott_NoObject);
            AddTableFields.AddB1Table(Program.oCompany, "DELIVEBPL", "门店发货", BoUTBTableType.bott_Document);
            AddTableFields.AddB1Table(Program.oCompany, "LIVEBPL1", "门店发货行", BoUTBTableType.bott_DocumentLines);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Table(Program.oCompany, "PriceForBPLID", "加价类别表", BoUTBTableType.bott_NoObject);
            AddTableFields.AddB1Fields(Program.oCompany, "@PriceForBPLID", "PForBPLID", "加价比例", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Price , 10,null);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "PForBPLID", "加价类别", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null, "LinkTable", "PriceForBPLID",UDFLinkedSystemObjectTypesEnum.ulBanks );
            istrue = Program.oCompany.InTransaction;
            Dictionary<string, string> YesNo = new Dictionary<string, string>();
            YesNo.Clear();
            YesNo.Add("批发商", "批发商");
            YesNo.Add("生产商", "生产商");
            YesNo.Add("全部", "全部");
            AddTableFields.AddB1Fields(Program.oCompany, "@SUQTYPE", "CardGroup", "资质证书所属供应商", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, YesNo);
            YesNo.Clear();
            YesNo.Add("是", "是");
            YesNo.Add("否", "否");

            AddTableFields.AddB1Fields(Program.oCompany, "@SUQTYPE", "IsExp", "有效期管理", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "@SUQTYPE", "IsReq", "是否必须", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, YesNo);

            AddTableFields.AddB1Table(Program.oCompany, "SUQUA", "资质记录主表", BoUTBTableType.bott_MasterData);
            AddTableFields.AddB1Table(Program.oCompany, "UQUA1", "资质记录行表", BoUTBTableType.bott_MasterDataLines);

            AddTableFields.AddB1Fields(Program.oCompany, "@SUQUA", "CardGroup", "供应商类别", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, null);
            istrue = Program.oCompany.InTransaction;

            AddTableFields.AddB1Fields(Program.oCompany, "@UQUA1", "QuaName", "资质证书", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@UQUA1", "QuaDoc", "资质证书链接", BoFieldTypes.db_Memo,
                BoFldSubTypes.st_Link, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@UQUA1", "QuaExp", "资质证书到期日", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@UQUA1", "ManExp", "有效期管理", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "@UQUA1", "IsReq", "是否必须", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OCPR", "AccOf", "账期", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OCPR", "ContDate", "合同到期日", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OCPR", "PTotal", "铺底货金额", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Sum, 10, null);

            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "Speic", "规格", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "Factory", "产地", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "PcFactory", "产地拼音码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            Dictionary<string, string> InjeType = new Dictionary<string, string>();
            // 片剂、胶囊剂、颗粒剂、液体剂、丸剂、医疗器械、散剂、中药材、软膏剂、硬膏剂、气雾剂、缓释剂、栓剂、注射剂、输液剂、其他剂型
            InjeType.Add("片剂", "片剂");
            InjeType.Add("胶囊剂", "胶囊剂");
            InjeType.Add("颗粒剂", "颗粒剂");
            InjeType.Add("液体剂", "液体剂");
            InjeType.Add("丸剂", "丸剂");
            InjeType.Add("医疗器械", "医疗器械");
            InjeType.Add("散剂", "散剂");
            InjeType.Add("中药材", "中药材");
            InjeType.Add("软膏剂", "软膏剂");
            InjeType.Add("硬膏剂", "硬膏剂");
            InjeType.Add("气雾剂", "气雾剂");
            InjeType.Add("缓释剂", "缓释剂");
            InjeType.Add("栓剂", "栓剂");
            InjeType.Add("注射剂", "注射剂");
            InjeType.Add("输液剂", "输液剂");
            InjeType.Add("其他剂型", "其他剂型");

            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "InjeType", "剂型", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, InjeType);
            Dictionary<string, string> TypeA = new Dictionary<string, string>();
            TypeA.Add("药品", "药品");
            TypeA.Add("非药品", "非药品");
            TypeA.Add("中药材", "中药材");
            TypeA.Add("换购", "换购");
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "TypeA", "一级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, TypeA);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "TypeB", "二级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "TypeC", "三级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "TypeD", "四级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "Ifrom", "信息来源", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, iForm);
            Dictionary<string, string> PresClass = new Dictionary<string, string>();
            //处方用药、非处方用药、非药品、中药材
            PresClass.Add("处方用药", "处方用药");
            PresClass.Add("非处方用药", "非处方用药");
            PresClass.Add("非药品", "非药品");
            PresClass.Add("中药材", "中药材");

            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "PresClass", "处方分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, PresClass);
            Dictionary<string, string> MageCate = new Dictionary<string, string>();
            //（L）一级、（K）二级、（T）停售、(D）订货
            MageCate.Add("一级", "一级");
            MageCate.Add("二级", "二级");
            MageCate.Add("停售", "停售");
            MageCate.Add("订货", "订货");

            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "MageCate", "管理类别", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, MageCate);

            Dictionary<string, string> PriceType = new Dictionary<string, string>();
            //高/中/低
            PriceType.Add("-", "-");
            PriceType.Add("高", "高");
            PriceType.Add("中", "中");
            PriceType.Add("低", "低");

            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "PriceType", "价格带", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, PriceType);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "CardCodeA", "一级供应商编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "CardNameA", "一级供应商名称", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "CardCodeB", "二级供应商编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "CardNameB", "二级供应商名称", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "BetDate", "保质期(月)", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            YesNo.Clear();
            YesNo.Add("是", "是");
            YesNo.Add("非医保支付", "非医保支付");
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "MedPay", "医保支付", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            YesNo.Clear();
            YesNo.Add("是", "是");
            YesNo.Add("否", "否");
            AddTableFields.AddB1Fields(Program.oCompany, "OCRD", "IsFPay", "是否预付款", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "oldItemCode", "旧物料编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "AppNo", "批准文号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "PcName", "名称拼音码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "inleted", "是否进口药品", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsFistSo", "是否首营", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsUnbund", "是否拆零", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsTest", "是否试销", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsMHJ", "是否麻黄碱", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "MHJQty", "麻黄碱控制数量", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "CntcPrsn", "联系人", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsCodeL", "基本商品线", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsCSale", "是否维价", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "RetailPrice", "批导零售价", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Price, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "PurPrice", "批导采购价", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Price, 10, null);
            istrue = Program.oCompany.InTransaction;
            Dictionary<string,string> stCon = new Dictionary<string, string>();
            stCon.Add("1","常温"); stCon.Add("2", "阴凉"); stCon.Add("3", "冷藏"); stCon.Add("4", "含麻专区");
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "StCon", "存储条件", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, stCon);
            istrue = Program.oCompany.InTransaction;
            Dictionary<string, string> ConCat = new Dictionary<string, string>();
            ConCat.Add("1", "一般养护"); ConCat.Add("2", "重点养护"); ConCat.Add("3", "不养护");
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "ConCat", "养护类别", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, ConCat);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsCold", "是否冷藏", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsRegDrugs", "是否监管药品", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsToWMS", "是否传入WMS", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OITM", "IsStock", "是否备货", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            istrue = Program.oCompany.InTransaction;

            AddTableFields.AddB1Fields(Program.oCompany, "OITW", "IsPlan", "参与要货计划", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);

            AddTableFields.AddB1Fields(Program.oCompany, "OPDN", "Is_Inv", "已开票", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OCRD", "IsOneInSk", "多次入库", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "ORPD", "IsOK", "门店审核", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            YesNo.Clear();
            YesNo.Add("报废","报废");YesNo.Add("报损","报损");
            AddTableFields.AddB1Fields(Program.oCompany, "OIGE", "WMSType", "WMS类型", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);

            AddTableFields.AddB1Table(Program.oCompany, "TYPESET", "分类设置", BoUTBTableType.bott_NoObject);

            AddTableFields.AddB1Fields(Program.oCompany, "@TYPESET", "TypeA", "一级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TYPESET", "TypeB", "二级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TYPESET", "TypeC", "三级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TYPESET", "TypeD", "四级分类", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);


            AddTableFields.AddB1Table(Program.oCompany, "TOPDN", "采购收货_开发", BoUTBTableType.bott_Document);
            AddTableFields.AddB1Table(Program.oCompany, "TPDN1", "采购收货_开发_行", BoUTBTableType.bott_DocumentLines);

            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "CardCode", "供应商", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "CardName", "名称", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "DocDate", "收货日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "BPLID", "分支", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "UserSign", "制单人", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "UserSign2", "修改人", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "DocTotal", "不含税金额", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Sum, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "Gtotal", "含税金额", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Sum, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "Transid", "采购到货单号", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TOPDN", "WmsDocNum", "WMS单号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "BaseDoc", "基本单据", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "BaseLine", "基本单据行", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Baseobj", "基本单据类型", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "ItemCode", "商品编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Dscription", "商品描述", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Speic", "规格", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Factory", "生产厂家", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "unit1", "单位", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "whsCode", "仓库", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 30, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Quantity", "数量", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Quantity, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Price", "不含税单价", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Price, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "LineTotal", "不含税金额", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Sum, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "VatGroup", "税码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "VatPrcnt", "税率", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Percentage, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "PriceAfVat", "含税价", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Price, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "Gtotal", "含税金额", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Sum, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "DistNumber", "批次号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "ExpDate", "失效日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "MnfDate", "生产日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@TPDN1", "BetDate", "保质期限", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Table(Program.oCompany, "OPRQS", "采购申请汇总表", BoUTBTableType.bott_Document);
            AddTableFields.AddB1Table(Program.oCompany, "PRQ1S", "采购申请汇总表行", BoUTBTableType.bott_DocumentLines);
            AddTableFields.AddB1Table(Program.oCompany, "PRQ2S", "采购申请单号", BoUTBTableType.bott_DocumentLines);
            AddTableFields.AddB1Fields(Program.oCompany, "@OPRQS", "OPRQDoc", "采购申请单号", BoFieldTypes.db_Memo,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@OPRQS", "DocDate", "申请汇总日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ2S", "OPRQDoc", "采购申请单号", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "ItemCode", "物料编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "ItemName", "物料描述", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 150, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "Speic", "规格", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "Factory", "产地", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 150, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "unitMsr", "单位", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "Quantity", "数量", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Quantity, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "CntcPrsn", "联系人", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 20, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@PRQ1S", "IsCSale", "维价", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 10, YesNo);
            AddTableFields.AddB1Fields(Program.oCompany, "OPOR", "WMSDocNum", "传入WMS单号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            istrue = Program.oCompany.InTransaction;

            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "DocDate", "单据日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "DocDueDate", "发货日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 100, null);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "CardCode", "客户编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "CardName", "客户名称", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "BPLId", "分支编码", BoFieldTypes.db_Numeric,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "BPLName", "分支名称", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "Address2", "收货地址", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@DELIVEBPL", "WMSDocNum", "WMS单号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            istrue = Program.oCompany.InTransaction;
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "ItemCode", "物料编码", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "ItemName", "物料描述", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "Speic", "规格", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "AppNo", "批准文号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "Factory", "产地", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "unitMsr", "单位", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "Quantity", "数量", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Quantity, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "DistNumber", "批号", BoFieldTypes.db_Alpha,
                BoFldSubTypes.st_None, 100, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "MnfDate", "生产日期", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "ExpDate", "有效期至", BoFieldTypes.db_Date,
                BoFldSubTypes.st_None, 10, null);
            AddTableFields.AddB1Fields(Program.oCompany, "@LIVEBPL1", "ComQty", "可发数量", BoFieldTypes.db_Float,
                BoFldSubTypes.st_Quantity, 10, null);

        }

        public static void Ins_Data()
        {
            try
            {
                int i = RowInt;
                // oMatrix.SelectRow(i,true,false);
                string BaseDoc = TOPDN.TPDN1.GetValue("U_BaseDoc", RowInt - 1);
                if (BaseDoc.Equals(""))
                {
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("请在有数据的行上点击新增行,空行不允许新增!");
                    return;
                }

                TOPDN.TPDN1.Offset = RowInt - 1;
                TOPDN.TPDN1.InsertRecord(RowInt);
                TOPDN.TPDN1.SetValue("U_BaseDoc", i, TOPDN.TPDN1.GetValue("U_BaseDoc", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_BaseLine", i, TOPDN.TPDN1.GetValue("U_BaseLine", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_Baseobj", i, TOPDN.TPDN1.GetValue("U_Baseobj", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_ItemCode", i, TOPDN.TPDN1.GetValue("U_ItemCode", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_Dscription", i, TOPDN.TPDN1.GetValue("U_Dscription", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_Speic", i, TOPDN.TPDN1.GetValue("U_Speic", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_Factory", i, TOPDN.TPDN1.GetValue("U_Factory", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_unit1", i, TOPDN.TPDN1.GetValue("U_unit1", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_whsCode", i, TOPDN.TPDN1.GetValue("U_whsCode", RowInt - 1));
                decimal Qty = 0;
                TOPDN.TPDN1.SetValue("U_Quantity", i, Qty.ToString());
                decimal priceValue = Convert.ToDecimal(TOPDN.TPDN1.GetValue("U_Price", RowInt - 1));

                TOPDN.TPDN1.SetValue("U_Price", i, priceValue.ToString());

                TOPDN.TPDN1.SetValue("U_LineTotal", i, (Qty * priceValue).ToString());
                TOPDN.TPDN1.SetValue("U_VatGroup", i, TOPDN.TPDN1.GetValue("U_VatGroup", RowInt - 1));
                TOPDN.TPDN1.SetValue("U_VatPrcnt", i, TOPDN.TPDN1.GetValue("U_VatPrcnt", RowInt - 1));
                decimal priceAFValue = Convert.ToDecimal(TOPDN.TPDN1.GetValue("U_PriceAfVat", RowInt - 1));

                TOPDN.TPDN1.SetValue("U_PriceAfVat", i, priceAFValue.ToString());
                TOPDN.TPDN1.SetValue("U_Gtotal", i, (Qty * priceAFValue).ToString());
                TOPDN.TPDN1.SetValue("U_DistNumber", i, "");
                //TOPDN.TPDN1.SetValue("U_ExpDate", i, Grid0.DataTable.GetValue("基本单号", i).ToString());
                //TOPDN.TPDN1.SetValue("U_MnfDate", i, Grid0.DataTable.GetValue("基本单号", i).ToString());
                TOPDN.TPDN1.SetValue("U_BetDate", i, TOPDN.TPDN1.GetValue("U_BetDate", RowInt - 1));
                for (int j = 0; j < TOPDN.TPDN1.Size; j++)
                {
                    TOPDN.TPDN1.SetValue("VisOrder", j, (j + 1).ToString());
                }

                oMatrix.LoadFromDataSource();
                oMatrix.SetCellFocus(i + 1, 9);
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }

        }

        public static void Del_Row()
        {
            try
            {
                TOPDN.TPDN1.RemoveRecord(RowInt-1);
               ((SAPbouiCOM.Matrix)SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Items.Item("0_U_G").Specific).LoadFromDataSourceEx(false);
               SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm.Mode = BoFormMode.fm_UPDATE_MODE;
            }
            catch (Exception e)
            {
                
            }
        }

        public static int GetCntPrsn(string CardCode, string Names)
        {
            string sqlCmd = "SELECT CntctCode FROM OCPR WHERE CardCode='" + CardCode + "' AND Name+Cellolar='" + Names +
                            "'";
            SAPbobsCOM.Recordset oRec =
                Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery(sqlCmd);
            if (oRec.RecordCount > 0)
            {
                return int.Parse(oRec.Fields.Item(0).Value.ToString());
            }
            else
            {
                return 0;
            }
        }

        public static void AddOPOR(string[] CardCodeList, Dictionary<int, string> oshpList, SAPbouiCOM.Grid Grid0)
        {
            MsgShow oShow = new MsgShow();
            try
            {
                Program.oCompany.StartTransaction();
                
                oShow.Show();
                for (int i = 0; i < CardCodeList.Length; i++)
                {
                    string[] CardCode = CardCodeList[i].ToString().Split(',');
                    if (!CardCode[0].Equals(""))
                    {
                        for (int j = 0; j < oshpList.Count; j++)
                        {
                            int LineCount = 0;
                            SAPbobsCOM.Documents oDoc =
                                Program.oCompany.GetBusinessObject(BoObjectTypes.oPurchaseOrders) as
                                    SAPbobsCOM.Documents;
                            string oshp = oshpList[j].ToString();
                            oDoc.CardCode = CardCode[0];
                            oDoc.DocDate = DateTime.Now;
                            oDoc.DocDueDate = DateTime.Now.AddDays(5);
                            oDoc.Comments = CardCode[1].ToString() + "Test";
                            oDoc.ContactPersonCode = GetCntPrsn(CardCode[0], CardCode[1]);
                            oDoc.BPL_IDAssignedToInvoice = GetBPLID(oshp.Substring(0, oshp.IndexOf("_")));

                            
                            for (int k = 0; k < Grid0.Rows.SelectedRows.Count; k++)
                            {
                               int oRows = Grid0.Rows.SelectedRows.Item(k, BoOrderType.ot_RowOrder);
                                float Quantity = float.Parse(Grid0.DataTable.GetValue(oshp, oRows).ToString());
                                string CardCodeDetail = Grid0.DataTable.GetValue("CardCode", oRows).ToString();
                                string PerOrder = Grid0.DataTable.GetValue("U_PerOrder", oRows).ToString();
                                string cntPrsn = PerOrder.Equals("是")
                                    ? Grid0.DataTable.GetValue("U_CntcPrsn", oRows).ToString()
                                    : "";
                                if (Quantity != 0 && CardCode[0].Equals(CardCodeDetail) && CardCode[1].Equals(cntPrsn))
                                {
                                    //string oshpList00 = oshpList[j];
                                    oDoc.Lines.ItemCode = Grid0.DataTable.GetValue("ItemCode", oRows).ToString();
                                    oDoc.Lines.Quantity = Quantity;
                                    oDoc.Lines.PriceAfterVAT =
                                        double.Parse(Grid0.DataTable.GetValue("Price", oRows).ToString());
                                    oDoc.Lines.Add();
                                    LineCount++;

                                }
                            }

                            if (LineCount == 0)
                            {
                                continue;
                            }

                            if (oDoc.Add() == 0)
                            {
                                string DocEntry = Program.oCompany.GetNewObjectKey();
                                SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText(
                                    "生成采购订单成功,单号:" + DocEntry, BoMessageTime.bmt_Medium,
                                    BoStatusBarMessageType.smt_Success);
                            }
                            else
                            {
                                oShow.UIAPIRawForm.Close();
                                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(
                                    "生成采购订单失败\r\n" + Program.oCompany.GetLastErrorDescription());
                                if (Program.oCompany.InTransaction)
                                {
                                    Program.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                                }
                                return;
                            
                            }


                        }


                    }

                }

                if (Program.oCompany.InTransaction)
                {
                    oShow.UIAPIRawForm.Close();
                    Program.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("订单生成成功！");
                    
                }

            }
            catch (Exception e)
            {
                oShow.UIAPIRawForm.Close();
                if (Program.oCompany.InTransaction) { Program.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);}
                
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
               
            }
        }

    public static int GetBPLID(string BPLName)
        {
            string sqlCmd = "SELECT T1.BPLId FROM OBPL T1 WHERE T1.BPLName='" + BPLName + "'";
            SAPbobsCOM.Recordset oRec =
                Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery(sqlCmd);
            if (oRec.RecordCount > 0)
            {
                return int.Parse(oRec.Fields.Item(0).Value.ToString());
            }
            else
            {
                return 0;
            }

        }
    }
}

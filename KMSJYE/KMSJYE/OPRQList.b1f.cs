﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;
using Application = SAPbouiCOM.Application;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.OPRQList", "OPRQList.b1f")]
    class OPRQList : UserFormBase
    {
        public OPRQList()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Grid Grid0;

        private void OnCustomInitialize()
        {

            string sqlCmd = "SELECT DocEntry,DocDate,Requester,ReqName,t2.Name DepName ,T1.BPLName FROM OPRQ T1 INNER JOIN OUDP T2 ON T1.Department=T2.Code WHERE DocStatus='O' and DocEntry not in(select U_OPRQDoc from [@PRQ2S] where U_OPRQDoc is not null )";
            SAPbouiCOM.DataTable oDt = this.UIAPIRawForm.DataSources.DataTables.Item("CxData");
            oDt.ExecuteQuery(sqlCmd);
            Grid0.DataTable = oDt;
            Grid0.SelectionMode = BoMatrixSelect.ms_Auto;
            Grid0.RowHeaders.Width = 15;
            Grid0.Item.Enabled = false;
            Grid0.Columns.Item("DocEntry").TitleObject.Caption = "采购申请内部单号";
            Grid0.Columns.Item("DocDate").TitleObject.Caption = "申请日期";
            Grid0.Columns.Item("Requester").TitleObject.Caption = "申请者";
            Grid0.Columns.Item("ReqName").TitleObject.Caption = "申请人姓名";
            Grid0.Columns.Item("DepName").TitleObject.Caption = "部门";
            Grid0.Columns.Item("BPLName").TitleObject.Caption = "分支";
           
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        private void Grid0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(pVal.Row))
            {
                Grid0.Rows.SelectedRows.Remove(pVal.Row);
            }
            else
            {
                Grid0.Rows.SelectedRows.Add(pVal.Row);
            }

        }

        private Button Button2;

        private void Button2_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.IsSelected(0))
            {
                Grid0.Rows.SelectedRows.Clear();
            }
            else
            {
                Grid0.Rows.SelectedRows.AddRange(0, Grid0.Rows.Count);
            }

        }

        private void Button1_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();

        }

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (Grid0.Rows.SelectedRows.Count==0)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("未选数据!");
                
            }
            else
            {
                string DocString = "";
                for (int i = 0; i < Grid0.Rows.SelectedRows.Count; i++)
                {
                    DocString += ',' + Grid0.DataTable.GetValue("DocEntry",Grid0.Rows.SelectedRows.Item(i,BoOrderType.ot_RowOrder)).ToString();
                }

                OPRQS.OPRQSdb.SetValue("U_OPRQDoc",0,DocString.Substring(1));
                this.UIAPIRawForm.Close();
            }

        }
    }
}

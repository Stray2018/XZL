﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.OPORCal", "OPORCal.b1f")]
    class OPORCal : UserFormBase
    {
        public OPORCal()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_1").Specific));
            this.Grid0.GotFocusAfter += new SAPbouiCOM._IGridEvents_GotFocusAfterEventHandler(this.Grid0_GotFocusAfter);
            this.Grid0.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.Grid0_ClickAfter);
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_2").Specific));
            this.ComboBox0.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox0_ComboSelectAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_4").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            this.LinkedButton0 = ((SAPbouiCOM.LinkedButton)(this.GetItem("Item_7").Specific));
            this.ButtonCombo0 = ((SAPbouiCOM.ButtonCombo)(this.GetItem("Item_8").Specific));
            this.ButtonCombo0.ComboSelectBefore += new SAPbouiCOM._IButtonComboEvents_ComboSelectBeforeEventHandler(this.ButtonCombo0_ComboSelectBefore);
            this.ButtonCombo0.ComboSelectAfter += new SAPbouiCOM._IButtonComboEvents_ComboSelectAfterEventHandler(this.ButtonCombo0_ComboSelectAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.StaticText StaticText0;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 -
                                     this.UIAPIRawForm.Width / 2;
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            string sqlCmd = "SELECT Convert(varchar(10), T1.U_DocDate,121) DocDate ,T1.Remark,T1.DocEntry FROM [@OPRQS] T1 WHERE T1.Status='O'";
           oRec.DoQuery(sqlCmd);
            if (oRec.RecordCount >0)
            {
                for (int i = 0; i < oRec.RecordCount; i++)
                {
                    string DocEntry = oRec.Fields.Item("DocEntry").Value.ToString();
                    string DocDate = oRec.Fields.Item("DocDate").Value.ToString();
                    string Remark = oRec.Fields.Item("Remark").Value.ToString();
                    ComboBox0.ValidValues.Add(DocEntry, Remark == "" ? DocDate : "日期:" + DocDate + "备注:" + Remark);
                    oRec.MoveNext();
                }
                
            }
            this.UIAPIRawForm.EnableMenu("1281",false);
            this.UIAPIRawForm.EnableMenu("1282", false);
            ButtonCombo0.Caption = "生成采购订单";
            ButtonCombo0.ValidValues.Add("110", "生成当前行店铺采购订单");
            ButtonCombo0.ValidValues.Add("111", "生成所有店铺采购订单");
            ButtonCombo0.ExpandType = BoExpandType.et_DescriptionOnly;

        }

        private SAPbouiCOM.Grid Grid0;
        private SAPbouiCOM.ComboBox ComboBox0;
        private SAPbouiCOM.Button Button0;

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
          this.UIAPIRawForm.Close();

        }

        private void ComboBox0_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                this.UIAPIRawForm.Freeze(true);
            string CFLDoc = ComboBox0.Selected.Value;
            ButtonCombo0.Item.Enabled = true;
            string sqlCmd = "DECLARE @sqlCmd VARCHAR(max)='',@Fields1 VARCHAR(max) = '',@Fields2 VARCHAR(max) = '',@Fields3 VARCHAR(max) = '';" +
            " SELECT @Fields1 = @Fields1 + ',[' + T2.ReqName + '_申请数]', @Fields2 = @Fields2 + ',[' + T2.ReqName + '_订购数]'," +
               "@Fields3 = @Fields3 + ',case when orderID=1 then sum(['+T2.ReqName+'_申请数]) else 0 end as '+T2.ReqName+'_申请数,case when orderID=1 then sum(['+T2.ReqName+'_订购数]) else 0 end as '+T2.ReqName+'_订购数," +
                            "sum(['+T2.ReqName+'_订购数]) as '+T2.ReqName+'POQty,sum(['+T2.ReqName+'_申请数]) as '+T2.ReqName+'PUQty' FROM[@PRQ2S] T1 INNER JOIN OPRQ T2 ON T1.U_OPRQDoc = T2.DocEntry AND T1.U_OPRQDoc IS NOT NULL and T1.DocEntry =" + CFLDoc + " GROUP BY T2.ReqName ;" +
            " SELECT @Fields1 = SUBSTRING(@Fields1, 2, LEN(@Fields1) - 1), @Fields2 = SUBSTRING(@Fields2, 2, LEN(@Fields2) - 1),@Fields3 = SUBSTRING(@Fields3,2,LEN(@Fields3)-1);" +
            " SET @sqlCmd = '"+
            " select case when orderID = 1 then '''+'Y'+''' else '''+'N'+''' end as S0,convert(int,orderID) orderID,CardCode,CardName,U_PerOrder,U_CntcPrsn,CGDocEntry,DocDate,ItemCode,Dscription,U_Speic,U_Factory,unitMsr,PQTReqQty,Quantity,PriceAfVAT Price,FreeTxt,'+@Fields3+' from(" +
            "    select * from("+
            "    select * from(select U.*, U2.Quantity orderQty, U2.Quantity as cgQty, U3.ReqName + '''+'_订购数'+''' cgBPL, U3.ReqName + '''+'_申请数'+''' orderBPL from(SELECT ROW_NUMBER()OVER(PARTITION BY T2.ItemCode ORDER BY T2.PriceAfVAT, T1.CardCode) orderID," +
            " t1.CardCode, t1.CardName,T3.U_PerOrder,T2.U_CntcPrsn, t1.NumAtCard CGDocEntry, CONVERT(VARCHAR(10), T1.DocDate, 121) DocDate, T2.ItemCode, t2.Dscription, T2.U_Speic, t2.U_Factory," +
            " t2.unitMsr, t2.Quantity PQTReqQty, t2.Quantity, t2.PriceAfVAT, T2.FreeTxt FROM OPQT T1" +
            " INNER JOIN PQT1 T2 ON T2.DocEntry = T1.DocEntry AND T1.NumAtCard = "+ CFLDoc + " "+
            " inner join ocrd T3 on t1.CardCode=T3.CardCode  ) U inner join[@PRQ2S] U0 on U.CGDocEntry = U0.DocEntry "+
            " INNER JOIN PRQ1 U2 ON U0.U_OPRQDoc = U2.DocEntry AND U0.U_OPRQDoc IS NOT NULL AND U2.ItemCode = U.ItemCode"+
            " INNER JOIN OPRQ U3 ON U3.DocEntry = U2.DocEntry) as P "+
             "   )M Pivot(sum(orderQty) for M.orderBPL in('+@Fields1+')) as N) as O Pivot(sum(cgQty) for O.cgBPL in ('+@Fields2+')) Q group by " +
                            "orderID,CardCode,CardName,U_PerOrder,U_CntcPrsn,CGDocEntry,DocDate,ItemCode,Dscription,U_Speic,U_Factory,unitMsr,PQTReqQty,Quantity,PriceAfVAT,FreeTxt order by ItemCode,orderID,CardCode; ';" +
            " EXEC(@sqlCmd); ";
            Grid0.DataTable.ExecuteQuery(sqlCmd);
            Grid0.RowHeaders.Width = 20;
           
            Grid0.Columns.Item("S0").TitleObject.Caption = "选择";
            Grid0.Columns.Item("S0").Editable = true;
            Grid0.Columns.Item("S0").TitleObject.Sortable = true;
            Grid0.Columns.Item("S0").Type = BoGridColumnType.gct_CheckBox;
            Grid0.Columns.Item("orderID").TitleObject.Caption = "价格排序";
            Grid0.Columns.Item("orderID").Editable = false;
            Grid0.Columns.Item("CardCode").TitleObject.Caption = "供应商编码";
            Grid0.Columns.Item("CardCode").Type = BoGridColumnType.gct_EditText;
            SAPbouiCOM.EditTextColumn oCardCode = (SAPbouiCOM.EditTextColumn)Grid0.Columns.Item("CardCode");
            oCardCode.LinkedObjectType ="2";

            Grid0.Columns.Item("CardCode").Editable = false;
            Grid0.Columns.Item("CardName").TitleObject.Caption = "供应商名称";
            Grid0.Columns.Item("CardName").Editable = false;
            Grid0.Columns.Item("U_CntcPrsn").TitleObject.Caption = "联系人";
            Grid0.Columns.Item("U_CntcPrsn").Editable = false;
            Grid0.Columns.Item("U_PerOrder").TitleObject.Caption = "按联系人生成订单";
            Grid0.Columns.Item("U_PerOrder").Editable = false;
            Grid0.Columns.Item("CGDocEntry").TitleObject.Caption = "采购申请汇总单号";
            Grid0.Columns.Item("CGDocEntry").Editable = false;
            Grid0.Columns.Item("CGDocEntry").Type = BoGridColumnType.gct_EditText;
            SAPbouiCOM.EditTextColumn cgColumn = (SAPbouiCOM.EditTextColumn)Grid0.Columns.Item("CGDocEntry");
            Grid0.Columns.Item("CGDocEntry").Visible = false;
            cgColumn.LinkedObjectType = "OPRQS";
            Grid0.Columns.Item("DocDate").TitleObject.Caption = "报价日期";
            Grid0.Columns.Item("DocDate").Editable = false;
            Grid0.Columns.Item("DocDate").Visible = false;
            Grid0.Columns.Item("ItemCode").TitleObject.Caption = "物料编号";
            Grid0.Columns.Item("ItemCode").Type = BoGridColumnType.gct_EditText;
            SAPbouiCOM.EditTextColumn oItemCode = (SAPbouiCOM.EditTextColumn)Grid0.Columns.Item("ItemCode");
            oItemCode.LinkedObjectType = "4";
            Grid0.Columns.Item("ItemCode").Editable = false;
            Grid0.Columns.Item("Dscription").TitleObject.Caption = "物料描述";
            Grid0.Columns.Item("Dscription").Editable = false;
            Grid0.Columns.Item("U_Speic").TitleObject.Caption = "规格";
            Grid0.Columns.Item("U_Speic").Editable = false;
            Grid0.Columns.Item("U_Factory").TitleObject.Caption = "生产厂家";
            Grid0.Columns.Item("U_Factory").Editable = false;
            Grid0.Columns.Item("unitMsr").TitleObject.Caption = "单位";
            Grid0.Columns.Item("unitMsr").Editable = false;
            Grid0.Columns.Item("PQTReqQty").TitleObject.Caption = "报价数量";
            Grid0.Columns.Item("PQTReqQty").Editable = false;
            Grid0.Columns.Item("Quantity").TitleObject.Caption = "申请数量";
            Grid0.Columns.Item("Quantity").Editable = false;
            Grid0.Columns.Item("Price").TitleObject.Caption = "含税价";
            Grid0.Columns.Item("Price").Editable = false;
            Grid0.Columns.Item("FreeTxt").TitleObject.Caption = "报价行备注";
            Grid0.Columns.Item("FreeTxt").Editable = false;
            Grid0.Columns.Item("orderID").TitleObject.Caption = "排序";
            Grid0.Columns.Item("orderID").TitleObject.Sortable = true;
            for (int i = 0; i < Grid0.Columns.Count; i++)
            {
                string ColumnsDescr = Grid0.Columns.Item(i).TitleObject.Caption;
                if (ColumnsDescr.Contains("POQty") || ColumnsDescr.Contains("PUQty"))
                {
                    Grid0.Columns.Item(i).Visible = false;
                }
                if (ColumnsDescr.Contains("_申请数"))
                {
                    Grid0.Columns.Item(i).Editable = false;
                    GridColumn oGridColumn = Grid0.Columns.Item(i);
                    oGridColumn.Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                    SAPbouiCOM.EditTextColumn oEditGC = (SAPbouiCOM.EditTextColumn)oGridColumn;
                    SAPbouiCOM.BoColumnSumType oST = oEditGC.ColumnSetting.SumType;
                    oEditGC.ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto;
                }
                if (ColumnsDescr.Contains("_订购数"))
                {
                    GridColumn oGridColumn = Grid0.Columns.Item(i);
                    oGridColumn.Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                    SAPbouiCOM.EditTextColumn oEditGC = (SAPbouiCOM.EditTextColumn)oGridColumn;
                    SAPbouiCOM.BoColumnSumType oST = oEditGC.ColumnSetting.SumType;
                    oEditGC.ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto;
                }
            }

            for (int i = 0; i < Grid0.Rows.Count; i++)
            {
                string orderID = Grid0.DataTable.GetValue("orderID", i).ToString();
               
                if (orderID.Equals("1"))
                {
                    try
                    {
                       Grid0.CommonSetting.SetRowBackColor(i + 1,Color.Aqua.ToArgb());
                    }
                    catch (Exception e)
                    {
                        this.UIAPIRawForm.Freeze(false);
                    }
                   
                }
                Grid0.RowHeaders.SetText(i,(i+1).ToString());
            }
           
            this.UIAPIRawForm.State = BoFormStateEnum.fs_Maximized;
            Grid0.RowHeaders.TitleObject.Caption = "行号";
            Grid0.AutoResizeColumns();
            Grid0.CommonSetting.FixedColumnsCount = 12;
            EditText0.Item.Enabled = true;
            EditText0.Value = ComboBox0.Selected.Value;
            ComboBox0.Active = true;
            EditText0.Item.Enabled = false;
            this.UIAPIRawForm.Freeze(false);
            }
            catch (Exception e)
            {
                this.UIAPIRawForm.Freeze(false);
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.ToString());
            }
        }

        private void Grid0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            if (pVal.ColUID == "S0")
            {
                string checkS0 = Grid0.DataTable.GetValue(pVal.ColUID, pVal.Row).ToString();
                if (checkS0.Equals("Y"))
                {
                  
                    Grid0.CommonSetting.SetRowBackColor(pVal.Row+1,Color.Aqua.ToArgb());
                }
                else
                {
                    Grid0.CommonSetting.SetRowBackColor(pVal.Row+1, -1);
                }

            }

        }

        private StaticText StaticText1;
        private EditText EditText0;
        private LinkedButton LinkedButton0;
        private ButtonCombo ButtonCombo0;
        private string CurrOSHP = String.Empty;

        private void ButtonCombo0_ComboSelectAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                string BuCValue = ButtonCombo0.Selected.Value;
            if (BuCValue.Equals("110") && (CurrOSHP.Equals(string.Empty) || !CurrOSHP.Contains("_订购数")))
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("激活单元格不在店铺列中,请在店铺列中单击任意单元格后点击生成采购订单!");
                ButtonCombo0.Caption = "生成采购订单";
                return; 
            }
            this.UIAPIRawForm.Freeze(true);
            ButtonCombo0.Caption = "生成采购订单";
            Dictionary<int,string> oDictionary  = new Dictionary<int, string>();
            string[] CardCodeList0 = new string[Grid0.Rows.Count] ;
            for (int i = 0; i < Grid0.Rows.Count; i++)
            {
                CardCodeList0.SetValue(string.Empty, i);
                string S0 = Grid0.DataTable.GetValue("S0", i).ToString();
                if (S0.Equals("Y"))
                {
                    string CardCode = Grid0.DataTable.GetValue("CardCode", i).ToString();
                    string CntcPrsn = Grid0.DataTable.GetValue("U_CntcPrsn", i).ToString();
                        string PerOrder = Grid0.DataTable.GetValue("U_PerOrder", i).ToString();
                    if (PerOrder.Equals("是"))
                    {
                        CardCodeList0.SetValue(CardCode + "," + CntcPrsn, i);
                    }
                    else
                    {
                        CardCodeList0.SetValue(CardCode + "," + "", i);
                    }
                    Grid0.Rows.SelectedRows.Add(i);
                }
            }

            if (BuCValue.Equals("110"))
            {
                oDictionary.Add(0,CurrOSHP);
            }
            else
            {
                int dictKey = 0;
                for (int i = 0; i < Grid0.Columns.Count; i++)
                {
                    string ColName = Grid0.Columns.Item(i).TitleObject.Caption;
                    if (ColName.Contains("_订购数"))
                    {
                        oDictionary.Add(dictKey, ColName);
                        dictKey++;
                    }
                }
            }
           
            this.UIAPIRawForm.Freeze(false);


            string[] newCard = CardCodeList0.GroupBy(p => p).Select(p => p.Key).ToArray();
               CommonApp.AddOPOR(newCard, oDictionary,Grid0);
                
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.ToString());
                this.UIAPIRawForm.Freeze(false);
            }
            
        }


        private void ButtonCombo0_ComboSelectBefore(object sboObject, SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
           
        }

        private void Grid0_GotFocusAfter(object sboObject, SBOItemEventArg pVal)
        {

            try
            {
                CurrOSHP = Grid0.Columns.Item(Grid0.GetCellFocus().ColumnIndex).TitleObject.Caption;
            }
            catch (Exception e)
            {

            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.CreateOrderIsScale", "CreateOrderIsScale.b1f")]
    class CreateOrderIsScale : UserFormBase
    {
        public  static string OPRQList = String.Empty;
        public CreateOrderIsScale()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.EditText0.KeyDownAfter += new SAPbouiCOM._IEditTextEvents_KeyDownAfterEventHandler(this.EditText0_KeyDownAfter);
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_2").Specific));
            this.Grid0.GotFocusAfter += new SAPbouiCOM._IGridEvents_GotFocusAfterEventHandler(this.Grid0_GotFocusAfter);
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_3").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.ButtonCombo0 = ((SAPbouiCOM.ButtonCombo)(this.GetItem("Item_6").Specific));
            this.ButtonCombo0.ComboSelectAfter += new SAPbouiCOM._IButtonComboEvents_ComboSelectAfterEventHandler(this.ButtonCombo0_ComboSelectAfter);
            this.Button3 = ((SAPbouiCOM.Button)(this.GetItem("Item_8").Specific));
            this.Button3.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button3_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ActivateAfter += new SAPbouiCOM.Framework.FormBase.ActivateAfterHandler(this.Form_ActivateAfter);
            this.DataLoadAfter += new DataLoadAfterHandler(this.Form_DataLoadAfter);

        }

        private SAPbouiCOM.StaticText StaticText0;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width/2-this.UIAPIRawForm.Width/2;
            this.ButtonCombo0.Caption = "生成订单";
            this.ButtonCombo0.ExpandType = BoExpandType.et_DescriptionOnly;
            this.ButtonCombo0.ValidValues.Add("110", "全部店铺");
            this.ButtonCombo0.ValidValues.Add("111", "当前店铺");
            this.ButtonCombo0.Item.Visible = false;
            
        }

        private SAPbouiCOM.EditText EditText0;

        private void EditText0_KeyDownAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (pVal.CharPressed ==(int)(Keys.Tab))
            {
              IRQListScale oIrqListScale = new IRQListScale();
              oIrqListScale.Show();
            }

        }

        private void Form_ActivateAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.EditText0.Value = OPRQList;

        }

        private SAPbouiCOM.Grid Grid0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.ButtonCombo ButtonCombo0;

        private void Form_DataLoadAfter(ref BusinessObjectInfo pVal)
        {
            

        }

        private SAPbouiCOM.Button Button3;

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();

        }

        private void Button3_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
               
            if (!this.EditText0.Value.Equals(""))
            {
                this.UIAPIRawForm.Freeze(true);
                    string sqlCmd = "DECLARE @sqlCmd VARCHAR(max) = '';\r\n" +
                                " DECLARE @fields1 VARCHAR(max) = '',@fields2 VARCHAR(max) = '', @fields3 VARCHAR(max) = '';\r\n " +
                                " SELECT @fields1 = @fields1 + ',' + M.ReqName + '_申请数', @fields2 = @fields2 + ',' + M.ReqName + '_订购数', @fields3 = @fields3 + ',sum(isnull(' + M.ReqName + '_申请数,0)) as ' + M.ReqName + '_申请数,sum(isnull(' + M.ReqName + '_订购数,0)) as ' + M.ReqName + '_订购数' FROM(SELECT t1.ReqName FROM OPRQ T1 INNER JOIN PRQ1 T2 ON T2.DocEntry = T1.DocEntry AND T1.DocStatus = 'O' INNER JOIN OITM T3 ON T2.ItemCode = T3.ItemCode AND T3.U_IsCSale = '是' where T1.DocEntry in("+this.EditText0.Value+") GROUP BY t1.ReqName " +
                                "   )M ;\r\n" +
                                "SELECT @fields1 = RIGHT(@fields1, LEN(@fields1) - 1), @fields2 = RIGHT(@fields2, LEN(@fields2) - 1), @fields3 = RIGHT(@fields3, LEN(@fields3) - 1) ;\r\n" +
                                "SET @sqlCmd = '" +
                                "select '''+'Y'+''' as S0, K.ItemCode, K.ItemName, k.BuyUnitMsr, k.U_Speic, k.U_Factory, K.U_CntcPrsn,k.U_CardCodeA CardCode,K.U_CardNameA,L.U_PerOrder,j.Price, '+@fields3+' from( \r\n" +
                                "    SELECT * FROM(\r\n" +
                                "        SELECT TT.U_PerOrder,T2.ItemCode, T2.Quantity RQQty, T2.Quantity ORQty, T1.ReqName + '''+'_申请数'+''' as RQcbl, T1.ReqName + '''+'_订购数'+''' as ORcbl FROM OPRQ T1 INNER JOIN PRQ1 T2 ON T2.DocEntry = T1.DocEntry AND T1.DocStatus = '''+'O'+''' INNER JOIN OITM T3 ON T2.ItemCode = T3.ItemCode AND T3.U_IsCSale = '''+'是'+''' INNER JOIN OCRD TT ON T3.U_CardCodeA=TT.CardCode where T1.DocEntry in(" + this.EditText0.Value + ") GROUP BY t2.Quantity, t2.ItemCode, t1.ReqName,TT.U_PerOrder " +
                                "     ) u pivot(sum(RQQty) for u.RQcbl in('+@fields1+') ) as P pivot(sum(P.ORQty) for p.ORcbl in ('+@fields2+'))as M)L inner join oitm K on l.ItemCode = K.itemCode " +
                                " left join OSPP J ON J.CardCode = K.U_CardCodeA and j.ItemCode = K.ItemCode " +
                                " group by K.ItemCode, K.ItemName, k.BuyUnitMsr, k.U_Speic, k.U_Factory, K.U_CntcPrsn,k.U_CardCodeA,K.U_CardNameA,j.price,L.U_PerOrder ';\r\n" +
                                " EXEC(@sqlCmd)";
                Grid0.DataTable.ExecuteQuery(sqlCmd);
                Grid0.Columns.Item("S0").TitleObject.Caption = "选择";
                Grid0.Columns.Item("S0").Type = BoGridColumnType.gct_CheckBox;
                Grid0.Columns.Item("ItemCode").TitleObject.Caption = "物料编号";
                Grid0.Columns.Item("ItemCode").Editable = false;
                Grid0.Columns.Item("ItemName").TitleObject.Caption = "物料描述";
                Grid0.Columns.Item("ItemName").Editable = false;
                Grid0.Columns.Item("BuyUnitMsr").TitleObject.Caption = "单位";
                Grid0.Columns.Item("BuyUnitMsr").Editable = false;
                Grid0.Columns.Item("U_Speic").TitleObject.Caption = "规格";
                Grid0.Columns.Item("U_Speic").Editable = false;
                Grid0.Columns.Item("U_Factory").TitleObject.Caption = "产地";
                Grid0.Columns.Item("U_Factory").Editable = false;
                Grid0.Columns.Item("U_CntcPrsn").TitleObject.Caption = "联系人";
                Grid0.Columns.Item("U_CntcPrsn").Editable = false;
                Grid0.Columns.Item("CardCode").TitleObject.Caption = "供应商编码";
                Grid0.Columns.Item("CardCode").Editable = false;
                Grid0.Columns.Item("U_CardNameA").TitleObject.Caption = "供应商名称";
                Grid0.Columns.Item("U_CardNameA").Editable = false;
                Grid0.Columns.Item("U_PerOrder").TitleObject.Caption = "按联系人生成";
                    Grid0.Columns.Item("U_PerOrder").Editable = false;
                    Grid0.Columns.Item("Price").TitleObject.Caption = "价格";
                Grid0.Columns.Item("Price").Editable = false;
                for (int i = 0; i < Grid0.Columns.Count; i++)
                {
                    string ColumnsDescr = Grid0.Columns.Item(i).TitleObject.Caption;
                    if (ColumnsDescr.Contains("_申请数"))
                    {
                        Grid0.Columns.Item(i).Editable = false;
                        GridColumn oGridColumn = Grid0.Columns.Item(i);
                        oGridColumn.Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                        SAPbouiCOM.EditTextColumn oEditGC = (SAPbouiCOM.EditTextColumn) oGridColumn;
                        SAPbouiCOM.BoColumnSumType oST = oEditGC.ColumnSetting.SumType;
                        oEditGC.ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto;
                    }

                    if (ColumnsDescr.Contains("_订购数"))
                    {
                        Grid0.Columns.Item(i).Editable = true;
                        GridColumn oGridColumn = Grid0.Columns.Item(i);
                        oGridColumn.Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                        SAPbouiCOM.EditTextColumn oEditGC = (SAPbouiCOM.EditTextColumn) oGridColumn;
                        SAPbouiCOM.BoColumnSumType oST = oEditGC.ColumnSetting.SumType;
                        oEditGC.ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto;

                    }
                }

                Grid0.AutoResizeColumns();
                this.ButtonCombo0.Item.Visible = true;
                this.UIAPIRawForm.Freeze(false);

            }
            }
            catch (Exception e)
            {
                this.UIAPIRawForm.Freeze(false);
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("错误：" + e.Message);
            }

        }
        private string CurrOSHP = String.Empty;

        private void ButtonCombo0_ComboSelectAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                // ButtonCombo0.Caption = "生成订单";
                string BuCValue = ButtonCombo0.Selected.Value;
            if (BuCValue.Equals("111") && (CurrOSHP.Equals(string.Empty) || !CurrOSHP.Contains("_订购数")))
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("激活单元格不在店铺列中,请在店铺列中单击任意单元格后点击生成采购订单!");
                ButtonCombo0.Caption = "生成采购订单";
                return;
            }
            this.UIAPIRawForm.Freeze(true);
            ButtonCombo0.Caption = "生成采购订单";
            Dictionary<int, string> oDictionary = new Dictionary<int, string>();
            string[] CardCodeList0 = new string[Grid0.Rows.Count];
            for (int i = 0; i < Grid0.Rows.Count; i++)
            {
                CardCodeList0.SetValue(string.Empty, i);
                string S0 = Grid0.DataTable.GetValue("S0", i).ToString();
                if (S0.Equals("Y"))
                {
                    string CardCode = Grid0.DataTable.GetValue("CardCode", i).ToString();
                    string CntcPrsn = Grid0.DataTable.GetValue("U_CntcPrsn", i).ToString();
                    string PerOrder= Grid0.DataTable.GetValue("U_PerOrder", i).ToString();
                    if (PerOrder.Equals("是"))
                    {
                        CardCodeList0.SetValue(CardCode + "," + CntcPrsn, i);
                    }
                    else
                    {
                        CardCodeList0.SetValue(CardCode + "," + "", i);
                    }
                            
                    Grid0.Rows.SelectedRows.Add(i);
                }
            }

            if (BuCValue.Equals("111"))
            {
                oDictionary.Add(0, CurrOSHP);
            }
            else
            {
                int dictKey = 0;
                for (int i = 0; i < Grid0.Columns.Count; i++)
                {
                    string ColName = Grid0.Columns.Item(i).TitleObject.Caption;
                    if (ColName.Contains("_订购数"))
                    {
                        oDictionary.Add(dictKey, ColName);
                        dictKey++;
                    }
                }
            }

            this.UIAPIRawForm.Freeze(false);
            string[] newCard = CardCodeList0.GroupBy(p => p).Select(p => p.Key).ToArray();  //去除重复值
                CommonApp.AddOPOR(newCard, oDictionary,Grid0);

            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.ToString());
            }

        }

        private void Grid0_GotFocusAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                CurrOSHP = Grid0.Columns.Item(Grid0.GetCellFocus().ColumnIndex).TitleObject.Caption;
            }
            catch (Exception e)
            {

            }

        }
    }
}

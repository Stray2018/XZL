﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("KMSJYE.DelCopyData", "DelCopyData.b1f")]
    class DelCopyData : UserFormBase
    {
        public  static  string DocEntry = String.Empty;
       
        public DelCopyData()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>  
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Grid1 = ((SAPbouiCOM.Grid)(this.GetItem("Item_4").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {
            try
            {
                this.UIAPIRawForm.Left = SAPbouiCOM.Framework.Application.SBO_Application.Desktop.Width / 2 - this.UIAPIRawForm.Width / 2;
                Grid1.DataTable.ExecuteQuery("Exec Stray_GetDataDetailOPRQ '" + DocEntry + "','" + Program.CenterWhs + "'");
                Grid1.Columns.Item("xh").Visible = false;
                Grid1.Columns.Item("isDetail").Visible = false;
                for (int i = 0; i < Grid1.Columns.Count; i++)
                {
                    string ColumnName = Grid1.Columns.Item(i).UniqueID;
                    if (!ColumnName.Equals("本次发货数量"))
                    {
                        Grid1.Columns.Item(i).Editable = false;
                    }
                }
                for (int i = 0; i < Grid1.DataTable.Rows.Count; i++)
                {
                    string isDetail = Grid1.DataTable.GetValue("isDetail", i).ToString();
                    if (isDetail.Equals("1"))
                    {
                        int greenForeColor = Color.Bisque.R | (Color.Bisque.G << 8) | (Color.Bisque.B << 16);
                        Grid1.CommonSetting.SetRowBackColor(i + 1, greenForeColor);
                    }
                    else
                    {
                        Grid1.CommonSetting.SetRowEditable(i + 1, false);
                    }
                    Grid1.RowHeaders.SetText(i,(i+1).ToString());
                }

                this.UIAPIRawForm.State = BoFormStateEnum.fs_Maximized;
                Grid1.AutoResizeColumns();
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }
           
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            this.UIAPIRawForm.Close();
        }

        private Grid Grid1;

        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            int Curr = 0;
            try
            {
                DELIVEBPL.LineSource.Clear();
                DELIVEBPL.LineSource.InsertRecord(0);
                for (int i = 0; i < Grid1.Rows.Count; i++)
                {
                    Curr = i;
                    string isDetail = Grid1.DataTable.GetValue("isDetail", i).ToString();
                    if (isDetail.Equals("0"))
                    {
                        continue;
                    }
                    string ItemCode = Grid1.DataTable.GetValue("物料编码", i).ToString();
                    string ItemName = Grid1.DataTable.GetValue("物料描述", i).ToString();
                    string Speic = Grid1.DataTable.GetValue("规格", i).ToString();
                    string AppNo = Grid1.DataTable.GetValue("批准文号", i).ToString();
                    string Factory = Grid1.DataTable.GetValue("产地", i).ToString();
                    string unitMsr = Grid1.DataTable.GetValue("单位", i).ToString();

                    string ComQty = Grid1.DataTable.GetValue("可发货数量", i).ToString();
                    string DistNumber = Grid1.DataTable.GetValue("批号", i).ToString();
                    if (i.Equals(84))
                    {
                        Curr = i;
                    }

                    string MnfDate = String.Empty;
                    try
                    {
                        MnfDate = DateTime.Parse(Grid1.DataTable.GetValue("生产日期", i).ToString()).ToString("yyyyMMdd");
                    }
                    catch (Exception e)
                    {
                       
                    }

                    string ExpDate = String.Empty;
                    try
                    {
                        ExpDate = DateTime.Parse(Grid1.DataTable.GetValue("有效期至", i).ToString()).ToString("yyyyMMdd");
                    }
                    catch (Exception e)
                    {
                        
                    }
                    string Quantity = Grid1.DataTable.GetValue("本次发货数量", i).ToString();
                    if (!double.Parse(Quantity).Equals(0))
                    {
                        int Row = DELIVEBPL.LineSource.Size;
                        DELIVEBPL.LineSource.InsertRecord(Row);
                        DELIVEBPL.LineSource.SetValue("LineId", Row, (Row).ToString());

                        DELIVEBPL.LineSource.SetValue("U_ItemCode", Row - 1, ItemCode);
                        DELIVEBPL.LineSource.SetValue("U_ItemName", Row - 1, ItemName);
                        DELIVEBPL.LineSource.SetValue("U_Speic", Row - 1, Speic);
                        DELIVEBPL.LineSource.SetValue("U_AppNo", Row - 1, AppNo);
                        DELIVEBPL.LineSource.SetValue("U_Factory", Row - 1, Factory);
                        DELIVEBPL.LineSource.SetValue("U_unitMsr", Row - 1, unitMsr);
                        DELIVEBPL.LineSource.SetValue("U_ComQty", Row - 1, ComQty);
                        DELIVEBPL.LineSource.SetValue("U_DistNumber", Row - 1, DistNumber);
                        DELIVEBPL.LineSource.SetValue("U_MnfDate", Row - 1, MnfDate);
                        DELIVEBPL.LineSource.SetValue("U_ExpDate", Row - 1, ExpDate);
                        DELIVEBPL.LineSource.SetValue("U_Quantity", Row - 1, Quantity);
                    }

                }
                this.UIAPIRawForm.Close();
                
            }
            catch (Exception e)
            {
              
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("错误行号:"+Curr.ToString()+Environment.NewLine+e.Message);
            }

        }
    }
}

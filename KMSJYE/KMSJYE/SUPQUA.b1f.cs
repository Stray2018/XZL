﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("UDO_FT_SUQUA")]
    class SUPQUA : UDOFormBase
    {
        public SUPQUA()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("0_U_E").Specific));
            this.EditText0.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.EditText0_ChooseFromListAfter);
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("1_U_E").Specific));
            this.EditText1.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.EditText1_ChooseFromListAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("0_U_G").Specific));
            this.Matrix0.LostFocusAfter += new SAPbouiCOM._IMatrixEvents_LostFocusAfterEventHandler(this.Matrix0_LostFocusAfter);
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.LoadAfter += new SAPbouiCOM.Framework.FormBase.LoadAfterHandler(this.Form_LoadAfter);
            this.ActivateAfter += new SAPbouiCOM.Framework.FormBase.ActivateAfterHandler(this.Form_ActivateAfter);
            this.DataAddAfter += new SAPbouiCOM.Framework.FormBase.DataAddAfterHandler(this.Form_DataAddAfter);

        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();

        }

        private void OnCustomInitialize()
        {
            SAPbouiCOM.ChooseFromList CardCodeList = this.UIAPIRawForm.ChooseFromLists.Item("CardCode");
            SAPbouiCOM.Conditions cardCodeConditions = CardCodeList.GetConditions();
            SAPbouiCOM.Condition cardCodeCondition = cardCodeConditions.Add();
            cardCodeCondition.Alias = "CardType";
            cardCodeCondition.CondVal = "S";
            cardCodeCondition.Operation = BoConditionOperation.co_EQUAL;
            CardCodeList.SetConditions(cardCodeConditions);

            SAPbouiCOM.ChooseFromList CardNameList = this.UIAPIRawForm.ChooseFromLists.Item("CardName");
            SAPbouiCOM.Conditions cardNameConditions = CardNameList.GetConditions();
            SAPbouiCOM.Condition cardNameCondition = cardNameConditions.Add();
            cardNameCondition.Alias = "CardType";
            cardNameCondition.CondVal = "S";
            cardNameCondition.Operation = BoConditionOperation.co_EQUAL;
            CardNameList.SetConditions(cardNameConditions);
            this.Matrix0.AutoResizeColumns();
        }

        private EditText EditText0;

        private void EditText0_ChooseFromListAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                SAPbouiCOM.DataTable oDt = ((SAPbouiCOM.ISBOChooseFromListEventArg)pVal).SelectedObjects;
                if (!oDt.Equals(null))
                {
                    SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@SUQUA");
                    oDataSource.SetValue("Code", 0, oDt.GetValue("CardCode", 0).ToString());
                    oDataSource.SetValue("Name", 0, oDt.GetValue("CardName", 0).ToString());
                    oDataSource.SetValue("U_CardGroup", 0, GetOcrdGroupName(oDt.GetValue("CardCode", 0).ToString()));
                    this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1").Clear();
                    this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1").InsertRecord(0);
                    this.Matrix0.LoadFromDataSourceEx(false);
                    CardCode = oDt.GetValue("CardCode", 0).ToString();

                }
            }
            catch (Exception e)
            {

            }

        }

        private EditText EditText1;

        private void EditText1_ChooseFromListAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {
                SAPbouiCOM.DataTable oDt = ((SAPbouiCOM.ISBOChooseFromListEventArg)pVal).SelectedObjects;
                if (!oDt.Equals(null))
                {
                    SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@SUQUA");
                    oDataSource.SetValue("Code", 0, oDt.GetValue("CardCode", 0).ToString());
                    oDataSource.SetValue("Name", 0, oDt.GetValue("CardName", 0).ToString());
                    oDataSource.SetValue("U_CardGroup", 0, GetOcrdGroupName(oDt.GetValue("CardCode", 0).ToString()));
                    this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1").Clear();
                    this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1").InsertRecord(0);
                    this.Matrix0.LoadFromDataSourceEx(false);
                    CardCode = oDt.GetValue("CardCode", 0).ToString();
                }
            }
            catch (Exception e)
            {

            }

        }

        private string GetOcrdGroupName(string CardCode)
        {
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery("SELECT T2.GroupName FROM OCRD T1 INNER JOIN OCRG T2 ON T1.GroupCode=T2.GroupCode WHERE T1.CardCode='"+CardCode+"'");
            
            return oRec.Fields.Item(0).Value.ToString() ;
        }

        private void Form_ActivateAfter(SBOItemEventArg pVal)
        {
           this.UIAPIRawForm.Refresh();

        }

        private Matrix Matrix0;

        private void Matrix0_LostFocusAfter(object sboObject, SBOItemEventArg pVal)
        {
            Matrix0.FlushToDataSource();
            string QuaName = this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1").GetValue("U_QuaName", Matrix0.RowCount-1);
            if (pVal.ColUID == "C_0_2" && !QuaName.Equals(""))
            {
                this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1").InsertRecord(pVal.Row);
                Matrix0.LoadFromDataSourceEx(false);
            }

        }
        private EditText EditText2;
        private Button Button0;
        private static string CardCode = String.Empty;
        private void Button0_ClickAfter(object sboObject, SBOItemEventArg pVal)
        {
            try
            {

            SAPbouiCOM.DBDataSource headSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@SUQUA");
            SAPbouiCOM.DBDataSource lineSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@UQUA1");
            string cardCode = headSource.GetValue("Code", 0).ToString();
            if (cardCode.Equals(""))
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("请先选择供应商！");
                return;
            }
            string CardGroup = headSource.GetValue("U_CardGroup", 0).ToString();
            if (CardGroup.Equals("") || ((!CardGroup.Equals("生产商")) && (!CardGroup.Equals("批发商"))))
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("供应商分组不正确，请先设置正确的分组：生产商  或者  批发商！");
                return;
            }
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRec.DoQuery("SELECT * FROM [@SUQTYPE] T1 WHERE T1.U_CardGroup ='全部' OR  T1.U_CardGroup = '"+CardGroup+"'");
            lineSource.Clear();
            lineSource.InsertRecord(0);
            for (int i = 0; i < oRec.RecordCount; i++)
            {
                lineSource.InsertRecord(i);
                lineSource.SetValue("LineId",i,(i+1).ToString());
                lineSource.SetValue("U_QuaName",i,oRec.Fields.Item("Name").Value.ToString());
                lineSource.SetValue("U_ManExp", i, oRec.Fields.Item("U_IsExp").Value.ToString());
                lineSource.SetValue("U_IsReq", i, oRec.Fields.Item("U_IsReq").Value.ToString());
                oRec.MoveNext();   
            }
            this.Matrix0.LoadFromDataSourceEx(false);
            }
            catch (Exception e)
            {
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox(e.Message);
            }

        }

        private void Form_DataAddAfter(ref BusinessObjectInfo pVal)
        {
            
upDateOcrdUDOField(CardCode);
        }

        private void upDateOcrdUDOField(string UDOCode)
        {
            SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            string sql = "UPDATE OCRD SET U_SUPQUA = '"+UDOCode+"' WHERE CardCode='"+UDOCode+"'";
            oRec.DoQuery(sql);
            AddTableFields.ReleaseCom(oRec);
        }

        private StaticText StaticText0;
    }
}

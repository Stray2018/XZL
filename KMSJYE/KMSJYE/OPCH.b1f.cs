﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using SAPbobsCOM;
using SAPbouiCOM.Framework;

namespace KMSJYE
{
    [FormAttribute("141", "OPCH.b1f")]
    class OPCH : SystemFormBase
    {
        public OPCH()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("10").Specific));
            this.EditText0.LostFocusAfter += new SAPbouiCOM._IEditTextEvents_LostFocusAfterEventHandler(this.EditText0_LostFocusAfter);
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("12").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("4").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("85").Specific));
            this.ComboBox0.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox0_ComboSelectAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataAddBefore += new DataAddBeforeHandler(this.Form_DataAddBefore);

        }

        private void Form_DataAddBefore(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.DBDataSource oDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("PCH1");
            SAPbouiCOM.DBDataSource oPCH = this.UIAPIRawForm.DataSources.DBDataSources.Item("OPCH");
            string[] cntPrcn = new string[oDataSource.Size];
            for (int i = 0; i < oDataSource.Size; i++)
            {
                string cntPrcn1 = oDataSource.GetValue("U_CntcPrsn", i).ToString();
                if (!cntPrcn1.Equals(""))
                {
                    cntPrcn.SetValue(cntPrcn1, i);
                }
               
            }

            SAPbobsCOM.BusinessPartners oBp =
                Program.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners) as SAPbobsCOM.BusinessPartners;
            oBp.GetByKey(oPCH.GetValue("CardCode", 0));
            string PerOrder = oBp.UserFields.Fields.Item("U_PerOrder").Value.ToString();
            cntPrcn = cntPrcn.Where(s => !string.IsNullOrEmpty(s)).ToArray();  //去除为空的值
            string[] cntPrcn2 = cntPrcn.Distinct().ToArray();
            if (cntPrcn2.Length>1 && PerOrder.Equals("是"))
            {
                BubbleEvent = false;
                AddTableFields.ReleaseCom(oBp);
                SAPbouiCOM.Framework.Application.SBO_Application.MessageBox("发票中存在多个联系人供应的药品，不允许开在一张发票中，须分开开应付发票！");
                return;
            }
            AddTableFields.ReleaseCom(oBp);
            DocDueDate();
        }

        private void OnCustomInitialize()
        {

        }

        private SAPbouiCOM.EditText EditText0;  //过帐日期

        private void EditText0_LostFocusAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            //修改过帐日期后修改到期日期
            DocDueDate();

        }

        private SAPbouiCOM.EditText EditText1;　　//到期日期
        private SAPbouiCOM.EditText EditText2;//供应商
        private SAPbouiCOM.ComboBox ComboBox0; //联系人

        private void ComboBox0_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            //选择联系人后更改到期日期
            DocDueDate();
        }

        private void DocDueDate()
        {
            string CardCode = EditText2.Value;
            string CntcPrsn = "";

            try
            {
                CntcPrsn = ComboBox0.Selected.Description;
            }
            catch (Exception e)
            {
            }

            if (!CardCode.Equals("") && !CntcPrsn.Equals(""))
            {
                string sqlCmd = "SELECT T1.U_AccOf FROM OCPR T1 WHERE T1.CardCode='" + CardCode + "' AND T1.Name = '" + CntcPrsn + "'";
                SAPbobsCOM.Recordset oRec = Program.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                oRec.DoQuery(sqlCmd);
                if (oRec.RecordCount > 0)
                {
                    int AccOf = int.Parse(oRec.Fields.Item(0).Value.ToString());
                    DateTime DocDate = DateTime.ParseExact(EditText0.Value, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    DateTime DocDueDate = DocDate.AddDays(AccOf);
                    EditText1.Value = DocDueDate.ToString("yyyyMMdd");
                }
            }

            if (CntcPrsn.Equals(""))
            {
                EditText1.Value = EditText0.Value;
            }
        }
    }
}

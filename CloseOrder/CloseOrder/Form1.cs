﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CloseOrder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private SAPbobsCOM.Company sapCompany = new SAPbobsCOM.Company();
        private SAPbobsCOM.Company sCompanyobj = new SAPbobsCOM.Company();
        private void textBox1_Leave(object sender, EventArgs e)
        {
            textBox2.Text = textBox1.Text.Substring(0, textBox1.Text.IndexOf(",") > 0 ? textBox1.Text.IndexOf(",") : textBox1.Text.Trim().Length)+":30000";
        }
        private void GetCompanyList()
        {
            sapCompany.Server = textBox1.Text;
            sapCompany.SLDServer ="https://"+textBox2.Text.Replace("30000","40000");
            sapCompany.LicenseServer = textBox2.Text;
            string sqlType = comboBox2.SelectedItem.ToString();
            switch (sqlType)
            {
                case "SQL2008":
                    sapCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    break;

                case "SQL2012":
                    sapCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "SQL2014":
                    sapCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "SQL2016":
                    sapCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "SQL2017":
                    sapCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                    break;
                default:
                    break;
            }
            sapCompany.language = SAPbobsCOM.BoSuppLangs.ln_Chinese;
            //sapCompany.DbUserName = textBox6.Text.Trim();
            //sapCompany.DbPassword = textBox5.Text.Trim();
            SAPbobsCOM.Recordset oRec = sapCompany.GetCompanyList();
            int errCode = 0;
            string errMsg = string.Empty;
            sapCompany.GetLastError(out errCode, out errMsg);
            if (!errCode.Equals(0))
            {
                MessageBox.Show(errMsg);
                return;
            }
            comboBox1.Items.Clear();

            for (int i = 0; i < oRec.RecordCount; i++)
            {
             comboBox1.Items.Add(oRec.Fields.Item("dbName").Value.ToString());

                oRec.MoveNext();
            }
            comboBox1.SelectedIndex = 0;
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (sapCompany.Connected)
                {
                    sapCompany.Disconnect();
                }
           
            sapCompany.UserName = textBox3.Text.Trim();
            sapCompany.Password = textBox4.Text.Trim();
                sapCompany.CompanyDB = comboBox1.SelectedItem.ToString();
            sapCompany.Connect();
            int errCode = 0;
            string errMsg = string.Empty;
            sapCompany.GetLastError(out errCode,out errMsg);
            if (errCode != 0 )
            {
                MessageBox.Show(errMsg, "连接SAP失败");
            }
            else
            {
                button5.BackColor = Color.Green;
                MessageBox.Show("连接SAP成功");
            }
            }
            catch (Exception e1)
            {

                MessageBox.Show(e1.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

            
            CreateSqlConn();
            DataTable dataTable= new DataTable();
            string sqlTable = "if exists(select * from sys.tables where name='CloseOrder')  drop table CloseOrder;\r\ncreate table CloseOrder (\r\n DocEntry NVARCHAR(50),\r\n    DocNum NVARCHAR(50),\r\n    CardCode NVARCHAR(50),\r\n    DocDate NVARCHAR(20),\r\n    DocTotal NUMERIC(18,2),\r\n    Status NVARCHAR(10),\r\n    errMsg NVARCHAR(max)\r\n);\r\n";
            SqlDataAdapter osqlData = new SqlDataAdapter();
            osqlData = new SqlDataAdapter(sqlTable, oConn.ConnectionString);
            osqlData.Fill(dataTable);
            dataTable.Clear();
            osqlData = new SqlDataAdapter("select * from CloseOrder", oConn.ConnectionString);
            osqlData.Fill(dataTable);
            
         string DocNumText=Clipboard.GetText();

            if (!DocNumText.Equals(string.Empty))
            {
                foreach (string item0 in DocNumText.Split(new[] {"\r\n"} ,StringSplitOptions.RemoveEmptyEntries))
                {
                  DataRow dataRow= dataTable.NewRow();
                  dataRow["DocNum"]= item0.ToString();
                  dataTable.Rows.Add(dataRow);
                 
                }
                    SqlCommandBuilder scb = new SqlCommandBuilder(osqlData);
                    int m= osqlData.Update(dataTable.GetChanges());
                    dataTable.AcceptChanges();
                    osqlData.Dispose();
                    dataTable.Clear();
                string sqlQery = "select T2.DocEntry, T1.DocNum,T2.CardName as CardCode,convert(Nvarchar(10), T2.DocDate,121) DocDate,T2.DocTotal,N'待处理' as Status,'' as errMsg from CloseOrder T1 inner join ORDR T2 on T1.DocNum=T2.DocNum";
                osqlData = new SqlDataAdapter(sqlQery, oConn.ConnectionString);
                osqlData.Fill(dataTable);
                    dataGridView1.DataSource = dataTable;

                }
            else
            {
                MessageBox.Show("未获取到数据，请重新复制!");
            }
            }
            catch (Exception l)
            {

                MessageBox.Show(l.ToString());
            }
        }

        private SqlConnection oConn = new SqlConnection();
        private void CreateSqlConn()
        {
            SqlConnectionStringBuilder connStr= new SqlConnectionStringBuilder();
            connStr.DataSource = textBox1.Text.Trim();
            connStr.UserID= textBox6.Text.Trim();
            connStr.Password= textBox5.Text.Trim();
            connStr.InitialCatalog = comboBox1.SelectedItem.ToString();
            oConn.ConnectionString= connStr.ConnectionString;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                comboBox1.Items.Clear();
                GetCompanyList();
            }
            catch (Exception s)
            {
                MessageBox.Show(s.ToString());

            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SAPbobsCOM.Documents oDoc = sapCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders) as SAPbobsCOM.Documents;
            DataTable oDt = (DataTable)dataGridView1.DataSource;
            for (int i = 0; i < oDt.Rows.Count; i++)
            {
               string  DocEntry = oDt.Rows[i]["DocEntry"].ToString();
                int iDoc = int.Parse(DocEntry);
                if (oDoc.GetByKey(iDoc) ) { 
                    if (oDoc.DocumentStatus == SAPbobsCOM.BoStatus.bost_Open)
                    {
                        oDoc.Close();
                        int errCode;
                        string errMsg;
                        sapCompany.GetLastError(out errCode, out errMsg);
                        if (errCode.Equals(0))
                        {
                            oDt.Rows[i][columnName: "Status"] = "成功";
                        }
                        else
                        {
                            oDt.Rows[i][columnName: "Status"] = "失败";
                            oDt.Rows[i][columnName: "errMsg"] = errMsg;
                        }
                    }
                    else
                    {
                        oDt.Rows[i][columnName: "Status"] = "失败";
                        oDt.Rows[i][columnName: "errMsg"] = "销售订单不是打开状态，不能关闭!";
                    }
                
                    dataGridView1.Refresh();
                    
                }
            }
          

        }

        private void button7_Click(object sender, EventArgs e)
        {
           sCompany.Items.Clear(); 
            sGetCompanyList();

        }
        private void sGetCompanyList()
        {
            sCompanyobj.Server = sDataServer.Text;
            sCompanyobj.SLDServer = "https://" + sLiceServer.Text.Replace("30000", "40000");
            sCompanyobj.LicenseServer = sLiceServer.Text;
            string sqlType = sDataType.SelectedItem.ToString();
            switch (sqlType)
            {
                case "SQL2008":
                    sCompanyobj.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    break;

                case "SQL2012":
                    sCompanyobj.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "SQL2014":
                    sCompanyobj.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "SQL2016":
                    sCompanyobj.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "SQL2017":
                    sCompanyobj.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                    break;
                default:
                    break;
            }
            sCompanyobj.language = SAPbobsCOM.BoSuppLangs.ln_Chinese;
            //sapCompany.DbUserName = textBox6.Text.Trim();
            //sapCompany.DbPassword = textBox5.Text.Trim();
            SAPbobsCOM.Recordset oRec = sCompanyobj.GetCompanyList();
            int errCode = 0;
            string errMsg = string.Empty;
            sCompanyobj.GetLastError(out errCode, out errMsg);
            if (!errCode.Equals(0))
            {
                MessageBox.Show(errMsg);
                return;
            }
            sCompany.Items.Clear();

            for (int i = 0; i < oRec.RecordCount; i++)
            {
                sCompany.Items.Add(oRec.Fields.Item("dbName").Value.ToString());

                oRec.MoveNext();
            }
            sCompany.SelectedIndex = 0;
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec);

        }

        private void button5_Click(object sender, EventArgs e)
        {
           
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (sCompanyobj.Connected)
                {
                    sCompanyobj.Disconnect();
                }

                sCompanyobj.UserName = sUser.Text.Trim();
                sCompanyobj.Password = sPwd.Text.Trim();
                sCompanyobj.CompanyDB = sCompany.SelectedItem.ToString();
                sCompanyobj.Connect();
                int errCode = 0;
                string errMsg = string.Empty;
                sCompanyobj.GetLastError(out errCode, out errMsg);
                if (errCode != 0)
                {
                    MessageBox.Show(errMsg, "连接SAP失败");
                }
                else
                {
                    button6.BackColor = Color.Green;
                    MessageBox.Show("连接SAP成功");
                }
            }
            catch (Exception e1)
            {

                MessageBox.Show(e1.ToString());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace CARMERStyle
{
    [FormAttribute("UDO_FT_KSGL")]
    class STYLE1 : UDOFormBase
    {
        public STYLE1()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.PictureBox0 = ((SAPbouiCOM.PictureBox)(this.GetItem("Item_0").Specific));
            this.PictureBox0.DoubleClickBefore += new SAPbouiCOM._IPictureBoxEvents_DoubleClickBeforeEventHandler(this.PictureBox0_DoubleClickBefore);
            this.PictureBox1 = ((SAPbouiCOM.PictureBox)(this.GetItem("Item_1").Specific));
            this.PictureBox1.DoubleClickBefore += new SAPbouiCOM._IPictureBoxEvents_DoubleClickBeforeEventHandler(this.PictureBox1_DoubleClickBefore);
            this.PictureBox2 = ((SAPbouiCOM.PictureBox)(this.GetItem("Item_2").Specific));
            this.PictureBox2.DoubleClickBefore += new SAPbouiCOM._IPictureBoxEvents_DoubleClickBeforeEventHandler(this.PictureBox2_DoubleClickBefore);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_7").Specific));
            this.updater = ((SAPbouiCOM.EditText)(this.GetItem("15_U_E").Specific));
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("0_U_G").Specific));
            this.Matrix0.LostFocusAfter += new SAPbouiCOM._IMatrixEvents_LostFocusAfterEventHandler(this.Matrix0_LostFocusAfter);
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("Mnget").Specific));
            this.ComboBox2 = ((SAPbouiCOM.ComboBox)(this.GetItem("Atype").Specific));
            this.ComboBox3 = ((SAPbouiCOM.ComboBox)(this.GetItem("SKUS").Specific));
            this.ComboBox4 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_13").Specific));
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_8").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_10").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataUpdateBefore += new SAPbouiCOM.Framework.FormBase.DataUpdateBeforeHandler(this.Form_DataUpdateBefore);
            this.DataAddBefore += new DataAddBeforeHandler(this.Form_DataAddBefore);

        }

        private SAPbouiCOM.PictureBox PictureBox0;

        private void OnCustomInitialize()
        {

        }

        private SAPbouiCOM.PictureBox PictureBox1;
        private SAPbouiCOM.PictureBox PictureBox2;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.EditText updater;
        private void ChangePIC(string pic)
        {
            SAPbouiCOM.DBDataSource oDB = this.UIAPIRawForm.DataSources.DBDataSources.Item("@UDO_STYLE");
            oDB.SetValue(pic, 0, "");
        }

        private void PictureBox0_DoubleClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            // throw new System.NotImplementedException();
            if (pVal.Modifiers == SAPbouiCOM.BoModifiersEnum.mt_CTRL)
            { ChangePIC("U_KS_PIC1"); }

        }

        private void PictureBox1_DoubleClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //throw new System.NotImplementedException();
            if (pVal.Modifiers == SAPbouiCOM.BoModifiersEnum.mt_CTRL)
            { ChangePIC("U_KS_PIC2"); }

        }

        private void PictureBox2_DoubleClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            // throw new System.NotImplementedException();
            if (pVal.Modifiers == SAPbouiCOM.BoModifiersEnum.mt_CTRL)
            { ChangePIC("U_KS_PIC3"); }

        }

        private SAPbouiCOM.Matrix Matrix0;

        private void Matrix0_LostFocusAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            //throw new System.NotImplementedException();
            string Value0 = ((SAPbouiCOM.EditText)this.Matrix0.GetCellSpecific("C_0_1", pVal.Row)).Value.ToString().Trim();
            if (pVal.ColUID == "C_0_1" && pVal.Row == this.Matrix0.VisualRowCount && Value0!="0.0")
            {
                this.UIAPIRawForm.Menu.Item("KSGL_Add_Line").Activate();
            }
        }

        private void Form_DataUpdateBefore(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //throw new System.NotImplementedException();
            this.updater.Value = globals.oCompany.UserName;
        }

        private void Form_DataAddBefore(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //throw new System.NotImplementedException();
            this.updater.Value = globals.oCompany.UserName;
        }

        private SAPbouiCOM.ComboBox ComboBox1;
        private SAPbouiCOM.ComboBox ComboBox2;
        private SAPbouiCOM.ComboBox ComboBox3;
        private SAPbouiCOM.ComboBox ComboBox4;
        private SAPbouiCOM.StaticText StaticText2;
        private SAPbouiCOM.ComboBox ComboBox0;
    }
}

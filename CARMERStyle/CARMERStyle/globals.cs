﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;

namespace CARMERStyle
{
    sealed class globals
    {
        public static SAPbobsCOM.Company oCompany;
        public static string sErrMsg = null;
        public static int lErrCode = 0;
        public static int lRetCode;
        public static string mainCurr;
        #region 初始化公司服务
        
        #endregion
        #region 释放Com对象
        public static void ReleaseCom(Object o)
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
        }
        #endregion
        #region 增加表
        public static void AddTbs()
        {
            string ErrMsg = "";
            AddTableFields.AddB1Table("", "", BoUTBTableType.bott_Document, out ErrMsg);
            AddTableFields.AddB1Table("", "", BoUTBTableType.bott_DocumentLines, out ErrMsg);
        }
        #endregion
        #region 获取本币
        public static string GetMainCurr()
        {
            SAPbobsCOM.Recordset oRec =
                (SAPbobsCOM.Recordset)globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRec.DoQuery("SELECT MainCurncy FROM dbo.OADM");
            string Cur = oRec.Fields.Item(0).Value.ToString().Trim();
            ReleaseCom(oRec);
            return Cur;
        }
        #endregion
   

       
    }
    public class LimitAmount
    {
        public string code { get; set; }
        public double HighAmount { get; set; }
        public double LowAmount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;

namespace CARMERStyle
{
    class AddTableFields
    {
        #region 添加B1数据表方法
        /// <summary>
        /// AddTableToB1DB
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="tableDes"></param>
        /// <param name="tableType"></param>
        /// <returns>success_flag</returns>
        public static bool AddB1Table(string tableName, string tableDes, BoUTBTableType tableType, out string Errs)
        {
            SAPbobsCOM.UserTablesMD oUserTablesMd =
                (SAPbobsCOM.UserTablesMD)globals.oCompany.GetBusinessObject(BoObjectTypes.oUserTables);
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            if (!oUserTablesMd.GetByKey(tableName))
            {
                oUserTablesMd.TableName = tableName;
                oUserTablesMd.TableDescription = tableDes;
                oUserTablesMd.TableType = tableType;

                if (oUserTablesMd.Add() == 0)
                {
                    globals.ReleaseCom(oUserTablesMd);
                    Errs = null;
                    return true;
                }
                else
                {

                    globals.ReleaseCom(oUserTablesMd);
                    Errs = globals.oCompany.GetLastErrorDescription();
                    return false;
                }
            }
            else
            {
                globals.ReleaseCom(oUserTablesMd);
                Errs = null;
                return false;
            }
        }
        #endregion
        #region 添加B1字段到数据表方法
        /// <summary>
        /// AddFieldsToB1DBTable
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="fieldName"></param>
        /// <param name="fieldDes"></param>
        /// <param name="fieldTypes"></param>
        /// <param name="subTypes"></param>
        /// <param name="length"></param>
        /// <returns>success_flag</returns>
        public static bool AddB1Fields(string tableName, string fieldName, string fieldDes, BoFieldTypes fieldTypes, BoFldSubTypes subTypes, int? length, Dictionary<string, string> oDictionary)
        {
           
            SAPbobsCOM.UserFieldsMD oUserFieldsMD =
                (SAPbobsCOM.UserFieldsMD)globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields);
            oUserFieldsMD.TableName = tableName;
            oUserFieldsMD.Name = fieldName;
            oUserFieldsMD.Description = fieldDes;
            oUserFieldsMD.Type = fieldTypes;
            oUserFieldsMD.SubType = subTypes;
           
            if (oDictionary != null)
            {
                foreach (KeyValuePair<string, string> keyValuePair in oDictionary)
                {
                    oUserFieldsMD.ValidValues.Value = keyValuePair.Key;
                    oUserFieldsMD.ValidValues.Description = keyValuePair.Value;
                    oUserFieldsMD.ValidValues.Add();
                }
                if (fieldName == "Quantity")
                {
                    oUserFieldsMD.DefaultValue = "1";
                }
            }
            if (length.HasValue)
            {
                oUserFieldsMD.EditSize = length.Value;
            }
           // oUserFieldsMD.SubType = subTypes;

            if (oUserFieldsMD.Add() == 0)
            {
                globals.ReleaseCom(oUserFieldsMD);
                SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Create Table:" + tableName + "  Create Fields:" + fieldName, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                return true;
            }
            else
            {
                SAPbouiCOM.Framework.Application.SBO_Application.SetStatusBarMessage(globals.oCompany.GetLastErrorDescription(), BoMessageTime.bmt_Long, true);
                globals.ReleaseCom(oUserFieldsMD);
                return false;
            }
        }
        #endregion

        #region 添加对象可查找列
        public static void AddCanFindColumns(SAPbobsCOM.UserObjectMD_FindColumns Columns, string ColumnAlias, string ColumnDescription)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Set Find Fields:" + ColumnAlias, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            for (int j = 0; j < Columns.Count; j++)
            {
                Columns.SetCurrentLine(j);
                if (Columns.ColumnAlias == ColumnAlias)
                {
                    return;
                }
            }
            Columns.ColumnAlias = ColumnAlias;
            Columns.ColumnDescription = ColumnDescription;
            Columns.Add();
        }

        #endregion

        #region 添加对象默认字段
        public static void AddFormColumns(SAPbobsCOM.UserObjectMD_FormColumns Columns, string ColumnAlias, string ColumnDescription, BoYesNoEnum YoS, int i,int rowid)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Set Default Fields:" + ColumnAlias, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            for (int j = 0; j < Columns.Count; j++)
            {
                Columns.SetCurrentLine(j);
                if (Columns.FormColumnAlias == ColumnAlias)
                {
                    return;
                }
            }
            //Columns.FormColumnNumber = rowid;
            Columns.FormColumnAlias = ColumnAlias;
            Columns.FormColumnDescription = ColumnDescription;
            Columns.Editable = YoS;
            Columns.SonNumber = i;
            Columns.Add();
        }
        #endregion

        #region 添加对象默认字段
        public static void AddChildFormColumns(SAPbobsCOM.UserObjectMD_EnhancedFormColumns Columns, string ColumnAlias, string ColumnDescription, BoYesNoEnum YoS, int i)
        {
            SAPbouiCOM.Framework.Application.SBO_Application.StatusBar.SetText("Set Default Column:"+ColumnAlias,BoMessageTime.bmt_Short,BoStatusBarMessageType.smt_Success);
            
            for (int j = 0; j < Columns.Count; j++)
            {
                Columns.SetCurrentLine(j);
                if (Columns.ColumnAlias == ColumnAlias && Columns.ChildNumber == i)
                {
                    return;
                }
            }
            Columns.ColumnAlias = ColumnAlias;
            Columns.ColumnDescription = ColumnDescription;
            Columns.Editable = YoS;
            Columns.ColumnIsUsed = YoS;
            Columns.ChildNumber = i;
            Columns.Add();
        }
        #endregion
        #region 添加对象子表

        public static void AddChildTable(SAPbobsCOM.UserObjectMD_ChildTables childTables,string ChildTablename)
        {

            for (int i = 0; i < childTables.Count; i++)
            {
             childTables.SetCurrentLine(i);
              if(childTables.TableName==ChildTablename)
              {return;}
            }
            childTables.TableName = ChildTablename;
            childTables.LogTableName = "A" + ChildTablename;
            childTables.Add();
            
        }
        #endregion
        public static void GetChildTableSonNumber(SAPbobsCOM.UserObjectMD_ChildTables childTables,string ChildTableName,out int songnumber)
        {
            songnumber = 0;
            for (int i=0;i<childTables.Count;i++)
            {
                childTables.SetCurrentLine(i);
                if (childTables.TableName == ChildTableName)
                {
                    songnumber = childTables.SonNumber+1;
                    return;
                }
            }
        }
    }
}

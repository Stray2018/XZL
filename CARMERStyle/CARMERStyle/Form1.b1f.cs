﻿using System;
using System.Collections.Generic;
using System.Xml;
using SAPbouiCOM.Framework;

namespace CARMERStyle
{
    [FormAttribute("CARMERStyle.Form1", "Form1.b1f")]
    class Form1 : UserFormBase
    {
        public Form1()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {

        }

        private SAPbouiCOM.Button Button1;

        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //  throw new System.NotImplementedException();
            this.UIAPIRawForm.Close();
        }

        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            // throw new System.NotImplementedException();
            //创建表
            string errs;
            AddTableFields.AddB1Table("UDO_STYLE", "款式管理主表", SAPbobsCOM.BoUTBTableType.bott_MasterData,out errs);
            AddTableFields.AddB1Table("UDO_STYLE1", "款式管理子表1", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines, out errs);
            //创建字段
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_TYPE", "品类", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            Dictionary<string,string> oDictionary = new Dictionary<string, string>();
            oDictionary.Add("正常","正常");
            oDictionary.Add("淘汰", "淘汰");
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_STAT", "状态", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, oDictionary);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_CMPY", "创建公司", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_UPDATER", "修改人", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_MNETHOD", "管理方式", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_SKUS", "SKU 档数", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_FEATURE", "特殊描述", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_SPECREQ", "特殊要求", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_PIC1", "图片1",SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_Link, null, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_PIC2", "图片2", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_Link, null, null);
            AddTableFields.AddB1Fields("@UDO_STYLE", "KS_PIC3", "图片3", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_Link, null, null);

            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_MCT", "主石(分/P)", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_VCT", "副石1(ct/p)", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_PT", "PT(g)", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_AU", "AU(g)", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);

            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_ORIGN", "原版款号", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_ORIGF", "原加工厂", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_CURRN", "现版款号", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_STYLE2", "款号2", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, null);

            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_CURRF", "现加工厂", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, null);

            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_CDSTOCK", "成都标存)", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_CQSTOCK", "重庆标存", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);
            AddTableFields.AddB1Fields("@UDO_STYLE1", "KS_SZSTOCK", "深圳标存", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50, null);
            //创建对象
            SAPbobsCOM.UserObjectsMD oOBJmd = (SAPbobsCOM.UserObjectsMD) globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);
           
            bool haveOBJ = oOBJmd.GetByKey("KSGL");
            int err;
            string errs1;

            #region 设置主表属性

            oOBJmd.Code = "KSGL"; //对象代码
            oOBJmd.Name = "款式档案管理"; //对象名称
            oOBJmd.ObjectType = SAPbobsCOM.BoUDOObjType.boud_MasterData; //绑定数据类型
            oOBJmd.TableName = "UDO_STYLE"; //表名
            oOBJmd.CanFind = SAPbobsCOM.BoYesNoEnum.tYES; //是否可查找
            oOBJmd.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES;
            oOBJmd.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO;
            oOBJmd.CanClose = SAPbobsCOM.BoYesNoEnum.tNO;
            oOBJmd.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES; //创建默认form
            oOBJmd.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES;
            oOBJmd.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES;
            
           // oOBJmd.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO;

            oOBJmd.MenuItem = SAPbobsCOM.BoYesNoEnum.tYES;
            oOBJmd.MenuCaption = "款式档案管理";
            oOBJmd.MenuUID = "KSGL";
            oOBJmd.FatherMenuID = 3072;
            oOBJmd.Position = -1;
            oOBJmd.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
            #endregion

            #region   加入子表
            AddTableFields.AddChildTable(oOBJmd.ChildTables, "UDO_STYLE1");
            //oOBJmd.ChildTables.TableName = "UTRAN1";  //子表名
            //oOBJmd.ChildTables.LogTableName = "AUTRAN1";//子表日志名
            //oOBJmd.ChildTables.ObjectName = "OUTRAN"; //UDO对象名
            //oOBJmd.ChildTables.Add();
            #endregion
            int sonNumber = 0;
            int rowid = 1;
            
            AddTableFields.GetChildTableSonNumber(oOBJmd.ChildTables, "UDO_STYLE1", out sonNumber);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_MCT", "主石(分/P)", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_VCT", "副石1(ct/p)", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_PT", "PT(g)", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_AU", "AU(g)", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_ORIGN", "原版款号", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_ORIGF", "原加工厂", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_CURRN", "现版款号", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_CURRF", "现加工厂", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_CDSTOCK", "成都标存", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_CQSTOCK", "重庆标存", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_SZSTOCK", "深圳标存", SAPbobsCOM.BoYesNoEnum.tNO, sonNumber, rowid++);

            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_MCT", "主石(分/P)", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_VCT", " 副石1(ct/p)", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_PT", " PT(g)", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_AU", "AU(g)", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_ORIGN", "原版款号", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_ORIGF", "原加工厂", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_CURRN", "现版款号", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_CURRF", "现加工厂", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_CDSTOCK", "成都标存", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_CQSTOCK", "重庆标存", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);
            AddTableFields.AddChildFormColumns(oOBJmd.EnhancedFormColumns, "U_KS_SZSTOCK", "深圳标存", SAPbobsCOM.BoYesNoEnum.tYES, sonNumber);


            #region 设置查找字段
            AddTableFields.AddCanFindColumns(oOBJmd.FindColumns, "Code", "KLM款号");
            AddTableFields.AddCanFindColumns(oOBJmd.FindColumns, "U_KS_TYPE", "品类");
            AddTableFields.AddCanFindColumns(oOBJmd.FindColumns, "U_KS_FEATURE", "特色描述");
            AddTableFields.AddCanFindColumns(oOBJmd.FindColumns, "U_KS_SPECREQ", "特殊要求");
            #endregion
            #region 设置表格默认字段
            
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "Code", "KLM款号", SAPbobsCOM.BoYesNoEnum.tYES, 0,rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_TYPE", "品类", SAPbobsCOM.BoYesNoEnum.tYES, 0, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_CMPY", "创建公司", SAPbobsCOM.BoYesNoEnum.tYES, 0,rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_UPDATER", "修改人", SAPbobsCOM.BoYesNoEnum.tYES, 0, rowid++);
            //AddTableFields.AddFormColumns(oOBJmd.FormColumns, "UpdateDate", "修改时间", SAPbobsCOM.BoYesNoEnum.tYES, 0,rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_MNETHOD", "管理方式", SAPbobsCOM.BoYesNoEnum.tYES, 0, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_SKUS", "SKU 档数", SAPbobsCOM.BoYesNoEnum.tYES, 0, rowid++);
            // AddTableFields.AddFormColumns(oOBJmd.FormColumns, "CreateDate", "创建时间", SAPbobsCOM.BoYesNoEnum.tYES, 0,rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_FEATURE", "特色描述", SAPbobsCOM.BoYesNoEnum.tYES, 0, rowid++);
            AddTableFields.AddFormColumns(oOBJmd.FormColumns, "U_KS_SPECREQ", "特殊要求", SAPbobsCOM.BoYesNoEnum.tYES, 0, rowid++);
            
            #endregion
            if (!haveOBJ)
            {
                oOBJmd.Add();
            }
            else
            {
                oOBJmd.Update();
            }
            globals.oCompany.GetLastError(out err, out errs);
            if (err != 0)
            {
                Application.SBO_Application.SetStatusBarMessage(errs, SAPbouiCOM.BoMessageTime.bmt_Short, false);
            }
            else
            {

                Application.SBO_Application.SetStatusBarMessage(haveOBJ==true?"修改":"创建对象" +oOBJmd.Code +oOBJmd.Name+"创建完成！", SAPbouiCOM.BoMessageTime.bmt_Short, false);
            }
            globals.ReleaseCom(oOBJmd);


        }
    }
}
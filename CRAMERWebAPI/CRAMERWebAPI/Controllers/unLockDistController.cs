﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class unLockDistController : ApiController
    {
        //Para {"DocNum":"StrayTest","data":[{"DistNumber":"123"},{"DistNumber":"456"},{"DistNumber":"789"}]}
        public JObject POST([FromBody]JObject oPara)
        {
            Comm.Comm.GetsqlConn();
            try
            {

                string DocNum = oPara.GetValue("DocNum").Value<string>();
                JArray jArray = (JArray)JsonConvert.DeserializeObject(oPara["data"].ToString());
                string DistNumber = string.Empty, sqlCmdInsert = string.Empty;
                foreach (var PerRow in jArray)
                {
                    sqlCmdInsert += "delete XZL_LockDistNumber where DistNumber = '" + PerRow["DistNumber"].ToString() + "' and DocNum = '"+ DocNum + "';";
                }

                //string sqlCmd = "SELECT name FROM sys.tables WHERE name = 'XZL_LockDistNumber'";
                //DataTable ExistDt = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                //if (ExistDt.Rows.Count < 1)
                //{
                //    sqlCmd = "CREATE TABLE [XZL_LockDistNumber]( " +
                //             "[DistNumber][nvarchar](50) NOT NULL DEFAULT(''), " +
                //             "[DocNum] [nvarchar](50) NOT NULL DEFAULT('') " +
                //             ") ON[PRIMARY]; " +
                //             " CREATE INDEX DistNumberIndex ON XZL_LockDistNumber(DistNumber ASC, DocNum DESC); " +
                //             " CREATE INDEX DistNumberDocNum ON XZL_LockDistNumber(DistNumber);";
                //    SqlCommand oCommand = WebApiApplication.sqlConn.CreateCommand();
                //    oCommand.CommandText = sqlCmd;
                //    oCommand.ExecuteNonQuery();
                //    oCommand.Dispose();
                //}

                SqlCommand oCommandinsert = WebApiApplication.sqlConn.CreateCommand();
                oCommandinsert.CommandText = sqlCmdInsert;
                oCommandinsert.ExecuteNonQuery();
                // return "{\"errcode\":0,\"errmsg\":\"OK\",\"data\":{}}";
                return Comm.Comm.GetJObject("0", "OK", new JObject { { "data", "" } });
            }
            catch (Exception e)
            {
                // return "{\"errcode\":" + e.HResult.ToString() + ",\"errmsg\":\"" + e.Message + "\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class GetTXController : ApiController
    {
        //获取套系编码 和名称

        public JObject Post([FromBody]JObject oPara)
        {
            try
            {
                Comm.Comm.GetsqlConn();
                string[] CompanyList = oPara.GetValue("Company").Value<string>().Split(',');
                DataTable oDt = new DataTable();
                foreach (string Company1 in CompanyList)
                {
                    string sqlCmd = "SELECT t2.FldValue Code,t2.Descr Name FROM  " +Company1+".dbo.CUFD T1 INNER JOIN "+Company1+".dbo.UFD1 T2 " +
                                    "ON T1.TableID=T2.TableID AND T1.FieldID=T2.FieldID AND T1.AliasID='txmc' AND T1.TableID='OSRN'";
                    DataTable oDt1 = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                    oDt.Merge(oDt1);
                    oDt.AcceptChanges();
                }

                oDt.DefaultView.Sort = "Code ASC";
                DataTable odt12 = oDt.DefaultView.ToTable(true);
                string str = "{\"data\":" + JsonConvert.SerializeObject(odt12) + "}";
                JObject oData = JObject.Parse(str);
                return Comm.Comm.GetJObject("0", "OK", oData);
            }
            catch (Exception e)
            {
                //return "{\"errcode\":"+e.HResult.ToString()+",\"errmsg\":\"" + e.Message + "+\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }

        }
    }
}

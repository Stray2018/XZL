﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using  System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using LZ4;

namespace CRAMERWebAPI.Controllers
{
    public class GetDataCPController : ApiController
    {
   
        // POST: api/GetDataCP
        public HttpResponseMessage Post([FromBody] JObject oPara)
        {
            double QuerTime = 0, SerialTime = 0, ZipTime = 0;
            //DataTable ResultData= new DataTable();
            //ResultData.Columns.Add("QuerTime", typeof(double));
            //ResultData.Columns.Add("SerialTime", typeof(double));
            //ResultData.Columns.Add("ZipTime", typeof(double));
            //ResultData.Columns.Add("ResultData", typeof(string));
            //ResultData.Columns.Add("ErrMsg", typeof(string));
            string oConn = oPara.GetValue("sqlConn").Value<string>();
            string sqlCmd = oPara.GetValue("sqlCmd").Value<string>();
            using (SqlConnection sqlCon = new SqlConnection(oConn))
            {
                try
                {
                    DataTable oDt = new DataTable("Data");
                    sqlCon.Open();
                    DateTime stTime = DateTime.Now;
                    // sqldataAdapter 方法
                    //SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(sqlCmd, sqlCon);
                    //DataTable oDt = new DataTable("Data");
                    //oSqlDataAdapter.Fill(oDt);

                    //SqlDataReader 方法
                    SqlCommand oSqlCommand = sqlCon.CreateCommand();
                    oSqlCommand.CommandTimeout = Int32.MaxValue;
                    oSqlCommand.CommandText = sqlCmd;
                    SqlDataReader dr = oSqlCommand.ExecuteReader();
                    oDt.Load(dr);
                    
                    TimeSpan t = DateTime.Now.Subtract(stTime);
                    sqlCon.Close();
                    QuerTime = (((TimeSpan) (DateTime.Now.Subtract(stTime))).TotalMilliseconds) /1000;
                    
                    string DataStr = string.Empty;
                    //if (oDt.Rows.Count > 0)
                    //{
                        stTime = DateTime.Now;
                        DataStr = JsonConvert.SerializeObject(oDt,Formatting.None);
                        double dataStrSize =Convert.ToDouble(Encoding.UTF8.GetByteCount(DataStr))/ Convert.ToDouble(1048576);
                        SerialTime = (((TimeSpan)(DateTime.Now.Subtract(stTime))).TotalMilliseconds) / 1000;
                        
                        stTime = DateTime.Now;
                        var compressed = Convert.ToBase64String(LZ4Codec.Wrap(Encoding.UTF8.GetBytes(DataStr)));
                        double compressedSIZE = Convert.ToDouble(Encoding.UTF8.GetByteCount(compressed))/Convert.ToDouble(1048576);
                        ZipTime = (((TimeSpan)(DateTime.Now.Subtract(stTime))).TotalMilliseconds) / 1000;
                        stTime = DateTime.Now;
                        HttpResponseMessage result = new HttpResponseMessage { Content = new StringContent(compressed, Encoding.GetEncoding("UTF-8"), "text/plain") }; 
                        result.Headers.Add("QuerTime",QuerTime.ToString());
                        result.Headers.Add("SerialTime", SerialTime.ToString());
                        result.Headers.Add("ZipTime", ZipTime.ToString());
                        result.Headers.Add("SendTime", stTime.ToString());
                        result.Headers.Add("BeforeZipSize", dataStrSize.ToString());
                        result.Headers.Add("afterZipSize", compressedSIZE.ToString());
                       
                        return result;
                   // }
                }
                catch (Exception e)
                {
                    //DataRow oDataRow = ResultData.NewRow();
                    //oDataRow["QuerTime"] = QuerTime;
                    //oDataRow["SerialTime"] = SerialTime;
                    //oDataRow["ZipTime"] = ZipTime;
                    //oDataRow["ResultData"] = "";
                    //oDataRow["ErrMsg"] = e.ToString();
                    //ResultData.Rows.Add(oDataRow);
                    //ResultData.AcceptChanges();
                    sqlCon.Close();
                    return null;
                }
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class GetDistNumberController : ApiController
    {
        //{"Company":"H_CD","ItemCode":"TMP01","whsCode":"CD001","KuanHao":"abc123"}
        public JObject Post([FromBody] JObject oPara)
        {
            Comm.Comm.GetsqlConn();
            string osql = string.Empty;
            try
            {

                string Company1 = oPara.GetValue("Company").Value<string>();
                string ItemCode = oPara.GetValue("ItemCode").Value<string>();
                string whsCodeP = oPara.GetValue("whsCode").Value<string>();
                string cjkhP = oPara.GetValue("cjkh").Value<string>();
                string whsCode = string.Empty;
                string cjkh = "''";
                foreach (string whscode1 in whsCodeP.Split(','))
                {
                    whsCode = whsCode + ",'" + whscode1 + "'";
                }

                whsCode = whsCode.Substring(1);
                if (!cjkhP.Equals(""))
                {
                    cjkh = string.Empty;
                    foreach (string cjkhP1 in cjkhP.Split(','))
                    {
                        cjkh = cjkh + ",'" + cjkhP1 + "'";
                    }

                    cjkh = cjkh.Substring(1);
                }
               

                string sqlCmd = "SELECT T1.U_cjkh,'"+ Company1 + "'Company,T2.WhsCode,T1.DistNumber,T1.ItemCode,T1.U_Name ,T1.U_zsh,T1.U_caizhi,T1.U_lszsh,T1.U_cpleibie," +
                                "T1.U_yanse,T1.U_jingdu,T1.U_chima,T1.U_zongzhong,T1.U_zhushizhong,T1.U_zhushishu,T1.U_fushizhong," +
                                "T1.U_fushishu,T1.U_MarkPrice,T2.Quantity,T1.U_biaoqianjia,T1.U_xiangkou,T1.U_jiebi,T1.U_gongyi FROM " + Company1+".dbo.OSRN T1 " +
                                "INNER JOIN "+Company1+".dbo.OSRQ T2 ON T1.AbsEntry=T2.MdAbsEntry AND T2.Quantity<>0 " +
                                "WHERE T1.ItemCode='"+ ItemCode + "' AND (T1.U_cjkh in("+cjkh+") or '"+ cjkhP + "'='') AND T2.WhsCode in ("+whsCode+")";
                osql = sqlCmd;
                DataTable ALLdistNumber = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                string str = "{\"data\":" + JsonConvert.SerializeObject(ALLdistNumber) + "}";
                JObject oData = JObject.Parse(str);
                return Comm.Comm.GetJObject("0", "OK", oData);
            }
            catch (Exception e)
            {
                // return "{\"errcode\":" + e.HResult.ToString() + ",\"errmsg\":\"" + e.Message + "\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message+":"+osql, new JObject { { "data", "" } });
            }
        }
    }
}

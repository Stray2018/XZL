﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;

namespace CRAMERWebAPI.Controllers
{
    public class whsCodeController : ApiController
    {
        //{"Company":"H_CD"}
        public JObject Post([FromBody] JObject oPara)
        {
            Comm.Comm.GetsqlConn();

            try
            {

                string Company1 = oPara.GetValue("Company").Value<string>();
               
                string sqlCmd = "SELECT WhsCode,WhsName,'"+ Company1+ "' Company FROM "+Company1+".dbo.OWHS";
                DataTable ALLwhsCode = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                string str = "{\"data\":" + JsonConvert.SerializeObject(ALLwhsCode) + "}";
                JObject oData = JObject.Parse(str);
                return Comm.Comm.GetJObject("0", "OK", oData);
            }
            catch (Exception e)
            {
                // return "{\"errcode\":" + e.HResult.ToString() + ",\"errmsg\":\"" + e.Message + "\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }
        }
    }
}

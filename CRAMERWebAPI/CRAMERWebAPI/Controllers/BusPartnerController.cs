﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class BusPartnerController : ApiController
    {
    
        // POST: api/BusPartner
        //多公司选择
        public JObject Post([FromBody]JObject oPara)
        {
            try
            {
                Comm.Comm.GetsqlConn();
                string CompanyList = oPara.GetValue("Company").Value<string>();
                if (CompanyList.Equals(""))
                {
                    return Comm.Comm.GetJObject("-10", "未选择公司", new JObject {{ "data", "" }});
                }

                DataTable oDt = new DataTable();
                string sqlCmd = string.Empty;
                string[] CompanyList1 = CompanyList.Split(',');
                foreach (string Company1 in CompanyList1)
                {
                    sqlCmd = "select '"+Company1+ "' Company, CardCode,CardName,CardFname,Phone1,Phone2,isnull(U_khppbz,'') U_khppbz,isnull(U_beilv,0) U_beilv," +
                             "isnull(U_zkl,0) U_zkl from ["+Company1+"].dbo.OCRD WHERE CardType = 'C'";
                  DataTable  oDt1 = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                    oDt.Merge(oDt1);
                    oDt.AcceptChanges();
                }

                
                string str = "{\"data\":"+JsonConvert.SerializeObject(oDt)+"}";
                //str = "{\"errcode\":0,\"errmsg\":\"OK\",\"data\":" + str + "}";
                // HttpResponseMessage result = new HttpResponseMessage { Content = new StringContent(str, Encoding.GetEncoding("UTF-8"), "application/Json") };
                JObject oData  =JObject.Parse(str);
                return Comm.Comm.GetJObject("0", "OK", oData);
            }
            catch (Exception e)
            {
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }
            
        }
    }
}

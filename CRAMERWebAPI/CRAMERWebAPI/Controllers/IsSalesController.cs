﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class IsSalesController : ApiController
    {
        public JObject Post([FromBody] JObject oPara)
        {
            try
            {
                Comm.Comm.GetsqlConn();
                DataTable oDt =JsonConvert.DeserializeObject<DataTable>(oPara.GetValue("data").ToString());
                DataTable ResultoDt  = new DataTable();
                DataView odtView = oDt.DefaultView;
                DataTable CompanyList = odtView.ToTable(true, "Company");
                foreach (DataRow Company1 in CompanyList.Rows)
                {
                    string CompanyPer = Company1["Company"].ToString().Trim();
                    DataRow[] oDataRows = oDt.Select("Company = '" + CompanyPer + "' and ManSer = 'Y'");
                    string DistNumber = string.Empty;
                    foreach (DataRow PerRow in oDataRows)
                    {
                        DistNumber += ",'" + PerRow["DistNumber"].ToString().Trim() + "'";
                    }

                    DistNumber = DistNumber.Substring(1);

                    string sqlCmd = "select T1.DistNumber from ["+ CompanyPer + "].dbo.OSRN T1 inner join ["+ CompanyPer + "].dbo.OSRQ T2 on t1.AbsEntry=t2.MdAbsEntry " +
                                    "where t1.DistNumber in("+DistNumber+") and t2.Quantity = 0";
                    DataTable oDt1 = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                    ResultoDt.Merge(oDt1);
                    ResultoDt.AcceptChanges();
                }
                string str = "{\"data\":" + JsonConvert.SerializeObject(ResultoDt) + "}";
                JObject oData = JObject.Parse(str);
                string errCode = ResultoDt.Rows.Count > 0 ? "-111" : "0";
                string errMsg = ResultoDt.Rows.Count > 0 ? "存在已销售的序列号！" : "OK";
                return Comm.Comm.GetJObject(errCode, errMsg, oData);
            }
            catch (Exception e)
            {
                //return "{\"errcode\":"+e.HResult.ToString()+",\"errmsg\":\"" + e.Message + "+\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }

        }
    }
}

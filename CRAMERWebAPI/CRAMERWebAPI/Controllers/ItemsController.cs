﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class ItemsController : ApiController
    {
        // POST: api/Items
        public JObject Post([FromBody]JObject oPara)
        {
            try
            {
                Comm.Comm.GetsqlConn();
                string[] CompanyList = oPara.GetValue("Company").Value<string>().Split(',');
                DataTable oDt = new DataTable();
                foreach (string  Company1 in CompanyList)
                {
                    string sqlCmd = "SELECT t1.U_cjkh, t1.ItemCode,T1.itemName,T1.U_cpleibie,T1.U_txmc, 'Y' ManSer FROM [" + Company1+"].dbo.OSRN T1 " +
                                    "INNER JOIN ["+Company1+"].dbo.OSRQ T2 ON T1.AbsEntry = T2.MdAbsEntry " +
                                    "WHERE T2.Quantity <> 0 " +
                                    "GROUP BY t1.ItemCode, t1.U_cjkh,T1.itemName,T1.U_cpleibie ,T1.U_txmc";
                    DataTable oDt1 = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                    oDt.Merge(oDt1);
                    oDt.AcceptChanges();
                }

                oDt.DefaultView.Sort = "ItemCode ASC,U_cjkh ASC";
                DataTable odt12 = oDt.DefaultView.ToTable(true);
                string str = "{\"data\":" + JsonConvert.SerializeObject(odt12) + "}";
                JObject oData = JObject.Parse(str);
                return Comm.Comm.GetJObject("0", "OK", oData);
            }
            catch (Exception e)
            {
                //return "{\"errcode\":"+e.HResult.ToString()+",\"errmsg\":\"" + e.Message + "+\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }
            
        }
    }
}

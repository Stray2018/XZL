﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SAPbobsCOM;

namespace CRAMERWebAPI.Controllers
{

    //"{"DocNum":"StrayTest","DocDate":"20190201","CardCode":"Stray","DisCount":10,
    //"DocRow":[{"ItemCode":"TMP001","Quantity":1,"Price":1235.00,"DistNumber":"11223344","whsCode":"cd001","Company":"H_CD","ManSer":"Y"},
    //{"ItemCode":"TMP001","Quantity":1,"Price":1235.00,"DistNumber":"11223344","whsCode":"cd001","Company":"H_CD","ManSer":"Y"}]}"

    //ItemCode:物料编码，Quantity：数量，Price 单价，DistNumber：序列号,whsCode:仓库编码，Company:序列号所在数据库，ManSer:是否管理序列号,
    //DisCount:折扣
    public class CreateOrderController : ApiController
    {
        public JObject Post([FromBody] JObject oPara)
        {
            try
            {
                Comm.Comm.GetsapCompany();
                bool isAddDoc = true;
                string ErrDistNumber = String.Empty;
                SAPbobsCOM.Company oCompany = WebApiApplication.sapCompany;
                SAPbobsCOM.Documents oDoc = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
                string DocDate = oPara.GetValue("DocDate").Value<string>();
                oDoc.DocDate = DateTime.ParseExact(DocDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                oDoc.DocDueDate  = DateTime.ParseExact(DocDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);

                string DocNum = oPara.GetValue("DocNum").Value<string>();
                double DistCount = oPara.GetValue("DisCount").Value<double>();
               // oDoc.NumAtCard = oPara.GetValue("DocNum").Value<string>();
                oDoc.CardCode = oPara.GetValue("CardCode").Value<string>();

                SAPbobsCOM.Document_Lines oDocLines = oDoc.Lines;
                JArray jArray = (JArray)JsonConvert.DeserializeObject(oPara["DocRow"].ToString());
                foreach (var PerRow in jArray)
                {
                    string DistNumber = PerRow["DistNumber"].ToString();
                    oDocLines.ItemCode = PerRow["ItemCode"].ToString();
                    oDocLines.Quantity = double.Parse(PerRow["Quantity"].ToString());
                    oDocLines.DiscountPercent = DistCount;
                    oDocLines.WarehouseCode = PerRow["whsCode"].ToString();
                    oDocLines.FreeText = DocNum;
                    oDocLines.UserFields.Fields.Item("U_FreeTxt").Value = DistNumber;
                    oDocLines.UnitPrice =  double.Parse(PerRow["Price"].ToString());
                    if (PerRow["ManSer"].ToString().Trim().Equals("Y"))
                    {
                        oDocLines.SerialNumbers.InternalSerialNumber = DistNumber;
                        string sqlCmd = "select T1.*,isnull(T2.Quantity,0)  AS osrqQty from OSRN T1 left JOIN OSRQ T2 ON T1.ItemCode=T2.ItemCode AND T1.SysNumber=T2.SysNumber and T2.Quantity<>0 WHERE T1.DistNumber = '" + DistNumber + "' and T1.ItemCode = '" + oDocLines.ItemCode + "'";
                        DataTable oDt = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                        string osrqQty = oDt.Rows[0]["osrqQty"].ToString();
                        double Quantity = double.Parse(osrqQty);
                        if (Quantity.Equals(0))
                        {
                            isAddDoc = false;
                            ErrDistNumber +=","+ DistNumber;
                        }
                        oDocLines.UserFields.Fields.Item("U_Name").Value = oDt.Rows[0]["U_Name"].ToString();
                        oDocLines.UserFields.Fields.Item("U_cjkh").Value = oDt.Rows[0]["U_cjkh"].ToString();
                        oDocLines.UserFields.Fields.Item("U_cpleibie").Value = oDt.Rows[0]["U_cpleibie"].ToString();
                        oDocLines.UserFields.Fields.Item("U_beizhu").Value = oDt.Rows[0]["U_beizhu"].ToString();
                        oDocLines.UserFields.Fields.Item("U_cstiaoma").Value = oDt.Rows[0]["U_cstiaoma"].ToString();

                    }
                    oDocLines.Add();
                }
                int errcode = 0;
                string errmsg = string.Empty;
                if (isAddDoc.Equals(true))
                {
                    oDoc.Add();
                    oCompany.GetLastError(out errcode, out errmsg);
                }
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oDocLines);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                if (isAddDoc.Equals(false))
                {
                    errcode = -110;
                }
                if (errcode != 0  )
                {
                    //return "{\"errcode\":"+errcode.ToString()+",\"errmsg\":\""+errmsg+"\",\"data\":{}}";
                    errmsg += "序列号不在库存中,请检查:" + (ErrDistNumber.Length >1? ErrDistNumber.Substring(1):"");
                    JObject oData = new JObject {{"data", ""}};
                    return Comm.Comm.GetJObject(errcode.ToString(), errmsg, oData);
                }
                else
                {
                   // return "{\"errcode\":0,\"errmsg\":\"OK\",\"data\":{\"DocNum\":\""+oCompany.GetNewObjectKey()+"\"}}";
                    JObject oData = new JObject { { "data", new JObject { {"DocNum", oCompany.GetNewObjectKey() } } } };
                    return Comm.Comm.GetJObject(errcode.ToString(), errmsg, oData);
                }
            }
            catch (Exception e)
            {
                //  return "{\"errcode\":" + e.HResult.ToString() + ",\"errmsg\":\"" + e.Message + "\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.ToString(), new JObject { {"data","" }});
            }
        }
    }
}

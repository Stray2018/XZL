﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRAMERWebAPI.Controllers
{
    public class ItemTypeController : ApiController
    {
        public JObject Post([FromBody]dynamic oPara)
        {
            try
            {
                Comm.Comm.GetsqlConn();
               // string[] CompanyList = oPara.GetValue("Company").Value<string>().Split(',');
               
                string sqlCmd = "SELECT T2.FldValue 'U_cpleibie',T2.Descr FROM CUFD T1 " +
                                "INNER JOIN UFD1 T2 ON T1.TableID=T2.TableID AND T1.FieldID=T2.FieldID " +
                                "and T1.TableID = 'OSRN' AND T1.AliasID = 'cpleibie' ";
                    DataTable oDt = Comm.Comm.GetDt(WebApiApplication.sqlConn, sqlCmd);
                string str = "{\"data\":" + JsonConvert.SerializeObject(oDt) + "}";
                JObject oData = JObject.Parse(str);
                return Comm.Comm.GetJObject("0", "OK", oData);
            }
            catch (Exception e)
            {
                //return "{\"errcode\":"+e.HResult.ToString()+",\"errmsg\":\"" + e.Message + "+\",\"data\":{}}";
                return Comm.Comm.GetJObject(e.HResult.ToString(), e.Message, new JObject { { "data", "" } });
            }

        }
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using Newtonsoft.Json.Linq;
using SAPbobsCOM;

namespace CRAMERWebAPI.Comm
{
    public class Comm
    {
        public static DataTable GetDt(SqlConnection oSqlConn, string sqlCmd)
        {
            SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd, oSqlConn);
            DataTable dt = new DataTable();
            dt.TableName = "data";
            sqlDa.Fill(dt);
            return dt;
        }

        public static string GetsqlConn()
        {
            if (WebApiApplication.sqlConn.State != ConnectionState.Closed &&
                WebApiApplication.sqlConn.State != ConnectionState.Broken)
            {
                return "{\"Code\":0,ErrMsg:\"OK\",\"data\":{}}";
            }
            string oConnstr = WebConfigurationManager.ConnectionStrings["SAPCon"].ConnectionString;
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            foreach (string newstr in oConnstr.Split(';'))
            {
                string[] oNewstr = newstr.Split('=');
                oDictionary.Add(oNewstr[0].Trim(), oNewstr[1]);
            }
            SqlConnectionStringBuilder oSqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            oSqlConnectionStringBuilder.DataSource = oDictionary["sapServer"];
            oSqlConnectionStringBuilder.UserID = oDictionary["sqlID"];
            oSqlConnectionStringBuilder.Password = oDictionary["sqlPassword"];
            oSqlConnectionStringBuilder.InitialCatalog = oDictionary["DataBase"];
            WebApiApplication.sapCompany.Server = oDictionary["sapServer"];
            WebApiApplication.sapCompany.LicenseServer = oDictionary["sapLicense"];
            WebApiApplication.sapCompany.CompanyDB = oDictionary["DataBase"];
            WebApiApplication.sapCompany.SLDServer = "https://" + WebApiApplication.sapCompany.LicenseServer.Split(':')[0] + ":40000";
            switch (oDictionary["DbServerType"])
            {
                case "2008":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "2012":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "2014":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "2016":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "2017":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2017;
                    break;
            }

            WebApiApplication.sapCompany.DbUserName = oDictionary["sqlID"];
            WebApiApplication.sapCompany.DbPassword = oDictionary["sqlPassword"];
            WebApiApplication.sapCompany.UserName = oDictionary["sapID"];
            WebApiApplication.sapCompany.Password = oDictionary["sapPassword"];
            WebApiApplication.sqlConn = new SqlConnection(oSqlConnectionStringBuilder.ConnectionString);
            try
            {
                WebApiApplication.sqlConn.Open();
                return "{\"Code\":0,ErrMsg:\"OK\",\"data\":{}}";
            }
            catch (Exception e)
            {
                return "{\"Code\":"+e.Message.ToString()+",ErrMsg:\""+e.Message+"\",\"data\":{}}";
            }
        }
        public static string GetsapCompany()
        {
            if (WebApiApplication.sapCompany.Connected)
            {
                return "{\"Code\":0,ErrMsg:\"OK\",\"data\":{}}";
            }
            string oConnstr = WebConfigurationManager.ConnectionStrings["SAPCon"].ConnectionString;
            Dictionary<string, string> oDictionary = new Dictionary<string, string>();
            foreach (string newstr in oConnstr.Split(';'))
            {
                string[] oNewstr = newstr.Split('=');
                oDictionary.Add(oNewstr[0].Trim(), oNewstr[1]);
            }
            SqlConnectionStringBuilder oSqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            oSqlConnectionStringBuilder.DataSource = oDictionary["sapServer"];
            oSqlConnectionStringBuilder.UserID = oDictionary["sqlID"];
            oSqlConnectionStringBuilder.Password = oDictionary["sqlPassword"];
            oSqlConnectionStringBuilder.InitialCatalog = oDictionary["DataBase"];
            WebApiApplication.sapCompany.Server = oDictionary["sapServer"];
            WebApiApplication.sapCompany.LicenseServer = oDictionary["sapLicense"];
            WebApiApplication.sapCompany.CompanyDB = oDictionary["DataBase"];
            WebApiApplication.sapCompany.language = BoSuppLangs.ln_Chinese;
            WebApiApplication.sapCompany.SLDServer = "https://" + WebApiApplication.sapCompany.LicenseServer.Split(':')[0] + ":40000";
            switch (oDictionary["DbServerType"])
            {
                case "2008":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "2012":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "2014":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "2016":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "2017":
                    WebApiApplication.sapCompany.DbServerType = BoDataServerTypes.dst_MSSQL2017;
                    break;
            }

            WebApiApplication.sapCompany.DbUserName = oDictionary["sqlID"];
            WebApiApplication.sapCompany.DbPassword = oDictionary["sqlPassword"];
            WebApiApplication.sapCompany.UserName = oDictionary["sapID"];
            WebApiApplication.sapCompany.Password = oDictionary["sapPassword"];
            WebApiApplication.sqlConn = new SqlConnection(oSqlConnectionStringBuilder.ConnectionString);
            try
            {
                if (WebApiApplication.sapCompany.Connect() != 0)
                {
                    return "{\"Code\":"+WebApiApplication.sapCompany.GetLastErrorCode().ToString()+",ErrMsg:\""+WebApiApplication.sapCompany.GetLastErrorDescription()+"\",\"data\":{}}";
                }
               return "{\"Code\":0,ErrMsg:\"OK\",\"data\":{}}";
            }
            catch (Exception e)
            {
                return "{\"Code\":" + e.Message.ToString() + ",ErrMsg:\"" + e.Message + "\",\"data\":{}}";
            }
        }

        public static JObject GetJObject(string errCode,string errMsg,JObject oData)
        {
            JObject newJObject = new JObject
            {
                {"errcode",errCode}, {"errmsg",errMsg}
        };
            newJObject.Add("data",oData["data"]);
            return newJObject;
        }
    }
}